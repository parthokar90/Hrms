<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             UsersTableSeeder::class,
             OvertimeTableSeeder::class,
             EmployeesTableSeeder::class,
             DepartmentTableSeeder::class,
             DesignationTableSeeder::class,
             MaritalStatusesSeederTable::class,
             AttendanceSetupSeeder::class,
             WeekLeaveTableSeeder::class,
             LeaveTypeTableSeeder::class,
             CompanyInformationTableSeeder::class,
         ]);
    }
}
