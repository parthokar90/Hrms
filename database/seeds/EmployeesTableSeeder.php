<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'empFirstName'=>'Admin',
            'empLastName'=>'Admin',
            'employeeId'=>'0001',
            'empDesignationId'=>1,
            'empGenderId'=>1,
            'empRole'=>1,
            'empPassword'=>bcrypt('admin'),
            'empAccStatus'=>1,
            'empEmail'=>'admin@email.com',
            'empPhone'=>'000000000000001',
            'empDepartmentId'=>1,
            'payment_mode'=>'Cash',
            'empMaritalStatusId'=>1,
            'empJoiningDate'=>\Carbon\Carbon::now()->toDateString(),
            'probation_period'=>6,

        ]);
    }
}
