<?php

use Illuminate\Database\Seeder;

class validityInformation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbhrms_activation')->insert([
            'trackId'=>"validity",
            'haOption'=>"Days",
            'haValue'=>"1",
            'haSDate'=>"MjAxOC0wOC0xOA==",
            'haEDate'=>"MjAxOC0wOC0xNw==",
        ]);
    }
}
