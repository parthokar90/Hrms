<?php

use Illuminate\Database\Seeder;

class activationLoginInformation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbhrms_activation')->insert([
            'trackId'=>"access",
            'haOption'=>"d50a20bbbe810c3ab0925afdea053176",
            'haValue'=>"a835bb9356de7101ff5fefce76498545",
            'haSDate'=>"53a4b48d6e946faa59c35643d775c9d1",
        ]);
    }
}
