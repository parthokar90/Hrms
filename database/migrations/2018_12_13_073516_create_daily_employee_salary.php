<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyEmployeeSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_daily_salary', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->nullable();
            $table->integer('line_id')->nullable();
            $table->string('two_hour_ot')->nullable();
            $table->string('two_hour_ot_amount')->nullable();
            $table->string('total_ot')->nullable();
            $table->string('total_ot_amount')->nullable();
            $table->string('total_gross')->nullable();
            $table->date('salary_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_daily_salary');
    }
}
