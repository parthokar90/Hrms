<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeEducationInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_education_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->index('emp_id');
            $table->string('empExamTitle','50');
            $table->string('empInstitution','150');
            $table->string('empResult','20');
            $table->string('empScale','20');
            $table->string('empPassYear','4');
            $table->string('empCertificate','250')->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('modified_by',50)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('employee_education_infos');
    }
}
