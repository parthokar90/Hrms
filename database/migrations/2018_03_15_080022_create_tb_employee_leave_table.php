<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbEmployeeLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_employee_leave', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->index('employee_id');
            $table->integer('leave_type_id');
            $table->index('leave_type_id');
            $table->integer('leave_taken');
            $table->integer('leave_available');
            $table->timestamps();
            $table->string('year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_employee_leave');
    }
}
