<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacancyApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancy_application',function (Blueprint $table){
            $table->increments('id');
            $table->string('name',30);
            $table->string('phone',20)->nullable();
            $table->integer('vacancy_id');
            $table->timestamp('submittedDate')->nullable();
            $table->string('interviewStatus','10')->nullable();
            $table->timestamp('interviewDate')->nullable();
            $table->string('jobStatus','10')->nullable();
            $table->text('interviewNote')->nullable();
            $table->string('priorityStatus',15)->nullable();
            $table->string('attachment',180);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancy_application');
    }
}
