<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbLeaveOfEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_of_employee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_id','50');
            $table->index('emp_id');
            $table->string('leave_type_id','50');
            $table->index('leave_type_id');
            $table->string('total','50');
            $table->date('month','30');
            $table->index('month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_of_employee');
    }
}
