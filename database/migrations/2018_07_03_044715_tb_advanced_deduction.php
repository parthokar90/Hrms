<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbAdvancedDeduction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_loan_deduction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->decimal('loan_amount',18,2)->nullable();
            $table->decimal('month_wise_deduction_amount',18,2)->nullable();
            $table->string('month',20);
            $table->integer('given_id');
            $table->date('complete_month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_loan_deduction');
    }
}
