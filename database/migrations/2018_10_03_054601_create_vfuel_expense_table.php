<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVfuelExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbvfuel_expense', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyinteger('vehicleId')->nullable();
            $table->tinyinteger('fuelType')->nullable();
            $table->string('vexReferences')->nullable();
            $table->string('quantity',10)->nullable();
            $table->string('amount',10)->nullable();
            $table->date('vexDate')->nullable();
            $table->text('vexDescription')->nullable();
            $table->text('vexAttachment')->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbvfuel_expense');
    }
}
