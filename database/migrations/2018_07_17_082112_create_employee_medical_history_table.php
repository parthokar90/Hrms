<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeMedicalHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbemployee_medical_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empId')->nullable();
            $table->boolean('curHCs')->nullable();
            $table->text('curHCt')->nullable();
            $table->boolean('curMedicationss')->nullable();
            $table->text('curMedicationst')->nullable();
            $table->boolean('mcpis')->nullable();
            $table->text('mcpit')->nullable();
            $table->boolean('pastSurgerys')->nullable();
            $table->text('pastSurgeryt')->nullable();
            $table->boolean('ppmips')->nullable();
            $table->text('ppmipt')->nullable();
            $table->boolean('visionProblems')->nullable();
            $table->text('visionProblemt')->nullable();
            $table->boolean('hearProblems')->nullable();
            $table->text('hearProblemt')->nullable();
            $table->boolean('skillConditions')->nullable();
            $table->text('skillConditiont')->nullable();
            $table->boolean('schands')->nullable();
            $table->text('schandt')->nullable();
            $table->boolean('chemicals')->nullable();
            $table->text('chemicalt')->nullable();
            $table->boolean('noises')->nullable();
            $table->text('noiset')->nullable();
            $table->boolean('radiations')->nullable();
            $table->text('radiationt')->nullable();
            $table->boolean('latexs')->nullable();
            $table->text('latext')->nullable();
            $table->boolean('drugss')->nullable();
            $table->text('drugst')->nullable();
            $table->boolean('chemicalass')->nullable();
            $table->text('chemicalast')->nullable();
            $table->boolean('insects')->nullable();
            $table->text('insectt')->nullable();
            $table->boolean('fragrancess')->nullable();
            $table->text('fragrancest')->nullable();
            $table->boolean('others')->nullable();
            $table->text('othert')->nullable();
            $table->integer('createdBy')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbemployee_medical_history');
    }
}
