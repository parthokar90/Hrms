<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbbooking_history', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyinteger('biId')->nullable();
            $table->string('bookedBy',200)->nullable();
            $table->string('purpose',200)->nullable();
            $table->date('biDate')->nullable();
            $table->string('biStartTime')->nullable();
            $table->string('biEndTime')->nullable();
            $table->string('status',1)->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbbooking_history');
    }
}
