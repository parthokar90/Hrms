<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_structure', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->nullable();
            $table->integer('c_leave_id')->nullable();
            $table->integer('c_leave_total')->nullable();
            $table->integer('s_leave_id')->nullable();
            $table->integer('s_leave_total')->nullable();
            $table->integer('e_leave_id')->nullable();
            $table->integer('e_leave_total')->nullable();
            $table->date('month')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_structure');
    }
}
