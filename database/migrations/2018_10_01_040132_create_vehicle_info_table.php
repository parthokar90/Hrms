<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbvehicle_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vehicleName',150)->nullable();
            $table->string('registrationNumber',120)->nullable();
            $table->string('insuranceNumber',120)->nullable();
            $table->text('remarks')->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbvehicle_info');
    }
}
