<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Attendancesetup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_setup', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shiftName','20')->nullable();
            $table->string('entry_time','30')->nullable();
            $table->time('exit_time')->nullable();
            $table->index('exit_time')->nullable();
            $table->time('max_entry_time')->nullable();
            $table->index('max_entry_time')->nullable();
            $table->string('min_exit_time','30')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_setup');
    }
}
