<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbexpenselistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbexpenselist', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('categoryId');
            $table->index('categoryId');
            $table->string('amount');
            $table->string('reference')->nullable();
            $table->text('description')->nullable();
            $table->date('expenseDate');
            $table->index('expenseDate');
            $table->text('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbexpenselist');
    }
}
