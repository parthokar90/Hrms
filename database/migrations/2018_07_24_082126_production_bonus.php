<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductionBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_production_bonus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->index();
            $table->decimal('production_amount',18,2)->default(0);
            $table->decimal('production_percent',18,2)->default(0);
            $table->string('production_percent_total')->default(0);
            $table->string('month',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_production_bonus');
    }
}
