<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbvehicle_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyinteger('vehicleId')->nullable();
            $table->tinyinteger('vexCategoryId')->nullable();
            $table->string('vexReferences')->nullable();
            $table->string('amount',10)->nullable();
            $table->date('vexDate')->nullable();
            $table->text('vexDescription')->nullable();
            $table->text('vexAttachment')->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbvehicle_expenses');
    }
}
