<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbvehicle_booking', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyinteger('vehicleId')->nullable();
            $table->string('bookedBy',200)->nullable();
            $table->string('driverName',200)->nullable();
            $table->string('purpose',200)->nullable();
            $table->date('vbDate')->nullable();
            $table->string('vbStartTime')->nullable();
            $table->string('vbEndTime')->nullable();
            $table->string('status',1)->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbvehicle_booking');
    }
}
