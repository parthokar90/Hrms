<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbLeaveApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_leave_application', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->index('employee_id');
            $table->integer('leave_type_id');
            $table->index('leave_type_id');
            $table->date('leave_starting_date');
            $table->index('leave_starting_date');
            $table->date('leave_ending_date');
            $table->index('leave_ending_date');
            $table->integer('actual_days');
            $table->integer('status');
            $table->string('approved_by','20');
            $table->text('description')->nullable();
            $table->text('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_leave_application');
    }
}
