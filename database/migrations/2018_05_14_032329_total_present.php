<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TotalPresent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_total_present', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_id',50)->nullable();
            $table->index('emp_id');
            $table->string('total_present',50)->nullable();
            $table->string('total_absent',50)->nullable();
            $table->date('month',30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_total_present');
    }
}
