<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliceVerificationDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbpolice_verification_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empId')->nullable();
            $table->date('pvDate')->nullable();
            $table->text('vpkt')->nullable();
            $table->text('vpkz')->nullable();
            $table->text('mpsz')->nullable();
            $table->string('fathersJob')->nullable();
            $table->string('fathersNationality')->nullable();
            $table->integer('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbpolice_verification_data');
    }
}
