<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbSalaryProcessCheck extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_salary_process_check', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('process_id');
            $table->date('month','20')->nullable();
            $table->date('start_date','20')->nullable();
            $table->date('end_date','20')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_salary_process_check');
    }
}
