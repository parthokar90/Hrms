<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaturnityLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_maturnity_leave', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_id')->nullable();
            $table->string('leave_type_id')->nullable();
            $table->string('leave_start')->nullable();
            $table->string('leave_end')->nullable();
            $table->string('leave_total')->nullable();
            $table->string('leave_month')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_maturnity_leave');
    }
}
