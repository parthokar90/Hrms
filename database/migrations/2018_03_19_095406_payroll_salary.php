<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PayrollSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_salary', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->index('emp_id');
            $table->integer('grade_id');
            $table->index('grade_id');
            $table->decimal('basic_salary',18,2);
            $table->decimal('house_rant',18,2);
            $table->decimal('medical',18,2);
            $table->decimal('transport',18,2);
            $table->decimal('food',18,2);
            $table->decimal('other',18,2);
            $table->string('total_employee_salary');
            $table->string('current_month',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_salary');
    }
}
