<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbSalaryHistoryInstallment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_salary_history_installment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->nullable()->index();
            $table->integer('grade_id')->nullable()->index();
            $table->integer('department_id')->nullable()->index();
            $table->integer('designation_id')->nullable();
            $table->index('designation_id')->nullable();
            $table->decimal('basic_salary',18,2)->nullable();
            $table->decimal('house_rant',18,2)->nullable();
            $table->decimal('medical',18,2)->nullable();
            $table->decimal('transport',18,2)->nullable();
            $table->decimal('food',18,2)->nullable();
            $table->decimal('other',18,2)->nullable();
            $table->decimal('gross',18,2)->nullable();
            $table->string ('overtime',50)->nullable();
            $table->string ('overtime_rate',50)->nullable();
            $table->decimal('overtime_amount',18,2)->nullable();
            $table->string ('present',50)->nullable();
            $table->string ('absent',50)->nullable();
            $table->decimal('absent_deduction_amount',18,2)->nullable();
            $table->decimal('gross_pay',18,2)->nullable();
            $table->decimal('one_year_bonus',18,2)->nullable();
            $table->decimal('attendance_bonus',18,2)->nullable();
            $table->decimal('attendance_bonus_percent',18,2)->nullable();
            $table->decimal('festival_bonus_amount',18,2)->nullable();
            $table->decimal('festival_bonus_percent',18,2)->nullable();
            $table->decimal('increment_bonus_amount',18,2)->nullable();
            $table->decimal('increment_bonus_percent',18,2)->nullable();
            $table->decimal('normal_deduction',18,2)->nullable();
            $table->decimal('advanced_deduction',18,2)->nullable();
            $table->decimal('production_bonus',18,2)->nullable();
            $table->decimal('production_percent',18,2)->nullable();
            $table->string ('working_day',50)->nullable();
            $table->string ('weekend',50)->nullable();
            $table->string ('holiday',50)->nullable();
            $table->string ('total_payable_days',50)->nullable();
            $table->string('leave')->nullable();
            $table->string('total')->nullable();
            $table->decimal('net_amount',18,2)->nullable();
            $table->date('month',20)->nullable()->index();
            $table->string('dates',20)->nullable()->index();
            $table->date('start_date',20)->nullable()->index();
            $table->date('end_date',20)->nullable()->index();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_salary_history_installment');
    }
}
