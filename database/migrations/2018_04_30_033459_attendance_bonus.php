<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AttendanceBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_bonus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bonus_id')->index();
            $table->decimal('emp_gross',18,2)->default(0);
            $table->decimal('bonus_amount',18,2)->default(0);
            $table->decimal('bonus_percent',18,2)->default(0);
            $table->string('emp_total_percent','30')->default(0);
            $table->string('emp_total_amount','30')->default(0);
            $table->integer('bonus_given_id');
            $table->string('month','30');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_bonus');
    }
}
