<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grade_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gradeId')->index()->nullable();
            $table->string('basicSalary',100)->nullable();
            $table->string('houseRent',100)->nullable();
            $table->string('medical',100)->nullable();
            $table->string('transport',100)->nullable();
            $table->string('food',100)->nullable();
            $table->string('month',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grade_history');
    }
}
