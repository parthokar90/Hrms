<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbcompanyInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('tbcompany_information', function (Blueprint $table) {
        $table->increments('id');
        $table->string('company_name');
        $table->string('company_phone')->nullable();
        $table->string('company_email')->nullable();
        $table->string('company_address1')->nullable();
        $table->string('company_address2')->nullable();
        $table->string('bComName')->nullable();
        $table->string('bComPhone')->nullable();
        $table->string('bComAddress')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbcompany_information');
    }
}
