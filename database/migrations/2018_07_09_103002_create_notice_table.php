<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbnotices', function (Blueprint $table) {
            $table->increments('id');
            $table->date('startDate')->nullable();
            $table->date('endDate')->nullable();
            $table->text('noticeDescription')->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->tinyinteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbnotices');
    }
}
