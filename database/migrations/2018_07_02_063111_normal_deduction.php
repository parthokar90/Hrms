<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NormalDeduction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_deduction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->index('emp_id');
            $table->decimal('normal_deduction_amount',18,2)->nullable();
            $table->string('month',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_deduction');
    }
}
