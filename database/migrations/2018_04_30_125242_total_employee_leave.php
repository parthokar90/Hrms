<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TotalEmployeeLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_employee_leave', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_idss')->nullable();
            $table->index('emp_idss');
            $table->string('total_leaves_taken','40')->nullable();
            $table->date('month','30')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_employee_leave');
    }
}
