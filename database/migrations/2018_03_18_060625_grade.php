<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Grade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_grade', function (Blueprint $table) {
            $table->increments('id');
            $table->string('grade_name',150);
            $table->decimal('basic_salary');
            $table->decimal('house_rant',18,2);
            $table->decimal('transport',18,2);
            $table->decimal('medical',18,2);
            $table->decimal('food',18,2);
            $table->decimal('others',18,2);
            $table->decimal('total_salary',18,2);
            $table->string('created_by',50);
            $table->string('modified_by',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_grade');
    }
}
