<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Session;
use Redirect;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TemporaryEmployeeProxyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee_proxy_card_list=DB::table('tbtemp_employee_proxy')
        ->leftJoin('users','users.id','=','tbtemp_employee_proxy.createdBy')
        ->select('tbtemp_employee_proxy.*', 'users.name as created_by')
        ->orderBy('tbtemp_employee_proxy.employee_id')
        ->get();
        return view('attendance.temporary_employee_proxy_card_list',compact('employee_proxy_card_list'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function merge_attendance(){
//        return "jkk";
        $tempData=DB::select("SELECT temporary_card_attendance.*, employees.id as emp_id FROM `temporary_card_attendance` JOIN employees ON temporary_card_attendance.empCardNumber = employees.empCardNumber");
        $attData=array();
        $cardNumbers=array();
        foreach ($tempData as $td) {
            $cardNumbers[]=$td->empCardNumber;
            $emp_id = $td->emp_id;
            $checkEmp = DB::table('attendance')->where(['emp_id' => $emp_id, 'date' => $td->date])->get();
            if (!count($checkEmp)) {
                $attData[] = [
                    'emp_id' => $emp_id,
                    'in_time' => $td->in_time,
                    'out_time' => $td->out_time,
                    'date' => $td->date,

                ];
            }
        }
//        dd($cardNumbers);

        if(!empty($cardNumbers)){
            DB::table('temporary_card_attendance')->whereIn('empCardNumber',$cardNumbers)->delete();
            DB::table('tbtemp_employee_proxy')->whereIn('proxy_number',$cardNumbers)->delete();

        }

        if(!empty($attData)){
            DB::table('attendance')->insert($attData);
            Session::flash("success","Attendance data inserted successfully");
        }
        else{
            Session::flash("error","No data found to merge.");
        }
        return redirect()->back();



    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'proxy_number'=>'required'
        ]);

        $now=Carbon::now()->toDateTimeString();

        $data=DB::table('tbtemp_employee_proxy')->insert([
            'employee_id'=>$request->employee_id,
            'employee_name'=>$request->employee_name,
            'proxy_number'=>$request->proxy_number,
            'joining_date'=>date('Y-m-d', strtotime($request->joining_date)),
            'remarks'=>$request->remarks,
            'createdBy'=>Auth::user()->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        
        if($data){
            Session::flash('message', 'Temporary employee information has been successfully added.');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id= base64_decode($id);
        DB::table('tbtemp_employee_proxy')->where('id', '=', $id)->delete();
         Session::flash('message', 'Employee(temporary) information has been successfully destroyed.');
            return redirect()->back();
    }
}
