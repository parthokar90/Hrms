<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ActivityLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history_view()
    {
        $users=DB::table('users')->get();
        return view('activity_logs.activity_logs_history_view',compact('users'));
    }


    public function history_data(Request $request)
    {   
       $startDate = date('Y-m-d', strtotime($request->startDate));
       $endDate = date('Y-m-d', strtotime($request->endDate));

        if(base64_decode($request->userId)=="all"){
            $activity_logs_history=DB::table('tbactivity_logs_history')
            ->leftJoin('users','users.id','=','tbactivity_logs_history.createdBy')
            ->select('tbactivity_logs_history.*','users.name')
            ->whereBetween('tbactivity_logs_history.created_at', [$startDate, $endDate])
            ->orderBy('id','DESC')
            ->paginate(250);
        }else{
             $activity_logs_history=DB::table('tbactivity_logs_history')
            ->leftJoin('users','users.id','=','tbactivity_logs_history.createdBy')
            ->select('tbactivity_logs_history.*','users.name')
            ->whereBetween('tbactivity_logs_history.created_at', [$startDate, $endDate])
            ->where('tbactivity_logs_history.createdBy', '=', base64_decode($request->userId))
            ->orderBy('id','DESC')
            ->paginate(250);
        }
        return view('activity_logs.activity_logs_history_data', compact('activity_logs_history'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
