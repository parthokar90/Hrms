<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class EmployeeEducationInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['middleware'=>'check-permission:admin|hr|hr-admin']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.employeeEducation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'empExamTitle'=>'required',
            'empInstitution'=>'required',
            'empResult'=>'required',
            'empScale'=>'required',
            'empPassYear'=>'required',

        ]);



        if($file=$request->file('empCertificate')){
            if($request->file('empCertificate')->getClientSize()>5000000)
            {
                Session::flash('fileSize','Could Not Upload. File Size Limit Exceeded.');
                return redirect("/employee/".$request->emp_id);

            }
            $name=time()."_".$request->emp_id."_"."E_CERT_".$file->getClientOriginalName();
            $file->move('Educational_Certificates',$name);


        }
        else{
            $name=null;
        }
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $store=DB::table('employee_education_infos')->insert([
            'emp_id'=>$request->emp_id,
            'empExamTitle'=>$request->empExamTitle,
            'empInstitution'=>$request->empInstitution,
            'empResult'=>$request->empResult,
            'empScale'=>$request->empScale,
            'empPassYear'=>$request->empPassYear,
            'empCertificate'=>$name,
            'created_by'=>$user->name,
            'modified_by'=>$user->name,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($store){
            Session::flash('message','Educational Information Inserted');
        }

        return redirect("/employee/".$request->emp_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'empExamTitle'=>'required',
            'empInstitution'=>'required',
            'empResult'=>'required',
            'empScale'=>'required',
            'empPassYear'=>'required',

        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $table=DB::table('employee_education_infos')->where(['id'=>$id]);
        if($request->file('empCertificate')){
            if($request->file('empCertificate')->getClientSize()>5000000){
                Session::flash('fileSize','Could Not Upload. File Size Limit Exceeded.');
                return redirect("/employee/".session('emp_id'));
            }
            if($table->first()->empCertificate) {
                $path = public_path() . "/Educational_Certificates/" . $table->first()->empCertificate;
                if (file_exists($path)) {
//                    return $path;
                    unlink($path);
                }
            }
            $name=time()."_".session('emp_id')."_".$request->file('empCertificate')->getClientOriginalName();
            $request->file('empCertificate')->move('Educational_Certificates',$name);
        }
        else{
            $name = $table->first()->empCertificate;
        }
        $update=$table->update([
            'empExamTitle'=>$request->empExamTitle,
            'empInstitution'=>$request->empInstitution,
            'empResult'=>$request->empResult,
            'empScale'=>$request->empScale,
            'empPassYear'=>$request->empPassYear,
            'empCertificate'=>$name,
            'modified_by'=>$user->name,
            'updated_at'=>$now,

        ]);
        if($update){
            Session::flash('edit','Education Info Successfully Updated');
            return redirect("/employee/".session('emp_id'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $dlt=DB::table('employee_education_infos')->where(['id'=>$id]);
        if($dlt->first()->empCertificate) {
            $name = public_path() . "/Educational_Certificates" . "/" . $dlt->first()->empCertificate;
            if (file_exists($name)) {
//            Storage::delete($name);
                unlink($name);
            }
        }

        $dlt=$dlt->delete();
        if($dlt)
        {
            Session::flash('delete', 'Educational Info Successfully Deleted');
            return redirect("/employee/".session('emp_id'));
        }
    }
}
