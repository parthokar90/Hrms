<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OvertimeHourCountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $overtime_hour_count=DB::table('tb_overtime_hour_count')->get();
        return view('settings.overtime-hour-count.index',compact('overtime_hour_count'));
    }


//    public function create()
//    {
//        //
//    }

//    public function store(Request $request)
//    {
//        //
//    }

//    public function show($id)
//    {
//        //
//    }

//    public function edit($id)
//    {
//        //
//    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'overtime_hours'=>'required',
            'overtime_minutes'=>'required',
        ]);
        DB::table('tb_overtime_hour_count')->where('id','=',$id)->update([
            'overtime_hours'=>$request->overtime_hours,
            'overtime_minutes'=>$request->overtime_minutes,
        ]);
        Session::flash('message','Overtime hour count settings updated.');
        return redirect(route('overtime-hour-count.index'));
    }

//    public function destroy($id)
//    {
//        //
//    }
}
