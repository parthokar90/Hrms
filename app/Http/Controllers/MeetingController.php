<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Redirect;
use Session;
use auth;
use Symfony\Component;

class MeetingController extends Controller
{
    
	public function __construct()
    {
        $this->middleware(['middleware'=>'check-permission:admin|hr']);
    }
    
    public function index(){
        $meetings=DB::table('meetings')->latest()->get();

    	return view('meeting.index',compact('meetings'));
    }

    public function create(){
    	return view('meeting.create');
    }
   
    public function show($id)
    {
        $meeting=DB::table('meetings')
        ->where('meetings.id','=',$id)
        ->first();

        return view('meeting.show',compact('meeting'));

    }
    
    public function store(Request $request){
        
        $this->validate($request,[
            'msub'=>'required',
            'date'=>'required',
            'stime'=>'required',
            'etime'=>'required',
            'venue'=>'required',
            'descrip'=>'required',
        ]);

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        Carbon::parse($request->date)->format('Y-m-d');
        $id=DB::table('meetings')->insert([
            'msub'=>$request->msub,
            'date'=>Carbon::parse($request->date)->format('Y-m-d'),
            'stime'=>$request->stime,
            'etime'=>$request->etime,
            'venue'=>$request->venue,
            'descrip'=>$request->descrip,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($id){
            Session::flash('createMeeting', 'A new meeting has been created!');
        }
        return redirect("/meeting");
    }
    public function update(Request $request, $id){
        $this->validate($request,[
            'msub'=>'required',
            'date'=>'required',
            'stime'=>'required',
            'etime'=>'required',
            'venue'=>'required',
            'descrip'=>'required',
        ]);

        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();

        $up=DB::table('meetings')->where(['id'=>$id])->update([
            'msub'=>$request->msub,
            'date'=>Carbon::parse($request->date)->format('Y-m-d'),
            'stime'=>$request->stime,
            'etime'=>$request->etime,
            'descrip'=>$request->descrip,
            'venue'=>$request->venue,
            'updated_at'=>$now,
        ]);

        if($up){
            Session::flash('meetingUpdate', "Meeting information successfully Updated");
        }
        return redirect("/meeting");
    }

    public function destroy($id){
        $data=DB::table('meetings')->WHERE('id',$id)->delete();
        if($data){
            Session::flash('meetingdeleted', 'Meeting Deleted Successful!');
            return redirect()->back();
        }
    }

}
