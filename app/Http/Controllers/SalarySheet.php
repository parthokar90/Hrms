<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Auth;
use redirect;
use Session;
use stdClass;
use PDF;
use Excel;
use mPDF;
use DateTime;
use DatePeriod;
use DateInterval;

class SalarySheet extends Controller
{
    //process salary sheet view page
    public function view_salary_sheet(){
        $employee=DB::table('employees')->select('id','empFirstName','employeeId')->get();
        return view('report.payroll.salarysheet.index',compact('employee'));
    }

    //Private process salary sheet view page
    public function view_salary_sheet_private(){
        return view('report.payroll.salarysheet.private');
    }

    //working day find function
    public static function dayCalculator($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $interval1=1+round(abs($date1-$date2)/86400);
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $dcount=0;
        foreach($festivalLeave as $fleave){
            if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);

            }
            else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);

            }
            else{
                continue;
            }
        }
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
        $key=0;
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {

            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);

            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key++;
                }
            }

        }
        $interval=(int)$interval1-((int)$dcount+$key);
        return $interval;
    }


//Weekend Dates My Code
    function weekend_dates($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
        $weekend_dates=array();
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $weekend_dates[]=$dates;
                }
            }
        }
        return $weekend_dates;
    }
//Weekend Dates End My Code



    //weekend find function
    function weekdayCalculator($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
        $key=0;

        $festivalLeave = DB::table('tb_festival_leave')
            ->where(DB::raw('year(fest_starts)'),date('Y', $date1))
            ->orWhere(DB::raw('year(fest_ends)'),date('Y', $date1))
            ->get();
//        return $festivalLeave;

        $weekend_dates=array();
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key++;
                    $weekend_dates[]=$dates;
                }
            }
        }
        $h=0;
        foreach ($festivalLeave as $f){
//            return $f->fest_starts."     ".$f->fest_ends;
            foreach ($weekend_dates as $wd){
                if($wd>=$f->fest_starts && $wd<=$f->fest_ends){
                    $h++;
                };
            }
        }
        return $key-$h;
    }


    //holiday find function
    function holiday($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $interval1=1+round(abs($date1-$date2)/86400);
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $dcount=0;
        foreach($festivalLeave as $fleave){
            if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
            }
            else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
            }
            else{
                continue;
            }
        }
        return $dcount;
    }

    //condition wise salary sheet
    public function employee_SalarySheet(Request $request){
        $overtime_hours_count=DB::table('tb_overtime_hour_count')->first();
        DB::table('total_employee_leave')->delete();
        DB::table('extra_ot_result')->delete();
        DB::table('tb_total_present')->delete();
        DB::table('employee_overtime')->delete();
        DB::table('temp_employee_overtime')->delete();
        DB::table('salary_ot')->delete();
        DB::table('emp_total_late')->delete();
        DB::table('emp_total_lates')->delete();
        DB::table('regular_weekend_presence')->delete();
        DB::table('total_weekend_present')->delete();
        $s=date('Y-m-01',strtotime($request->salary_sheet_month));
        $e=date('Y-m-t',strtotime($request->salary_sheet_month));
		$leaveattyear=date('Y',strtotime($request->salary_sheet_month));
        $leaveattmonth=date('m',strtotime($request->salary_sheet_month));
		//check leave between attendance then delete attendance 
		$delete=DB::SELECT("SELECT attendance.id as att_id FROM leave_of_employee LEFT JOIN attendance ON leave_of_employee.emp_id=attendance.emp_id WHERE attendance.date BETWEEN leave_of_employee.start_date AND leave_of_employee.end_date AND YEAR(leave_of_employee.month)='$leaveattyear' AND MONTH(leave_of_employee.month)='$leaveattmonth'"); 		
        $delete_id = [];
        foreach ($delete as $key => $value) {
            $delete_id[]=$value->att_id;
        } 
        $ok = DB::table('attendance')->whereIn('id', $delete_id)->delete();
		//check leave between attendance then delete attendance 
		
        //Start For In Active Employee
        DB::table('employees')->where('date_of_discontinuation','>=',$s)->update([
            'empAccStatus'=>1,
        ]);
        //End For In Active Employee
        $count=DB::table('tb_salary_history')->where('month',$request->salary_sheet_month)->count();
        if($count>0){
          DB::table('tb_salary_history')->where('month',$request->salary_sheet_month)->delete();
        }
        $set_date='';
        $mof_month=date('t',strtotime($request->salary_sheet_month))/2;
        $round_middle=(int)$mof_month;
        $regular_days_setup_check=DB::table('tbweekend_regular_day')
        ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
        ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
        ->count();
  // MY CODE
        $regular_fridays_date=DB::table('tbweekend_regular_day')
        ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
        ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
        ->get(['weekend_date']);
        $regular_weekend_dates=array();
        foreach ($regular_fridays_date as $rfd){
            $regular_weekend_dates[]=$rfd->weekend_date;
        }
        $regular_weekend_presence=DB::table('employees')
        ->where('employees.is_three_shift',0)
        ->join('attendance','employees.id', '=', 'attendance.emp_id')
        ->whereIn('attendance.date',$regular_weekend_dates)
        ->select('employees.id as emp_id',DB::raw("count(attendance.emp_id) as present"))
        ->groupBy('employees.id')
        ->get();
        foreach ($regular_weekend_presence as $presents){
            $insert[]=[
                'emp_id'=>$presents->emp_id,
                'regular_present'=>$presents->present,
            ];
        }
        if(!empty($insert)){
            DB::table('regular_weekend_presence')->insert($insert);
            unset($insert);
        }
        $weekend_present=DB::table('employees')
        ->where('employees.is_three_shift',0)
        ->join('attendance','employees.id','=','attendance.emp_id')
        ->whereIn('attendance.date',$this->weekend_dates($s,$e))
        ->select(DB::raw('count(employees.id) as weekend_present'),'employees.id as emp_id')
        ->groupBy('employees.id')
        ->get();
        foreach ($weekend_present as $wp){
            $insert[]=[
                'emp_id'=>$wp->emp_id,
                'weekend_present'=>$wp->weekend_present,
            ];
        }
        if(!empty($insert)){
            DB::table('total_weekend_present')->insert($insert);
            unset($insert);
        }
// My Code End
        $money_deduction=10;
        $month_check=date('Y-m-d',strtotime($request->salary_sheet_month));
        // $weeks=$this->weekdayCalculator($s,$e)-$regular_days_setup_check;
        // $holii=$this->holiday($s,$e);
        // $actual_working_day=date('t',strtotime($request->salary_sheet_month))-$weeks-$holii;
        $process_check=DB::table('tb_salary_process_check')
            ->where('month',$month_check)
            ->where('status','=',1)
            ->count();
        if($process_check>0){
            Session::flash('salaryprocesscheck','Process Salary Already Completed this month');
            return redirect()->back();
        }
        $date=date('m-Y');
        $current_time = Carbon::now()->toDateTimeString();
        $current_month_first=date('Y-m-01',strtotime($request->salary_sheet_month));
        $current_month_last=date('Y-m-t',strtotime($request->salary_sheet_month));
        $month=date('Y-m-d',strtotime($request->salary_sheet_month));
        $workingmonth=$request->salary_sheet_month;
        // $overtime_result = DB::SELECT(DB::raw("SELECT attendance.emp_id, employees.empFirstName,attendance.emp_id,attendance.out_time,attendance.date,MINUTE(attendance.out_time)-MINUTE(attendance_setup.exit_time) as overtime_minute,SUM(IF(HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time)>2,2,HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time))) as tot,SUM(CASE WHEN HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time)>1 THEN 0 WHEN MINUTE(attendance.out_time)-MINUTE(attendance_setup.exit_time)>54 THEN 1 WHEN MINUTE(attendance.out_time)-MINUTE(attendance_setup.exit_time)>24 THEN .5 END) AS t FROM attendance LEFT JOIN employees ON attendance.emp_id=employees.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE attendance.date BETWEEN '$current_month_first' AND '$current_month_last' AND employees.empOTStatus=1 AND HOUR(attendance.out_time)>HOUR(attendance_setup.exit_time) AND attendance.out_time != attendance.in_time AND DAYNAME(attendance.date)!='Friday' GROUP BY attendance.emp_id"));
        $regular_work_days =DB::table('tbweekend_regular_day')
        ->select(DB::raw('group_concat(weekend_date) as date'))
        ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
        ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
        ->groupBy('weekend_month')
        ->first();
        if(isset($regular_work_days)){
           $set_date=$regular_work_days->date;
        }
       $overtime_result=DB::SELECT("SELECT attendance.*,
       attendance.emp_id,employees.employeeId,employees.empFirstName,
       attendance.date as att_date 
       FROM attendance
       LEFT JOIN employees ON attendance.emp_id=employees.id 
       WHERE attendance.date BETWEEN '$current_month_first' AND '$current_month_last' AND employees.empOTStatus=1 AND employees.is_three_shift=0 AND DAYNAME(attendance.date)!='Friday' AND attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times GROUP BY attendance.emp_id, attendance.date 
       ");
       $insert_data = []; 
       foreach($overtime_result as $extraots){  
        $ot_data=[
                     'emp_id' =>$extraots->emp_id,
                     'out_time'=>$extraots->out_time,
                     'exit_time'=>$extraots->exit_times,
                     'att_date'=>date('Y-m-d',strtotime($extraots->att_date)),
                 ];
                 $insert_data[]=$ot_data;
       }
       $insert_data = collect($insert_data); // Make a collection to use the chunk method
//    // it will chunk the dataset in smaller collections containing 500 values each.
//    // Play with the value to get best result
        $chunks = $insert_data->chunk(3000);
        foreach ($chunks as $chunk)
        {
            \DB::table('salary_ot')->insert($chunk->toArray());
        }
   $data_ot=DB::SELECT("SELECT emp_id,out_time,exit_time,att_date,HOUR(out_time) as hour_out_time,HOUR(exit_time) as hour_exit_time,MINUTE(out_time) as minute_out_time,MINUTE(exit_time)as minute_exit_time from salary_ot WHERE out_time>exit_time");
        
        $data=0;
        $insert_data = [];
      function roundTime($time){
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));
    if($minute<25){
        $minute=0;
    }
    elseif ($minute>=25 &&$minute<55){
        $minute=.5;
    }
    else{
        $minute=0;
        $hour++;
    }
    $kind=number_format((float)($hour+$minute),1,'.','');
    return $kind;
}
       foreach($data_ot as $extraots){ 
	     $ot=roundTime(date('G:i',strtotime($extraots->out_time)-strtotime($extraots->exit_time)));
		   if($ot>=$overtime_hours_count->overtime_hours){
           $total_ot=$overtime_hours_count->overtime_hours;
         }else{
             $total_ot=roundTime(date('G:i',strtotime($extraots->out_time)-strtotime($extraots->exit_time)));
         }
       $ot_data = [
        'emp_id' =>$extraots->emp_id,
        'emp_hour'=>$total_ot,
        'emp_minute'=>0,
        // 'att_date'=>$extraots->att_date,
    ];
    $insert_data[] = $ot_data;
   }
    $insert_data = collect($insert_data); // Make a collection to use the chunk method
//    // it will chunk the dataset in smaller collections containing 500 values each.
//    // Play with the value to get best result
   $chunks = $insert_data->chunk(5000);
   foreach ($chunks as $chunk)
   {
      \DB::table('extra_ot_result')->insert($chunk->toArray());
   }
  //end big data insert
  $late=DB::table('employees')
  ->where('employees.is_three_shift',0)
  ->join('attendance','employees.id','=','attendance.emp_id')
  ->whereBetween('attendance.date',[$current_month_first,$current_month_last])
  ->where('attendance.in_time','>',DB::raw('attendance.max_entry'))
  ->select('employees.id as emp_id',DB::raw('count(employees.id) as late'))
  ->groupBy('employees.id')
  ->get();
  $latee=0;
  foreach ($late as $lates){
          $insert[]=[
              'emp_id'=>$lates->emp_id,
              'total_late'=>$lates->late,
              'month'=>$month,
          ];
  }
  if($latee%550==0){
      if(!empty($insert)){
          $insertData = DB::table('emp_total_late')->insert($insert);
          unset($insert);
      }
  }
  $latee++;
   $extraotsss=DB::SELECT("SELECT extra_ot_result.emp_id,SUM(emp_hour) as total_hour from extra_ot_result
   GROUP BY extra_ot_result.emp_id");
        $present = DB::SELECT("SELECT attendance.emp_id,empFirstName,count(date) as present,count(date)-(ifnull(total_weekend_present.weekend_present, 0)-ifnull(regular_weekend_presence.regular_present,0)) as actual_present,attendance.date,total_weekend_present.weekend_present,regular_weekend_presence.regular_present from attendance LEFT JOIN employees ON attendance.emp_id=employees.id LEFT JOIN regular_weekend_presence ON attendance.emp_id=regular_weekend_presence.emp_id LEFT JOIN total_weekend_present ON attendance.emp_id=total_weekend_present.emp_id WHERE attendance.in_time IS NOT NULL AND employees.is_three_shift=0 AND attendance.date BETWEEN '$current_month_first' AND '$current_month_last' GROUP BY attendance.emp_id");
        $att=DB::SELECT("SELECT employees.id as emp_id,tbattendance_bonus.bTitle,tbattendance_bonus.bAmount FROM employees LEFT JOIN tbattendance_bonus ON employees.empAttBonusId = tbattendance_bonus.id AND employees.is_three_shift=0 AND employees.empAttBonusId!=0");
        $ott=0;
        foreach ($extraotsss as $or){
                $insert[]=[
                    'emp_ids'=>$or->emp_id,
                    'overtime_hour'=>$or->total_hour,
                    'month'=>$month,
                ];
        }
        if($ott%550==0){
            if(!empty($insert)){
                $insertData = DB::table('employee_overtime')->insert($insert);
                unset($insert);
            }
        }
        $ott++;
       $total_emp_overtime_empls=DB::SELECT("SELECT employee_overtime.emp_ids as rmid,SUM(employee_overtime.overtime_hour) as total_overtime FROM employee_overtime GROUP BY employee_overtime.emp_ids");
       $temp_ott=0;
        foreach($total_emp_overtime_empls as $tmp_overtime){
            $insert[]=[
                'overtime_id' =>$tmp_overtime->rmid,
                'employee_overtime' =>$tmp_overtime->total_overtime,
                'month' =>$month
            ];
        }
        if($temp_ott%550==0){
            if(!empty($insert)){
                $insertData = DB::table('temp_employee_overtime')->insert($insert);
                unset($insert);
            }
        }
        $temp_ott++;
        $pre_insert=0;
        foreach ($present as $presents){
            $insert[]=[
                'emp_id'=>$presents->emp_id,
                'total_present'=>$presents->present,
                'total_absent'=>$this->dayCalculator($current_month_first,$current_month_last)-$presents->present,
                'month'=>$month,
            ];
        }
        if($pre_insert%550==0){
            if(!empty($insert)){
                $insertData = DB::table('tb_total_present')->insert($insert);
                unset($insert);
            }
        }
        $pre_insert++;
        $leave_tota=DB::SELECT("SELECT leave_of_employee.emp_id as employee_id,SUM(leave_of_employee.total) as total_leave 
                                FROM leave_of_employee
                                LEFT JOIN employees ON leave_of_employee.emp_id = employees.id
                                WHERE employees.is_three_shift=0
                                AND month='$month_check'
                                GROUP BY leave_of_employee.emp_id");                      
        $total_emp_leavesss=0;
        foreach ($leave_tota as $leaves){
            $insert[]=[
                'emp_idss'=>$leaves->employee_id,
                'total_leaves_taken'=>$leaves->total_leave,
                'month'=>$month,
            ];
        }
        if($total_emp_leavesss%550==0){
            if(!empty($insert)){
                $insertData = DB::table('total_employee_leave')->insert($insert);
                unset($insert);
            }
        }
        $total_emp_leavesss++;
        $checkattbonus=DB::table('attendance_bonus')->where('month',date('Y-m',strtotime($request->salary_sheet_month)))->count();
       if($checkattbonus>0){
            $att_bonus_incret=0;
            $deleteattbonus=DB::table('attendance_bonus')->where('month',date('Y-m',strtotime($request->salary_sheet_month)))->delete();
            foreach ($att as $atts){
                if($atts->bAmount==''){
                    $bonus=0;
                }
                else{
                    $bonus=$atts->bAmount;
                }
                $insert[]=[
                    'bonus_id'=>$atts->emp_id,
                    'bonus_amount'=>$bonus,
                    'emp_total_amount'=>$bonus,
                    'month'=>date('Y-m',strtotime($request->salary_sheet_month)),
                    'created_at' =>Carbon::now()->toDateTimeString(),
                    'updated_at' =>Carbon::now()->toDateTimeString(),
                ];
            }
            if($att_bonus_incret%550==0){
                if(!empty($insert)){
                    $insertData = DB::table('attendance_bonus')->insert($insert);
                    unset($insert);
                }
            }
            $att_bonus_incret++;
        }
        else{
            $att_bonus_incret_one=0;
            foreach ($att as $atts){
                if($atts->bAmount==''){
                    $bonus=0;
                }
                else{
                    $bonus=$atts->bAmount;
                }
                $insert[]=[
                    'bonus_id'=>$atts->emp_id,
                    'bonus_amount'=>$bonus,
                    'emp_total_amount'=>$bonus,
                    'month'=>date('Y-m',strtotime($request->salary_sheet_month)),
                    'created_at' =>Carbon::now()->toDateTimeString(),
                    'updated_at' =>Carbon::now()->toDateTimeString(),
                ];
            }
            if($att_bonus_incret_one%550==0){
                if(!empty($insert)){
                    $insertData = DB::table('attendance_bonus')->insert($insert);
                    unset($insert);
                }
            }
            $att_bonus_incret_one++;
        }
        $start = date('Y-m-01',strtotime($request->salary_sheet_month));
        $end = date('Y-m-t',strtotime($request->salary_sheet_month));
        $monthscheck=date('Y-m-d',strtotime($request->salary_sheet_month));
        $bonusmonth=date('Y-m',strtotime($request->salary_sheet_month));
        $festmonth=date('Y-m-01',strtotime($request->salary_sheet_month));
        $data=DB::table('payroll_salary')
            ->leftJoin('employees', function ($query) {
                $query->on('payroll_salary.emp_id', '=', 'employees.id');
                $query->where('employees.empAccStatus','=',1);
            })
            ->leftJoin('payroll_grade', function ($query) {
                $query->on('payroll_salary.grade_id', '=', 'payroll_grade.id');
            })
            ->leftJoin('units', function ($query) {
                $query->on('employees.unit_id', '=', 'units.id');
            })
            ->leftJoin('floors', function ($query) {
                $query->on('employees.floor_id', '=', 'floors.id');
            })
            ->leftJoin('tblines', function ($query) {
                $query->on('employees.line_id', '=', 'tblines.id');
            })
            ->leftJoin('tb_advance_salary', function ($query) use($start) {
                $query->on('payroll_salary.emp_id', '=', 'tb_advance_salary.emp_id');
                $query->where('tb_advance_salary.month','=',$start);
            })
            ->leftJoin('total_employee_leave', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'total_employee_leave.emp_idss');
                $query->where('total_employee_leave.month','=',$monthscheck);
            })
            ->leftJoin('leave_of_employee', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'leave_of_employee.emp_id');
                $query->where('leave_of_employee.month','=',$monthscheck);
            })
            ->leftJoin('tb_leave_application', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'tb_leave_application.employee_id');
                $query->whereYear('tb_leave_application.leave_ending_date', '=',date('Y',strtotime($monthscheck)));
                $query->whereMonth('tb_leave_application.leave_ending_date', '=',date('m',strtotime($monthscheck)));
                $query->where('tb_leave_application.status','=',1);
                $query->where('tb_leave_application.leave_type_id','=',1);
                })
            ->leftJoin('tb_leave_type', function ($query) use($start,$end) {
                $query->on('leave_of_employee.leave_type_id', '=', 'tb_leave_type.id');
            })
            ->leftJoin('tb_total_present', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'tb_total_present.emp_id');
                $query->where('tb_total_present.month','=',$monthscheck);
            })
            ->leftJoin('emp_total_lates', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'emp_total_lates.emp_id');
                $query->where('emp_total_lates.month','=',$monthscheck);
            })
            ->leftJoin('temp_employee_overtime', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'temp_employee_overtime.overtime_id');
                $query->where('temp_employee_overtime.month','=',$monthscheck);
            })
            ->leftJoin('emp_manual_overtime', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'emp_manual_overtime.emp_overtime_id');
                $query->where('emp_manual_overtime.month','=',$bonusmonth);
            })
            ->leftJoin('attendance_bonus', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'attendance_bonus.bonus_id');
                $query->where('attendance_bonus.month','=',$bonusmonth);
            })
            ->leftJoin('employees_bonus', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'employees_bonus.emp_id');
                $query->where('employees_bonus.date','=',$bonusmonth);
            })
            ->leftJoin('festival_bonus', function ($query) use($festmonth) {
                $query->on('payroll_salary.emp_id', '=', 'festival_bonus.emp_id');
                $query->where('festival_bonus.month','=',$festmonth);
            })
            ->leftJoin('tb_deduction', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'tb_deduction.emp_id');
                $query->where('tb_deduction.month','=',$bonusmonth);
            })
            ->leftJoin('tb_loan_deduction', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'tb_loan_deduction.emp_id');
                $query->where('tb_loan_deduction.month','=',$bonusmonth);
            })
            ->leftJoin('tb_production_bonus', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'tb_production_bonus.emp_id');
                $query->where('tb_production_bonus.month','=',$bonusmonth);
            })

            ->leftJoin('departments', function ($query) {
                $query->on('employees.empDepartmentId', '=', 'departments.id');
            })
            ->Join('designations', function ($query) {
                $query->on('designations.id', '=', 'employees.empDesignationId');
            })
            ->select('emp_total_lates.total_late','payroll_salary.*','employees.is_three_shift','employees.empDesignationId','tb_leave_application.leave_ending_date as maternity_ending','employees.payment_mode','employees.empDepartmentId','employees.empFirstName','employees.empLastName','employees.empJoiningDate','employees.empSection','employees.employeeId','payroll_grade.grade_name','designations.designation','employees.unit_id','units.name','employees.floor_id','floors.floor','employees.line_id','tblines.line_no','employees.empDepartmentId','departments.departmentName','employees.work_group','employees.payment_mode','total_employee_leave.total_leaves_taken','employees_bonus.emp_bonus as special_bonus','employees_bonus.emp_amount','attendance_bonus.bonus_amount as attendance_bonus','attendance_bonus.bonus_percent','festival_bonus.emp_amount as f_amount','tb_total_present.total_present','leave_of_employee.leave_type_id','leave_of_employee.total','tb_deduction.normal_deduction_amount','tb_loan_deduction.month_wise_deduction_amount','tb_production_bonus.production_amount','tb_production_bonus.production_percent','temp_employee_overtime.employee_overtime as temp_ot','emp_manual_overtime.employee_overtime as manual_ot','designations.gradeId','date_of_discontinuation','tb_advance_salary.advance_amount','leave_of_employee.start_date','leave_of_employee.end_date')
            ->selectraw("CONCAT(GROUP_CONCAT( DISTINCT(tb_leave_type.leave_type),leave_of_employee.total)) as leave_name_value")
            ->selectraw("GROUP_CONCAT(DISTINCT(leave_of_employee.leave_type_id)SEPARATOR ',') as leave_with_id")
            ->selectraw("GROUP_CONCAT(DISTINCT(leave_of_employee.total)SEPARATOR ',') as leave_with_total")
            ->selectraw("SUM(festival_bonus.emp_bonus) as f_bonus")
            ->where('employees.is_three_shift',0)
            ->groupBy('payroll_salary.emp_id')
            ->get();
            $month=date('Y-m-d',strtotime($request->salary_sheet_month));
            $dates=date('Y-m',strtotime($request->salary_sheet_month));
            $check=DB::table('tb_salary_history')->where('month',$month)->count();
            if($check>0){
                $delete=DB::table('tb_salary_history')->where('month',$month)->delete();
            }
            $current_month_first=date('Y-m-01',strtotime($workingmonth));
            $current_month_last=date('Y-m-t',strtotime($workingmonth));
            $working_day=$this->dayCalculator($current_month_first,$current_month_last)+$regular_days_setup_check;
            $total_sal_datassss=0;
            foreach($data as $testdata){
                $regular_day_check=DB::table('tbweekend_regular_day')
                ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
                ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
                ->get();
                $leave_of_employee=DB::table('leave_of_employee')
                ->where('emp_id',$testdata->emp_id)
                ->where('month','=',$month_check)
                ->get();
                $leave_weekend=0;
                $leave_holiday=0;
                $dept_section_holiday_leave=0;
                $regular_day_count=0;
                $leave_regular_weekend=0;
                $dept_sec_holi_present=0;
                foreach ($leave_of_employee as $loe){
                    $leave_weekend+=$this->weekdayCalculator($loe->start_date,$loe->end_date);
                    $leave_holiday+=$this->holiday($loe->start_date,$loe->end_date);
                    $leave_regular_weekend+=DB::table('tbweekend_regular_day')
                        ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
                        ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
                        ->whereBetween('weekend_date',[$loe->start_date,$loe->end_date])
                        ->count();
                    $dept_section_holiday_leave+=DB::table('emp_wise_holiday')
                    ->where('emp_id',$loe->emp_id)
                    ->whereBetween('holiday_date',[$loe->start_date,$loe->end_date])
                    ->count();
                }
                $dept_sec_holis=DB::table('emp_wise_holiday')
                ->where('emp_id',$testdata->emp_id)
                ->where('holiday_month',$s)
                ->get();
                foreach($dept_sec_holis as $tt){
                    $dept_sec_holi_present+=DB::table('attendance')
                    ->where('emp_id',$testdata->emp_id)
                    ->where('date',$tt->holiday_date)
                    ->count();
                }
                global $deduction;
                global  $ac_abs_day;
                global  $weekend;
                global  $holi;
                global  $total_payable_days;
                global $total_gross;
                global  $join_day;
                global $dis_continution_day;
                $att_bonus=0;
                $status=0;
                $total_gross_pre=0;
                $current_month_day=date('t',strtotime($workingmonth));
                $overtime_rate=$testdata->basic_salary/104;
                $actual_overtime_rate=$overtime_rate;
                $total_overtt_time=$testdata->temp_ot+$testdata->manual_ot;
                $overtime_amount=$actual_overtime_rate*$total_overtt_time;
             //default calculation
                $dept_section_total_holiday=DB::table('emp_wise_holiday')
                ->where('emp_id',$testdata->emp_id)
                ->where('holiday_month',$s)
                ->count()-$dept_section_holiday_leave;   
               $weekss=$this->weekdayCalculator($s,$e)-$regular_days_setup_check-$leave_weekend+$leave_regular_weekend;
               $holiii=$this->holiday($s,$e)-$leave_holiday+$dept_section_total_holiday;
               $actual_working_day=date('t',strtotime($request->salary_sheet_month))-$weekss-$holiii;
               $absentday=$actual_working_day-$testdata->total_leaves_taken-$testdata->total_present;
               $ac_abs_day=max($absentday,0);
               $weekend = $weekss;
               $holi = $holiii;
               $month_lst_day=date('t',strtotime($month));
               $total_payable_days=$month_lst_day-$ac_abs_day;
               $deduction=round($testdata->basic_salary/30*$ac_abs_day,2);
            //default calculation
            //check date of discontinution
            if(date('Y-m',strtotime($testdata->empJoiningDate))==$bonusmonth && date('Y-m',strtotime($testdata->date_of_discontinuation))==$bonusmonth){
                $dept_sec_holis=DB::table('emp_wise_holiday')
                ->where('emp_id',$testdata->emp_id)
                ->whereBetween('holiday_date',[$testdata->empJoiningDate, $testdata->date_of_discontinuation])
                ->get();
                foreach($dept_sec_holis as $tt){
                    $dept_sec_holi_present+=DB::table('attendance')
                    ->where('emp_id',$testdata->emp_id)
                    ->where('date',$tt->holiday_date)
                    ->count();
                }
                $dept_section_total_holiday=DB::table('emp_wise_holiday')
                ->where('emp_id',$testdata->emp_id)
                ->whereBetween('holiday_date',[$testdata->empJoiningDate, $testdata->date_of_discontinuation])
                ->count()-$dept_section_holiday_leave;
                $weekss = $this->weekdayCalculator($testdata->empJoiningDate,$testdata->date_of_discontinuation)-$regular_days_setup_check+$leave_regular_weekend-$leave_weekend;
                $holiii = $this->holiday($testdata->empJoiningDate,$testdata->date_of_discontinuation)-$leave_holiday+$dept_section_total_holiday;
                $p_date1 = date_create(date('Y-m-01', strtotime($request->salary_sheet_month)));
                $p_date2 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
                $previous_diff = date_diff($p_date1, $p_date2);
                $total_previous_difference = $previous_diff->format("%a");
                $p_date1 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
                $p_date2 = date_create(date('Y-m-t', strtotime($request->salary_sheet_month)));
                $previous_diff2 = date_diff($p_date1, $p_date2);
                $total_previous_difference2 = $previous_diff2->format("%a");
                $total_previous_difference+=$total_previous_difference2;
                $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
                $date1 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
                $date2 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
                $diff = date_diff($date1, $date2);
                $total_difference = $diff->format("%a") + 1;
                $actual_working = $total_difference - $weekss - $holiii;
                $working_days = $actual_working;
//                    return $working_days;
                $absentdays = $working_days - $testdata->total_leaves_taken - $testdata->total_present;
                $ac_abs_day = max($absentdays, 0);
                $weekend = $weekss;
                $holi = $holiii;
                $month_lst_day = date('t', strtotime($month));
                $total_payable_days = $month_lst_day - $ac_abs_day;
                $month_salary = date('01', strtotime($request->salary_sheet_month));
                $join_date = $testdata->empJoiningDate;
                $join_day = date('d', strtotime($testdata->empJoiningDate));
                if (date('d', strtotime($join_date)) >= $month_salary) {
                    $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
                }
                else{
                    $deduction=0;
                }
            }
             //check date of discontinution
                else {
                    //check if join date this month
                    if (date('Y-m', strtotime($testdata->empJoiningDate)) == $bonusmonth) {
                        $dept_sec_holis=DB::table('emp_wise_holiday')
                        ->where('emp_id',$testdata->emp_id)
                        ->whereBetween('holiday_date',[$testdata->empJoiningDate,$e])
                        ->get();
                        foreach($dept_sec_holis as $tt){
                            $dept_sec_holi_present+=DB::table('attendance')
                            ->where('emp_id',$testdata->emp_id)
                            ->where('date',$tt->holiday_date)
                            ->count();
                        }
                        $dept_section_total_holiday=DB::table('emp_wise_holiday')
                        ->where('emp_id',$testdata->emp_id)
                        ->whereBetween('holiday_date',[$testdata->empJoiningDate,$e])
                        ->count()-$dept_section_holiday_leave;
                        $tot_week_regular=DB::table('tbweekend_regular_day')
                        ->whereBetween('weekend_date', array($testdata->empJoiningDate, $e))
                        ->count();
                        $weekss = $this->weekdayCalculator($testdata->empJoiningDate,$e)-$tot_week_regular+$leave_regular_weekend-$leave_weekend;
                        $holiii = $this->holiday($testdata->empJoiningDate, $e)-$leave_holiday+$dept_section_total_holiday;
                        $p_date1 = date_create(date('Y-m-01', strtotime($request->salary_sheet_month)));
                        $p_date2 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
                        $previous_diff = date_diff($p_date1, $p_date2);
                        $total_previous_difference = $previous_diff->format("%a");
                        $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
                        //  dd($testdata->current_month_day);
                        $date1 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
                        $date2 = date_create(date('Y-m-t', strtotime($request->salary_sheet_month)));
                        $diff = date_diff($date1, $date2);
                        $total_difference = $diff->format("%a") + 1;
                        $actual_working = $total_difference - $weekss - $holiii;
                        $working_days = $actual_working;
                        $absentdays = $working_days - $testdata->total_leaves_taken - $testdata->total_present;
                        $ac_abs_day = max($absentdays, 0);
                        $weekend = $weekss;
                        $holi = $holiii;
                        $month_lst_day = date('t', strtotime($month));
                        $total_payable_days = $month_lst_day - $ac_abs_day;
                        $month_salary = date('01', strtotime($request->salary_sheet_month));
                        $join_date = $testdata->empJoiningDate;
                        $join_day = date('d', strtotime($testdata->empJoiningDate));
                        if (date('d', strtotime($join_date)) >= $month_salary) {
                            $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
                        }
                    }
                    //check if join date this month
                    //check if date of discontinuation this month
                    if (date('Y-m', strtotime($testdata->date_of_discontinuation)) == $bonusmonth) {
                        $dis_continution_day = date('d', strtotime($testdata->date_of_discontinuation));
                        if (date('d', strtotime($testdata->date_of_discontinuation)) >= 01 && date('d', strtotime($testdata->date_of_discontinuation)) <= 31) {
                            $dept_sec_holis=DB::table('emp_wise_holiday')
                        ->where('emp_id',$testdata->emp_id)
                        ->whereBetween('holiday_date',[$s,$testdata->date_of_discontinuation])
                        ->get();
                        foreach($dept_sec_holis as $tt){
                            $dept_sec_holi_present+=DB::table('attendance')
                            ->where('emp_id',$testdata->emp_id)
                            ->where('date',$tt->holiday_date)
                            ->count();
                        }
                            $dept_section_total_holiday=DB::table('emp_wise_holiday')
                            ->where('emp_id',$testdata->emp_id)
                            ->whereBetween('holiday_date',[$s,$testdata->date_of_discontinuation])
                            ->count()-$dept_section_holiday_leave;
                            $month_start = date('Y-m-01', strtotime($request->salary_sheet_month));
                            $weekss = $this->weekdayCalculator($month_start,$testdata->date_of_discontinuation)-$regular_days_setup_check+$leave_regular_weekend-$leave_weekend;
                            $holiii = $this->holiday($month_start, $testdata->date_of_discontinuation)-$leave_holiday+$dept_section_total_holiday;
                            $weekend = $weekss;
                            $holi = $holiii;
                            $p_date1 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
                            $p_date2 = date_create(date('Y-m-t', strtotime($request->salary_sheet_month)));
                            $previous_diff = date_diff($p_date1, $p_date2);
                            $total_previous_difference = $previous_diff->format("%a");
                            $actual_working_day = date('d', strtotime($testdata->date_of_discontinuation)) - $weekend - $holi;
                            $absentday = $actual_working_day - $testdata->total_leaves_taken - $testdata->total_present;
                            $ac_abs_day = max($absentday, 0);
                            $month_lst_day = date('t', strtotime($month));
                            $total_payable_days = $month_lst_day - $ac_abs_day;
                            $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
                            $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
                        }
                    }
                    //check if date of discontinuation this month
                }
                    //check if maternity leave this month
                      if(date('Y-m',strtotime($testdata->start_date))==$bonusmonth && $testdata->leave_with_id==1){
                        $dept_sec_holis=DB::table('emp_wise_holiday')
                        ->where('emp_id',$testdata->emp_id)
                        ->whereBetween('holiday_date',[$s,$e])
                        ->get();
                        foreach($dept_sec_holis as $tt){
                            $dept_sec_holi_present+=DB::table('attendance')
                            ->where('emp_id',$testdata->emp_id)
                            ->where('date',$tt->holiday_date)
                            ->count();
                        }
                        $dept_section_total_holiday=DB::table('emp_wise_holiday')
                            ->where('emp_id',$testdata->emp_id)
                            ->whereBetween('holiday_date',[$s,$e])
                            ->count()-$dept_section_holiday_leave;  
                        $month_start=date('Y-m-01',strtotime($request->salary_sheet_month));
                        $month_end=date('Y-m-t',strtotime($request->salary_sheet_month));
                        $weekss=$this->weekdayCalculator($s,$e)-$regular_days_setup_check-$leave_weekend;
                        $holiii=$this->holiday($s,$e)-$leave_holiday+$dept_section_total_holiday;
                        $weekend=$weekss;
                        $holi=$holiii;
                        $actual_working_day=date('t',strtotime($request->salary_sheet_month))- $weekss- $holiii;
                        $absentday=$actual_working_day-$testdata->total_leaves_taken-$testdata->total_present;
                        $ac_abs_day=max($absentday,0);
                        $total_gross_pre=($testdata->total_employee_salary/$current_month_day)*$testdata->total;
                        $deduction=round($testdata->basic_salary/30*$ac_abs_day,2);
                        }
                   //check if maternity leabve this month
                //check if date of maternity ending this month
                // if($testdata->maternity_ending){
                //     $m_day=date('d',strtotime($testdata->maternity_ending));
                //     if($m_day>1){
                //         $month_start=date('Y-m-01',strtotime($request->salary_sheet_month));
                //         $month_end=date('Y-m-t',strtotime($request->salary_sheet_month));
                //         $weekss=$this->weekdayCalculator($month_start,$month_end)-$regular_days_setup_check-$leave_weekend;
                //         $holiii=$this->holiday($month_start,$month_end)-$leave_holiday;
                //      $actual_working=date('t',strtotime($request->salary_sheet_month))-$weekss-$holiii;
                //      $working_days=$actual_working;
                //      $absentdays=$working_days-$testdata->total_leaves_taken-$testdata->total_present;
                //      $ac_abs_day=max($absentdays,0);
                //      $weekend = $weekss;
                //      $holi = $holiii;
                //      $month_lst_day=date('t',strtotime($month));
                //      $total_payable_days=$month_lst_day-$ac_abs_day;
                //      $total_gross_pre=($testdata->total_employee_salary/$current_month_day)*$testdata->total;
                //      $deduction=round($testdata->basic_salary/30*$ac_abs_day,2);
                //     }
                //  }
               //check if date of maternity ending this month

                 if($testdata->leave_with_id==1 && $testdata->total_present==''){
                    $status=1;
                  }
                    if($testdata->payment_mode=='Bank' || $testdata->payment_mode=='bKash') {
                        $stampFee = 0;
                    }
                    else {
                        $stampFee = 10;
                    }
                      //check att bonus condition wise
                    if($testdata->total=='' && $ac_abs_day==0 && $testdata->total_late<3 && $testdata->empJoiningDate<=$current_month_first){
                        $att_bonus=$testdata->attendance_bonus;
                        if($testdata->date_of_discontinuation!=null && $testdata->date_of_discontinuation<$current_month_last){
                            $att_bonus=0;
                        }
                    }else{
                        $att_bonus=0;
                       }
                     //check att bonus condition
                   $gross_pay=$testdata->total_employee_salary-$deduction-$testdata->normal_deduction_amount-$testdata->month_wise_deduction_amount-$stampFee;
                   $gross_pay=$gross_pay-$total_gross_pre;
                   $month=date('Y-m-d',strtotime($request->salary_sheet_month));
                   $net_wages=$gross_pay+$overtime_amount+$testdata->production_amount+$testdata->production_percent+$att_bonus+$testdata->advance_amount;
                   $actual_net_wages=$net_wages;
                   if($actual_net_wages<=1000){
                    $gross_pay=$gross_pay+10;
                    $actual_net_wages=$actual_net_wages+10;
                   }
                   $insert[]=[
                       'emp_id' => $testdata->emp_id,
                       'grade_id' => $testdata->gradeId,
                       'department_id' => $testdata->empDepartmentId,
                       'section_id' => $testdata->empSection,
                       'designation_id' => $testdata->empDesignationId,
                       'basic_salary' => $testdata->basic_salary,
                       'house_rant' => $testdata->house_rant,
                       'medical' => $testdata->medical,
                       'transport' => $testdata->transport,
                       'food' => $testdata->food,
                       'other' => $testdata->other,
                       'gross' => $testdata->total_employee_salary,
                       'gross_pay' => $gross_pay,
                       'present' => $testdata->total_present,
                       'absent' => $ac_abs_day,
                       'absent_deduction_amount' => $deduction,
                       'attendance_bonus' =>  $att_bonus,
                       'attendance_bonus_percent' => $testdata->bonus_percent,
                       'festival_bonus_amount' => $testdata->f_amount,
                       'festival_bonus_percent' => $testdata->f_bonus,
                       'increment_bonus_amount' => $testdata->emp_amount,
                       'increment_bonus_percent' => $testdata->special_bonus,
                       'normal_deduction' => $testdata->normal_deduction_amount,
                       'advanced_deduction' => $testdata->month_wise_deduction_amount,
                       'production_bonus' => $testdata->production_amount,
                       'production_percent' => $testdata->production_percent,
                       'working_day' =>$working_day,
                       'weekend' =>$weekend,
                       'holiday' =>$holi,
                       'total_payable_days' =>$total_payable_days,
                       'overtime' =>$total_overtt_time,
                       'overtime_rate' =>$actual_overtime_rate,
                       'overtime_amount' =>$overtime_amount,
                       'leave' =>$testdata->leave_with_id,
                       'total' =>$testdata->total_leaves_taken,
                       'total_late' =>$testdata->total_late,
                       'advance_salary'=>$testdata->advance_amount,
                       'net_amount' =>$actual_net_wages,
                       'month' => $month,
                       'dates' => $dates,
                       'status' =>  $status,
                       'created_at' =>Carbon::now()->toDateTimeString(),
                       'updated_at' =>Carbon::now()->toDateTimeString(),
                   ];
               }
            if($total_sal_datassss%550==0){
                if(!empty($insert)){
                    $insertData = DB::table('tb_salary_history')->insert($insert);
                    unset($insert);
                }
            }
            $total_sal_datassss++;
        $insert_check_data=DB::table('tb_salary_process_check')->insert([
            'month' =>$month,
            'status' =>0,
            'process_id' =>auth()->user()->id,
            'created_at' =>Carbon::now()->toDateTimeString(),
            'updated_at' =>Carbon::now()->toDateTimeString(),
        ]);
        \Illuminate\Support\Facades\DB::table('tbactivity_logs_history')->insert([
            'acType'=>'Manual Attendance',
            'details'=>"Salary has been processed for ".Carbon::parse($request->salary_sheet_month)->format('M Y'),
            'createdBy'=>Auth::user()->id,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        DB::table('total_employee_leave')->delete();
        DB::table('extra_ot_result')->delete();
        DB::table('tb_total_present')->delete();
        DB::table('employee_overtime')->delete();
        DB::table('temp_employee_overtime')->delete();
        DB::table('salary_ot')->delete();
        DB::table('emp_total_late')->delete();
        DB::table('emp_total_lates')->delete();
        DB::table('regular_weekend_presence')->delete();
        DB::table('total_weekend_present')->delete();
        //Start For In Active Employee
        DB::table('employees')->where('date_of_discontinuation','>=',$s)->update([
            'empAccStatus'=>0,
        ]);
//        return $as;
        //End For In Active Employee
        return redirect('report/payroll/employee/salary/sheet');
    }

    //salary sheet view page
    public function monthwisesalarysheetview(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $employee   =DB::table('employees')->where('is_three_shift',0)->select('id','empFirstName','empLastName','employeeId')->get();
        return view('report.payroll.salarysheet.salarysheetmonth',compact('department','section','employee'));
    }

    //bank payment sheet
    public function bank_payment_sheet(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $employee   =DB::table('employees')->where('is_three_shift',0)->select('id','empFirstName','empLastName','employeeId')->get();
        return view('report.payroll.bank.bank_payment_sheet',compact('department','section','employee'));

    }

      //bkash payment sheet
      public function bkash_payment_sheet(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $employee   =DB::table('employees')->where('is_three_shift',0)->select('id','empFirstName','empLastName','employeeId')->get();
        return view('report.payroll.bkash.bkash_payment_sheet',compact('department','section','employee'));

    }

    //bank payment sheet report
    public function bank_payment_sheet_show(Request $request){
        $month=$request->emp_sal_month;
        $dt= $request->emp_sal_month."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $data=DB::table('tb_salary_history')
        ->join('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftJoin('designations','employees.empDesignationId','=','designations.id')
            ->where('employees.payment_mode','=','Bank')
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.status','=',0)
            ->where('employees.is_three_shift','=',0)
            ->where('tb_salary_history.dates','=',$month);
            
        if($request->type=="dept_wise_data"){
            if($request->department_id!="0") {
                $data = $data->where('employees.empDepartmentId', '=', $request->department_id);
            }
        }
        elseif ($request->type=="section_wise_data"){
            if($request->section_id!="0") {
                $data=$data->where('employees.empSection','=',$request->section_id);
            }
        }
        elseif($request->type=="employee_wise_data"){
            $data=$data->where('employees.id','=',$request->emp_id);
        }
        if($request->working_group!='0' && $request->type!="employee_wise_data"){
            $data=$data->where('employees.work_group','=',$request->working_group);

        }
        $data=$data->select('employees.empFirstName','employees.empLastName','employees.employeeId',
            'employees.empSection','designations.designation','employees.bank_account','tb_salary_history.net_amount')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        if(count($data)==0){
            Session::flash('error',"No Data Found");
            return redirect()->back();
        }
        if(isset($request->salary_month_wise_excel)){
            if($request->salary_month_wise_excel=='salary_month_wise_excel'){
                Excel::create("bank_payment_sheet_excel", function($excel) use ($month, $data) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Bank Payment Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Bank Payment Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($month,$data) {
                        $sheet->loadView('report.payroll.bank.bank_payment_sheet_excel')
                            ->with('data',$data)
                            ->with('month',$month);
                    });
                })->download('xlsx');
                return view('report.payroll.bank.bank_payment_sheet_excel',compact('data','month'));

            }
        }
//        return $data;
        return view('report.payroll.bank.bank_payment_sheet_show',compact('data','month'));
    }


      //bkash payment sheet report
      public function bkash_payment_sheet_show(Request $request){
                $month=$request->emp_sal_month;
                $dt= $request->emp_sal_month."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $data=DB::table('tb_salary_history')
                ->join('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftJoin('designations','employees.empDesignationId','=','designations.id')
                    ->where('employees.payment_mode','=','bKash')
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('tb_salary_history.status','=',0)
                    ->where('employees.is_three_shift','=',0)
                    ->where('tb_salary_history.dates','=',$month);
                    
                if($request->type=="dept_wise_data"){
                    if($request->department_id!="0") {
                        $data = $data->where('employees.empDepartmentId', '=', $request->department_id);
                    }
                }
                elseif ($request->type=="section_wise_data"){
                    if($request->section_id!="0") {
                        $data=$data->where('employees.empSection','=',$request->section_id);
                    }
                }
                elseif($request->type=="employee_wise_data"){
                    $data=$data->where('employees.id','=',$request->emp_id);
                }
                if($request->working_group!='0' && $request->type!="employee_wise_data"){
                    $data=$data->where('employees.work_group','=',$request->working_group);
        
                }
                $data=$data->select('employees.empFirstName','employees.empLastName','employees.employeeId',
                    'employees.empSection','designations.designation','employees.bank_account','tb_salary_history.net_amount')
                    ->orderBy('employees.employeeId','ASC')
                    ->get();
                if(count($data)==0){
                    Session::flash('error',"No Data Found");
                    return redirect()->back();
                }
                if(isset($request->salary_month_wise_excel)){
                    if($request->salary_month_wise_excel=='salary_month_wise_excel'){
                        Excel::create("bkash_payment_sheet_excel", function($excel) use ($month, $data) {
                            // Set the title
                            $name=Auth::user()->name;
                            $excel->setTitle("Bkash Payment Sheet");
                            // Chain the setters
                            $excel->setCreator($name);
                            $excel->setDescription('Bkash Payment Sheet');
                            $excel->sheet('Sheet 1', function ($sheet) use ($month,$data) {
                                $sheet->loadView('report.payroll.bkash.bkash_payment_sheet_excel')
                                    ->with('data',$data)
                                    ->with('month',$month);
                            });
                        })->download('xlsx');
                        return view('report.payroll.bkash.bkash_payment_sheet_excel',compact('data','month'));
        
                    }
                }
                return view('report.payroll.bkash.bkash_payment_sheet_show',compact('data','month'));
        
            }


    //private salary sheet view page
    public function monthwisesalarysheetviewPrivate(){
        return view('report.payroll.salarysheet.salarysheetmonthprivate');
    }

    //salary process check month view
    public function CheckProcessMonth(){
        $data=DB::SELECT("SELECT tb_salary_process_check.id,tb_salary_process_check.start_date,tb_salary_process_check.end_date,tb_salary_process_check.created_at,status,month,COUNT(status) as total,users.name 
        FROM tb_salary_process_check
        LEFT JOIN users ON tb_salary_process_check.process_id=users.id
        GROUP BY month
        ORDER BY tb_salary_process_check.id DESC");
        return view('payroll.process_month',compact('data'));
    }

    //salary process month complete delete
    public function CheckProcessMonthDelete($month){
        $process_delete=DB::table('tb_salary_process_check')->where('month',$month)->delete();
        Session::flash('procesdeletemonth', 'Delete successful!');
        return redirect()->back();
    }

    //salary process status update
    public function CheckProcessMonthUpdate($month){
        $data=DB::table('tb_salary_process_check')->where('month',$month)->get();
        foreach($data as $check){

            if($check->status==1){
                $update=DB::table('tb_salary_process_check')->where('month',$month)->update([
                    'month' =>$month,
                    'status' =>0,
                    'process_id' =>auth()->user()->id,
                    'created_at' =>Carbon::now()->toDateTimeString(),
                    'updated_at' =>Carbon::now()->toDateTimeString(),
                ]);
            }

            if($check->status==0){
                $update=DB::table('tb_salary_process_check')->where('month',$month)->update([
                    'month' =>$month,
                    'status' =>1,
                    'process_id' =>auth()->user()->id,
                    'created_at' =>Carbon::now()->toDateTimeString(),
                    'updated_at' =>Carbon::now()->toDateTimeString(),
                ]);
            }

        }
        Session::flash('procesupdate', 'Update successful!');
        return redirect()->back();
    }

    //generate employee month wise salary sheet
    public function Monthwiseemployeesalarysheet(Request $request){
        $dt= $request->emp_sal_month."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();

        if($request->type=='dept_wise_data'){
                if($request->salary_month_wise_excel_department=='salary_month_wise_excel_department'){
                    $count_data=DB::table('tb_salary_history')
                    ->select('tb_salary_history.month')
                    ->where('dates',$request->emp_sal_month)
                    ->count();
                    if($count_data>0){
                    $payment_date=$request->salary_payment_date;
                    $months=$request->emp_sal_month;
                    $ye=date('Y',strtotime($request->emp_sal_month));
                    $mo=date('m',strtotime($request->emp_sal_month));
                    $monthname=DB::table('tb_salary_history')
                        ->select('tb_salary_history.month')
                        ->where('dates',$months)
                        ->first();
                    $department=DB::table('tb_salary_history')
                        ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                        ->leftjoin('tblines','employees.line_id','=','tblines.id')
                        ->select('departments.departmentName','employees.empSection','tb_salary_history.department_id','tblines.line_no','section_id')
                        ->where('tb_salary_history.department_id',$request->department_id)
                        ->groupBy('tb_salary_history.department_id')
                        ->orderBy('employees.employeeId', 'ASC')
                        ->get();
                        $excelName=time()."_Salary_Sheet";
                        Excel::create("$excelName", function($excel) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                            // Set the title
                            $name=Auth::user()->name;
                            $excel->setTitle("Employee Salary Sheet");
                            // Chain the setters
                            $excel->setCreator($name);
                            $excel->setDescription('Employee Salary Summary');
                            $excel->sheet('Sheet 1', function ($sheet) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                                $sheet->loadView('report.payroll.excel.salary_sheet_dept_excel')
                                    ->with('monthname',$monthname)
                                    ->with('dt',$dt)
                                    ->with('department',$department)
                                    ->with('months',$months)
                                    ->with('ye',$ye)
                                    ->with('mo',$mo)
                                    ->with('payment_date',$payment_date);
                            });
                        })->download('xlsx');
                }else{
                    return redirect()->back()->with('msg', 'No Data Found');
                }
              }
                $count_data=DB::table('tb_salary_history')
                ->select('tb_salary_history.month')
                ->where('dates',$request->emp_sal_month)
                ->count();
                if($count_data>0){
                $payment_date=$request->salary_payment_date;
                $months=$request->emp_sal_month;
                $ye=date('Y',strtotime($request->emp_sal_month));
                $mo=date('m',strtotime($request->emp_sal_month));
                $monthname=DB::table('tb_salary_history')
                    ->select('tb_salary_history.month')
                    ->where('dates',$months)
                    ->first();
                $department=DB::table('tb_salary_history')
                    ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                    ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                    ->leftjoin('tblines','employees.line_id','=','tblines.id')
                    ->select('departments.departmentName','employees.empSection','tb_salary_history.department_id','tblines.line_no','section_id')
                    ->where('tb_salary_history.department_id',$request->department_id)
                    ->groupBy('tb_salary_history.department_id')
                    ->orderBy('employees.employeeId', 'ASC')
                    ->get();
                return view('report.payroll.salary_sheet_show',compact('monthname','dt','department','months','ye','mo','payment_date'));
            }else{
                return redirect()->back()->with('msg', 'No Data Found');
            }
        }

        if($request->type=='section_wise_data'){
            if($request->salary_month_wise_excel_section=='salary_month_wise_excel_section'){
                $count_data=DB::table('tb_salary_history')
                ->select('tb_salary_history.month')
                ->where('dates',$request->emp_sal_month)
                ->count();
                if($count_data>0){
                    $payment_date=$request->salary_payment_date;
                    $months=$request->emp_sal_month;
                    $ye=date('Y',strtotime($request->emp_sal_month));
                    $mo=date('m',strtotime($request->emp_sal_month));
                    $monthname=DB::table('tb_salary_history')
                        ->select('tb_salary_history.month')
                        ->where('dates',$months)
                        ->first();
                    $department=DB::table('tb_salary_history')
                        ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                        ->leftjoin('tblines','employees.line_id','=','tblines.id')
                        ->select('departments.departmentName','employees.empSection','tb_salary_history.department_id','tblines.line_no','section_id')
                        ->where('tb_salary_history.section_id',$request->section_id)
                        ->groupBy('tb_salary_history.section_id')
                        ->get();

                        $excelName=time()."_Salary_Sheet";
                        Excel::create("$excelName", function($excel) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                            // Set the title
                            $name=Auth::user()->name;
                            $excel->setTitle("Employee Salary Sheet");
                            // Chain the setters
                            $excel->setCreator($name);
                            $excel->setDescription('Employee Salary Sheet');
                            $excel->sheet('Sheet 1', function ($sheet) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                                $sheet->loadView('report.payroll.excel.salary_sheet_section_excel')
                                    ->with('monthname',$monthname)
                                    ->with('dt',$dt)
                                    ->with('department',$department)
                                    ->with('months',$months)
                                    ->with('ye',$ye)
                                    ->with('mo',$mo)
                                    ->with('payment_date',$payment_date);
                            });
                        })->download('xlsx');
                }else{
                    return redirect()->back()->with('msg', 'No Data Found');
                }

             }
                $count_data=DB::table('tb_salary_history')
                    ->select('tb_salary_history.month')
                    ->where('dates',$request->emp_sal_month)
                    ->count();
                if($count_data>0){
                $payment_date=$request->salary_payment_date;
                $months=$request->emp_sal_month;
                $ye=date('Y',strtotime($request->emp_sal_month));
                $mo=date('m',strtotime($request->emp_sal_month));
                $monthname=DB::table('tb_salary_history')
                    ->select('tb_salary_history.month')
                    ->where('dates',$months)
                    ->first();
                $department=DB::table('tb_salary_history')
                    ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                    ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                    ->leftjoin('tblines','employees.line_id','=','tblines.id')
                    ->select('departments.departmentName','employees.empSection','tb_salary_history.department_id','tblines.line_no','section_id')
                    ->where('tb_salary_history.section_id',$request->section_id)
                    ->groupBy('tb_salary_history.section_id')
                    ->get();
                return view('report.payroll.section_wise_salary_sheet',compact('monthname','dt','department','months','ye','mo','payment_date'));
            }else{
                return redirect()->back()->with('msg', 'No Data Found');
            }
        }

       if($request->type=='employee_wise_data'){
           if($request->emp_id==''){
            return redirect()->back()->with('msg', 'Select Employee');
           }
            $em_id=$request->emp_id;
            $id=implode(",",$em_id);
            if($request->salary_month_wise_excel_employee=='salary_month_wise_excel_employee'){
                
            $count_data=DB::table('tb_salary_history')
                ->select('tb_salary_history.month')
                ->where('dates',$request->emp_sal_month)
                ->count();
            if($count_data>0){
            $payment_date=$request->salary_payment_date;
            $months=$request->emp_sal_month;
            $ye=date('Y',strtotime($request->emp_sal_month));
            $mo=date('m',strtotime($request->emp_sal_month));
            $monthname=DB::table('tb_salary_history')
                ->select('tb_salary_history.month')
                ->where('dates',$months)
                ->first();
            $department=DB::SELECT("SELECT departments.departmentName,employees.empSection,tb_salary_history.department_id,tblines.line_no,section_id,emp_id
            FROM tb_salary_history 
            LEFT JOIN departments ON tb_salary_history.department_id=departments.id
            LEFT JOIN employees ON tb_salary_history.emp_id=employees.id
            LEFT JOIN tblines ON employees.line_id=tblines.id 
            WHERE tb_salary_history.emp_id IN($id)
            GROUP BY tb_salary_history.emp_id");
                $excelName=time()."_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Employee Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Employee Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($monthname,$department,$dt,$months,$ye,$mo,$payment_date) {
                        $sheet->loadView('report.payroll.excel.salary_sheet_employee_excel')
                            ->with('monthname',$monthname)
                            ->with('dt',$dt)
                            ->with('department',$department)
                            ->with('months',$months)
                            ->with('ye',$ye)
                            ->with('mo',$mo)
                            ->with('payment_date',$payment_date);
                    });
                })->download('xlsx');
        }else{
            return redirect()->back()->with('msg', 'No Data Found');
        }

        }
            $count_data=DB::table('tb_salary_history')
                ->select('tb_salary_history.month')
                ->where('dates',$request->emp_sal_month)
                ->count();
            if($count_data>0){
            $payment_date=$request->salary_payment_date;
            $months=$request->emp_sal_month;
            $ye=date('Y',strtotime($request->emp_sal_month));
            $mo=date('m',strtotime($request->emp_sal_month));
            $monthname=DB::table('tb_salary_history')
                ->select('tb_salary_history.month')
                ->where('dates',$months)
                ->first();
            $department=
         DB::SELECT("SELECT departments.departmentName,employees.empSection,tb_salary_history.department_id,tblines.line_no,section_id,emp_id
        FROM tb_salary_history 
        LEFT JOIN departments ON tb_salary_history.department_id=departments.id
        LEFT JOIN employees ON tb_salary_history.emp_id=employees.id
        LEFT JOIN tblines ON employees.line_id=tblines.id 
        WHERE tb_salary_history.emp_id IN($id)
        GROUP BY tb_salary_history.emp_id");
            return view('report.payroll.emp_wise_salary_sheet',compact('monthname','department','dt','months','ye','mo','payment_date'));
        }else{
            return redirect()->back()->with('msg', 'No Data Found');
        }
     }



     if($request->type=='month_wise_data'){
        if($request->salary_month_wise_excel_month=='salary_month_wise_excel_month'){
        $count_data=DB::table('tb_salary_history')
            ->select('tb_salary_history.month')
            ->where('dates',$request->emp_sal_month)
            ->count();
        if($count_data>0){
        $payment_date=$request->salary_payment_date;
        $months=$request->emp_sal_month;
        $ye=date('Y',strtotime($request->emp_sal_month));
        $mo=date('m',strtotime($request->emp_sal_month));
        $monthname=DB::table('tb_salary_history')
            ->select('tb_salary_history.month')
            ->where('dates',$months)
            ->first();
        $department=DB::table('tb_salary_history')
            ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','employees.empSection','tb_salary_history.department_id','tblines.line_no','section_id','emp_id')
            ->groupBy('tb_salary_history.department_id')
            ->orderBy('departments.id','ASC')
            ->get();
            $excelName=time()."_Salary_Sheet";
            Excel::create("$excelName", function($excel) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Employee Salary Sheet");
                // Chain the setters
                $excel->setCreator($name);
                $excel->setDescription('Employee Salary S');
                $excel->sheet('Sheet 1', function ($sheet) use ($monthname,$department,$dt,$months,$ye,$mo,$payment_date) {
                    $sheet->loadView('report.payroll.excel.salary_sheet_month_excel')
                        ->with('monthname',$monthname)
                        ->with('dt',$dt)
                        ->with('department',$department)
                        ->with('months',$months)
                        ->with('ye',$ye)
                        ->with('mo',$mo)
                        ->with('payment_date',$payment_date);
                });
            })->download('xlsx');
        }else{
            return redirect()->back()->with('msg', 'No Data Found');
        }

        }
        $count_data=DB::table('tb_salary_history')
            ->select('tb_salary_history.month')
            ->where('dates',$request->emp_sal_month)
            ->count();
        if($count_data>0){
        $payment_date=$request->salary_payment_date;
        $months=$request->emp_sal_month;
        $ye=date('Y',strtotime($request->emp_sal_month));
        $mo=date('m',strtotime($request->emp_sal_month));
        $monthname=DB::table('tb_salary_history')
            ->select('tb_salary_history.month')
            ->where('dates',$months)
            ->first();
        $department=DB::table('tb_salary_history')
            ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','employees.empSection','tb_salary_history.department_id','tblines.line_no','section_id','emp_id')
            ->groupBy('tb_salary_history.department_id')
            ->orderBy('departments.id','ASC')
            ->get();
        return view('report.payroll.month_wise_salary_sheet',compact('monthname','department','dt','months','ye','mo','payment_date'));
    }else{
        return redirect()->back()->with('msg', 'No Data Found');
    }
 }

}

    //employee salary summery
    public function salarySummeryReport(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $work_group =DB::table('employees')->select('work_group')->whereNotNull('work_group')->groupBy('work_group')->get();
        return view('report.payroll.salary_summery',compact('department','section','work_group'));
    }

    //salary summery report show year and month wise
      public function salarySummeryReportShow(Request $request){
       if($request->group_id=='all_group'){
        $dt= $request->salary_summery_monthsss."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $month=$request->salary_summery_monthsss;
        $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
        $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
        $all_group='all_group';
        $data=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('employees.work_group','departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
            ->where(function ($query) use ($dt){
                $query->where('date_of_discontinuation','=',null);
                $query->orWhere('date_of_discontinuation','>',$dt);
            })
        ->where('employees.empJoiningDate','<=',$dt)
        ->where('tb_salary_history.status','=',0)
        ->groupBy('employees.work_group')
        ->get();
        $lefty=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',1)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();

        $resigned=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',2)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        if($request->download_excel=='download_excel'){
            $excelName=time()."_Salary_Summary";
            Excel::create("$excelName", function($excel) use ($data,$month,$all_group,$lefty,$resigned) {
                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Salary Summary");
                // Chain the setters
                $excel->setCreator($name);
                $excel->setDescription('Employee Salary Summary');
                $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$all_group,$lefty,$resigned) {
                    $sheet->loadView('report.payroll.salary_summary_xl_data')
                        ->with('data',$data)
                        ->with('month',$month)
                        ->with('all_group',$all_group)
                        ->with('lefty',$lefty)
                        ->with('resigned',$resigned);
                });
            })->download('xlsx');
       }else{
        return view('report.payroll.salary_summery_monthly_data',compact('data','month','all_group','lefty','resigned'));
       }
    }

       if($request->group_id){
        $dt= $request->salary_summery_monthsss."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $month=$request->salary_summery_monthsss;
        $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
        $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
        $group=$request->group_id;
        $data=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('employees.work_group','departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
            ->where(function ($query) use ($dt){
                $query->where('date_of_discontinuation','=',null);
                $query->orWhere('date_of_discontinuation','>',$dt);
            })
        ->where('employees.empJoiningDate','<=',$dt)
        ->where('tb_salary_history.status','=',0)
        ->where('employees.work_group',$request->group_id)
        ->get();
        $lefty=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',1)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();

        $resigned=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',2)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        if($request->download_excel=='download_excel'){
            $excelName=time()."_Salary_Summary";
            Excel::create("$excelName", function($excel) use ($data,$month,$group,$lefty,$resigned) {
                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Salary Summary");
                // Chain the setters
                $excel->setCreator($name);
                $excel->setDescription('Employee Salary Summary');
                $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$group,$lefty,$resigned) {
                    $sheet->loadView('report.payroll.salary_summary_xl_data')
                        ->with('data',$data)
                        ->with('month',$month)
                        ->with('group',$group)
                        ->with('lefty',$lefty)
                        ->with('resigned',$resigned);
                });
            })->download('xlsx');
        }else{
            return view('report.payroll.salary_summery_monthly_data',compact('data','month','group','lefty','resigned'));
        }
       }

      if($request->department_id=='all_department'){
        if($request->download_excel=='download_excel'){
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $all_dept='all_dept';
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
                ->where(function ($query) use ($dt){
                    $query->where('date_of_discontinuation','=',null);
                    $query->orWhere('date_of_discontinuation','>',$dt);
                })
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.status','=',0)
            ->groupBy('tb_salary_history.department_id')
            ->get();
            $lefty=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',1)
            ->where('employees.empJoiningDate','<=',$dt)
            // ->groupBy('tb_salary_history.department_id')
            ->get();

            $resigned=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',2)
            ->where('employees.empJoiningDate','<=',$dt)
            // ->groupBy('tb_salary_history.department_id')
            ->get();
            $excelName=time()."_Salary_Summary";
            Excel::create("$excelName", function($excel) use ($data,$month,$all_dept,$lefty,$resigned) {
                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Salary Summary");
                // Chain the setters
                $excel->setCreator($name);
                $excel->setDescription('Employee Salary Summary');
                $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$all_dept,$lefty,$resigned) {
                    $sheet->loadView('report.payroll.salary_summary_xl_data')
                        ->with('data',$data)
                        ->with('month',$month)
                        ->with('all_dept',$all_dept)
                        ->with('lefty',$lefty)
                        ->with('resigned',$resigned);
                });
            })->download('xlsx');
        }

        $dt= $request->salary_summery_monthsss."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $month=$request->salary_summery_monthsss;
        $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
        $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
        $all_dept='all_dept';
        $data=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
            ->where(function ($query) use ($dt){
                $query->where('date_of_discontinuation','=',null);
                $query->orWhere('date_of_discontinuation','>',$dt);
            })
        ->where('employees.empJoiningDate','<=',$dt)
        ->where('tb_salary_history.status','=',0)
        ->groupBy('tb_salary_history.department_id')
        ->get();
        $lefty=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',1)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();

        $resigned=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',2)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        return view('report.payroll.salary_summery_monthly_data',compact('data','month','all_dept','lefty','resigned'));
      }
      if($request->type=='department_wise_data'){
        if($request->download_excel=='download_excel'){
        $dt= $request->salary_summery_monthsss."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
        $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
        $month=$request->salary_summery_monthsss;
        $dept_id=$request->department_id;
        $data=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
            ->where(function ($query) use ($dt){
                $query->where('date_of_discontinuation','=',null);
                $query->orWhere('date_of_discontinuation','>',$dt);
            })
        ->where('employees.empJoiningDate','<=',$dt)
        ->where('tb_salary_history.department_id','=',$dept_id)
        ->where('tb_salary_history.status','=',0)
        ->get();
        $lefty=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',1)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        $resigned=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',2)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        $excelName=time()."_Salary_Summary";
        Excel::create("$excelName", function($excel) use ($data,$month,$dept_id,$lefty,$resigned) {
            // Set the title
            $name=Auth::user()->name;
            $excel->setTitle("Salary Summary");
            // Chain the setters
            $excel->setCreator($name);
            $excel->setDescription('Employee Salary Summary');
            $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$dept_id,$lefty,$resigned) {
                $sheet->loadView('report.payroll.salary_summary_xl_data')
                    ->with('data',$data)
                    ->with('month',$month)
                    ->with('dept_id',$dept_id)
                    ->with('lefty',$lefty)
                    ->with('resigned',$resigned);
            });
        })->download('xlsx');

        }

        $dt= $request->salary_summery_monthsss."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
        $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
        $month=$request->salary_summery_monthsss;
        $dept_id=$request->department_id;
        $data=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
            ->where(function ($query) use ($dt){
                $query->where('date_of_discontinuation','=',null);
                $query->orWhere('date_of_discontinuation','>',$dt);
            })
        ->where('employees.empJoiningDate','<=',$dt)
        ->where('tb_salary_history.department_id','=',$dept_id)
        ->where('tb_salary_history.status','=',0)
        ->get();
        $lefty=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',1)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        $resigned=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',2)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        return view('report.payroll.salary_summery_monthly_data',compact('data','month','dept_id','lefty','resigned'));
      }

      if($request->section_id=='all_section'){
        if($request->download_excel=='download_excel'){

        $dt= $request->salary_summery_monthsss."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
        $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
        $month=$request->salary_summery_monthsss;
        $all_section='all_section';
        $data=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
            ->where(function ($query) use ($dt){
                $query->where('date_of_discontinuation','=',null);
                $query->orWhere('date_of_discontinuation','>',$dt);
            })
        ->where('employees.empJoiningDate','<=',$dt)
        ->where('tb_salary_history.status','=',0)
        ->groupBy('tb_salary_history.section_id')
        ->get();
        $lefty=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',1)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        $resigned=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',2)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();

        $excelName=time()."_Salary_Summary";
        Excel::create("$excelName", function($excel) use ($data,$month,$all_section,$lefty,$resigned) {
            // Set the title
            $name=Auth::user()->name;
            $excel->setTitle("Salary Summary");
            // Chain the setters
            $excel->setCreator($name);
            $excel->setDescription('Employee Salary Summary');
            $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$all_section,$lefty,$resigned) {
                $sheet->loadView('report.payroll.salary_summary_xl_data')
                    ->with('data',$data)
                    ->with('month',$month)
                    ->with('all_section',$all_section)
                    ->with('lefty',$lefty)
                    ->with('resigned',$resigned);
            });
        })->download('xlsx');

        }

        $dt= $request->salary_summery_monthsss."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
        $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
        $month=$request->salary_summery_monthsss;
        $all_section='all_section';
        $data=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->where(function ($query) use ($dt){
            $query->where('date_of_discontinuation','=',null);
            $query->orWhere('date_of_discontinuation','>',$dt);
        })
        ->where('employees.empJoiningDate','<=',$dt)
        ->where('tb_salary_history.status','=',0)
        ->groupBy('tb_salary_history.section_id')
        ->get();
        $lefty=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',1)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        $resigned=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',2)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        return view('report.payroll.salary_summery_monthly_data',compact('data','month','all_section','lefty','resigned'));
      }

      if($request->type=='section_wise_data'){
        if($request->download_excel=='download_excel'){
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $month=$request->salary_summery_monthsss;
            $section_id=$request->section_id;
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->where(function ($query) use ($dt){
                $query->where('date_of_discontinuation','=',null);
                $query->orWhere('date_of_discontinuation','>',$dt);
            })
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.section_id','=',$section_id)
            ->where('tb_salary_history.status','=',0)
            ->get();

            $lefty=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',1)
            ->where('employees.empJoiningDate','<=',$dt)
            // ->groupBy('tb_salary_history.department_id')
            ->get();
            $resigned=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',2)
            ->where('employees.empJoiningDate','<=',$dt)
            // ->groupBy('tb_salary_history.department_id')
            ->get();

            $excelName=time()."_Salary_Summary";
            Excel::create("$excelName", function($excel) use ($data,$month,$section_id,$lefty,$resigned) {
                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Salary Summary");
                // Chain the setters
                $excel->setCreator($name);
                $excel->setDescription('Employee Salary Summary');
                $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$section_id,$lefty,$resigned) {
                    $sheet->loadView('report.payroll.salary_summary_xl_data')
                        ->with('data',$data)
                        ->with('month',$month)
                        ->with('section_id',$section_id)
                        ->with('lefty',$lefty)
                        ->with('resigned',$resigned);
                });
            })->download('xlsx');

        }
        $dt= $request->salary_summery_monthsss."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
        $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
        $month=$request->salary_summery_monthsss;
        $section_id=$request->section_id;
        $data=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
            ->where(function ($query) use ($dt){
                $query->where('date_of_discontinuation','=',null);
                $query->orWhere('date_of_discontinuation','>',$dt);
            })
        ->where('employees.empJoiningDate','<=',$dt)
        ->where('tb_salary_history.section_id','=',$section_id)
        ->where('tb_salary_history.status','=',0)
        ->get();

        $lefty=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',1)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        $resigned=DB::table('tb_salary_history')
        ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
        ->where('tb_salary_history.dates',$month)
        ->whereBetween('employees.date_of_discontinuation',array($s,$e))
        ->where('employees.discon_type','=',2)
        ->where('employees.empJoiningDate','<=',$dt)
        // ->groupBy('tb_salary_history.department_id')
        ->get();
        return view('report.payroll.salary_summery_monthly_data',compact('data','month','section_id','lefty','resigned'));
      }


       if($request->type=='all_data'){
            if($request->download_excel=='download_excel'){
                $dt= $request->salary_summery_monthsss."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                $month=$request->salary_summery_monthsss;
                $monthly_data='Monthly';
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                ->where('tb_salary_history.dates',$month)
                ->where(function ($query) use ($dt){
                    $query->where('date_of_discontinuation','=',null);
                    $query->orWhere('date_of_discontinuation','>',$dt);
                })
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.status','=',0)
                ->groupBy('tb_salary_history.department_id')
                ->get();

                $lefty=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                ->where('tb_salary_history.dates',$month)
                ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                ->where('employees.discon_type','=',1)
                ->where('employees.empJoiningDate','<=',$dt)
                // ->groupBy('tb_salary_history.department_id')
                ->get();
                $resigned=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                ->where('tb_salary_history.dates',$month)
                ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                ->where('employees.discon_type','=',2)
                ->where('employees.empJoiningDate','<=',$dt)
                // ->groupBy('tb_salary_history.department_id')
                ->get();
                $excelName=time()."_Salary_Summary";
                Excel::create("$excelName", function($excel) use ($data,$month,$monthly_data,$lefty,$resigned) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Salary Summary");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Employee Salary Summary');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$monthly_data,$lefty,$resigned) {
                        $sheet->loadView('report.payroll.salary_summary_xl_data')
                            ->with('data',$data)
                            ->with('month',$month)
                            ->with('monthly_data',$monthly_data)
                            ->with('lefty',$lefty)
                            ->with('resigned',$resigned);
                    });
                })->download('xlsx');

            }
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $month=$request->salary_summery_monthsss;
            $monthly_data='Monthly';
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
                ->where(function ($query) use ($dt){
                    $query->where('date_of_discontinuation','=',null);
                    $query->orWhere('date_of_discontinuation','>',$dt);
                })
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.status','=',0)
            ->groupBy('tb_salary_history.department_id')
            ->get();

            $lefty=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',1)
            ->where('employees.empJoiningDate','<=',$dt)
            // ->groupBy('tb_salary_history.department_id')
            ->get();
            $resigned=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',2)
            ->where('employees.empJoiningDate','<=',$dt)
            // ->groupBy('tb_salary_history.department_id')
            ->get();
            return view('report.payroll.salary_summery_monthly_data',compact('data','month','monthly_data','lefty','resigned'));
        }
    }
    //salary increment employees method view page
    public function SalaryIncrementEmployees(){
        $next_increment=DB::SELECT("SELECT increment_month
                        FROM emp_inc_check
                        GROUP BY increment_month");
        return view('payroll.increment_salary_employees',compact('next_increment'));
    }

    //search employee month wise to show which employee complete 1 year or more
    public function SalaryIncrementEmployeesSearch(Request $request){
          $year=date('Y',strtotime($request->incr_employees_month));
          $month=date('m',strtotime($request->incr_employees_month));
          $requestmonth=date('Y-m-d',strtotime($request->incr_employees_month));
          $type=$request->work_group;
          $increment=DB::SELECT("SELECT emp_inc_check.status,emp_inc_check.increment_month as next_increment,employees.id as em_id,employees.empFirstName,employees.employeeId,work_group,empJoiningDate,MONTH(empJoiningDate)as join_month,YEAR(empJoiningDate) as join_year,abs(YEAR(empJoiningDate)-'$year') as Year_difference,payroll_salary.total_employee_salary as cur_salary,payroll_salary.total_employee_salary-1850 as after_deduction
          FROM employees 
          LEFT JOIN payroll_salary ON payroll_salary.emp_id=employees.id 
          LEFT JOIN emp_inc_check ON emp_inc_check.emp_check_id=employees.id 
          WHERE MONTH(empJoiningDate)='$month' AND abs(YEAR(empJoiningDate)-'$year')>=1 AND employees.empAccStatus=1 AND work_group='$type'");
          return view('payroll.one_year',compact('increment','requestmonth'));
    }

    //increment bonus store multiple
    public function SalaryIncrementEmployeesBonusStore(Request $request){
        $month=date('Y-m');
        $addyear = date('Y-m-d', strtotime('+1 years'));
        $i=0;
        foreach($request->emp_id as $key=>$value)
        {
            $empid = $value;
            $percent=$_POST['emp_percent'][$key];
            $total_salary = $_POST['gross_salary'][$key];
            $percent_bonus= (int)$_POST['after_deduction'][$key]/100*(int)$percent;
            $insert[]=[
                'emp_id' =>$empid,
                'emp_gross' =>$_POST['gross_salary'][$key],
                'emp_bonus' =>$percent_bonus,
                'emp_total_percent' =>$percent,
                'bonus_given_id' =>auth()->user()->id,
                'date' =>date('Y-m'),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];

            $emp_inc_check[]=[
                'emp_check_id' =>$empid,
                'status' =>1,
                'increment_month' =>$addyear,
                'month' =>$month,
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
        }
        if($i%550==0){
            if(!empty($insert)){
                $insert_emp_bonus_data = DB::table('employees_bonus')->insert($insert);
                unset($insert);
            }
            if(!empty($emp_inc_check)){
                $emp_inc_checks = DB::table('emp_inc_check')->insert($emp_inc_check);
                unset($emp_inc_check);
            }
        }
        $i++;


        if($emp_inc_checks){
            for($i=0;$i<count($request->emp_id);$i++){
                $id=$request->emp_id[$i];
                $percent=$request->emp_percent[$i];
                $current_salary=$request->gross_salary[$i];
                $after_deduction=$request->after_deduction[$i];
                $percent_wise=$after_deduction/100*$percent;
                $totals=$percent_wise;
                $update=DB::table('payroll_salary')->where('emp_id',$id)->update([
                    'total_employee_salary' =>$totals+$current_salary
                ]);
            }
        }
        Session::flash('increment_bonus_add', 'Increment successful!');
        return redirect()->route('increment_search_home');
    }


    //next increment employees search to show data
    public function EmployeesNextIcrementSearch(Request $request){
        $search_year=$request->check_increment;
        $next_increment=DB::SELECT("SELECT emp_inc_check.emp_check_id as ac_emp_id,emp_inc_check.status,emp_inc_check.increment_month as next_increment,employees.id as em_id,employees.empFirstName,employees.employeeId,employees.work_group,payroll_salary.total_employee_salary as cur_salary,employees.empJoiningDate,TIMESTAMPDIFF(year,empJoiningDate,'$search_year') as year, TIMESTAMPDIFF(month,empJoiningDate,'$search_year') as month FROM employees
          LEFT JOIN payroll_salary ON payroll_salary.emp_id=employees.id
          LEFT JOIN emp_inc_check ON emp_inc_check.emp_check_id=employees.id
          WHERE emp_inc_check.increment_month='$search_year'");
          return view('payroll.next_increment_search',compact('next_increment'));
    }

    //existing increment data delete
    public function Increment_delete(Request $request){
      $delete_increment_year=$request->delete_next_increment;
      $data=DB::table('emp_inc_check')->where('increment_month',$delete_increment_year)->delete();
        Session::flash('increment_bonus_delete', 'Delete successful!');
        return redirect()->route('increment_search_home');
    }


    //resigned employees salary sheet view page
    public function resignedEmployeesSalaryView(){
        $department =DB::table('employees')
        ->where('employees.date_of_discontinuation','!=',null)
        ->where('employees.discon_type','=',2)
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->select('departments.id as dept_id','departments.departmentName')
        ->groupBy('employees.empDepartmentId')
        ->get();
        $section =DB::table('employees')
        ->select('empSection')
        ->where('employees.date_of_discontinuation','!=',null)
        ->where('employees.discon_type','=',2)
        ->whereNotNull('empSection')
        ->groupBy('empSection')->get();
        $employee   =DB::table('employees')
        ->where('employees.date_of_discontinuation','!=',null)
        ->where('employees.discon_type','=',2)
        ->select('id','empFirstName','empLastName','employeeId')
        ->get();
        return view('report.payroll.salarysheet.resigned_employee_salary_sheet',compact('department','section','employee'));
    }


    //resigned employees salary sheet generate
    public function resignedEmployeesSalaryShow(Request $request){
        $months=$request->emp_sal_month;
        $dt= $request->emp_sal_month."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $ft= $request->emp_sal_month."-28";
        $ft= Carbon::parse($ft)->firstOfMonth();
        $ft= Carbon::parse($ft)->toDateString();
        // dd($dt);
        $y=date('Y',strtotime($months));
        $m=date('m',strtotime($months));
        $monthname=DB::table('tb_salary_history')
            ->select('tb_salary_history.month')
            ->where('dates',$months)
            ->first();
        if($request->type=='dept_wise_data'){
            if($request->dept_excel=='dept_excel'){
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.department_id',$request->department_id)
                ->where('employees.discon_type','=',2)
                ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."Resigned_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Resigned Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Resigned Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('report.payroll.salarysheet.excel.resigned_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');

            }
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.department_id',$request->department_id)
            ->where('employees.discon_type','=',2)
            ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='section_wise_data'){
            if($request->section_excel=='section_excel'){
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.section_id',$request->section_id)
                ->where('employees.discon_type','=',2)
                ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."Resigned_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Resigned Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Resigned Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('report.payroll.salarysheet.excel.resigned_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');
            }   
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.section_id',$request->section_id)
            ->where('employees.discon_type','=',2)
            ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='employee_wise_data'){
            if($request->employee_excel=='employee_excel'){
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.emp_id',$request->emp_id)
                ->where('employees.discon_type','=',2)
                ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."Resigned_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Resigned Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Resigned Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('report.payroll.salarysheet.excel.resigned_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');
            }   
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.emp_id',$request->emp_id)
            ->where('employees.discon_type','=',2)
            ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='month_wise_data'){
            if($request->month_excel=='month_excel'){
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('employees.discon_type','=',2)
                ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."Resigned_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Resigned Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Resigned Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('report.payroll.salarysheet.excel.resigned_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');
            }   
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('employees.discon_type','=',2)
            ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }
        return view('report.payroll.salarysheet.resigned_employee_salary_sheet_show',compact('data','monthname','months'));
    }


    //lefty employees salary sheet view page
    public function leftyEmployeesSalaryView(){
      $department =DB::table('employees')
      ->where('employees.date_of_discontinuation','!=',null)
      ->where('employees.discon_type','=',1)
      ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
      ->select('departments.id as dept_id','departments.departmentName')
      ->groupBy('employees.empDepartmentId')
      ->get();
      $section =DB::table('employees')
      ->select('empSection')
      ->where('employees.date_of_discontinuation','!=',null)
      ->where('employees.discon_type','=',1)
      ->whereNotNull('empSection')
      ->groupBy('empSection')
      ->get();
      $employee =DB::table('employees')
      ->where('employees.date_of_discontinuation','!=',null)
      ->where('employees.discon_type','=',1)
      ->select('id','empFirstName','empLastName','employeeId')
      ->get();
      return view('report.payroll.salarysheet.lefty_employee_salary_sheet',compact('department','section','employee'));
    }


    //lefty employees salary sheet generate
    public function leftyEmployeesSalaryShow(Request $request){
        $months=$request->emp_sal_month;
        $dt= $request->emp_sal_month."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $ft= $request->emp_sal_month."-28";
        $ft= Carbon::parse($ft)->firstOfMonth();
        $ft= Carbon::parse($ft)->toDateString();
        // dd($dt);
        $y=date('Y',strtotime($months));
        $m=date('m',strtotime($months));
        $monthname=DB::table('tb_salary_history')
            ->select('tb_salary_history.month')
            ->where('dates',$months)
            ->first();
        if($request->type=='dept_wise_data'){
            if($request->dept_excel=='dept_excel'){
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.department_id',$request->department_id)
                ->where('employees.discon_type','=',1)
                ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."lefty_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Lefty Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Lefty Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('report.payroll.salarysheet.excel.lefty_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');
            }
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.department_id',$request->department_id)
            ->where('employees.discon_type','=',1)
            ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='section_wise_data'){
            if($request->section_excel=='section_excel'){
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.section_id',$request->section_id)
                ->where('employees.discon_type','=',1)
                ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."lefty_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Lefty Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Lefty Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('report.payroll.salarysheet.excel.lefty_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');

            }

            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.section_id',$request->section_id)
            ->where('employees.discon_type','=',1)
            ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='employee_wise_data'){
            if($request->employee_excel=='employee_excel'){
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.emp_id',$request->emp_id)
                ->where('employees.discon_type','=',1)
                ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."lefty_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Lefty Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Lefty Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('report.payroll.salarysheet.excel.lefty_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');

            }   
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.emp_id',$request->emp_id)
            ->where('employees.discon_type','=',1)
            ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='month_wise_data'){
            if($request->month_excel=='month_excel'){
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('employees.discon_type','=',1)
                ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."lefty_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Lefty Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Lefty Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('report.payroll.salarysheet.excel.lefty_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');
            }    
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('employees.discon_type','=',1)
            ->select('advance_salary','tb_salary_history.emp_id','employees.payment_mode','tb_salary_history.present','tb_salary_history.weekend','tb_salary_history.holiday','tb_salary_history.total_payable_days','tb_salary_history.basic_salary','tb_salary_history.house_rant','tb_salary_history.medical','tb_salary_history.transport','tb_salary_history.food','tb_salary_history.gross','tb_salary_history.leave','tb_salary_history.total','tb_salary_history.working_day','tb_salary_history.absent','tb_salary_history.absent_deduction_amount','tb_salary_history.advanced_deduction','tb_salary_history.gross_pay','tb_salary_history.overtime','tb_salary_history.overtime_rate','tb_salary_history.overtime_amount','tb_salary_history.attendance_bonus','tb_salary_history.increment_bonus_percent','tb_salary_history.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }
        return view('report.payroll.salarysheet.lefty_employee_salary_sheet_show',compact('data','monthname','months'));
    }

    //Employees Manual Overtime Add view page
    public function manual_overtime(){
        $employees=DB::table('employees')->select('id','employeeId','empFirstName')
            ->where('empAccStatus','=',1)
            ->where('empOTStatus','=',1)
            ->get();
        return view('payroll.overtime.menual_overtime',compact('employees'));
    }

    //Employees Manual Overtime store
    public function manual_overtime_store(Request $request){
        $check=DB::table('emp_manual_overtime')->where('emp_overtime_id',$request->emp_id)->where('month',$request->ot_bonus_month)->count();
       if($check=1){
           $delete=DB::table('emp_manual_overtime')->where('emp_overtime_id',$request->emp_id)->where('month',$request->ot_bonus_month)->delete();
           $overtime_manual=DB::table('emp_manual_overtime')->insert([
               'emp_overtime_id' =>$request->emp_id,
               'employee_overtime' =>$request->overtime,
               'month' =>$request->ot_bonus_month,
               'created_at' =>Carbon::now()->toDateTimeString(),
               'updated_at' =>Carbon::now()->toDateTimeString(),
           ]);
       }else{
           $overtime_manual=DB::table('emp_manual_overtime')->insert([
               'emp_overtime_id' =>$request->emp_id,
               'employee_overtime' =>$request->overtime,
               'month' =>$request->ot_bonus_month,
               'created_at' =>Carbon::now()->toDateTimeString(),
               'updated_at' =>Carbon::now()->toDateTimeString(),
           ]);
       }
        Session::flash('overtime_add_manually', 'Overtime added successfully');
        return redirect()->back();
    }


    //daily employee salary
    public function daily_employee_salary_view(){
        return view('report.payroll.daily_employees_salary');
    }

    //daily employee salary report data
    public function daily_employee_salary_view_data(Request $request){
      $dates=date('Y-m-d',strtotime($request->daily_date));
      $count=DB::table('employee_daily_salary')->count();
      if($count>0){
          $count=DB::table('employee_daily_salary')->delete();
      }
      $dates_show=date('d-m-Y',strtotime($request->daily_date));
      $data=DB::SELECT("SELECT attendance_setup.shiftName,attendance.emp_id,attendance.date,HOUR(attendance.out_time) as out_time,HOUR(attendance_setup.exit_time) as shift_wise_exit,employees.line_id as line_wise_id,payroll_salary.basic_salary as basic,payroll_salary.total_employee_salary as gross,payroll_salary.basic_salary/104 as ot_rate,HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time) as total_ot FROM attendance LEFT JOIN employees ON attendance.emp_id=employees.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id Left JOIN tblines ON employees.line_id=tblines.id LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id WHERE attendance.date='$dates' AND HOUR(attendance.out_time)>=HOUR(attendance_setup.exit_time) GROUP BY attendance.emp_id");
      if($data){
          $i=0;
          $two=0;
          foreach($data as $calculate){
              if($calculate->total_ot>2){
                  $two=$calculate->total_ot-2;
              }
              $total_ot_amount=$calculate->ot_rate*$calculate->total_ot;
              $insert_sals[]=[
                  'emp_id' =>$calculate->emp_id,
                  'line_id' =>$calculate->line_wise_id,
                  'two_hour_ot' =>$two,
                  'two_hour_ot_amount' =>round($calculate->ot_rate*$two,0),
                  'total_ot' =>$calculate->total_ot,
                  'total_ot_amount' =>round($total_ot_amount,0),
                  'total_gross' =>round($calculate->gross,0),
                  'salary_date' =>$dates,
                  'created_at' =>Carbon::now()->toDateTimeString(),
                  'updated_at' =>Carbon::now()->toDateTimeString(),
              ];
          }
          if($i%550==0){
              if(!empty($insert_sals)){
                  $insert_bonus_data = DB::table('employee_daily_salary')->insert($insert_sals);
                  unset($insert_sals);
              }
          }
          $i++;
          $select_salary=DB::SELECT("SELECT tblines.line_no,COUNT(employee_daily_salary.emp_id) as man_power,sum(employee_daily_salary.total_gross) as salary,SUM(employee_daily_salary.two_hour_ot) as two_hours_ot,SUM(employee_daily_salary.two_hour_ot_amount) as two_hours_ot_amount,SUM(employee_daily_salary.total_ot) as total_ot_hour,SUM(employee_daily_salary.total_ot_amount) as total_ot_amount FROM employee_daily_salary LEFT JOIN tblines ON employee_daily_salary.line_id=tblines.id WHERE employee_daily_salary.salary_date='$dates' GROUP BY employee_daily_salary.line_id");
          return view('report.payroll.dailyemployeesalaryview',compact('select_salary','dates_show'));
      }
      else{
          Session::flash('message', 'Sorry no data found');
          return redirect()->back();
      }
    }

    //download increment letter
    public function incrementLetter($id){
        $employeeData=DB::table('employees')
        ->leftjoin('designations','employees.empDesignationId','=','designations.id')
        ->leftjoin('employees_bonus','employees_bonus.emp_id','=','employees.id')
        ->leftjoin('payroll_salary','payroll_salary.emp_id','=','employees.id')
        ->leftjoin('payroll_grade','payroll_salary.grade_id','=','payroll_grade.id')
        ->leftjoin('grade_history','payroll_salary.grade_id','=','grade_history.gradeId')
        ->select('employees.empFirstName','employees.empLastName','designations.designation','designations.designationBangla','employees.empCardNumber','employees.empSection','employees.empJoiningDate','employees_bonus.emp_gross as previous_total_gross','payroll_salary.total_employee_salary as current_gross','payroll_grade.grade_name','grade_history.basicSalary as employees_previous_basic','grade_history.houseRent as previous_house_rent','grade_history.medical as previous_medical','grade_history.transport as previous_transport','grade_history.food as previous_food','payroll_salary.basic_salary as current_basic','payroll_salary.house_rant as current_houserent','payroll_salary.medical as current_medical','payroll_salary.transport as current_transport','payroll_salary.food as current_food','employees_bonus.emp_total_percent','employees_bonus.emp_bonus')
        ->where('employees.id',$id)
        ->first();
        $pdf = mPDF::loadView('payroll.increment_letter.letters',compact('employeeData'));
        $name=time()."increment_letter.pdf";
        return $pdf->download($name);
    }

    //employee wise salary process
    public function id_card_process_salary(Request $request){
       DB::table('extra_ot_result')->delete();
       DB::table('tb_total_present')->delete();
       $count=DB::table('tb_salary_history')->where('month',$request->salary_sheet_month)->where('emp_id',$request->card_no)->count();
       if($count>0){
         DB::table('tb_salary_history')->where('month',$request->salary_sheet_month)->where('emp_id',$request->card_no)->delete();
       }

       $set_date='';
       $mof_month=date('t',strtotime($request->salary_sheet_month))/2;
       $round_middle=(int)$mof_month;
       $s=date('Y-m-01',strtotime($request->salary_sheet_month));
       $e=date('Y-m-t',strtotime($request->salary_sheet_month));
       $regular_days_setup_check=DB::table('tbweekend_regular_day')
       ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
       ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
       ->count();
       $card=$request->card_no;
        // MY CODE
        $regular_fridays_date=DB::table('tbweekend_regular_day')
        ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
        ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
        ->get(['weekend_date']);
        $regular_weekend_dates=array();
        foreach ($regular_fridays_date as $rfd){
            $regular_weekend_dates[]=$rfd->weekend_date;
        }
        $regular_weekend_presence=DB::table('employees')
        ->join('attendance','employees.id', '=', 'attendance.emp_id')
        ->where('employees.id',$card)
        ->whereIn('attendance.date',$regular_weekend_dates)
        ->select('employees.id as emp_id',DB::raw("count(attendance.emp_id) as present"))
        ->groupBy('employees.id')
        ->get();
        foreach ($regular_weekend_presence as $presents){
            $insert[]=[
                'emp_id'=>$presents->emp_id,
                'regular_present'=>$presents->present,
            ];
        }

        if(!empty($insert)){
            DB::table('regular_weekend_presence')->insert($insert);
            unset($insert);
        }
        $weekend_present=DB::table('employees')
        ->join('attendance','employees.id','=','attendance.emp_id')
        ->where('employees.id',$card)
        ->whereIn('attendance.date',$this->weekend_dates($s,$e))
        ->select(DB::raw('count(employees.id) as weekend_present'),'employees.id as emp_id')
        ->groupBy('employees.id')
        ->get();
        foreach ($weekend_present as $wp){
            $insert[]=[
                'emp_id'=>$wp->emp_id,
                'weekend_present'=>$wp->weekend_present,
            ];
        }

        if(!empty($insert)){
            DB::table('total_weekend_present')->insert($insert);
            unset($insert);
        }
        // My Code End

       $money_deduction=10;
       $month_check=date('Y-m-d',strtotime($request->salary_sheet_month));
       $weeks=$this->weekdayCalculator($s,$e)-$regular_days_setup_check;
       $holii=$this->holiday($s,$e);
       $actual_working_day=date('t',strtotime($request->salary_sheet_month))-$weeks-$holii;
       $process_check=DB::table('tb_salary_process_check')
           ->where('month',$month_check)
           ->where('status','=',1)
           ->count();
       if($process_check>0){
           Session::flash('salaryprocesscheck','Process Salary Already Completed this month');
           return redirect()->back();
       }
       $date=date('m-Y');
       $current_time = Carbon::now()->toDateTimeString();
       $current_month_first=date('Y-m-01',strtotime($request->salary_sheet_month));
       $current_month_last=date('Y-m-t',strtotime($request->salary_sheet_month));
       $working_day=$this->dayCalculator($current_month_first,$current_month_last);
       $month=date('Y-m-d',strtotime($request->salary_sheet_month));
       $workingmonth=$request->salary_sheet_month;
    //    $overtime_result = DB::SELECT(DB::raw("SELECT attendance.emp_id, employees.empFirstName,attendance.emp_id,attendance.out_time,attendance.date,MINUTE(attendance.out_time)-MINUTE(attendance_setup.exit_time) as overtime_minute,SUM(IF(HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time)>2,2,HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time))) as tot,SUM(CASE WHEN HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time)>1 THEN 0 WHEN MINUTE(attendance.out_time)-MINUTE(attendance_setup.exit_time)>54 THEN 1 WHEN MINUTE(attendance.out_time)-MINUTE(attendance_setup.exit_time)>24 THEN .5 END) AS t FROM attendance LEFT JOIN employees ON attendance.emp_id=employees.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE attendance.date BETWEEN '$current_month_first' AND '$current_month_last' AND employees.empOTStatus=1 AND HOUR(attendance.out_time)>HOUR(attendance_setup.exit_time) AND attendance.emp_id= $card AND attendance.out_time != attendance.in_time AND DAYNAME(attendance.date)!='Friday' GROUP BY attendance.emp_id"));
    $regular_work_days =DB::table('tbweekend_regular_day')
    ->select(DB::raw('group_concat(weekend_date) as date'))
    ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
    ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
    ->groupBy('weekend_month')
    ->first();
    if(isset($regular_work_days)){
       $set_date=$regular_work_days->date;
    }
   $overtime_result=DB::SELECT("SELECT attendance.emp_id,employees.employeeId,employees.empFirstName,attendance.emp_id,attendance_setup.exit_time AS exit_time,attendance.out_time as out_time,HOUR(attendance_setup.exit_time) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance_setup.exit_time) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM attendance LEFT JOIN employees ON attendance.emp_id=employees.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE attendance.date BETWEEN '$current_month_first' AND '$current_month_last' AND employees.empOTStatus=1 AND (DAYNAME(attendance.date)!='Friday' OR attendance.date IN ('$set_date')) AND time(attendance.out_time)>time(attendance_setup.exit_time) AND attendance.emp_id=$card AND attendance.in_time!=attendance.out_time  GROUP BY attendance.emp_id, attendance.date");

       $ot_minute=0;
       $data=0;
       foreach($overtime_result as $extraots){
        $minute=0;
       $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;
       if($ot_hour>=2){
           $ot_hours=2;
       }
       elseif($ot_hour<2){
           $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time;
            $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
            if($ot_minute>=25 && $ot_minute<55){
                $minute=.5;
            }
            elseif($ot_minute>=55){
                $minute=1;
            }
            else{
                $minute=0;

            }
       }
       else{
           $ot_hours=0;
       }
       $insert[]=[
           'emp_id' =>$extraots->emp_id,
           'emp_hour'=>$ot_hours,
           'emp_minute'=>$minute,
       ];
   }
   if($data%550==0){
       if(!empty($insert)){
           $insertData = DB::table('extra_ot_result')->insert($insert);
           unset($insert);
       }
   }
   $data++;
   $extraotsss=DB::SELECT("SELECT extra_ot_result.emp_id,SUM(emp_hour) as total_hour,SUM(emp_minute) as total_minute from extra_ot_result
   GROUP BY extra_ot_result.emp_id");

       $present = DB::SELECT("SELECT attendance.emp_id,empFirstName,count(date) as present,count(date)-(ifnull(total_weekend_present.weekend_present, 0)-ifnull(regular_weekend_presence.regular_present,0)) as actual_present,attendance.date,total_weekend_present.weekend_present,regular_weekend_presence.regular_present from attendance 
       LEFT JOIN employees ON attendance.emp_id=employees.id 
       LEFT JOIN regular_weekend_presence ON attendance.emp_id=regular_weekend_presence.emp_id 
       LEFT JOIN total_weekend_present ON attendance.emp_id=total_weekend_present.emp_id
       WHERE attendance.in_time IS NOT NULL AND attendance.date BETWEEN '$current_month_first' AND '$current_month_last' AND attendance.emp_id=$card 
       GROUP BY attendance.emp_id");

       $att=DB::SELECT("SELECT employees.id as emp_id,tbattendance_bonus.bTitle,tbattendance_bonus.bAmount FROM employees LEFT JOIN tbattendance_bonus ON employees.empAttBonusId = tbattendance_bonus.id WHERE employees.id=$card  AND employees.empAttBonusId!=0");
       $late=DB::table('employees')
        ->join('attendance_setup','employees.empShiftId','=','attendance_setup.id')
        ->join('attendance','employees.id','=','attendance.emp_id')
        ->whereBetween('attendance.date',[$current_month_first,$current_month_last])
        ->where('employees.id','=',$card)
        ->where('attendance.in_time','>',DB::raw('attendance_setup.max_entry_time'))
        ->select('employees.id as emp_id',DB::raw('count(employees.id) as late'))
        ->groupBy('employees.id')
        ->get();

       $ott=0;
       foreach ($extraotsss as $or){
               $insert[]=[
                   'emp_ids'=>$or->emp_id,
                   'overtime_hour'=>$or->total_hour+$or->total_minute,
                   'month'=>$month,
               ];
       }
       if($ott%550==0){
           if(!empty($insert)){
               $insertData = DB::table('employee_overtime')->insert($insert);
               unset($insert);
           }
       }
       $ott++;
      $total_emp_overtime_empls=DB::SELECT("SELECT employee_overtime.emp_ids as rmid,SUM(employee_overtime.overtime_hour) as total_overtime FROM employee_overtime WHERE employee_overtime.emp_ids=$card GROUP BY employee_overtime.emp_ids");
      $temp_ott=0;
       foreach($total_emp_overtime_empls as $tmp_overtime){
           $insert[]=[
               'overtime_id' =>$tmp_overtime->rmid,
               'employee_overtime' =>$tmp_overtime->total_overtime,
               'month' =>$month
           ];
       }
       if($temp_ott%550==0){
           if(!empty($insert)){
               $insertData = DB::table('temp_employee_overtime')->insert($insert);
               unset($insert);
           }
       }
       $temp_ott++;


       $latee=0;
       foreach ($late as $lates){
               $insert[]=[
                   'emp_id'=>$lates->emp_id,
                   'total_late'=>$lates->late,
                   'month'=>$month,
               ];
       }
       if($latee%550==0){
           if(!empty($insert)){
               $insertData = DB::table('emp_total_late')->insert($insert);
               unset($insert);
           }
       }
       $latee++;

       $pre_insert=0;
       foreach ($present as $presents){
           $insert[]=[
               'emp_id'=>$presents->emp_id,
               'total_present'=>$presents->present,
               'total_absent'=>$this->dayCalculator($current_month_first,$current_month_last)-$presents->present,
               'month'=>$month,
           ];
       }
       if($pre_insert%550==0){
           if(!empty($insert)){
               $insertData = DB::table('tb_total_present')->insert($insert);
               unset($insert);
           }
       }

        $leave_of_employee=DB::table('leave_of_employee')->where('emp_id','=',$card)
            ->where('month','=',$month_check)
            ->get();

       $pre_insert++;
       $leave_tota=DB::SELECT("SELECT leave_of_employee.emp_id as employee_id,SUM(leave_of_employee.total) as total_leave 
                               FROM leave_of_employee
                               WHERE month='$month_check'
                               AND leave_of_employee.emp_id=$card
                               GROUP BY leave_of_employee.emp_id");
       $total_emp_leavesss=0;
       foreach ($leave_tota as $leaves){
           $insert[]=[
               'emp_idss'=>$leaves->employee_id,
               'total_leaves_taken'=>$leaves->total_leave,
               'month'=>$month,
           ];
       }
       if($total_emp_leavesss%550==0){
           if(!empty($insert)){
               $insertData = DB::table('total_employee_leave')->insert($insert);
               unset($insert);
           }
       }
       $total_emp_leavesss++;
       $checkattbonus=DB::table('attendance_bonus')->where('month',date('Y-m',strtotime($request->salary_sheet_month)))->count();
      if($checkattbonus>0){
           $att_bonus_incret=0;
           $deleteattbonus=DB::table('attendance_bonus')->where('month',date('Y-m',strtotime($request->salary_sheet_month)))->delete();
           foreach ($att as $atts){
               if($atts->bAmount==''){
                   $bonus=0;
               }
               else{
                   $bonus=$atts->bAmount;
               }
               $insert[]=[
                   'bonus_id'=>$atts->emp_id,
                   'bonus_amount'=>$bonus,
                   'emp_total_amount'=>$bonus,
                   'month'=>date('Y-m',strtotime($request->salary_sheet_month)),
                   'created_at' =>Carbon::now()->toDateTimeString(),
                   'updated_at' =>Carbon::now()->toDateTimeString(),
               ];
           }
           if($att_bonus_incret%550==0){
               if(!empty($insert)){
                   $insertData = DB::table('attendance_bonus')->insert($insert);
                   unset($insert);
               }
           }
           $att_bonus_incret++;
       }
       else{
           $att_bonus_incret_one=0;
           foreach ($att as $atts){
               if($atts->bAmount==''){
                   $bonus=0;
               }
               else{
                   $bonus=$atts->bAmount;
               }
               $insert[]=[
                   'bonus_id'=>$atts->emp_id,
                   'bonus_amount'=>$bonus,
                   'emp_total_amount'=>$bonus,
                   'month'=>date('Y-m',strtotime($request->salary_sheet_month)),
                   'created_at' =>Carbon::now()->toDateTimeString(),
                   'updated_at' =>Carbon::now()->toDateTimeString(),
               ];
           }
           if($att_bonus_incret_one%550==0){
               if(!empty($insert)){
                   $insertData = DB::table('attendance_bonus')->insert($insert);
                   unset($insert);
               }
           }
           $att_bonus_incret_one++;
       }
       $start = date('Y-m-01',strtotime($request->salary_sheet_month));
       $end = date('Y-m-t',strtotime($request->salary_sheet_month));
       $monthscheck=date('Y-m-d',strtotime($request->salary_sheet_month));
       $bonusmonth=date('Y-m',strtotime($request->salary_sheet_month));
       $bonusmonth_salary=date('Y-m-d',strtotime($request->salary_sheet_month));
       $data=DB::table('payroll_salary')
           ->leftJoin('employees', function ($query){
               $query->on('payroll_salary.emp_id', '=', 'employees.id');
            //    $query->where('employees.empAccStatus','=',1);
           })
           ->leftJoin('payroll_grade', function ($query){
               $query->on('payroll_salary.grade_id', '=', 'payroll_grade.id');
           })
           ->leftJoin('units', function ($query) {
               $query->on('employees.unit_id', '=', 'units.id');
           })
           ->leftJoin('floors', function ($query) {
               $query->on('employees.floor_id', '=', 'floors.id');
           })
           ->leftJoin('tb_advance_salary', function ($query) use($start) {
            $query->on('payroll_salary.emp_id', '=', 'tb_advance_salary.emp_id');
            $query->where('tb_advance_salary.month','=',$start);
          })
           ->leftJoin('tblines', function ($query){
               $query->on('employees.line_id', '=', 'tblines.id');
           })
           ->leftJoin('total_employee_leave', function ($query) use($monthscheck) {
               $query->on('payroll_salary.emp_id', '=', 'total_employee_leave.emp_idss');
               $query->where('total_employee_leave.month','=',$monthscheck);
           })
           ->leftJoin('leave_of_employee', function ($query) use($monthscheck) {
               $query->on('payroll_salary.emp_id', '=', 'leave_of_employee.emp_id');
               $query->where('leave_of_employee.month','=',$monthscheck);
           })
           ->leftJoin('tb_leave_application', function ($query) use($monthscheck) {
            $query->on('payroll_salary.emp_id', '=', 'tb_leave_application.employee_id');
            $query->whereYear('tb_leave_application.leave_ending_date', '=',date('Y',strtotime($monthscheck)));
            $query->whereMonth('tb_leave_application.leave_ending_date', '=',date('m',strtotime($monthscheck)));
            $query->where('tb_leave_application.status','=',1);
            $query->where('tb_leave_application.leave_type_id','=',1);
            })
           ->leftJoin('emp_total_late', function ($query) use($monthscheck) {
            $query->on('payroll_salary.emp_id', '=', 'emp_total_late.emp_id');
            $query->where('emp_total_late.month','=',$monthscheck);
           })
           ->leftJoin('tb_leave_type', function ($query) {
               $query->on('leave_of_employee.leave_type_id', '=', 'tb_leave_type.id');
           })
           ->leftJoin('tb_total_present', function ($query) use($monthscheck) {
               $query->on('payroll_salary.emp_id', '=', 'tb_total_present.emp_id');
               $query->where('tb_total_present.month','=',$monthscheck);
           })
           ->leftJoin('temp_employee_overtime', function ($query) use($monthscheck) {
               $query->on('payroll_salary.emp_id', '=', 'temp_employee_overtime.overtime_id');
               $query->where('temp_employee_overtime.month','=',$monthscheck);
           })
           ->leftJoin('emp_manual_overtime', function ($query) use($bonusmonth) {
               $query->on('payroll_salary.emp_id', '=', 'emp_manual_overtime.emp_overtime_id');
               $query->where('emp_manual_overtime.month','=',$bonusmonth);
           })
           ->leftJoin('attendance_bonus', function ($query) use($bonusmonth) {
               $query->on('payroll_salary.emp_id', '=', 'attendance_bonus.bonus_id');
               $query->where('attendance_bonus.month','=',$bonusmonth);
           })
           ->leftJoin('employees_bonus', function ($query) use($bonusmonth) {
               $query->on('payroll_salary.emp_id', '=', 'employees_bonus.emp_id');
               $query->where('employees_bonus.date','=',$bonusmonth);
           })
           ->leftJoin('festival_bonus', function ($query) use($bonusmonth) {
               $query->on('payroll_salary.emp_id', '=', 'festival_bonus.emp_id');
               $query->where('festival_bonus.month','=',$bonusmonth);
           })
           ->leftJoin('tb_deduction', function ($query) use($bonusmonth) {
               $query->on('payroll_salary.emp_id', '=', 'tb_deduction.emp_id');
               $query->where('tb_deduction.month','=',$bonusmonth);
           })
           ->leftJoin('tb_loan_deduction', function ($query) use($bonusmonth) {
               $query->on('payroll_salary.emp_id', '=', 'tb_loan_deduction.emp_id');
               $query->where('tb_loan_deduction.month','=',$bonusmonth);
           })
           ->leftJoin('tb_production_bonus', function ($query) use($bonusmonth) {
               $query->on('payroll_salary.emp_id', '=', 'tb_production_bonus.emp_id');
               $query->where('tb_production_bonus.month','=',$bonusmonth);
           })
           ->leftJoin('departments', function ($query) {
               $query->on('employees.empDepartmentId', '=', 'departments.id');
           })
           ->Join('designations', function ($query) {
               $query->on('designations.id', '=', 'employees.empDesignationId');
           })
           ->select('payroll_salary.*','employees.empDesignationId','employees.payment_mode','tb_leave_application.leave_ending_date as maternity_ending','employees.empDepartmentId','employees.empFirstName','employees.empLastName','employees.empJoiningDate','employees.empSection','employees.employeeId','payroll_grade.grade_name','designations.designation','employees.unit_id','units.name','employees.floor_id','floors.floor','employees.line_id','tblines.line_no','employees.empDepartmentId','departments.departmentName','employees.work_group','employees.payment_mode','total_employee_leave.total_leaves_taken','employees_bonus.emp_bonus as special_bonus','employees_bonus.emp_amount','attendance_bonus.bonus_amount as attendance_bonus','attendance_bonus.bonus_percent','festival_bonus.emp_bonus as f_bonus','festival_bonus.emp_amount as f_amount','tb_total_present.total_present','leave_of_employee.leave_type_id','leave_of_employee.total','tb_deduction.normal_deduction_amount','tb_loan_deduction.month_wise_deduction_amount','tb_production_bonus.production_amount','tb_production_bonus.production_percent','temp_employee_overtime.employee_overtime as temp_ot','emp_manual_overtime.employee_overtime as manual_ot','designations.gradeId','emp_total_late.total_late','date_of_discontinuation','tb_advance_salary.advance_amount','leave_of_employee.start_date','leave_of_employee.end_date')
           ->selectraw("CONCAT(GROUP_CONCAT( DISTINCT(tb_leave_type.leave_type),leave_of_employee.total)) as leave_name_value")
           ->selectraw("GROUP_CONCAT(DISTINCT(leave_of_employee.leave_type_id)SEPARATOR ',') as leave_with_id")
           ->selectraw("GROUP_CONCAT(DISTINCT(leave_of_employee.total)SEPARATOR ',') as leave_with_total")
           ->where('payroll_salary.emp_id',$card)
           ->groupBy('payroll_salary.emp_id')
           ->get();


           $month=date('Y-m-d',strtotime($request->salary_sheet_month));
           $dates=date('Y-m',strtotime($request->salary_sheet_month));
           $check=DB::table('tb_salary_history')->count();
           if($check>0){
//               $delete=DB::table('tb_salary_history')->delete();
               DB::table('tb_salary_history')->where('month',$request->salary_sheet_month)->where('emp_id',$request->card_no)->delete();
           }
           $current_month_first=date('Y-m-01',strtotime($workingmonth));
           $current_month_last=date('Y-m-t',strtotime($workingmonth));
           $working_day=$this->dayCalculator($current_month_first,$current_month_last)+$regular_days_setup_check;
           $total_sal_datassss=0;
           foreach($data as $testdata){
           $leave_weekend=0;
           $leave_regular_weekend=0;
           foreach ($leave_of_employee as $loe){
               $leave_regular_weekend+=DB::table('tbweekend_regular_day')
                   ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
                   ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
                   ->whereBetween('weekend_date',[$loe->start_date,$loe->end_date])
                   ->count();
               $leave_weekend+=$this->weekdayCalculator($loe->start_date,$loe->end_date);

           }
            global $deduction;
            global  $ac_abs_day;
            global  $weekend;
            global  $holi;
            global  $total_payable_days;
            global $total_gross;
            global  $join_day;
            global $dis_continution_day;
            $status=0;
            $att_bonus=0;
            $total_gross_pre=0;
            $current_month_day=date('t',strtotime($workingmonth));
            $overtime_rate=$testdata->basic_salary/104;
            $actual_overtime_rate=$overtime_rate;
            $total_overtt_time=$testdata->temp_ot+$testdata->manual_ot;
            $overtime_amount=$actual_overtime_rate*$total_overtt_time;
            //default calculation
            $weeks=$this->weekdayCalculator($s,$e)-$regular_days_setup_check-$leave_weekend+$leave_regular_weekend;
            $holii=$this->holiday($s,$e);
            $actual_working_day=date('t',strtotime($request->salary_sheet_month))-$weeks-$holii;
            $absentday=$actual_working_day-$testdata->total_leaves_taken-$testdata->total_present;
            $ac_abs_day=max($absentday,0);
            $weekend=$weeks;
            $holi=$this->holiday($current_month_first,$current_month_last);
            $month_lst_day=date('t',strtotime($month));
            $total_payable_days=$month_lst_day-$ac_abs_day;
            $deduction=round($testdata->basic_salary/30*$ac_abs_day,2);
            //default calculation


               if(date('Y-m',strtotime($testdata->empJoiningDate))==$bonusmonth && date('Y-m',strtotime($testdata->date_of_discontinuation))==$bonusmonth){
                   $weekss = $this->weekdayCalculator($testdata->empJoiningDate, $testdata->date_of_discontinuation) - $regular_days_setup_check - $leave_weekend+$leave_regular_weekend;
                   $holiii = $this->holiday($testdata->empJoiningDate, $testdata->date_of_discontinuation);
                   $p_date1 = date_create(date('Y-m-01', strtotime($request->salary_sheet_month)));
                   $p_date2 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
                   $previous_diff = date_diff($p_date1, $p_date2);
                   $total_previous_difference = $previous_diff->format("%a");
                   $p_date1 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
                   $p_date2 = date_create(date('Y-m-t', strtotime($request->salary_sheet_month)));
                   $previous_diff2 = date_diff($p_date1, $p_date2);
                   $total_previous_difference2 = $previous_diff2->format("%a");
                   $total_previous_difference+=$total_previous_difference2;
                   $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
                   $date1 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
                   $date2 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
                   $diff = date_diff($date1, $date2);
                   $total_difference = $diff->format("%a") + 1;
                   $actual_working = $total_difference - $weekss - $holiii;
                   $working_days = $actual_working;
//                    return $working_days;
                   $absentdays = $working_days - $testdata->total_leaves_taken - $testdata->total_present;
                   $ac_abs_day = max($absentdays, 0);
                   $weekend = $weekss;
                   $holi = $holiii;
                   $month_lst_day = date('t', strtotime($month));
                   $total_payable_days = $month_lst_day - $ac_abs_day;
                   $month_salary = date('01', strtotime($request->salary_sheet_month));
                   $join_date = $testdata->empJoiningDate;
                   $join_day = date('d', strtotime($testdata->empJoiningDate));
                   if (date('d', strtotime($join_date)) >= $month_salary) {
                       $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
                   }
                   else{
                       $deduction=0;
                   }


               }

               else {
                   //check if join date this month
                   if (date('Y-m', strtotime($testdata->empJoiningDate)) == $bonusmonth) {
                       $tot_week_regular=DB::table('tbweekend_regular_day')
                       ->whereBetween('weekend_date', array($testdata->empJoiningDate, $e))
                       ->count();
                       $weekss = $this->weekdayCalculator($testdata->empJoiningDate,$e)-$tot_week_regular - $leave_weekend+$leave_regular_weekend;
                       $holiii = $this->holiday($testdata->empJoiningDate,$e);
                       $p_date1 = date_create(date('Y-m-01', strtotime($request->salary_sheet_month)));
                       $p_date2 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
                       $previous_diff = date_diff($p_date1, $p_date2);
                       $total_previous_difference = $previous_diff->format("%a");
                       $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
                       //  dd($testdata->current_month_day);
                       $date1 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
                       $date2 = date_create(date('Y-m-t', strtotime($request->salary_sheet_month)));
                       $diff = date_diff($date1, $date2);
                       $total_difference = $diff->format("%a") + 1;
                       $actual_working = $total_difference - $weekss - $holiii;
                       $working_days = $actual_working;
                       $absentdays = $working_days - $testdata->total_leaves_taken - $testdata->total_present;
                       $ac_abs_day = max($absentdays, 0);
                       $weekend = $weekss;
                       $holi = $holiii;
                       $month_lst_day = date('t', strtotime($month));
                       $total_payable_days = $month_lst_day - $ac_abs_day;
                       $month_salary = date('01', strtotime($request->salary_sheet_month));
                       $join_date = $testdata->empJoiningDate;
                       $join_day = date('d', strtotime($testdata->empJoiningDate));

                       if (date('d', strtotime($join_date)) >= $month_salary) {
                           $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
                       }
                   }
                   //check if join date this month

                   //check if date of discontinuation this month
                   if (date('Y-m', strtotime($testdata->date_of_discontinuation)) == $bonusmonth) {
                       $dis_continution_day = date('d', strtotime($testdata->date_of_discontinuation));
                       if (date('d', strtotime($testdata->date_of_discontinuation)) >= 01 && date('d', strtotime($testdata->date_of_discontinuation)) <= 31) {
                           $month_start = date('Y-m-01', strtotime($request->salary_sheet_month));
                           $weekss = $this->weekdayCalculator($month_start, $testdata->date_of_discontinuation)-$regular_days_setup_check - $leave_weekend+$leave_regular_weekend;
                           $holiii = $this->holiday($month_start, $testdata->date_of_discontinuation);
                           $weekend = $weekss;
                           $holi = $holiii;
                           $p_date1 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
                           $p_date2 = date_create(date('Y-m-t', strtotime($request->salary_sheet_month)));
                           $previous_diff = date_diff($p_date1, $p_date2);
                           $total_previous_difference = $previous_diff->format("%a");
                           $actual_working_day = date('d', strtotime($testdata->date_of_discontinuation)) - $weekend - $holi;
                           $absentday = $actual_working_day - $testdata->total_leaves_taken - $testdata->total_present;
                           $ac_abs_day = max($absentday, 0);
                           $month_lst_day = date('t', strtotime($month));
                           $total_payable_days = $month_lst_day - $ac_abs_day;
                           $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
                           $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
                       }
                   }
                   //check if date of discontinuation this month
               }

                //check if maternity leave this month
                 if(date('Y-m',strtotime($testdata->start_date))==$bonusmonth && $testdata->leave_with_id==1){
                    $month_start=date('Y-m-01',strtotime($request->salary_sheet_month));
                    $month_end=date('Y-m-t',strtotime($request->salary_sheet_month));
                    $weekss=$this->weekdayCalculator($month_start,$month_end) -$regular_days_setup_check-$leave_weekend;
                    $holiii=$this->holiday($month_start,$testdata->start_date);
                    $weekend=$weekss;
                    $holi=$holiii;
                    $actual_working_day=date('t',strtotime($request->salary_sheet_month))-$weeks-$holii;
                    $absentday=$actual_working_day-$testdata->total_leaves_taken-$testdata->total_present;
                    $ac_abs_day=max($absentday,0);
                    $total_gross_pre=($testdata->total_employee_salary/$current_month_day)*$testdata->total;
                    $deduction=round($testdata->basic_salary/30*$ac_abs_day,2);

                 }
                  //check if maternity leabve this month



            //check if maternity ending this month
            // if($testdata->maternity_ending){
            //     dd('emp found');
            //     $m_day=date('d',strtotime($testdata->maternity_ending));
            //     if($m_day>1){
            //      $weekss=$this->weekdayCalculator($testdata->maternity_ending,$e)-$regular_days_setup_check+$leave_regular_weekend;
            //      $holiii=$this->holiday($testdata->maternity_ending,$e);
            //      $actual_working=date('t',strtotime($request->salary_sheet_month))-$weekss-$holiii;
            //      $working_days=$actual_working;
            //      $absentdays=$working_days-$testdata->total_leaves_taken-$testdata->total_present;
            //      $ac_abs_day=max($absentdays,0);
            //      $weekend=$weekss;
            //      $holi=$holiii;
            //      $month_lst_day=date('t',strtotime($month));
            //      $total_payable_days=$month_lst_day-$ac_abs_day;
            //      $deduction=round($testdata->total_employee_salary/$current_month_day*$ac_abs_day,2);
            //     }
            //  }
            //check if maternity ending this month




               if($testdata->payment_mode!='Bank') {
                   $stampFee = 10;
               }
               else {
                   $stampFee = 0;
               }
             //check if maternity leave exists and no present
              if($testdata->leave_with_id==1 && $testdata->total_present==''){
                $status=1;
              }
              //check if maternity leave exists and no present

            //check att bonus condition wise
               if($testdata->total=='' && $ac_abs_day==0 && $testdata->total_late<3 && $testdata->empJoiningDate<=$current_month_first){
                   $att_bonus=$testdata->attendance_bonus;
                   if($testdata->date_of_discontinuation!=null && $testdata->date_of_discontinuation<$current_month_last){
                       $att_bonus=0;

                   }
               }else{
                $att_bonus=0;
               }
             //check att bonus condition

               $gross_pay=$testdata->total_employee_salary-$deduction-$testdata->normal_deduction_amount-$testdata->month_wise_deduction_amount-$stampFee;
            //    dd($total_gross_pre);
                $gross_pay=$gross_pay-$total_gross_pre;
               $month=date('Y-m-d',strtotime($request->salary_sheet_month));
               $net_wages=$gross_pay+$overtime_amount+$testdata->f_bonus+$testdata->f_amount+$testdata->production_amount+$testdata->production_percent+$att_bonus+$testdata->advance_amount;
            //    dd($net_wages);
               $actual_net_wages=$net_wages;

               if($actual_net_wages<=1000){
                $gross_pay=$gross_pay+10;
                $actual_net_wages=$actual_net_wages+10;
               }
               $insert[]=[
                   'emp_id' => $testdata->emp_id,
                   'grade_id' => $testdata->gradeId,
                   'department_id' => $testdata->empDepartmentId,
                   'section_id' => $testdata->empSection,
                   'designation_id' => $testdata->empDesignationId,
                   'basic_salary' => $testdata->basic_salary,
                   'house_rant' => $testdata->house_rant,
                   'medical' => $testdata->medical,
                   'transport' => $testdata->transport,
                   'food' => $testdata->food,
                   'other' => $testdata->other,
                   'gross' => $testdata->total_employee_salary,
                   'gross_pay' => $gross_pay,
                   'present' => $testdata->total_present,
                   'absent' => $ac_abs_day,
                   'absent_deduction_amount' => $deduction,
                   'attendance_bonus' =>  $att_bonus,
                   'attendance_bonus_percent' => $testdata->bonus_percent,
                   'festival_bonus_amount' => $testdata->f_amount,
                   'festival_bonus_percent' => $testdata->f_bonus,
                   'increment_bonus_amount' => $testdata->emp_amount,
                   'increment_bonus_percent' => $testdata->special_bonus,
                   'normal_deduction' => $testdata->normal_deduction_amount,
                   'advanced_deduction' => $testdata->month_wise_deduction_amount,
                   'production_bonus' => $testdata->production_amount,
                   'production_percent' => $testdata->production_percent,
                   'working_day' =>$working_day,
                   'weekend' =>$weekend,
                   'holiday' =>$holi,
                   'total_payable_days' =>$total_payable_days,
                   'overtime' =>$total_overtt_time,
                   'overtime_rate' =>$actual_overtime_rate,
                   'overtime_amount' =>$overtime_amount,
                   'leave' =>$testdata->leave_with_id,
                   'total' =>$testdata->total_leaves_taken,
                   'total_late' =>$testdata->total_late,
                   'advance_salary'=>$testdata->advance_amount,
                   'net_amount' =>$actual_net_wages,
                   'month' => $month,
                   'dates' => $dates,
                   'status' =>  $status,
                   'created_at' =>Carbon::now()->toDateTimeString(),
                   'updated_at' =>Carbon::now()->toDateTimeString(),
               ];
           }
            //    dd($actual_net_wages+$testdata->advance_amount);
           if($total_sal_datassss%550==0){
               if(!empty($insert)){
                   $insertData = DB::table('tb_salary_history')->insert($insert);
                   unset($insert);
               }
           }
           $total_sal_datassss++;
       $insert_check_data=DB::table('tb_salary_process_check')->insert([
           'month' =>$month,
           'status' =>0,
           'process_id' =>auth()->user()->id,
           'created_at' =>Carbon::now()->toDateTimeString(),
           'updated_at' =>Carbon::now()->toDateTimeString(),
       ]);
       \Illuminate\Support\Facades\DB::table('tbactivity_logs_history')->insert([
           'acType'=>'Manual Attendance',
           'details'=>"Salary has been processed for ".Carbon::parse($request->salary_sheet_month)->format('M Y'),
           'createdBy'=>Auth::user()->id,
           'created_at'=>Carbon::now()->toDateTimeString(),
           'updated_at'=>Carbon::now()->toDateTimeString(),
       ]);
       $delete_total_employee_leave=DB::table('total_employee_leave')->delete();
       $delete_total_employee_present=DB::table('tb_total_present')->delete();
       $delete_total_employee_overtime=DB::table('employee_overtime')->delete();
       $temp_overt=DB::table('temp_employee_overtime')->delete();
       $late_emp_delete=DB::table('emp_total_late')->delete();
       DB::table('regular_weekend_presence')->delete();
       DB::table('total_weekend_present')->delete();
       return redirect('report/payroll/employee/salary/sheet');
    }


    //id card wise process report
    public function id_card_process_salary_report(){
        return view('report.payroll.salarysheet.card_salary');
    }

    //id card wise salary monthwise show
    public function salary_month_wise_id_card_show(Request $request){
        $dt= $request->emp_sal_month."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();

        if(isset($_POST['salary_month_wise_excel'])){
            $count_data=DB::table('tb_salary_history')
                ->select('tb_salary_history.month')
                ->where('dates',$request->emp_sal_month)
                ->count();
            if($count_data>0){
                $payment_date=$request->salary_payment_date;
                $months=$request->emp_sal_month;
                $ye=date('Y',strtotime($request->emp_sal_month));
                $mo=date('m',strtotime($request->emp_sal_month));
                $monthname=DB::table('tb_salary_history')
                    ->select('tb_salary_history.month')
                    ->where('dates',$months)
                    ->first();
                $department=DB::table('tb_salary_history')
                    ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
                    ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                    ->leftjoin('tblines','employees.line_id','=','tblines.id')
                    ->select('departments.departmentName','employees.empSection','tb_salary_history.department_id','tblines.line_no')
                    ->groupBy('tb_salary_history.department_id')
                    ->get();
                Excel::create('employee_salary_sheet',function($excel) use($department,$monthname,$months,$ye,$mo,$payment_date){
                    $excel->setTitle('Emp Data');
                    $excel->sheet('Emp Data',function($sheet) use($department,$monthname,$months,$ye,$mo,$payment_date){
                        $sheet->loadView('report.excel.salary_sheet_show')
                            ->with('department',$department)
                            ->with('months',$months)
                            ->with('monthname',$monthname)
                            ->with('ye',$ye)
                            ->with('mo',$mo)
                            ->with('payment_date',$payment_date);
                    });
                })->download('xlsx');
                return view('report.excel.salary_sheet_show_card_wise',compact('data','monthname','department','months','payment_date'));
            }else{
                echo "NO data";
            }
        }
        $count_data=DB::table('tb_salary_history')
            ->select('tb_salary_history.month')
            ->where('dates',$request->emp_sal_month)
            ->count();
       if($count_data>0){
           $payment_date=$request->salary_payment_date;
           $months=$request->emp_sal_month;
           $ye=date('Y',strtotime($request->emp_sal_month));
           $mo=date('m',strtotime($request->emp_sal_month));
           $monthname=DB::table('tb_salary_history')
               ->select('tb_salary_history.month')
               ->where('dates',$months)
               ->first();
           $department=DB::table('tb_salary_history')
               ->leftjoin('departments','tb_salary_history.department_id','=','departments.id')
               ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
               ->leftjoin('tblines','employees.line_id','=','tblines.id')
               ->select('departments.departmentName','employees.empSection','tb_salary_history.department_id','tblines.line_no')
               ->groupBy('tb_salary_history.department_id')
               ->get();
           return view('report.payroll.salary_sheet_show_card_wise',compact('monthname','dt','department','months','ye','mo','payment_date'));
       }else{
           return redirect()->back()->with('msg', 'No Data Found');
       }
    }



     //lefty employee salary summery
     public function salarySummeryReportLefty(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $data=DB::SELECT("SELECT YEAR(month) as years FROM tb_salary_history GROUP BY YEAR(month)");
        return view('report.payroll.salary_summary_lefty',compact('department','section','data'));
    }

    //lefty employee salary summery show
    public function salarySummeryReportLeftyShow(Request $request){
        if($request->department_id=='all_department'){
            if($request->dept_excel=='dept_excel'){
              $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
              $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
              $dt= $request->salary_summery_monthsss."-28";
              $dt= Carbon::parse($dt)->endOfMonth();
              $dt= Carbon::parse($dt)->toDateString();
              $month=$request->salary_summery_monthsss;
              $all_dept='all_dept';
              $data=DB::table('tb_salary_history')
              ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
              ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
              ->leftjoin('tblines','employees.line_id','=','tblines.id')
              ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
              ->where('tb_salary_history.dates',$month)
              ->whereBetween('employees.date_of_discontinuation',array($s,$e))
              ->where('employees.discon_type','=',1)
              ->where('employees.empJoiningDate','<=',$dt)
              ->groupBy('tb_salary_history.department_id')
              ->get();
              Excel::create('employee_salary_summary_lefty',function($excel) use($data,$month,$all_dept){
                $excel->setTitle('Emp Data');
                $excel->sheet('Emp Data',function($sheet) use($data,$month,$all_dept){
                    $sheet->loadView('report.payroll.salarysheet.excel.salary_summery_lefty_excel')
                        ->with('data',$data)
                        ->with('month',$month)
                        ->with('all_dept',$all_dept);
                });
            })->download('xlsx');

           }
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $all_dept='all_dept';
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',1)
            ->where('employees.empJoiningDate','<=',$dt)
            ->groupBy('tb_salary_history.department_id')
            ->get();
            return view('report.payroll.salary_summary_lefty_data',compact('data','month','all_dept'));
          }
          if($request->type=='department_wise_data'){
            if($request->dept_excel=='dept_excel'){
                $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                $dt= $request->salary_summery_monthsss."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $month=$request->salary_summery_monthsss;
                $dept_id=$request->department_id;
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                ->where('tb_salary_history.dates',$month)
                ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                ->where('employees.discon_type','=',1)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.department_id','=',$dept_id)
                ->get();
                Excel::create('employee_salary_summary_lefty',function($excel) use($data,$month,$dept_id){
                    $excel->setTitle('Emp Data');
                    $excel->sheet('Emp Data',function($sheet) use($data,$month,$dept_id){
                        $sheet->loadView('report.payroll.salarysheet.excel.salary_summery_lefty_excel')
                            ->with('data',$data)
                            ->with('month',$month)
                            ->with('dept_id',$dept_id);
                    });
                })->download('xlsx');

              }
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $dept_id=$request->department_id;
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',1)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.department_id','=',$dept_id)
            ->get();
            return view('report.payroll.salary_summary_lefty_data',compact('data','month','dept_id'));
          }

          if($request->section_id=='all_section'){
              if($request->section_excel=='section_excel'){
                $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                $dt= $request->salary_summery_monthsss."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $month=$request->salary_summery_monthsss;
                $all_section='all_section';
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                ->where('tb_salary_history.dates',$month)
                ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                ->where('employees.discon_type','=',1)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.status','=',0)
                ->groupBy('tb_salary_history.section_id')
                ->get();
                Excel::create('employee_salary_summary_lefty',function($excel) use($data,$month,$all_section){
                    $excel->setTitle('Emp Data');
                    $excel->sheet('Emp Data',function($sheet) use($data,$month,$all_section){
                        $sheet->loadView('report.payroll.salarysheet.excel.salary_summery_lefty_excel')
                            ->with('data',$data)
                            ->with('month',$month)
                            ->with('all_section',$all_section);
                    });
                })->download('xlsx');
              }
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $all_section='all_section';
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',1)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.status','=',0)
            ->groupBy('tb_salary_history.section_id')
            ->get();
            return view('report.payroll.salary_summary_lefty_data',compact('data','month','all_section'));
          }

          if($request->type=='section_wise_data'){
            if($request->section_excel=='section_excel'){
                $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $section_id=$request->section_id;
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',1)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.section_id','=',$section_id)
            ->where('tb_salary_history.status','=',0)
            ->get();
            Excel::create('employee_salary_summary_lefty',function($excel) use($data,$month,$section_id){
                $excel->setTitle('Emp Data');
                $excel->sheet('Emp Data',function($sheet) use($data,$month,$section_id){
                    $sheet->loadView('report.payroll.salarysheet.excel.salary_summery_lefty_excel')
                        ->with('data',$data)
                        ->with('month',$month)
                        ->with('section_id',$section_id);
                });
            })->download('xlsx');
            }   
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $section_id=$request->section_id;
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',1)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.section_id','=',$section_id)
            ->where('tb_salary_history.status','=',0)
            ->get();
            return view('report.payroll.salary_summary_lefty_data',compact('data','month','section_id'));
          }


            if($request->monthwise_excel='monthwise_excel'){
                if($request->month_excel=='month_excel')
                {
                    $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                    $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                    $dt= $request->salary_summery_monthsss."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $month=$request->salary_summery_monthsss;
                    $monthly_data='Monthly';
                    $data=DB::table('tb_salary_history')
                    ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                    ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                    ->leftjoin('tblines','employees.line_id','=','tblines.id')
                    ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                    ->where('tb_salary_history.dates',$month)
                    ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                    ->where('employees.discon_type','=',1)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->groupBy('tb_salary_history.department_id')
                    ->get();
                Excel::create('employee_salary_summary_lefty',function($excel) use($data,$month,$monthly_data){
                        $excel->setTitle('Emp Data');
                        $excel->sheet('Emp Data',function($sheet) use($data,$month,$monthly_data){
                            $sheet->loadView('report.payroll.salarysheet.excel.salary_summery_lefty_excel')
                                ->with('data',$data)
                                ->with('month',$month)
                                ->with('monthly_data',$monthly_data);
                        });
                    })->download('xlsx');
                }
                $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                $dt= $request->salary_summery_monthsss."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $month=$request->salary_summery_monthsss;
                $monthly_data='Monthly';
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                ->where('tb_salary_history.dates',$month)
                ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                ->where('employees.discon_type','=',1)
                ->where('employees.empJoiningDate','<=',$dt)
                ->groupBy('tb_salary_history.department_id')
                ->get();
                return view('report.payroll.salary_summary_lefty_data',compact('data','month','monthly_data'));
            }
    }


     //resigned employee salary summery
       public function salarySummeryReportResigned(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        return view('report.payroll.salary_summary_resigned',compact('department','section'));
    }

     //resigned employee salary summery show
     public function salarySummeryReportResignedShow(Request $request){
        if($request->department_id=='all_department'){
            if($request->dept_excel=='dept_excel'){
                $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                $dt= $request->salary_summery_monthsss."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $month=$request->salary_summery_monthsss;
                $all_dept='all_dept';
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                ->where('tb_salary_history.dates',$month)
                ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                ->where('employees.discon_type','=',2)
                ->where('employees.empJoiningDate','<=',$dt)
                ->groupBy('tb_salary_history.department_id')
                ->get();
                Excel::create('employee_salary_summary_resigned',function($excel) use($data,$month,$all_dept){
                    $excel->setTitle('Emp Data');
                    $excel->sheet('Emp Data',function($sheet) use($data,$month,$all_dept){
                        $sheet->loadView('report.payroll.salarysheet.excel.salary_summery_resigned_excel')
                            ->with('data',$data)
                            ->with('month',$month)
                            ->with('all_dept',$all_dept);
                    });
                })->download('xlsx');
            }
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $all_dept='all_dept';
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',2)
            ->where('employees.empJoiningDate','<=',$dt)
            ->groupBy('tb_salary_history.department_id')
            ->get();
            return view('report.payroll.salary_summary_resigned_data',compact('data','month','all_dept'));
          }
          if($request->type=='department_wise_data'){
            if($request->dept_excel=='dept_excel'){
                $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                $dt= $request->salary_summery_monthsss."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $month=$request->salary_summery_monthsss;
                $dept_id=$request->department_id;
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                ->where('tb_salary_history.dates',$month)
                ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                ->where('employees.discon_type','=',2)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.department_id','=',$dept_id)
                ->get();
                Excel::create('employee_salary_summary_resigned',function($excel) use($data,$month,$dept_id){
                    $excel->setTitle('Emp Data');
                    $excel->sheet('Emp Data',function($sheet) use($data,$month,$dept_id){
                        $sheet->loadView('report.payroll.salarysheet.excel.salary_summery_resigned_excel')
                            ->with('data',$data)
                            ->with('month',$month)
                            ->with('dept_id',$dept_id);
                    });
                })->download('xlsx');
            }
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $dept_id=$request->department_id;
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',2)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.department_id','=',$dept_id)
            ->get();
            return view('report.payroll.salary_summary_resigned_data',compact('data','month','dept_id'));
          }

          if($request->section_id=='all_section'){
            if($request->section_excel=='section_excel'){
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $all_section='all_section';
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',2)
            ->where('employees.empJoiningDate','<=',$dt)
            ->groupBy('tb_salary_history.section_id')
            ->get();
            Excel::create('employee_salary_summary_resigned',function($excel) use($data,$month,$all_section){
                $excel->setTitle('Emp Data');
                $excel->sheet('Emp Data',function($sheet) use($data,$month,$all_section){
                    $sheet->loadView('report.payroll.salarysheet.excel.salary_summery_resigned_excel')
                        ->with('data',$data)
                        ->with('month',$month)
                        ->with('all_section',$all_section);
                });
            })->download('xlsx');
            }
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $all_section='all_section';
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',2)
            ->where('employees.empJoiningDate','<=',$dt)
            ->groupBy('tb_salary_history.section_id')
            ->get();
            return view('report.payroll.salary_summary_resigned_data',compact('data','month','all_section'));
          }

          if($request->type=='section_wise_data'){
            if($request->section_excel=='section_excel'){
                $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                $dt= $request->salary_summery_monthsss."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $month=$request->salary_summery_monthsss;
                $section_id=$request->section_id;
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                ->where('tb_salary_history.dates',$month)
                ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                ->where('employees.discon_type','=',2)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history.section_id','=',$section_id)
                ->get();
                Excel::create('employee_salary_summary_resigned',function($excel) use($data,$month,$section_id){
                    $excel->setTitle('Emp Data');
                    $excel->sheet('Emp Data',function($sheet) use($data,$month,$section_id){
                        $sheet->loadView('report.payroll.salarysheet.excel.salary_summery_resigned_excel')
                            ->with('data',$data)
                            ->with('month',$month)
                            ->with('section_id',$section_id);
                    });
                })->download('xlsx');
            }
            $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
            $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
            $dt= $request->salary_summery_monthsss."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $month=$request->salary_summery_monthsss;
            $section_id=$request->section_id;
            $data=DB::table('tb_salary_history')
            ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
            ->where('tb_salary_history.dates',$month)
            ->whereBetween('employees.date_of_discontinuation',array($s,$e))
            ->where('employees.discon_type','=',2)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history.section_id','=',$section_id)
            ->get();
            return view('report.payroll.salary_summary_resigned_data',compact('data','month','section_id'));
          }


            if($request->monthwiseexcel=='monthwiseexcel'){
                if($request->month_excel=='month_excel'){
                    $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                    $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                    $dt= $request->salary_summery_monthsss."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $month=$request->salary_summery_monthsss;
                    $monthly_data='Monthly';
                    $data=DB::table('tb_salary_history')
                    ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                    ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                    ->leftjoin('tblines','employees.line_id','=','tblines.id')
                    ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                    ->where('tb_salary_history.dates',$month)
                    ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                    ->where('employees.discon_type','=',2)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->groupBy('tb_salary_history.department_id')
                    ->get();
                    Excel::create('employee_salary_summary_resigned',function($excel) use($data,$month,$monthly_data){
                        $excel->setTitle('Emp Data');
                        $excel->sheet('Emp Data',function($sheet) use($data,$month,$monthly_data){
                            $sheet->loadView('report.payroll.salarysheet.excel.salary_summery_resigned_excel')
                                ->with('data',$data)
                                ->with('month',$month)
                                ->with('monthly_data',$monthly_data);
                        });
                    })->download('xlsx');
                }
                $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                $dt= $request->salary_summery_monthsss."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $month=$request->salary_summery_monthsss;
                $monthly_data='Monthly';
                $data=DB::table('tb_salary_history')
                ->leftjoin('employees','tb_salary_history.emp_id','=','employees.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','tb_salary_history.section_id',DB::raw('count(tb_salary_history.emp_id) as worker'),DB::raw('SUM(tb_salary_history.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history.overtime) as overtimes'),DB::raw('SUM(tb_salary_history.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history.basic_salary) as basic'),DB::raw('SUM(tb_salary_history.house_rant) as house'),DB::raw('SUM(tb_salary_history.medical) as medicals'),DB::raw('SUM(tb_salary_history.transport) as transports'),DB::raw('SUM(tb_salary_history.food) as foods'),DB::raw('SUM(tb_salary_history.gross) as gorss'),DB::raw('SUM(tb_salary_history.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history.net_amount) as net_total'),DB::raw('SUM(tb_salary_history.attendance_bonus) as attendance_bonus'))
                ->where('tb_salary_history.dates',$month)
                ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                ->where('employees.discon_type','=',2)
                ->where('employees.empJoiningDate','<=',$dt)
                ->groupBy('tb_salary_history.department_id')
                ->get();
                return view('report.payroll.salary_summary_resigned_data',compact('data','month','monthly_data'));
            }
      }
	  
	  
	  
	  
	   public function ttt(){
        // $del_year=date('Y',strtotime($request->salary_sheet_month));
        // $del_month=date('m',strtotime($request->salary_sheet_month));

        $delete=DB::SELECT("SELECT attendance.id as att_id FROM leave_of_employee LEFT JOIN attendance ON leave_of_employee.emp_id=attendance.emp_id WHERE attendance.date BETWEEN leave_of_employee.start_date AND leave_of_employee.end_date AND YEAR(leave_of_employee.month)='2020' AND MONTH(leave_of_employee.month)='02'");       
        $delete_id = [];
        foreach ($delete as $key => $value) {
            $delete_id[]=$value->att_id;
            // dd($value->att_id);
        }
        
         $ok = DB::table('attendance')->whereIn('id', $delete_id)->delete();
        
        if ($ok) {
            dd('delete success');
        }
        dd($delete);
      }

	  
	  
	  
	  
	  

}
