<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use redirect;
use Session;
use Carbon\Carbon;
class DepartmentWiseShift extends Controller
{
    //multiple employee shift view
    public function multiple_emp_shift_data(Request $request){
        $department=DB::table('departments')
        ->select('id','departmentName')
        ->get();
        $section=DB::table('employees')
        ->select('empSection')
        ->where('empSection','!=',NULL)
        ->groupBy('empSection')
        ->get();
        // $employee=DB::table('employees')->select('id','empFirstName','employeeId')->get();
        $shift=DB::table('attendance_setup')->get();
        return view('employee.multiple_shift',compact('department','section','shift'));
    }

    //assign shift list
    public function assign_shift_list(Request $request){
        $start=date('Y-m-d',strtotime($request->start));
        $end=date('Y-m-d',strtotime($request->end));
        $list_dept=DB::table('shift_history')
        ->leftjoin('departments','shift_history.dept_id','=','departments.id')
        ->leftjoin('attendance_setup','shift_history.shift_id','=','attendance_setup.id')
        ->select('shift_history.created_at','shift_history.process_status','departmentName','shiftName','start_date','end_date','shift_history.max_entry as m_entry','shift_history.break_time as b_time','shift_history.exit_time as e_time','shift_history.dept_id')
        ->where('dept_id','!=',NULL)
        ->whereBetween('start_date',[$start,$end])
        ->whereBetween('end_date',[$start,$end])
        ->groupBy('dept_id','shift_id','start_date')
        ->orderBy('start_date','ASC')
        ->get();
        $list_section=DB::table('shift_history')
        ->leftjoin('attendance_setup','shift_history.shift_id','=','attendance_setup.id')
        ->select('shift_history.created_at','shift_history.process_status','shift_history.section_id','shiftName','start_date','end_date','shift_history.max_entry as m_entry','shift_history.break_time as b_time','shift_history.exit_time as e_time')
        ->where('section_id','!=',NULL)
        ->whereBetween('start_date',[$start,$end])
        ->whereBetween('end_date',[$start,$end])
        ->groupBy('section_id','shift_id','start_date')
        ->orderBy('start_date','ASC')
        ->get();
        // $list_employee=DB::table('shift_history')
        // ->leftjoin('attendance_setup','shift_history.shift_id','=','attendance_setup.id')
        // ->leftjoin('employees','shift_history.emp_id','=','employees.id')
        // ->select('shift_history.emp_id','empFirstName','employeeId','shiftName','start_date','end_date','shift_history.max_entry as m_entry','shift_history.break_time as b_time','shift_history.exit_time as e_time')
        // ->get();
        return view('employee.shift_list',compact('list_dept','list_section'));
    }

    //employee shift
    public function employee_shift(){
        $department=DB::table('departments')
        ->select('id','departmentName')
        ->get();
        $section=DB::table('employees')
        ->select('empSection')
        ->where('empSection','!=',NULL)
        ->groupBy('empSection')
        ->get();
        // $employee=DB::table('employees')->select('id','empFirstName','employeeId')->get();
        $shift=DB::table('attendance_setup')->get();
        return view('employee.employee_shift',compact('department','section','shift'));
    }

    //shift update department
    public function employee_shift_dept(Request $request){
      $start=date('Y-m-01');  
      $end=date('Y-m-t');  
      $check=DB::table('shift_history')
      ->where('dept_id',$request->dept_id)
      ->whereBetween('start_date',[$start,$end])
      ->whereBetween('end_date',[$start,$end])
      ->count();
      if($check>0){
        DB::table('shift_history')
        ->where('dept_id',$request->dept_id)
        ->whereBetween('start_date',[$start,$end])
        ->whereBetween('end_date',[$start,$end])
        ->delete();
      }
      DB::table('employees')->where('empDepartmentId',$request->dept_id)->update([
        'empShiftId' =>$request->shift_id
      ]);
        $data=DB::table('employees')->where('empDepartmentId',$request->dept_id)->select('id')->get();
        $shift=DB::table('attendance_setup')->where('id',$request->shift_id)->first();
        $update_id = [];
        $history_data = [];
        foreach ($data as $key => $value) {
            $data=[
                'shift_id'=>$request->shift_id,
                'emp_id'=>$value->id,
                'max_entry'=>$shift->max_entry_time,
                'break_time'=>$shift->break_time,
                'exit_time'=>$shift->exit_time,
                'start_date'=>$start,
                'end_date'=>$end,
                'dept_id' =>$request->dept_id,
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
            $update_id[]=$value->id;
            $history_data[]=$data;
         } 
         $history_data = collect($history_data); 
         $chunks = $history_data->chunk(3000);
         foreach ($chunks as $chunk)
         {
            \DB::table('shift_history')->insert($chunk->toArray());
         }
         DB::table('attendance')
         ->whereIn('emp_id',$update_id)
         ->whereBetween('date',[$start,$end])
         ->update([
             'max_entry'=>$shift->max_entry_time,
             'exit_times'=>$shift->exit_time,
         ]);
      Session::flash('success','Shift has been Update Successfully');
      return redirect()->back();
    }

    //shift update section
    public function employee_shift_section(Request $request){
        $start=date('Y-m-01');  
        $end=date('Y-m-t'); 
        $check=DB::table('shift_history')
        ->where('section_id',$request->section_id)
        ->whereBetween('start_date',[$start,$end])
        ->whereBetween('end_date',[$start,$end])
        ->count();
        if($check>0){
            DB::table('shift_history')
            ->where('section_id',$request->section_id)
            ->whereBetween('start_date',[$start,$end])
            ->whereBetween('end_date',[$start,$end])
            ->delete();
        }
         DB::table('employees')->where('empSection',$request->section_id)->update([
            'empShiftId' =>$request->shift_id
         ]);   
          $data=DB::table('employees')->where('empSection',$request->section_id)->select('id')->get();
          $shift=DB::table('attendance_setup')->where('id',$request->shift_id)->first();
          $update_id = [];
          $history_data = [];
          foreach ($data as $key => $value) {
              $data=[
                  'shift_id'=>$request->shift_id,
                  'emp_id'=>$value->id,
                  'max_entry'=>$shift->max_entry_time,
                  'break_time'=>$shift->break_time,
                  'exit_time'=>$shift->exit_time,
                  'start_date'=>$start,
                  'end_date'=>$end,
                  'section_id' =>$request->section_id,
                  'created_at' =>Carbon::now()->toDateTimeString(),
                  'updated_at' =>Carbon::now()->toDateTimeString(),
              ];
              $update_id[]=$value->id;
              $history_data[]=$data;
           } 
           $history_data = collect($history_data); 
           $chunks = $history_data->chunk(3000);
           foreach ($chunks as $chunk)
           {
              \DB::table('shift_history')->insert($chunk->toArray());
           }
           DB::table('attendance')
           ->whereIn('emp_id',$update_id)
           ->whereBetween('date',[$start,$end])
           ->update([
               'max_entry'=>$shift->max_entry_time,
               'exit_times'=>$shift->exit_time,
           ]);
        Session::flash('success','Shift has been Update Successfully');
        return redirect()->back();
    }


    //shift process department
    public function emp_shift_process_dept($id,$start,$end,$m_time,$e_time){
       $data=DB::table('employees')->where('empDepartmentId',$id)->select('id')->get();
       $update_id = [];
       foreach ($data as $key => $value) {
           $update_id[]=$value->id;
        } 
       DB::table('attendance')
       ->whereIn('emp_id',$update_id)
       ->whereBetween('date',[$start,$end])
       ->update([
           'max_entry'=>$m_time,
           'exit_times'=>$e_time,
       ]);
       DB::table('shift_history')
       ->where('dept_id',$id)
       ->whereBetween('start_date',[$start,$end])
       ->whereBetween('end_date',[$start,$end])
       ->update([
           'process_status'=>1,
           'created_at' =>Carbon::now()->toDateTimeString(),
           'updated_at' =>Carbon::now()->toDateTimeString(),
       ]);
       Session::flash('success','Process has been completed successfullly');
       return redirect()->back(); 
    }

    //shift process department
    public function emp_shift_process_section($id,$start,$end,$m_time,$e_time){
        $data=DB::table('employees')->where('empSection',$id)->select('id')->get();

        $update_id = [];
        foreach ($data as $key => $value) {
            $update_id[]=$value->id;
         } 
        DB::table('attendance')
        ->whereIn('emp_id',$update_id)
        ->whereBetween('date',[$start,$end])
        ->update([
            'max_entry'=>$m_time,
            'exit_times'=>$e_time,
        ]);
        DB::table('shift_history')
        ->where('section_id',$id)
        ->whereBetween('start_date',[$start,$end])
        ->whereBetween('end_date',[$start,$end])
        ->update([
            'process_status'=>1,
            'created_at' =>Carbon::now()->toDateTimeString(),
            'updated_at' =>Carbon::now()->toDateTimeString(),
        ]);
        Session::flash('success','Process has been completed successfullly');
        return redirect()->back(); 
    }

    //shift update multiple employee department wise
    public function multiple_emp_shift_data_update(Request $request){
        $start=date('Y-m-d',strtotime($request->start_date));
        $end=date('Y-m-d',strtotime($request->end_date));
        $check=DB::table('shift_history')
        ->where('dept_id',$request->dept_id)
        ->whereBetween('start_date',[$start,$end])
        ->whereBetween('end_date',[$start,$end])
        ->count();
        if($check>0){
            Session::flash('error','Shift already assigned');
            return redirect()->back()->withInput();
        }
        $data=DB::table('employees')->where('empDepartmentId',$request->dept_id)->select('id')->get();
        $shift=DB::table('attendance_setup')->where('id',$request->shift_id)->first();
        $update_id = [];
        $history_data = [];
        foreach ($data as $key => $value) {
            $data=[
                'shift_id'=>$request->shift_id,
                'emp_id'=>$value->id,
                'max_entry'=>$shift->max_entry_time,
                'break_time'=>$shift->break_time,
                'exit_time'=>$shift->exit_time,
                'start_date'=>$start,
                'end_date'=>$end,
                'dept_id' =>$request->dept_id,
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
            $update_id[]=$value->id;
            $history_data[]=$data;
         } 
         $history_data = collect($history_data); 
         $chunks = $history_data->chunk(3000);
         foreach ($chunks as $chunk)
         {
            \DB::table('shift_history')->insert($chunk->toArray());
         }
        DB::table('attendance')
        ->whereIn('emp_id',$update_id)
        ->whereBetween('date',[$start,$end])
        ->update([
            'max_entry'=>$shift->max_entry_time,
            'exit_times'=>$shift->exit_time,
        ]);
        Session::flash('success','Shift has been assigned successfullly');
        return redirect()->back()->withInput();
    }

     //shift update multiple employee section wise
     public function multiple_emp_shift_data_update_section_wise(Request $request){
        $start=date('Y-m-d',strtotime($request->start_dates));
        $end=date('Y-m-d',strtotime($request->end_dates));
        $check=DB::table('shift_history')
        ->where('section_id',$request->section_ids)
        ->whereBetween('start_date',[$start,$end])
        ->whereBetween('end_date',[$start,$end])
        ->count();
        if($check>0){
            Session::flash('error','Shift already assigned');
            return redirect()->back()->withInput();
        }
        $data=DB::table('employees')->where('empSection',$request->section_ids)->select('id')->get();
        $shift=DB::table('attendance_setup')->where('id',$request->shift_ids)->first();
        $update_id = [];
        $history_data = [];
        foreach ($data as $key => $value) {
            $data=[
                'shift_id'=>$request->shift_ids,
                'emp_id'=>$value->id,
                'max_entry'=>$shift->max_entry_time,
                'break_time'=>$shift->break_time,
                'exit_time'=>$shift->exit_time,
                'start_date'=>$start,
                'end_date'=>$end,
                'section_id' =>$request->section_ids,
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
            $update_id[]=$value->id;
            $history_data[]=$data;
         } 
         $history_data = collect($history_data); 
         $chunks = $history_data->chunk(3000);
         foreach ($chunks as $chunk)
         {
            \DB::table('shift_history')->insert($chunk->toArray());
         }
        DB::table('attendance')
        ->whereIn('emp_id',$update_id)
        ->whereBetween('date',[$start,$end])
        ->update([
            'max_entry'=>$shift->max_entry_time,
            'exit_times'=>$shift->exit_time,
        ]);
        Session::flash('success','Shift has been assigned successfullly');
        return redirect()->back()->withInput();
    }

     //shift update multiple employee section wise
    //  public function multiple_emp_shift_data_update_emp_wise(Request $request){
    //     $start=date('Y-m-d',strtotime($request->start_datess));
    //     $end=date('Y-m-d',strtotime($request->end_datess));
    //     $check=DB::table('shift_history')
    //     ->where('emp_id',$request->emp_idss)
    //     ->whereBetween('start_date',[$start,$end])
    //     ->whereBetween('end_date',[$start,$end])
    //     ->count();
    //     if($check>0){
    //         Session::flash('error','Shift already assigned');
    //         return redirect()->back()->withInput();
    //     }
    //     $data=DB::table('employees')->where('id',$request->emp_idss)->select('id','empDepartmentId','empSection')->get();
    //     $shift=DB::table('attendance_setup')->where('id',$request->shift_idss)->first();
    //     $update_id = [];
    //     $history_data = [];
    //     foreach ($data as $key => $value) {
    //         $data=[
    //             'shift_id'=>$request->shift_idss,
    //             'emp_id'=>$value->id,
    //             'max_entry'=>$shift->max_entry_time,
    //             'exit_time'=>$shift->exit_time,
    //             'break_time'=>$shift->break_time,
    //             'start_date'=>$start,
    //             'end_date'=>$end,
    //             'dept_id' =>$value->empDepartmentId,
    //             'section_id' =>$value->empSection,
    //             'created_at' =>Carbon::now()->toDateTimeString(),
    //             'updated_at' =>Carbon::now()->toDateTimeString(),
    //         ];
    //         $update_id[]=$value->id;
    //         $history_data[]=$data;
    //      } 
    //      $history_data = collect($history_data); 
    //      $chunks = $history_data->chunk(3000);
    //      foreach ($chunks as $chunk)
    //      {
    //         \DB::table('shift_history')->insert($chunk->toArray());
    //      }
    //     DB::table('employees')->whereIn('id', $update_id)->update([
    //       'empShiftId' =>$request->shift_idss
    //     ]); 
    //     DB::table('attendance')
    //     ->whereIn('emp_id',$update_id)
    //     ->whereBetween('date',[$start,$end])
    //     ->update([
    //         'max_entry'=>$shift->max_entry_time,
    //         'exit_times'=>$shift->exit_time,
    //     ]);
    //     Session::flash('success','Shift has been assigned successfullly');
    //     return redirect()->back()->withInput();
    // }


    //assign shift delete
    public function assign_shift_delete_dept($id,$start,$end){
      DB::table('shift_history')
      ->where('dept_id',$id)
      ->whereBetween('start_date',[$start,$end])
      ->whereBetween('end_date',[$start,$end])
      ->delete();
      $data=DB::table('employees')->where('empDepartmentId',$id)->select('id')->get();
      $update_id = [];
      foreach ($data as $key => $value) {
          $update_id[]=$value->id;
       } 
       DB::table('attendance')
       ->whereIn('emp_id',$update_id)
       ->whereBetween('date',[$start,$end])
       ->update([
           'max_entry'=>'',
           'exit_times'=>'',
       ]);
       Session::flash('success','Shift has been deleted successfullly');
       return redirect()->back()->withInput();
    }

    //assign shift delete section 
    public function assign_shift_delete_section($id,$start,$end){
      DB::table('shift_history')
      ->where('section_id',$id)
      ->whereBetween('start_date',[$start,$end])
      ->whereBetween('end_date',[$start,$end])
      ->delete();
      $data=DB::table('employees')->where('empSection',$id)->select('id')->get();
      $update_id = [];
      foreach ($data as $key => $value) {
          $update_id[]=$value->id;
       } 
       DB::table('attendance')
       ->whereIn('emp_id',$update_id)
       ->whereBetween('date',[$start,$end])
       ->update([
           'max_entry'=>'',
           'exit_times'=>'',
       ]);
       Session::flash('success','Shift has been deleted successfullly');
       return redirect()->back()->withInput();
    }

    //department wise holiday
    public function dept_wise_holiday(Request $request){
        $holi_date=date('Y-m-d',strtotime($request->holi_date));
        $holi_month=date('Y-m-01',strtotime($request->holi_date));
        $department=DB::table('departments')->where('id',$request->dept_id)->select('departmentName')->first();
        $del=DB::table('emp_wise_holiday')->where('dept',$department->departmentName)->where('holiday_date',$holi_date)->count();
        if($del>0){
            $del=DB::table('emp_wise_holiday')->where('dept',$department->departmentName)->where('holiday_date',$holi_date)->delete();
        }
        $data=DB::table('employees')
        ->where('empDepartmentId',$request->dept_id)
        ->select('id')
        ->get();
        $total_data=[];
        $emp_id = [];
        foreach ($data as $key => $value) {
            $emp_id[]=$value->id;
            $data=[
                'emp_id' => $emp_id[$key],
                'dept' =>$department->departmentName,
                'holiday_date' =>$holi_date,
                'holiday_end_date' =>$holi_date,
                'holiday_month' =>$holi_month,
                'perpose' =>$request->perpose,
                'created_by' =>auth()->user()->id
            ];    
           $total_data[]=$data;
        } 
        $total_data = collect($total_data);
        $total_data = $total_data->chunk(500);
        foreach ($total_data as $total_datas)
        {
         \DB::table('emp_wise_holiday')->insert($total_datas->toArray());
        }
        Session::flash('success','Holiday has been assigned successfullly');
        return redirect()->back();

    } 

    //section wise holiday
    public function section_wise_holiday(Request $request){
        $holi_date=date('Y-m-d',strtotime($request->holi_date));
        $holi_month=date('Y-m-01',strtotime($request->holi_date));
        $del=DB::table('emp_wise_holiday')->where('section',$request->section_id)->where('holiday_date',$holi_date)->count();
        if($del>0){
            $del=DB::table('emp_wise_holiday')->where('section',$request->section_id)->where('holiday_date',$holi_date)->delete();
        }
        $data=DB::table('employees')
        ->where('empSection',$request->section_id)
        ->select('id')
        ->get();
        $total_data=[];
        $emp_id = [];
        foreach ($data as $key => $value) {
            $emp_id[]=$value->id;
            $data=[
                'emp_id' => $emp_id[$key],
                'section' =>$request->section_id,
                'holiday_date' =>$holi_date,
                'holiday_end_date' =>$holi_date,
                'holiday_month' =>$holi_month,
                'perpose' =>$request->perpose,
                'created_by' =>auth()->user()->id
            ];    
           $total_data[]=$data;
        } 
        $total_data = collect($total_data);
        $total_data = $total_data->chunk(500);
        foreach ($total_data as $total_datas)
        {
         \DB::table('emp_wise_holiday')->insert($total_datas->toArray());
        }
        Session::flash('success','Holiday has been assigned successfullly');
        return redirect()->back();
    }

    //holiday delete department/Section wise
    public function dept_section_holiday_delete($medium,$date){
      $data=DB::table('emp_wise_holiday')
      ->where('dept',$medium)
      ->where('holiday_date',$date)
      ->delete();
      DB::table('emp_wise_holiday')
      ->where('section',$medium)
      ->where('holiday_date',$date)
      ->delete();
      Session::flash('success','Holiday has been deleted successfullly');
      return redirect()->back();
    }




}
