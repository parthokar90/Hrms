<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Redirect;
use Session;
use auth;

class noticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices=DB::table('tbnotices')
        ->leftJoin('users','users.id','=','tbnotices.createdBy')
        ->select('tbnotices.*','users.name as created_by')
        ->latest()->get();

        return view('notices.index',compact('notices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                
        $this->validate($request,[
            'startDate'=>'required',
            'endDate'=>'required',
            'noticeDescription'=>'required',
            'status'=>'required',
        ]);

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();


        $str = DB::table('tbnotices')->insert([
            'startDate'=>Carbon::parse($request->startDate)->format('Y-m-d'),
            'endDate'=>Carbon::parse($request->endDate)->format('Y-m-d'),
            'noticeDescription'=>$request->noticeDescription,
            'status'=>$request->status,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

       if($str)
        {
            Session::flash('message','Notice Information Successfully Inserted ');
        }else{
            Session::flash('failedMessage','Notice Information Insertion Failed.');
        }
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notices=DB::table('tbnotices')
        ->leftJoin('users','users.id','=','tbnotices.createdBy')
        ->select('tbnotices.*','users.name as created_by')
        ->where('tbnotices.id','=',$id)
        ->first();
        return view('notices.show',compact('notices'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                        
        $this->validate($request,[
            'startDate'=>'required',
            'endDate'=>'required',
            'noticeDescription'=>'required',
            'status'=>'required',
        ]);

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tbnotices')->where(['id'=>$id])->update([
            'startDate'=>Carbon::parse($request->startDate)->format('Y-m-d'),
            'endDate'=>Carbon::parse($request->endDate)->format('Y-m-d'),
            'noticeDescription'=>$request->noticeDescription,
            'status'=>$request->status,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);

       if($str)
        {
            Session::flash('message','Notice Information Successfully Updated ');
        }else{
            Session::flash('failedMessage','Notice Information Updation Failed.');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $dept=DB::table('tbnotices')->where(['id'=>$id])->delete();
            Session::flash('message','Notice Information Successfully Deleted.');
        return redirect()->back();
    }
}
