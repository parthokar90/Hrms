<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class HrmsActivationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('activation.login_form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $username=md5($request->username);
        $password=md5($request->password);
        $accessKey=md5($request->accessKey);
        $data=DB::table('tbhrms_activation')
        ->where([['trackId','=','access'],
                ['haOption','=',$username],
                ['haValue','=',$password],
                ['haSDate','=',$accessKey]])
        ->get();
        $access="Permitted";
        if(count($data)>0){
            return view('activation.activation_process',compact('access'));
        }else{
            Session::flash('afailed', 'Authentication Failed, Invalid Credentials !');
            return view('activation.login_form');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $current1 = Carbon::now();
        $current = $current1;

        $startDate = base64_encode($current->toDateString());
        $trialExpires = $current->addDays($request->numberOfDays);
        $endDate = base64_encode($trialExpires->toDateString());
        
        $status=DB::table('tbhrms_activation')->where(['trackId'=>'validity'])->update([
                'haSDate'=>$startDate,
                'haEDate'=>$endDate,
                'haOption'=>'Days',
                'haValue'=>$request->numberOfDays,
                'created_at'=>$current1,
                'updated_at'=>$current1,
            ]);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validity_form()
    {
        return view('activation.login_form');
    }
}
