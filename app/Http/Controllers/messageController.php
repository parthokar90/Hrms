<?php

namespace App\Http\Controllers;

use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class messageController extends Controller
{
    
    public static function sendSMS($contact, $msg) {
        // SMS Gateway of your Bulk SMS Account
        $smsGateway = "http://sms.surprisebd.com/smsapi";

        // API Key of your Bulk SMS Account
        $smsApiKey = 'C20012145a9badf0adcc28.42187327';
        
        // Sender ID of your Bulk SMS Account
        $smsSenderID = 'MawaTravels';

        $type = 'text';
        $contact = chop($contact, "+");

        $client = new \GuzzleHttp\Client();

        $response = $client->post($smsGateway, [
            \GuzzleHttp\RequestOptions::JSON => ['api_key' => $smsApiKey, 'type' => $type,
                'senderid' => $smsSenderID, 'contacts' => $contact, 'msg' => $msg]
        ]);
        return $response;
    }

    public function sent_sms(Request $request){

        $phone = $request->phoneNumber;
        $msg = $request->message;
        // messageController::sendSMS($phone, $msg);

        $cmsg = "SMS has been sent successfully to - ".$phone;
        Session::flash('notification', $cmsg);
            return back();
    }

    public function sent_email(Request $request){
        // return 
        
        $server_email = getenv('MAIL_USERNAME');
        $email = $request->emailAddress;
        $mail_body = $request->emailMessage;
        $subject = $request->emailSubject;
        $attachment = (isset($request['file']) ? $request->file('file') : null);
        try {
                // for($i=0;$i<100;$i++){

                    Mail::send([], [], function($message) use ($email, $server_email, $mail_body, $subject, $attachment){
                        $message
                            ->to($email)
                            ->from($server_email,"Far-East IT Solutions Ltd.")
                            ->subject($subject)
                            ->setBody($mail_body);
                        if($attachment != null)
                            $message->attach($attachment, ['mime' => $attachment->getClientMimeType(), 'as' => $attachment->getClientOriginalName()]);
                    });
                    
                // }

                $cmsg = "Email has been sent successfully to - ".$request->emailAddress;
                Session::flash('notification', $cmsg);
                return redirect()->back();  
        }
        catch (Exception $e) {
             $cmsg = "Something went wrong, Could not send email to ".$request->emailAddress;
                Session::flash('fnotification', $cmsg);
                return  redirect()->back(); 
        }
      

    }
    public function manual_sms(){
       return view('messages.manual_sms');
    }

    public function manual_email(){
       return view('messages.manual_email');
    }

    public function manual_bulk_sms_view(){

        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();
       return view('messages.manual_bulk_sms_view',compact('designations','departments','units'));
    }

    public function proccess_bulk_sms_view(Request $request){

        
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 AND employees.empPhone IS NOT NULL ";

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (!empty($request->unitId)) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->empSection)) {
            $query.=" AND employees.empSection='".$request->empSection."'";
        }
        
        $employees= DB::select(DB::raw($query));

       return view('messages.bulk_sms_employee_list',compact('employees'));
    }

    public function send_manual_bulk_sms(Request $request){
        $i=0;
        $myData = array();
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
           foreach($request->cid as $key=>$value)
            {
                $empId=$_POST['empId'][$value];

                $insert[] = [
                    'empId'=>$empId,
                    'message'=>$request->message,
                    'createdBy'=>$user->id,
                    'created_at'=>$now,
                    'updated_at'=>$now
                ];

                if($i%250==0){
                    if(!empty($insert)){
                        $insertData = DB::table('tbbulk_sms_history')->insert($insert);
                        unset($insert);
                    }
                }
                $i++;
                
                //   messageController::sendSMS($_POST['sendAbleEmployeePhone'][$value], $request->message);
                $myData[] =array("sendAbleEmployeeName"=>$_POST['sendAbleEmployeeName'][$value],"sendAbleEmployeePhone"=>$_POST['sendAbleEmployeePhone'][$value]);
            }


                if($i<250){
                    if(!empty($insert)){
                        $insertData = DB::table('tbbulk_sms_history')->insert($insert);
                        unset($insert);
                    }
                }
            

       return response()->json($myData);
    }
    
    public function test1(){
       return view('messages.aa');
        
    }

}
