<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Auth;
use redirect;
use Session;
use stdClass;
use PDF;
use Excel;
use mPDF;

class ThreeShiftSalaryReport extends Controller
{

 //holiday find function
 function holiday($d1,$d2){
    $date1=strtotime($d1);
    $date2=strtotime($d2);
    $interval1=1+round(abs($date1-$date2)/86400);
    $festivalLeave = DB::table('tb_festival_leave')->get();
    $dcount=0;
    foreach($festivalLeave as $fleave){
        if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
            $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
        }
        else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
            $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
        }
        else{
            continue;
        }
    }
    return $dcount;
}   

//salary report 3-shift view
  public function emp_salary_report(){
    $department =DB::table('departments')->select('id','departmentName')->get();
    $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
    $employee   =DB::table('employees')->where('is_three_shift',1)->select('id','empFirstName','empLastName','employeeId')->get();
    return view('three_shift_report.salary.salary_report',compact('department','section','employee'));
  }

  public function Monthwiseemployeesalarysheet(Request $request){
    $dt= $request->emp_sal_month."-28";
    $dt= Carbon::parse($dt)->endOfMonth();
    $dt= Carbon::parse($dt)->toDateString();

    if($request->type=='dept_wise_data'){
            if($request->salary_month_wise_excel_department=='salary_month_wise_excel_department'){
                $count_data=DB::table('tb_salary_history_shift')
                ->select('tb_salary_history_shift.month')
                ->where('dates',$request->emp_sal_month)
                ->count();
                if($count_data>0){
                $payment_date=$request->salary_payment_date;
                $months=$request->emp_sal_month;
                $ye=date('Y',strtotime($request->emp_sal_month));
                $mo=date('m',strtotime($request->emp_sal_month));
                $monthname=DB::table('tb_salary_history_shift')
                    ->select('tb_salary_history_shift.month')
                    ->where('dates',$months)
                    ->first();
                $department=DB::table('tb_salary_history_shift')
                    ->where('dates',$months)
                    ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
                    ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                    ->leftjoin('tblines','employees.line_id','=','tblines.id')
                    ->select('departments.departmentName','employees.empSection','tb_salary_history_shift.department_id','tblines.line_no','section_id')
                    ->where('tb_salary_history_shift.department_id',$request->department_id)
                    ->groupBy('tb_salary_history_shift.department_id')
                    ->orderBy('employees.employeeId', 'ASC')
                    ->get();
                    $excelName=time()."_Salary_Sheet";
                    Excel::create("$excelName", function($excel) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                        // Set the title
                        $name=Auth::user()->name;
                        $excel->setTitle("Employee Salary Sheet");
                        // Chain the setters
                        $excel->setCreator($name);
                        $excel->setDescription('Employee Salary Summary');
                        $excel->sheet('Sheet 1', function ($sheet) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                            $sheet->loadView('three_shift_report.salary.excel.salary_sheet_dept_excel')
                                ->with('monthname',$monthname)
                                ->with('dt',$dt)
                                ->with('department',$department)
                                ->with('months',$months)
                                ->with('ye',$ye)
                                ->with('mo',$mo)
                                ->with('payment_date',$payment_date);
                        });
                    })->download('xlsx');
            }else{
                return redirect()->back()->with('msg', 'No Data Found');
            }
          }
            $count_data=DB::table('tb_salary_history_shift')
            ->select('tb_salary_history_shift.month')
            ->where('dates',$request->emp_sal_month)
            ->count();
            if($count_data>0){
            $payment_date=$request->salary_payment_date;
            $months=$request->emp_sal_month;
            $ye=date('Y',strtotime($request->emp_sal_month));
            $mo=date('m',strtotime($request->emp_sal_month));
            $monthname=DB::table('tb_salary_history_shift')
                ->select('tb_salary_history_shift.month')
                ->where('dates',$months)
                ->first();
            $department=DB::table('tb_salary_history_shift')
                ->where('dates',$months)
                ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
                ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','employees.empSection','tb_salary_history_shift.department_id','tblines.line_no','section_id')
                ->where('tb_salary_history_shift.department_id',$request->department_id)
                ->groupBy('tb_salary_history_shift.department_id')
                ->orderBy('employees.employeeId', 'ASC')
                ->get();
            return view('three_shift_report.salary.salary_sheet_show',compact('monthname','dt','department','months','ye','mo','payment_date'));
        }else{
            return redirect()->back()->with('msg', 'No Data Found');
        }
    }

    if($request->type=='section_wise_data'){
        if($request->salary_month_wise_excel_section=='salary_month_wise_excel_section'){
            $count_data=DB::table('tb_salary_history_shift')
            ->select('tb_salary_history_shift.month')
            ->where('dates',$request->emp_sal_month)
            ->count();
            if($count_data>0){
                $payment_date=$request->salary_payment_date;
                $months=$request->emp_sal_month;
                $ye=date('Y',strtotime($request->emp_sal_month));
                $mo=date('m',strtotime($request->emp_sal_month));
                $monthname=DB::table('tb_salary_history_shift')
                    ->select('tb_salary_history_shift.month')
                    ->where('dates',$months)
                    ->first();
                $department=DB::table('tb_salary_history_shift')
                    ->where('dates',$months)
                    ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
                    ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                    ->leftjoin('tblines','employees.line_id','=','tblines.id')
                    ->select('departments.departmentName','employees.empSection','tb_salary_history_shift.department_id','tblines.line_no','section_id')
                    ->where('tb_salary_history_shift.section_id',$request->section_id)
                    ->groupBy('tb_salary_history_shift.section_id')
                    ->get();

                    $excelName=time()."_Salary_Sheet";
                    Excel::create("$excelName", function($excel) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                        // Set the title
                        $name=Auth::user()->name;
                        $excel->setTitle("Employee Salary Sheet");
                        // Chain the setters
                        $excel->setCreator($name);
                        $excel->setDescription('Employee Salary Sheet');
                        $excel->sheet('Sheet 1', function ($sheet) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                            $sheet->loadView('three_shift_report.salary.excel.salary_sheet_section_excel')
                                ->with('monthname',$monthname)
                                ->with('dt',$dt)
                                ->with('department',$department)
                                ->with('months',$months)
                                ->with('ye',$ye)
                                ->with('mo',$mo)
                                ->with('payment_date',$payment_date);
                        });
                    })->download('xlsx');
            }else{
                return redirect()->back()->with('msg', 'No Data Found');
            }

         }
            $count_data=DB::table('tb_salary_history_shift')
                ->select('tb_salary_history_shift.month')
                ->where('dates',$request->emp_sal_month)
                ->count();
            if($count_data>0){
            $payment_date=$request->salary_payment_date;
            $months=$request->emp_sal_month;
            $ye=date('Y',strtotime($request->emp_sal_month));
            $mo=date('m',strtotime($request->emp_sal_month));
            $monthname=DB::table('tb_salary_history_shift')
                ->select('tb_salary_history_shift.month')
                ->where('dates',$months)
                ->first();
            $department=DB::table('tb_salary_history_shift')
                ->where('dates',$months)
                ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
                ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->select('departments.departmentName','employees.empSection','tb_salary_history_shift.department_id','tblines.line_no','section_id')
                ->where('tb_salary_history_shift.section_id',$request->section_id)
                ->groupBy('tb_salary_history_shift.section_id')
                ->get();
            return view('three_shift_report.salary.section_wise_salary_sheet',compact('monthname','dt','department','months','ye','mo','payment_date'));
        }else{
            return redirect()->back()->with('msg', 'No Data Found');
        }
    }

   if($request->type=='employee_wise_data'){
        if($request->salary_month_wise_excel_employee=='salary_month_wise_excel_employee'){
            
        $count_data=DB::table('tb_salary_history_shift')
            ->select('tb_salary_history_shift.month')
            ->where('dates',$request->emp_sal_month)
            ->count();
        if($count_data>0){
        $payment_date=$request->salary_payment_date;
        $months=$request->emp_sal_month;
        $ye=date('Y',strtotime($request->emp_sal_month));
        $mo=date('m',strtotime($request->emp_sal_month));
        $monthname=DB::table('tb_salary_history_shift')
            ->select('tb_salary_history_shift.month')
            ->where('dates',$months)
            ->first();
        $department=DB::table('tb_salary_history_shift')
            ->where('tb_salary_history_shift.emp_id',$request->emp_id)
            ->where('dates',$months)
            ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
            ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','employees.empSection','tb_salary_history_shift.department_id','tblines.line_no','section_id','emp_id')
            ->groupBy('tb_salary_history_shift.emp_id')
            ->get();


            $excelName=time()."_Salary_Sheet";
            Excel::create("$excelName", function($excel) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Employee Salary Sheet");
                // Chain the setters
                $excel->setCreator($name);
                $excel->setDescription('Employee Salary Sheet');
                $excel->sheet('Sheet 1', function ($sheet) use ($monthname,$department,$dt,$months,$ye,$mo,$payment_date) {
                    $sheet->loadView('three_shift_report.salary.excel.emp_wise_salary_sheet_excel')
                        ->with('monthname',$monthname)
                        ->with('dt',$dt)
                        ->with('department',$department)
                        ->with('months',$months)
                        ->with('ye',$ye)
                        ->with('mo',$mo)
                        ->with('payment_date',$payment_date);
                });
            })->download('xlsx');
    }else{
        return redirect()->back()->with('msg', 'No Data Found');
    }

    }
        $count_data=DB::table('tb_salary_history_shift')
            ->select('tb_salary_history_shift.month')
            ->where('dates',$request->emp_sal_month)
            ->count();
        if($count_data>0){
        $payment_date=$request->salary_payment_date;
        $months=$request->emp_sal_month;
        $ye=date('Y',strtotime($request->emp_sal_month));
        $mo=date('m',strtotime($request->emp_sal_month));
        $monthname=DB::table('tb_salary_history_shift')
            ->select('tb_salary_history_shift.month')
            ->where('dates',$months)
            ->first();
        $department=DB::table('tb_salary_history_shift')
            ->where('tb_salary_history_shift.emp_id',$request->emp_id)
            ->where('dates',$months)
            ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
            ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->select('departments.departmentName','employees.empSection','tb_salary_history_shift.department_id','tblines.line_no','section_id','emp_id')
            ->groupBy('tb_salary_history_shift.emp_id')
            ->get();
        return view('three_shift_report.salary.emp_wise_salary_sheet',compact('monthname','department','dt','months','ye','mo','payment_date'));
    }else{
        return redirect()->back()->with('msg', 'No Data Found');
    }
 }



 if($request->type=='month_wise_data'){
    if($request->salary_month_wise_excel_month=='salary_month_wise_excel_month'){
    $count_data=DB::table('tb_salary_history_shift')
        ->select('tb_salary_history_shift.month')
        ->where('dates',$request->emp_sal_month)
        ->count();
    if($count_data>0){
    $payment_date=$request->salary_payment_date;
    $months=$request->emp_sal_month;
    $ye=date('Y',strtotime($request->emp_sal_month));
    $mo=date('m',strtotime($request->emp_sal_month));
    $monthname=DB::table('tb_salary_history_shift')
        ->select('tb_salary_history_shift.month')
        ->where('dates',$months)
        ->first();
    $department=DB::table('tb_salary_history_shift')
        ->where('dates',$months)
        ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
        ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','employees.empSection','tb_salary_history_shift.department_id','tblines.line_no','section_id','emp_id')
        ->groupBy('tb_salary_history_shift.department_id')
        ->orderBy('departments.id','ASC')
        ->get();
        $excelName=time()."_Salary_Sheet";
        Excel::create("$excelName", function($excel) use ($monthname,$dt,$department,$months,$ye,$mo,$payment_date) {
            // Set the title
            $name=Auth::user()->name;
            $excel->setTitle("Employee Salary Sheet");
            // Chain the setters
            $excel->setCreator($name);
            $excel->setDescription('Employee Salary S');
            $excel->sheet('Sheet 1', function ($sheet) use ($monthname,$department,$dt,$months,$ye,$mo,$payment_date) {
                $sheet->loadView('three_shift_report.salary.excel.salary_sheet_month_excel')
                    ->with('monthname',$monthname)
                    ->with('dt',$dt)
                    ->with('department',$department)
                    ->with('months',$months)
                    ->with('ye',$ye)
                    ->with('mo',$mo)
                    ->with('payment_date',$payment_date);
            });
        })->download('xlsx');
    }else{
        return redirect()->back()->with('msg', 'No Data Found');
    }

    }
    $count_data=DB::table('tb_salary_history_shift')
        ->select('tb_salary_history_shift.month')
        ->where('dates',$request->emp_sal_month)
        ->count();
    if($count_data>0){
    $payment_date=$request->salary_payment_date;
    $months=$request->emp_sal_month;
    $ye=date('Y',strtotime($request->emp_sal_month));
    $mo=date('m',strtotime($request->emp_sal_month));
    $monthname=DB::table('tb_salary_history_shift')
        ->select('tb_salary_history_shift.month')
        ->where('dates',$months)
        ->first();
    $department=DB::table('tb_salary_history_shift')
        ->where('dates',$months)
        ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
        ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
        ->leftjoin('tblines','employees.line_id','=','tblines.id')
        ->select('departments.departmentName','employees.empSection','tb_salary_history_shift.department_id','tblines.line_no','section_id','emp_id')
        ->groupBy('tb_salary_history_shift.department_id')
        ->orderBy('departments.id','ASC')
        ->get();
    return view('three_shift_report.salary.month_wise_salary_sheet',compact('monthname','department','dt','months','ye','mo','payment_date'));
}else{
    return redirect()->back()->with('msg', 'No Data Found');
}
}

}

//payslip generate view
 public function payslipGenerate(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $employee   =DB::table('employees')->where('is_three_shift',1)->select('id','empFirstName','empLastName','employeeId')->get();
        return view('three_shift_report.payslip.index',compact('department','section','employee'));
}

 //payslip report
 public function payslipReportemployees(Request $request){
    if($request->type=='dept_wise_data'){
       if($request->payslip_option=='bangla'){
           $dt= $request->payslip_generate_report."-28";
           $dt= Carbon::parse($dt)->endOfMonth();
           $dt= Carbon::parse($dt)->toDateString();
           $data=DB::table('tb_salary_history_shift')
           ->where('dates',$request->payslip_generate_report)
           ->where('department_id',$request->department_id)
           ->where(function ($query) use ($dt){
               $query->where('date_of_discontinuation','=',null);
               $query->orWhere('date_of_discontinuation','>=',$dt);
           })
           ->where('employees.empJoiningDate','<=',$dt)
           ->where('tb_salary_history_shift.status',0)
           ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
           ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
           ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
           ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
           ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
           ->orderBy('employees.employeeId','ASC')
           ->paginate(500);
           return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
       }else{
           $dt= $request->payslip_generate_report."-28";
           $dt= Carbon::parse($dt)->endOfMonth();
           $dt= Carbon::parse($dt)->toDateString();
           $data=DB::table('tb_salary_history_shift')
           ->where('dates',$request->payslip_generate_report)
           ->where('department_id',$request->department_id)
           ->where(function ($query) use ($dt){
               $query->where('date_of_discontinuation','=',null);
               $query->orWhere('date_of_discontinuation','>=',$dt);
           })
           ->where('employees.empJoiningDate','<=',$dt)
           ->where('tb_salary_history_shift.status',0)
               ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
               ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
               ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
               ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
               ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
               ->orderBy('employees.employeeId','ASC')
               ->paginate(500);
           return view('three_shift_report.payslip.payslippdf',compact('data'));
       }
    }


    if($request->type=='section_wise_data'){
       $dt= $request->payslip_generate_report."-28";
       $dt= Carbon::parse($dt)->endOfMonth();
       $dt= Carbon::parse($dt)->toDateString();
       if($request->payslip_option=='bangla'){
           $data=DB::table('tb_salary_history_shift')
           ->where('dates',$request->payslip_generate_report)
           ->where('section_id',$request->section_id)
           ->where(function ($query) use ($dt){
               $query->where('date_of_discontinuation','=',null);
               $query->orWhere('date_of_discontinuation','>=',$dt);
           })
           ->where('employees.empJoiningDate','<=',$dt)
           ->where('tb_salary_history_shift.status',0)
               ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
               ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
               ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
               ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
               ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
               ->orderBy('employees.employeeId','ASC')
               ->paginate(500);
           return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
       }else{
           $dt= $request->payslip_generate_report."-28";
           $dt= Carbon::parse($dt)->endOfMonth();
           $dt= Carbon::parse($dt)->toDateString();
           $data=DB::table('tb_salary_history_shift')
           ->where('dates',$request->payslip_generate_report)
           ->where('section_id',$request->section_id)
           ->where(function ($query) use ($dt){
               $query->where('date_of_discontinuation','=',null);
               $query->orWhere('date_of_discontinuation','>=',$dt);
           })
           ->where('employees.empJoiningDate','<=',$dt)
           ->where('tb_salary_history_shift.status',0)
               ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
               ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
               ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
               ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
               ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
               ->orderBy('employees.employeeId','ASC')
               ->paginate(500);
           return view('three_shift_report.payslip.payslippdf',compact('data'));
       }
    }

    if($request->type=='emp_wise_data'){
       $dt= $request->payslip_generate_report."-28";
       $dt= Carbon::parse($dt)->endOfMonth();
       $dt= Carbon::parse($dt)->toDateString();
       if($request->payslip_option=='bangla'){
           $month=$request->payslip_generate_report;
           $data=DB::table('tb_salary_history_shift')
           ->where('dates',$request->payslip_generate_report)
           ->where(function ($query) use ($dt){
               $query->where('date_of_discontinuation','=',null);
               $query->orWhere('date_of_discontinuation','>=',$dt);
           })
           ->where('tb_salary_history_shift.emp_id','=',$request->emp_id)
           ->where('employees.empJoiningDate','<=',$dt)
           ->where('tb_salary_history_shift.status',0)
               ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
               ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
               ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
               ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
               ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
               ->orderBy('employees.employeeId','ASC')
               ->paginate(500);
           return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
       }else{
           $month=$request->payslip_generate_report;
           $dt= $request->payslip_generate_report."-28";
           $dt= Carbon::parse($dt)->endOfMonth();
           $dt= Carbon::parse($dt)->toDateString();
           $data=DB::table('tb_salary_history_shift')
           ->where('dates',$request->payslip_generate_report)
           ->where(function ($query) use ($dt){
               $query->where('date_of_discontinuation','=',null);
               $query->orWhere('date_of_discontinuation','>',$dt);
           })
           ->where('tb_salary_history_shift.emp_id','=',$request->emp_id)
           ->where('employees.empJoiningDate','<=',$dt)
           ->where('tb_salary_history_shift.status',0)
               ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
               ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
               ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
               ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
               ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
               ->orderBy('employees.employeeId','ASC')
               ->paginate(500);
           return view('three_shift_report.payslip.payslippdf',compact('data'));
       }

    }

    if($request->type=='month_wise_data'){
       if($request->payslip_option=='bangla'){
           $month=$request->payslip_generate_report;
           $dt= $request->payslip_generate_report."-28";
           $dt= Carbon::parse($dt)->endOfMonth();
           $dt= Carbon::parse($dt)->toDateString();
              $data=DB::table('tb_salary_history_shift')
                   ->where('dates',$request->payslip_generate_report)
                  ->where(function ($query) use ($dt){
                      $query->where('date_of_discontinuation','=',null);
                      $query->orWhere('date_of_discontinuation','>',$dt);
                  })
                   ->where('employees.empJoiningDate','<=',$dt)
                   ->where('tb_salary_history_shift.status',0)
                  ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                  ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                  ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                  ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
                  ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                  ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                  ->orderBy('departments.id','ASC')
                  ->orderBy('employees.employeeId','ASC')
                  ->paginate(500);
              return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
          }else{
           $month=$request->payslip_generate_report;
           $dt= $request->payslip_generate_report."-28";
           $dt= Carbon::parse($dt)->endOfMonth();
           $dt= Carbon::parse($dt)->toDateString();
              $data=DB::table('tb_salary_history_shift')
                   ->where('dates',$request->payslip_generate_report)
                  ->where(function ($query) use ($dt){
                      $query->where('date_of_discontinuation','=',null);
                      $query->orWhere('date_of_discontinuation','>',$dt);
                  })
                   ->where('employees.empJoiningDate','<=',$dt)
                   ->where('tb_salary_history_shift.status',0)
                  ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                  ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                  ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                  ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
                  ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                  ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                  ->orderBy('departments.id','ASC')
                  ->orderBy('employees.employeeId','ASC')
                  ->paginate(500);
           //    return count($data);
              return view('three_shift_report.payslip.payslippdf',compact('data'));
          }
       }
    }

      //payslip resigned employee
      public function payslipGenerateResignedEmployee(){
        $department=DB::table('employees')
        ->where('employees.date_of_discontinuation','!=',null)      
        ->where('employees.discon_type','=',2)
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->select('departments.id as dept_id','departments.departmentName')
        ->groupBy('employees.empDepartmentId')
        ->get();

        $section=DB::table('employees')
        ->select('empSection')
        ->where('employees.date_of_discontinuation','!=',null)      
        ->where('employees.discon_type','=',2)
        ->whereNotNull('empSection')
        ->groupBy('empSection')
        ->get();

       $employee=DB::table('employees')
       ->where('employees.date_of_discontinuation','!=',null)      
       ->where('employees.discon_type','=',2)
       ->where('employees.is_three_shift','=',1)
       ->select('id','empFirstName','empLastName','employeeId')
       ->get();
        return view('three_shift_report.payslip.resigned_employee',compact('department','section','employee'));
    }
    //payslip resigned employee show
    public function payslipGenerateResignedEmployeeshow(Request $request){
        if($request->type=='dept_wise_data'){
            if($request->payslip_option=='bangla'){
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                $data=DB::table('tb_salary_history_shift')
                ->where('dates',$request->payslip_generate_report)
                ->where('department_id',$request->department_id)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('employees.discon_type','=',2)
                ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                ->orderBy('employees.employeeId','ASC')
                ->paginate(500);
                return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
            }else{
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                $data=DB::table('tb_salary_history_shift')
                ->where('dates',$request->payslip_generate_report)
                ->where('department_id',$request->department_id)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('employees.discon_type','=',2)
                    ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                return view('three_shift_report.payslip.payslippdf',compact('data'));
            }
         }
         if($request->type=='section_wise_data'){
            $dt= $request->payslip_generate_report."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $ft= $request->payslip_generate_report."-28";
            $ft= Carbon::parse($ft)->firstOfMonth();
            $ft= Carbon::parse($ft)->toDateString();
            if($request->payslip_option=='bangla'){
                $data=DB::table('tb_salary_history_shift')
                ->where('dates',$request->payslip_generate_report)
                ->where('section_id',$request->section_id)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('employees.discon_type','=',2)
                    ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
            }else{
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                $data=DB::table('tb_salary_history_shift')
                ->where('dates',$request->payslip_generate_report)
                ->where('section_id',$request->section_id)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('employees.discon_type','=',2)
                    ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                return view('three_shift_report.payslip.payslippdf',compact('data'));
            }
         }
         if($request->type=='emp_wise_data'){
            $dt= $request->payslip_generate_report."-28";
            $dt= Carbon::parse($dt)->endOfMonth();
            $dt= Carbon::parse($dt)->toDateString();
            $ft= $request->payslip_generate_report."-28";
            $ft= Carbon::parse($ft)->firstOfMonth();
            $ft= Carbon::parse($ft)->toDateString();
            if($request->payslip_option=='bangla'){
                $month=$request->payslip_generate_report;
                $data=DB::table('tb_salary_history_shift')
                ->where('dates',$request->payslip_generate_report)
                ->where('tb_salary_history_shift.emp_id','=',$request->emp_id)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('employees.discon_type','=',2)
                    ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
            }else{
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                $data=DB::table('tb_salary_history_shift')
                ->where('dates',$request->payslip_generate_report)
                ->where('tb_salary_history_shift.emp_id','=',$request->emp_id)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('employees.discon_type','=',2)
                    ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                return view('three_shift_report.payslip.payslippdf',compact('data'));
            }
         }
         if($request->type=='month_wise_data'){
            if($request->payslip_option=='bangla'){
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                   $data=DB::table('tb_salary_history_shift')
                        ->where('dates',$request->payslip_generate_report)
                        ->where('employees.date_of_discontinuation','!=',null)
                        ->where('employees.date_of_discontinuation','<=',$dt)
                        ->where('employees.date_of_discontinuation','>=',$ft)
                        ->where('employees.date_of_discontinuation','!=',null)
                        ->where('employees.empJoiningDate','<=',$dt)
                        ->where('employees.discon_type','=',2)
                       ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                       ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                       ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                       ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
                       ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                       ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                       ->orderBy('departments.id','ASC')
                       ->orderBy('employees.employeeId','ASC')
                       ->paginate(500);
                   return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
               }else{
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                   $data=DB::table('tb_salary_history_shift')
                        ->where('dates',$request->payslip_generate_report)
                        ->where('employees.date_of_discontinuation','!=',null)
                        ->where('employees.date_of_discontinuation','<=',$dt)
                        ->where('employees.date_of_discontinuation','>=',$ft)
                        ->where('employees.date_of_discontinuation','!=',null)
                        ->where('employees.empJoiningDate','<=',$dt)
                        ->where('employees.discon_type','=',2)
                       ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                       ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                       ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                       ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
                       ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                       ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                       ->orderBy('departments.id','ASC')
                       ->orderBy('employees.employeeId','ASC')
                       ->paginate(500);
                   return view('three_shift_report.payslip.payslippdf',compact('data'));
               }
            }
       }



         //payslip lefty employee
         public function payslipGenerateLeftyEmployee(){
            $department=DB::table('employees')
            ->where('employees.date_of_discontinuation','!=',null)      
            ->where('employees.discon_type','=',1)
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->select('departments.id as dept_id','departments.departmentName')
            ->groupBy('employees.empDepartmentId')
            ->get();

            $section=DB::table('employees')
            ->select('empSection')
            ->where('employees.date_of_discontinuation','!=',null)      
            ->where('employees.discon_type','=',1)
            ->whereNotNull('empSection')
            ->groupBy('empSection')
            ->get();

           $employee=DB::table('employees')
           ->where('employees.date_of_discontinuation','!=',null)      
           ->where('employees.discon_type','=',1)
           ->where('employees.is_three_shift','=',1)
           ->select('id','empFirstName','empLastName','employeeId')
           ->get();
            return view('three_shift_report.payslip.lefty_employee',compact('department','section','employee'));
        }

         //lefty employee payslip show
         public function payslipGenerateLeftyEmployeeshow(Request $request){
            if($request->type=='dept_wise_data'){
                if($request->payslip_option=='bangla'){
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history_shift')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('department_id',$request->department_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                    ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                    ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                    ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                    ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                    ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                    ->orderBy('employees.employeeId','ASC')
                    ->paginate(500);
                    return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
                }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history_shift')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('department_id',$request->department_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                        ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('three_shift_report.payslip.payslippdf',compact('data'));
                }
             }
    
    
             if($request->type=='section_wise_data'){
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                if($request->payslip_option=='bangla'){
                    $data=DB::table('tb_salary_history_shift')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('section_id',$request->section_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                        ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
                }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history_shift')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('section_id',$request->section_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                        ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('three_shift_report.payslip.payslippdf',compact('data'));
                }
             }
    
             if($request->type=='emp_wise_data'){
                $dt= $request->payslip_generate_report."-28";
                $dt= Carbon::parse($dt)->endOfMonth();
                $dt= Carbon::parse($dt)->toDateString();
                $ft= $request->payslip_generate_report."-28";
                $ft= Carbon::parse($ft)->firstOfMonth();
                $ft= Carbon::parse($ft)->toDateString();
                if($request->payslip_option=='bangla'){
                    $month=$request->payslip_generate_report;
                    $data=DB::table('tb_salary_history_shift')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('tb_salary_history_shift.emp_id','=',$request->emp_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                        ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
                }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                    $data=DB::table('tb_salary_history_shift')
                    ->where('dates',$request->payslip_generate_report)
                    ->where('tb_salary_history_shift.emp_id','=',$request->emp_id)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.date_of_discontinuation','<=',$dt)
                    ->where('employees.date_of_discontinuation','>=',$ft)
                    ->where('employees.date_of_discontinuation','!=',null)
                    ->where('employees.empJoiningDate','<=',$dt)
                    ->where('employees.discon_type','=',1)
                        ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                        ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                        ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                        ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                        ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                        ->orderBy('employees.employeeId','ASC')
                        ->paginate(500);
                    return view('three_shift_report.payslip.payslippdf',compact('data'));
                }
    
             }
    
             if($request->type=='month_wise_data'){
                if($request->payslip_option=='bangla'){
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                       $data=DB::table('tb_salary_history_shift')
                            ->where('dates',$request->payslip_generate_report)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.date_of_discontinuation','<=',$dt)
                            ->where('employees.date_of_discontinuation','>=',$ft)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.empJoiningDate','<=',$dt)
                            ->where('employees.discon_type','=',1)
                           ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                           ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                           ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                           ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
                           ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                           ->select('tb_salary_history_shift.*','employees.empFirstName','employees.work_group','employees.empJoiningDate','employees.empSection','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                           ->orderBy('departments.id','ASC')
                           ->orderBy('employees.employeeId','ASC')
                           ->paginate(500);
                       return view('three_shift_report.payslip.payslip_bangla_pdf',compact('data'));
                   }else{
                    $dt= $request->payslip_generate_report."-28";
                    $dt= Carbon::parse($dt)->endOfMonth();
                    $dt= Carbon::parse($dt)->toDateString();
                    $ft= $request->payslip_generate_report."-28";
                    $ft= Carbon::parse($ft)->firstOfMonth();
                    $ft= Carbon::parse($ft)->toDateString();
                       $data=DB::table('tb_salary_history_shift')
                            ->where('dates',$request->payslip_generate_report)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.date_of_discontinuation','<=',$dt)
                            ->where('employees.date_of_discontinuation','>=',$ft)
                            ->where('employees.date_of_discontinuation','!=',null)
                            ->where('employees.empJoiningDate','<=',$dt)
                            ->where('employees.discon_type','=',1)
                           ->leftJoin('employees','tb_salary_history_shift.emp_id', '=', 'employees.id')
                           ->leftJoin('payroll_grade','tb_salary_history_shift.grade_id', '=', 'payroll_grade.id')
                           ->leftJoin('designations','tb_salary_history_shift.designation_id', '=', 'designations.id')
                           ->leftjoin('departments','tb_salary_history_shift.department_id','=','departments.id')
                           ->leftJoin('tblines','employees.line_id', '=', 'tblines.id')
                           ->select('tb_salary_history_shift.*','employees.empFirstName','employees.empJoiningDate','employees.empSection','employees.work_group','employees.employeeId','tblines.line_no','payroll_grade.grade_name','designations.designation','designations.designationBangla','employees.empBnFullName','employees.employeeBnId','employees.payment_mode')
                           ->orderBy('departments.id','ASC')
                           ->orderBy('employees.employeeId','ASC')
                           ->paginate(500);
                    //    return count($data);
                       return view('three_shift_report.payslip.payslippdf',compact('data'));
                   }
                }
          }


    //lefty employees salary sheet view page
    public function leftyEmployeesSalaryView(){
        $department =DB::table('employees')
        ->where('employees.date_of_discontinuation','!=',null)
        ->where('employees.discon_type','=',1)
        ->where('employees.is_three_shift','=',1)
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->select('departments.id as dept_id','departments.departmentName')
        ->groupBy('employees.empDepartmentId')
        ->get();
        $section =DB::table('employees')
        ->select('empSection')
        ->where('employees.date_of_discontinuation','!=',null)
        ->where('employees.discon_type','=',1)
        ->where('employees.is_three_shift','=',1)
        ->whereNotNull('empSection')
        ->groupBy('empSection')
        ->get();
        $employee =DB::table('employees')
        ->where('employees.date_of_discontinuation','!=',null)
        ->where('employees.discon_type','=',1)
        ->where('employees.is_three_shift','=',1)
        ->select('id','empFirstName','empLastName','employeeId')
        ->get();
        return view('three_shift_report.salary.lefty_employee_salary_sheet',compact('department','section','employee'));
      }


    //lefty employees salary sheet generate
    public function leftyEmployeesSalaryShow(Request $request){
        $months=$request->emp_sal_month;
        $dt= $request->emp_sal_month."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $ft= $request->emp_sal_month."-28";
        $ft= Carbon::parse($ft)->firstOfMonth();
        $ft= Carbon::parse($ft)->toDateString();
        // dd($dt);
        $y=date('Y',strtotime($months));
        $m=date('m',strtotime($months));
        $monthname=DB::table('tb_salary_history_shift')
            ->select('tb_salary_history_shift.month')
            ->where('dates',$months)
            ->first();
        if($request->type=='dept_wise_data'){
            if($request->dept_excel=='dept_excel'){
                $data=DB::table('tb_salary_history_shift')
                ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history_shift.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history_shift.department_id',$request->department_id)
                ->where('employees.discon_type','=',1)
                ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."lefty_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Lefty Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Lefty Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('three_shift_report.salary.excel.lefty_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');
            }
            $data=DB::table('tb_salary_history_shift')
            ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history_shift.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history_shift.department_id',$request->department_id)
            ->where('employees.discon_type','=',1)
            ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='section_wise_data'){
            if($request->section_excel=='section_excel'){
                $data=DB::table('tb_salary_history_shift')
                ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history_shift.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history_shift.section_id',$request->section_id)
                ->where('employees.discon_type','=',1)
                ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."lefty_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Lefty Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Lefty Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('three_shift_report.salary.excel.lefty_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');

            }

            $data=DB::table('tb_salary_history_shift')
            ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history_shift.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history_shift.section_id',$request->section_id)
            ->where('employees.discon_type','=',1)
            ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='employee_wise_data'){
            if($request->employee_excel=='employee_excel'){
                $data=DB::table('tb_salary_history_shift')
                ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history_shift.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history_shift.emp_id',$request->emp_id)
                ->where('employees.discon_type','=',1)
                ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."lefty_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Lefty Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Lefty Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('three_shift_report.salary.excel.lefty_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');

            }   
            $data=DB::table('tb_salary_history_shift')
            ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history_shift.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history_shift.emp_id',$request->emp_id)
            ->where('employees.discon_type','=',1)
            ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='month_wise_data'){
            if($request->month_excel=='month_excel'){
                $data=DB::table('tb_salary_history_shift')
                ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history_shift.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('employees.discon_type','=',1)
                ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."lefty_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Lefty Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Lefty Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('three_shift_report.salary.excel.lefty_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');
            }    
            $data=DB::table('tb_salary_history_shift')
            ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history_shift.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('employees.discon_type','=',1)
            ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }
        return view('three_shift_report.salary.lefty_employee_salary_sheet_show',compact('data','monthname','months'));
    }


     //resigned employees salary sheet view page
     public function resignedEmployeesSalaryView(){
        $department =DB::table('employees')
        ->where('employees.date_of_discontinuation','!=',null)
        ->where('employees.discon_type','=',2)
        ->where('employees.is_three_shift','=',1)
        ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
        ->select('departments.id as dept_id','departments.departmentName')
        ->groupBy('employees.empDepartmentId')
        ->get();
        $section =DB::table('employees')
        ->select('empSection')
        ->where('employees.date_of_discontinuation','!=',null)
        ->where('employees.discon_type','=',2)
        ->where('employees.is_three_shift','=',1)
        ->whereNotNull('empSection')
        ->groupBy('empSection')->get();
        $employee   =DB::table('employees')
        ->where('employees.date_of_discontinuation','!=',null)
        ->where('employees.discon_type','=',2)
        ->where('employees.is_three_shift','=',1)
        ->select('id','empFirstName','empLastName','employeeId')
        ->get();
        return view('three_shift_report.salary.resigned_employee_salary_sheet',compact('department','section','employee'));
    }


    //resigned employees salary sheet generate
    public function resignedEmployeesSalaryShow(Request $request){
        $months=$request->emp_sal_month;
        $dt= $request->emp_sal_month."-28";
        $dt= Carbon::parse($dt)->endOfMonth();
        $dt= Carbon::parse($dt)->toDateString();
        $ft= $request->emp_sal_month."-28";
        $ft= Carbon::parse($ft)->firstOfMonth();
        $ft= Carbon::parse($ft)->toDateString();
        // dd($dt);
        $y=date('Y',strtotime($months));
        $m=date('m',strtotime($months));
        $monthname=DB::table('tb_salary_history_shift')
            ->select('tb_salary_history_shift.month')
            ->where('dates',$months)
            ->first();
        if($request->type=='dept_wise_data'){
            if($request->dept_excel=='dept_excel'){
                $data=DB::table('tb_salary_history_shift')
                ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history_shift.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history_shift.department_id',$request->department_id)
                ->where('employees.discon_type','=',2)
                ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."Resigned_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Resigned Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Resigned Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('three_shift_report.salary.excel.resigned_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');

            }
            $data=DB::table('tb_salary_history_shift')
            ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history_shift.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history_shift.department_id',$request->department_id)
            ->where('employees.discon_type','=',2)
            ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='section_wise_data'){
            if($request->section_excel=='section_excel'){
                $data=DB::table('tb_salary_history_shift')
                ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history_shift.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history_shift.section_id',$request->section_id)
                ->where('employees.discon_type','=',2)
                ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."Resigned_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Resigned Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Resigned Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('three_shift_report.salary.excel.resigned_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');
            }   
            $data=DB::table('tb_salary_history_shift')
            ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history_shift.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history_shift.section_id',$request->section_id)
            ->where('employees.discon_type','=',2)
            ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='employee_wise_data'){
            if($request->employee_excel=='employee_excel'){
                $data=DB::table('tb_salary_history_shift')
                ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history_shift.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('tb_salary_history_shift.emp_id',$request->emp_id)
                ->where('employees.discon_type','=',2)
                ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."Resigned_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Resigned Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Resigned Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('three_shift_report.salary.excel.resigned_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');
            }   
            $data=DB::table('tb_salary_history_shift')
            ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history_shift.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('tb_salary_history_shift.emp_id',$request->emp_id)
            ->where('employees.discon_type','=',2)
            ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }

        if($request->type=='month_wise_data'){
            if($request->month_excel=='month_excel'){
                $data=DB::table('tb_salary_history_shift')
                ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
                ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('tblines','employees.line_id','=','tblines.id')
                ->where('tb_salary_history_shift.dates',$months)
                ->where('employees.date_of_discontinuation','<=',$dt)
                ->where('employees.date_of_discontinuation','>=',$ft)
                ->where('employees.date_of_discontinuation','!=',null)
                ->where('employees.empJoiningDate','<=',$dt)
                ->where('employees.discon_type','=',2)
                ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
                ->orderBy('employees.empSection', 'DESC')
                ->orderBy('employees.employeeId','ASC')
                ->get();
                $excelName=time()."Resigned_Salary_Sheet";
                Excel::create("$excelName", function($excel) use ($data,$monthname,$months) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Resigned Salary Sheet");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Resigned Salary Sheet');
                    $excel->sheet('Sheet 1', function ($sheet) use ($data,$monthname,$months) {
                        $sheet->loadView('three_shift_report.salary.excel.resigned_excel')
                            ->with('data',$data)
                            ->with('monthname',$monthname)
                            ->with('months',$months);
                    });
                })->download('xlsx');
            }   
            $data=DB::table('tb_salary_history_shift')
            ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
            ->leftjoin('payroll_grade','tb_salary_history_shift.grade_id','=','payroll_grade.id')
            ->leftjoin('designations','tb_salary_history_shift.designation_id','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('tblines','employees.line_id','=','tblines.id')
            ->where('tb_salary_history_shift.dates',$months)
            ->where('employees.date_of_discontinuation','<=',$dt)
            ->where('employees.date_of_discontinuation','>=',$ft)
            ->where('employees.date_of_discontinuation','!=',null)
            ->where('employees.empJoiningDate','<=',$dt)
            ->where('employees.discon_type','=',2)
            ->select('tb_salary_history_shift.emp_id','employees.payment_mode','tb_salary_history_shift.present','tb_salary_history_shift.weekend','tb_salary_history_shift.holiday','tb_salary_history_shift.total_payable_days','tb_salary_history_shift.basic_salary','tb_salary_history_shift.house_rant','tb_salary_history_shift.medical','tb_salary_history_shift.transport','tb_salary_history_shift.food','tb_salary_history_shift.gross','tb_salary_history_shift.leave','tb_salary_history_shift.total','tb_salary_history_shift.working_day','tb_salary_history_shift.absent','tb_salary_history_shift.absent_deduction_amount','tb_salary_history_shift.advanced_deduction','tb_salary_history_shift.gross_pay','tb_salary_history_shift.overtime','tb_salary_history_shift.overtime_rate','tb_salary_history_shift.overtime_amount','tb_salary_history_shift.attendance_bonus','tb_salary_history_shift.increment_bonus_percent','tb_salary_history_shift.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','employees.employeeId as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.empSection')
            ->orderBy('employees.empSection', 'DESC')
            ->orderBy('employees.employeeId','ASC')
            ->get();
        }
        return view('three_shift_report.salary.resigned_employee_salary_sheet_show',compact('data','monthname','months'));
    }



    
    //bank payment sheet
    public function bank_payment_sheet(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $employee   =DB::table('employees')->where('is_three_shift',1)->select('id','empFirstName','empLastName','employeeId')->get();
        return view('three_shift_report.salary.bank.bank_payment_sheet',compact('department','section','employee'));

    }

      //bkash payment sheet
    public function bkash_payment_sheet(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $employee   =DB::table('employees')->where('is_three_shift',1)->select('id','empFirstName','empLastName','employeeId')->get();
        return view('three_shift_report.salary.bkash.bkash_payment_sheet',compact('department','section','employee'));
    }


     //bank payment sheet report
     public function bank_payment_sheet_show(Request $request){
        //        return $request->all();
                $month=Carbon::parse($request->emp_sal_month."-01")->toDateString();
        //        return $month;
                $data=DB::table('tb_salary_history_shift')
                ->join('employees','tb_salary_history_shift.emp_id','=','employees.id')
                ->leftJoin('designations','employees.empDesignationId','=','designations.id')
                    ->where('employees.payment_mode','=','Bank')
                    ->where('tb_salary_history_shift.month','=',$month);
                if($request->type=="dept_wise_data"){
                    if($request->department_id!="0") {
                        $data = $data->where('employees.empDepartmentId', '=', $request->department_id);
                    }
                }
                elseif ($request->type=="section_wise_data"){
                    if($request->section_id!="0") {
                        $data=$data->where('employees.empSection','=',$request->section_id);
                    }
                }
                elseif($request->type=="employee_wise_data"){
                    $data=$data->where('employees.id','=',$request->emp_id);
                }
                if($request->working_group!='0' && $request->type!="employee_wise_data"){
                    $data=$data->where('employees.work_group','=',$request->working_group);
        
                }
                $data=$data->select('employees.empFirstName','employees.empLastName','employees.employeeId',
                    'employees.empSection','designations.designation','employees.bank_account','tb_salary_history_shift.net_amount')
                    ->get();
                if(count($data)==0){
                    Session::flash('error',"No Data Found");
                    return redirect()->back();
                }
                if(isset($request->salary_month_wise_excel)){
                    if($request->salary_month_wise_excel=='salary_month_wise_excel'){
                        Excel::create("bank_payment_sheet_excel", function($excel) use ($month, $data) {
                            // Set the title
                            $name=Auth::user()->name;
                            $excel->setTitle("Bank Payment Sheet");
                            // Chain the setters
                            $excel->setCreator($name);
                            $excel->setDescription('Bank Payment Sheet');
                            $excel->sheet('Sheet 1', function ($sheet) use ($month,$data) {
                                $sheet->loadView('three_shift_report.salary.bank.bank_payment_sheet_excel')
                                    ->with('data',$data)
                                    ->with('month',$month);
                            });
                        })->download('xlsx');
                        return view('three_shift_report.salary.bank.bank_payment_sheet_excel',compact('data','month'));
        
                    }
                }
        //        return $data;
                return view('three_shift_report.salary.bank.bank_payment_sheet_show',compact('data','month'));
            }
        
        
              //bkash payment sheet report
              public function bkash_payment_sheet_show(Request $request){
                //        return $request->all();
                        $month=Carbon::parse($request->emp_sal_month."-01")->toDateString();
                //        return $month;
                        $data=DB::table('tb_salary_history_shift')->join('employees','tb_salary_history_shift.emp_id','=','employees.id')->leftJoin('designations','employees.empDesignationId','=','designations.id')
                            ->where('employees.payment_mode','=','bKash')
                            ->where('tb_salary_history_shift.month','=',$month);
                        if($request->type=="dept_wise_data"){
                            if($request->department_id!="0") {
                                $data = $data->where('employees.empDepartmentId', '=', $request->department_id);
                            }
                        }
                        elseif ($request->type=="section_wise_data"){
                            if($request->section_id!="0") {
                                $data=$data->where('employees.empSection','=',$request->section_id);
                            }
                        }
                        elseif($request->type=="employee_wise_data"){
                            $data=$data->where('employees.id','=',$request->emp_id);
                        }
                        if($request->working_group!='0' && $request->type!="employee_wise_data"){
                            $data=$data->where('employees.work_group','=',$request->working_group);
                
                        }
                        $data=$data->select('employees.empFirstName','employees.empLastName','employees.employeeId',
                            'employees.empSection','designations.designation','employees.bank_account','tb_salary_history_shift.net_amount')
                            ->get();
                        if(count($data)==0){
                            Session::flash('error',"No Data Found");
                            return redirect()->back();
                        }
                        if(isset($request->salary_month_wise_excel)){
                            if($request->salary_month_wise_excel=='salary_month_wise_excel'){
                                Excel::create("bkash_payment_sheet_excel", function($excel) use ($month, $data) {
                                    // Set the title
                                    $name=Auth::user()->name;
                                    $excel->setTitle("Bkash Payment Sheet");
                                    // Chain the setters
                                    $excel->setCreator($name);
                                    $excel->setDescription('Bkash Payment Sheet');
                                    $excel->sheet('Sheet 1', function ($sheet) use ($month,$data) {
                                        $sheet->loadView('report.payroll.bkash.bkash_payment_sheet_excel')
                                            ->with('data',$data)
                                            ->with('month',$month);
                                    });
                                })->download('xlsx');
                                return view('three_shift_report.salary.bkash.bkash_payment_sheet_excel',compact('data','month'));
                
                            }
                        }
                        return view('three_shift_report.salary.bkash.bkash_payment_sheet_show',compact('data','month'));
                    }


     //employee salary summery
     public function salarySummeryReport(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $work_group =DB::table('employees')->select('work_group')->whereNotNull('work_group')->groupBy('work_group')->get();
        return view('three_shift_report.salary.salary_summery',compact('department','section','work_group'));
    }

      //salary summery report show year and month wise
      public function salarySummeryReportShow(Request $request){
        if($request->group_id=='all_group'){
         $dt= $request->salary_summery_monthsss."-28";
         $dt= Carbon::parse($dt)->endOfMonth();
         $dt= Carbon::parse($dt)->toDateString();
         $month=$request->salary_summery_monthsss;
         $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
         $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
         $all_group='all_group';
         $data=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('employees.work_group','departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
             ->where(function ($query) use ($dt){
                 $query->where('date_of_discontinuation','=',null);
                 $query->orWhere('date_of_discontinuation','>',$dt);
             })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.status','=',0)
         ->groupBy('employees.work_group')
         ->get();
         $lefty=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',1)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
 
         $resigned=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',2)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         if($request->download_excel=='download_excel'){
             $excelName=time()."_Salary_Summary";
             Excel::create("$excelName", function($excel) use ($data,$month,$all_group,$lefty,$resigned) {
                 // Set the title
                 $name=Auth::user()->name;
                 $excel->setTitle("Salary Summary");
                 // Chain the setters
                 $excel->setCreator($name);
                 $excel->setDescription('Employee Salary Summary');
                 $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$all_group,$lefty,$resigned) {
                     $sheet->loadView('three_shift_report.salary.excel.salary_summary_xl_data')
                         ->with('data',$data)
                         ->with('month',$month)
                         ->with('all_group',$all_group)
                         ->with('lefty',$lefty)
                         ->with('resigned',$resigned);
                 });
             })->download('xlsx');
        }else{
         return view('three_shift_report.salary.salary_summery_monthly_data',compact('data','month','all_group','lefty','resigned'));
        }
     }
 
        if($request->group_id){
         $dt= $request->salary_summery_monthsss."-28";
         $dt= Carbon::parse($dt)->endOfMonth();
         $dt= Carbon::parse($dt)->toDateString();
         $month=$request->salary_summery_monthsss;
         $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
         $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
         $group=$request->group_id;
         $data=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('employees.work_group','departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
             ->where(function ($query) use ($dt){
                 $query->where('date_of_discontinuation','=',null);
                 $query->orWhere('date_of_discontinuation','>',$dt);
             })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.status','=',0)
         ->where('employees.work_group',$request->group_id)
         ->get();
         $lefty=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',1)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
 
         $resigned=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',2)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         if($request->download_excel=='download_excel'){
             $excelName=time()."_Salary_Summary";
             Excel::create("$excelName", function($excel) use ($data,$month,$group,$lefty,$resigned) {
                 // Set the title
                 $name=Auth::user()->name;
                 $excel->setTitle("Salary Summary");
                 // Chain the setters
                 $excel->setCreator($name);
                 $excel->setDescription('Employee Salary Summary');
                 $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$group,$lefty,$resigned) {
                     $sheet->loadView('three_shift_report.salary.excel.salary_summary_xl_data')
                         ->with('data',$data)
                         ->with('month',$month)
                         ->with('group',$group)
                         ->with('lefty',$lefty)
                         ->with('resigned',$resigned);
                 });
             })->download('xlsx');
         }else{
             return view('three_shift_report.salary.salary_summery_monthly_data',compact('data','month','group','lefty','resigned'));
         }
        }
 
       if($request->department_id=='all_department'){
         if($request->download_excel=='download_excel'){
             $dt= $request->salary_summery_monthsss."-28";
             $dt= Carbon::parse($dt)->endOfMonth();
             $dt= Carbon::parse($dt)->toDateString();
             $month=$request->salary_summery_monthsss;
             $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
             $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
             $all_dept='all_dept';
             $data=DB::table('tb_salary_history_shift')
             ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
             ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
             ->leftjoin('tblines','employees.line_id','=','tblines.id')
             ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
             ->where('tb_salary_history_shift.dates',$month)
                 ->where(function ($query) use ($dt){
                     $query->where('date_of_discontinuation','=',null);
                     $query->orWhere('date_of_discontinuation','>',$dt);
                 })
             ->where('employees.empJoiningDate','<=',$dt)
             ->where('tb_salary_history_shift.status','=',0)
             ->groupBy('tb_salary_history_shift.department_id')
             ->get();
             $lefty=DB::table('tb_salary_history_shift')
             ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
             ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
             ->leftjoin('tblines','employees.line_id','=','tblines.id')
             ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
             ->where('tb_salary_history_shift.dates',$month)
             ->whereBetween('employees.date_of_discontinuation',array($s,$e))
             ->where('employees.discon_type','=',1)
             ->where('employees.empJoiningDate','<=',$dt)
             // ->groupBy('tb_salary_history_shift.department_id')
             ->get();
 
             $resigned=DB::table('tb_salary_history_shift')
             ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
             ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
             ->leftjoin('tblines','employees.line_id','=','tblines.id')
             ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
             ->where('tb_salary_history_shift.dates',$month)
             ->whereBetween('employees.date_of_discontinuation',array($s,$e))
             ->where('employees.discon_type','=',2)
             ->where('employees.empJoiningDate','<=',$dt)
             // ->groupBy('tb_salary_history_shift.department_id')
             ->get();
             $excelName=time()."_Salary_Summary";
             Excel::create("$excelName", function($excel) use ($data,$month,$all_dept,$lefty,$resigned) {
                 // Set the title
                 $name=Auth::user()->name;
                 $excel->setTitle("Salary Summary");
                 // Chain the setters
                 $excel->setCreator($name);
                 $excel->setDescription('Employee Salary Summary');
                 $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$all_dept,$lefty,$resigned) {
                     $sheet->loadView('three_shift_report.salary.excel.salary_summary_xl_data')
                         ->with('data',$data)
                         ->with('month',$month)
                         ->with('all_dept',$all_dept)
                         ->with('lefty',$lefty)
                         ->with('resigned',$resigned);
                 });
             })->download('xlsx');
         }
 
         $dt= $request->salary_summery_monthsss."-28";
         $dt= Carbon::parse($dt)->endOfMonth();
         $dt= Carbon::parse($dt)->toDateString();
         $month=$request->salary_summery_monthsss;
         $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
         $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
         $all_dept='all_dept';
         $data=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
             ->where(function ($query) use ($dt){
                 $query->where('date_of_discontinuation','=',null);
                 $query->orWhere('date_of_discontinuation','>',$dt);
             })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.status','=',0)
         ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         $lefty=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',1)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
 
         $resigned=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',2)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         return view('three_shift_report.salary.salary_summery_monthly_data',compact('data','month','all_dept','lefty','resigned'));
       }
       if($request->type=='department_wise_data'){
         if($request->download_excel=='download_excel'){
         $dt= $request->salary_summery_monthsss."-28";
         $dt= Carbon::parse($dt)->endOfMonth();
         $dt= Carbon::parse($dt)->toDateString();
         $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
         $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
         $month=$request->salary_summery_monthsss;
         $dept_id=$request->department_id;
         $data=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
             ->where(function ($query) use ($dt){
                 $query->where('date_of_discontinuation','=',null);
                 $query->orWhere('date_of_discontinuation','>',$dt);
             })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.department_id','=',$dept_id)
         ->where('tb_salary_history_shift.status','=',0)
         ->get();
         $lefty=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',1)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         $resigned=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',2)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         $excelName=time()."_Salary_Summary";
         Excel::create("$excelName", function($excel) use ($data,$month,$dept_id,$lefty,$resigned) {
             // Set the title
             $name=Auth::user()->name;
             $excel->setTitle("Salary Summary");
             // Chain the setters
             $excel->setCreator($name);
             $excel->setDescription('Employee Salary Summary');
             $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$dept_id,$lefty,$resigned) {
                 $sheet->loadView('three_shift_report.salary.excel.salary_summary_xl_data')
                     ->with('data',$data)
                     ->with('month',$month)
                     ->with('dept_id',$dept_id)
                     ->with('lefty',$lefty)
                     ->with('resigned',$resigned);
             });
         })->download('xlsx');
 
         }
 
         $dt= $request->salary_summery_monthsss."-28";
         $dt= Carbon::parse($dt)->endOfMonth();
         $dt= Carbon::parse($dt)->toDateString();
         $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
         $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
         $month=$request->salary_summery_monthsss;
         $dept_id=$request->department_id;
         $data=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
             ->where(function ($query) use ($dt){
                 $query->where('date_of_discontinuation','=',null);
                 $query->orWhere('date_of_discontinuation','>',$dt);
             })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.department_id','=',$dept_id)
         ->where('tb_salary_history_shift.status','=',0)
         ->get();
         $lefty=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',1)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         $resigned=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',2)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         return view('three_shift_report.salary.salary_summery_monthly_data',compact('data','month','dept_id','lefty','resigned'));
       }
 
       if($request->section_id=='all_section'){
         if($request->download_excel=='download_excel'){
 
         $dt= $request->salary_summery_monthsss."-28";
         $dt= Carbon::parse($dt)->endOfMonth();
         $dt= Carbon::parse($dt)->toDateString();
         $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
         $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
         $month=$request->salary_summery_monthsss;
         $all_section='all_section';
         $data=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
             ->where(function ($query) use ($dt){
                 $query->where('date_of_discontinuation','=',null);
                 $query->orWhere('date_of_discontinuation','>',$dt);
             })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.status','=',0)
         ->groupBy('tb_salary_history_shift.section_id')
         ->get();
         $lefty=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',1)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         $resigned=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',2)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
 
         $excelName=time()."_Salary_Summary";
         Excel::create("$excelName", function($excel) use ($data,$month,$all_section,$lefty,$resigned) {
             // Set the title
             $name=Auth::user()->name;
             $excel->setTitle("Salary Summary");
             // Chain the setters
             $excel->setCreator($name);
             $excel->setDescription('Employee Salary Summary');
             $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$all_section,$lefty,$resigned) {
                 $sheet->loadView('three_shift_report.salary.excel.salary_summary_xl_data')
                     ->with('data',$data)
                     ->with('month',$month)
                     ->with('all_section',$all_section)
                     ->with('lefty',$lefty)
                     ->with('resigned',$resigned);
             });
         })->download('xlsx');
 
         }
 
         $dt= $request->salary_summery_monthsss."-28";
         $dt= Carbon::parse($dt)->endOfMonth();
         $dt= Carbon::parse($dt)->toDateString();
         $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
         $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
         $month=$request->salary_summery_monthsss;
         $all_section='all_section';
         $data=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->where(function ($query) use ($dt){
             $query->where('date_of_discontinuation','=',null);
             $query->orWhere('date_of_discontinuation','>',$dt);
         })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.status','=',0)
         ->groupBy('tb_salary_history_shift.section_id')
         ->get();
         $lefty=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',1)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         $resigned=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',2)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         return view('three_shift_report.salary.salary_summery_monthly_data',compact('data','month','all_section','lefty','resigned'));
       }
 
       if($request->type=='section_wise_data'){
         if($request->download_excel=='download_excel'){
             $dt= $request->salary_summery_monthsss."-28";
             $dt= Carbon::parse($dt)->endOfMonth();
             $dt= Carbon::parse($dt)->toDateString();
             $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
             $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
             $month=$request->salary_summery_monthsss;
             $section_id=$request->section_id;
             $data=DB::table('tb_salary_history_shift')
             ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
             ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
             ->leftjoin('tblines','employees.line_id','=','tblines.id')
             ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
             ->where('tb_salary_history_shift.dates',$month)
             ->where(function ($query) use ($dt){
                 $query->where('date_of_discontinuation','=',null);
                 $query->orWhere('date_of_discontinuation','>',$dt);
             })
             ->where('employees.empJoiningDate','<=',$dt)
             ->where('tb_salary_history_shift.section_id','=',$section_id)
             ->where('tb_salary_history_shift.status','=',0)
             ->get();
 
             $lefty=DB::table('tb_salary_history_shift')
             ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
             ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
             ->leftjoin('tblines','employees.line_id','=','tblines.id')
             ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
             ->where('tb_salary_history_shift.dates',$month)
             ->whereBetween('employees.date_of_discontinuation',array($s,$e))
             ->where('employees.discon_type','=',1)
             ->where('employees.empJoiningDate','<=',$dt)
             // ->groupBy('tb_salary_history_shift.department_id')
             ->get();
             $resigned=DB::table('tb_salary_history_shift')
             ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
             ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
             ->leftjoin('tblines','employees.line_id','=','tblines.id')
             ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
             ->where('tb_salary_history_shift.dates',$month)
             ->whereBetween('employees.date_of_discontinuation',array($s,$e))
             ->where('employees.discon_type','=',2)
             ->where('employees.empJoiningDate','<=',$dt)
             // ->groupBy('tb_salary_history_shift.department_id')
             ->get();
 
             $excelName=time()."_Salary_Summary";
             Excel::create("$excelName", function($excel) use ($data,$month,$section_id,$lefty,$resigned) {
                 // Set the title
                 $name=Auth::user()->name;
                 $excel->setTitle("Salary Summary");
                 // Chain the setters
                 $excel->setCreator($name);
                 $excel->setDescription('Employee Salary Summary');
                 $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$section_id,$lefty,$resigned) {
                     $sheet->loadView('three_shift_report.salary.excel.salary_summary_xl_data')
                         ->with('data',$data)
                         ->with('month',$month)
                         ->with('section_id',$section_id)
                         ->with('lefty',$lefty)
                         ->with('resigned',$resigned);
                 });
             })->download('xlsx');
 
         }
         $dt= $request->salary_summery_monthsss."-28";
         $dt= Carbon::parse($dt)->endOfMonth();
         $dt= Carbon::parse($dt)->toDateString();
         $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
         $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
         $month=$request->salary_summery_monthsss;
         $section_id=$request->section_id;
         $data=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
             ->where(function ($query) use ($dt){
                 $query->where('date_of_discontinuation','=',null);
                 $query->orWhere('date_of_discontinuation','>',$dt);
             })
         ->where('employees.empJoiningDate','<=',$dt)
         ->where('tb_salary_history_shift.section_id','=',$section_id)
         ->where('tb_salary_history_shift.status','=',0)
         ->get();
 
         $lefty=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',1)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         $resigned=DB::table('tb_salary_history_shift')
         ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
         ->where('tb_salary_history_shift.dates',$month)
         ->whereBetween('employees.date_of_discontinuation',array($s,$e))
         ->where('employees.discon_type','=',2)
         ->where('employees.empJoiningDate','<=',$dt)
         // ->groupBy('tb_salary_history_shift.department_id')
         ->get();
         return view('three_shift_report.salary.salary_summery_monthly_data',compact('data','month','section_id','lefty','resigned'));
       }
 
 
        if($request->type=='all_data'){
             if($request->download_excel=='download_excel'){
                 $dt= $request->salary_summery_monthsss."-28";
                 $dt= Carbon::parse($dt)->endOfMonth();
                 $dt= Carbon::parse($dt)->toDateString();
                 $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
                 $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
                 $month=$request->salary_summery_monthsss;
                 $monthly_data='Monthly';
                 $data=DB::table('tb_salary_history_shift')
                 ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                 ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                 ->leftjoin('tblines','employees.line_id','=','tblines.id')
                 ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
                 ->where('tb_salary_history_shift.dates',$month)
                 ->where(function ($query) use ($dt){
                     $query->where('date_of_discontinuation','=',null);
                     $query->orWhere('date_of_discontinuation','>',$dt);
                 })
                 ->where('employees.empJoiningDate','<=',$dt)
                 ->where('tb_salary_history_shift.status','=',0)
                 ->groupBy('tb_salary_history_shift.department_id')
                 ->get();
 
                 $lefty=DB::table('tb_salary_history_shift')
                 ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                 ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                 ->leftjoin('tblines','employees.line_id','=','tblines.id')
                 ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
                 ->where('tb_salary_history_shift.dates',$month)
                 ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                 ->where('employees.discon_type','=',1)
                 ->where('employees.empJoiningDate','<=',$dt)
                 // ->groupBy('tb_salary_history_shift.department_id')
                 ->get();
                 $resigned=DB::table('tb_salary_history_shift')
                 ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
                 ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                 ->leftjoin('tblines','employees.line_id','=','tblines.id')
                 ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
                 ->where('tb_salary_history_shift.dates',$month)
                 ->whereBetween('employees.date_of_discontinuation',array($s,$e))
                 ->where('employees.discon_type','=',2)
                 ->where('employees.empJoiningDate','<=',$dt)
                 // ->groupBy('tb_salary_history_shift.department_id')
                 ->get();
                 $excelName=time()."_Salary_Summary";
                 Excel::create("$excelName", function($excel) use ($data,$month,$monthly_data,$lefty,$resigned) {
                     // Set the title
                     $name=Auth::user()->name;
                     $excel->setTitle("Salary Summary");
                     // Chain the setters
                     $excel->setCreator($name);
                     $excel->setDescription('Employee Salary Summary');
                     $excel->sheet('Sheet 1', function ($sheet) use ($data,$month,$monthly_data,$lefty,$resigned) {
                         $sheet->loadView('three_shift_report.salary.excel.salary_summary_xl_data')
                             ->with('data',$data)
                             ->with('month',$month)
                             ->with('monthly_data',$monthly_data)
                             ->with('lefty',$lefty)
                             ->with('resigned',$resigned);
                     });
                 })->download('xlsx');
 
             }
             $dt= $request->salary_summery_monthsss."-28";
             $dt= Carbon::parse($dt)->endOfMonth();
             $dt= Carbon::parse($dt)->toDateString();
             $s=date('Y-m-01',strtotime($request->salary_summery_monthsss));
             $e=date('Y-m-t',strtotime($request->salary_summery_monthsss));
             $month=$request->salary_summery_monthsss;
             $monthly_data='Monthly';
             $data=DB::table('tb_salary_history_shift')
             ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
             ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
             ->leftjoin('tblines','employees.line_id','=','tblines.id')
             ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
             ->where('tb_salary_history_shift.dates',$month)
                 ->where(function ($query) use ($dt){
                     $query->where('date_of_discontinuation','=',null);
                     $query->orWhere('date_of_discontinuation','>',$dt);
                 })
             ->where('employees.empJoiningDate','<=',$dt)
             ->where('tb_salary_history_shift.status','=',0)
             ->groupBy('tb_salary_history_shift.department_id')
             ->get();
 
             $lefty=DB::table('tb_salary_history_shift')
             ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
             ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
             ->leftjoin('tblines','employees.line_id','=','tblines.id')
             ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
             ->where('tb_salary_history_shift.dates',$month)
             ->whereBetween('employees.date_of_discontinuation',array($s,$e))
             ->where('employees.discon_type','=',1)
             ->where('employees.empJoiningDate','<=',$dt)
             // ->groupBy('tb_salary_history_shift.department_id')
             ->get();
             $resigned=DB::table('tb_salary_history_shift')
             ->leftjoin('employees','tb_salary_history_shift.emp_id','=','employees.id')
             ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
             ->leftjoin('tblines','employees.line_id','=','tblines.id')
             ->select('departments.departmentName','tb_salary_history_shift.section_id',DB::raw('count(tb_salary_history_shift.emp_id) as worker'),DB::raw('SUM(tb_salary_history_shift.gross_pay) as gross_pay'),  DB::raw('SUM(tb_salary_history_shift.overtime) as overtimes'),DB::raw('SUM(tb_salary_history_shift.overtime_amount) as ot_amounts'),DB::raw('SUM(tb_salary_history_shift.advanced_deduction) as advanced_deduction'), DB::raw('SUM(tb_salary_history_shift.basic_salary) as basic'),DB::raw('SUM(tb_salary_history_shift.house_rant) as house'),DB::raw('SUM(tb_salary_history_shift.medical) as medicals'),DB::raw('SUM(tb_salary_history_shift.transport) as transports'),DB::raw('SUM(tb_salary_history_shift.food) as foods'),DB::raw('SUM(tb_salary_history_shift.gross) as gorss'),DB::raw('SUM(tb_salary_history_shift.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_shift.absent_deduction_amount) as a_deduction_amount'),DB::raw('SUM(tb_salary_history_shift.net_amount) as net_total'),DB::raw('SUM(tb_salary_history_shift.attendance_bonus) as attendance_bonus'))
             ->where('tb_salary_history_shift.dates',$month)
             ->whereBetween('employees.date_of_discontinuation',array($s,$e))
             ->where('employees.discon_type','=',2)
             ->where('employees.empJoiningDate','<=',$dt)
             // ->groupBy('tb_salary_history_shift.department_id')
             ->get();
             return view('three_shift_report.salary.salary_summery_monthly_data',compact('data','month','monthly_data','lefty','resigned'));
         }
     }



   //job card single 3-shift employee
   public function emp_single_job_card(){
    $employees=DB::table('employees')
    ->where('empAccStatus','=',1)
    ->where('is_three_shift',1)
    ->where('date_of_discontinuation','=',NULL)
    ->get(['empFirstName','employeeId','id']);
    return view('report.attendance.attendance_card_three_shift',compact('employees'));
   }

   //3-shift job card report single employee
   public function job_card_single(Request $request){   
    DB::table('emp_shift_leave_attendance')->truncate();   
    DB::table('emp_shift_holiday')->truncate();  
    DB::table('emp_shift_status')->truncate();  
    $emp_id=$request->emp_id;
    $active_check=DB::table('employees')
    ->where('id',$emp_id)
    ->where('is_three_shift',1)
    ->where('empAccStatus',1)
    ->where('date_of_discontinuation',NULL)
    ->count();
    if($active_check<=0){
        Session::flash('error','Please Activate the employee');
        return redirect()->back();
    }
    $s=date('Y-m-d',strtotime($request->start_date));
    $e=date('Y-m-d',strtotime($request->end_date));
    $check=DB::table('employees')
    ->where('employees.id',$emp_id)
    ->where('is_three_shift',1)
    ->join('three_shift_employee','employees.id','=','three_shift_employee.emp_id')
    ->whereBetween('shift_date',[$s,$e])
    ->count();
    if($check>0){   
     $month_check=date('Y-m-01',strtotime($request->start_date));
     $y=date('Y',strtotime($request->start_date));
     $m=date('m',strtotime($request->start_date));
     $start =date('Y-m-d',strtotime($request->start_date));
     $end =date('Y-m-d',strtotime($request->end_date));
     $s =date('Y-m-d',strtotime($request->start_date));
     $e =date('Y-m-d',strtotime($request->end_date));
     $leaveattyear=date('Y',strtotime($request->start_date));
     $leaveattmonth=date('m',strtotime($request->start_date));
     //check leave between attendance then delete attendance 
     $delete=DB::SELECT("SELECT attendance.id as att_id 
     FROM leave_of_employee 
     LEFT JOIN attendance ON leave_of_employee.emp_id=attendance.emp_id 
     WHERE attendance.date BETWEEN leave_of_employee.start_date
     AND leave_of_employee.end_date AND YEAR(leave_of_employee.month)='$leaveattyear' 
     AND MONTH(leave_of_employee.month)='$leaveattmonth' AND attendance.emp_id='$emp_id' AND leave_of_employee.emp_id='$emp_id'"); 		
     $delete_id = [];
     foreach ($delete as $key => $value) {
         $delete_id[]=$value->att_id;
     } 
     $ok = DB::table('attendance')->whereIn('id', $delete_id)->delete();
     //check leave between attendance then delete attendance 
     $em_total_leave=DB::table('leave_of_employee')
     ->where('emp_id',$emp_id)
     ->where('month',$month_check)
     ->get(); 
     $shift_leave=[]; 
     foreach($em_total_leave as $l){
        for($i=$l->start_date;$i<=$l->end_date;$i++){
            $data=[
                'emp_id'=>$l->emp_id,
                'leave_id'=>$l->leave_type_id,
                'leave_date'=>$i,
                'total'=>1
            ];
          $shift_leave[]=$data;
        }
    }
    $shift_leave = collect($shift_leave);
    $shift_leave = $shift_leave->chunk(500);
    foreach ($shift_leave as $shift_leaves)
    {
     \DB::table('emp_shift_leave_attendance')->insert($shift_leaves->toArray());
    }
    $em_total_festival=DB::table('tb_festival_leave')
    ->whereYear('fest_starts',$y)
    ->whereMonth('fest_starts',$m)
    ->get();
    $tot_fest=[];
    foreach($em_total_festival as $fl){
        for($i=$fl->fest_starts;$i<=$fl->fest_ends;$i++){
            $data=[
             'holiday_date'=>$i,
              'total'=>1
            ];
          $tot_fest[]=$data;
        }
    }
    $tot_fest = collect($tot_fest);
    $tot_fest = $tot_fest->chunk(500);
    foreach ($tot_fest as $tot_fests)
    {
     \DB::table('emp_shift_holiday')->insert($tot_fests->toArray());
    }
     $employee=DB::Table('employees')
     ->where('employees.id',$emp_id)
     ->where('is_three_shift',1)
     ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
     ->leftjoin('designations','employees.empDesignationId','=','designations.id')
     ->select('employeeId','empFirstName','empLastName','empJoiningDate','empSection','departmentName','designation','empOTStatus')
     ->first();
     //check join this month
     $check_join=DB::table('employees')
     ->where('id',$emp_id)
     ->whereYear('empJoiningDate',date('Y',strtotime($request->start_date)))
     ->whereMonth('empJoiningDate',date('m',strtotime($request->start_date)))
     ->count();
     if($check_join>0){
         $leave_holiday=0;
         $regular_present=0;
         $regular_leave=0;
         $shift_min_max=DB::table('three_shift_employee')
         ->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$employee->empJoiningDate,$e])
         ->select(DB::raw("MIN(shift_date) as min"), DB::raw("MAX(shift_date) as max"))
         ->first();
         $data=DB::table('three_shift_employee')
         ->where('three_shift_employee.emp_id',$emp_id)
         ->whereBetween('shift_date',[$employee->empJoiningDate,$shift_min_max->max])
         ->leftjoin('attendance','date','=','shift_date')
         ->leftjoin('emp_shift_leave_attendance','leave_date','=','shift_date')
         ->leftjoin('emp_shift_holiday','holiday_date','=','shift_date')
         ->leftjoin('tbweekend_regular_day','weekend_date','=','shift_date')
         ->select('three_shift_employee.id','three_shift_employee.emp_id','shift_date','weekend','weekend_date','leave_date','date','holiday_date')
         ->orderBy('three_shift_employee.id','ASC')
         ->get();
         $shift_status_insert=[];
         foreach($data as $item){
             $data=[
                'emp_id'=>$item->emp_id,
                'weekend'=>$item->weekend,
                'present'=>$item->date,
                'leave_date'=>$item->leave_date,
                'holiday'=>$item->holiday_date,
                'regular'=>$item->weekend_date,
                'shift_date'=>$item->shift_date
             ];
           $shift_status_insert[]=$data;
         }
         $shift_status_insert = collect($shift_status_insert);
         $shift_status_insert = $shift_status_insert->chunk(500);
         foreach ($shift_status_insert as $shift_status_inserts)
         {
          \DB::table('emp_shift_status')->insert($shift_status_inserts->toArray());
         }
         $shift_emp=DB::Table('three_shift_employee')->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$employee->empJoiningDate,$shift_min_max->max])
         ->get();
         $total_present=DB::table('attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('date',[$employee->empJoiningDate,$shift_min_max->max])
         ->count();
         $total_leave=DB::table('emp_shift_leave_attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('leave_date',[$employee->empJoiningDate,$shift_min_max->max])
         ->count();
         $emp_leave=DB::table('emp_shift_leave_attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('leave_date',[$employee->empJoiningDate,$shift_min_max->max])
         ->select('leave_date')
         ->get();
         foreach($emp_leave as $leave){
            $leave_holiday+=DB::table('emp_shift_holiday')
            ->where('holiday_date',$leave->leave_date)
            ->count();;
         }
         $total_regular_date=DB::table('tbweekend_regular_day')
         ->whereBetween('weekend_date',[$employee->empJoiningDate,$shift_min_max->max])
         ->select('weekend_date')
         ->get();
         foreach($total_regular_date as $rd){
            $regular_present+=DB::table('attendance')
            ->where('emp_id',$emp_id)
            ->where('date',$rd->weekend_date)
            ->count();
            $regular_leave+=DB::table('emp_shift_leave_attendance')
            ->where('emp_id',$emp_id)
            ->where('leave_date',$rd->weekend_date)
            ->count();
         }
         $total_regular_day=DB::table('tbweekend_regular_day')
         ->whereBetween('weekend_date',[$employee->empJoiningDate,$shift_min_max->max])
         ->count()-$regular_present-$regular_leave;
         $total_holiday=DB::table('emp_shift_holiday')
         ->whereBetween('holiday_date',[$employee->empJoiningDate,$shift_min_max->max])
         ->count();
         $ac_holiday=$total_holiday-$leave_holiday; 
        $weekend_employee_total=DB::table('emp_shift_status')
        ->where('emp_id',$emp_id)
        ->where('weekend','!=',NULL)
        ->where('present','=',NULL)
        ->where('leave_date','=',NULL)
        ->where('holiday','=',NULL)
        ->where('regular','=',NULL)
        ->count(); 
         $total_weekend=$weekend_employee_total;
         $total_working_day=DB::table('three_shift_employee')
         ->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$employee->empJoiningDate,$shift_min_max->max])
         ->count();
         $total_absent=$total_working_day-$total_weekend-$total_present-$total_leave-$ac_holiday+$total_regular_day;
        }else{
         $leave_holiday=0;
         $regular_present=0;
         $regular_leave=0;
         $shift_min_max=DB::table('three_shift_employee')
         ->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$s,$e])
         ->select(DB::raw("MIN(shift_date) as min"), DB::raw("MAX(shift_date) as max"))
         ->first();
         $data=DB::table('three_shift_employee')
         ->where('three_shift_employee.emp_id',$emp_id)
         ->whereBetween('shift_date',[$shift_min_max->min,$shift_min_max->max])
         ->leftjoin('attendance','date','=','shift_date')
         ->leftjoin('emp_shift_leave_attendance','leave_date','=','shift_date')
         ->leftjoin('emp_shift_holiday','holiday_date','=','shift_date')
         ->leftjoin('tbweekend_regular_day','weekend_date','=','shift_date')
         ->select('three_shift_employee.id','three_shift_employee.emp_id','shift_date','weekend','weekend_date','leave_date','date','holiday_date')
         ->orderBy('three_shift_employee.id','ASC')
         ->get();
         $shift_status_insert=[];
         foreach($data as $item){
             $data=[
                'emp_id'=>$item->emp_id,
                'weekend'=>$item->weekend,
                'present'=>$item->date,
                'leave_date'=>$item->leave_date,
                'holiday'=>$item->holiday_date,
                'regular'=>$item->weekend_date,
                'shift_date'=>$item->shift_date
             ];
           $shift_status_insert[]=$data;
         }
         $shift_status_insert = collect($shift_status_insert);
         $shift_status_insert = $shift_status_insert->chunk(500);
         foreach ($shift_status_insert as $shift_status_inserts)
         {
          \DB::table('emp_shift_status')->insert($shift_status_inserts->toArray());
         }
         $shift_emp=DB::Table('three_shift_employee')->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$shift_min_max->min,$shift_min_max->max])
         ->get();
         $total_present=DB::table('attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('date',[$shift_min_max->min,$shift_min_max->max])
         ->count();
         $total_leave=DB::table('emp_shift_leave_attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('leave_date',[$shift_min_max->min,$shift_min_max->max])
         ->count();
         $total_regular_date=DB::table('tbweekend_regular_day')
         ->whereBetween('weekend_date',[$shift_min_max->min,$shift_min_max->max])
         ->select('weekend_date')
         ->get();
         foreach($total_regular_date as $rd){
            $regular_present+=DB::table('attendance')
            ->where('emp_id',$emp_id)
            ->where('date',$rd->weekend_date)
            ->count();
            $regular_leave+=DB::table('emp_shift_leave_attendance')
            ->where('emp_id',$emp_id)
            ->where('leave_date',$rd->weekend_date)
            ->count();
         }
         $total_regular_day=DB::table('tbweekend_regular_day')
         ->whereBetween('weekend_date',[$shift_min_max->min,$shift_min_max->max])
         ->count()-$regular_present-$regular_leave;
         $emp_leave=DB::table('emp_shift_leave_attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('leave_date',[$shift_min_max->min,$shift_min_max->max])
         ->select('leave_date')
         ->get();
         foreach($emp_leave as $leave){
          $leave_holiday+=DB::table('emp_shift_holiday')
          ->where('holiday_date',$leave->leave_date)
          ->count();
         }
         $total_holiday=DB::table('emp_shift_holiday')
         ->whereBetween('holiday_date',[$shift_min_max->min,$shift_min_max->max])
         ->count();
         $ac_holiday=$total_holiday-$leave_holiday;   
         $weekend_employee_total=DB::table('emp_shift_status')
         ->where('emp_id',$emp_id)
         ->where('weekend','!=',NULL)
         ->where('present','=',NULL)
         ->where('leave_date','=',NULL)
         ->where('holiday','=',NULL)
         ->where('regular','=',NULL)
         ->count(); 
         $total_weekend=$weekend_employee_total;
         $total_working_day=DB::table('three_shift_employee')
         ->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$shift_min_max->min,$shift_min_max->max])
         ->count();
         $total_absent=$total_working_day-$total_weekend-$total_present-$total_leave-$ac_holiday+$total_regular_day;
     }
     return view('report.attendance.three_shift.attendance_card',compact('shift_emp','employee','start','end','total_present','total_leave','ac_holiday','total_weekend','total_absent'));  
    }else{
        Session::flash('error','Please Assign Employee Shift');
        return redirect()->back();
    }
   }


   //job card single lefty/resign employee 3-shift
   public function emp_single_job_card_lefty_resign(){
    $employees=DB::table('employees')
    ->where('empAccStatus','=',0)
    ->where('is_three_shift',1)
    ->where('date_of_discontinuation','!=',NULL)
    ->get(['empFirstName','employeeId','id']);
    return view('report.attendance.attendance_card_three_shift_lefty_resign',compact('employees'));
   } 


   //lefty/resign employee job card show
   public function left_resign_single_job_card(Request $request){
    DB::table('emp_shift_leave_attendance')->truncate();   
    DB::table('emp_shift_holiday')->truncate();  
    DB::table('emp_shift_status')->truncate(); 
    $emp_id=$request->emp_id;
    $inactive_check=DB::table('employees')
    ->where('id',$emp_id)
    ->where('is_three_shift',1)
    ->where('empAccStatus',0)
    ->where('date_of_discontinuation','!=',NULL)
    ->count();
    if($inactive_check<=0){
        Session::flash('error','Please Inactive the employee Status');
        return redirect()->back();
    }
    $s=date('Y-m-d',strtotime($request->start_date));
    $e=date('Y-m-d',strtotime($request->end_date));
    $check=DB::table('employees')
    ->where('employees.id',$emp_id)
    ->where('is_three_shift',1)
    ->join('three_shift_employee','employees.id','=','three_shift_employee.emp_id')
    ->whereBetween('shift_date',[$s,$e])
    ->count();
    if($check>0){
     $month_check=date('Y-m-01',strtotime($request->start_date));
     $y=date('Y',strtotime($request->start_date));
     $m=date('m',strtotime($request->start_date));
     $start =date('Y-m-d',strtotime($request->start_date));
     $end =date('Y-m-d',strtotime($request->end_date));
     $s =date('Y-m-d',strtotime($request->start_date));
     $e =date('Y-m-d',strtotime($request->end_date));
     $leaveattyear=date('Y',strtotime($request->start_date));
     $leaveattmonth=date('m',strtotime($request->start_date));
     //check leave between attendance then delete attendance 
     $delete=DB::SELECT("SELECT attendance.id as att_id 
     FROM leave_of_employee 
     LEFT JOIN attendance ON leave_of_employee.emp_id=attendance.emp_id 
     WHERE attendance.date BETWEEN leave_of_employee.start_date
     AND leave_of_employee.end_date AND YEAR(leave_of_employee.month)='$leaveattyear' 
     AND MONTH(leave_of_employee.month)='$leaveattmonth' AND attendance.emp_id='$emp_id' AND leave_of_employee.emp_id='$emp_id'"); 		
     $delete_id = [];
     foreach ($delete as $key => $value) {
         $delete_id[]=$value->att_id;
     } 
     $ok = DB::table('attendance')->whereIn('id', $delete_id)->delete();
     //check leave between attendance then delete attendance 
     $em_total_leave=DB::table('leave_of_employee')
     ->where('emp_id',$emp_id)
     ->where('month',$month_check)
     ->get();  
     foreach($em_total_leave as $l){
        for($i=$l->start_date;$i<=$l->end_date;$i++){
          DB::table('emp_shift_leave_attendance')->insert([
              'emp_id'=>$l->emp_id,
              'leave_id'=>$l->leave_type_id,
              'leave_date'=>$i,
              'total'=>1
          ]);
        }
    }
    $em_total_festival=DB::table('tb_festival_leave')
    ->whereYear('fest_starts',$y)
    ->whereMonth('fest_starts',$m)
    ->get();
    foreach($em_total_festival as $fl){
        for($i=$fl->fest_starts;$i<=$fl->fest_ends;$i++){
          DB::table('emp_shift_holiday')->insert([
              'holiday_date'=>$i,
              'total'=>1
          ]);
        }
    }
     $employee=DB::Table('employees')
     ->where('employees.id',$emp_id)
     ->where('is_three_shift',1)
     ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
     ->leftjoin('designations','employees.empDesignationId','=','designations.id')
     ->select('employeeId','empFirstName','empLastName','empJoiningDate','empSection','departmentName','designation','empOTStatus','date_of_discontinuation')
     ->first();
     //check discontinuation this month
     $check_discontinuation=DB::table('employees')
     ->where('id',$emp_id)
     ->whereYear('date_of_discontinuation',date('Y',strtotime($request->start_date)))
     ->whereMonth('date_of_discontinuation',date('m',strtotime($request->start_date)))
     ->count();
     if($check_discontinuation>0){
         $leave_holiday=0;
         $regular_present=0;
         $regular_leave=0;
         $shift_min_max=DB::table('three_shift_employee')
         ->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$s,$employee->date_of_discontinuation])
         ->select(DB::raw("MIN(shift_date) as min"), DB::raw("MAX(shift_date) as max"))
         ->first();
         $data=DB::table('three_shift_employee')
         ->where('three_shift_employee.emp_id',$emp_id)
         ->whereBetween('shift_date',[$shift_min_max->min,$shift_min_max->max])
         ->leftjoin('attendance','date','=','shift_date')
         ->leftjoin('emp_shift_leave_attendance','leave_date','=','shift_date')
         ->leftjoin('emp_shift_holiday','holiday_date','=','shift_date')
         ->leftjoin('tbweekend_regular_day','weekend_date','=','shift_date')
         ->select('three_shift_employee.id','three_shift_employee.emp_id','shift_date','weekend','weekend_date','leave_date','date','holiday_date')
         ->orderBy('three_shift_employee.id','ASC')
         ->get();
         foreach($data as $item){
            DB::table('emp_shift_status')->insert([
                'emp_id'=>$item->emp_id,
                'weekend'=>$item->weekend,
                'present'=>$item->date,
                'leave_date'=>$item->leave_date,
                'holiday'=>$item->holiday_date,
                'regular'=>$item->weekend_date,
                'shift_date'=>$item->shift_date
            ]);
          }
         $shift_emp=DB::Table('three_shift_employee')->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$shift_min_max->min,$employee->date_of_discontinuation])
         ->get();
         $total_present=DB::table('attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('date',[$shift_min_max->min,$employee->date_of_discontinuation])
         ->count();
         $total_leave=DB::table('emp_shift_leave_attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('leave_date',[$shift_min_max->min,$employee->date_of_discontinuation])
         ->count();
         $emp_leave=DB::table('emp_shift_leave_attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('leave_date',[$shift_min_max->min,$employee->date_of_discontinuation])
         ->select('leave_date')
         ->get();
         foreach($emp_leave as $leave){
            $leave_holiday+=DB::table('emp_shift_holiday')
            ->where('holiday_date',$leave->leave_date)
            ->count();;
         }
         $total_regular_date=DB::table('tbweekend_regular_day')
         ->whereBetween('weekend_date',[$shift_min_max->min,$employee->date_of_discontinuation])
         ->select('weekend_date')
         ->get();
         foreach($total_regular_date as $rd){
            $regular_present+=DB::table('attendance')
            ->where('emp_id',$emp_id)
            ->where('date',$rd->weekend_date)
            ->count();
            $regular_leave+=DB::table('emp_shift_leave_attendance')
            ->where('emp_id',$emp_id)
            ->where('leave_date',$rd->weekend_date)
            ->count();
         }
         $total_regular_day=DB::table('tbweekend_regular_day')
         ->whereBetween('weekend_date',[$shift_min_max->min,$employee->date_of_discontinuation])
         ->count()-$regular_present-$regular_leave;
         $total_holiday=DB::table('emp_shift_holiday')
         ->whereBetween('holiday_date',[$shift_min_max->min,$employee->date_of_discontinuation])
         ->count();
         $ac_holiday=$total_holiday-$leave_holiday; 
         $weekend_employee_total=DB::table('emp_shift_status')
         ->where('emp_id',$emp_id)
         ->where('weekend','!=',NULL)
         ->where('present','=',NULL)
         ->where('leave_date','=',NULL)
         ->where('holiday','=',NULL)
         ->where('regular','=',NULL)
         ->count();
         $total_weekend=$weekend_employee_total;
         $total_working_day=DB::table('three_shift_employee')
         ->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$shift_min_max->min,$employee->date_of_discontinuation])
         ->count();
         $total_absent=$total_working_day-$total_weekend-$total_present-$total_leave-$ac_holiday+$total_regular_day;
     }else{
         $leave_holiday=0;
         $regular_present=0;
         $regular_leave=0;
         $shift_min_max=DB::table('three_shift_employee')
         ->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$s,$e])
         ->select(DB::raw("MIN(shift_date) as min"), DB::raw("MAX(shift_date) as max"))
         ->first();
         $data=DB::table('three_shift_employee')
         ->where('three_shift_employee.emp_id',$emp_id)
         ->whereBetween('shift_date',[$shift_min_max->min,$shift_min_max->max])
         ->leftjoin('attendance','date','=','shift_date')
         ->leftjoin('emp_shift_leave_attendance','leave_date','=','shift_date')
         ->leftjoin('emp_shift_holiday','holiday_date','=','shift_date')
         ->leftjoin('tbweekend_regular_day','weekend_date','=','shift_date')
         ->select('three_shift_employee.id','three_shift_employee.emp_id','shift_date','weekend','weekend_date','leave_date','date','holiday_date')
         ->orderBy('three_shift_employee.id','ASC')
         ->get();
         foreach($data as $item){
            DB::table('emp_shift_status')->insert([
                'emp_id'=>$item->emp_id,
                'weekend'=>$item->weekend,
                'present'=>$item->date,
                'leave_date'=>$item->leave_date,
                'holiday'=>$item->holiday_date,
                'regular'=>$item->weekend_date,
                'shift_date'=>$item->shift_date
            ]);
          }
         $shift_emp=DB::Table('three_shift_employee')->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$shift_min_max->min,$shift_min_max->max])
         ->get();
         $total_present=DB::table('attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('date',[$shift_min_max->min,$shift_min_max->max])
         ->count();
         $total_leave=DB::table('emp_shift_leave_attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('leave_date',[$shift_min_max->min,$shift_min_max->max])
         ->count();
         $total_regular_date=DB::table('tbweekend_regular_day')
         ->whereBetween('weekend_date',[$shift_min_max->min,$shift_min_max->max])
         ->select('weekend_date')
         ->get();
         foreach($total_regular_date as $rd){
            $regular_present+=DB::table('attendance')
            ->where('emp_id',$emp_id)
            ->where('date',$rd->weekend_date)
            ->count();
            $regular_leave+=DB::table('emp_shift_leave_attendance')
            ->where('emp_id',$emp_id)
            ->where('leave_date',$rd->weekend_date)
            ->count();
         }
         $total_regular_day=DB::table('tbweekend_regular_day')
         ->whereBetween('weekend_date',[$shift_min_max->min,$shift_min_max->max])
         ->count()-$regular_present-$regular_leave;
         $emp_leave=DB::table('emp_shift_leave_attendance')
         ->where('emp_id',$emp_id)
         ->whereBetween('leave_date',[$shift_min_max->min,$shift_min_max->max])
         ->select('leave_date')
         ->get();
         foreach($emp_leave as $leave){
          $leave_holiday+=DB::table('emp_shift_holiday')
          ->where('holiday_date',$leave->leave_date)
          ->count();
         }
         $total_holiday=DB::table('emp_shift_holiday')
         ->whereBetween('holiday_date',[$shift_min_max->min,$shift_min_max->max])
         ->count();
         $ac_holiday=$total_holiday-$leave_holiday;  
         $weekend_employee_total=DB::table('emp_shift_status')
         ->where('emp_id',$emp_id)
         ->where('weekend','!=',NULL)
         ->where('present','=',NULL)
         ->where('leave_date','=',NULL)
         ->where('holiday','=',NULL)
         ->where('regular','=',NULL)
         ->count();  
         $total_weekend=$weekend_employee_total;
         $total_working_day=DB::table('three_shift_employee')
         ->where('emp_id',$emp_id)
         ->whereBetween('shift_date',[$shift_min_max->min,$shift_min_max->max])
         ->count();
         $total_absent=$total_working_day-$total_weekend-$total_present-$total_leave-$ac_holiday+$total_regular_day;
     }
     return view('report.attendance.three_shift.attendance_card_lefty_resign',compact('shift_emp','employee','start','end','total_present','total_leave','ac_holiday','total_weekend','total_absent'));  
    }else{
        Session::flash('error','Please Assign Employee Shift');
        return redirect()->back();
    }
   }



  //3-shift job card all 
  public function attendanceCardAll(){
    $units=DB::table('units')->get(['name','id']);
    $departments=DB::table('departments')->get(['departmentName','id']);
    $floors=DB::table('floors')->get(['floor','id']);
    $sections=DB::table('employees')->whereNotNull('empSection')->distinct()->get(['empSection']);
    $designations=DB::table('designations')->get(['designation','id']);
    return view('report.attendance.three_shift.job_card_all', compact('units','departments',
        'floors', 'sections', 'designations'));
 }

  //3-shift job card all data
  public function attendanceCardAllshift(Request $request){      
   DB::table('emp_shift_leave_attendance')->truncate();   
   DB::table('emp_shift_holiday')->truncate(); 
   DB::table('emp_shift_status')->truncate(); 
   $start=date('Y-m-d',strtotime($request->start_date));
   $end=date('Y-m-d',strtotime($request->end_date));
   $year_month=date('Y-m',strtotime($request->start_date));
   $year=date('Y',strtotime($request->start_date));
   $month=date('m',strtotime($request->start_date));
   $month_check=date('Y-m-01',strtotime($request->end_date));
   $delete=DB::SELECT("SELECT attendance.id as att_id 
     FROM leave_of_employee 
     LEFT JOIN attendance ON leave_of_employee.emp_id=attendance.emp_id 
     LEFT JOIN employees ON leave_of_employee.emp_id=employees.id 
     WHERE attendance.date BETWEEN leave_of_employee.start_date
     AND leave_of_employee.end_date AND YEAR(leave_of_employee.month)='$year' 
     AND MONTH(leave_of_employee.month)='$month' AND employees.is_three_shift=1"); 		
     $delete_id = [];
     foreach ($delete as $key => $value) {
         $delete_id[]=$value->att_id;
     } 
     $ok = DB::table('attendance')->whereIn('id', $delete_id)->delete();
     //check leave between attendance then delete attendance 
     $em_total_leave=DB::table('leave_of_employee')
     ->leftjoin('employees','leave_of_employee.emp_id','=','employees.id')
     ->where('month',$month_check)
     ->where('employees.is_three_shift',1)
     ->get();  
     foreach($em_total_leave as $l){
        for($i=$l->start_date;$i<=$l->end_date;$i++){
          DB::table('emp_shift_leave_attendance')->insert([
              'emp_id'=>$l->emp_id,
              'leave_id'=>$l->leave_type_id,
              'leave_date'=>$i,
              'total'=>1
          ]);
        }
    }
    $em_total_festival=DB::table('tb_festival_leave')
    ->whereYear('fest_starts',$year)
    ->whereMonth('fest_starts',$month)
    ->get();
    foreach($em_total_festival as $fl){
        for($i=$fl->fest_starts;$i<=$fl->fest_ends;$i++){
          DB::table('emp_shift_holiday')->insert([
              'holiday_date'=>$i,
              'total'=>1
          ]);
        }
    }
   $query=DB::table('employees')
   ->where('employees.is_three_shift',1)
   ->leftJoin('units','employees.unit_id','=','units.id')
   ->leftJoin('departments','employees.empDepartmentId','=','departments.id');
        if (($request->unitId)!=0) {
            $query=$query->where('employees.unit_id','=', $request->unitId);
        }
        if (($request->floorId)!=0) {
            $query=$query->where('employees.floor_id','=', $request->floorId);
        }
        if (!empty($request->sectionName)) {
            $query=$query->where('employees.empSection','=', $request->sectionName);
        }
        if (($request->departmentId)!=0) {
            $query=$query->where('employees.empDepartmentId','=', $request->departmentId);
        }
        if (($request->designationId)!=0) {
            $query=$query->where('employees.empDesignationId','=', $request->designationId);
        }
        $query=$query->where('employees.empAccStatus','=', $request->accStatus);
        $query=$query->select('employees.id','date_of_discontinuation','empOTStatus','empJoiningDate');
        $employee=$query->get();
        $shift_status_insert=[];
        foreach($employee as $employees){ 
          $data=DB::SELECT("SELECT weekend,three_shift_employee.emp_id,shift_date,attendance.date,leave_date,weekend_date,holiday_date
          FROM
          three_shift_employee
          LEFT JOIN attendance ON three_shift_employee.shift_date=attendance.date
          LEFT JOIN emp_shift_leave_attendance ON three_shift_employee.shift_date=emp_shift_leave_attendance.leave_date
          LEFT JOIN tbweekend_regular_day ON three_shift_employee.shift_date=tbweekend_regular_day.weekend_date
          LEFT JOIN emp_shift_holiday ON three_shift_employee.shift_date=emp_shift_holiday.holiday_date 
          WHERE weekend IS NOT NULL AND three_shift_employee.emp_id='$employees->id'
          ORDER BY three_shift_employee.shift_date ASC");
          foreach($data as $s_status){
              DB::table('emp_shift_status')->insert([
                  'emp_id'=>$s_status->emp_id,
                  'weekend'=>$s_status->weekend,
                  'present'=>$s_status->date,
                  'leave_date'=>$s_status->leave_date,
                  'holiday'=>$s_status->holiday_date,
                  'regular'=>$s_status->weekend_date,
                  'shift_date'=>$s_status->shift_date
              ]);
           }
          $join_date=date('Y-m',strtotime($employees->empJoiningDate));
          $discontinuation_date=date('Y-m',strtotime($employees->date_of_discontinuation));
          $ot_status=$employees->empOTStatus;

          if($join_date==$year_month){
            $shift_emp[$employees->id]=DB::table('three_shift_employee')
            ->where('three_shift_employee.emp_id',$employees->id)
            ->whereBetween('shift_date',[$employees->empJoiningDate,$end])
            ->select('three_shift_employee.emp_id','shift_date')
            ->orderBy('three_shift_employee.shift_date','ASC')
            ->get();
            if($join_date==$year_month && $discontinuation_date==$year_month){
                $shift_emp=DB::table('three_shift_employee')
                ->where('three_shift_employee.emp_id',$employees->id)
                ->whereBetween('shift_date',[$employees->empJoiningDate,$employees->date_of_discontinuation])
                ->select('three_shift_employee.emp_id','shift_date')
                ->orderBy('three_shift_employee.shift_date','ASC')
                ->get();
            }
          }
          elseif($discontinuation_date==$year_month){
            $shift_emp[$employees->id]=DB::table('three_shift_employee')
            ->where('three_shift_employee.emp_id',$employees->id)
            ->whereBetween('shift_date',[$start,$employees->date_of_discontinuation])
            ->orderBy('three_shift_employee.shift_date','ASC')
            ->get();
          }else{
            $shift_emp[$employees->id]=DB::table('three_shift_employee')
            ->where('three_shift_employee.emp_id',$employees->id)
            ->whereBetween('shift_date',[$start,$end])
            ->select('three_shift_employee.emp_id','shift_date')
            ->orderBy('three_shift_employee.shift_date','ASC')
            ->get();
          }

        }
        return view('report.attendance.three_shift.job_card_all_data',compact('start','end','employee','shift_emp'));
        
  }






}
