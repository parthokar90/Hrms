<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class FloorController extends Controller
{
    function __construct()
    {
        $this->middleware(['middleware'=>'check-permission:admin|hr|hr-admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $floors=DB::table('floors')->orderBy('id','asc')->get();
        $floors=DB::table('floors')->get();
        return view('settings.floor.index',compact('floors'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'floor'=>'required',
        ]);
        DB::table('floors')->insert([
            'floor'=>$request->floor,
        ]);
        Session::flash('message','Floor added');
        return redirect(route('floor.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $floor=DB::table('floors')->where('id','=',$id)->first();
        return view('settings.floor.show',compact('floor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $floor=DB::table('floors')->where('id','=',$id)->first();
        return view('settings.floor.editForm',compact('floor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'floor'=>'required',
        ]);
        DB::table('floors')->where(['id'=>$id])->update([
            'floor'=>$request->floor,
        ]);
        Session::flash('edit','Floor information updated successfully');
        return redirect(route('floor.index'));
    }

    public function showDelete($id){
        $floor=DB::table('floors')->where(['id'=>$id])->first();
        return view('settings.floor.deleteForm',compact('floor'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $line=DB::table('tblines')->where(['floor_id'=>$id])->get();
        if(count($line)){
            Session::flash('delete',"Can't delete. This floor have some lines.");
            return redirect(route('floor.index'));

        }
        else {
            DB::table('floors')->where(['id' => $id])->delete();
            Session::flash('message', 'Floor information has been deleted');
            return redirect(route('floor.index'));
        }
    }
}
