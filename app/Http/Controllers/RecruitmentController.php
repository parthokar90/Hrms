<?php

namespace App\Http\Controllers;

use App\Helper\AppHelper;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class RecruitmentController extends Controller
{

     function __construct()
     {
         $this->middleware(['middleware'=>'check-permission:admin|hr|executive|hr-admin']);
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacancies=DB::table('vacancies')->latest()->get();
        return view('recruitment.vacancyList',compact('vacancies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recruitment.vacancyCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->VacAnnounceStartingDate)){
            Session::flash('message','Incorrect date format.');
            return view('recruitment.vacancyCreate');
        }
        elseif(!$helper->checkIsAValidDate($request->vacAnnounceEndingDate)){
            Session::flash('message','Incorrect date format.');
            return view('recruitment.vacancyCreate');
        }
        elseif(!$helper->checkIsAValidDate($request->vacJoiningDate)){
            Session::flash('message','Incorrect date format.');
            return view('recruitment.vacancyCreate');
        }

        $this->validate($request,[
            'vacTitle'=>'required',
            'vacNumber'=>'required',
            'VacAnnounceStartingDate'=>'required',
            'vacAnnounceEndingDate'=>'required',

        ]);

        $store=DB::table('vacancies')->insert([
            'vacTitle'=>$request->vacTitle,
            'vacDescription'=>$request->vacDescription,
            'vacNumber'=>$request->vacNumber,
            'VacAnnounceStartingDate'=>Carbon::parse($request->VacAnnounceStartingDate)->format('Y-m-d H:i:s'),
            'vacAnnounceEndingDate'=>Carbon::parse($request->vacAnnounceEndingDate)->format('Y-m-d H:i:s'),
            'vacJoiningDate'=>Carbon::parse($request->vacJoiningDate)->format('Y-m-d H:i:s'),
            'vacStatus'=>$request->vacStatus,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString(),
            'created_by'=>Auth::user()->name,
            'updated_by'=>Auth::user()->name,

        ]);

        if($store)
        {
            Session::flash('store','Vacancy Created Successfully');
        }
        return redirect('recruitment/vacancy');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $v=DB::table('vacancies')->where(['id'=>$id])->first();
        $applications =DB::table('vacancy_application')->where(['vacancy_id'=>$id])->get();
        return view('recruitment.vacancyDetails', compact('v','applications'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->VacAnnounceStartingDate)){
            Session::flash('message','Incorrect date format.');
            return $this->show($id);
        }
        elseif(!$helper->checkIsAValidDate($request->vacAnnounceEndingDate)){
            Session::flash('message','Incorrect date format.');
            return $this->show($id);

        }
        elseif(!$helper->checkIsAValidDate($request->vacJoiningDate)){
            Session::flash('message','Incorrect date format.');
            return $this->show($id);

        }
        $this->validate($request, [
            'vacTitle'=>'required',
            'vacNumber'=>'required',
            'VacAnnounceStartingDate'=>'required',
            'vacAnnounceEndingDate'=>'required',
        ]);

        $up=DB::table('vacancies')->where(['id'=>$id])->update([
            'vacTitle'=>$request->vacTitle,
            'vacDescription'=>$request->vacDescription,
            'vacNumber'=>$request->vacNumber,
            'VacAnnounceStartingDate'=>Carbon::parse($request->VacAnnounceStartingDate)->format('Y-m-d H:i:s'),
            'vacAnnounceEndingDate'=>Carbon::parse($request->vacAnnounceEndingDate)->format('Y-m-d H:i:s'),
            'vacJoiningDate'=>Carbon::parse($request->vacJoiningDate)->format('Y-m-d H:i:s'),
            'vacStatus'=>$request->vacStatus,
            'updated_at'=>Carbon::now()->toDateTimeString(),
            'updated_by'=>Auth::user()->name,

        ]);
        if($up)
        {
            Session::flash('up','Vacancy Info successfully updated');
        }
        return redirect('recruitment/vacancy/'.$id);
        //return $request->all();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $up=DB::table('vacancies')->where(['id'=>$id])->delete();
        if($up){
            Session::flash('msg','Vacancy deleted');
        }
        return redirect('recruitment/vacancy');

    }

    public function vacancyApplication(){
        $vacancies=DB::table('vacancies')->where(['vacStatus'=>'1'])->pluck('vacTitle','id')->all();
        return view('recruitment.vacancyApplication',compact('vacancies'));

    }
    public function storeVacancyApplication(Request $request){

        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->submittedDate)){
            Session::flash('message','Incorrect date format.');
            return $this->vacancyApplication();
        }

        $this->validate($request,[
            'name'=>'required',
            'vacancy_id'=>'required',
            'attachment'=>'required',
        ]);
        if($file=$request->file('attachment')){
//            $request->file('emp')->move('','name');
            if($file->getClientSize()<5000000){
//                return Carbon::parse(null)->format('Y-m-d H:i:s');
                if($request->interviewDate==null){
                    $interviewDate=null;
//                    return "yes";
                }
                else{
                    if(!$helper->checkIsAValidDate($request->interviewDate)){
                        Session::flash('message','Incorrect date format.');
                        return $this->vacancyApplication();
                    }
                    $interviewDate=Carbon::parse($request->interviewDate)->format('Y-m-d H:i:s');
//                    return 'no';
                }
                $name=time()."_".$request->id."_".$file->getClientOriginalName();
                $file->move('Applications',$name);
                $store=DB::table('vacancy_application')->insert([
                    'name'=>$request->name,
                    'phone'=>$request->phone,
                    'vacancy_id'=>$request->vacancy_id,
                    'submittedDate'=>Carbon::parse($request->submittedDate)->format('Y-m-d H:i:s'),
                    'interviewStatus'=>$request->interviewStatus,
                    'interviewDate'=>$interviewDate,
                    'interviewNote'=>$request->interviewNote,
                    'priorityStatus'=>$request->priorityStatus,
                    'attachment'=>$name,
                    'jobStatus'=>$request->jobStatus,

                ]);

            }
            else{
                Session::flash('file_size','File Size Limit Exceeded');
                return redirect('recruitment/application/create');
            }
        }
        Session::flash('store','Application successful.');

        return redirect('recruitment/application');
    }

    public function indexVacancyApplication(){
        $applications=DB::table('vacancy_application')
            ->join('vacancies','vacancy_application.vacancy_id',"=","vacancies.id")
            ->select('vacancy_application.*','vacancies.vacTitle')
            ->latest()->get();

        return view('recruitment.indexVacancyApplication',compact('applications'));
    }

    public function dailyRecruitment(){
        return view('recruitment.dailyRecruitment');
    }

    public function recruitmentStatus($date){
        $applications=DB::table('vacancy_application')
            ->join('vacancies','vacancy_application.vacancy_id',"=",'vacancies.id')
            ->select('vacancy_application.*','vacancies.vacTitle')
            ->where(['submittedDate'=>$date])
            ->latest()
            ->get();
        return view('recruitment.recruitmentStatus',compact('applications'));

    }

    public function detailsVacancyApplication($id){
        $vacancies=DB::table('vacancies')->pluck('vacTitle','id')->all();
        $application=DB::table('vacancy_application')
            ->join('vacancies','vacancy_application.vacancy_id','=','vacancies.id')
            ->select('vacancy_application.*','vacancies.vacTitle')
            ->where(['vacancy_application.id'=>$id])
            ->first();
//        return $application;
        return view('recruitment.detailsVacancyApplication',compact('application','vacancies'));

    }

    public function download_job_application($id)
    {
        $v=DB::table('vacancies')->where(['id'=>$id])->first();
//        return view('report.pdf.company.job_application',compact('v'));
        $pdf=PDF::loadView('report.pdf.company.job_application',compact('v'));
        $name=time()."_download_job_application.pdf";
        return $pdf->download($name);

    }

    public function applicationUpdate(Request $request,$id){
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->submittedDate)){
            Session::flash('message','Incorrect date format.');
            return $this->detailsVacancyApplication($id);
        }
        $this->validate($request,[
            'name'=>'required',
            'vacancy_id'=>'required',
        ]);
        if($request->interviewDate==null){
            $interviewDate=null;
//            echo "yes";
        }
        else{
            if(!$helper->checkIsAValidDate($request->interviewDate)){
                Session::flash('message','Incorrect date format.');
                return $this->detailsVacancyApplication($id);
            }
            $interviewDate=Carbon::parse($request->interviewDate)->format('Y-m-d H:i:s');
        }
//        return $interviewDate;

        if($file=$request->file('attachment')){
            if($file->getClientSize()<5000000){
                $name=time()."_".$id."_".$file->getClientOriginalName();
                $file->move('Applications',$name);
                $path = public_path() . "/Applications/" . DB::table('vacancy_application')->where(['id'=>$id])->first()->attachment;
                if (file_exists($path)) {
//                    return $path;
                    unlink($path);
                }
                $up=DB::table('vacancy_application')->where(['id'=>$id])->update([
                    'name'=>$request->name,
                    'phone'=>$request->phone,
                    'vacancy_id'=>$request->vacancy_id,
                    'submittedDate'=>Carbon::parse($request->submittedDate)->format('Y-m-d H:i:s'),
                    'interviewStatus'=>$request->interviewStatus,
                    'interviewDate'=>$interviewDate,
                    'interviewNote'=>$request->interviewNote,
                    'priorityStatus'=>$request->priorityStatus,
                    'attachment'=>$name,
                    'jobStatus'=>$request->jobStatus,
                ]);


            }
            else{
                Session::flash('file_size','File Size Limit Exceeded');
                return redirect('recruitment/application/'.$id);
            }

        }
        else{
            $up=DB::table('vacancy_application')->where(['id'=>$id])->update([
                'name'=>$request->name,
                'phone'=>$request->phone,
                'vacancy_id'=>$request->vacancy_id,
                'submittedDate'=>Carbon::parse($request->submittedDate)->format('Y-m-d H:i:s'),
                'interviewStatus'=>$request->interviewStatus,
                'interviewDate'=>$interviewDate,
                'interviewNote'=>$request->interviewNote,
                'priorityStatus'=>$request->priorityStatus,
                'jobStatus'=>$request->jobStatus,
            ]);

        }

        Session::flash('up','Applicant information successfully updated.');

        return redirect('recruitment/application/'.$id);


    }
    public function applicationDelete($id){
        $path = public_path() . "/Applications/" . DB::table('vacancy_application')->where(['id'=>$id])->first()->attachment;
        if (file_exists($path)) {
//                    return $path;
            unlink($path);
        }
        $del=DB::table('vacancy_application')->where(['id'=>$id])->delete();
        if($del){

            Session::flash('del','Application delete successful');
        }
        return redirect("recruitment/application");

    }
}
