<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SkillTestController extends Controller
{
    function __construct()
    {
        $this->middleware(['middleware'=>'check-permission:admin|hr|hr-admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'skill_name'=>'required',
            'performance'=>'required',
        ]);

        if($file=$request->file('attachment'))
        {
            if($file->getClientSize()<5000000) {
                $name = time() . "_" . session('emp_id') . "_" . $file->getClientOriginalName();
                $file->move('Employee_skill_test', $name);
            }
            else{
                Session::flash('fileSize','File size limit exceeded');
                return redirect('employee/'.session('emp_id'));
            }
        }
        else{
            $name=null;
        }

        $skillTest=DB::table('tb_employee_skill_test')->insert([
            'emp_id'=>session('emp_id'),
            'skill_name'=>$request->skill_name,
            'performance'=>$request->performance,
            'test_date'=>Carbon::parse($request->test_date)->format('Y-m-d H:i:s'),
            'skill_Description'=> $request->skill_Description,
            'attachment'=>$name,
            'created_by'=>Auth::user()->name,
            'modified_by'=>Auth::user()->name,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        if($skillTest){
            Session::flash('message','Successfully inserted skill');
        }
        return redirect('employee/'.session('emp_id'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $we=DB::table('tb_employee_skill_test')->where(['id'=>$id])->first();
        return view("employee.modal.Skill.editForm",compact('we','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'skill_name'=>'required',
            'performance'=>'required',
        ]);

        $table=DB::table('tb_employee_skill_test')->where(['id'=>$id]);

        if($file=$request->file('attachment'))
        {
            if($file->getClientSize()<5000000) {
                $name = time() . "_" . session('emp_id') . "_" . $file->getClientOriginalName();
                $file->move('Employee_skill_test', $name);
                if($table->first()->attachment) {
                    $path = public_path() . "/Employee_skill_test/" . $table->first()->attachment;
                    if (file_exists($path)) {
//                    return $path;
                        unlink($path);
                    }
                }
            }
            else{
                Session::flash('fileSize','File size limit exceeded');
                return redirect('employee/'.session('emp_id'));
            }
        }
        else{
            $name=$table->first()->attachment;

        }
        $store=DB::table('tb_employee_skill_test')->where(['id'=>$id])->update([
            'emp_id'=>session('emp_id'),
            'skill_name'=>$request->skill_name,
            'performance'=>$request->performance,
            'test_date'=>Carbon::parse($request->test_date)->format('Y-m-d H:i:s'),
            'skill_Description'=> $request->skill_Description,
            'attachment'=>$name,
            'created_by'=>Auth::user()->name,
            'modified_by'=>Auth::user()->name,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        if($store){
            Session::flash('message',"Skill test information updated Successfully");
        }

        return redirect('employee/'.session('emp_id'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showDelete($id){
        $we=DB::table('tb_employee_skill_test')->where(['id'=>$id])->first();
        return view('employee.modal.Skill.deleteForm',compact('id','we'));
    }
    public function destroy($id)
    {
        $table=DB::table('tb_employee_skill_test')->where(['id'=>$id]);
        if($table->first()->attachment) {
            $path = public_path() . "/Employee_skill_test/" . $table->first()->attachment;
            if (file_exists($path)) {
//                    return $path;
                unlink($path);
            }
        }
        $delete=$table->delete();
        if($delete){
            Session::flash('delete','Data successfully removed');
        }

        return redirect('employee/'.session('emp_id'));

    }
}
