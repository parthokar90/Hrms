<?php

namespace App\Http\Controllers;

use App\Helper\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Symfony\Component;
use mPDF;
use Excel;



class expenseController extends Controller
{
    public function index(){
        $today=date("Y-m-d");
        $catagory=DB::table('tbexpensecategory')->select('categoryName','id')->get();
        $expenseList=DB::table('tbexpenselist')
        ->join('tbexpensecategory','tbexpensecategory.id','=','tbexpenselist.categoryId')
        ->select('tbexpensecategory.*','tbexpenselist.*','tbexpenselist.id as listID')
        ->where(['tbexpenselist.expenseDate'=>$today])
        ->get()->reverse();
        //dd($expenseList);
        return view('expense.index',compact('catagory','expenseList'));
    }
    public function addDailyExpense(Request $request){
        $current_time = Carbon::now()->toDateTimeString();
        if($file=$request->file('attachment')) {
            $name = time() . "_" .$request->title. "_".$request->amount.$file->getClientOriginalName() ;
            $file->move('Expense_Attachment', $name);
        }
        else{
            $name=null;
        }
        $addDailyExpense=DB::table('tbexpenselist')->insert([
            'title' =>$request->expenseReason,
            'categoryId' =>$request->catagoryID, 
            'amount' =>$request->amount, 
            'reference' =>$request->reference, 
            'description' =>$request->description, 
            'expenseDate' =>date("Y-m-d",strtotime($request->date)), 
            'attachment'=>$name,
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if( $addDailyExpense){
            Session::flash('addExpense', 'Expense Successfully Added!');
            return redirect()->back();
        }
    }

    public function deleteDailyExpense($id){
        //dd($id);
        $data=DB::table('tbexpenselist')->WHERE('id',$id)->delete();
       // Storage::delete($filename);
        if($data){
            Session::flash('deleteExpense', 'Expense Successfully Deleted!');
            return redirect()->back();
        }
    }

    public function expenseSettings(){
        $expenseData=DB::table('tbexpensecategory')->latest()->get();
        //dd($expenseData);
        return view('expense.expenseSettings',compact('expenseData'));

    }

    public function expenseAdd(Request $request){
        $current_time = Carbon::now()->toDateTimeString();
        $expenseCatagory=DB::table('tbexpensecategory')->insert([
            'categoryName' =>$request->catagoryName,
            'categoryDescription' =>$request->catagoryDescription, 
            'created_at' =>$current_time,
            'updated_at' =>$current_time,
        ]);
        if( $expenseCatagory){
            Session::flash('successMessage', 'Expense Category Store Successful!');

        }
        return redirect()->back();
    }
    public function expenseUpdate(Request $request){
        $id=$request->expense_id;
        
        $current_time = Carbon::now()->toDateTimeString();
        $expUpdate=DB::table('tbexpensecategory')->WHERE('id',$id)->update([
            'categoryName' =>$request->catagoryName,
            'categoryDescription' =>$request->catagoryDescription, 
            'updated_at' =>$current_time,
        ]);
        
        if($expUpdate){
            Session::flash('successMessage', 'Expense Category Updated Successfully!');

        }
        return redirect()->back();
    }
    public function expenseDelete($id){
        
        $data=DB::table('tbexpensecategory')->WHERE('id',$id)->delete();
        if($data){
            Session::flash('successMessage', 'Expense Category Delete Successful!');
            return redirect()->back();
        }
    }

    public function expenseHistory(Request $request){
        $startdate=date("Y-m-d",strtotime($request->expense_start_date));
        $enddate=date("Y-m-d",strtotime($request->expense_end_date));
        //dd($startdate,$enddate);
        $expenseHistory=DB::table('tbexpenselist')
        ->join('tbexpensecategory','tbexpensecategory.id','=','tbexpenselist.categoryId')
        ->select('tbexpensecategory.*','tbexpenselist.*','tbexpenselist.id as listID')
        //->where(['tbexpenselist.expenseDate'>$startdate])
        //->where(['tbexpenselist.expenseDate'<$enddate])
        ->where('tbexpenselist.expenseDate','>=',$startdate)
        ->where('tbexpenselist.expenseDate','<=',$enddate)
        ->get()->reverse();
        
        //dd($expenseHistory);
        return view('expense.expenseHistory',compact('expenseHistory','request'));
    }
    public function expenseDateSelect(){
        return view('expense.expenseHistoryDate');
    }
    public function expense_report(){
        $categoryList=DB::table('tbexpensecategory')->get(['id','categoryName']);
        return view('expense.expense_report',compact('categoryList'));
    }

    public function expense_report_data(Request $request){
//        return $request->all();
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->start_date)){
            Session::flash('message','Incorrect date format.');
            return $this->expense_report();
        }
        elseif(!$helper->checkIsAValidDate($request->end_date)){
            Session::flash('message','Incorrect date format.');
            return $this->expense_report();
        }
        $start_date=Carbon::parse($request->start_date)->toDateString();
        $end_date=Carbon::parse($request->end_date)->toDateString();
        if($request->category==0) {
            $data=DB::table('tbexpenselist')
                ->join('tbexpensecategory','categoryId','=','tbexpensecategory.id')
                ->whereBetween('tbexpenselist.expenseDate',[$start_date,$end_date])
                ->get(['tbexpenselist.id','tbexpenselist.title',
                    'tbexpenselist.amount','tbexpenselist.reference','tbexpenselist.attachment',
                    'tbexpenselist.description','tbexpenselist.expenseDate',
                    'tbexpensecategory.categoryName']);
//            return $data;
        }
        else{
            $data=DB::table('tbexpenselist')
                ->join('tbexpensecategory','categoryId','=','tbexpensecategory.id')
                ->where('tbexpensecategory.id','=',$request->category)
                ->whereBetween('tbexpenselist.expenseDate',[$start_date,$end_date])
                ->get(['tbexpenselist.id','tbexpenselist.title',
                    'tbexpenselist.amount','tbexpenselist.reference', 'tbexpenselist.attachment',
                    'tbexpenselist.description','tbexpenselist.expenseDate',
                    'tbexpensecategory.categoryName']);
//            return $data;

        }
        if($request->viewType=="Preview") {
            return view('expense.expense_report_data', compact('data', 'request' ));
        }
        elseif($request->viewType=="Generate PDF"){
            $pdf = mPDF::loadView('report.pdf.expense_report_pdf', compact('data','request'));
//            $pdf->setPaper('A4', 'portrait');
            $name = time() . "_expense_report.pdf";
            return $pdf->download($name);

        }
        elseif($request->viewType=="Generate Excel"){
            $excelName=time()."_expense_report";
            Excel::create("$excelName", function($excel) use ($data, $request) {

                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Expense Report");


                // Chain the setters
                $excel->setCreator($name);

                $excel->setDescription('Expense Report');

                $excel->sheet('Sheet 1', function ($sheet) use ($data, $request) {
                    $sheet->loadView('report.excel.expense_report_excel')
                        ->with('data',$data)
                        ->with('request',$request);
                });

            })->download($request->extensions);
        }
    }
}
