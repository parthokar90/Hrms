<?php

namespace App\Http\Controllers;

use App\Helper\AppHelper;
use Dompdf\Exception;
use NumberFormatter;
use PDF;
use mPDF;
use Illuminate\Support\Facades\Mail;
use App\Mail\mailSender;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DateTime;
use Illuminate\Support\Facades\Input;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['middleware'=>'check-permission:admin|hr|executive|hr-admin']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.id',
                'employees.employeeId','employees.empJoiningDate','employees.empSection',
                'employees.empGenderId','designations.designation','units.name as unitName',
                'departments.departmentName','employees.empAccStatus')
            // ->get();
            ->orderBy('employees.employeeId', 'ASC')
            ->paginate(100);
        return view('employee.index',compact('employees','designations','departments','units'));
    }

    public function delete_employee_list(){
        $now=Carbon::now()->toDateString();
        $past=Carbon::now()->subDays(30)->toDateString();
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->whereBetween('date_of_discontinuation',[$past,$now])
            ->whereBetween('empJoiningDate',[$past,$now])
            ->select('employees.empFirstName','employees.empLastName','employees.id',
                'employees.employeeId','employees.empJoiningDate','employees.empSection',
                'employees.empGenderId','designations.designation','units.name as unitName','employees.date_of_discontinuation',
                'departments.departmentName','employees.empAccStatus')
            ->get();
        return view('employee.delete_employee_list',compact('employees'));

    }

    public function employee_confirm_delete($id){
        $employee=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->where('employees.id','=', base64_decode($id))
            ->select('employees.empFirstName','employees.empLastName','employees.id',
                'employees.employeeId','employees.empJoiningDate','employees.empSection',
                'employees.empGenderId','designations.designation','units.name as unitName','employees.date_of_discontinuation',
                'departments.departmentName','employees.empAccStatus','employees.empCardNumber')
            ->first();
        return view('employee.employee_confirm_delete',compact('employee'));

    }

    public function employee_confirmed_delete($id){
        $id=base64_decode($id);
        DB::table('employees')->where('id','=',$id)->delete();
        DB::table('attendance')->where('emp_id','=',$id)->delete();
        DB::table('employees_bonus')->where('emp_id','=',$id)->delete();
        DB::table('employee_attachments')->where('emp_id','=',$id)->delete();
        DB::table('employee_daily_salary')->where('emp_id','=',$id)->delete();
        DB::table('employee_education_infos')->where('emp_id','=',$id)->delete();
        DB::table('employee_training_history')->where('emp_id','=',$id)->delete();
        DB::table('employee_working_histories')->where('emp_id','=',$id)->delete();
        DB::table('emp_manual_overtime')->where('emp_overtime_id','=',$id)->delete();
        DB::table('festival_bonus')->where('emp_id','=',$id)->delete();
        DB::table('leave_of_employee')->where('emp_id','=',$id)->delete();
        DB::table('leave_of_employee')->where('emp_id','=',$id)->delete();
        DB::table('manual_earn_leave')->where('emp_id','=',$id)->delete();
        DB::table('nominees')->where('emp_id','=',$id)->delete();
        DB::table('payroll_salary')->where('emp_id','=',$id)->delete();
        DB::table('tbemployee_medical_history')->where('empId','=',$id)->delete();
        DB::table('tbemployee_living_history_data')->where('empId','=',$id)->delete();
        DB::table('tbpolice_verification_data')->where('empId','=',$id)->delete();
        DB::table('tb_advance_salary')->where('emp_id','=',$id)->delete();
        DB::table('tb_deduction')->where('emp_id','=',$id)->delete();
        DB::table('tb_employee_skill_test')->where('emp_id','=',$id)->delete();
        DB::table('tb_employee_leave')->where('employee_id','=',$id)->delete();
        DB::table('tb_employee_training')->where('employee_id','=',$id)->delete();
        DB::table('tb_leave_application')->where('employee_id','=',$id)->delete();
        DB::table('tb_loan_deduction')->where('emp_id','=',$id)->delete();
        DB::table('tb_production_bonus')->where('emp_id','=',$id)->delete();
        DB::table('tb_maternity_allowance')->where('employee_id','=',$id)->delete();
        DB::table('tb_salary_history')->where('emp_id','=',$id)->delete();
        DB::table('users')->where('emp_id','=',$id)->delete();

        Session::flash('success','Employee deleted.');

        return redirect(route('employee.delete_employee_list'));
    }

    public function employee_id_check($id){
        $emp=DB::table('employees')->where('employeeId','=',$id)->count();
        return $emp;

    }

    public function employee_card_number_check($id){
        $emp=DB::table('employees')->where('empCardNumber','=',$id)->count();
        return $emp;

    }

    public function employee_id_check_edit($eid,$rid){
        $emp=DB::table('employees')->where([['employeeId','=',$eid],['id','!=',$rid]])->count();
        return $emp;

    }

    public function employee_card_number_check_edit($cid,$rid){
        $emp=DB::table('employees')->where([['empCardNumber','=',$cid],['id','!=',$rid]])->count();
        return $emp;

    }


    public function employee_search(Request $request)
    {
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.id',
                'employees.employeeId','employees.empJoiningDate','employees.empSection',
                'employees.empGenderId','designations.designation','units.name as unitName',
                'departments.departmentName','employees.empAccStatus')
            ->orwhere('employeeId','like','%'.$request->keyword.'%')
            ->orwhere('empFirstName','like','%'.$request->keyword.'%')
            ->orwhere('empLastName','like','%'.$request->keyword.'%')
            ->orwhere('empPhone','like','%'.$request->keyword.'%')
            ->orwhere('empEmail','like','%'.$request->keyword.'%')
            ->orderBy('employees.employeeId', 'ASC')
            ->get();
        return view('employee.search_employee',compact('employees'));
    }    

    public function filter(Request $request)
    {
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (!empty($request->unitId)) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->empSection)) {
            $query.=" AND employees.empSection='".$request->empSection."'";
        }
        
        $query.=" order by employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));

        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();

        $filter = "yes";
        return view('employee.filter_employee_list',compact('employees','designations','departments','units','filter','request'));
    }


 public function grid_view_index()
    {
        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.empPhoto','employees.empPhone','employees.id','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empGenderId','designations.designation','units.name as unitName','departments.departmentName')
            ->orderBy('employees.employeeId', 'ASC')
            ->paginate(66);
        return view('employee.grid_index',compact('employees','designations','departments','units'));
    }   

    public function grid_employee_search(Request $request)
    {
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.empPhoto','employees.empPhone','employees.id','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empGenderId','designations.designation','units.name as unitName','departments.departmentName')
            ->orwhere('employeeId','like','%'.$request->keyword.'%')
            ->orwhere('empFirstName','like','%'.$request->keyword.'%')
            ->orwhere('empLastName','like','%'.$request->keyword.'%')
            ->orwhere('empPhone','like','%'.$request->keyword.'%')
            ->orwhere('empEmail','like','%'.$request->keyword.'%')
            ->orderBy('employees.employeeId', 'ASC')
            ->get();
        return view('employee.search_grid_employee',compact('employees','request'));
    }    

    public function grid_filter(Request $request)
    {
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.id>0 ";

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (!empty($request->unitId)) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->empSection)) {
            $query.=" AND employees.empSection='".$request->empSection."'";
        }
        $query.=" order by employees.employeeId ASC";
        
        $employees= DB::select(DB::raw($query));

        // $employees = $this->arrayPaginator($employees, $request);
        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();

        $filter = "yes";

        // return $request;
        return view('employee.filter_grid_employee_list',compact('employees','designations','departments','units','filter','request'));
    }


    public function active_employee_list()
    {
        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.empAccStatus','employees.id','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empGenderId','designations.designation','units.name as unitName','departments.departmentName')
            ->where('empAccStatus','=','1')
            ->orderBy('employees.employeeId', 'ASC')
            ->paginate(100);
        return view('employee.active_employee_list',compact('employees','designations','departments','units'));
    }   

    public function active_employee_search(Request $request)
    {
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.empAccStatus','employees.id','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empGenderId','designations.designation','units.name as unitName','departments.departmentName')
            ->orwhere('employeeId','like','%'.$request->keyword.'%')
            ->orwhere('empFirstName','like','%'.$request->keyword.'%')
            ->orwhere('empLastName','like','%'.$request->keyword.'%')
            ->orwhere('empPhone','like','%'.$request->keyword.'%')
            ->orwhere('empEmail','like','%'.$request->keyword.'%')
            ->where('employees.empAccStatus','=','1')
            ->orderBy('employees.employeeId', 'ASC')
            ->get();
        return view('employee.search_active_employee',compact('employees'));
    }    

    public function filter_active_employee(Request $request)
    {
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.empAccStatus=1 ";

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (!empty($request->unitId)) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->empSection)) {
            $query.=" AND employees.empSection='".$request->empSection."'";
        }
        
        $query.=" order by employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));

        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();

        $filter = "yes";
        return view('employee.filter_active_employee_list',compact('employees','designations','departments','units','filter','request'));
    }


    public function inactive_employee_list()
    {
        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.empAccStatus','employees.id','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empGenderId','designations.designation','units.name as unitName','departments.departmentName')
            ->where('empAccStatus','=','0')
            ->orderBy('employees.employeeId', 'ASC')
            ->paginate(100);
        return view('employee.inactive_employee_list',compact('employees','designations','departments','units'));
    }   

    public function inactive_employee_search(Request $request)
    {
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.empAccStatus','employees.id','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empGenderId','designations.designation','units.name as unitName','departments.departmentName')
            ->orwhere('employeeId','like','%'.$request->keyword.'%')
            ->orwhere('empFirstName','like','%'.$request->keyword.'%')
            ->orwhere('empLastName','like','%'.$request->keyword.'%')
            ->orwhere('empPhone','like','%'.$request->keyword.'%')
            ->orwhere('empEmail','like','%'.$request->keyword.'%')
            ->where('employees.empAccStatus','=','0')
            ->orderBy('employees.employeeId', 'ASC')
            ->get();
        return view('employee.search_inactive_employee',compact('employees'));
    }    

    public function filter_inactive_employee(Request $request)
    {
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.empAccStatus=0 ";

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (!empty($request->unitId)) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->empSection)) {
            $query.=" AND employees.empSection='".$request->empSection."'";
        }
        
        $query.=" order by employees.employeeId ASC";

        $employees= DB::select(DB::raw($query));

        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();

        $filter = "yes";
        return view('employee.filter_inactive_employee_list',compact('employees','designations','departments','units','filter','request'));
    }


    public function male_employee_list()
    {
        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.empAccStatus','employees.id','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empGenderId','designations.designation','units.name as unitName','departments.departmentName')
            ->where('empGenderId','=','1')
            ->orderBy('employees.employeeId', 'ASC')
            ->paginate(100);
        return view('employee.male_employee_list',compact('employees','designations','departments','units'));
    }   

    public function male_employee_search(Request $request)
    {
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.empAccStatus','employees.id','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empGenderId','designations.designation','units.name as unitName','departments.departmentName')
            ->orwhere('employeeId','like','%'.$request->keyword.'%')
            ->orwhere('empFirstName','like','%'.$request->keyword.'%')
            ->orwhere('empLastName','like','%'.$request->keyword.'%')
            ->orwhere('empPhone','like','%'.$request->keyword.'%')
            ->orwhere('empEmail','like','%'.$request->keyword.'%')
            ->where('employees.empGenderId','=','1')
            ->orderBy('employees.employeeId', 'ASC')
            ->get();
        return view('employee.search_male_employee',compact('employees'));
    }    

    public function filter_male_employee(Request $request)
    {
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.empGenderId=1 ";

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (!empty($request->unitId)) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->empSection)) {
            $query.=" AND employees.empSection='".$request->empSection."'";
        }

        $query.=" order by employees.employeeId ASC";
        
        $employees= DB::select(DB::raw($query));

        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();

        $filter = "yes";
        return view('employee.filter_male_employee_list',compact('employees','designations','departments','units','filter','request'));
    }


    public function female_employee_list()
    {
        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.empAccStatus','employees.id','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empGenderId','designations.designation','units.name as unitName','departments.departmentName')
            ->where('empGenderId','=','2')
            ->orderBy('employees.employeeId', 'ASC')
            ->paginate(100);
        return view('employee.female_employee_list',compact('employees','designations','departments','units'));
    }   

    public function female_employee_search(Request $request)
    {
        $employees=DB::table('employees')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('units','employees.unit_id','=','units.id')
            ->select('employees.empFirstName','employees.empLastName','employees.empAccStatus','employees.id','employees.employeeId','employees.empJoiningDate','employees.empSection','employees.empGenderId','designations.designation','units.name as unitName','departments.departmentName')
            ->orwhere('employeeId','like','%'.$request->keyword.'%')
            ->orwhere('empFirstName','like','%'.$request->keyword.'%')
            ->orwhere('empLastName','like','%'.$request->keyword.'%')
            ->orwhere('empPhone','like','%'.$request->keyword.'%')
            ->orwhere('empEmail','like','%'.$request->keyword.'%')
            ->where('employees.empGenderId','=','2')
            ->orderBy('employees.employeeId', 'ASC')
            ->get();
        return view('employee.search_female_employee',compact('employees'));
    }    

    public function filter_female_employee(Request $request)
    {
        $query=@"SELECT employees.*,designations.designation,departments.departmentName,units.name unitName FROM employees LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id LEFT JOIN units ON employees.unit_id=units.id WHERE employees.empGenderId=2 ";

        if (($request->departmentId)!=0) {
            $query.=" AND employees.empDepartmentId='".$request->departmentId."'";
        }

        if (($request->designationId)!=0) {
            $query.=" AND employees.empDesignationId='".$request->designationId."'";
        }

        if (!empty($request->unitId)) {
            $query.=" AND employees.unit_id='".$request->unitId."'";
        }

        if (!empty($request->empSection)) {
            $query.=" AND employees.empSection='".$request->empSection."'";
        }
        
        $query.=" order by employees.employeeId ASC";
        
        $employees= DB::select(DB::raw($query));

        $designations=DB::table('designations')->get();
        $departments=DB::table('departments')->get();
        $units=DB::table('units')->get();

        $filter = "yes";
        return view('employee.filter_female_employee_list',compact('employees','designations','departments','units','filter','request'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $designations=DB::table('designations')->pluck('designation','id')->all();
        $shifts=DB::table('attendance_setup')->get();
        $attendance_bonus=DB::table('tbattendance_bonus')->get();
        $departments=DB::table('departments')->pluck('departmentName','id')->all();
        $maritalStatuses=DB::table('marital_statuses')->pluck('name','id')->all();
        $nationalities=DB::table('nationalities')->pluck('name','id')->all();
        $units=DB::table('units')->orderBy('name','asc')->pluck('name','id')->all();
        $floors=DB::table('floors')->pluck('floor','id')->all();
        $lines=DB::table('tblines')->orderBy('line_no','asc')->pluck('line_no','id')->all();
        return view('employee.create',compact('designations','departments','maritalStatuses','nationalities','units','lines','floors','shifts','attendance_bonus'));

    }

    public function bangla_information($id)
    {
        $id=base64_decode($id);
        
        $employee=DB::table('employees')
            ->leftJoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftJoin('marital_statuses','employees.empMaritalStatusId','=','marital_statuses.id')
            ->leftJoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftJoin('nationalities','employees.empNationalityId','=','nationalities.id')
            ->leftJoin('units','employees.unit_id','=','units.id')
            ->leftJoin('tblines','employees.line_id','=','tblines.id')
            ->leftJoin('floors','employees.floor_id','=','floors.id')
            ->leftJoin('attendance_setup','employees.empShiftId','=','attendance_setup.id')
            ->leftJoin('tbattendance_bonus','employees.empAttBonusId','=','tbattendance_bonus.id')
            ->select('employees.*','designations.designation','departments.departmentName',
                'marital_statuses.name as ms_name','nationalities.name as nationalitiesName',
                'units.name as unit_name', 'tblines.line_no','floors.floor','floors.id as ffid','attendance_setup.shiftName as shiftName','tbattendance_bonus.bTitle')
            ->where(['employees.id'=>$id])
            ->first();
            
        return view('employee.update_employee_bangla_information',compact('employee'));

    }
    
    public function store_bangla_information(Request $request)
    {
        $id=base64_decode($request->empId);
        $up=DB::table('employees')->where(['id'=>$id])->update([
            'empBnFullName'=>$request->empBnFullName,
            'employeeBnId'=>$request->employeeBnId,
            'empBnFatherName'=>$request->empBnFatherName,
            'empBnMotherName'=>$request->empBnMotherName,
            'empBnCurrentAddress'=>$request->empBnCurrentAddress,
            'empBnParAddress'=>$request->empBnParAddress,
            'empBnEduQuality'=>$request->empBnEduQuality,
            'empPreExperience'=>$request->empPreExperience,
            'empBnWorkingType'=>$request->empBnWorkingType,
            'empChildrenNumber'=>$request->empChildrenNumber,
            'empBoyNumber'=>$request->empBoyNumber,
            'empGirlNumber'=>$request->empGirlNumber,
        ]);

        Session::flash('message','Employee bangla information has been successfully updated.');
        return redirect("employee/".$id);
    }
       

    public function store(Request $request)
    {
        $helper=AppHelper::instance();


        if(empty($request->probation_period)){
            $request->probation_period=6;
        }

        $this->validate($request,[
            'empFirstName'=>'required',
            'employeeId'=>'required|unique:employees',
            'empDesignationId'=>'required',
            'empGenderId'=>'required',
            'empRole'=>'required',
            'empAccStatus'=>'required',
            'empJoiningDate'=>'required',
            'empMaritalStatusId'=>'required',
            'empDepartmentId'=>'required',
            'empPhoto'=>'mimes:png,jpg,jpeg',
            'empCardNumber'=>'nullable|unique:employees',
            'empEmail'=>'nullable|unique:employees'
        ]);

        if($request->empDOB==null){
            $empDOB=null;
        }
        else{
            $empDOB=Carbon::parse($request->empDOB)->format('Y-m-d H:i:s');
        }
        if($request->date_of_discontinuation==null){
            $date_of_discontinuation=null;
        }
        else{
            if(!$helper->checkIsAValidDate($request->date_of_discontinuation)){
                Session::flash('message','Incorrect date format.');
                return $this->create();

            }
            $date_of_discontinuation=Carbon::parse($request->date_of_discontinuation)->format('Y-m-d H:i:s');
        }
        if($request->file('empPhoto')) {
            if ($request->file('empPhoto')->getClientSize() > 2000000) {
                Session::flash('fileSize', "Employee create failed. File Size Limit Exceeded");
                return redirect('employee/create');
            }
        }


        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();
        if($file=$request->file('empPhoto')){
            $name=time()."_".$request->id."_".$file->getClientOriginalName();
            $file->move('Employee_Profile_Pic',$name);
        }
        else{
            $name=null;
        }
        if($request->empLastName==null){
            $request->empLastName='';
        }
        // return $request;
        Carbon::parse($request->empJoiningDate)->format('Y-m-d H:i:s');
        $id=DB::table('employees')->insertGetId([
            'empFirstName'=>$request->empFirstName,
            'empLastName'=>$request->empLastName,
            'employeeId'=>$request->employeeId,
            'empDesignationId'=>$request->empDesignationId,
            'empGenderId'=>$request->empGenderId,
            'empRole'=>$request->empRole,
            // 'empShiftId'=>$request->empShiftId,
            'empOTStatus'=>$request->empOTStatus,
            'empAttBonusId'=>$request->empAttBonusId,
            'empPassword'=>bcrypt($request->empPassword),
            'empAccStatus'=>$request->empAccStatus,
            'empEmail'=>$request->empEmail,
            'empPhone'=>$request->empPhone,
            'empParAddress'=>$request->empParAddress,
            'empCurrentAddress'=>$request->empCurrentAddress,
            'empFatherName'=>$request->empFatherName,
            'empMotherName'=>$request->empMotherName,
            'empDepartmentId'=>$request->empDepartmentId,
            'empJoiningDate'=>Carbon::parse($request->empJoiningDate)->format('Y-m-d H:i:s'),
            'empDOB'=>$empDOB,
            'empMaritalStatusId'=>$request->empMaritalStatusId,
            'empEcontactName'=>$request->empEcontactName,
            'emergencyPhone'=>$request->emergencyPhone,
            'emergencyAddress'=>$request->emergencyAddress,
            'empReligion'=>$request->empReligion,
            'empNid'=>$request->empNid,
            'empNationalityId'=>$request->empNationalityId,
            'empGlobalId'=>$request->empGlobalId,
            'empPhoto'=>$name,
            'empSection'=>$request->empSection,
            'empBloodGroup'=>$request->empBloodGroup,
            'empCardNumber'=>$request->empCardNumber,
            'unit_id'=>$request->unit_id,
            'line_id'=>$request->line_id,
            'floor_id'=>$request->floor_name,
            'work_group'=>$request->work_group,
            'salary_type'=>$request->salary_type,
            'bank_account'=>$request->bank_account,
            'bank_info'=>$request->bank_info,
            'payment_mode'=>$request->payment_mode,
            'date_of_discontinuation'=>$date_of_discontinuation,
            'reason_of_discontinuation'=>$request->reason_of_discontinuation,
            'emergency_contact_relation'=>$request->emergency_contact_relation,
            'skill_level'=>$request->skill_level,
            'reference_type'=>$request->reference_type,
            'reference_description'=>$request->reference_details,
            'create_by'=>$user->name,
            'modified_by'=>$user->name,
            'created_at'=>$now,
            'updated_at'=>$now,
            'probation_period'=>$request->probation_period,
        ]);


        if($id){
            Session::flash('createEmployee','Employee Created Successfully');

            if($request->empRole ==6){
//                return ("Ok");
            }
            else{
                DB::table('users')->insert([
                    'emp_id'=>$id,
                    'name'=>$request->empFirstName,
                    'email'=>$request->empEmail,
                    'password'=>bcrypt($request->empPassword),
                    'is_permission'=>$request->empRole,
                    'theme_style'=>'0',
                    'created_at'=>$now,
                    'updated_at'=>$now,

                ]);
            }
        }
        $grade=DB::table('designations')->where('id','=',$request->empDesignationId)->first();
        $gradeId=$grade->gradeId;
        \session(['emp_id'=>$id]);
        \session(['gradeId'=>$gradeId]);
        return redirect("/salary");
//        return $request->all();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $weekday=DB::table('week_leave')->get();
        $designations=DB::table('designations')->pluck('designation','id')->all();
        $shifts=DB::table('attendance_setup')->get();
        $departments=DB::table('departments')->pluck('departmentName','id')->all();
        $maritalStatuses=DB::table('marital_statuses')->pluck('name','id')->all();
        $nationalities=DB::table('nationalities')->pluck('name','id')->all();
        $units=DB::table('units')->pluck('name','id')->all();
        $floors=DB::table('floors')->pluck('floor','id')->all();
        $attendance_bonus=DB::table('tbattendance_bonus')->get();
        $previous_earn_leave=DB::table('manual_earn_leave')->where('emp_id','=',$id)->sum('earn_leave_days');


        $check_employee=DB::table('employees')
                  ->where(['employees.id'=>$id])
                  ->count();
        if($check_employee!=0){
        $employee=DB::table('employees')
            ->leftJoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftJoin('marital_statuses','employees.empMaritalStatusId','=','marital_statuses.id')
            ->leftJoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftJoin('nationalities','employees.empNationalityId','=','nationalities.id')
            ->leftJoin('units','employees.unit_id','=','units.id')
            ->leftJoin('tblines','employees.line_id','=','tblines.id')
            ->leftJoin('floors','employees.floor_id','=','floors.id')
            ->leftJoin('attendance_setup','employees.empShiftId','=','attendance_setup.id')
            ->leftJoin('tbattendance_bonus','employees.empAttBonusId','=','tbattendance_bonus.id')
            ->select('employees.*','designations.designation','departments.departmentName',
                'marital_statuses.name as ms_name','nationalities.name as nationalitiesName',
                'units.name as unit_name', 'tblines.line_no','floors.floor','floors.id as ffid','attendance_setup.shiftName as shiftName','tbattendance_bonus.bTitle')
            ->where(['employees.id'=>$id])
            ->first();

        $start_date=Carbon::parse($employee->empJoiningDate);
        $now = Carbon::now();
        $diff = $start_date->diffInDays($now);
        $paid_leave=DB::table('earn_leave_payment')->where('emp_id','=',$id)->sum('leave_to_cash');

            if($diff<364){
            $days=0;

        }
        else{
            $days=DB::select("select count(*) as working_days from `attendance` where `emp_id` = $id");
            $days= $days[0]->working_days;
        }

        if(empty($employee)){
            return abort(404);
        }


        $lines=DB::table('tblines')->where('floor_id','=',$employee->ffid )->pluck('line_no','id')->all();

        $edu_cers=DB::table('employee_education_infos')->where(['emp_id'=>$id])->get();
        $workExp=DB::table('employee_working_histories')->where(['emp_id'=>$id])->latest()->get();
        $skillTest=DB::table('tb_employee_skill_test')->where(['emp_id'=>$id])->latest()->get();
        $attachments=DB::table('employee_attachments')->where(['emp_id'=>$id])->latest()->get();
        $nominees=DB::table('nominees')->where('emp_id','=',$id)->orderBy('priority')->get();
        $payGrade=DB::table('payroll_grade')->latest()->get();
        $training_history=DB::table('employee_training_history')->where(['emp_id'=>$id])->get();
        $company_training_history=DB::table('tb_employee_training')->
            join('tb_training','training_id','=','tb_training.id')
            ->where(['employee_id'=>$id])
            ->select('training_name','description','duration','training_starting_date','training_ending_date','tb_training.attachment')
            ->get();
//        return $company_training_history;


        $leave_type=DB::table('tb_leave_type')->get();
        $leave_data=[];

        foreach ($leave_type as $lt) {
            if($lt->id==4){
                $data = DB::table('tb_leave_application')
                    ->join('employees', 'employees.id', '=', 'tb_leave_application.employee_id')
                    ->join('designations', 'designations.id', '=', 'employees.empDesignationId')
                    ->leftjoin('tb_employee_leave',
                        ['tb_employee_leave.employee_id' => 'tb_leave_application.employee_id',
                            'tb_employee_leave.leave_type_id' => 'tb_leave_application.leave_type_id'])
                    ->join('tb_leave_type', 'tb_leave_type.id', '=', 'tb_leave_application.leave_type_id')
                    ->select('employees.empFirstName', 'tb_employee_leave.leave_available', 'tb_employee_leave.leave_taken',
                        'employees.empLastName', 'employees.employeeId as eID', 'tb_leave_type.leave_type',
                        'designations.designation', 'tb_leave_type.total_days', 'tb_leave_application.leave_starting_date')
                    ->where('tb_leave_application.status', '=', 1)
                    ->where(['tb_leave_application.employee_id' => $id])
                    ->where(['tb_leave_application.leave_type_id' => $lt->id])
                    ->groupBy('tb_leave_type.total_days')->get();

            }
            else {
                $data = DB::table('tb_leave_application')
                    ->join('employees', 'employees.id', '=', 'tb_leave_application.employee_id')
                    ->join('designations', 'designations.id', '=', 'employees.empDesignationId')
                    ->leftjoin('tb_employee_leave',
                        ['tb_employee_leave.employee_id' => 'tb_leave_application.employee_id',
                            'tb_employee_leave.leave_type_id' => 'tb_leave_application.leave_type_id'])
                    ->join('tb_leave_type', 'tb_leave_type.id', '=', 'tb_leave_application.leave_type_id')
                    ->select('employees.empFirstName', 'tb_employee_leave.leave_available', 'tb_employee_leave.leave_taken',
                        'employees.empLastName', 'employees.employeeId as eID', 'tb_leave_type.leave_type',
                        'designations.designation', 'tb_leave_type.total_days', 'tb_leave_application.leave_starting_date')
                    ->where('tb_leave_application.status', '=', 1)
                    ->where(['tb_leave_application.employee_id' => $id])
                    ->where(['tb_leave_application.leave_type_id' => $lt->id])
                    ->where(['tb_employee_leave.year' => Carbon::now()->format('Y')])
                    ->groupBy('tb_leave_type.total_days')->get();
            }

            $count = $data->count();
            //dd($count);
            if ($count != 0) {
                $data = json_decode(json_encode($data), true);
                $leave_data=array_merge($leave_data,$data);
            }
            else {
                $leaveData = DB::table('tb_leave_type')
                    ->where(['tb_leave_type.id' => $lt->id])
                    //->groupBy('tb_leave_type.total_days')
                    ->get();
                $array = json_decode(json_encode($leaveData), true);
                $leave_data=array_merge($leave_data,$array);
            }
        }

        $leave_data = json_decode(json_encode($leave_data));

//        return $leave_data;

        $start=Carbon::now()->startOfMonth()->toDateString();
//        return $start;

        $month= Carbon::now()->format('F');

        $present=DB::table('attendance')->where('emp_id','=',$id)->whereBetween('date',[$start,Carbon::now()->toDateString()])->count();
//        $leave=DB::table('tb_leave_applications')->groupBy('employee_id')->where('employee_id','=',$id)->where('status','=',1)->whereBetween('leave_starting_date',[$start,Carbon::now()->toDateString()])->orWhereBetween('leave_ending_date',[$start,Carbon::now()->toDateString()])->count();
        $now=Carbon::now()->toDateString();
        $leave=DB::select("select SUM(actual_days) as aggregate from `tb_leave_application` where `employee_id` = $id and `status` = 1 and (`leave_starting_date` between '$start' and '$now'  or `leave_ending_date` between '$start' and '$now') group by `employee_id`");
        if(isset($leave[0]->aggregate)){
            $l=$leave[0]->aggregate;

        }
        else{
            $l=0;
        }


        $total=leaveController::dayCalculator($start,Carbon::now()->toDateString());




        $empPresent = (object) [
            'month' => $month,
            'present' => $present,
            'leave' => $l,
            'total'=>$total,
        ];

//        return $empPresent->leave;

        $emp_id=$id;
        \session(['emp_id'=>$id]);

        $checkSal=DB::table('payroll_salary')->where(['emp_id'=>$emp_id])->get();
        if(count($checkSal)==0){
            $checkSal=0;

        }
        else{
            $checkSal=1;
            $sal = DB::table('payroll_salary')
            ->leftjoin('employees', 'payroll_salary.emp_id', '=', 'employees.id')
            ->leftJoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftJoin('payroll_grade', 'designations.gradeId','=','payroll_grade.id')
            ->select('payroll_salary.*', 'payroll_grade.grade_name','designations.gradeId as designationGradeId')
            ->where(['emp_id'=>$emp_id])
            ->first();
//            return $sal;
        }

        // dd($sal);
        return view('employee.details', compact('edu_cers', 'paid_leave','previous_earn_leave','days','paid_leave','emp_id','workExp','skillTest','employee','attachments','designations','departments','maritalStatuses','nationalities','units','lines','floors','nominees','payGrade','checkSal','sal','training_history','company_training_history','leave_data','empPresent','shifts','attendance_bonus','weekday'));
        }else{
            Session::flash('message', 'Invalid Employee Information.');
            return view('error_page');
        }
    }
    public function employee_profile($id)
    {
        $id=base64_decode($id);
        $designations=DB::table('designations')->pluck('designation','id')->all();
        $shifts=DB::table('attendance_setup')->get();
        $departments=DB::table('departments')->pluck('departmentName','id')->all();
        $maritalStatuses=DB::table('marital_statuses')->pluck('name','id')->all();
        $nationalities=DB::table('nationalities')->pluck('name','id')->all();
        $units=DB::table('units')->pluck('name','id')->all();
        $floors=DB::table('floors')->pluck('floor','id')->all();
        $attendance_bonus=DB::table('tbattendance_bonus')->get();
        $paid_leave=DB::table('earn_leave_payment')->where('emp_id','=',$id)->sum('leave_to_cash');
        $previous_earn_leave=DB::table('manual_earn_leave')->where('emp_id','=',$id)->sum('earn_leave_days');
        $check_employee=DB::table('employees')
                  ->where(['employees.id'=>$id])
                  ->count();
        if($check_employee!=0){
        $employee=DB::table('employees')
            ->leftJoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftJoin('marital_statuses','employees.empMaritalStatusId','=','marital_statuses.id')
            ->leftJoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftJoin('nationalities','employees.empNationalityId','=','nationalities.id')
            ->leftJoin('units','employees.unit_id','=','units.id')
            ->leftJoin('tblines','employees.line_id','=','tblines.id')
            ->leftJoin('floors','employees.floor_id','=','floors.id')
            ->leftJoin('attendance_setup','employees.empShiftId','=','attendance_setup.id')
            ->leftJoin('tbattendance_bonus','employees.empAttBonusId','=','tbattendance_bonus.id')
            ->select('employees.*','designations.designation','departments.departmentName',
                'marital_statuses.name as ms_name','nationalities.name as nationalitiesName',
                'units.name as unit_name', 'tblines.line_no','floors.floor','floors.id as ffid','attendance_setup.shiftName as shiftName','tbattendance_bonus.bTitle')
            ->where(['employees.id'=>$id])
            ->first();
        $start_date=Carbon::parse($employee->empJoiningDate);
        $now = Carbon::now();
        $diff = $start_date->diffInDays($now);
        $paid_leave=DB::table('earn_leave_payment')->where('emp_id','=',$id)->sum('leave_to_cash');

            if($diff<364){
            $days=0;

        }
        else{
            $days=DB::select("select count(*) as working_days from `attendance` where `emp_id` = $id");
            $days= $days[0]->working_days;
        }

        if(empty($employee)){
            return abort(404);
        }
//        else{
//            return "AA";
//        }
//        return $employee;
        if(isset($employee->floor))
        {
            $lines=DB::table('tblines')->where('floor_id','=',$employee->ffid )->pluck('line_no','id')->all();
        }
        else{
            $lines=['Select a floor first'=>''];
        }
//        return $id;

        $edu_cers=DB::table('employee_education_infos')->where(['emp_id'=>$id])->get();
        $workExp=DB::table('employee_working_histories')->where(['emp_id'=>$id])->latest()->get();
        $skillTest=DB::table('tb_employee_skill_test')->where(['emp_id'=>$id])->latest()->get();
        $attachments=DB::table('employee_attachments')->where(['emp_id'=>$id])->latest()->get();
        $nominees=DB::table('nominees')->where('emp_id','=',$id)->orderBy('priority')->get();
        $payGrade=DB::table('payroll_grade')->latest()->get();
        $training_history=DB::table('employee_training_history')->where(['emp_id'=>$id])->get();
        $company_training_history=DB::table('tb_employee_training')->
            join('tb_training','training_id','=','tb_training.id')
            ->where(['employee_id'=>$id])
            ->select('training_name','description','duration','training_starting_date','training_ending_date','tb_training.attachment')
            ->get();
//        return $company_training_history;


        $leave_type=DB::table('tb_leave_type')->get();
        $leave_data=[];

        foreach ($leave_type as $lt) {
            $data = DB::table('tb_leave_application')
                ->join('employees', 'employees.id', '=', 'tb_leave_application.employee_id')
                ->join('designations', 'designations.id', '=', 'employees.empDesignationId')
                ->leftjoin('tb_employee_leave',
                    ['tb_employee_leave.employee_id' => 'tb_leave_application.employee_id',
                        'tb_employee_leave.leave_type_id' => 'tb_leave_application.leave_type_id'])
                ->join('tb_leave_type', 'tb_leave_type.id', '=', 'tb_leave_application.leave_type_id')
                ->select('employees.empFirstName', 'tb_employee_leave.leave_available', 'tb_employee_leave.leave_taken',
                    'employees.empLastName', 'employees.employeeId as eID', 'tb_leave_type.leave_type',
                    'designations.designation', 'tb_leave_type.total_days','tb_leave_application.leave_starting_date')
                ->where('tb_leave_application.status', '=', 1)
                ->where(['tb_leave_application.employee_id' => $id])
                ->where(['tb_leave_application.leave_type_id' => $lt->id])
                //->where(['tb_employee_leave.year'=>$year])
                ->groupBy('tb_leave_type.total_days')->get();

            $count = $data->count();
            //dd($count);
            if ($count != 0) {
                $data = json_decode(json_encode($data), true);
                $leave_data=array_merge($leave_data,$data);
            }
            else {
                $leaveData = DB::table('tb_leave_type')
                    ->where(['tb_leave_type.id' => $lt->id])
                    //->groupBy('tb_leave_type.total_days')
                    ->get();
                $array = json_decode(json_encode($leaveData), true);
                $leave_data=array_merge($leave_data,$array);
            }
        }

        $leave_data = json_decode(json_encode($leave_data));

//        return $leave_data;

        $start=Carbon::now()->startOfMonth()->toDateString();
//        return $start;

        $month= Carbon::now()->format('F');

        $present=DB::table('attendance')->where('emp_id','=',$id)->whereBetween('date',[$start,Carbon::now()->toDateString()])->count();
//        $leave=DB::table('tb_leave_applications')->groupBy('employee_id')->where('employee_id','=',$id)->where('status','=',1)->whereBetween('leave_starting_date',[$start,Carbon::now()->toDateString()])->orWhereBetween('leave_ending_date',[$start,Carbon::now()->toDateString()])->count();
        $now=Carbon::now()->toDateString();
        $leave=DB::select("select SUM(actual_days) as aggregate from `tb_leave_application` where `employee_id` = $id and `status` = 1 and (`leave_starting_date` between '$start' and '$now'  or `leave_ending_date` between '$start' and '$now') group by `employee_id`");
        if(isset($leave[0]->aggregate)){
            $l=$leave[0]->aggregate;

        }
        else{
            $l=0;
        }


        $total=leaveController::dayCalculator($start,Carbon::now()->toDateString());




        $empPresent = (object) [
            'month' => $month,
            'present' => $present,
            'leave' => $l,
            'total'=>$total,
        ];

//        return $empPresent->leave;

        $emp_id=$id;
        \session(['emp_id'=>$id]);

        $checkSal=DB::table('payroll_salary')->where(['emp_id'=>$emp_id])->get();
        if(count($checkSal)==0){
            $checkSal=0;

        }
        else{
            $checkSal=1;
           
            $sal = DB::table('payroll_salary')
            ->leftjoin('employees', 'payroll_salary.emp_id', '=', 'employees.id')
            ->leftJoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftJoin('payroll_grade', 'designations.gradeId','=','payroll_grade.id')
            ->select('payroll_salary.*', 'payroll_grade.grade_name')
            ->where(['emp_id'=>$emp_id])
            ->first();


        }

        // dd($sal);
        return view('employee.employee_profile', compact('edu_cers', 'paid_leave','previous_earn_leave','days','paid_leave','emp_id','workExp','skillTest','employee','attachments','designations','departments','maritalStatuses','nationalities','units','lines','floors','nominees','payGrade','checkSal','sal','training_history','company_training_history','leave_data','empPresent','shifts','attendance_bonus'));
        }
        else{
            Session::flash('message', 'Invalid Employee Information.');
            return view('error_page');
        }

       
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function floor_line($id)
    {
        $lines=DB::table('tblines')->where('floor_id','=',$id)->pluck('line_no','id')->all();
        return view('employee.floor_lines',compact('lines'));
    }
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
//        return $id;
        $helper=AppHelper::instance();
//        if(!$helper->checkIsAValidDate($request->empJoiningDate)){
//            Session::flash('delete','Incorrect date format.');
//            return $this->show(\session('emp_id'));
//        }
        if(empty($request->probation_period)){
            $request->probation_period=6;
        }

        $this->validate($request,[
            'empFirstName'=>'required',
            'employeeId'=>'required',
            'empDesignationId'=>'required',
            'empGenderId'=>'required',
            'empRole'=>'required',
            'empAccStatus'=>'required',
            'empJoiningDate'=>'required',
            'empMaritalStatusId'=>'required',
            'empDepartmentId'=>'required',

        ]);

        if($request->date_of_discontinuation==null){
            $date_of_discontinuation=null;
            $discon_type=null;
        }
        else{
            if(!$helper->checkIsAValidDate($request->date_of_discontinuation)){
                Session::flash('delete','Incorrect date format. T');
                return $this->show(\session('emp_id'));
            }
            $date_of_discontinuation=Carbon::parse($request->date_of_discontinuation)->format('Y-m-d H:i:s');
            if($request->discon_type!=""){
                $discon_type=$request->discon_type;
            }else{
                Session::flash('failedMessage', "Please select discontinuation type for lefty/resign employee. ");
                return redirect()->back();
            }
        }

        if($request->empDOB==null){
            $empDOB=null;
        }

        else{
            $empDOB=Carbon::parse($request->empDOB)->format('Y-m-d H:i:s');
        }
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        if($request->empLastName==null){
            $request->empLastName='';
        }

        $up=DB::table('employees')->where(['id'=>$id])->update([
            'empFirstName'=>$request->empFirstName,
            'empLastName'=>$request->empLastName,
            'employeeId'=>$request->employeeId,
            'empDesignationId'=>$request->empDesignationId,
            'empGenderId'=>$request->empGenderId,
            'empRole'=>$request->empRole,
            'empAccStatus'=>$request->empAccStatus,
            // 'empShiftId'=>$request->empShiftId,
            'empAttBonusId'=>$request->empAttBonusId,
            'empOTStatus'=>$request->empOTStatus,
            'empEmail'=>$request->empEmail,
            'empPhone'=>$request->empPhone,
            'empParAddress'=>$request->empParAddress,
            'empCurrentAddress'=>$request->empCurrentAddress,
            'empFatherName'=>$request->empFatherName,
            'empMotherName'=>$request->empMotherName,
            'empDepartmentId'=>$request->empDepartmentId,
            'empJoiningDate'=>Carbon::parse($request->empJoiningDate)->format('Y-m-d H:i:s'),
            'empDOB'=>$empDOB,
            'empMaritalStatusId'=>$request->empMaritalStatusId,
            'empEcontactName'=>$request->empEcontactName,
            'emergencyPhone'=>$request->emergencyPhone,
            'emergencyAddress'=>$request->emergencyAddress,
            'empReligion'=>$request->empReligion,
            'empNid'=>$request->empNid,
            'empNationalityId'=>$request->empNationalityId,
            'empGlobalId'=>$request->empGlobalId,
            'empSection'=>$request->empSection,
            'empBloodGroup'=>$request->empBloodGroup,
            'unit_id'=>$request->unit_id,
            'line_id'=>$request->line_id,
            'floor_id'=>$request->floor_name,
            'work_group'=>$request->work_group,
            'salary_type'=>$request->salary_type,
            'bank_account'=>$request->bank_account,
            'bank_info'=>$request->	bank_info,
            'payment_mode'=>$request->payment_mode,
            'date_of_discontinuation'=>$date_of_discontinuation,
            'reason_of_discontinuation'=>$request->reason_of_discontinuation,
            'emergency_contact_relation'=>$request->emergency_contact_relation,
            'skill_level'=>$request->skill_level,
            'reference_type'=>$request->reference_type,
            'reference_description'=>$request->reference_details,
            'modified_by'=>$user->name,
            'updated_at'=>$now,
            'probation_period'=>$request->probation_period,
            'discon_type'=>$discon_type,
            'is_three_shift'=>$request->is_three_shift,
        ]);

        if($up){
            if($request->empRole ==6){
                DB::table('users')->where(['emp_id'=>$id])->delete();
            }
            else{
                $exist=DB::table('users')->where(['emp_id'=>$id])->update([
                    'name'=>$request->empFirstName,
                    'email'=>$request->empEmail,
                    'is_permission'=>$request->empRole,
                    'updated_at'=>$now,

                ]);
                if($exist){


                }
                else{
                    DB::table('users')->insert([
                        'emp_id'=>$id,
                        'name'=>$request->empFirstName,
                        'email'=>$request->empEmail,
                        'is_permission'=>$request->empRole,
                        'password'=>bcrypt($request->empPassword),

                    ]);
                }
            }
            Session::flash('employeeUpdate', "Employee Information Successfully Updated");
        }



        return redirect("employee/".$id);
    }

    public function UpdatePassword(Request $request,$id){
        $this->validate($request,[
            'empPassword'=>'required',
        ]);
        $upPass=DB::table('employees')->where(['id'=>$id])->update([
            'empPassword'=>bcrypt($request->empPassword),
        ]);
        if($upPass){
            if($request->empRole ==6){
                return redirect("employee/".$id);
            }
            else{
                $exist=DB::table('users')->where(['emp_id'=>$id])->update([
                    'password'=>bcrypt($request->empPassword),

                ]);
            }
            Session::flash('upPassword','Password Updated');
        }


        return redirect("employee/".$id);


    }

    public function UpdatePhoto(Request $request, $id){

//        $this->validate($request,[
//            'empPhoto'=>'mimes:png,jpg,jpeg',
//        ]);

        if($file=$request->file('empPhoto')){
            if ($request->file('empPhoto')->getClientSize() > 2000000) {
                Session::flash('fileSize', "Employee create failed. File Size Limit Exceeded");
                return redirect('employee/create');
            }
            $name=time()."_".$id."_".$file->getClientOriginalName();
            $file->move('Employee_Profile_Pic',$name);
            $upPhoto=DB::table('employees')->where(['id'=>$id])->update([
                'empPhoto'=>$name,
            ]);
            if($upPhoto)
            {
                Session::flash('upPassword',"Profile Picture Updated Successfully");
            }
        }
        return redirect('employee/'.$id);
    }

    public function UpdateProxy(Request $request,$id){
        $this->validate($request,[
            'empCardNumber'=>'required',
        ]);
        $check=DB::table('employees')
            ->where([['empAccStatus','=',1],['empCardNumber','=',$request->empCardNumber],['id','!=',$id]])->count();
//        return $check;
        if($check==0){
            DB::table('employees')->where('id','=',$id)->update([
                'empCardNumber'=>$request->empCardNumber
            ]);
            $employee=DB::table('employees')->where('id','=',$id)->first();
            DB::table('notifications')->insert([
                'type'=>'Proxy Update',
                'message'=>"$employee->empFirstName $employee->empLastName's Proxy Card Changed.",
                'notification_by'=>Auth::user()->id,
                'notification_to'=>$employee->id,
                'created_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
                'updated_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
            ]);
            DB::table('tbactivity_logs_history')->insert([
                'acType'=>'Manual Attendance',
                'details'=>"$employee->empFirstName $employee->empLastName's Proxy Card Changed.",
                'createdBy'=>Auth::user()->id,
                'created_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
                'updated_at'=>Carbon::now('Asia/Dhaka')->toDateTimeString(),
            ]);
            Session::flash('mhmessage','Employee Proxy Card Number Is Updated.');
            return redirect(route('employee.show',$id));

        }
        else{
            Session::flash('appointment_latter_message','The Proxy Number Is Already In Use.');
            return redirect(route('employee.show',$id));
        }
    }

    public function NotificationUpdate($id, Request $request){
        DB::table('notifications')->where('id','=',$request->not_id)->update([
            'read_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
//        return $request->not_id;

    }


    public function destroy($id)
    {
        //
    }

    public function id_card($id){
        $employee=DB::table('employees')
            ->join('departments','employees.empDepartmentId','=','departments.id')
            ->join('designations', 'employees.empDesignationId','=','designations.id')
            ->select('employees.*','designations.designation','designations.designationBangla','departments.departmentName')
            ->where('employees.id','=',$id)
            ->first();

//        $pdf=PDF::loadView('employee.id_card',compact('employee'));
//        $name=time()."_".$id."_"."Id_Card.pdf";
//        return $pdf->download($name);
        return view('employee.id_card_finbangla',compact('employee'));

    }

    public function recruitment_policy(){
        $pdf=PDF::loadView('report.pdf.company.recruitment_policy');
        $name=time()."_Recruitment_Policy.pdf";
        return $pdf->download($name);
//        return view('report.pdf.company.recruitment_policy');
    }



    public function getContinueAbsentList(){
        return view('attendance.continued_absent_list');
    }

    public function inactive_continue_absent(Request $request){
    //        return $request->emp_id;
        if(empty($request->emp_id)){
            Session::flash('smsnotification','Employee List Is Empty. No Employee Found.');
            return redirect()->back();
        }
        $e=DB::table('employees')->whereIn('id',$request->emp_id)->update([
            'empAccStatus'=>2,
            'date_of_discontinuation'=>Carbon::now()->toDateString(),

        ]);
        //        return $e;
        Session::flash('smsnotification','Employee have been deactivated');
        return redirect()->back();
    }

    public function continued_absent_list_data(){
        $date=Carbon::now()->subDays(10)->toDateString();
        $now=Carbon::now()->toDateString();
        $dateBetween=0;
        $diff=1;
        $addDiff=0;
        $forceBreak=0;
        while($dateBetween!=10 && $forceBreak<50){
            $forceBreak++;
            $dateBetween=leaveController::dayCalculator1($date,$now);
            $diff=10-$dateBetween;
            $addDiff+=$diff;
            if($diff==0){
                $dateBetween=10;
            }
            else{
                $date=Carbon::now()->subDays(10+$addDiff)->toDateString();
           
            }
        }
       
        $sql="SELECT id FROM employees WHERE empAccStatus=1 AND id NOT IN ( SELECT * FROM (SELECT emp_id FROM attendance GROUP BY  date, emp_id HAVING attendance.date>'$date') AS subquery) AND id NOT IN
              (SELECT employee_id FROM `tb_leave_application` WHERE status=1 AND (leave_ending_date>='$date' AND leave_ending_date<='$now' OR leave_starting_date >= '$date' AND leave_starting_date<='$now'))";


        $pastSql="SELECT employees.id FROM employees LEFT JOIN attendance ON employees.id=attendance.emp_id AND attendance.date>'$date' LEFT JOIN tb_leave_application ON employees.id=tb_leave_application.employee_id AND tb_leave_application.status=1 AND (tb_leave_application.leave_starting_date>='$date' AND tb_leave_application.leave_ending_date<='$now' OR tb_leave_application.leave_starting_date >= '$date' AND tb_leave_application.leave_starting_date<='$now') WHERE attendance.emp_id IS null AND tb_leave_application.employee_id IS null";
      
     
        $ttDays=[];
        $dd=DB::select($sql);
    

        //return $dd;
        foreach ($dd as $d){
            $sql="SELECT attendance.*, employees.id, employees.employeeId, employees.empFirstName, employees.empLastName, designations.designation, departments.departmentName FROM `attendance` LEFT JOIN employees ON employees.id=attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id WHERE attendance.emp_id=$d->id ORDER BY `attendance`.`date` DESC LIMIT 1";
            $totalDays=DB::select($sql);
            if(!empty($totalDays[0])){
                $sDate=$totalDays[0]->date;
                $leaveSql="SELECT * FROM `tb_leave_application` WHERE leave_ending_date>='$sDate' AND leave_ending_date<='$now' OR leave_starting_date >= '$sDate' AND leave_starting_date<='$now' HAVING employee_id=$d->id AND  status='1'";
                $leaveCounts=DB::select($leaveSql);
                if(!count($leaveCounts)){
                    $totalDays[0]->day_between = leaveController::dayCalculator1($totalDays[0]->date,$now);
                }
                else{
                   $totalDays[0]->day_between  = leaveController::dayCalculator1($totalDays[0]->date,$now)-$leaveCounts[0]->actual_days;
                }
            }
            $ttDays=array_merge($totalDays,$ttDays);

        }
        return view('attendance.continued_absent_list_data', compact('ttDays'));
    }


    public function continueAbsentEmployeeNotification(Request $request){
        $user =DB::table('employees')
            ->join('designations', 'employees.empDesignationId','=','designations.id')
            ->join('departments','employees.empDepartmentId','=','departments.id')
            ->select('employees.*','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName')
            ->where('employees.id', $request->id)->get()->first();

        if(($request->notificationType)=="downloadletter"){
            $companyInformation=DB::table('tbcompany_information')->first();
                $lastNotify = DB::table('tbcaenhistory')->where('emp_id', $request->id)->latest()->first();
                $secondLastNotify = DB::table('tbcaenhistory')
                                    ->where('emp_id', $request->id)
                                    ->orderBy('id', 'desc')
                                    ->skip(1)
                                    ->take(1)
                                    ->first();
                $sender=Auth::user();
                // dd($user);
                $now=Carbon::now()->toDateTimeString();
                 $id = DB::table('tbcaenhistory')->insertGetId(
                    [
                        'emp_id' => $user->id,
                        'senderId' => $sender->emp_id,
                        'ntype'=>'3', 
                        'ndate'=>$now,
                        'absentDays'=>$request->days,
                        'lastPresent'=>$request->lastPresent,
                        'created_at'=>$now,
                        'updated_at'=>$now
                    ]
                );
            // return view('report.pdf.continue_absent_letter',compact('user','request','companyInformation','lastNotify','secondLastNotify'));
            $pdf=mPDF::loadView('report.pdf.continue_absent_letter',compact('user','request','companyInformation','lastNotify','secondLastNotify'));
//            $pdf->setPaper('A4', 'portrait');
            $name=time()."_letter.pdf";
            return $pdf->download($name);
        }

        if(($request->notificationType)=="sentsms"){

                if (empty($user)) return response()->json(['success' => 'false', 'error'=>'User could not be found'], 200);
            
                $phone = $user->empPhone;
                $sender=Auth::user();
                $now=Carbon::now()->toDateTimeString();
                $msg="Dear ".$user->empFirstName.", You were absent since ".$request->days."days without informing company. Last present date was ".Carbon::parse($request->lastPresent)->format('d-M-Y').". Please Join to work as soon as possible and inform the actual cause of lefty to management.Thank you";
                // $msg ="আপনি গত ".$request->lastPresent." ইং তারিখ হতে অদ্যাবধি বিনা নোটিশে বা কর্তৃপক্ষের বিনা অনুমতিতে কারখানায় অনুপস্থিত রয়েছেন । আপনার এ ধরনের অনুপস্থিতির কারনে কারখানার উৎপাদন প্রক্রিয়া ব্যহত হচ্ছে বিধায় বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে অনতিবিলম্বে  আপনাকে অনুপস্থিতির কারন ব্যাখ্যা সহ কাজে যোগদান অথবা আপনার এ ধরনের বিনা নোটিশে বা উর্দ্ধতন কর্তৃপক্ষের বিনা অনুমতিতে ১০ দিনের অধিক সময় কারখানায় অনুপস্থিত থাকার বিষয়ে ব্যাখ্যা প্রদানের নির্দেশ প্রদান করা হল । ";
                
                if(!empty($user->empPhone)){
                try {
                     // messageController::sendSMS($phone, $msg);

                    //  $id = DB::table('tbcaenhistory')->insertGetId(
                    // [
                    //     'emp_id' => $user->id,
                    //     'senderId' => $sender->emp_id,
                    //     'ntype'=>'1', 
                    //     'ndate'=>$now,
                    //     'absentDays'=>$request->days,
                    //     'lastPresent'=>$request->lastPresent,
                    //     'created_at'=>$now,
                    //     'updated_at'=>$now
                    // ]
                    // );

                    $cmsg= "SMS has been successfully sent to ".$user->empFirstName." ".$user->empLastName ." (".$phone.") ";
                    Session::flash('smsnotification', $cmsg);

                    return back();
                    
                }
                catch (Exception $e) {
                    return response()->json(['success' => 'false', 'error'=>'something went wrong, could not send SMS'], 200);
                }
                }else{
                    $cmsg= "No phone number for ".$user->empFirstName." ".$user->empLastName;
                    Session::flash('smsnotificationfailed', $cmsg);
                    return back(); 
                }
        }

        if(($request->notificationType)=="sentmail"){

            if(!empty($user->empEmail)){

                $email = $user->empEmail;
                $companyInformation=DB::table('tbcompany_information')->first();
                $lastNotify = DB::table('tbcaenhistory')->where('emp_id', $request->id)->latest()->first();
                $secondLastNotify = DB::table('tbcaenhistory')
                                    ->where('emp_id', $request->id)
                                    ->orderBy('id', 'desc')
                                    ->skip(1)
                                    ->take(1)
                                    ->first();
                $sender=Auth::user();
                $now=Carbon::now()->toDateTimeString();

                // dd($lastNotify);
            // return view('mails.continued_absent_list',compact('user','request','lastNotify','secondLastNotify'));

                Mail::to($email)->send(new mailSender($user,$request,$companyInformation,$lastNotify,$secondLastNotify));

                // $id = DB::table('tbcaenhistory')->insertGetId(
                //     [
                //         'emp_id' => $user->id,
                //         'senderId' => $sender->emp_id,
                //         'ntype'=>'2', 
                //         'ndate'=>$now,
                //         'absentDays'=>$request->days,
                //         'lastPresent'=>$request->lastPresent,
                //         'created_at'=>$now,
                //         'updated_at'=>$now
                //     ]
                // );

                $cmsg= "Email has been successfully sent to ".$user->empFirstName." ".$user->empLastName ."(".$email.")";
                Session::flash('emailnotification', $cmsg);
                return back();

            }else{
               $cmsg= "No email address for ".$user->empFirstName;
                Session::flash('emailnotificationfailed', $cmsg);
                return back(); 
            }

        }
    }

    public function sectionAjaxData(Request $request){

        $query = $request->get('query','');        

        $sections = DB::table('employees')->distinct('empSection')->where('empSection','LIKE','%'.$query.'%')->pluck('empSection');        

        return response()->json($sections);

    }

    public function data_employee(Request $request){
        $query = $request->get('query','');

        $posts=DB::table('employees')->where('empFirstName','LIKE','%'.$query.'%')
            ->orWhere('empLastName','LIKE','%'.$query.'%')
            ->orWhere('employeeId','LIKE','%'.$query.'%')
            ->get(['empFirstName','empLastName','employeeId']);
        $data=[];
        foreach ($posts as $p){
            $data[]=$p->empFirstName." ".$p->empLastName."  #".$p->employeeId;

        }
        return $data;

    }

    public function resignation_letter($id){
        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','designations.designation','departments.departmentName')
            ->where('employees.id','=',$id)
            ->first();
//        return $employee;
        $pdf=mPDF::loadView('employee.resignation_letter',compact('employee'));
        $name=time()."_".$id."_"."resignation_letter.pdf";
        return $pdf->download($name);
//        return $employee;
    }

    public function final_settlement($id){
            $employee=DB::table('employees')
                ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
                ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
                ->leftjoin('units', 'employees.unit_id','=','units.id')
                ->select('employees.*','units.name as unitName','designations.designation','departments.departmentName')
                ->where('employees.id','=',$id)
                ->first();
//            return $employee->date_of_discontinuation;
        $month=Carbon::parse($employee->date_of_discontinuation)->subMonth()->format('Y-m-01');
        $dayCount=date('t', mktime(0, 0, 0, Carbon::now()->subMonth()->format('m'), 1, Carbon::now()->subMonth()->format('Y')));

        $present_salary=DB::table('payroll_salary')->where('emp_id','=',$id)->select('total_employee_salary','basic_salary')->first();

        if(!isset($present_salary->total_employee_salary)){
        Session::flash('message',"Couldn't Download. No Salary Record Found.");
        return redirect(route('employee.show',$id));

        }

        try {
            $previous_salary_all = DB::table('tb_salary_history')->where('emp_id', '=', $id)->where('month', '=', $month)->select('net_amount','basic_salary')->first();
            $previous_salary=$previous_salary_all->net_amount;
            $previous_salary_basic=$previous_salary_all->basic_salary;
        }
        catch (\Exception $ex){
            $previous_salary=0;
            $previous_salary_basic=0;

        }

        $days=\Illuminate\Support\Facades\DB::select("select count(*) as working_days from `attendance` where `emp_id` = $id");
        $previous_earn_leave=\Illuminate\Support\Facades\DB::table('manual_earn_leave')->where('emp_id','=',$id)->sum('earn_leave_days');
        $encashment=\Illuminate\Support\Facades\DB::table('earn_leave_payment')->where('emp_id','=',$id)->sum('leave_to_cash');


        $total_working_days=$days[0]->working_days;
        $total_earn_leave=round($total_working_days/18)+$previous_earn_leave;
        if($total_earn_leave>=40)
        {
            $total_earn_leave=40;
        }
        $datetime1 = new DateTime($employee->empJoiningDate);
        $datetime2 = new DateTime($employee->date_of_discontinuation);
        $interval = $datetime1->diff($datetime2);
        $leave_enjoyed=DB::table('tb_employee_leave')->where(['employee_id'=>$id,'leave_type_id'=>'4'])->sum('leave_taken');

        $pdf=mPDF::loadView('employee.final_settlement',compact('employee','present_salary','interval','dayCount','previous_salary','total_earn_leave','leave_enjoyed','previous_salary_basic','encashment'));
        $name=time()."_".$id."_"."final_settlement.pdf";
        return $pdf->download($name);

    }

    public function UpdateResignationDate(Request $request){
//        return $request->all();
        $helper=AppHelper::instance();
        if(!$helper->checkIsAValidDate($request->date_of_discontinuation)){
            Session::flash('delete','Incorrect date format.');
            return $this->show(\session('emp_id'));
        }
        $this->validate($request,[
            'date_of_discontinuation'=>'required',
        ]);
        DB::table('employees')->where('id','=',$request->emp_id)->update([
            'date_of_discontinuation'=>Carbon::parse($request->date_of_discontinuation)->format('Y-m-d'),
            'reason_of_discontinuation'=>$request->reason_of_discontinuation,
        ]);
        return $this->final_settlement($request->emp_id);
    }

    public function sent_auto_sms(){
        $date=Carbon::now()->subDays(10)->toDateString();
        $now=Carbon::now()->toDateString();
        $dateBetween=0;
        $diff=1;
        $addDiff=0;
        $forceBreak=0;
        while($dateBetween!=10 && $forceBreak<50){
            $forceBreak++;
            $dateBetween=leaveController::dayCalculator1($date,$now);

            $diff=10-$dateBetween;
            $addDiff+=$diff;
            if($diff==0){
                $dateBetween=10;

            }
            else{
                $date=Carbon::now()->subDays(10+$addDiff)->toDateString();
            }
        }
        $sql="SELECT id FROM employees WHERE empAccStatus=1 AND id NOT IN (SELECT emp_id FROM attendance WHERE attendance.date>'$date') AND id NOT IN 
              (SELECT employee_id FROM `tb_leave_application` WHERE status=1 AND (leave_ending_date>='$date' AND leave_ending_date<='$now' OR leave_starting_date >= '$date' AND leave_starting_date<='$now'))";

//        return $sql;
        $ttDays=[];
        $dd=DB::select($sql);
        foreach ($dd as $d){
            $sql="SELECT attendance.*, employees.id, employees.empPhone, employees.employeeId, employees.empFirstName, employees.empLastName, designations.designation, departments.departmentName FROM `attendance` LEFT JOIN employees ON employees.id=attendance.emp_id LEFT JOIN designations ON employees.empDesignationId=designations.id LEFT JOIN departments ON employees.empDepartmentId=departments.id WHERE attendance.emp_id=$d->id ORDER BY `attendance`.`date` DESC LIMIT 1";
//            return $sql;
            $totalDays=DB::select($sql);
            if(!empty($totalDays[0])){
                $sDate=$totalDays[0]->date;
                $leaveSql="SELECT * FROM `tb_leave_application` WHERE leave_ending_date>='$sDate' AND leave_ending_date<='$now' OR leave_starting_date >= '$sDate' AND leave_starting_date<='$now' HAVING employee_id=$d->id AND  status='1'";
                $leaveCounts=DB::select($leaveSql);
                if(!count($leaveCounts)){
                    $totalDays[0]->day_between = leaveController::dayCalculator1($totalDays[0]->date,$now);

                }
                else{
                    $totalDays[0]->day_between = leaveController::dayCalculator1($totalDays[0]->date,$now)-$leaveCounts[0]->actual_days;


                }
            }
            $ttDays=array_merge($totalDays,$ttDays);
        }

        $employees = array();
        $sender=Auth::user();
        $now=Carbon::now()->toDateTimeString();
        $status = DB::table('tbauto_sms_sent_history')->whereDate('created_at', Carbon::today())->count();
        if($status==0){

        foreach($ttDays as $ttDay){

            $employeeName=$ttDay->empFirstName ." ".$ttDay->empLastName;

            if($ttDay->day_between==10){
                if(!empty($ttDay->empPhone)){
                   try{

                    $msg ="আপনি গত ".$ttDay->date." ইং তারিখ হতে অদ্যাবধি বিনা নোটিশে বা কর্তৃপক্ষের বিনা অনুমতিতে কারখানায় অনুপস্থিত রয়েছেন । আপনার এ ধরনের অনুপস্থিতির কারনে কারখানার উৎপাদন প্রক্রিয়া ব্যহত হচ্ছে বিধায় বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে অনতিবিলম্বে  আপনাকে অনুপস্থিতির কারন ব্যাখ্যা সহ কাজে যোগদান অথবা আপনার এ ধরনের বিনা নোটিশে বা উর্দ্ধতন কর্তৃপক্ষের বিনা অনুমতিতে ১০ দিনের অধিক সময় কারখানায় অনুপস্থিত থাকার বিষয়ে ব্যাখ্যা প্রদানের নির্দেশ প্রদান করা হল । ";
                       // messageController::sendSMS($phone, $msg);

                    $emp = array("employeeName"=>$employeeName, "designationName"=>$ttDay->designation, "departmentName"=>$ttDay->departmentName, "absentDays"=>$ttDay->day_between, "lastPresentDate"=>$ttDay->date, "id"=>$ttDay->id, "employeeId"=>$ttDay->employeeId, "empPhone"=>$ttDay->empPhone,"status"=>1);

                        $employees[]=$emp;

                        DB::table('tbcaenhistory')->insert(
                            [
                                'emp_id' => $ttDay->id,
                                'senderId' => $sender->emp_id,
                                'ntype'=>'1', 
                                'ndate'=>$now,
                                'absentDays'=>$ttDay->day_between,
                                'lastPresent'=>$ttDay->date,
                                'created_at'=>$now,
                                'updated_at'=>$now
                            ]
                        );

                    }
                    catch (Exception $e) {
                         $emp = array("employeeName"=>$employeeName, "designationName"=>$ttDay->designation, "departmentName"=>$ttDay->departmentName, "absentDays"=>$ttDay->day_between, "lastPresentDate"=>$ttDay->date, "id"=>$ttDay->id, "employeeId"=>$ttDay->employeeId, "empPhone"=>$ttDay->empPhone,"status"=>0);

                        $employees[]=$emp;

                    }
                }else{
                  $emp = array("employeeName"=>$employeeName, "designationName"=>$ttDay->designation, "departmentName"=>$ttDay->departmentName, "absentDays"=>$ttDay->day_between, "lastPresentDate"=>$ttDay->date, "id"=>$ttDay->id, "employeeId"=>$ttDay->employeeId, "empPhone"=>$ttDay->empPhone,"status"=>2);

                    $employees[]=$emp;
                }
            }


            if($ttDay->day_between==18){
                if(!empty($ttDay->empPhone)){
                   try{
                       $msg ="আপনি গত ".$ttDay->date." ইং তারিখ হতে অদ্যাবধি বিনা নোটিশে বা কর্তৃপক্ষের বিনা অনুমতিতে কারখানায় অনুপস্থিত রয়েছেন । আপনার এ ধরনের অনুপস্থিতির কারনে কারখানার উৎপাদন প্রক্রিয়া ব্যহত হচ্ছে বিধায় বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে অনতিবিলম্বে  আপনাকে অনুপস্থিতির কারন ব্যাখ্যা সহ কাজে যোগদান অথবা আপনার এ ধরনের বিনা নোটিশে বা উর্দ্ধতন কর্তৃপক্ষের বিনা অনুমতিতে ১৮ দিনের অধিক সময় কারখানায় অনুপস্থিত থাকার বিষয়ে ব্যাখ্যা প্রদানের নির্দেশ প্রদান করা হল । ";
                       // messageController::sendSMS($phone, $msg);
                        
                        $emp = array("employeeName"=>$employeeName, "designationName"=>$ttDay->designation, "departmentName"=>$ttDay->departmentName, "absentDays"=>$ttDay->day_between, "lastPresentDate"=>$ttDay->date, "id"=>$ttDay->id, "employeeId"=>$ttDay->employeeId, "empPhone"=>$ttDay->empPhone,"status"=>1);
                        $employees[]=$emp;


                        DB::table('tbcaenhistory')->insert(
                            [
                                'emp_id' => $ttDay->id,
                                'senderId' => $sender->emp_id,
                                'ntype'=>'1', 
                                'ndate'=>$now,
                                'absentDays'=>$ttDay->day_between,
                                'lastPresent'=>$ttDay->date,
                                'created_at'=>$now,
                                'updated_at'=>$now
                            ]
                        );

                    }
                    catch (Exception $e) {
                         $emp = array("employeeName"=>$employeeName, "designationName"=>$ttDay->designation, "departmentName"=>$ttDay->departmentName, "absentDays"=>$ttDay->day_between, "lastPresentDate"=>$ttDay->date, "id"=>$ttDay->id, "employeeId"=>$ttDay->employeeId, "empPhone"=>$ttDay->empPhone,"status"=>0);
                        $employees[]=$emp;
                    }
                }else{
                  $emp = array("employeeName"=>$employeeName, "designationName"=>$ttDay->designation, "departmentName"=>$ttDay->departmentName, "absentDays"=>$ttDay->day_between, "lastPresentDate"=>$ttDay->date, "id"=>$ttDay->id, "employeeId"=>$ttDay->employeeId, "empPhone"=>$ttDay->empPhone,"status"=>2);

                    $employees[]=$emp;
                }
            }


            if($ttDay->day_between==28){
                if(!empty($ttDay->empPhone)){
                   try{
                       $msg ="আপনি গত ".$ttDay->date." ইং তারিখ হতে অদ্যাবধি বিনা নোটিশে বা কর্তৃপক্ষের বিনা অনুমতিতে কারখানায় অনুপস্থিত রয়েছেন । আপনার এ ধরনের অনুপস্থিতির কারনে কারখানার উৎপাদন প্রক্রিয়া ব্যহত হচ্ছে বিধায় বাংলাদেশ শ্রম আইন, ২০০৬ এর ২৭(৩ক) ধারামতে অনতিবিলম্বে  আপনাকে অনুপস্থিতির কারন ব্যাখ্যা সহ কাজে যোগদান অথবা আপনার এ ধরনের বিনা নোটিশে বা উর্দ্ধতন কর্তৃপক্ষের বিনা অনুমতিতে ২৮ দিনের অধিক সময় কারখানায় অনুপস্থিত থাকার বিষয়ে ব্যাখ্যা প্রদানের নির্দেশ প্রদান করা হল । ";
                       // messageController::sendSMS($phone, $msg);
                    
                        $emp = array("employeeName"=>$employeeName, "designationName"=>$ttDay->designation, "departmentName"=>$ttDay->departmentName, "absentDays"=>$ttDay->day_between, "lastPresentDate"=>$ttDay->date, "id"=>$ttDay->id, "employeeId"=>$ttDay->employeeId, "empPhone"=>$ttDay->empPhone,"status"=>1);
                        $employees[]=$emp;


                        DB::table('tbcaenhistory')->insert(
                            [
                                'emp_id' => $ttDay->id,
                                'senderId' => $sender->emp_id,
                                'ntype'=>'1', 
                                'ndate'=>$now,
                                'absentDays'=>$ttDay->day_between,
                                'lastPresent'=>$ttDay->date,
                                'created_at'=>$now,
                                'updated_at'=>$now
                            ]
                        );
                        
                    }
                    catch (Exception $e) {
                         $emp = array("employeeName"=>$employeeName, "designationName"=>$ttDay->designation, "departmentName"=>$ttDay->departmentName, "absentDays"=>$ttDay->day_between, "lastPresentDate"=>$ttDay->date, "id"=>$ttDay->id, "employeeId"=>$ttDay->employeeId, "empPhone"=>$ttDay->empPhone,"status"=>0);
                        $employees[]=$emp;
                    }
                }else{
                  $emp = array("employeeName"=>$employeeName, "designationName"=>$ttDay->designation, "departmentName"=>$ttDay->departmentName, "absentDays"=>$ttDay->day_between, "lastPresentDate"=>$ttDay->date, "id"=>$ttDay->id, "employeeId"=>$ttDay->employeeId, "empPhone"=>$ttDay->empPhone,"status"=>2);
                    $employees[]=$emp;
                }
            }
        }


        DB::table('tbauto_sms_sent_history')->insert(
            [
                'createBy' => $sender->emp_id,
                'created_at'=>$now,
                'updated_at'=>$now
            ]
        );
    }else{
        Session::flash('message',"Already SMS were sent for today.");
        }

        return view('attendance.sms_confirmation_list', compact('employees'));

    }

    public function job_application_form($id){
        $emp=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->leftjoin('marital_statuses', 'employees.empMaritalStatusId','=','marital_statuses.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName','marital_statuses.bnName as bnMeritalStatus','marital_statuses.name as enMeritalStatus')
            ->where('employees.id','=',$id)
            ->first();

            // return view('employee.job_application_form',compact('emp'));
            $pdf=mPDF::loadView('employee.job_application_form',compact('emp'));
       
        $name=time()."_".$id."_"."job_application_form.pdf";
        return $pdf->download($name);
    }


    public function employee_background_check($id){
        $emp=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->leftjoin('marital_statuses', 'employees.empMaritalStatusId','=','marital_statuses.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName','marital_statuses.bnName as bnMeritalStatus','marital_statuses.name as enMeritalStatus')
            ->where('employees.id','=',$id)
            ->first();
            // dd($emp);
            // return view('employee.employee_background_check',compact('emp'));
            $pdf=mPDF::loadView('employee.employee_background_check',compact('emp'));
       
        $name=time()."_".$id."_"."employee_background_check.pdf";
        return $pdf->download($name);
    }


    public function age_ability_certificate($id){
        $emp=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->leftjoin('marital_statuses', 'employees.empMaritalStatusId','=','marital_statuses.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName','marital_statuses.bnName as bnMeritalStatus','marital_statuses.name as enMeritalStatus')
            ->where('employees.id','=',$id)
            ->first();

            // return view('employee.age_ability_certificate_pdf',compact('emp'));
            $pdf=mPDF::loadView('employee.age_ability_certificate_pdf',compact('emp'));
       
        $name=time()."_".$id."_"."age_ability_certificate.pdf";
        return $pdf->download($name);
    }


    public function nominee_form_pdf($id){
        $emp=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->leftjoin('marital_statuses', 'employees.empMaritalStatusId','=','marital_statuses.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName','marital_statuses.bnName as bnMeritalStatus','marital_statuses.name as enMeritalStatus')
            ->where('employees.id','=',$id)
            ->first();

            // return view('employee.nominee_form_pdf',compact('emp'));
            $pdf=mPDF::loadView('employee.nominee_form_pdf',compact('emp'));
       
        $name=time()."_".$id."_"."nominee_form_pdf.pdf";
        return $pdf->download($name);
    }


    public function appointment_latter($id){
        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName')
            ->where('employees.id','=',$id)
            ->get();
            
        $employee1=DB::table('employees')
            ->where('employees.id','=',$id)
            ->first();

        $salary = DB::table('payroll_salary')
            ->leftjoin('payroll_grade', 'payroll_salary.grade_id','=','payroll_grade.id')
            ->where('payroll_salary.emp_id','=',$id)
            ->select('payroll_salary.*','payroll_grade.grade_name as gradeName')
            ->get();

        if(($employee1->work_group)=="Staff"){  
            $pdf=mPDF::loadView('employee.staff_appointment_latter',compact('employee','salary'));
        }elseif(($employee1->work_group)=="Worker"){
            $pdf=mPDF::loadView('employee.worker_appointment_latter',compact('employee','salary'));
        }else{
            Session::flash('appointment_latter_message',"Please insert employee work group first to download appointment latter.");
            return back();
        }

        $name=time()."_".$id."_"."appointment_latter.pdf";
        return $pdf->download($name);
    }


    public function training_subject_list($id){
        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName')
            ->where('employees.id','=',$id)
            ->get();

            $pdf=mPDF::loadView('employee.training_list',compact('employee'));
       
        $name=time()."_".$id."_"."Training_Subject_List.pdf";
        return $pdf->download($name);
    }


    public function sewing_machine_responsibility_guide($id){
        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName')
            ->where('employees.id','=',$id)
            ->get();

            $pdf=mPDF::loadView('employee.sewing_machine_responsibility_guide',compact('employee'));
       
        $name=time()."_".$id."_"."sewing_machine_responsibility_guide.pdf";
        return $pdf->download($name);
    }

    public function finishing_assistant_responsibility_guide($id){
        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName')
            ->where('employees.id','=',$id)
            ->get();

            $pdf=mPDF::loadView('employee.finishing_assistant_responsibility_guide',compact('employee'));
       
        $name=time()."_".$id."_"."finishing_assistant_responsibility_guide.pdf";
        return $pdf->download($name);
    }

    public function sewing_assistant_machine_responsibility_guide($id){
        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName')
            ->where('employees.id','=',$id)
            ->get();

            $pdf=mPDF::loadView('employee.sewing_assistant_machine_responsibility_guide',compact('employee'));
       
        $name=time()."_".$id."_"."sewing_assistant_machine_responsibility_guide.pdf";
        return $pdf->download($name);
    }


    public function quality_inspactor_responsibility_guide($id){
        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName')
            ->where('employees.id','=',$id)
            ->get();

            $pdf=mPDF::loadView('employee.quality_inspactor_responsibility_guide',compact('employee'));
       
        $name=time()."_".$id."_"."quality_inspactor_responsibility_guide.pdf";
        return $pdf->download($name);
    }

    public function line_ironman_responsibility_guide($id){
        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','units.bnName as bnName','designations.designation','designations.designationBangla','departments.departmentName','departments.departBanglaName')
            ->where('employees.id','=',$id)
            ->get();

            $pdf=mPDF::loadView('employee.line_ironman_responsibility_guide',compact('employee'));
       
        $name=time()."_".$id."_"."line_ironman_responsibility_guide.pdf";
        return $pdf->download($name);
    }

    public function generate_pva($id){

        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','designations.designation','departments.departmentName')
            ->where('employees.id','=',$id)
            ->take(1)
            ->get();
        return view('employee.police_verification_structure',compact('id','employee'));
    }

    public function save_pva_data(Request $request){
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();

        foreach($request->address as $key=>$value)
        {
            $address= $value;
            $insert[]=
            [   
                'empId'=>$request->empId,
                'address'=>$address,
                'fromAddress'=>Carbon::parse($_POST['fromAddress'][$key])->format('Y-m-d'),
                'toAddress'=>Carbon::parse($_POST['toAddress'][$key])->format('Y-m-d'),
                'remarks'=>$_POST['remarks'][$key],
                'createdBy'=>$user->id,
                'created_at'=>$now,
                'updated_at'=>$now
            ];
        }

        $insertData = DB::table('tbemployee_living_history_data')->insert($insert);

         DB::table('tbpolice_verification_data')->insert([
            'empId'=>$request->empId,
            'pvDate'=>Carbon::parse($request->pvDate)->format('Y-m-d'),
            'vpkt'=>$request->vpkt,
            'vpkz'=>$request->vpkz,
            'mpsz'=>$request->mpsz,
            'fathersJob'=>$request->fathersJob,
            'fathersNationality'=>$request->fathersNationality,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now
        ]);

         $id = $request->empId;
         return redirect("employee/".$id);

    }

    public function download_police_varification_application($id)
    {
        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','designations.designation','designations.designationBangla','departments.departmentName')
            ->where('employees.id','=',$id)
            ->take(1)
            ->get();

        $police_verification_data=DB::table('tbpolice_verification_data')->where('empId','=',$id)->take(1)->get();

        $living_history=DB::table('tbemployee_living_history_data')->where('empId','=',$id)->get();
        $pdf=mPDF::loadView('employee.police_verification_pdf_bangla',compact('employee','living_history','police_verification_data'));
        $name=time()."_".$id."_"."police_verification_latter.pdf";
        return $pdf->download($name);

    }
    
    public function store_medical_history(Request $request){
        
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();

         DB::table('tbemployee_medical_history')->insert([
            'empId'=>$request->empId,
            'curHCs'=>$request->curHCs, 
            'curHCt'=>$request->curHCt,
            'curMedicationss'=>$request->curMedicationss, 
            'curMedicationst'=>$request->curMedicationst, 
            'mcpis'=>$request->mcpis, 
            'mcpit'=>$request->mcpit, 
            'pastSurgerys'=>$request->pastSurgerys, 
            'pastSurgeryt'=>$request->pastSurgeryt, 
            'ppmips'=>$request->ppmips, 
            'ppmipt'=>$request->ppmipt,  
            'visionProblems'=>$request->visionProblems, 
            'visionProblemt'=>$request->visionProblemt, 
            'hearProblems'=>$request->hearProblems, 
            'hearProblemt'=>$request->hearProblemt, 
            'skillConditions'=>$request->skillConditions, 
            'skillConditiont'=>$request->skillConditiont, 
            'schands'=>$request->schands, 
            'schandt'=>$request->schandt, 
            'chemicals'=>$request->chemicals, 
            'chemicalt'=>$request->chemicalt, 
            'noises'=>$request->noises, 
            'noiset'=>$request->noiset, 
            'radiations'=>$request->radiations, 
            'radiationt'=>$request->radiationt, 
            'latexs'=>$request->latexs, 
            'latext'=>$request->latext, 
            'drugss'=>$request->drugss, 
            'drugst'=>$request->drugst, 
            'chemicalass'=>$request->chemicalass, 
            'chemicalast'=>$request->chemicalast, 
            'insects'=>$request->insects, 
            'insectt'=>$request->insectt, 
            'fragrancess'=>$request->fragrancess, 
            'fragrancest'=>$request->fragrancest, 
            'others'=>$request->others, 
            'othert'=>$request->othert,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        
        Session::flash('mhmessage','Employee medical history has been successfully recorded.');

         $id = $request->empId;
         return redirect("employee/".$id);

    }
   public function update_medical_history(Request $request,$id){


        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();

         DB::table('tbemployee_medical_history')->where(['empId'=>$id])->update([
            'curHCs'=>$request->curHCs, 
            'curHCt'=>$request->curHCt,
            'curMedicationss'=>$request->curMedicationss, 
            'curMedicationst'=>$request->curMedicationst, 
            'mcpis'=>$request->mcpis, 
            'mcpit'=>$request->mcpit, 
            'pastSurgerys'=>$request->pastSurgerys, 
            'pastSurgeryt'=>$request->pastSurgeryt, 
            'ppmips'=>$request->ppmips, 
            'ppmipt'=>$request->ppmipt,  
            'visionProblems'=>$request->visionProblems, 
            'visionProblemt'=>$request->visionProblemt, 
            'hearProblems'=>$request->hearProblems, 
            'hearProblemt'=>$request->hearProblemt, 
            'skillConditions'=>$request->skillConditions, 
            'skillConditiont'=>$request->skillConditiont, 
            'schands'=>$request->schands, 
            'schandt'=>$request->schandt, 
            'chemicals'=>$request->chemicals, 
            'chemicalt'=>$request->chemicalt, 
            'noises'=>$request->noises, 
            'noiset'=>$request->noiset, 
            'radiations'=>$request->radiations, 
            'radiationt'=>$request->radiationt, 
            'latexs'=>$request->latexs, 
            'latext'=>$request->latext, 
            'drugss'=>$request->drugss, 
            'drugst'=>$request->drugst, 
            'chemicalass'=>$request->chemicalass, 
            'chemicalast'=>$request->chemicalast, 
            'insects'=>$request->insects, 
            'insectt'=>$request->insectt, 
            'fragrancess'=>$request->fragrancess, 
            'fragrancest'=>$request->fragrancest, 
            'others'=>$request->others, 
            'othert'=>$request->othert,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);
        
        Session::flash('mhmessage','Employee medical history has been successfully Updated.');
         return redirect("employee/".$id);
    }
    
    public function download_ems($id)
    {
        $employee=DB::table('employees')
            ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
            ->leftjoin('designations', 'employees.empDesignationId','=','designations.id')
            ->leftjoin('units', 'employees.unit_id','=','units.id')
            ->select('employees.*','units.name as unitName','designations.designation','departments.departmentName')
            ->where('employees.id','=',$id)
            ->take(1)
            ->first();

        $medical_history=DB::table('tbemployee_medical_history')
            ->where('empId','=',$id)
            ->take(1)
            ->first();

         $pdf=mPDF::loadView('employee.medical_history_pdf_bangla',compact('employee','medical_history'));
        $name=time()."_".$id."_"."medical_history.pdf";
        return $pdf->download($name);

    }
  

    public function arrayPaginator($array, $request)
    {
        $page = $request->page;
        $perPage = 51;
        $offset = ($page * $perPage) - $perPage;

        return new \Illuminate\Pagination\LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }

    //employee three shift add view
    public function emp_three_shift_add_view(){
        $employee=DB::table('employees')->where('is_three_shift',1)->select('id','employeeId','empFirstName')->get();
        return view('employee.three_shift.add',compact('employee'));
    }

    //Employee Shift Status update ajax call
    public function emp_three_shift_add_process(Request $request){
         $start=date('Y-m-d',strtotime($request->start_date));
         $end=date('Y-m-d',strtotime($request->end_date));
         $y=date('Y',strtotime($request->start_date));
         $m=date('m',strtotime($request->end_date));
         $check=DB::table('three_shift_employee')->where('emp_id',$request->emp_id)->whereYear('shift_date',$y)->whereMonth('shift_date',$m)->count();
         if($check>0){
            Session::flash('error','Data already exists');
            return redirect()->route('emp_three_shift_add');
        }else{
            $shifts=DB::table('attendance_setup')->get();
            $week_leave=DB::table('week_leave')->get();
            $employee=DB::table('employees')->where('id',$request->emp_id)->select('id','employeeId','empFirstName')->first();
           return view('employee.three_shift.request_data',compact('start','end','shifts','week_leave','employee'));
        }

    }

    //employee 3-shift information store to database
    public function emp_three_shift_add_process_store(Request $request){
            for($i=0;$i<count($request->shift_date);$i++){
                $insert=DB::table('three_shift_employee')->insert([
                    'emp_id' =>$request->emp_id,
                    'emp_shift'=>$request->emp_shift[$i],
                    'shift_date' =>$request->shift_date[$i],
                    'weekend' =>$request->weekend[$i],
                   ]);
            }
            Session::flash('success','Information has been saved successfully');
            return redirect()->route('emp_three_shift_add');
    }

    //update view page 3-shift data
    public function emp_three_shift_update_process(Request $request){
        $start=date('Y-m-d',strtotime($request->start_date));
        $end=date('Y-m-d',strtotime($request->end_date));
        $y=date('Y',strtotime($request->start_date));
        $m=date('m',strtotime($request->end_date));
        $check=DB::table('three_shift_employee')
        ->where('emp_id',$request->emp_id)
        ->whereYear('shift_date',$y)
        ->whereMonth('shift_date',$m)
        ->count();
        if($check>0){
            $data=DB::table('three_shift_employee')
            ->where('emp_id',$request->emp_id)->whereBetween('shift_date',[$start,$end])
            ->orderBy('shift_date')
            ->get();
            $employee=DB::table('employees')->where('id',$request->emp_id)->select('id','employeeId','empFirstName')->first();
            $shift=DB::table('attendance_setup')->get();
            return view('employee.three_shift.emp_three_shift_add_update',compact('data','employee','shift','check'));

        }else{
            Session::flash('error','Sorry no data found!Pleas add shift');
            return redirect()->route('emp_three_shift_add');
        }
    }

    //update 3-shift employee data
    public function update_thre_shift(Request $request){
        $date=date('Y-m-d',strtotime($request->shift_date));
        $check=DB::table('three_shift_employee')->where('emp_id',$request->emp_id)->where('shift_date',$date)->count();
        if($check>0){
            Session::flash('error','Shift date already exists');
            return back();
        }else{
            $insert=DB::table('three_shift_employee')->insert([
                'emp_id' =>$request->emp_id,
                'emp_shift'=>$request->emp_shift,
                'shift_date' =>$date,
                'weekend' =>$request->weekend,
               ]);
               return back();
        } 
    }

    //delete three shift
    public function delete_thre_shift($id){
      $delete=DB::table('three_shift_employee')
      ->where('id',$id)->delete();
      return back();
    }

    //all data update for 3-shift employee
    public function update_thre_shift_employee_final_all(Request $request){
       $y=date('Y',strtotime($request->shift_date[0])); 
       $m=date('m',strtotime($request->shift_date[0])); 
       $t=DB::table('three_shift_employee')
       ->whereYear('shift_date',$y)
       ->whereMonth('shift_date',$m)
       ->where('emp_id',$request->emp_id)
       ->delete();
       for($i=0;$i<count($request->shift_date);$i++){
        $insert=DB::table('three_shift_employee')->insert([
            'emp_id' =>$request->emp_id,
            'emp_shift'=>$request->emp_shift[$i],
            'shift_date' =>$request->shift_date[$i],
            'weekend' =>$request->weekend[$i],
           ]);
    }
    Session::flash('success','Information has been update successfully');
    return back();
    }


}
