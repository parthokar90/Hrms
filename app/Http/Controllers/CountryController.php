<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Excel;


class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['middleware' => 'check-permission:admin|hr|hr-admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country = \DB::table('nationalities')
            ->leftJoin('users', 'nationalities.created_by', '=', 'users.id')
            ->select('nationalities.id as id', 'nationalities.name as n_name', 'users.name as u_name', 'users.id as u_id')
            ->get();


//        $country=\DB::table('nationalities')->latest()->get();
//        //return $country;
        return view('settings.country.index', compact('country'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);


        $user = Auth::user();
        \DB::table('nationalities')->insert(['name' => $request->name, 'created_by' => $user->id, 'updated_by' => $user->id]);
        return redirect("settings/country");
        $now = Carbon::now()->toDateTimeString();


        $user = Auth::user();
        \DB::table('nationalities')->insert([
            'name' => $request->name,
            'created_by' => $user->id,
            'updated_by' => $user->id,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        if ($user) {
            Session::flash('message', 'Successful ! Nationality is added');
            return redirect("settings/country");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);

        \DB::table('nationalities')->where('id', $id)
            ->update([
                'name' => $request->name,
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
        Session::flash('edit', 'Information Successfully Updated');
        return redirect("settings/country");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = DB::table('employees')->where(['empNationalityId' => $id])->get();
        if (count($check)) {
            Session::flash('edit', "Can't delete. There are already some employees in this country.");
        } else {
            DB::table('nationalities')->where('id', $id)->delete();
            Session::flash('delete', 'Nationality Deleted');
        }
        return redirect("settings/country");

    }

}
