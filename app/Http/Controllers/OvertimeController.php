<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OvertimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $overtime=DB::table('tb_overtime')->get();
        return view('settings.overtime.index',compact('overtime'));
    }


//    public function create()
//    {
//        //
//    }

//    public function store(Request $request)
//    {
//        //
//    }

//    public function show($id)
//    {
//        //
//    }

//    public function edit($id)
//    {
//        //
//    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'min_overtime'=>'required',
            'max_overtime'=>'required',
        ]);
        DB::table('tb_overtime')->where('id','=',$id)->update([
            'min_overtime'=>$request->min_overtime,
            'max_overtime'=>$request->max_overtime,
        ]);
        Session::flash('message','Overtime settings updated.');
        return redirect(route('overtime.index'));
    }

    public static function overtime_start(){
        $overtime_start=DB::table('tb_overtime')->first()->min_overtime;
        return $overtime_start;

    }
    public static function overtime_end(){
        $overtime_end=DB::table('tb_overtime')->first()->max_overtime;
        return $overtime_end;

    }
//    public function destroy($id)
//    {
//        //
//    }
}
