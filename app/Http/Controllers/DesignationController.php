<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DesignationController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'middleware'=>'check-permission:admin|hr|hr-admin',
        ]);
    }

    public function index()
    {
        $payroll_grade=DB::table('payroll_grade')->get();
        $designations=DB::table('designations')
        ->leftjoin('payroll_grade', 'payroll_grade.id','=','designations.gradeId')
        ->select('designations.*','payroll_grade.grade_name as gradeName')
        ->get();
        return view('settings.designation.index',compact('designations','payroll_grade'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);
        $now=Carbon::now()->toDateTimeString();

        $des=DB::table('designations')->insert([
            'designation'=>$request->name,
            'designationBangla'=>$request->designationBangla,
            'description'=>$request->description,
            'gradeId'=>$request->gradeId,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);
        if($des){
            Session::flash('message','Designation has been successfully inserted.');
            return redirect("settings/designation");
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);
        $now=Carbon::now()->toDateTimeString();
        $upd=DB::table('designations')->where(['id'=>$id])->update([
            'designation'=>$request->name,
            'designationBangla'=>$request->designationBangla,
            'description'=>$request->description,
            'gradeId'=>$request->gradeId,
            'updated_at'=>$now,
        ]);
        if($upd)
        {
            Session::flash('edit', "Designation has been successfully updated.");
        }
        return redirect("settings/designation");
    }

    public function destroy($id)
    {
        $check=DB::table('employees')->where(['empDesignationId'=>$id])->get();
        if(count($check)){
            Session::flash('edit',"Can't delete. There are already some employees bearing this designation." );
        }
        else {
            DB::table('designations')->where(['id'=>$id])->delete();
            Session::flash('delete', "Designation has been successfully deleted.");
        }
        return redirect("settings/designation");
    }
}
