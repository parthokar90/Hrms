<?php 


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Auth;
use redirect;
use Session;
use stdClass;
use PDF;
use Excel;
use mPDF;
use DateTime;
use DatePeriod;
use DateInterval;

class EmpSalaryProcess extends Controller
{

//working day find function
public static function dayCalculator($d1,$d2){
    $date1=strtotime($d1);
    $date2=strtotime($d2);
    $interval1=1+round(abs($date1-$date2)/86400);
    $festivalLeave = DB::table('tb_festival_leave')->get();
    $dcount=0;
    foreach($festivalLeave as $fleave){
        if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
            $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);

        }
        else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
            $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);

        }
        else{
            continue;
        }
    }
    $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
    $start_date=date('Y-m-d', $date1);
    $end_date=date('Y-m-d', $date2);
    $key=0;
    for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {

        $dates = $i->format('Y-m-d');
        $timestamp = strtotime($dates);
        $day = date('l', $timestamp);

        for($j=0;$j<count($weekends);$j++)
        {
            if($day==$weekends[$j]){
                $key++;
            }
        }

    }
    $interval=(int)$interval1-((int)$dcount+$key);
    return $interval;
}


//Weekend Dates My Code
function weekend_dates($d1,$d2){
    $date1=strtotime($d1);
    $date2=strtotime($d2);
    $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
    $start_date=date('Y-m-d', $date1);
    $end_date=date('Y-m-d', $date2);
    $weekend_dates=array();
    for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
        $dates = $i->format('Y-m-d');
        $timestamp = strtotime($dates);
        $day = date('l', $timestamp);
        for($j=0;$j<count($weekends);$j++)
        {
            if($day==$weekends[$j]){
                $weekend_dates[]=$dates;
            }
        }
    }
    return $weekend_dates;
}
//Weekend Dates End My Code



//weekend find function
function weekdayCalculator($d1,$d2){
    $date1=strtotime($d1);
    $date2=strtotime($d2);
    $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
    $start_date=date('Y-m-d', $date1);
    $end_date=date('Y-m-d', $date2);
    $key=0;

    $festivalLeave = DB::table('tb_festival_leave')
        ->where(DB::raw('year(fest_starts)'),date('Y', $date1))
        ->orWhere(DB::raw('year(fest_ends)'),date('Y', $date1))
        ->get();
//        return $festivalLeave;

    $weekend_dates=array();
    for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
        $dates = $i->format('Y-m-d');
        $timestamp = strtotime($dates);
        $day = date('l', $timestamp);
        for($j=0;$j<count($weekends);$j++)
        {
            if($day==$weekends[$j]){
                $key++;
                $weekend_dates[]=$dates;
            }
        }
    }
    $h=0;
    foreach ($festivalLeave as $f){
//            return $f->fest_starts."     ".$f->fest_ends;
        foreach ($weekend_dates as $wd){
            if($wd>=$f->fest_starts && $wd<=$f->fest_ends){
                $h++;
            };
        }
    }
    return $key-$h;
}


//holiday find function
function holiday($d1,$d2){
    $date1=strtotime($d1);
    $date2=strtotime($d2);
    $interval1=1+round(abs($date1-$date2)/86400);
    $festivalLeave = DB::table('tb_festival_leave')->get();
    $dcount=0;
    foreach($festivalLeave as $fleave){
        if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
            $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
        }
        else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
            $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
        }
        else{
            continue;
        }
    }
    return $dcount;
}


//employee wise salary sheet
public function employee_SalarySheet(Request $request){
    $id=$request->emp_id;
    $overtime_hours_count=DB::table('tb_overtime_hour_count')->first();
    DB::table('total_employee_leave')->delete();
    DB::table('extra_ot_result')->delete();
    DB::table('tb_total_present')->delete();
    DB::table('employee_overtime')->delete();
    DB::table('temp_employee_overtime')->delete();
    DB::table('salary_ot')->delete();
    DB::table('emp_total_late')->delete();
    DB::table('emp_total_lates')->delete();
    DB::table('regular_weekend_presence')->delete();
    DB::table('total_weekend_present')->delete();
    $s=date('Y-m-01',strtotime($request->salary_sheet_month));
    $e=date('Y-m-t',strtotime($request->salary_sheet_month));
    $leaveattyear=date('Y',strtotime($request->salary_sheet_month));
    $leaveattmonth=date('m',strtotime($request->salary_sheet_month));
    DB::table('tb_salary_history')
    ->where('emp_id',$id)
    ->where('month',$request->salary_sheet_month)
    ->delete();
    //check leave between attendance then delete attendance 
    $delete=DB::SELECT("SELECT attendance.id as att_id 
    FROM leave_of_employee 
    LEFT JOIN attendance ON leave_of_employee.emp_id=attendance.emp_id
    WHERE leave_of_employee.emp_id='$id' AND attendance.date BETWEEN leave_of_employee.start_date AND leave_of_employee.end_date AND YEAR(leave_of_employee.month)='$leaveattyear' AND MONTH(leave_of_employee.month)='$leaveattmonth'"); 		
    $delete_id = [];
    foreach ($delete as $key => $value) {
        $delete_id[]=$value->att_id;
    } 
    $ok = DB::table('attendance')->whereIn('id', $delete_id)->delete();
    //check leave between attendance then delete attendance 
    
    //Start For In Active Employee
    DB::table('employees')
    ->where('date_of_discontinuation','>=',$s)
    ->where('id',$id)
    ->update([
        'empAccStatus'=>1,
    ]);
    //End For In Active Employee
    $set_date='';
    $mof_month=date('t',strtotime($request->salary_sheet_month))/2;
    $round_middle=(int)$mof_month;
    $regular_days_setup_check=DB::table('tbweekend_regular_day')
    ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
    ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
    ->count();
// MY CODE
    $regular_fridays_date=DB::table('tbweekend_regular_day')
    ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
    ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
    ->get(['weekend_date']);
    $regular_weekend_dates=array();
    foreach ($regular_fridays_date as $rfd){
        $regular_weekend_dates[]=$rfd->weekend_date;
    }
    $regular_weekend_presence=DB::table('employees')
    ->where('employees.is_three_shift',0)
    ->where('employees.id',$id)
    ->join('attendance','employees.id', '=', 'attendance.emp_id')
    ->whereIn('attendance.date',$regular_weekend_dates)
    ->select('employees.id as emp_id',DB::raw("count(attendance.emp_id) as present"))
    ->groupBy('employees.id')
    ->get();
    foreach ($regular_weekend_presence as $presents){
        $insert[]=[
            'emp_id'=>$presents->emp_id,
            'regular_present'=>$presents->present,
        ];
    }
    if(!empty($insert)){
        DB::table('regular_weekend_presence')->insert($insert);
        unset($insert);
    }
    $weekend_present=DB::table('employees')
    ->where('employees.is_three_shift',0)
    ->where('employees.id',$id)
    ->join('attendance','employees.id','=','attendance.emp_id')
    ->whereIn('attendance.date',$this->weekend_dates($s,$e))
    ->select(DB::raw('count(employees.id) as weekend_present'),'employees.id as emp_id')
    ->groupBy('employees.id')
    ->get();
    foreach ($weekend_present as $wp){
        $insert[]=[
            'emp_id'=>$wp->emp_id,
            'weekend_present'=>$wp->weekend_present,
        ];
    }
    if(!empty($insert)){
        DB::table('total_weekend_present')->insert($insert);
        unset($insert);
    }
// My Code End
    $money_deduction=10;
    $month_check=date('Y-m-d',strtotime($request->salary_sheet_month));
    // $weeks=$this->weekdayCalculator($s,$e)-$regular_days_setup_check;
    // $holii=$this->holiday($s,$e);
    // $actual_working_day=date('t',strtotime($request->salary_sheet_month))-$weeks-$holii;
    $process_check=DB::table('tb_salary_process_check')
        ->where('month',$month_check)
        ->where('status','=',1)
        ->count();
    if($process_check>0){
        Session::flash('salaryprocesscheck','Process Salary Already Completed this month');
        return redirect()->back();
    }
    $date=date('m-Y');
    $current_time = Carbon::now()->toDateTimeString();
    $current_month_first=date('Y-m-01',strtotime($request->salary_sheet_month));
    $current_month_last=date('Y-m-t',strtotime($request->salary_sheet_month));
    $month=date('Y-m-d',strtotime($request->salary_sheet_month));
    $workingmonth=$request->salary_sheet_month;
    // $overtime_result = DB::SELECT(DB::raw("SELECT attendance.emp_id, employees.empFirstName,attendance.emp_id,attendance.out_time,attendance.date,MINUTE(attendance.out_time)-MINUTE(attendance_setup.exit_time) as overtime_minute,SUM(IF(HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time)>2,2,HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time))) as tot,SUM(CASE WHEN HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time)>1 THEN 0 WHEN MINUTE(attendance.out_time)-MINUTE(attendance_setup.exit_time)>54 THEN 1 WHEN MINUTE(attendance.out_time)-MINUTE(attendance_setup.exit_time)>24 THEN .5 END) AS t FROM attendance LEFT JOIN employees ON attendance.emp_id=employees.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE attendance.date BETWEEN '$current_month_first' AND '$current_month_last' AND employees.empOTStatus=1 AND HOUR(attendance.out_time)>HOUR(attendance_setup.exit_time) AND attendance.out_time != attendance.in_time AND DAYNAME(attendance.date)!='Friday' GROUP BY attendance.emp_id"));
    $regular_work_days =DB::table('tbweekend_regular_day')
    ->select(DB::raw('group_concat(weekend_date) as date'))
    ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
    ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
    ->groupBy('weekend_month')
    ->first();
    if(isset($regular_work_days)){
       $set_date=$regular_work_days->date;
    }
   $overtime_result=DB::SELECT("SELECT attendance.*,
   attendance.emp_id,employees.employeeId,employees.empFirstName,
   attendance.date as att_date 
   FROM attendance
   LEFT JOIN employees ON attendance.emp_id=employees.id 
   WHERE attendance.emp_id='$id' AND attendance.date BETWEEN '$current_month_first' AND '$current_month_last' AND employees.empOTStatus=1 AND employees.is_three_shift=0 AND DAYNAME(attendance.date)!='Friday' AND attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times GROUP BY attendance.emp_id, attendance.date 
   ");
   $insert_data = []; 
   foreach($overtime_result as $extraots){  
    $ot_data=[
                 'emp_id' =>$extraots->emp_id,
                 'out_time'=>$extraots->out_time,
                 'exit_time'=>$extraots->exit_times,
                 'att_date'=>date('Y-m-d',strtotime($extraots->att_date)),
             ];
             $insert_data[]=$ot_data;
   }
   $insert_data = collect($insert_data); // Make a collection to use the chunk method
//    // it will chunk the dataset in smaller collections containing 500 values each.
//    // Play with the value to get best result
    $chunks = $insert_data->chunk(3000);
    foreach ($chunks as $chunk)
    {
        \DB::table('salary_ot')->insert($chunk->toArray());
    }
$data_ot=DB::SELECT("SELECT emp_id,out_time,exit_time,att_date,HOUR(out_time) as hour_out_time,HOUR(exit_time) as hour_exit_time,MINUTE(out_time) as minute_out_time,MINUTE(exit_time)as minute_exit_time
 from salary_ot WHERE out_time>exit_time AND emp_id='$id'");
    $data=0;
    $insert_data = [];
  function roundTime($time){
$hour=date('G',strtotime($time));
$minute=date('i',strtotime($time));
if($minute<25){
    $minute=0;
}
elseif ($minute>=25 &&$minute<55){
    $minute=.5;
}
else{
    $minute=0;
    $hour++;
}
$kind=number_format((float)($hour+$minute),1,'.','');
return $kind;
}
   foreach($data_ot as $extraots){ 
     $ot=roundTime(date('G:i',strtotime($extraots->out_time)-strtotime($extraots->exit_time)));
       if($ot>=$overtime_hours_count->overtime_hours){
       $total_ot=$overtime_hours_count->overtime_hours;
     }else{
         $total_ot=roundTime(date('G:i',strtotime($extraots->out_time)-strtotime($extraots->exit_time)));
     }
   $ot_data = [
    'emp_id' =>$extraots->emp_id,
    'emp_hour'=>$total_ot,
    'emp_minute'=>0,
    // 'att_date'=>$extraots->att_date,
];
$insert_data[] = $ot_data;
}
$insert_data = collect($insert_data); // Make a collection to use the chunk method
//    // it will chunk the dataset in smaller collections containing 500 values each.
//    // Play with the value to get best result
$chunks = $insert_data->chunk(5000);
foreach ($chunks as $chunk)
{
  \DB::table('extra_ot_result')->insert($chunk->toArray());
}
//end big data insert
$late=DB::table('employees')
->where('employees.is_three_shift',0)
->where('employees.id',$id)
->join('attendance','employees.id','=','attendance.emp_id')
->whereBetween('attendance.date',[$current_month_first,$current_month_last])
->where('attendance.in_time','>',DB::raw('attendance.max_entry'))
->select('employees.id as emp_id',DB::raw('count(employees.id) as late'))
->groupBy('employees.id')
->get();
$latee=0;
foreach ($late as $lates){
      $insert[]=[
          'emp_id'=>$lates->emp_id,
          'total_late'=>$lates->late,
          'month'=>$month,
      ];
}
if($latee%550==0){
  if(!empty($insert)){
      $insertData = DB::table('emp_total_late')->insert($insert);
      unset($insert);
  }
}
$latee++;
$extraotsss=DB::SELECT("SELECT extra_ot_result.emp_id,SUM(emp_hour) as total_hour from extra_ot_result
GROUP BY extra_ot_result.emp_id");
    $present = DB::SELECT("SELECT attendance.emp_id,empFirstName,count(date) as present,count(date)-(ifnull(total_weekend_present.weekend_present, 0)-ifnull(regular_weekend_presence.regular_present,0)) as actual_present,attendance.date,total_weekend_present.weekend_present,regular_weekend_presence.regular_present 
    from attendance LEFT JOIN employees ON attendance.emp_id=employees.id 
    LEFT JOIN regular_weekend_presence ON attendance.emp_id=regular_weekend_presence.emp_id
    LEFT JOIN total_weekend_present ON attendance.emp_id=total_weekend_present.emp_id
    WHERE attendance.emp_id='$id' AND attendance.in_time IS NOT NULL AND employees.is_three_shift=0 AND attendance.date BETWEEN '$current_month_first' AND '$current_month_last' GROUP BY attendance.emp_id");
    $att=DB::SELECT("SELECT employees.id as emp_id,tbattendance_bonus.bTitle,tbattendance_bonus.bAmount
    FROM employees
    LEFT JOIN tbattendance_bonus ON employees.empAttBonusId = tbattendance_bonus.id 
    WHERE employees.id='$id' AND employees.is_three_shift=0 AND employees.empAttBonusId!=0");
    $ott=0;
    foreach ($extraotsss as $or){
            $insert[]=[
                'emp_ids'=>$or->emp_id,
                'overtime_hour'=>$or->total_hour,
                'month'=>$month,
            ];
    }
    if($ott%550==0){
        if(!empty($insert)){
            $insertData = DB::table('employee_overtime')->insert($insert);
            unset($insert);
        }
    }
    $ott++;
   $total_emp_overtime_empls=DB::SELECT("SELECT employee_overtime.emp_ids as rmid,SUM(employee_overtime.overtime_hour) as total_overtime FROM employee_overtime GROUP BY employee_overtime.emp_ids");
   $temp_ott=0;
    foreach($total_emp_overtime_empls as $tmp_overtime){
        $insert[]=[
            'overtime_id' =>$tmp_overtime->rmid,
            'employee_overtime' =>$tmp_overtime->total_overtime,
            'month' =>$month
        ];
    }
    if($temp_ott%550==0){
        if(!empty($insert)){
            $insertData = DB::table('temp_employee_overtime')->insert($insert);
            unset($insert);
        }
    }
    $temp_ott++;
    $pre_insert=0;
    foreach ($present as $presents){
        $insert[]=[
            'emp_id'=>$presents->emp_id,
            'total_present'=>$presents->present,
            'total_absent'=>$this->dayCalculator($current_month_first,$current_month_last)-$presents->present,
            'month'=>$month,
        ];
    }
    if($pre_insert%550==0){
        if(!empty($insert)){
            $insertData = DB::table('tb_total_present')->insert($insert);
            unset($insert);
        }
    }
    $pre_insert++;
    $leave_tota=DB::SELECT("SELECT leave_of_employee.emp_id as employee_id,SUM(leave_of_employee.total) as total_leave 
                            FROM leave_of_employee
                            LEFT JOIN employees ON leave_of_employee.emp_id = employees.id
                            WHERE employees.id='$id' AND employees.is_three_shift=0
                            AND month='$month_check'
                            GROUP BY leave_of_employee.emp_id");                      
    $total_emp_leavesss=0;
    foreach ($leave_tota as $leaves){
        $insert[]=[
            'emp_idss'=>$leaves->employee_id,
            'total_leaves_taken'=>$leaves->total_leave,
            'month'=>$month,
        ];
    }
    if($total_emp_leavesss%550==0){
        if(!empty($insert)){
            $insertData = DB::table('total_employee_leave')->insert($insert);
            unset($insert);
        }
    }
    $total_emp_leavesss++;
    $checkattbonus=DB::table('attendance_bonus')->where('month',date('Y-m',strtotime($request->salary_sheet_month)))->count();
   if($checkattbonus>0){
        $att_bonus_incret=0;
        $deleteattbonus=DB::table('attendance_bonus')->where('month',date('Y-m',strtotime($request->salary_sheet_month)))->delete();
        foreach ($att as $atts){
            if($atts->bAmount==''){
                $bonus=0;
            }
            else{
                $bonus=$atts->bAmount;
            }
            $insert[]=[
                'bonus_id'=>$atts->emp_id,
                'bonus_amount'=>$bonus,
                'emp_total_amount'=>$bonus,
                'month'=>date('Y-m',strtotime($request->salary_sheet_month)),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
        }
        if($att_bonus_incret%550==0){
            if(!empty($insert)){
                $insertData = DB::table('attendance_bonus')->insert($insert);
                unset($insert);
            }
        }
        $att_bonus_incret++;
    }
    else{
        $att_bonus_incret_one=0;
        foreach ($att as $atts){
            if($atts->bAmount==''){
                $bonus=0;
            }
            else{
                $bonus=$atts->bAmount;
            }
            $insert[]=[
                'bonus_id'=>$atts->emp_id,
                'bonus_amount'=>$bonus,
                'emp_total_amount'=>$bonus,
                'month'=>date('Y-m',strtotime($request->salary_sheet_month)),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
        }
        if($att_bonus_incret_one%550==0){
            if(!empty($insert)){
                $insertData = DB::table('attendance_bonus')->insert($insert);
                unset($insert);
            }
        }
        $att_bonus_incret_one++;
    }
    $start = date('Y-m-01',strtotime($request->salary_sheet_month));
    $end = date('Y-m-t',strtotime($request->salary_sheet_month));
    $monthscheck=date('Y-m-d',strtotime($request->salary_sheet_month));
    $bonusmonth=date('Y-m',strtotime($request->salary_sheet_month));
    $festmonth=date('Y-m-01',strtotime($request->salary_sheet_month));
    $data=DB::table('payroll_salary')
        ->leftJoin('employees', function ($query) use($id) {
            $query->on('payroll_salary.emp_id', '=', 'employees.id');
            $query->where('employees.empAccStatus','=',1);
            $query->where('employees.id','=',$id);
        })
        ->leftJoin('payroll_grade', function ($query) {
            $query->on('payroll_salary.grade_id', '=', 'payroll_grade.id');
        })
        ->leftJoin('units', function ($query) {
            $query->on('employees.unit_id', '=', 'units.id');
        })
        ->leftJoin('floors', function ($query) {
            $query->on('employees.floor_id', '=', 'floors.id');
        })
        ->leftJoin('tblines', function ($query) {
            $query->on('employees.line_id', '=', 'tblines.id');
        })
        ->leftJoin('tb_advance_salary', function ($query) use($start,$id) {
            $query->on('payroll_salary.emp_id', '=', 'tb_advance_salary.emp_id');
            $query->where('tb_advance_salary.month','=',$start);
            $query->where('tb_advance_salary.emp_id','=',$id);
        })
        ->leftJoin('total_employee_leave', function ($query) use($monthscheck,$id) {
            $query->on('payroll_salary.emp_id', '=', 'total_employee_leave.emp_idss');
            $query->where('total_employee_leave.month','=',$monthscheck);
            $query->where('total_employee_leave.emp_idss','=',$id);
        })
        ->leftJoin('leave_of_employee', function ($query) use($monthscheck,$id) {
            $query->on('payroll_salary.emp_id', '=', 'leave_of_employee.emp_id');
            $query->where('leave_of_employee.month','=',$monthscheck);
            $query->where('leave_of_employee.emp_id','=',$id);
        })
        ->leftJoin('tb_leave_application', function ($query) use($monthscheck) {
            $query->on('payroll_salary.emp_id', '=', 'tb_leave_application.employee_id');
            $query->whereYear('tb_leave_application.leave_ending_date', '=',date('Y',strtotime($monthscheck)));
            $query->whereMonth('tb_leave_application.leave_ending_date', '=',date('m',strtotime($monthscheck)));
            $query->where('tb_leave_application.status','=',1);
            $query->where('tb_leave_application.leave_type_id','=',1);
            })
        ->leftJoin('tb_leave_type', function ($query) use($start,$end) {
            $query->on('leave_of_employee.leave_type_id', '=', 'tb_leave_type.id');
        })
        ->leftJoin('tb_total_present', function ($query) use($monthscheck,$id) {
            $query->on('payroll_salary.emp_id', '=', 'tb_total_present.emp_id');
            $query->where('tb_total_present.month','=',$monthscheck);
            $query->where('tb_total_present.emp_id','=',$id);
        })
        ->leftJoin('emp_total_lates', function ($query) use($monthscheck,$id) {
            $query->on('payroll_salary.emp_id', '=', 'emp_total_lates.emp_id');
            $query->where('emp_total_lates.month','=',$monthscheck);
            $query->where('emp_total_lates.emp_id','=',$id);
        })
        ->leftJoin('temp_employee_overtime', function ($query) use($monthscheck,$id) {
            $query->on('payroll_salary.emp_id', '=', 'temp_employee_overtime.overtime_id');
            $query->where('temp_employee_overtime.month','=',$monthscheck);
            $query->where('temp_employee_overtime.overtime_id','=',$id);
        })
        ->leftJoin('emp_manual_overtime', function ($query) use($bonusmonth,$id) {
            $query->on('payroll_salary.emp_id', '=', 'emp_manual_overtime.emp_overtime_id');
            $query->where('emp_manual_overtime.month','=',$bonusmonth);
            $query->where('emp_manual_overtime.emp_overtime_id','=',$id);
        })
        ->leftJoin('attendance_bonus', function ($query) use($bonusmonth,$id) {
            $query->on('payroll_salary.emp_id', '=', 'attendance_bonus.bonus_id');
            $query->where('attendance_bonus.month','=',$bonusmonth);
            $query->where('attendance_bonus.bonus_id','=',$id);
        })
        ->leftJoin('employees_bonus', function ($query) use($bonusmonth,$id) {
            $query->on('payroll_salary.emp_id', '=', 'employees_bonus.emp_id');
            $query->where('employees_bonus.date','=',$bonusmonth);
            $query->where('employees_bonus.emp_id','=',$id);
        })
        ->leftJoin('festival_bonus', function ($query) use($festmonth,$id) {
            $query->on('payroll_salary.emp_id', '=', 'festival_bonus.emp_id');
            $query->where('festival_bonus.month','=',$festmonth);
            $query->where('festival_bonus.emp_id','=',$id);
        })
        ->leftJoin('tb_deduction', function ($query) use($bonusmonth,$id) {
            $query->on('payroll_salary.emp_id', '=', 'tb_deduction.emp_id');
            $query->where('tb_deduction.month','=',$bonusmonth);
            $query->where('tb_deduction.emp_id','=',$id);
        })
        ->leftJoin('tb_loan_deduction', function ($query) use($bonusmonth,$id) {
            $query->on('payroll_salary.emp_id', '=', 'tb_loan_deduction.emp_id');
            $query->where('tb_loan_deduction.month','=',$bonusmonth);
            $query->where('tb_loan_deduction.emp_id','=',$id);
        })
        ->leftJoin('tb_production_bonus', function ($query) use($bonusmonth,$id) {
            $query->on('payroll_salary.emp_id', '=', 'tb_production_bonus.emp_id');
            $query->where('tb_production_bonus.month','=',$bonusmonth);
            $query->where('tb_production_bonus.emp_id','=',$id);
        })

        ->leftJoin('departments', function ($query) {
            $query->on('employees.empDepartmentId', '=', 'departments.id');
        })
        ->Join('designations', function ($query) {
            $query->on('designations.id', '=', 'employees.empDesignationId');
        })
        ->select('emp_total_lates.total_late','payroll_salary.*','employees.is_three_shift','employees.empDesignationId','tb_leave_application.leave_ending_date as maternity_ending','employees.payment_mode','employees.empDepartmentId','employees.empFirstName','employees.empLastName','employees.empJoiningDate','employees.empSection','employees.employeeId','payroll_grade.grade_name','designations.designation','employees.unit_id','units.name','employees.floor_id','floors.floor','employees.line_id','tblines.line_no','employees.empDepartmentId','departments.departmentName','employees.work_group','employees.payment_mode','total_employee_leave.total_leaves_taken','employees_bonus.emp_bonus as special_bonus','employees_bonus.emp_amount','attendance_bonus.bonus_amount as attendance_bonus','attendance_bonus.bonus_percent','festival_bonus.emp_amount as f_amount','tb_total_present.total_present','leave_of_employee.leave_type_id','leave_of_employee.total','tb_deduction.normal_deduction_amount','tb_loan_deduction.month_wise_deduction_amount','tb_production_bonus.production_amount','tb_production_bonus.production_percent','temp_employee_overtime.employee_overtime as temp_ot','emp_manual_overtime.employee_overtime as manual_ot','designations.gradeId','date_of_discontinuation','tb_advance_salary.advance_amount','leave_of_employee.start_date','leave_of_employee.end_date')
        ->selectraw("CONCAT(GROUP_CONCAT( DISTINCT(tb_leave_type.leave_type),leave_of_employee.total)) as leave_name_value")
        ->selectraw("GROUP_CONCAT(DISTINCT(leave_of_employee.leave_type_id)SEPARATOR ',') as leave_with_id")
        ->selectraw("GROUP_CONCAT(DISTINCT(leave_of_employee.total)SEPARATOR ',') as leave_with_total")
        ->selectraw("SUM(festival_bonus.emp_bonus) as f_bonus")
        ->where('employees.id',$id)
        ->where('employees.is_three_shift',0)
        ->groupBy('payroll_salary.emp_id')
        ->get();
        $month=date('Y-m-d',strtotime($request->salary_sheet_month));
        $dates=date('Y-m',strtotime($request->salary_sheet_month));
        $current_month_first=date('Y-m-01',strtotime($workingmonth));
        $current_month_last=date('Y-m-t',strtotime($workingmonth));
        $working_day=$this->dayCalculator($current_month_first,$current_month_last)+$regular_days_setup_check;
        $total_sal_datassss=0;
        foreach($data as $testdata){
            $regular_day_check=DB::table('tbweekend_regular_day')
            ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
            ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
            ->get();
            $leave_of_employee=DB::table('leave_of_employee')
            ->where('emp_id',$testdata->emp_id)
            ->where('month','=',$month_check)
            ->get();
            $leave_weekend=0;
            $leave_holiday=0;
            $dept_section_holiday_leave=0;
            $regular_day_count=0;
            $leave_regular_weekend=0;
            $dept_sec_holi_present=0;
            foreach ($leave_of_employee as $loe){
                $leave_weekend+=$this->weekdayCalculator($loe->start_date,$loe->end_date);
                $leave_holiday+=$this->holiday($loe->start_date,$loe->end_date);
                $leave_regular_weekend+=DB::table('tbweekend_regular_day')
                    ->whereYear('weekend_month',date('Y',strtotime($request->salary_sheet_month)))
                    ->whereMonth('weekend_month',date('m',strtotime($request->salary_sheet_month)))
                    ->whereBetween('weekend_date',[$loe->start_date,$loe->end_date])
                    ->count();
                $dept_section_holiday_leave+=DB::table('emp_wise_holiday')
                ->where('emp_id',$loe->emp_id)
                ->whereBetween('holiday_date',[$loe->start_date,$loe->end_date])
                ->count();
            }
            $dept_sec_holis=DB::table('emp_wise_holiday')
            ->where('emp_id',$testdata->emp_id)
            ->where('holiday_month',$s)
            ->get();
            foreach($dept_sec_holis as $tt){
                $dept_sec_holi_present+=DB::table('attendance')
                ->where('emp_id',$testdata->emp_id)
                ->where('date',$tt->holiday_date)
                ->count();
            }
            global $deduction;
            global  $ac_abs_day;
            global  $weekend;
            global  $holi;
            global  $total_payable_days;
            global $total_gross;
            global  $join_day;
            global $dis_continution_day;
            $att_bonus=0;
            $status=0;
            $total_gross_pre=0;
            $current_month_day=date('t',strtotime($workingmonth));
            $overtime_rate=$testdata->basic_salary/104;
            $actual_overtime_rate=$overtime_rate;
            $total_overtt_time=$testdata->temp_ot+$testdata->manual_ot;
            $overtime_amount=$actual_overtime_rate*$total_overtt_time;
         //default calculation
            $dept_section_total_holiday=DB::table('emp_wise_holiday')
            ->where('emp_id',$testdata->emp_id)
            ->where('holiday_month',$s)
            ->count()-$dept_section_holiday_leave;   
           $weekss=$this->weekdayCalculator($s,$e)-$regular_days_setup_check-$leave_weekend+$leave_regular_weekend;
           $holiii=$this->holiday($s,$e)-$leave_holiday+$dept_section_total_holiday;
           $actual_working_day=date('t',strtotime($request->salary_sheet_month))-$weekss-$holiii;
           $absentday=$actual_working_day-$testdata->total_leaves_taken-$testdata->total_present;
           $ac_abs_day=max($absentday,0);
           $weekend = $weekss;
           $holi = $holiii;
           $month_lst_day=date('t',strtotime($month));
           $total_payable_days=$month_lst_day-$ac_abs_day;
           $deduction=round($testdata->basic_salary/30*$ac_abs_day,2);
        //default calculation
        //check date of discontinution
        if(date('Y-m',strtotime($testdata->empJoiningDate))==$bonusmonth && date('Y-m',strtotime($testdata->date_of_discontinuation))==$bonusmonth){
            $dept_sec_holis=DB::table('emp_wise_holiday')
            ->where('emp_id',$testdata->emp_id)
            ->whereBetween('holiday_date',[$testdata->empJoiningDate, $testdata->date_of_discontinuation])
            ->get();
            foreach($dept_sec_holis as $tt){
                $dept_sec_holi_present+=DB::table('attendance')
                ->where('emp_id',$testdata->emp_id)
                ->where('date',$tt->holiday_date)
                ->count();
            }
            $dept_section_total_holiday=DB::table('emp_wise_holiday')
            ->where('emp_id',$testdata->emp_id)
            ->whereBetween('holiday_date',[$testdata->empJoiningDate, $testdata->date_of_discontinuation])
            ->count()-$dept_section_holiday_leave;
            $weekss = $this->weekdayCalculator($testdata->empJoiningDate,$testdata->date_of_discontinuation)-$regular_days_setup_check+$leave_regular_weekend-$leave_weekend;
            $holiii = $this->holiday($testdata->empJoiningDate,$testdata->date_of_discontinuation)-$leave_holiday+$dept_section_total_holiday;
            $p_date1 = date_create(date('Y-m-01', strtotime($request->salary_sheet_month)));
            $p_date2 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
            $previous_diff = date_diff($p_date1, $p_date2);
            $total_previous_difference = $previous_diff->format("%a");
            $p_date1 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
            $p_date2 = date_create(date('Y-m-t', strtotime($request->salary_sheet_month)));
            $previous_diff2 = date_diff($p_date1, $p_date2);
            $total_previous_difference2 = $previous_diff2->format("%a");
            $total_previous_difference+=$total_previous_difference2;
            $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
            $date1 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
            $date2 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
            $diff = date_diff($date1, $date2);
            $total_difference = $diff->format("%a") + 1;
            $actual_working = $total_difference - $weekss - $holiii;
            $working_days = $actual_working;
//                    return $working_days;
            $absentdays = $working_days - $testdata->total_leaves_taken - $testdata->total_present;
            $ac_abs_day = max($absentdays, 0);
            $weekend = $weekss;
            $holi = $holiii;
            $month_lst_day = date('t', strtotime($month));
            $total_payable_days = $month_lst_day - $ac_abs_day;
            $month_salary = date('01', strtotime($request->salary_sheet_month));
            $join_date = $testdata->empJoiningDate;
            $join_day = date('d', strtotime($testdata->empJoiningDate));
            if (date('d', strtotime($join_date)) >= $month_salary) {
                $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
            }
            else{
                $deduction=0;
            }
        }
         //check date of discontinution
            else {
                //check if join date this month
                if (date('Y-m', strtotime($testdata->empJoiningDate)) == $bonusmonth) {
                    $dept_sec_holis=DB::table('emp_wise_holiday')
                    ->where('emp_id',$testdata->emp_id)
                    ->whereBetween('holiday_date',[$testdata->empJoiningDate,$e])
                    ->get();
                    foreach($dept_sec_holis as $tt){
                        $dept_sec_holi_present+=DB::table('attendance')
                        ->where('emp_id',$testdata->emp_id)
                        ->where('date',$tt->holiday_date)
                        ->count();
                    }
                    $dept_section_total_holiday=DB::table('emp_wise_holiday')
                    ->where('emp_id',$testdata->emp_id)
                    ->whereBetween('holiday_date',[$testdata->empJoiningDate,$e])
                    ->count()-$dept_section_holiday_leave;
                    $tot_week_regular=DB::table('tbweekend_regular_day')
                    ->whereBetween('weekend_date', array($testdata->empJoiningDate, $e))
                    ->count();
                    $weekss = $this->weekdayCalculator($testdata->empJoiningDate,$e)-$tot_week_regular+$leave_regular_weekend-$leave_weekend;
                    $holiii = $this->holiday($testdata->empJoiningDate, $e)-$leave_holiday+$dept_section_total_holiday;
                    $p_date1 = date_create(date('Y-m-01', strtotime($request->salary_sheet_month)));
                    $p_date2 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
                    $previous_diff = date_diff($p_date1, $p_date2);
                    $total_previous_difference = $previous_diff->format("%a");
                    $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
                    //  dd($testdata->current_month_day);
                    $date1 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
                    $date2 = date_create(date('Y-m-t', strtotime($request->salary_sheet_month)));
                    $diff = date_diff($date1, $date2);
                    $total_difference = $diff->format("%a") + 1;
                    $actual_working = $total_difference - $weekss - $holiii;
                    $working_days = $actual_working;
                    $absentdays = $working_days - $testdata->total_leaves_taken - $testdata->total_present;
                    $ac_abs_day = max($absentdays, 0);
                    $weekend = $weekss;
                    $holi = $holiii;
                    $month_lst_day = date('t', strtotime($month));
                    $total_payable_days = $month_lst_day - $ac_abs_day;
                    $month_salary = date('01', strtotime($request->salary_sheet_month));
                    $join_date = $testdata->empJoiningDate;
                    $join_day = date('d', strtotime($testdata->empJoiningDate));
                    if (date('d', strtotime($join_date)) >= $month_salary) {
                        $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
                    }
                }
                //check if join date this month
                //check if date of discontinuation this month
                if (date('Y-m', strtotime($testdata->date_of_discontinuation)) == $bonusmonth) {
                    $dis_continution_day = date('d', strtotime($testdata->date_of_discontinuation));
                    if (date('d', strtotime($testdata->date_of_discontinuation)) >= 01 && date('d', strtotime($testdata->date_of_discontinuation)) <= 31) {
                        $dept_sec_holis=DB::table('emp_wise_holiday')
                    ->where('emp_id',$testdata->emp_id)
                    ->whereBetween('holiday_date',[$s,$testdata->date_of_discontinuation])
                    ->get();
                    foreach($dept_sec_holis as $tt){
                        $dept_sec_holi_present+=DB::table('attendance')
                        ->where('emp_id',$testdata->emp_id)
                        ->where('date',$tt->holiday_date)
                        ->count();
                    }
                        $dept_section_total_holiday=DB::table('emp_wise_holiday')
                        ->where('emp_id',$testdata->emp_id)
                        ->whereBetween('holiday_date',[$s,$testdata->date_of_discontinuation])
                        ->count()-$dept_section_holiday_leave;
                        $month_start = date('Y-m-01', strtotime($request->salary_sheet_month));
                        $weekss = $this->weekdayCalculator($month_start,$testdata->date_of_discontinuation)-$regular_days_setup_check+$leave_regular_weekend-$leave_weekend;
                        $holiii = $this->holiday($month_start, $testdata->date_of_discontinuation)-$leave_holiday+$dept_section_total_holiday;
                        $weekend = $weekss;
                        $holi = $holiii;
                        $p_date1 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
                        $p_date2 = date_create(date('Y-m-t', strtotime($request->salary_sheet_month)));
                        $previous_diff = date_diff($p_date1, $p_date2);
                        $total_previous_difference = $previous_diff->format("%a");
                        $actual_working_day = date('d', strtotime($testdata->date_of_discontinuation)) - $weekend - $holi;
                        $absentday = $actual_working_day - $testdata->total_leaves_taken - $testdata->total_present;
                        $ac_abs_day = max($absentday, 0);
                        $month_lst_day = date('t', strtotime($month));
                        $total_payable_days = $month_lst_day - $ac_abs_day;
                        $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
                        $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
                    }
                }
                //check if date of discontinuation this month
            }
                //check if maternity leave this month
                  if(date('Y-m',strtotime($testdata->start_date))==$bonusmonth && $testdata->leave_with_id==1){
                    $dept_sec_holis=DB::table('emp_wise_holiday')
                    ->where('emp_id',$testdata->emp_id)
                    ->whereBetween('holiday_date',[$s,$e])
                    ->get();
                    foreach($dept_sec_holis as $tt){
                        $dept_sec_holi_present+=DB::table('attendance')
                        ->where('emp_id',$testdata->emp_id)
                        ->where('date',$tt->holiday_date)
                        ->count();
                    }
                    $dept_section_total_holiday=DB::table('emp_wise_holiday')
                        ->where('emp_id',$testdata->emp_id)
                        ->whereBetween('holiday_date',[$s,$e])
                        ->count()-$dept_section_holiday_leave;  
                    $month_start=date('Y-m-01',strtotime($request->salary_sheet_month));
                    $month_end=date('Y-m-t',strtotime($request->salary_sheet_month));
                    $weekss=$this->weekdayCalculator($s,$e)-$regular_days_setup_check-$leave_weekend;
                    $holiii=$this->holiday($s,$e)-$leave_holiday+$dept_section_total_holiday;
                    $weekend=$weekss;
                    $holi=$holiii;
                    $actual_working_day=date('t',strtotime($request->salary_sheet_month))- $weekss- $holiii;
                    $absentday=$actual_working_day-$testdata->total_leaves_taken-$testdata->total_present;
                    $ac_abs_day=max($absentday,0);
                    $total_gross_pre=($testdata->total_employee_salary/$current_month_day)*$testdata->total;
                    $deduction=round($testdata->basic_salary/30*$ac_abs_day,2);
                    }
               //check if maternity leabve this month
            //check if date of maternity ending this month
            // if($testdata->maternity_ending){
            //     $m_day=date('d',strtotime($testdata->maternity_ending));
            //     if($m_day>1){
            //         $month_start=date('Y-m-01',strtotime($request->salary_sheet_month));
            //         $month_end=date('Y-m-t',strtotime($request->salary_sheet_month));
            //         $weekss=$this->weekdayCalculator($month_start,$month_end)-$regular_days_setup_check-$leave_weekend;
            //         $holiii=$this->holiday($month_start,$month_end)-$leave_holiday;
            //      $actual_working=date('t',strtotime($request->salary_sheet_month))-$weekss-$holiii;
            //      $working_days=$actual_working;
            //      $absentdays=$working_days-$testdata->total_leaves_taken-$testdata->total_present;
            //      $ac_abs_day=max($absentdays,0);
            //      $weekend = $weekss;
            //      $holi = $holiii;
            //      $month_lst_day=date('t',strtotime($month));
            //      $total_payable_days=$month_lst_day-$ac_abs_day;
            //      $total_gross_pre=($testdata->total_employee_salary/$current_month_day)*$testdata->total;
            //      $deduction=round($testdata->basic_salary/30*$ac_abs_day,2);
            //     }
            //  }
           //check if date of maternity ending this month

             if($testdata->leave_with_id==1 && $testdata->total_present==''){
                $status=1;
              }
                if($testdata->payment_mode=='Bank' || $testdata->payment_mode=='bKash') {
                    $stampFee = 0;
                }
                else {
                    $stampFee = 10;
                }
                  //check att bonus condition wise
                if($testdata->total=='' && $ac_abs_day==0 && $testdata->total_late<3 && $testdata->empJoiningDate<=$current_month_first){
                    $att_bonus=$testdata->attendance_bonus;
                    if($testdata->date_of_discontinuation!=null && $testdata->date_of_discontinuation<$current_month_last){
                        $att_bonus=0;
                    }
                }else{
                    $att_bonus=0;
                   }
                 //check att bonus condition
               $gross_pay=$testdata->total_employee_salary-$deduction-$testdata->normal_deduction_amount-$testdata->month_wise_deduction_amount-$stampFee;
               $gross_pay=$gross_pay-$total_gross_pre;
               $month=date('Y-m-d',strtotime($request->salary_sheet_month));
               $net_wages=$gross_pay+$overtime_amount+$testdata->production_amount+$testdata->production_percent+$att_bonus+$testdata->advance_amount;
               $actual_net_wages=$net_wages;
               if($actual_net_wages<=1000){
                $gross_pay=$gross_pay+10;
                $actual_net_wages=$actual_net_wages+10;
               }
               $insert[]=[
                   'emp_id' => $testdata->emp_id,
                   'grade_id' => $testdata->gradeId,
                   'department_id' => $testdata->empDepartmentId,
                   'section_id' => $testdata->empSection,
                   'designation_id' => $testdata->empDesignationId,
                   'basic_salary' => $testdata->basic_salary,
                   'house_rant' => $testdata->house_rant,
                   'medical' => $testdata->medical,
                   'transport' => $testdata->transport,
                   'food' => $testdata->food,
                   'other' => $testdata->other,
                   'gross' => $testdata->total_employee_salary,
                   'gross_pay' => $gross_pay,
                   'present' => $testdata->total_present,
                   'absent' => $ac_abs_day,
                   'absent_deduction_amount' => $deduction,
                   'attendance_bonus' =>  $att_bonus,
                   'attendance_bonus_percent' => $testdata->bonus_percent,
                   'festival_bonus_amount' => $testdata->f_amount,
                   'festival_bonus_percent' => $testdata->f_bonus,
                   'increment_bonus_amount' => $testdata->emp_amount,
                   'increment_bonus_percent' => $testdata->special_bonus,
                   'normal_deduction' => $testdata->normal_deduction_amount,
                   'advanced_deduction' => $testdata->month_wise_deduction_amount,
                   'production_bonus' => $testdata->production_amount,
                   'production_percent' => $testdata->production_percent,
                   'working_day' =>$working_day,
                   'weekend' =>$weekend,
                   'holiday' =>$holi,
                   'total_payable_days' =>$total_payable_days,
                   'overtime' =>$total_overtt_time,
                   'overtime_rate' =>$actual_overtime_rate,
                   'overtime_amount' =>$overtime_amount,
                   'leave' =>$testdata->leave_with_id,
                   'total' =>$testdata->total_leaves_taken,
                   'total_late' =>$testdata->total_late,
                   'advance_salary'=>$testdata->advance_amount,
                   'net_amount' =>$actual_net_wages,
                   'month' => $month,
                   'dates' => $dates,
                   'status' =>  $status,
                   'created_at' =>Carbon::now()->toDateTimeString(),
                   'updated_at' =>Carbon::now()->toDateTimeString(),
               ];
           }
        if($total_sal_datassss%550==0){
            if(!empty($insert)){
                $insertData = DB::table('tb_salary_history')->insert($insert);
                unset($insert);
            }
        }
        $total_sal_datassss++;
    $insert_check_data=DB::table('tb_salary_process_check')->insert([
        'month' =>$month,
        'status' =>0,
        'process_id' =>auth()->user()->id,
        'created_at' =>Carbon::now()->toDateTimeString(),
        'updated_at' =>Carbon::now()->toDateTimeString(),
    ]);
    \Illuminate\Support\Facades\DB::table('tbactivity_logs_history')->insert([
        'acType'=>'Manual Attendance',
        'details'=>"Salary has been processed for ".Carbon::parse($request->salary_sheet_month)->format('M Y'),
        'createdBy'=>Auth::user()->id,
        'created_at'=>Carbon::now()->toDateTimeString(),
        'updated_at'=>Carbon::now()->toDateTimeString(),
    ]);
    DB::table('total_employee_leave')->delete();
    DB::table('extra_ot_result')->delete();
    DB::table('tb_total_present')->delete();
    DB::table('employee_overtime')->delete();
    DB::table('temp_employee_overtime')->delete();
    DB::table('salary_ot')->delete();
    DB::table('emp_total_late')->delete();
    DB::table('emp_total_lates')->delete();
    DB::table('regular_weekend_presence')->delete();
    DB::table('total_weekend_present')->delete();
    //Start For In Active Employee
    DB::table('employees')
    ->where('id',$id)
    ->where('date_of_discontinuation','>=',$s)
    ->update([
        'empAccStatus'=>0,
    ]);
//        return $as;
    //End For In Active Employee
    Session::flash('msg','Process Completed');
    return redirect()->back();
}

}