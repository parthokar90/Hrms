<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class EmployeeTrainingController extends Controller
{
    function __construct()
    {
        $this->middleware(['middleware'=>'check-permission:admin|hr|hr-admin']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'training_name'=>'required',
            'training_start'=>'required',
            'training_end'=>'required',
            'training_institution'=>'required',
        ]);

        if($file=$request->file('training_attachment')){
            if($request->file('training_attachment')->getClientSize()>5000000)
            {
                Session::flash('fileSize','Could Not Upload. File Size Limit Exceeded.');
                return redirect("/employee/".$request->emp_id);

            }
            $name=time()."_".$request->emp_id."_"."Training_".$file->getClientOriginalName();
            $file->move('Training_Attachments',$name);


        }
        else{
            $name=null;
        }
        $now=Carbon::now()->toDateTimeString();
        $store=DB::table('employee_training_history')->insert([
            'emp_id'=>$request->emp_id,
            'training_name'=>$request->training_name,
            'training_start'=>Carbon::parse($request->training_start)->toDateString(),
            'training_end'=>Carbon::parse($request->training_end)->toDateString(),
            'training_description'=>$request->training_description,
            'training_institution'=>$request->training_institution,
            'training_attachment'=>$name,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($store){
            Session::flash('message','Training Information Added Successfully!');
        }

        return redirect("/employee/".$request->emp_id);
    }

    public function edit($id)
    {
        $data=DB::table('employee_training_history')->where('id','=',"$id")->first();
        return view('employee.modal.training.editForm',compact('data'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'training_name'=>'required',
            'training_start'=>'required',
            'training_end'=>'required',
            'training_institution'=>'required',
        ]);

        $table=DB::table('employee_training_history')->where(['id'=>$id]);

        if($file=$request->file('training_attachment'))
        {
            if($file->getClientSize()<5000000) {
                $name=time()."_".$request->emp_id."_"."Training_".$file->getClientOriginalName();
                $file->move('Employee_Nominee_Attachments', $name);
                if($table->first()->training_attachment) {
                    $path = public_path() . "/Employee_Nominee_Attachments/" . $table->first()->training_attachment;
                    if (file_exists($path)) {
//                    return $path;
                        unlink($path);
                    }
                }
            }
            else{
                Session::flash('fileSize','File size limit exceeded');
                return redirect('employee/'.session('emp_id'));
            }
        }
        else{
            $name=$table->first()->training_attachment;

        }
        $now=Carbon::now()->toDateTimeString();
        $store=DB::table('employee_training_history')->where('id','=',$id)->update([
//            'emp_id'=>$request->emp_id,
            'training_name'=>$request->training_name,
            'training_start'=>Carbon::parse($request->training_start)->toDateString(),
            'training_end'=>Carbon::parse($request->training_end)->toDateString(),
            'training_description'=>$request->training_description,
            'training_institution'=>$request->training_institution,
            'training_attachment'=>$name,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);
        if($store){
            Session::flash('message',"Training Information Updated");
        }

        return redirect('employee/'.session('emp_id'));
    }


    public function showDelete($id){
        $data=DB::table('employee_training_history')->where(['id'=>$id])->first();
        return view('employee.modal.training.deleteForm',compact('data'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dlt=DB::table('employee_training_history')->where(['id'=>$id]);
        if($dlt->first()->training_attachment) {
            $name = public_path() . "/Training_Attachments" . "/" . $dlt->first()->training_attachment;
            if (file_exists($name)) {
                unlink($name);
            }
        }

        $dlt=$dlt->delete();
        if($dlt)
        {
            Session::flash('delete', 'Educational Info Successfully Deleted');
            return redirect("/employee/".session('emp_id'));
        }
    }
}
