<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $booking_items=DB::table('tbbooking_items')
        ->leftJoin('users','users.id','=','tbbooking_items.createdBy')
        ->select('tbbooking_items.*','users.name as created_by')
        ->get();
        return view('booking.bookingitems',compact('booking_items'));
    } 

    public function bookinglist()
    {
        $date=date('Y-m-d');
        $booking_history=DB::table('tbbooking_history')
        ->leftJoin('tbbooking_items','tbbooking_items.id','=','tbbooking_history.biId')
        ->select('tbbooking_history.*','tbbooking_items.biName')
        ->whereDate('biDate', '>=', $date)
        ->get();
        return view('booking.index',compact('booking_history'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'biName'=>'required',
        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $des=DB::table('tbbooking_items')->insert([
            'biName'=>$request->biName,
            'biDescription'=>$request->biDescription,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);
        if($des){
            Session::flash('message','Item inserted Successfully');
            return  redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'biName'=>'required',
        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $st=DB::table('tbbooking_items')->where(['id'=>$id])->update([
            'biName'=>$request->biName,
            'biDescription'=>$request->biDescription,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);

        if($st){
            Session::flash('message','Item Information Successfully Updated .');
            return  redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check=DB::table('tbbooking_history')->where(['biId'=>$id])->get();
        if(count($check)){
            Session::flash('edit',"Can't delete. There are already some data bearing this resource." );
        }
        else {
            DB::table('tbbooking_items')->where(['id'=>$id])->delete();
            Session::flash('message', "Successfully has been deleted.");
        }
        return redirect()->back();
    }    

    public function new_request()
    {
        $booking_items=DB::table('tbbooking_items')->get();
        return view('booking.new_booking_request',compact('booking_items'));
    }

    public function bookingitems()
    {
        $booking_items=DB::table('tbbooking_items')->get();
        return view('booking.bookingitems',compact('booking_items'));
    }

    public function save_new_request(Request $request)
    {
        $this->validate($request,[
            'biId'=>'required',
            'bookedBy'=>'required',
            'biDate'=>'required',
        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tbbooking_history')->where([['biId','=',$request->biId],['status','!=',1],['biDate','=',Carbon::parse($request->biDate)->format('Y-m-d')]])->get();
        $save=0;

        foreach ($str as $key) {
            $time1=strtotime($key->biStartTime); 
            $time2=strtotime($key->biEndTime); 
            $currenttime=strtotime(date($request->biStartTime)); 
            if($time1 - $currenttime <= 0 && $currenttime - $time2 <= 0 ) { 
                $save++;
            }
        }

        if($save==0){
            $des=DB::table('tbbooking_history')->insert([
                'biId'=>$request->biId,
                'bookedBy'=>$request->bookedBy,
                'purpose'=>$request->purpose,
                'biDate'=>Carbon::parse($request->biDate)->format('Y-m-d'),
                'biStartTime'=>$request->biStartTime,
                'biEndTime'=>$request->biEndTime,
                'status'=>0,
                'createdBy'=>$user->id,
                'created_at'=>$now,
                'updated_at'=>$now,

            ]);
             Session::flash('message','Booking Information Successfully Added. ');
            }else{
                Session::flash('delete','This Item already booked for selected time. Choose another items/room or choose different time.');
            }
        return redirect()->back();
    }


    public function booking_cancel(Request $request)
    {

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tbbooking_history')->where(['id'=>$request->id])->update([
            'status'=>1,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','Booking Information Successfully Cancelled. ');
        }else{
            Session::flash('failedMessage','Booking Information Cancellation Failed.');
        }
        return redirect()->back();
    }


    public function booking_report_view()
    {
        $booking_items=DB::table('tbbooking_items')->get();
        return view('booking.booking_report_view',compact('booking_items'));
    }

    public function booking_report_data(Request $request)
    {
        $start_date=Carbon::parse($request->start_date)->format('Y-m-d');
        $end_date=Carbon::parse($request->end_date)->format('Y-m-d');
       if(($request->biId)==0){
            $booking_history=DB::table('tbbooking_history')
            ->leftJoin('tbbooking_items','tbbooking_items.id','=','tbbooking_history.biId')
            ->select('tbbooking_history.*','tbbooking_items.biName as biName')
            ->whereBetween('tbbooking_history.biDate', [$start_date, $end_date])
            ->get();
       }else{
            $booking_history=DB::table('tbbooking_history')
            ->leftJoin('tbbooking_items','tbbooking_items.id','=','tbbooking_history.biId')
            ->select('tbbooking_history.*','tbbooking_items.biName as biName')
            ->whereBetween('tbbooking_history.biDate', [$start_date, $end_date])
            ->where('tbbooking_history.biId','=',$request->biId)
            ->get();
       }
        return view('booking.booking_report_data',compact('booking_history','request'));
    }


}
