<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OtherAttachmentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'empAttachmentTitle'=>'required',
            'empAttachmentDescription'=>'required',
        ]);
        if($file=$request->file('empAttachment'))
        {

            if($file->getClientSize()<5000000) {
                $name = time() . "_" . session('emp_id') . "_" . $file->getClientOriginalName();
                $file->move('Employee_Attachments', $name);
            }
            else{
                return redirect("employee/".session('emp_id'));
            }

        }
        else{
            $name=null;

        }
        $message=DB::table('employee_attachments')->insert([
            'emp_id'=>session('emp_id'),
            'empAttachmentTitle'=>$request->empAttachmentTitle,
            'empAttachmentDescription'=>$request->empAttachmentDescription,
            'empAttachment'=>$name,
            'created_by'=>Auth::user()->name,
            'modified_by'=>Auth::user()->name,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        if($message){
            Session::flash('message',"Attachment Added.");
        }

        return redirect('employee/'.session('emp_id'));
        //return $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=DB::table('employee_attachments')->where(['id'=>$id])->first();
        return view('employee.modal.Other Attachment.deleteForm',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $otherAttachment=DB::table('employee_attachments')->where(['id'=>$id])->first();
        return view('employee.modal.Other Attachment.editForm',compact('otherAttachment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($file=$request->file('empAttachment')){
            $path=public_path()."/Employee_Attachments/".DB::table('employee_attachments')->where(['id'=>$id])->first()->empAttachment;
//                return $path;
            if(file_exists($path)){
                unlink($path);

            }
            $name=time()."_".session('emp_id')."_".$file->getClientOriginalName();
            $file->move('Employee_Attachments', $name);
            $data=DB::table('employee_attachments')->where(['id'=>$id])->update([
                'empAttachmentTitle'=>$request->empAttachmentTitle,
                'empAttachmentDescription'=>$request->empAttachmentDescription,
                'empAttachment'=>$name,
                'updated_at'=>Carbon::now()->toDateTimeString(),
                'modified_by'=>Auth::user()->name,
            ]);

        }
        else{
            $data=DB::table('employee_attachments')->where(['id'=>$id])->update([
                'empAttachmentTitle'=>$request->empAttachmentTitle,
                'empAttachmentDescription'=>$request->empAttachmentDescription,
                'updated_at'=>Carbon::now()->toDateTimeString(),
                'modified_by'=>Auth::user()->name,

            ]);

        }

        if($data){
//            Session::f
            Session::flash('edit',"Attachment information successfully updated");
        }
        return redirect('employee/'.session('emp_id'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $path=public_path()."/Employee_Attachments/".DB::table('employee_attachments')->where(['id'=>$id])->first()->empAttachment;
//                return $path;
        if(file_exists($path)){
            unlink($path);

        }
        $dlt=DB::table('employee_attachments')->where(['id'=>$id])->delete();
        if($dlt){
            Session::flash('delete',"Employee Attachment Successfully Deleted");
        }
        return redirect('employee/'.session('emp_id'));

    }
}
