<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Auth;
use redirect;
use Session;
use stdClass;
use PDF;
use Excel;
use mPDF;
use DateTime;
use DatePeriod;
use DateInterval;

class EmployeeThreeShiftController extends Controller
{
    //holiday find function
    function holiday($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $interval1=1+round(abs($date1-$date2)/86400);
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $dcount=0;
        foreach($festivalLeave as $fleave){
            if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
            }
            else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
            }
            else{
                continue;
            }
        }
        return $dcount;
    }

    //Weekend Dates My Code
    function weekend_dates($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
        $weekend_dates=array();
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $weekend_dates[]=$dates;
                }
            }
        }
        return $weekend_dates;
    }
    //Weekend Dates End My Code


    //3-shift employee salary process
    public function shift_employee_SalarySheet(Request $request){
        //delete some table initially start
        DB::table('extra_ot_result')->truncate();
        DB::table('tb_total_present')->truncate();
        DB::table('emp_shift_start_end')->truncate();
        DB::table('shift_emp_late')->truncate();
        DB::table('total_employee_leave')->truncate();
        DB::table('tb_total_present')->truncate();
        DB::table('employee_overtime')->truncate();
        DB::table('temp_employee_overtime')->truncate();
        DB::table('emp_total_late')->truncate(); 
        DB::table('emp_shift_leave')->truncate();   
        DB::table('emp_shift_holiday')->truncate();    
        DB::table('emp_shift_status')->truncate();    
        //delete some table initially end 

        //form request year month day start 
        $s=date('Y-m-01',strtotime($request->salary_sheet_month));
        $e=date('Y-m-t',strtotime($request->salary_sheet_month));
        $year=date('Y',strtotime($request->salary_sheet_month));
        $month=date('m',strtotime($request->salary_sheet_month));
        $month_day=date('Y-m-d',strtotime($request->salary_sheet_month));
        $year_month=date('Y-m',strtotime($request->salary_sheet_month));
       //form request year month day end 

       //check process status 1 or not if 1 then salary process would not be continue
        $process_check=DB::table('tb_salary_process_check')
        ->where('month',$s)
        ->where('status','=',1)
        ->count();
        if($process_check>0){
          Session::flash('salaryprocesscheck','Process Salary Already Completed this month');
          return redirect()->back();
        }
       //check process status 1 or not if 1 then salary process would not be continue

       //check 3-shift table that data already exists this month then delete
        $count=DB::table('tb_salary_history_shift')->where('month',$request->salary_sheet_month)->count();
        if($count>0){
          DB::table('tb_salary_history_shift')->where('month',$request->salary_sheet_month)->truncate();
        }
       //check 3-shift table that data already exists this month then delete


        //check leave between attendance then delete attendance 
		$delete=DB::SELECT("SELECT attendance.id as att_id 
        FROM leave_of_employee 
        LEFT JOIN attendance ON leave_of_employee.emp_id=attendance.emp_id 
        LEFT JOIN employees ON leave_of_employee.emp_id=employees.id 
        WHERE attendance.date BETWEEN leave_of_employee.start_date
        AND leave_of_employee.end_date AND YEAR(leave_of_employee.month)='$year' 
        AND MONTH(leave_of_employee.month)='$month' AND employees.is_three_shift=1"); 		
        $delete_id = [];
        foreach ($delete as $key => $value) {
            $delete_id[]=$value->att_id;
        } 
        $ok = DB::table('attendance')->whereIn('id', $delete_id)->delete();
        //check leave between attendance then delete attendance

        //holiday and leave insert start   
        $em_total_leave=DB::table('leave_of_employee')
        ->leftjoin('employees','leave_of_employee.emp_id','=','employees.id')
        ->where('employees.is_three_shift',1)
        ->where('leave_of_employee.month',$s)
        ->select('leave_of_employee.emp_id','leave_of_employee.leave_type_id','start_date','end_date')
        ->get(); 
        $em_tot_leave=[]; 
        foreach($em_total_leave as $l){
            for($i=$l->start_date;$i<=$l->end_date;$i++){
                $data=[
                    'emp_id'=>$l->emp_id,
                    'leave_id'=>$l->leave_type_id,
                    'leave_date'=>$i,
                    'total'=>1
                ];
                $em_tot_leave[]=$data;
            }
        }
        
        $em_tot_leave = collect($em_tot_leave);
        $em_tot_leave = $em_tot_leave->chunk(500);
        foreach ($em_tot_leave as $em_tot_leaves)
        {
            \DB::table('emp_shift_leave')->insert( $em_tot_leaves->toArray());
        }
        $em_total_festival=DB::table('tb_festival_leave')
        ->whereYear('fest_starts',$year)
        ->whereMonth('fest_starts',$month)
        ->get();
        $tot_holiday=[];
        foreach($em_total_festival as $fl){
            for($i=$fl->fest_starts;$i<=$fl->fest_ends;$i++){

            $data=[
                'holiday_date'=>$i,
                'total'=>1
            ];     
            $tot_holiday[]=$data;
            }
        }
        $tot_holiday = collect($tot_holiday);
        $tot_holiday =  $tot_holiday->chunk(500);
        foreach ($tot_holiday as  $tot_holidays)
        {
            \DB::table('emp_shift_holiday')->insert($tot_holidays->toArray());
        }
        //holiday and leave insert end

        //Start For In Active Employee then status change to 1
         DB::table('employees')->where('is_three_shift',1)->where('date_of_discontinuation','>=',$s)->update([
            'empAccStatus'=>1,
         ]);
        //End For In Active Employee then status change to 1


       //check employee shift found or not
        $check_shift=DB::table('three_shift_employee')
            ->whereYear('shift_date',$year)
            ->whereMonth('shift_date',$month)
            ->join('employees','three_shift_employee.emp_id','=','employees.id')
            ->where('employees.is_three_shift',1)
            ->count();
         if($check_shift<=0){
            Session::flash('salaryprocesscheck','Please Assign Employee Shift');
            return redirect()->back();
          } 
       //check employee shift found or not

       //check join or discontinuation or no condition match wise shift start and shift end
       $check_shift_emp=DB::table('three_shift_employee')
       ->whereYear('shift_date',$year)
       ->whereMonth('shift_date',$month)
       ->join('employees','three_shift_employee.emp_id','=','employees.id')
       ->where('employees.is_three_shift',1)
       ->select('employees.id as emp_id','empJoiningDate','date_of_discontinuation')
       ->groupBy('employees.id')
       ->get();
        $shift_s_e=[];
        foreach($check_shift_emp as $emp){
          $join_date=date('Y-m',strtotime($emp->empJoiningDate));
          $discontinution_date=date('Y-m',strtotime($emp->date_of_discontinuation));
          if($join_date==$year_month){
            $id=$emp->emp_id;
            $joining=date('Y-m-d',strtotime($emp->empJoiningDate));  
            $shift_employee=DB::SELECT("SELECT emp_id,MIN(shift_date) as starts_date,
            MAX(shift_date) as end_date
            FROM three_shift_employee
            WHERE shift_date BETWEEN '$joining' AND '$e'
            AND three_shift_employee.emp_id='$emp->emp_id'
            GROUP BY emp_id");
             if($join_date==$year_month && $discontinution_date==$year_month){
                $joining=date('Y-m-d',strtotime($emp->empJoiningDate));
                $discontinution=date('Y-m-d',strtotime($emp->date_of_discontinuation));
                $shift_employee=DB::SELECT("SELECT emp_id,MIN(shift_date) as starts_date,
                MAX(shift_date) as end_date
                FROM three_shift_employee
                WHERE shift_date BETWEEN '$joining' AND '$discontinution'
                AND three_shift_employee.emp_id='$emp->emp_id'
                GROUP BY emp_id");  
             }
          }
          elseif($discontinution_date==$year_month){
            $discontinution=date('Y-m-d',strtotime($emp->date_of_discontinuation));
            $shift_employee=DB::SELECT("SELECT emp_id,MIN(shift_date) as starts_date,
            MAX(shift_date) as end_date
            FROM three_shift_employee
            WHERE shift_date BETWEEN '$s' AND '$discontinution'
            AND three_shift_employee.emp_id='$emp->emp_id'
            GROUP BY emp_id");  
          }
          else{
            $shift_employee=DB::SELECT("SELECT emp_id,MIN(shift_date) as starts_date,
            MAX(shift_date) as end_date
            FROM three_shift_employee
            WHERE shift_date BETWEEN '$s' AND '$e'
            AND three_shift_employee.emp_id='$emp->emp_id'
            GROUP BY emp_id");
          }
          foreach($shift_employee as $shift_employees){
              $data=[
                'emp_id' =>$shift_employees->emp_id,
                'shift_start' =>$shift_employees->starts_date,
                'shift_end' =>$shift_employees->end_date,
              ];
            $shift_s_e[]=$data;
          }
        }
        $shift_s_e = collect($shift_s_e);
        $shift_s_e = $shift_s_e->chunk(500);
        foreach ($shift_s_e as $shift_s_es)
        {
            \DB::table('emp_shift_start_end')->insert($shift_s_es->toArray());
        }
       //check join or discontinuation or no condition match wise shift start and shift end
        
       //overtime condition start
        $overtime_result=DB::SELECT("SELECT three_shift_employee.emp_id as shift_emp,attendance.emp_id,
        employees.employeeId,employees.empFirstName,
        attendance.emp_id,attendance_setup.exit_time AS exit_time,
        attendance.out_time as out_time,HOUR(attendance_setup.exit_time) as hour_exit_time,
        HOUR(attendance.out_time) as hour_out_time,
        MINUTE(attendance_setup.exit_time) as minute_exit_time,
        MINUTE(attendance.out_time) as minute_out_time,
        attendance.date as att_date  
          FROM three_shift_employee 
          JOIN emp_shift_start_end ON three_shift_employee.emp_id=emp_shift_start_end.emp_id 
          JOIN employees ON three_shift_employee.emp_id=employees.id
          JOIN attendance ON three_shift_employee.emp_id=attendance.emp_id
          JOIN attendance_setup ON three_shift_employee.emp_shift=attendance_setup.id 
          WHERE attendance.date BETWEEN shift_start AND shift_end
          AND attendance.emp_id=emp_shift_start_end.emp_id
          AND employees.empOTStatus=1 AND employees.is_three_shift=1
          AND three_shift_employee.emp_shift IS NOT NULL
          AND DAYNAME(attendance.date)!='Friday'
          AND time(attendance.out_time)>time(attendance_setup.exit_time)
          AND attendance.in_time!=attendance.out_time
          GROUP BY attendance.emp_id,attendance.date"); 
          $ot_minute=0;
          $data=0;
          $insert_data = [];
          foreach($overtime_result as $extraots){
           $minute=0;
          $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;
          if($ot_hour>=2){
              $ot_hours=2;
          }
          elseif($ot_hour<2){
           $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time;
           $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
           if($ot_minute>=25 && $ot_minute<55){
               $minute=.5;
           }
           elseif($ot_minute>=55){
               $minute=1;
           }
           else{
               $minute=0;
           }
          }
          else{
              $ot_hours=0;
          }
          $ot_data = [
           'emp_id' =>$extraots->emp_id,
           'emp_hour'=>$ot_hours,
           'emp_minute'=>$minute,
       ];
       $insert_data[] = $ot_data;
      }
      $insert_data = collect($insert_data);
      $chunks = $insert_data->chunk(500);
      foreach ($chunks as $chunk)
      {
         \DB::table('extra_ot_result')->insert($chunk->toArray());
      }
     $extraotsss=DB::SELECT("SELECT extra_ot_result.emp_id,SUM(emp_hour) as total_hour,SUM(emp_minute) as total_minute from extra_ot_result
     GROUP BY extra_ot_result.emp_id");
     //overtime condition end
    //ot result insert to table start
    $ott=[];
    foreach ($extraotsss as $or){
            $data=[
                'emp_ids'=>$or->emp_id,
                'overtime_hour'=>$or->total_hour+$or->total_minute,
                'month'=>$s,
            ];
            $ott[]=$data;
    }
      $ott = collect($ott);
      $ott = $ott->chunk(500);
      foreach ($ott as $otts)
      {
         \DB::table('employee_overtime')->insert($otts->toArray());
      }    
   $total_emp_overtime_empls=DB::SELECT("SELECT employee_overtime.emp_ids as rmid,SUM(employee_overtime.overtime_hour) as total_overtime FROM employee_overtime GROUP BY employee_overtime.emp_ids");
   $temp_ott=[];
    foreach($total_emp_overtime_empls as $tmp_overtime){
        $data=[
            'overtime_id' =>$tmp_overtime->rmid,
            'employee_overtime' =>$tmp_overtime->total_overtime,
            'month' =>$s
        ];
        $temp_ott[]=$data;
    }
    $temp_ott = collect($temp_ott);
    $temp_ott = $temp_ott->chunk(500);
    foreach ($temp_ott as $temp_otts)
    {
       \DB::table('temp_employee_overtime')->insert($temp_otts->toArray());
    } 
    //ot result insert to table end

   //employee late count start
    $late_data=DB::SELECT("SELECT three_shift_employee.emp_id,
    attendance.in_time,attendance_setup.max_entry_time,attendance.date 
    FROM three_shift_employee 
    JOIN emp_shift_start_end ON three_shift_employee.emp_id=emp_shift_start_end.emp_id
    JOIN attendance_setup ON three_shift_employee.emp_shift=attendance_setup.id 
    JOIN attendance ON three_shift_employee.emp_id=attendance.emp_id 
    JOIN employees ON three_shift_employee.emp_id=employees.id
    WHERE attendance.date BETWEEN shift_start AND shift_end
    AND employees.is_three_shift=1 
    GROUP BY attendance.emp_id,attendance.date
    ");
    foreach($late_data as $lates){
      $in_time=date('H:i',strtotime($lates->in_time));
      $shift_time=date('H:i',strtotime($lates->max_entry_time));
      if($in_time>$shift_time){
        $late_day=1; 
      }else{
        $late_day=0; 
      }
      DB::table('shift_emp_late')->insert([
        'emp_id' =>$lates->emp_id,
        'late' =>$late_day,
    ]);
    }
    $late=DB::SELECT("SELECT shift_emp_late.emp_id as emp_id,SUM(shift_emp_late.late) as late
    FROM shift_emp_late 
    GROUP BY shift_emp_late.emp_id");
       $latee=0;
       foreach ($late as $lates){
               $insert[]=[
                   'emp_id'=>$lates->emp_id,
                   'total_late'=>$lates->late,
                   'month'=>$s,
               ];
       }
       if($latee%550==0){
           if(!empty($insert)){
               $insertData = DB::table('emp_total_late')->insert($insert);
               unset($insert);
           }
       }
       $latee++;
   //employee late count end

   //employee att bonus start
   $att=DB::SELECT("SELECT three_shift_employee.emp_id,
   tbattendance_bonus.bTitle,tbattendance_bonus.bAmount
   FROM three_shift_employee
   JOIN employees ON three_shift_employee.emp_id=employees.id
   JOIN tbattendance_bonus ON employees.empAttBonusId=tbattendance_bonus.id
   WHERE employees.is_three_shift=1 AND three_shift_employee.shift_date
   BETWEEN '$s' AND '$e' GROUP BY three_shift_employee.emp_id");
   $checkattbonus=DB::table('attendance_bonus')
   ->where('month',date('Y-m',strtotime($request->salary_sheet_month)))
   ->count();
   if($checkattbonus>0){
        $att_bonus_incret=0;
        $deleteattbonus=DB::table('attendance_bonus')
        ->where('month',date('Y-m',strtotime($request->salary_sheet_month)))
        ->delete();
        foreach ($att as $atts){
            if($atts->bAmount==''){
                $bonus=0;
            }
            else{
                $bonus=$atts->bAmount;
            }
            $insert[]=[
                'bonus_id'=>$atts->emp_id,
                'bonus_amount'=>$bonus,
                'emp_total_amount'=>$bonus,
                'month'=>date('Y-m',strtotime($request->salary_sheet_month)),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
        }
        if($att_bonus_incret%550==0){
            if(!empty($insert)){
                $insertData = DB::table('attendance_bonus')->insert($insert);
                unset($insert);
            }
        }
        $att_bonus_incret++;
    }
    else{
        $att_bonus_incret_one=0;
        foreach ($att as $atts){
            if($atts->bAmount==''){
                $bonus=0;
            }
            else{
                $bonus=$atts->bAmount;
            }
            $insert[]=[
                'bonus_id'=>$atts->emp_id,
                'bonus_amount'=>$bonus,
                'emp_total_amount'=>$bonus,
                'month'=>date('Y-m',strtotime($request->salary_sheet_month)),
                'created_at' =>Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ];
        }
        if($att_bonus_incret_one%550==0){
            if(!empty($insert)){
                $insertData = DB::table('attendance_bonus')->insert($insert);
                unset($insert);
            }
        }
        $att_bonus_incret_one++;
    }
   //employee att bonus end

   //leave count and insert start
     $leave_tota=DB::SELECT("SELECT emp_shift_leave.emp_id as emp_id,
     SUM(total) as totals
     from emp_shift_leave 
     LEFT JOIN  emp_shift_start_end ON emp_shift_leave.emp_id=emp_shift_start_end.emp_id
     WHERE emp_shift_leave.leave_date BETWEEN shift_start AND shift_end    
     GROUP BY emp_shift_leave.emp_id");
    $total_emp_leavesss=0;
    foreach ($leave_tota as $leaves){
    $insert[]=[
    'emp_idss'=>$leaves->emp_id,
    'total_leaves_taken'=>$leaves->totals,
    'month'=>$s,
    ];
    }
    if($total_emp_leavesss%550==0){
    if(!empty($insert)){
    $insertData = DB::table('total_employee_leave')->insert($insert);
    unset($insert);
    }
    }
    $total_emp_leavesss++;
   //leave count and insert end
   //employee present count and insert to table start 
      $present = DB::SELECT("SELECT attendance.emp_id,shift_start,shift_end,
      count(date) as present
      from attendance 
      LEFT JOIN employees ON attendance.emp_id=employees.id
      LEFT JOIN  emp_shift_start_end ON attendance.emp_id=emp_shift_start_end.emp_id
      WHERE attendance.date BETWEEN shift_start AND shift_end
      AND attendance.in_time IS NOT NULL AND employees.is_three_shift=1    
      GROUP BY attendance.emp_id
      ");
         $pre_insert=[];
         foreach ($present as $presents){
             $data=[
                 'emp_id'=>$presents->emp_id,
                 'total_present'=>$presents->present,
                 'month'=>$s,
             ];
             $pre_insert[]=$data;
         }     
         $pre_insert = collect($pre_insert);
         $pre_insert = $pre_insert->chunk(500);
        foreach ($pre_insert as $pre_inserts)
        {
        \DB::table('tb_total_present')->insert($pre_inserts->toArray());
        }
   //employee present count and insert to table end 
   //salary and other table merge start
//    $start = date('Y-m-01',strtotime($request->salary_sheet_month));
   $end = date('Y-m-t',strtotime($request->salary_sheet_month));
//    $monthscheck=date('Y-m-d',strtotime($request->salary_sheet_month));
//    $bonusmonth=date('Y-m',strtotime($request->salary_sheet_month));


   //employee shift status insert start
   $start_end=DB::table('emp_shift_start_end')
   ->select('emp_id','shift_start','shift_end')
   ->groupBy('emp_id')
   ->get();
   $shift_start_ends=[];
   foreach($start_end as $start_ends){
    $data=DB::table('three_shift_employee')
    ->where('three_shift_employee.emp_id',$start_ends->emp_id)
    ->whereBetween('shift_date',[$start_ends->shift_start,$start_ends->shift_end])
    ->leftjoin('attendance','date','=','shift_date')
    ->leftjoin('emp_shift_leave_attendance','leave_date','=','shift_date')
    ->leftjoin('emp_shift_holiday','holiday_date','=','shift_date')
    ->leftjoin('tbweekend_regular_day','weekend_date','=','shift_date')
    ->select('three_shift_employee.id','three_shift_employee.emp_id','shift_date','weekend','weekend_date','leave_date','date','holiday_date')
    ->orderBy('three_shift_employee.id','ASC')
    ->get();
    foreach($data as $item){
        $data=[
            'emp_id'=>$item->emp_id,
            'weekend'=>$item->weekend,
            'present'=>$item->date,
            'leave_date'=>$item->leave_date,
            'holiday'=>$item->holiday_date,
            'regular'=>$item->weekend_date,
            'shift_date'=>$item->shift_date
        ];
        $shift_start_ends[]=$data;
      }
   }
   $shift_start_ends = collect($shift_start_ends);
   $shift_start_ends = $shift_start_ends->chunk(500);
   foreach ($shift_start_ends as $shift_start_end)
   {
    \DB::table('emp_shift_status')->insert($shift_start_end->toArray());
   }
   //employee shift status insert end


   $y=date('Y',strtotime($request->salary_sheet_month));
   $m=date('m',strtotime($request->salary_sheet_month));
   $data=DB::table('payroll_salary')
       ->leftJoin('employees', function ($query) {
           $query->on('payroll_salary.emp_id', '=', 'employees.id');
           $query->where('employees.empAccStatus','=',1);
           $query->where('employees.is_three_shift',1);
       })
       ->leftJoin('payroll_grade', function ($query) {
           $query->on('payroll_salary.grade_id', '=', 'payroll_grade.id');
       })
       ->leftJoin('units', function ($query) {
           $query->on('employees.unit_id', '=', 'units.id');
       })
       ->leftJoin('floors', function ($query) {
           $query->on('employees.floor_id', '=', 'floors.id');
       })
       ->leftJoin('tblines', function ($query) {
           $query->on('employees.line_id', '=', 'tblines.id');
       })
       ->leftJoin('tb_advance_salary', function ($query) use($s) {
           $query->on('payroll_salary.emp_id', '=', 'tb_advance_salary.emp_id');
           $query->where('tb_advance_salary.month','=',$s);
       })
       ->leftJoin('emp_shift_start_end', function ($query) {
        $query->on('payroll_salary.emp_id', '=', 'emp_shift_start_end.emp_id');
       })
       ->leftJoin('total_employee_leave', function ($query) use($month_day) {
           $query->on('payroll_salary.emp_id', '=', 'total_employee_leave.emp_idss');
           $query->where('total_employee_leave.month','=',$month_day);
       })
       ->leftJoin('emp_shift_leave', function ($query) use($month_day) {
           $query->on('payroll_salary.emp_id', '=', 'emp_shift_leave.emp_id');
           $query->whereYear('emp_shift_leave.leave_date', '=',date('Y',strtotime($month_day)));
           $query->whereMonth('emp_shift_leave.leave_date', '=',date('m',strtotime($month_day)));
       })
       ->leftJoin('emp_total_late', function ($query) use($month_day) {
           $query->on('payroll_salary.emp_id', '=', 'emp_total_late.emp_id');
           $query->where('emp_total_late.month','=',$month_day);
       })
       ->leftJoin('tb_leave_type', function ($query) {
           $query->on('emp_shift_leave.leave_id', '=', 'tb_leave_type.id');
       })
       ->leftJoin('tb_total_present', function ($query) use($month_day) {
           $query->on('payroll_salary.emp_id', '=', 'tb_total_present.emp_id');
           $query->where('tb_total_present.month','=',$month_day);
       })
       ->leftJoin('temp_employee_overtime', function ($query) use($month_day) {
           $query->on('payroll_salary.emp_id', '=', 'temp_employee_overtime.overtime_id');
           $query->where('temp_employee_overtime.month','=',$month_day);
       })
       ->leftJoin('emp_manual_overtime', function ($query) use($year_month) {
           $query->on('payroll_salary.emp_id', '=', 'emp_manual_overtime.emp_overtime_id');
           $query->where('emp_manual_overtime.month','=',$year_month);
       })
       ->leftJoin('attendance_bonus', function ($query) use($year_month) {
           $query->on('payroll_salary.emp_id', '=', 'attendance_bonus.bonus_id');
           $query->where('attendance_bonus.month','=',$year_month);
       })
       ->leftJoin('employees_bonus', function ($query) use($year_month) {
           $query->on('payroll_salary.emp_id', '=', 'employees_bonus.emp_id');
           $query->where('employees_bonus.date','=',$year_month);
       })
       ->leftJoin('festival_bonus', function ($query) use($year_month) {
           $query->on('payroll_salary.emp_id', '=', 'festival_bonus.emp_id');
           $query->where('festival_bonus.month','=',$year_month);
       })
       ->leftJoin('tb_deduction', function ($query) use($year_month) {
           $query->on('payroll_salary.emp_id', '=', 'tb_deduction.emp_id');
           $query->where('tb_deduction.month','=',$year_month);
       })
       ->leftJoin('tb_loan_deduction', function ($query) use($year_month) {
           $query->on('payroll_salary.emp_id', '=', 'tb_loan_deduction.emp_id');
           $query->where('tb_loan_deduction.month','=',$year_month);
       })
       ->leftJoin('tb_production_bonus', function ($query) use($year_month) {
           $query->on('payroll_salary.emp_id', '=', 'tb_production_bonus.emp_id');
           $query->where('tb_production_bonus.month','=',$year_month);
       })
       ->leftJoin('departments', function ($query) {
           $query->on('employees.empDepartmentId', '=', 'departments.id');
       })
       ->Join('designations', function ($query) {
           $query->on('designations.id', '=', 'employees.empDesignationId');
       })
       ->Join('three_shift_employee', function ($query) use($year,$month) {
        $query->on('payroll_salary.emp_id', '=', 'three_shift_employee.emp_id');
        $query->whereYear('three_shift_employee.shift_date','=',$year);
        $query->whereMonth('three_shift_employee.shift_date','=',$month);
      })
       ->select('emp_shift_leave.leave_date','payroll_salary.*','employees.is_three_shift','employees.empDesignationId','employees.payment_mode','employees.empDepartmentId','employees.empFirstName','employees.empLastName','employees.empJoiningDate','employees.empSection','employees.employeeId','payroll_grade.grade_name','designations.designation','employees.unit_id','units.name','employees.floor_id','floors.floor','employees.line_id','tblines.line_no','employees.empDepartmentId','departments.departmentName','employees.work_group','employees.payment_mode','total_employee_leave.total_leaves_taken','employees_bonus.emp_bonus as special_bonus','employees_bonus.emp_amount','attendance_bonus.bonus_amount as attendance_bonus','attendance_bonus.bonus_percent','festival_bonus.emp_bonus as f_bonus','festival_bonus.emp_amount as f_amount','tb_total_present.total_present','emp_shift_leave.leave_id','emp_shift_leave.total','tb_deduction.normal_deduction_amount','tb_loan_deduction.month_wise_deduction_amount','tb_production_bonus.production_amount','tb_production_bonus.production_percent','temp_employee_overtime.employee_overtime as temp_ot','emp_manual_overtime.employee_overtime as manual_ot','designations.gradeId','emp_total_late.total_late','date_of_discontinuation','tb_advance_salary.advance_amount','three_shift_employee.shift_date','emp_shift_start_end.shift_start as emp_shift_start','emp_shift_start_end.shift_end as emp_shift_end')
       ->selectraw("CONCAT(GROUP_CONCAT( DISTINCT(tb_leave_type.leave_type),emp_shift_leave.total)) as leave_name_value")
       ->selectraw("GROUP_CONCAT(DISTINCT(emp_shift_leave.leave_id)SEPARATOR ',') as leave_with_id")
       ->selectraw("GROUP_CONCAT(DISTINCT(emp_shift_leave.total)SEPARATOR ',') as leave_with_total")
       ->groupBy('payroll_salary.emp_id')
       ->get();
       //salary and other table merge end
       //check if slary history has already data then delete
          $month=date('Y-m-d',strtotime($request->salary_sheet_month));
          $dates=date('Y-m',strtotime($request->salary_sheet_month));
          $check=DB::table('tb_salary_history_shift')->where('month',$month)->count();
          if($check>0){
              $delete=DB::table('tb_salary_history_shift')->where('month',$month)->delete();
          }
          $total_salary_data=[];
       //check if slary history has already data then delete
       //salary condition wise data insert to table start
         //salary history insert data  start
         foreach($data as $testdata){
        
            global $deduction;
            global  $ac_abs_day;
            global  $weekend;
            global  $holi;
            global  $total_payable_days;
            global $total_gross;
            global  $join_day;
            global $dis_continution_day;
            global $working_day;
            $att_bonus=0;
            $status=0;
            $total_gross_pre=0;
            $current_month_day=date('t',strtotime($request->salary_sheet_month));
            $overtime_rate=$testdata->basic_salary/104;
            $actual_overtime_rate=$overtime_rate;
            $total_overtt_time=$testdata->temp_ot+$testdata->manual_ot;
            $overtime_amount=$actual_overtime_rate*$total_overtt_time;
            //default calculation
            $leave_holiday=0;
            $regular_present=0;
            $regular_leave=0;
            $total_weekend=DB::table('emp_shift_status')
            ->where('emp_id',$testdata->emp_id)
            ->where('weekend','!=',NULL)
            ->where('present','=',NULL)
            ->where('leave_date','=',NULL)
            ->where('holiday','=',NULL)
            ->where('regular','=',NULL)
            ->count();
            $weekend=$total_weekend;
            $emp_leave=DB::table('emp_shift_leave')
            ->where('emp_id',$testdata->emp_id)
            ->whereBetween('leave_date',[$testdata->emp_shift_start,$testdata->emp_shift_end])
            ->select('emp_id','leave_date')
            ->get();
            foreach($emp_leave as $emp_leaves){
              $leave_holiday+=DB::table('emp_shift_holiday')
              ->where('holiday_date',$emp_leaves->leave_date)
              ->count();
            }
            $total_holiday=DB::table('emp_shift_holiday')
            ->whereBetween('holiday_date',[$testdata->emp_shift_start,$testdata->emp_shift_end])
            ->count()-$leave_holiday;
            $holiday=$total_holiday;
            $work_days_emp=DB::table('three_shift_employee')
            ->where('emp_id',$testdata->emp_id)
            ->whereBetween('shift_date',[$testdata->emp_shift_start,$testdata->emp_shift_end])
            ->count();
            $actual_working_day=$work_days_emp-$weekend-$holiday;
            $total_regular_date=DB::table('tbweekend_regular_day')
            ->whereBetween('weekend_date',[$testdata->emp_shift_start,$testdata->emp_shift_end])
            ->select('weekend_date')
            ->get();
            foreach($total_regular_date as $rd){
               $regular_present+=DB::table('attendance')
               ->where('emp_id',$testdata->emp_id)
               ->where('date',$rd->weekend_date)
               ->count();
               $regular_leave+=DB::table('emp_shift_leave')
               ->where('emp_id',$testdata->emp_id)
               ->where('leave_date',$rd->weekend_date)
               ->count();
            }
            $total_regular_day=DB::table('tbweekend_regular_day')
            ->whereBetween('weekend_date',[$testdata->emp_shift_start,$testdata->emp_shift_end])
            ->count()-$regular_present-$regular_leave;
            $absentday=$actual_working_day-$testdata->total_leaves_taken-$testdata->total_present+$total_regular_day;
            $ac_abs_day=max($absentday,0);
            $weekend = $weekend;
            $holi = $holiday;
            $month_lst_day=date('t',strtotime($month));
            $total_payable_days=$month_lst_day-$ac_abs_day;
            $deduction=round($testdata->basic_salary/30*$ac_abs_day,2);
            //default calculation




        //check date of discontinution and joining date this month start
        if(date('Y-m',strtotime($testdata->empJoiningDate))==$year_month && date('Y-m',strtotime($testdata->date_of_discontinuation))==$year_month){
            $weekend = $weekend;
            $holi = $holiday;
            $p_date1 = date_create(date('Y-m-d', strtotime($testdata->emp_shift_start)));
            $p_date2 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
            $previous_diff = date_diff($p_date1, $p_date2);
            $total_previous_difference = $previous_diff->format("%a");
            $p_date1 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
            $p_date2 = date_create(date('Y-m-d', strtotime($testdata->emp_shift_end)));
            $previous_diff2 = date_diff($p_date1, $p_date2);
            $total_previous_difference2 = $previous_diff2->format("%a");
            $total_previous_difference+=$total_previous_difference2;
            $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
            $date1 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
            $date2 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
            $diff = date_diff($date1, $date2);
            $total_difference = $diff->format("%a") + 1;
            $actual_working = $total_difference - $weekend - $holi;
            $working_day = $actual_working;
            $absentdays = $working_day - $testdata->total_leaves_taken - $testdata->total_present+$total_regular_day;
            $ac_abs_day = max($absentdays, 0);
            $weekend = $weekend;
            $holi = $holiday;
            $month_lst_day = date('t', strtotime($month));
            $total_payable_days = $month_lst_day - $ac_abs_day;
            $month_salary = date('01', strtotime($request->salary_sheet_month));
            $join_date = $testdata->empJoiningDate;
            $join_day = date('d', strtotime($testdata->empJoiningDate));
            if (date('d', strtotime($join_date)) >= $month_salary) {
                $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
            }
            else{
                $deduction=0;
            }
        }
       //check date of discontinution and joining date this month end
    else {
        //check if join date this month
        if (date('Y-m', strtotime($testdata->empJoiningDate)) == $year_month) {
            $weekend = $weekend;
            $holi = $holiday;
            $p_date1 = date_create(date('Y-m-d', strtotime($testdata->emp_shift_start)));
            $p_date2 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
            $previous_diff = date_diff($p_date1, $p_date2);
            $total_previous_difference = $previous_diff->format("%a");
            $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
            //  dd($testdata->current_month_day);
            $date1 = date_create(date('Y-m-d', strtotime($testdata->empJoiningDate)));
            $date2 = date_create(date('Y-m-d', strtotime($testdata->emp_shift_end)));
            $diff = date_diff($date1, $date2);
            $total_difference = $diff->format("%a") + 1;
            $actual_working = $total_difference - $weekend - $holi;
            $working_day = $actual_working;
            $absentdays = $working_day - $testdata->total_leaves_taken - $testdata->total_present+$total_regular_day;
            $ac_abs_day = max($absentdays, 0);
            $month_lst_day = date('t', strtotime($month));
            $total_payable_days = $month_lst_day - $ac_abs_day;
            $month_salary = date('01', strtotime($request->salary_sheet_month));
            $join_date = $testdata->empJoiningDate;
            $join_day = date('d', strtotime($testdata->empJoiningDate));
            if (date('d', strtotime($join_date)) >= $month_salary) {
                $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
            }
        }
       // check if join date this month


        //check if date of discontinuation this month
        if (date('Y-m', strtotime($testdata->date_of_discontinuation)) == $year_month) {
            $dis_continution_day = date('d', strtotime($testdata->date_of_discontinuation));
            $weekend = $weekend;
            $holi = $holiday;
            if (date('d', strtotime($testdata->date_of_discontinuation)) >= date('d',strtotime($testdata->emp_shift_start)) && date('d', strtotime($testdata->date_of_discontinuation)) <= date('d',strtotime($testdata->emp_shift_end))) {
                $month_start = date('Y-m-d', strtotime($testdata->emp_shift_start));
                $p_date1 = date_create(date('Y-m-d', strtotime($testdata->date_of_discontinuation)));
                $p_date2 = date_create(date('Y-m-d', strtotime($testdata->emp_shift_end)));
                $previous_diff = date_diff($p_date1, $p_date2);
                $total_previous_difference = $previous_diff->format("%a");
                $actual_working_day = date('d', strtotime($testdata->date_of_discontinuation)) - $weekend - $holi;
                $absentday = $actual_working_day - $testdata->total_leaves_taken - $testdata->total_present+$total_regular_day;
                $ac_abs_day = max($absentday, 0);
                $month_lst_day = date('t', strtotime($month));
                $total_payable_days = $month_lst_day - $ac_abs_day;
                $total_gross_pre = ($testdata->total_employee_salary / $current_month_day) * $total_previous_difference;
                $deduction = round($testdata->basic_salary / 30 * $ac_abs_day, 2);
            }
        }
        //check if date of discontinuation this month
    }



        //check if maternity leave this month
           if(date('Y-m',strtotime($testdata->leave_date))==$year_month && $testdata->leave_with_id==1){
            $month_start=date('Y-m-01',strtotime($request->salary_sheet_month));
            $month_end=date('Y-m-t',strtotime($request->salary_sheet_month));
            $weekend = $weekend;
            $holi = $holiday;
            $actual_working_day=date('t',strtotime($request->salary_sheet_month))- $weekend- $holi;
            $absentday=$actual_working_day-$testdata->total_leaves_taken-$testdata->total_present+$total_regular_day;
            $ac_abs_day=max($absentday,0);
            $total_gross_pre=($testdata->total_employee_salary/$current_month_day)*$testdata->total;
            $deduction=round($testdata->basic_salary/30*$ac_abs_day,2);
            }
       //check if maternity leabve this month



       if($testdata->leave_with_id==1 && $testdata->total_present==''){
           $status=1;
        }
        if($testdata->payment_mode=='Bank' || $testdata->payment_mode=='bKash') {
            $stampFee = 0;
        }
        else {
            $stampFee = 10;
        }
          //check att bonus condition wise
        if($testdata->total=='' && $ac_abs_day==0 && $testdata->total_late<3 && $testdata->empJoiningDate<=$e){
            $att_bonus=$testdata->attendance_bonus;
            if($testdata->date_of_discontinuation!=null && $testdata->date_of_discontinuation<$e){
                $att_bonus=0;
            }
        }else{
            $att_bonus=0;
           }
         //check att bonus condition
       $gross_pay=$testdata->total_employee_salary
       -$deduction-$testdata->normal_deduction_amount
       -$testdata->month_wise_deduction_amount-
       $stampFee;
       $gross_pay=$gross_pay-$total_gross_pre;
       $month=date('Y-m-d',strtotime($request->salary_sheet_month));
       $net_wages=$gross_pay+$overtime_amount+$testdata->f_bonus+$testdata->f_amount+$testdata->production_amount+$testdata->production_percent+$att_bonus+$testdata->advance_amount;
       $actual_net_wages=$net_wages;
       if($actual_net_wages<=1000){
        $gross_pay=$gross_pay+10;
        $actual_net_wages=$actual_net_wages+10;
       }
    
       $data=[
           'emp_id' => $testdata->emp_id,
           'grade_id' => $testdata->gradeId,
           'department_id' => $testdata->empDepartmentId,
           'section_id' => $testdata->empSection,
           'designation_id' => $testdata->empDesignationId,
           'basic_salary' => $testdata->basic_salary,
           'house_rant' => $testdata->house_rant,
           'medical' => $testdata->medical,
           'transport' => $testdata->transport,
           'food' => $testdata->food,
           'other' => $testdata->other,
           'gross' => $testdata->total_employee_salary,
           'gross_pay' => $gross_pay,
           'present' => $testdata->total_present,
           'absent' => $ac_abs_day,
           'absent_deduction_amount' => $deduction,
           'attendance_bonus' =>  $att_bonus,
           'attendance_bonus_percent' => $testdata->bonus_percent,
           'festival_bonus_amount' => $testdata->f_amount,
           'festival_bonus_percent' => $testdata->f_bonus,
           'increment_bonus_amount' => $testdata->emp_amount,
           'increment_bonus_percent' => $testdata->special_bonus,
           'normal_deduction' => $testdata->normal_deduction_amount,
           'advanced_deduction' => $testdata->month_wise_deduction_amount,
           'production_bonus' => $testdata->production_amount,
           'production_percent' => $testdata->production_percent,
           'working_day' =>$working_day,
           'weekend' =>$weekend,
           'holiday' =>$holi,
           'total_payable_days' =>$total_payable_days,
           'overtime' =>$total_overtt_time,
           'overtime_rate' =>$actual_overtime_rate,
           'overtime_amount' =>$overtime_amount,
           'leave' =>$testdata->leave_with_id,
           'total' =>$testdata->total_leaves_taken,
           'total_late' =>$testdata->total_late,
           'advance_salary'=>$testdata->advance_amount,
           'net_amount' =>$actual_net_wages,
           'month' => $month,
           'dates' => $dates,
           'status' =>  $status,
           'created_at' =>Carbon::now()->toDateTimeString(),
           'updated_at' =>Carbon::now()->toDateTimeString(),
           'shift_start' =>  $testdata->emp_shift_start,
           'shift_end' =>  $testdata->emp_shift_end,
       ];
       $total_salary_data[]=$data;
     }
            $total_salary_data = collect($total_salary_data);
            $total_salary_data = $total_salary_data->chunk(500);
            foreach ($total_salary_data as $total_salary_data)
            {
             \DB::table('tb_salary_history_shift')->insert($total_salary_data->toArray());
            }
            
        $insert_check_data=DB::table('tb_salary_process_check')->insert([
            'month' =>$month,
            'status' =>0,
            'process_id' =>auth()->user()->id,
            'created_at' =>Carbon::now()->toDateTimeString(),
            'updated_at' =>Carbon::now()->toDateTimeString(),
        ]);
        \Illuminate\Support\Facades\DB::table('tbactivity_logs_history')->insert([
            'acType'=>'Salary Process',
            'details'=>"Salary has been processed for ".Carbon::parse($request->salary_sheet_month)->format('M Y'),
            'createdBy'=>Auth::user()->id,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString(),
        ]);
        DB::table('total_employee_leave')->truncate();
        DB::table('tb_total_present')->truncate();
        DB::table('employee_overtime')->truncate();
        DB::table('temp_employee_overtime')->truncate();
        DB::table('emp_total_late')->truncate();
        DB::table('regular_weekend_presence')->truncate();
        DB::table('total_weekend_present')->truncate();
        //Start For In Active Employee
        DB::table('employees')
        ->where('date_of_discontinuation','>=',$s)
        ->where('is_three_shift',1)
        ->update([
            'empAccStatus'=>0,
        ]);
       //End For In Active Employee
    return redirect('/report/payroll/employee/salary/sheet/three_shift');
  //salary history insert data end
  }




}

