<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use redirect;
use Session;
use stdClass;
use PDF;
use Excel;
use mPDF;

class datesalarycontroller extends Controller
{
    //working day find function
    public static function dayCalculator($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $interval1=1+round(abs($date1-$date2)/86400);
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $dcount=0;
        foreach($festivalLeave as $fleave){
            if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);

            }
            else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);

            }
            else{
                continue;
            }
        }
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
        $key=0;
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {

            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);

            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key++;
                }
            }

        }
        $interval=(int)$interval1-((int)$dcount+$key);
        return $interval;
    }

    //weekend find function
    function weekdayCalculator($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $interval1=1+round(abs($date1-$date2)/86400);
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $dcount=0;
        foreach($festivalLeave as $fleave){
            if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
            }
            else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
            }
            else{
                continue;
            }
        }
        $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
        $start_date=date('Y-m-d', $date1);
        $end_date=date('Y-m-d', $date2);
        $key=0;
        for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
            $dates = $i->format('Y-m-d');
            $timestamp = strtotime($dates);
            $day = date('l', $timestamp);
            for($j=0;$j<count($weekends);$j++)
            {
                if($day==$weekends[$j]){
                    $key++;
                }
            }
        }
        $interval=(int)$interval1-((int)$dcount+$key);
        return $key;
    }

    //holiday find function
    function holiday($d1,$d2){
        $date1=strtotime($d1);
        $date2=strtotime($d2);
        $interval1=1+round(abs($date1-$date2)/86400);
        $festivalLeave = DB::table('tb_festival_leave')->get();
        $dcount=0;
        foreach($festivalLeave as $fleave){
            if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
            }
            else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
            }
            else{
                continue;
            }
        }
        return $dcount;
    }

    //date wise salary sheet generate
    public function date_wise_process(Request $request){
        $start_date=date('Y-m-d',strtotime($request->salary_start_date));
        $end_date=date('Y-m-d',strtotime($request->salary_end_date));
        $check=DB::SELECT("SELECT tb_salary_process_check.start_date,tb_salary_process_check.end_date
        FROM tb_salary_process_check
        WHERE tb_salary_process_check.start_date BETWEEN '$start_date' AND '$end_date' OR tb_salary_process_check.end_date BETWEEN '$start_date' AND '$end_date'");
        $count = count($check);
        if($count>0){
            Session::flash('salaryprocesscheckdatewise', 'Process Salary Already Completed');
            return redirect()->back();
        }else{
        $month=date('Y-m-d',strtotime($request->salary_start_date));
        $workdays=$this->dayCalculator($start_date,$end_date);
        $weekend=$this->weekdayCalculator($start_date,$end_date);
        $holiday=$this->holiday($start_date,$end_date);
        $money_deduction=10;
        $process_check=DB::table('tb_salary_process_check')
            ->where('start_date','<=',$start_date)
            ->where('end_date','>=',$end_date)
            ->where('status','=',1)
            ->count();
        if($process_check>0){
            Session::flash('salaryprocesscheckdatewise', 'Process Already Completed');
            return redirect()->back();
        }
        $overtime_result = DB::select( DB::raw("SELECT employees.empFirstName,attendance_setup.shiftName,DAYNAME(attendance.date),attendance_setup.exit_time as shift_wise_exit_time,employees.empOTStatus,attendance.emp_id,attendance.date,HOUR(attendance_setup.exit_time) as exit_hour,HOUR(attendance.out_time) as hour_out_time,HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time) as overtime_hour,MINUTE(attendance.out_time)-MINUTE(attendance_setup.exit_time) as overtime_minute FROM attendance LEFT JOIN employees ON attendance.emp_id=employees.id LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empOTStatus=1 AND HOUR(attendance.out_time)>HOUR(attendance_setup.exit_time) AND MINUTE(attendance.out_time)>MINUTE(attendance_setup.exit_time) AND DAYNAME(attendance.date)!='Friday' AND HOUR(attendance.out_time)-HOUR(attendance_setup.exit_time)<=2 GROUP BY attendance.emp_id"));
        $present = DB::select(DB::raw("SELECT emp_id,empFirstName,count(date) as present,attendance.date,DAYNAME(attendance.date) as day_name from attendance,employees WHERE DAYNAME(attendance.date)!='Friday' AND attendance.emp_id=employees.id AND date BETWEEN '$start_date' AND '$end_date' GROUP BY attendance.emp_id"));
        $leave_result_two=DB::SELECT("SELECT employee_id,leave_type_id,SUM(actual_days) as total FROM `tb_leave_application` WHERE status=1 AND (leave_ending_date>='$start_date' AND leave_ending_date<='$end_date' OR leave_starting_date >= '$start_date' AND leave_starting_date<='$end_date') GROUP BY leave_type_id");
        $leave_result_two_check=DB::table('leave_of_employee')->where('month',$month)->count();
        if($leave_result_two_check>0){
            $leave_result_two_delete=DB::table('leave_of_employee')->where('month',$month)->delete();
        }
        $att=DB::SELECT("SELECT employees.id as emp_id,tbattendance_bonus.bTitle,tbattendance_bonus.bAmount FROM employees LEFT JOIN tbattendance_bonus ON employees.empAttBonusId = tbattendance_bonus.id WHERE employees.empAccStatus=1");
        $ott=0;
        foreach ($overtime_result as $or){
            if($or->overtime_minute>=55){
                $insert[]=[
                    'emp_ids'=>$or->emp_id,
                    'overtime_hour'=>$or->overtime_hour+1,
                    'month'=>$month,
                ];
            }
            elseif($or->overtime_minute>=25){
                $insert[]=[
                    'emp_ids'=>$or->emp_id,
                    'overtime_hour'=>$or->overtime_hour+0.5,
                    'month'=>$month,
                ];
            }
            else{
                $insert[]=[
                    'emp_ids'=>$or->emp_id,
                    'overtime_hour'=>$or->overtime_hour,
                    'month'=>$month,
                ];
            }
        }
        if($ott%550==0){
            if(!empty($insert)){
                $insertData = DB::table('employee_overtime')->insert($insert);
                unset($insert);
            }
        }
        $ott++;
        $total_emp_overtime_empls=DB::SELECT("SELECT employee_overtime.emp_ids as rmid,SUM(employee_overtime.overtime_hour) as total_overtime FROM employee_overtime GROUP BY employee_overtime.emp_ids");

        $temp_ott=0;
        foreach($total_emp_overtime_empls as $tmp_overtime){
            $insert[]=[
                'overtime_id' =>$tmp_overtime->rmid,
                'employee_overtime' =>$tmp_overtime->total_overtime,
                'month' =>$month
            ];
        }
        if($temp_ott%550==0){
            if(!empty($insert)){
                $insertData = DB::table('temp_employee_overtime')->insert($insert);
                unset($insert);
            }
        }
        $temp_ott++;
        $pre_insert=0;
        foreach ($present as $presents){
            $insert[]=[
                'emp_id'=>$presents->emp_id,
                'total_present'=>$presents->present,
                'total_absent'=>$this->dayCalculator($start_date,$end_date)-$presents->present,
                'month'=>$month,
            ];
        }
        if($pre_insert%550==0){
            if(!empty($insert)){
                $insertData = DB::table('tb_total_present')->insert($insert);
                unset($insert);
            }
        }
        $pre_insert++;
        $condition_leave=0;
        foreach ($leave_result_two as $leaves_two){
            if($leaves_two->leave_type_id==1){
                $maternity_leave=$leaves_two->total;
                $insert[]=[
                    'emp_id'=>$leaves_two->employee_id,
                    'leave_type_id'=>$leaves_two->leave_type_id,
                    'total'=>$leaves_two->total-$maternity_leave,
                    'month'=>$month,
                ];
            }
            elseif($leaves_two->leave_type_id==4){
                $earn_leave=$leaves_two->total;
                $insert[]=[
                    'emp_id'=>$leaves_two->employee_id,
                    'leave_type_id'=>$leaves_two->leave_type_id,
                    'total'=>$leaves_two->total-$earn_leave,
                    'month'=>$month,
                ];
            }
            else{
                $insert[]=[
                    'emp_id'=>$leaves_two->employee_id,
                    'leave_type_id'=>$leaves_two->leave_type_id,
                    'total'=>$leaves_two->total,
                    'month'=>$month,
                ];
            }
        }
        if($condition_leave%550==0){
            if(!empty($insert)){
                $insertData = DB::table('leave_of_employee')->insert($insert);
                unset($insert);
            }
        }
        $condition_leave++;
        $leave_tota=DB::SELECT("SELECT leave_of_employee.emp_id as employee_id,SUM(leave_of_employee.total) as total_leave 
                                FROM leave_of_employee
                                WHERE month='$start_date'
                                GROUP BY leave_of_employee.emp_id");
        $total_emp_leavesss=0;
        foreach ($leave_tota as $leaves){
            $insert[]=[
                'emp_idss'=>$leaves->employee_id,
                'total_leaves_taken'=>$leaves->total_leave,
                'month'=>$month,
            ];
        }
        if($total_emp_leavesss%550==0){
            if(!empty($insert)){
                $insertData = DB::table('total_employee_leave')->insert($insert);
                unset($insert);
            }
        }
        $total_emp_leavesss++;
        $checkattbonus=DB::table('attendance_bonus')->where('month',date('Y-m',strtotime($request->salary_start_date)))->count();
        if($checkattbonus>0){
            $att_bonus_incret=0;
            $deleteattbonus=DB::table('attendance_bonus')->where('month',date('Y-m',strtotime($request->salary_start_date)))->delete();
            foreach ($att as $atts){
                if($atts->bAmount==''){
                    $bonus=0;
                }
                else{
                    $bonus=$atts->bAmount;
                }
                $insert[]=[
                    'bonus_id'=>$atts->emp_id,
                    'bonus_amount'=>$bonus,
                    'emp_total_amount'=>$bonus,
                    'month'=>date('Y-m',strtotime($request->salary_start_date)),
                    'created_at' =>Carbon::now()->toDateTimeString(),
                    'updated_at' =>Carbon::now()->toDateTimeString(),
                ];
            }
            if($att_bonus_incret%550==0){
                if(!empty($insert)){
                    $insertData = DB::table('attendance_bonus')->insert($insert);
                    unset($insert);
                }
            }
            $att_bonus_incret++;
        }
        else{
            $att_bonus_incret_one=0;
            foreach ($att as $atts){
                if($atts->bAmount==''){
                    $bonus=0;
                }
                else{
                    $bonus=$atts->bAmount;
                }
                $insert[]=[
                    'bonus_id'=>$atts->emp_id,
                    'bonus_amount'=>$bonus,
                    'emp_total_amount'=>$bonus,
                    'month'=>date('Y-m',strtotime($request->salary_start_date)),
                    'created_at' =>Carbon::now()->toDateTimeString(),
                    'updated_at' =>Carbon::now()->toDateTimeString(),
                ];
            }
            if($att_bonus_incret_one%550==0){
                if(!empty($insert)){
                    $insertData = DB::table('attendance_bonus')->insert($insert);
                    unset($insert);
                }
            }
            $att_bonus_incret_one++;
        }
        $start=date('Y-m-d',strtotime($request->salary_start_date));
        $end=date('Y-m-d',strtotime($request->salary_end_date));
        $monthscheck=date('Y-m-d',strtotime($request->salary_start_date));
        $bonusmonth=date('Y-m',strtotime($request->salary_start_date));
        $data=DB::table('payroll_salary')
            ->leftJoin('employees', function ($query) {
                $query->on('payroll_salary.emp_id', '=', 'employees.id');
                $query->where('employees.empAccStatus','=',1);
            })
            ->leftJoin('payroll_grade', function ($query) {
                $query->on('payroll_salary.grade_id', '=', 'payroll_grade.id');
            })
            ->leftJoin('units', function ($query) {
                $query->on('employees.unit_id', '=', 'units.id');
            })
            ->leftJoin('floors', function ($query) {
                $query->on('employees.floor_id', '=', 'floors.id');
            })
            ->leftJoin('tblines', function ($query) {
                $query->on('employees.line_id', '=', 'tblines.id');
            })
            ->leftJoin('total_employee_leave', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'total_employee_leave.emp_idss');
                $query->where('total_employee_leave.month','=',$monthscheck);
            })
            ->leftJoin('leave_of_employee', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'leave_of_employee.emp_id');
                $query->where('leave_of_employee.month','=',$monthscheck);
            })
            ->leftJoin('tb_leave_type', function ($query) use($start,$end) {
                $query->on('leave_of_employee.leave_type_id', '=', 'tb_leave_type.id');
            })
            ->leftJoin('tb_total_present', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'tb_total_present.emp_id');
                $query->where('tb_total_present.month','=',$monthscheck);
            })
            ->leftJoin('temp_employee_overtime', function ($query) use($monthscheck) {
                $query->on('payroll_salary.emp_id', '=', 'temp_employee_overtime.overtime_id');
                $query->where('temp_employee_overtime.month','=',$monthscheck);
            })
            ->leftJoin('emp_manual_overtime', function ($query) use($bonusmonth) {
                $query->on('payroll_salary.emp_id', '=', 'emp_manual_overtime.emp_overtime_id');
                $query->where('emp_manual_overtime.month','=',$bonusmonth);
            })
            ->leftJoin('departments', function ($query) {
                $query->on('employees.empDepartmentId', '=', 'departments.id');
            })
            ->join('designations', function ($query) {
                $query->on('designations.id', '=', 'employees.empDesignationId');
            })
            ->select('payroll_salary.*','employees.empDesignationId','employees.empDepartmentId','employees.empFirstName','employees.empLastName','employees.empJoiningDate','employees.empSection','employees.employeeId','payroll_grade.grade_name','designations.designation','employees.unit_id','units.name','employees.floor_id','floors.floor','employees.line_id','tblines.line_no','employees.empDepartmentId','departments.departmentName','employees.work_group','employees.payment_mode','total_employee_leave.total_leaves_taken','tb_total_present.total_present','leave_of_employee.leave_type_id','leave_of_employee.total','temp_employee_overtime.employee_overtime as temp_ot','emp_manual_overtime.employee_overtime as manual_ot')
            ->selectraw("CONCAT(GROUP_CONCAT( DISTINCT(tb_leave_type.leave_type),leave_of_employee.total)) as leave_name_value")
            ->selectraw("GROUP_CONCAT(DISTINCT(leave_of_employee.leave_type_id)SEPARATOR ',') as leave_with_id")
            ->selectraw("GROUP_CONCAT(DISTINCT(leave_of_employee.total)SEPARATOR ',') as leave_with_total")
            ->groupBy('payroll_salary.emp_id')
            ->get();
        if($data){
            $month=date('Y-m-d',strtotime($request->salary_start_date));
            $dates=date('Y-m',strtotime($request->salary_start_date));
            $check=DB::table('tb_salary_history_installment')->where('month',$month)->count();
            if($check>0){
                $delete=DB::table('tb_salary_history_installment')->where('month',$month)->delete();
            }
            $current_month_first=date('Y-m-d',strtotime($request->salary_start_date));
            $current_month_last=date('Y-m-d',strtotime($request->salary_end_date));
            $working_day=$this->dayCalculator($current_month_first,$current_month_last);
            $total_sal_datassss=0;
            foreach($data as $testdata){
                $current_month_day=date('t',strtotime($request->salary_start_date));
                $overtime_rate=$testdata->basic_salary/104;
                $actual_overtime_rate=round($overtime_rate,2);
                $total_overtt_time=(int)$testdata->temp_ot+(int)$testdata->manual_ot;
                $overtime_amount=(int)$actual_overtime_rate*(int)$total_overtt_time;
                $absentday=$working_day-$testdata->total_leaves_taken-$testdata->total_present;
                $month_lst_day=date('t',strtotime($request->salary_start_date));
                $total_payable_days=$month_lst_day-$absentday;
                $weekend=$this->weekdayCalculator($current_month_first,$current_month_last);
                $holi=$this->holiday($current_month_first,$current_month_last);
                $gross_pay=$testdata->total_employee_salary;
                $month=date('Y-m-d',strtotime($request->salary_start_date));
                $net_wages=$gross_pay+$overtime_amount;
                $actual_net_wages=round($net_wages,2);
                $insert[]=[
                    'emp_id' => $testdata->emp_id,
                    'grade_id' => $testdata->grade_id,
                    'department_id' => $testdata->empDepartmentId,
                    'designation_id' => $testdata->empDesignationId,
                    'basic_salary' => $testdata->basic_salary,
                    'house_rant' => $testdata->house_rant,
                    'medical' => $testdata->medical,
                    'transport' => $testdata->transport,
                    'food' => $testdata->food,
                    'other' => $testdata->other,
                    'gross' => $testdata->total_employee_salary,
                    'gross_pay' => $gross_pay,
                    'present' => $testdata->total_present,
                    'absent' => $absentday,
                    'absent_deduction_amount' => 0,
                    'attendance_bonus' => 0,
                    'attendance_bonus_percent' => 0,
                    'festival_bonus_amount' => 0,
                    'festival_bonus_percent' => 0,
                    'increment_bonus_amount' => 0,
                    'increment_bonus_percent' => 0,
                    'normal_deduction' => 0,
                    'advanced_deduction' => 0,
                    'production_bonus' => 0,
                    'production_percent' => 0,
                    'working_day' =>$working_day,
                    'weekend' =>$weekend,
                    'holiday' =>$holi,
                    'total_payable_days' =>$total_payable_days,
                    'overtime' =>$total_overtt_time,
                    'overtime_rate' =>$actual_overtime_rate,
                    'overtime_amount' =>$overtime_amount,
                    'leave' =>$testdata->leave_name_value,
                    'total' =>$testdata->total_leaves_taken,
                    'net_amount' =>$actual_net_wages,
                    'month' => $month,
                    'dates' => $dates,
                    'start_date'=>$current_month_first,
                    'end_date'=>$current_month_last,
                    'status' => 0,
                    'created_at' =>Carbon::now()->toDateTimeString(),
                    'updated_at' =>Carbon::now()->toDateTimeString(),
                ];
            }
            if($total_sal_datassss%550==0){
                if(!empty($insert)){
                    $insertData = DB::table('tb_salary_history_installment')->insert($insert);
                    unset($insert);
                }
            }
            $total_sal_datassss++;
        }
        $insert_check_data=DB::table('tb_salary_process_check')->insert([
            'month' =>$month,
            'start_date' =>date('Y-m-d',strtotime($request->salary_start_date)),
            'end_date' =>date('Y-m-d',strtotime($request->salary_end_date)),
            'status' =>0,
            'process_id' =>auth()->user()->id,
            'created_at' =>Carbon::now()->toDateTimeString(),
            'updated_at' =>Carbon::now()->toDateTimeString(),
        ]);
        $delete_total_employee_leave=DB::table('total_employee_leave')->delete();
        $delete_total_employee_present=DB::table('tb_total_present')->delete();
        $delete_total_employee_overtime=DB::table('employee_overtime')->delete();
        $temp_overt=DB::table('temp_employee_overtime')->delete();
        return redirect()->route('datewisesalaryemployees');
      }
    }

    //extra overtime report view
    public function extraovertimeReport(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $employee   =DB::table('employees')->select('id','empFirstName','empLastName','employeeId')->get();
        return view('report.payroll.overtime.extra_overtime_report',compact('department','section','employee'));
    }


    //extra overtime report show
    public function extraovertimeReportshow(Request $request){
        DB::table('extra_ot_result')->delete();
        DB::table('salary_ot')->delete();
        $start_date=date('Y-m-d',strtotime($request->start_date));
        $end_date=date('Y-m-d',strtotime($request->end_date));
        $start_month=date('Y-m-01',strtotime($request->ot_month));
        $end_month=date('Y-m-t',strtotime($request->ot_month));
        $p_select=$request->payment_select;
        if($request->payment_id==0){
            $p_type="All";
        }elseif($request->payment_id==1){
            $p_type="Bank";
        }elseif($request->payment_id==2){
            $p_type="bKash";
        }else{
            $p_type="Cash";
        }
        if($request->type=='dept_wise_data'){
            $department =DB::table('departments')->where('id',$request->department_id)->select('departmentName')->first();
            $extraot=DB::SELECT("SELECT attendance.*, 
            dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,
            attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,out_time,attendance.in_time,attendance.date as att_date 
            FROM attendance
            LEFT JOIN employees ON attendance.emp_id=employees.id
            LEFT JOIN designations ON employees.empDesignationId=designations.id
            LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
            LEFT JOIN tblines ON tblines.id=employees.line_id
            LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date
            WHERE attendance.date BETWEEN '$start_date' AND '$end_date' and employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times AND employees.empDepartmentId=$request->department_id and employees.date_of_discontinuation IS NULL AND is_three_shift=0  GROUP BY attendance.emp_id,attendance.date");
        }
        if($request->type=='section_wise_data'){
            $section =DB::table('employees')->where('empSection',$request->section_id)->select('empSection')->first();
            $extraot=DB::SELECT("SELECT attendance.*, 
            dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,
            attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,out_time,attendance.in_time,attendance.date as att_date 
            FROM attendance
             LEFT JOIN employees ON attendance.emp_id=employees.id 
             LEFT JOIN designations ON employees.empDesignationId=designations.id 
             LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
             LEFT JOIN tblines ON tblines.id=employees.line_id
             LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date 
             WHERE attendance.date BETWEEN '$start_date' AND '$end_date' and employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times AND employees.empSection='$request->section_id' and employees.date_of_discontinuation IS NULL AND is_three_shift=0 GROUP BY attendance.emp_id,attendance.date");
        }
        if($request->type=='employee_wise_data'){
            $extraot=DB::SELECT("SELECT attendance.*, 
            dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,
            attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,out_time,attendance.in_time,attendance.date as att_date 
            FROM attendance
            LEFT JOIN employees ON attendance.emp_id=employees.id
            LEFT JOIN designations ON employees.empDesignationId=designations.id 
            LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
            LEFT JOIN tblines ON tblines.id=employees.line_id
            LEFT JOIN tbweekend_regular_day ON  DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date   
            WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empOTStatus=1 AND employees.id=$request->emp_id and attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times and employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' and employees.date_of_discontinuation IS NULL  GROUP BY attendance.emp_id,attendance.date");
        }
        if($request->type=='month_wise_data'){
            $extraot=DB::SELECT("SELECT attendance.*, 
            dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,
            attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,out_time,attendance.in_time,attendance.date as att_date 
            FROM attendance
            LEFT JOIN employees ON attendance.emp_id=employees.id
            LEFT JOIN designations ON employees.empDesignationId=designations.id 
            LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
            LEFT JOIN tblines ON tblines.id=employees.line_id
            LEFT JOIN tbweekend_regular_day ON  DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date   
            WHERE attendance.date BETWEEN '$start_month' AND '$end_month' AND employees.empOTStatus=1  and attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times and employees.empAccStatus=1 and employees.empJoiningDate<='$end_month' and employees.date_of_discontinuation IS NULL  GROUP BY attendance.emp_id,attendance.date");
        }
        $insert_data = []; 
        foreach($extraot as $extraots){   
            $ot_data=[
                'emp_id' =>$extraots->emp_id,
                'out_time'=>$extraots->out_time,
                'exit_time'=>$extraots->exit_times,
                'att_date'=>date('Y-m-d',strtotime($extraots->att_date)),
            ];
            $insert_data[]=$ot_data;
        }
        $insert_data = collect($insert_data);
        $chunks = $insert_data->chunk(3000);
        foreach ($chunks as $chunk)
        {
           \DB::table('salary_ot')->insert($chunk->toArray());
        }
        function roundTime($time){
            $hour=date('G',strtotime($time));
            $minute=date('i',strtotime($time));
            if($minute<25){
                $minute=0;
            }
            elseif ($minute>=25 && $minute<55){
                $minute=.5;
            }
            else{
                $minute=0;
                $hour++;
            }
            $kind=number_format((float)($hour+$minute),1,'.','');
            return $kind;
        }
        $data_ot=DB::SELECT("SELECT  salary_ot.emp_id,out_time,exit_time,att_date,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary
        from salary_ot 
        LEFT JOIN employees ON salary_ot.emp_id=employees.id
        LEFT JOIN designations ON employees.empDesignationId=designations.id 
        LEFT JOIN payroll_salary ON salary_ot.emp_id=payroll_salary.emp_id 
        LEFT JOIN tblines ON tblines.id=employees.line_id 
        WHERE out_time>exit_time");
        $finish_ot=[];
        foreach($data_ot as $datas_ot){
            $tot_ot=roundTime(date('G:i',strtotime($datas_ot->out_time)-strtotime($datas_ot->exit_time)));
            if($tot_ot>2){
                $totals_ot=roundTime(date('G:i',strtotime($datas_ot->out_time)-strtotime($datas_ot->exit_time)))-2;
            }else{
              $totals_ot=0;
            }
            $total_ot=$totals_ot;
            $basic=$datas_ot->basic_salary;
            $ot_rate=round($basic/104,2);
            $ot_amount=$total_ot*$ot_rate;
          $ots_data=[
            'emp_id' =>$datas_ot->emp_id,
            'emp_hour'=>$totals_ot,
            'emp_minute'=>0,
            'basic_salary'=>$datas_ot->basic_salary,
            'total_salary'=>$datas_ot->total_employee_salary,
            'employee_id'=>$datas_ot->employeeId,
            'name'=>$datas_ot->empFirstName,
            'designation'=>$datas_ot->designation,
            'line_no'=>$datas_ot->line_no,
            'friday_hour'=>0,
            'amount'=>$ot_amount,
          ];
          $finish_ot[]=$ots_data;
        }
        $finish_ot = collect($finish_ot); 
        $chunks = $finish_ot->chunk(5000);
        foreach ($chunks as $chunk)
        {
            \DB::table('extra_ot_result')->insert($chunk->toArray());
        }
        if($p_type=="All"){
            $extraotsss=DB::SELECT("SELECT payment_mode,bank_account,extra_ot_result.emp_id,basic_salary,total_salary,employee_id,name,designation,line_no, SUM(emp_hour) as total_hour,SUM(emp_minute) as total_minute,SUM(friday_hour) as friday_hour
            from extra_ot_result
            left join employees on extra_ot_result.emp_id=employees.id
            GROUP BY extra_ot_result.emp_id  ORDER BY employees.employeeId ASC");
            }else{
            $extraotsss=DB::SELECT("SELECT payment_mode,bank_account,extra_ot_result.emp_id,basic_salary,total_salary,employee_id,name,designation,line_no, SUM(emp_hour) as total_hour,SUM(emp_minute) as total_minute,SUM(friday_hour) as friday_hour
            from extra_ot_result
            left join employees on extra_ot_result.emp_id=employees.id
            where payment_mode='$p_type'
            GROUP BY extra_ot_result.emp_id  ORDER BY employees.employeeId ASC");
            }      
         if($request->type=='dept_wise_data'){
             if($request->dept_excel=='dept_excel'){
                $excelName=time()."_extra_ot_Sheet";
                Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_date,$end_date,$department,$p_select) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Extra Overtime Summary");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Extra Overtime Summary');
                    $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_date,$end_date,$department,$p_select) {
                        $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel')
                            ->with('extraotsss',$extraotsss)
                            ->with('request',$request)
                            ->with('start_date',$start_date)
                            ->with('end_date',$end_date)
                            ->with('department',$department)
                            ->with('p_select',$p_select);
                    });
                })->download('xlsx');
             }else{
                return view('report.payroll.overtime.extra_overtime_report_print',compact('extraotsss','request','start_date','end_date','department','p_select'));
             }
         }
         if($request->type=='section_wise_data'){
            if($request->section_excel=='section_excel'){
                $excelName=time()."_extra_ot_Sheet";
                Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_date,$end_date,$section,$p_select) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Extra Overtime Summary");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Extra Overtime Summary');
                    $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_date,$end_date,$section,$p_select) {
                        $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel')
                            ->with('extraotsss',$extraotsss)
                            ->with('request',$request)
                            ->with('start_date',$start_date)
                            ->with('end_date',$end_date)
                            ->with('section',$section)
                            ->with('p_select',$p_select);
                    });
                })->download('xlsx');
             }else{
                return view('report.payroll.overtime.extra_overtime_report_print',compact('extraotsss','request','start_date','end_date','section','p_select'));
             }
         }
         if($request->type=='employee_wise_data'){
            if($request->employee_excel=='employee_excel'){
                $excelName=time()."_extra_ot_Sheet";
                Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_date,$end_date,$p_select) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Extra Overtime Summary");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Extra Overtime Summary');
                    $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_date,$end_date,$p_select) {
                        $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel')
                            ->with('extraotsss',$extraotsss)
                            ->with('request',$request)
                            ->with('start_date',$start_date)
                            ->with('end_date',$end_date)
                            ->with('p_select',$p_select);
                    });
                })->download('xlsx');
             }else{
                return view('report.payroll.overtime.extra_overtime_report_print',compact('extraotsss','request','start_date','end_date','p_select'));
             }
         }
         if($request->type=='month_wise_data'){
            if($request->month_excel=='month_excel'){
                $excelName=time()."_extra_ot_Sheet";
                Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_month,$end_month,$p_select) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Extra Overtime Summary");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Extra Overtime Summary');
                    $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_month,$end_month,$p_select) {
                        $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel')
                            ->with('extraotsss',$extraotsss)
                            ->with('request',$request)
                            ->with('start_month',$start_month)
                            ->with('p_select',$p_select)
                            ->with('end_month',$end_month);
                    });
                })->download('xlsx');
             }else{
                return view('report.payroll.overtime.extra_overtime_report_print',compact('extraotsss','request','start_month','end_month','p_select'));
             }
         }
    }


    public function extraovertimeReportPrint(Request $request){
        $delete=DB::table('extra_ot_result')->delete();
        $start_date=date('Y-m-d',strtotime($request->start_date));
        $end_date=date('Y-m-d',strtotime($request->end_date));

        if($request->type=='dept_wise_data'){
            $department =DB::table('departments')->where('id',$request->department_id)->select('departmentName')->first();
            $extraot=DB::SELECT("SELECT
            dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,HOUR(attendance.in_time) as in_time_hour_friday,
            attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance_setup.exit_time AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance_setup.exit_time) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance_setup.exit_time) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date 
            FROM attendance
            LEFT JOIN employees ON attendance.emp_id=employees.id
            LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
            LEFT JOIN designations ON employees.empDesignationId=designations.id
            LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
            LEFT JOIN tblines ON tblines.id=employees.line_id
            LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date
            WHERE attendance.date BETWEEN '$start_date' AND '$end_date' and employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance_setup.exit_time) AND employees.empDepartmentId=$request->department_id and employees.date_of_discontinuation IS NULL  GROUP BY attendance.emp_id,attendance.date");
        }

        if($request->type=='section_wise_data'){
            $section =DB::table('employees')->where('empSection',$request->section_id)->select('empSection')->first();
            $extraot=DB::SELECT("SELECT
            dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day, HOUR(attendance.in_time) as in_time_hour_friday,
            attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance_setup.exit_time AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance_setup.exit_time) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance_setup.exit_time) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM attendance
             LEFT JOIN employees ON attendance.emp_id=employees.id 
             LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
             LEFT JOIN designations ON employees.empDesignationId=designations.id 
             LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
             LEFT JOIN tblines ON tblines.id=employees.line_id
             LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date 
             WHERE attendance.date BETWEEN '$start_date' AND '$end_date' and employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance_setup.exit_time) AND employees.empSection='$request->section_id' and employees.date_of_discontinuation IS NULL  GROUP BY attendance.emp_id,attendance.date");
        }

        if($request->type=='employee_wise_data'){
            $extraot=DB::SELECT("SELECT
            dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,attendance.out_time, attendance.in_time, HOUR(attendance.in_time) as in_time_hour_friday, attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance_setup.exit_time AS exit_time,attendance.out_time as out_time,HOUR(attendance_setup.exit_time) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance_setup.exit_time) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM attendance
            LEFT JOIN employees ON attendance.emp_id=employees.id
            LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
            LEFT JOIN designations ON employees.empDesignationId=designations.id 
            LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
            LEFT JOIN tblines ON tblines.id=employees.line_id
            LEFT JOIN tbweekend_regular_day ON  DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date   
            WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empOTStatus=1 AND employees.id=$request->emp_id and attendance.in_time!=attendance.out_time and employees.empAccStatus=1 and time(attendance.out_time)>time(attendance_setup.exit_time) and employees.empJoiningDate<='$end_date' and employees.date_of_discontinuation IS NULL  GROUP BY attendance.emp_id,attendance.date");
        }

        $f_ot=0;
        $ot_minute=0;
        $data=0;
        $minute=0;
        $insert_data=[];
        foreach($extraot as $extraots){
            $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;

            if($ot_hour>=2 && !$extraots->friday_attendance){
                $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time-2;
            }
            else{
                $ot_hours=0;
            }


            //friday or regular day condition
            if($extraots->friday_attendance){
                if(date('Y-m-d',strtotime($extraots->att_date))==$extraots->regular_day){
                    $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->exit_time)));
//                    return $f_ot;
                    if($f_ot>2){
                        $f_ot=$f_ot-2;
                    }
                    else{
                        $f_ot=0;
                    }

//                   $f_ot=($extraots->hour_out_time-$extraots->hour_exit_time)-2;
                }

                if(date('Y-m-d',strtotime($extraots->att_date))!=$extraots->regular_day){
//                   dd($extraots);
                    if(strtotime($extraots->in_time)<strtotime("8:00")){
                        $extraots->in_time="8:00";
                    }
                    $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->in_time)));
                    $f_ot=(int)$f_ot-1;
//                   return $f_ot;
                }

            }
            else{
                $f_ot=0;
            }

            //friday or regular day condition end




            //without friday or regular day minute condition
            if($ot_hour>=2 && !$extraots->friday_attendance){
                $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
                if($ot_minute<25){
                    $minute=0;
                }
                if($ot_minute>=25 && $ot_minute<55){
                    $minute=.5;
                }
                if($ot_minute>=55){
                    $minute=1;
                }
            }
            else{
                $minute=0;
            }
            //without friday or regular day minute condition end

            $total_ot=$ot_hours+$minute+$f_ot;
            $basic=$extraots->basic_salary;
            $ot_rate=round($basic/104,2);
            $ot_amount= $total_ot*$ot_rate;
            $insert=[
                'emp_id' =>$extraots->emp_id,
                'emp_hour'=>$ot_hours,
                'emp_minute'=>$minute,
                'basic_salary'=>$extraots->basic_salary,
                'total_salary'=>$extraots->total_employee_salary,
                'employee_id'=>$extraots->employeeId,
                'name'=>$extraots->empFirstName,
                'designation'=>$extraots->designation,
                'line_no'=>$extraots->line_no,
                'friday_hour'=>$f_ot,
                'amount'=>$ot_amount,
            ];
            $insert_data[] = $insert;
        }
        $data_insert = collect($insert_data); // Make a collection to use the chunk method

        // it will chunk the dataset in smaller collections containing 500 values each.
        // Play with the value to get best result
        $chunks = $data_insert->chunk(500);

        foreach ($chunks as $chunk)
        {
            \DB::table('extra_ot_result')->insert($chunk->toArray());
        }
        //end big data insert

        $extraotsss=DB::SELECT("SELECT extra_ot_result.emp_id,basic_salary,total_salary,employee_id,name,designation,line_no, SUM(emp_hour) as total_hour,SUM(emp_minute) as total_minute,SUM(friday_hour) as friday_hour
        from extra_ot_result
        GROUP BY extra_ot_result.emp_id");
        if($request->type=='dept_wise_data'){
            return view('report.payroll.overtime.extra_overtime_report_show',compact('extraotsss','request','start_date','end_date','department'));
        }
        if($request->type=='section_wise_data'){
            return view('report.payroll.overtime.extra_overtime_report_show',compact('extraotsss','request','start_date','end_date','section'));
        }
        if($request->type=='employee_wise_data'){
            return view('report.payroll.overtime.extra_overtime_report_show',compact('extraotsss','request','start_date','end_date'));
        }
    }


    //extra overtime for resign employee
    public function extraovertimeReportResignEmployee(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $employee   =DB::table('employees')->where([['employees.empAccStatus','=', 0], ['employees.discon_type','=', 2]])->select('id','empFirstName','empLastName','employeeId')->get();
        return view('report.payroll.overtime.extra_overtime_report_resign',compact('department','section','employee'));
    }

    //extra overtime report show for resign employee
        public function extraovertimeReportshowResignEmployee(Request $request){
            $delete=DB::table('extra_ot_result')->delete();
            $start_date=date('Y-m-d',strtotime($request->start_date));
            $end_date=date('Y-m-d',strtotime($request->end_date));
            $start_month=date('Y-m-01',strtotime($request->ot_month));
            $end_month=date('Y-m-t',strtotime($request->ot_month));
            
            if($request->payment_id==0){
                $p_type="All";
            }elseif($request->payment_id==1){
                $p_type="Bank";
            }elseif($request->payment_id==2){
                $p_type="bKash";
            }else{
                $p_type="Cash";
            }
            $p_select=$request->payment_select;
            if($request->type=='dept_wise_data'){
                $department =DB::table('departments')->where('id',$request->department_id)->select('departmentName')->first();
                $extraot=DB::SELECT("SELECT attendance.*,
                dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,HOUR(attendance.in_time) as in_time_hour_friday,
                attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance.exit_times) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date 
                FROM attendance
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                LEFT JOIN tblines ON tblines.id=employees.line_id
                LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date
                WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=2 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance.exit_times) AND employees.empDepartmentId=$request->department_id AND employees.date_of_discontinuation between '$start_date' AND '$end_date'  GROUP BY attendance.emp_id,attendance.date");
            }
    
            if($request->type=='section_wise_data'){
                $section =DB::table('employees')->where('empSection',$request->section_id)->select('empSection')->first();
                $extraot=DB::SELECT("SELECT attendance.*,
                dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day, HOUR(attendance.in_time) as in_time_hour_friday,
                attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance.exit_times) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM attendance
                 LEFT JOIN employees ON attendance.emp_id=employees.id 
                 LEFT JOIN designations ON employees.empDesignationId=designations.id 
                 LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
                 LEFT JOIN tblines ON tblines.id=employees.line_id
                 LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date 
                 WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=2 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance.exit_times) AND employees.empSection='$request->section_id' AND employees.date_of_discontinuation between '$start_date' AND '$end_date'  GROUP BY attendance.emp_id,attendance.date");
            }
    
            if($request->type=='employee_wise_data'){
                $extraot=DB::SELECT("SELECT attendance.*,
                dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,attendance.out_time, attendance.in_time, HOUR(attendance.in_time) as in_time_hour_friday, attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time,HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance.exit_times) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM attendance
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id 
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
                LEFT JOIN tblines ON tblines.id=employees.line_id
                LEFT JOIN tbweekend_regular_day ON  DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date   
                WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=2 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance.exit_times) AND employees.id=$request->emp_id AND employees.date_of_discontinuation between '$start_date' AND '$end_date'  GROUP BY attendance.emp_id,attendance.date");
            }
            
            if($request->type=='month_wise_data'){
                $extraot=DB::SELECT("SELECT attendance.*,
                dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,HOUR(attendance.in_time) as in_time_hour_friday,
                attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance.exit_times) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date 
                FROM attendance
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                LEFT JOIN tblines ON tblines.id=employees.line_id
                LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date
                WHERE attendance.date BETWEEN '$start_month' AND '$end_month' AND employees.empAccStatus=0 AND employees.discon_type=2 AND employees.empJoiningDate<='$end_month' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance.exit_times)  AND employees.date_of_discontinuation between '$start_month' AND '$end_month'  GROUP BY attendance.emp_id,attendance.date");
            }
    
                $f_ot=0;
                $ot_minute=0;
                $data=0;
                $minute=0;
                foreach($extraot as $extraots){   
                $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;
    
                if($ot_hour>=2 && !$extraots->friday_attendance){
                    $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time-2;
                }
                else{
                    $ot_hours=0;
                }
    
    
                //friday or regular day condition
                if($extraots->friday_attendance){
                   if(date('Y-m-d',strtotime($extraots->att_date))==$extraots->regular_day){
                       $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->exit_time)));
    //                    return $f_ot;
                        if($f_ot>2){
                            $f_ot=$f_ot-2;
                        }
                        else{
                            $f_ot=0;
                        }
    
    //                   $f_ot=($extraots->hour_out_time-$extraots->hour_exit_time)-2;
                   }
    
                   if(date('Y-m-d',strtotime($extraots->att_date))!=$extraots->regular_day){
    //                   dd($extraots);
                       if(strtotime($extraots->in_time)<strtotime("8:00")){
                           $extraots->in_time="8:00";
                       }
                        $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->in_time)));
                        $f_ot=(int)$f_ot-1;
    //                   return $f_ot;
                   }
                }
                else{
                    $f_ot=0; 
                }
                //friday or regular day condition end 
    
    
    
    
              //without friday or regular day minute condition
                if($ot_hour>=2 && !$extraots->friday_attendance){
                    $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
                    if($ot_minute<25){
                        $minute=0;
                    }
                    if($ot_minute>=25 && $ot_minute<55){
                        $minute=.5;
                    }
                    if($ot_minute>=55){
                        $minute=1;
                    }
                }
                else{
                    $minute=0;
                }
             //without friday or regular day minute condition end
    
              $total_ot=$ot_hours+$minute+$f_ot;
              $basic=$extraots->basic_salary;
              $ot_rate=round($basic/104,2);
              $ot_amount= $total_ot*$ot_rate;
    
                $insert[]=[
                    'emp_id' =>$extraots->emp_id,
                    'emp_hour'=>$ot_hours,
                    'emp_minute'=>$minute,
                    'basic_salary'=>$extraots->basic_salary,
                    'total_salary'=>$extraots->total_employee_salary,
                    'employee_id'=>$extraots->employeeId,
                    'name'=>$extraots->empFirstName,
                    'designation'=>$extraots->designation,
                    'line_no'=>$extraots->line_no,
                    'friday_hour'=>$f_ot,
                    'amount'=>$ot_amount,
                ];    
            }
    //            return $insert;
            if($data%550==0){
                if(!empty($insert)){
                    $insertData = DB::table('extra_ot_result')->insert($insert);
                    unset($insert);
                }
            }
            $data++;

            if($p_type=="All"){
                $extraotsss=DB::SELECT("SELECT payment_mode,bank_account,extra_ot_result.emp_id,basic_salary,total_salary,employee_id,name,designation,line_no, SUM(emp_hour) as total_hour,SUM(emp_minute) as total_minute,SUM(friday_hour) as friday_hour
                from extra_ot_result
                left join employees on extra_ot_result.emp_id=employees.id
                GROUP BY extra_ot_result.emp_id");
            }else{
                $extraotsss=DB::SELECT("SELECT payment_mode,bank_account,extra_ot_result.emp_id,basic_salary,total_salary,employee_id,name,designation,line_no, SUM(emp_hour) as total_hour,SUM(emp_minute) as total_minute,SUM(friday_hour) as friday_hour
                from extra_ot_result
                left join employees on extra_ot_result.emp_id=employees.id
                where payment_mode='$p_type'
                GROUP BY extra_ot_result.emp_id");
            }
        
             if($request->type=='dept_wise_data'){
                 if($request->dept_excel=='dept_excel'){
                    $excelName=time()."_extra_ot_Sheet";
                    Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_date,$end_date,$department,$p_select) {
                        // Set the title
                        $name=Auth::user()->name;
                        $excel->setTitle("Extra Overtime Summary");
                        // Chain the setters
                        $excel->setCreator($name);
                        $excel->setDescription('Extra Overtime Summary');
                        $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_date,$end_date,$department,$p_select) {
                            $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel_resigned')
                                ->with('extraotsss',$extraotsss)
                                ->with('request',$request)
                                ->with('start_date',$start_date)
                                ->with('end_date',$end_date)
                                ->with('p_select',$p_select)
                                ->with('department',$department);
                        });
                    })->download('xlsx');
                 }else{
               return view('report.payroll.overtime.extra_overtime_report_show_resign_employees',compact('extraotsss','request','start_date','end_date','department','p_select'));
             }
            }
             if($request->type=='section_wise_data'){
                 if($request->section_excel=='section_excel'){
                    $excelName=time()."_extra_ot_Sheet";
                    Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_date,$end_date,$section,$p_select) {
                        // Set the title
                        $name=Auth::user()->name;
                        $excel->setTitle("Extra Overtime Summary");
                        // Chain the setters
                        $excel->setCreator($name);
                        $excel->setDescription('Extra Overtime Summary');
                        $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_date,$end_date,$section,$p_select) {
                            $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel_resigned')
                                ->with('extraotsss',$extraotsss)
                                ->with('request',$request)
                                ->with('start_date',$start_date)
                                ->with('end_date',$end_date)
                                ->with('section',$section)
                                ->with('p_select',$p_select);
                                
                        });
                    })->download('xlsx');
                 }else{
                return view('report.payroll.overtime.extra_overtime_report_show_resign_employees',compact('extraotsss','request','start_date','end_date','section','p_select'));
             }
            }
             if($request->type=='employee_wise_data'){
                 if($request->emp_excel=='emp_excel'){
                    $excelName=time()."_extra_ot_Sheet";
                    Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_date,$end_date,$p_select) {
                        // Set the title
                        $name=Auth::user()->name;
                        $excel->setTitle("Extra Overtime Summary");
                        // Chain the setters
                        $excel->setCreator($name);
                        $excel->setDescription('Extra Overtime Summary');
                        $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_date,$end_date,$p_select) {
                            $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel_resigned')
                                ->with('extraotsss',$extraotsss)
                                ->with('request',$request)
                                ->with('start_date',$start_date)
                                ->with('end_date',$end_date)
                                ->with('p_select',$p_select);
                        });
                    })->download('xlsx');
                 }else{
                    return view('report.payroll.overtime.extra_overtime_report_show_resign_employees',compact('extraotsss','request','start_date','end_date','p_select'));
                 }
             }
             if($request->type=='month_wise_data'){
                if($request->month_excel=='month_excel'){
                   $excelName=time()."_extra_ot_Sheet";
                   Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_month,$end_month,$p_select) {
                       // Set the title
                       $name=Auth::user()->name;
                       $excel->setTitle("Extra Overtime Summary");
                       // Chain the setters
                       $excel->setCreator($name);
                       $excel->setDescription('Extra Overtime Summary');
                       $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_month,$end_month,$p_select) {
                           $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel_resigned')
                               ->with('extraotsss',$extraotsss)
                               ->with('request',$request)
                               ->with('start_month',$start_month)
                               ->with('end_month',$end_month)
                               ->with('p_select',$p_select); 
                       });
                   })->download('xlsx');
                }else{
                   return view('report.payroll.overtime.extra_overtime_report_show_resign_employees',compact('extraotsss','request','start_month','end_month','p_select'));
                }
            }
        }

    //extra overtime for lefty employee
    public function extraovertimeReportLeftyEmployee(){
        $department =DB::table('departments')->select('id','departmentName')->get();
        $section    =DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        $employee   =DB::table('employees')->where([['employees.empAccStatus','=', 0], ['employees.discon_type','=', 1]])->select('id','empFirstName','empLastName','employeeId')->get();
        return view('report.payroll.overtime.extra_overtime_report_lefty',compact('department','section','employee'));
    }

    //extra overtime report show for lefty employee
        public function extraovertimeReportshowLeftyEmployee(Request $request){
            $delete=DB::table('extra_ot_result')->delete();
            $start_date=date('Y-m-d',strtotime($request->start_date));
            $end_date=date('Y-m-d',strtotime($request->end_date));
            $start_month=date('Y-m-01',strtotime($request->ot_month));
            $end_month=date('Y-m-t',strtotime($request->ot_month));
            if($request->payment_id==0){
                $p_type="All";
            }elseif($request->payment_id==1){
                $p_type="Bank";
            }elseif($request->payment_id==2){
                $p_type="bKash";
            }else{
                $p_type="Cash";
            }
            $p_select=$request->payment_select;
            if($request->type=='dept_wise_data'){
                    $department =DB::table('departments')->where('id',$request->department_id)->select('departmentName')->first();
                    $extraot=DB::SELECT("SELECT attendance.*,
                    dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,HOUR(attendance.in_time) as in_time_hour_friday,
                    attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time,attendance.in_time,HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance.exit_times) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date 
                    FROM attendance
                    LEFT JOIN employees ON attendance.emp_id=employees.id
                    LEFT JOIN designations ON employees.empDesignationId=designations.id
                    LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                    LEFT JOIN tblines ON tblines.id=employees.line_id
                    LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date
                    WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=1 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance.exit_times) AND employees.empDepartmentId=$request->department_id AND employees.date_of_discontinuation between '$start_date' AND '$end_date'  GROUP BY attendance.emp_id,attendance.date");
            }
    
            if($request->type=='section_wise_data'){
                $section =DB::table('employees')->where('empSection',$request->section_id)->select('empSection')->first();
                $extraot=DB::SELECT("SELECT attendance.*,
                dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day, HOUR(attendance.in_time) as in_time_hour_friday,
                attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance.exit_times) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date 
                FROM attendance
                 LEFT JOIN employees ON attendance.emp_id=employees.id 
                 LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
                 LEFT JOIN designations ON employees.empDesignationId=designations.id 
                 LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
                 LEFT JOIN tblines ON tblines.id=employees.line_id
                 LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date 
                 WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=1 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance.exit_times) AND employees.empSection='$request->section_id' AND employees.date_of_discontinuation between '$start_date' AND '$end_date'  GROUP BY attendance.emp_id,attendance.date");
            }
    
            if($request->type=='employee_wise_data'){
                    $extraot=DB::SELECT("SELECT attendance.*,
                    dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,attendance.out_time, attendance.in_time, HOUR(attendance.in_time) as in_time_hour_friday, attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time,HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance.exit_times) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM attendance
                    LEFT JOIN employees ON attendance.emp_id=employees.id
                    LEFT JOIN designations ON employees.empDesignationId=designations.id 
                    LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
                    LEFT JOIN tblines ON tblines.id=employees.line_id
                    LEFT JOIN tbweekend_regular_day ON  DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date   
                    WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=1 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance.exit_times) AND employees.id=$request->emp_id AND employees.date_of_discontinuation between '$start_date' AND '$end_date'  GROUP BY attendance.emp_id,attendance.date");
            }

            if($request->type=='month_wise_data'){
              $extraot=DB::SELECT("SELECT attendance.*,
              dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,HOUR(attendance.in_time) as in_time_hour_friday,
              attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance.exit_times) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date 
              FROM attendance
              LEFT JOIN employees ON attendance.emp_id=employees.id
              LEFT JOIN designations ON employees.empDesignationId=designations.id
              LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
              LEFT JOIN tblines ON tblines.id=employees.line_id
              LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date
              WHERE attendance.date BETWEEN '$start_month' AND '$end_month' AND employees.empAccStatus=0 AND employees.discon_type=1 AND employees.empJoiningDate<='$end_month' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance.exit_times) AND employees.date_of_discontinuation between '$start_month' AND '$end_month'  GROUP BY attendance.emp_id,attendance.date");
            }
                $f_ot=0;
                $ot_minute=0;
                $data=0;
                $minute=0;
                foreach($extraot as $extraots){   
                $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;
    
                if($ot_hour>=2 && !$extraots->friday_attendance){
                    $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time-2;
                }
                else{
                    $ot_hours=0;
                }
                //friday or regular day condition
                if($extraots->friday_attendance){
                   if(date('Y-m-d',strtotime($extraots->att_date))==$extraots->regular_day){
                       $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->exit_time)));
    //                    return $f_ot;
                        if($f_ot>2){
                            $f_ot=$f_ot-2;
                        }
                        else{
                            $f_ot=0;
                        }
    
    //                   $f_ot=($extraots->hour_out_time-$extraots->hour_exit_time)-2;
                   }
    
                   if(date('Y-m-d',strtotime($extraots->att_date))!=$extraots->regular_day){
    //                   dd($extraots);
                       if(strtotime($extraots->in_time)<strtotime("8:00")){
                           $extraots->in_time="8:00";
                       }
                        $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->in_time)));
                        $f_ot=(int)$f_ot-1;
    //                   return $f_ot;
                   }
                }
                else{
                    $f_ot=0; 
                }
    
                //friday or regular day condition end 
    
              //without friday or regular day minute condition
                if($ot_hour>=2 && !$extraots->friday_attendance){
                    $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
                    if($ot_minute<25){
                        $minute=0;
                    }
                    if($ot_minute>=25 && $ot_minute<55){
                        $minute=.5;
                    }
                    if($ot_minute>=55){
                        $minute=1;
                    }
                }
                else{
                    $minute=0;
                }
             //without friday or regular day minute condition end
    
              $total_ot=$ot_hours+$minute+$f_ot;
              $basic=$extraots->basic_salary;
              $ot_rate=round($basic/104,2);
              $ot_amount= $total_ot*$ot_rate;
    
                $insert[]=[
                    'emp_id' =>$extraots->emp_id,
                    'emp_hour'=>$ot_hours,
                    'emp_minute'=>$minute,
                    'basic_salary'=>$extraots->basic_salary,
                    'total_salary'=>$extraots->total_employee_salary,
                    'employee_id'=>$extraots->employeeId,
                    'name'=>$extraots->empFirstName,
                    'designation'=>$extraots->designation,
                    'line_no'=>$extraots->line_no,
                    'friday_hour'=>$f_ot,
                    'amount'=>$ot_amount,
                ];    
            }
    //            return $insert;
            if($data%550==0){
                if(!empty($insert)){
                    $insertData = DB::table('extra_ot_result')->insert($insert);
                    unset($insert);
                }
            }
            $data++;
            if($p_type=="All"){
                $extraotsss=DB::SELECT("SELECT payment_mode,bank_account,extra_ot_result.emp_id, basic_salary, total_salary, employee_id,name, designation, line_no, SUM(emp_hour) as total_hour, SUM(emp_minute) as total_minute, SUM(friday_hour) as friday_hour
                 from extra_ot_result 
                 left join employees on extra_ot_result.emp_id=employees.id
                 GROUP BY extra_ot_result.emp_id
                 ");
            }else{
                $extraotsss=DB::SELECT("SELECT payment_mode,bank_account,extra_ot_result.emp_id, basic_salary, total_salary, employee_id,name, designation, line_no, SUM(emp_hour) as total_hour, SUM(emp_minute) as total_minute, SUM(friday_hour) as friday_hour
                from extra_ot_result 
                left join employees on extra_ot_result.emp_id=employees.id
                where payment_mode='$p_type'
                GROUP BY extra_ot_result.emp_id
                ");
            }
            
             
            if($request->type=='dept_wise_data'){
                if($request->dept_excel=='dept_excel'){
                $excelName=time()."_extra_ot_Sheet";
                Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_date,$end_date,$department,$p_select) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Extra Overtime Summary");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Extra Overtime Summary');
                    $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_date,$end_date,$department,$p_select) {
                        $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel_lefty')
                            ->with('extraotsss',$extraotsss)
                            ->with('request',$request)
                            ->with('start_date',$start_date)
                            ->with('end_date',$end_date)
                            ->with('department',$department)
                            ->with('p_select',$p_select);
                            
                    });
                })->download('xlsx');
                }else{
               return view('report.payroll.overtime.extra_overtime_report_show_lefty_employees',compact('extraotsss','request','start_date','end_date','department','p_select'));
             }
            }

             if($request->type=='section_wise_data'){
                if($request->dept_excel=='section_excel'){
                 
                $excelName=time()."_extra_ot_Sheet";
                Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_date,$end_date,$section,$p_select) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Extra Overtime Summary");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Extra Overtime Summary');
                    $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_date,$end_date,$section,$p_select) {
                        $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel_lefty')
                            ->with('extraotsss',$extraotsss)
                            ->with('request',$request)
                            ->with('start_date',$start_date)
                            ->with('end_date',$end_date)
                            ->with('section',$section) 
                            ->with('p_select',$p_select);
                    });
                })->download('xlsx');
                }else{
                return view('report.payroll.overtime.extra_overtime_report_show_lefty_employees',compact('extraotsss','request','start_date','end_date','section','p_select'));
             }
            }

             if($request->type=='employee_wise_data'){
               if($request->emp_excel=='emp_excel'){
                $excelName=time()."_extra_ot_Sheet";
                Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_date,$end_date,$p_select) {
                    // Set the title
                    $name=Auth::user()->name;
                    $excel->setTitle("Extra Overtime Summary");
                    // Chain the setters
                    $excel->setCreator($name);
                    $excel->setDescription('Extra Overtime Summary');
                    $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_date,$end_date,$p_select) {
                        $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel_lefty')
                            ->with('extraotsss',$extraotsss)
                            ->with('request',$request)
                            ->with('start_date',$start_date)
                            ->with('end_date',$end_date)
                            ->with('p_select',$p_select);
                    });
                })->download('xlsx');
               }else{
                return view('report.payroll.overtime.extra_overtime_report_show_lefty_employees',compact('extraotsss','request','start_date','end_date','p_select'));
             }
            } 
            
            if($request->type=='month_wise_data'){
                if($request->month_excel=='month_excel'){
                    $excelName=time()."_extra_ot_Sheet";
                    Excel::create("$excelName", function($excel) use ($extraotsss,$request,$start_month,$end_month,$p_select) {
                        // Set the title
                        $name=Auth::user()->name;
                        $excel->setTitle("Extra Overtime Summary");
                        // Chain the setters
                        $excel->setCreator($name);
                        $excel->setDescription('Extra Overtime Summary');
                        $excel->sheet('Sheet 1', function ($sheet) use ($extraotsss,$request,$start_month,$end_month,$p_select) {
                            $sheet->loadView('report.payroll.overtime.extra_overtime_report_print_excel_lefty')
                                ->with('extraotsss',$extraotsss)
                                ->with('request',$request)
                                ->with('start_month',$start_month)
                                ->with('p_select',$p_select)
                                ->with('end_month',$end_month);
                        });
                    })->download('xlsx');
                }else{
                    return view('report.payroll.overtime.extra_overtime_report_show_lefty_employees',compact('extraotsss','request','start_month','end_month','p_select'));
                }
            }    

        }


     //daily overtime view
     public function daileOvertime(){
        $department=DB::table('departments')->select('id','departmentName')->get();
        $section=DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        return view("report.payroll.overtime.daily_overtime",compact('department','section'));
     }

     //daily overtime show
     public function dailyOvertimeShow(Request $request){
        $delete=DB::table('extra_ot_result')->delete();
        DB::table('tb_total_present')->delete();
        $start_date=date('Y-m-d',strtotime($request->daily_ot_date));
        $end_date=date('Y-m-t',strtotime($request->daily_ot_date));
        $monthss=date('Y-m-d',strtotime($request->daily_ot_date));
        if($request->department_id=='all'){
        $test_salary=0;    
        $extraot=DB::SELECT("SELECT
        dayname(attendance.date)='Friday' as friday_attendance,employees.empDepartmentId,tbweekend_regular_day.weekend_date as regular_day,HOUR(attendance.in_time) as in_time_hour_friday,
        attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance_setup.exit_time AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance_setup.exit_time) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance_setup.exit_time) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date 
        FROM attendance
        LEFT JOIN employees ON attendance.emp_id=employees.id
        LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
        LEFT JOIN designations ON employees.empDesignationId=designations.id
        LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
        LEFT JOIN tblines ON tblines.id=employees.line_id
        LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date
        WHERE attendance.date ='$start_date' AND employees.empAccStatus=1 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance_setup.exit_time) 
        GROUP BY attendance.emp_id,attendance.date,employees.empDepartmentId");
  
                $f_ot=0;
                $ot_minute=0;
                $data=0;
                $minute=0;
              //start big data insert
                $insert_data = [];
                foreach($extraot as $extraots){   
                    $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;
            
                    if($ot_hour && !$extraots->friday_attendance){
                        $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time;
                    }
                    else{
                        $ot_hours=0;
                    }
            
                            //friday or regular day condition
                            if($extraots->friday_attendance){
                                if(date('Y-m-d',strtotime($extraots->att_date))==$extraots->regular_day){
                                    $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->exit_time)));
                     //                    return $f_ot;
                                     if($f_ot){
                                         $f_ot=$f_ot;
                                     }
                                     else{
                                         $f_ot=0;
                                     }
                     //                   $f_ot=($extraots->hour_out_time-$extraots->hour_exit_time)-2;
                                }
                     
                                if(date('Y-m-d',strtotime($extraots->att_date))!=$extraots->regular_day){
                     //                   dd($extraots);
                                    if(strtotime($extraots->in_time)<strtotime("8:00")){
                                        $extraots->in_time="8:00";
                                    }
                                     $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->in_time)));
                                     $f_ot=(int)$f_ot-1;
                     //                   return $f_ot;
                                }
                                
                             }
                             else{
                                 $f_ot=0; 
                             }
                     
                             //friday or regular day condition end 
                     
                           //without friday or regular day minute condition
                             if($ot_hour && !$extraots->friday_attendance){
                                 $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
                                 if($ot_minute<25){
                                     $minute=0;
                                 }
                                 if($ot_minute>=25 && $ot_minute<55){
                                     $minute=.5;
                                 }
                                 if($ot_minute>=55){
                                     $minute=1;
                                 }
                             }
                             else{
                                 $minute=0;
                             }
                          //without friday or regular day minute condition end

                    $total_ot=$ot_hours+$minute+$f_ot;
                    $basic=$extraots->basic_salary;
                    $ot_rate=round($basic/104,2);
                    $ot_amount= $total_ot*$ot_rate;
                    $data = [
                        'emp_id' =>$extraots->emp_id,
                        'emp_hour'=>$ot_hours,
                        'emp_minute'=>$minute,
                        'basic_salary'=>$extraots->basic_salary,
                        'total_salary'=>$extraots->total_employee_salary,
                        'employee_id'=>$extraots->employeeId,
                        'name'=>$extraots->empFirstName,
                        'designation'=>$extraots->designation,
                        'department'=>$extraots->empDepartmentId,              
                        'line_no'=>$extraots->line_no,
                        'friday_hour'=>$f_ot,
                        'amount'=>$ot_amount,
                    ];
                    $insert_data[] = $data;
                }
                $insert_data = collect($insert_data); // Make a collection to use the chunk method
                
                // it will chunk the dataset in smaller collections containing 500 values each. 
                // Play with the value to get best result
                $chunks = $insert_data->chunk(500);
                
                foreach ($chunks as $chunk)
                {
                   \DB::table('extra_ot_result')->insert($chunk->toArray());
                }
               //end big data insert

                if($request->department_id=='all'){
                      //basic and gross of employee
                      $salary_total=DB::SELECT("SELECT extra_ot_result.department,SUM(DISTINCT(extra_ot_result.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_result.total_salary)) as gross_s
                      FROM extra_ot_result 
                      GROUP BY extra_ot_result.emp_id");
                      foreach($salary_total as $totals){
                          $insert[]=[
                              'emp_id'=>$totals->department,
                              'total_present' =>$totals->basic_s,
                              'total_absent'=>$totals->gross_s,
                          ]; 
                      }
                      if($test_salary%550==0){
                          if(!empty($insert)){
                              $insertData = DB::table('tb_total_present')->insert($insert);
                              unset($insert);
                          }
                      }
                      $test_salary++;
                    //basic and gross of employee
                    $total=DB::SELECT("SELECT departments.departmentName,extra_ot_result.department,SUM(extra_ot_result.emp_hour) as total_hour,SUM(extra_ot_result.emp_minute) as total_minute,SUM(extra_ot_result.friday_hour) as friday_hour,SUM(extra_ot_result.amount) as total_amount
                    from extra_ot_result
                    left join departments on extra_ot_result.department=departments.id
                    GROUP BY extra_ot_result.department");
                   return view('report.payroll.overtime.daily_overtime_show_department_employee',compact('total','monthss'));
                }
          }

          if($request->section_id=='all'){
            $test_salary=0;
            $extraot=DB::SELECT("SELECT
            dayname(attendance.date)='Friday' as friday_attendance,employees.empsection,tbweekend_regular_day.weekend_date as regular_day, HOUR(attendance.in_time) as in_time_hour_friday,
            attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance_setup.exit_time AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance_setup.exit_time) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance_setup.exit_time) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM attendance
            LEFT JOIN employees ON attendance.emp_id=employees.id 
            LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
            LEFT JOIN designations ON employees.empDesignationId=designations.id 
            LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
            LEFT JOIN tblines ON tblines.id=employees.line_id
            LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date 
            WHERE attendance.date='$start_date' AND employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance_setup.exit_time)
            GROUP BY attendance.emp_id,attendance.date,employees.empsection");      
                $f_ot=0;
                $ot_minute=0;
                $data=0;
                $minute=0;
             
              //start big data insert
                $insert_data = [];
                foreach($extraot as $extraots){   
                    $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;
                    if($ot_hour && !$extraots->friday_attendance){
                        $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time;
                    }
                    else{
                        $ot_hours=0;
                    }
                    //friday or regular day condition
                    if($extraots->friday_attendance){
                       if(date('Y-m-d',strtotime($extraots->att_date))==$extraots->regular_day){
                           $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->exit_time)));
            //                    return $f_ot;
                            if($f_ot){
                                $f_ot=$f_ot;
                            }
                            else{
                                $f_ot=0;
                            }
            //                   $f_ot=($extraots->hour_out_time-$extraots->hour_exit_time)-2;
                       }
            
                       if(date('Y-m-d',strtotime($extraots->att_date))!=$extraots->regular_day){
            //                   dd($extraots);
                           if(strtotime($extraots->in_time)<strtotime("8:00")){
                               $extraots->in_time="8:00";
                           }
                            $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->in_time)));
                            $f_ot=(int)$f_ot-1;
            //                   return $f_ot;
                       }
                       
                    }
                    else{
                        $f_ot=0; 
                    }
            
                    //friday or regular day condition end 
            
                  //without friday or regular day minute condition
                    if($ot_hour && !$extraots->friday_attendance){
                        $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
                        if($ot_minute<25){
                            $minute=0;
                        }
                        if($ot_minute>=25 && $ot_minute<55){
                            $minute=.5;
                        }
                        if($ot_minute>=55){
                            $minute=1;
                        }
                    }
                    else{
                        $minute=0;
                    }
                 //without friday or regular day minute condition end

                    $total_ot=$ot_hours+$minute+$f_ot;
                    $basic=$extraots->basic_salary;
                    $ot_rate=round($basic/104,2);
                    $ot_amount= $total_ot*$ot_rate;
                    $data = [
                        'emp_id' =>$extraots->emp_id,
                        'emp_hour'=>$ot_hours,
                        'emp_minute'=>$minute,
                        'basic_salary'=>$extraots->basic_salary,
                        'total_salary'=>$extraots->total_employee_salary,
                        'employee_id'=>$extraots->employeeId,
                        'name'=>$extraots->empFirstName,
                        'designation'=>$extraots->designation,
                        'section'=>$extraots->empsection,              
                        'line_no'=>$extraots->line_no,
                        'friday_hour'=>$f_ot,
                        'amount'=>$ot_amount,
                    ];
                
                    $insert_data[] = $data;
                }
                
                $insert_data = collect($insert_data); // Make a collection to use the chunk method
                
                // it will chunk the dataset in smaller collections containing 500 values each. 
                // Play with the value to get best result
                $chunks = $insert_data->chunk(500);
                
                foreach ($chunks as $chunk)
                {
                   \DB::table('extra_ot_result')->insert($chunk->toArray());
                }
               //end big data insert

                if($request->section_id=='all'){
                    //basic and gross of employee
                    $salary_total=DB::SELECT("SELECT section,SUM(DISTINCT(extra_ot_result.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_result.total_salary)) as gross_s
                    FROM extra_ot_result 
                    GROUP BY extra_ot_result.emp_id");
                    foreach($salary_total as $totals){
                        $insert[]=[
                            'emp_id'=>$totals->section,
                            'total_present' =>$totals->basic_s,
                            'total_absent'=>$totals->gross_s,
                        ]; 
                    }
                    if($test_salary%550==0){
                        if(!empty($insert)){
                            $insertData = DB::table('tb_total_present')->insert($insert);
                            unset($insert);
                        }
                    }
                    $test_salary++;
                  //basic and gross of employee
                    $total_basic_gross=DB::SELECT("SELECT SUM(total_present) as total_basic,SUM(total_absent) as total_gross FROM tb_total_present group by emp_id"); 
                    $total=DB::SELECT("SELECT extra_ot_result.department,extra_ot_result.section,SUM(extra_ot_result.total_salary) as total_gross,SUM(extra_ot_result.basic_salary) as total_basic,SUM(extra_ot_result.emp_hour) as total_hour,SUM(extra_ot_result.emp_minute) as total_minute,SUM(extra_ot_result.friday_hour) as friday_hour,SUM(extra_ot_result.amount) as total_amount
                    from extra_ot_result
                    GROUP BY extra_ot_result.section");
                return view('report.payroll.overtime.daily_overtime_show_section_employee',compact('total','monthss','total_basic_gross'));
                }
          }

        if($request->type=='department_wise_data'){
            $department =DB::table('departments')->where('id',$request->department_id)->select('departmentName')->first();
            $extraot=DB::SELECT("SELECT
            dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day,HOUR(attendance.in_time) as in_time_hour_friday,
            attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance_setup.exit_time AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance_setup.exit_time) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance_setup.exit_time) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date 
            FROM attendance
            LEFT JOIN employees ON attendance.emp_id=employees.id
            LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
            LEFT JOIN designations ON employees.empDesignationId=designations.id
            LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
            LEFT JOIN tblines ON tblines.id=employees.line_id
            LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date
            WHERE attendance.date='$start_date' AND employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance_setup.exit_time) AND employees.empDepartmentId=$request->department_id  GROUP BY attendance.emp_id,attendance.date");
          
        }

        if($request->type=='section_wise_data'){
            $section=DB::table('employees')->where('empSection',$request->section_id)->select('empSection')->first();
            $extraot=DB::SELECT("SELECT
            dayname(attendance.date)='Friday' as friday_attendance,tbweekend_regular_day.weekend_date as regular_day, HOUR(attendance.in_time) as in_time_hour_friday,
            attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance_setup.exit_time AS exit_time,attendance.out_time as out_time, attendance.in_time, HOUR(attendance_setup.exit_time) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance_setup.exit_time) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM attendance
             LEFT JOIN employees ON attendance.emp_id=employees.id 
             LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
             LEFT JOIN designations ON employees.empDesignationId=designations.id 
             LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id 
             LEFT JOIN tblines ON tblines.id=employees.line_id
             LEFT JOIN tbweekend_regular_day ON DATE_FORMAT(attendance.date,'%Y-%m-%d')=tbweekend_regular_day.weekend_date 
             WHERE attendance.date='$start_date' AND '$end_date' AND employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND time(attendance.out_time)>time(attendance_setup.exit_time) AND employees.empSection='$request->section_id'  GROUP BY attendance.emp_id,attendance.date");
        }

        $f_ot=0;
        $ot_minute=0;
        $data=0;
        $total_basic=0;
        $total_gross=0;
        $minute=0;
        $insert_data=[]; 
        foreach($extraot as $extraots){   
        $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;

        if($ot_hour && !$extraots->friday_attendance){
            $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time;
        }
        else{
            $ot_hours=0;
        }
        //friday or regular day condition
        if($extraots->friday_attendance){
           if(date('Y-m-d',strtotime($extraots->att_date))==$extraots->regular_day){
               $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->exit_time)));
//                    return $f_ot;
                if($f_ot){
                    $f_ot=$f_ot;
                }
                else{
                    $f_ot=0;
                }
//                   $f_ot=($extraots->hour_out_time-$extraots->hour_exit_time)-2;
           }

           if(date('Y-m-d',strtotime($extraots->att_date))!=$extraots->regular_day){
//                   dd($extraots);
               if(strtotime($extraots->in_time)<strtotime("8:00")){
                   $extraots->in_time="8:00";
               }
                $f_ot=$this->roundTime(date('G:i', strtotime($extraots->out_time) - strtotime($extraots->in_time)));
                $f_ot=(int)$f_ot-1;
//                   return $f_ot;
           }
        }
        else{
            $f_ot=0; 
        }
        //friday or regular day condition end 




      //without friday or regular day minute condition
        if($ot_hour && !$extraots->friday_attendance){
            $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
            if($ot_minute<25){
                $minute=0;
            }
            if($ot_minute>=25 && $ot_minute<55){
                $minute=.5;
            }
            if($ot_minute>=55){
                $minute=1;
            }
        }
        else{
            $minute=0;
        }
     //without friday or regular day minute condition end
            $total_ot=$ot_hours+$minute+$f_ot;
            $basic=$extraots->basic_salary;
            $ot_rate=round($basic/104,2);
            $ot_amount= $total_ot*$ot_rate;
            $insert=[
                'emp_id' =>$extraots->emp_id,
                'emp_hour'=>$ot_hours,
                'emp_minute'=>$minute,
                'basic_salary'=>$extraots->basic_salary,
                'total_salary'=>$extraots->total_employee_salary,
                'employee_id'=>$extraots->employeeId,
                'name'=>$extraots->empFirstName,
                'designation'=>$extraots->designation,
                'department'=>$request->department_id,
                'section'=>$request->section_id,               
                'line_no'=>$extraots->line_no,
                'friday_hour'=>$f_ot,
                'amount'=>$ot_amount,
            ];    

            $insert_data[] = $insert;

        }
        $data_insert = collect($insert_data); // Make a collection to use the chunk method
                
        // it will chunk the dataset in smaller collections containing 500 values each. 
        // Play with the value to get best result
        $chunks = $data_insert->chunk(500);
        
        foreach ($chunks as $chunk)
        {
           \DB::table('extra_ot_result')->insert($chunk->toArray());
        }
       //end big data insert


        if($request->type=='department_wise_data'){
            $salary_total=DB::SELECT("SELECT emp_id,SUM(DISTINCT(extra_ot_result.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_result.total_salary)) as gross_s,SUM(emp_hour) as hour,SUM(emp_minute) as minute,SUM(friday_hour) as f_hour
            FROM extra_ot_result 
            GROUP BY extra_ot_result.emp_id");
            //basic and gross of employee
            $insert_data=[]; 
            foreach($salary_total as $totals){
                $insert=[
                    'total_present'=>$totals->basic_s,
                    'total_absent'=>$totals->gross_s,
                    'emp_id'      =>$totals->hour+$totals->minute+$totals->f_hour
                ]; 
                $insert_data[] = $insert;
            }
            $data_insert = collect($insert_data); // Make a collection to use the chunk method
                
            // it will chunk the dataset in smaller collections containing 500 values each. 
            // Play with the value to get best result
            $chunks = $data_insert->chunk(500);
            
            foreach ($chunks as $chunk)
            {
               \DB::table('tb_total_present')->insert($chunk->toArray());
            }
           //end big data insert
            //basic and gross of employee
            $total_basic_gross=DB::SELECT("SELECT SUM(total_present) as total_basic,SUM(total_absent) as total_gross FROM tb_total_present");  
            $total=DB::SELECT("SELECT extra_ot_result.emp_id,extra_ot_result.department,SUM(distinct(extra_ot_result.total_salary)) as total_gross,SUM(distinct(extra_ot_result.basic_salary)) as total_basic,SUM(extra_ot_result.emp_hour) as total_hour,SUM(extra_ot_result.emp_minute) as total_minute,SUM(extra_ot_result.friday_hour) as friday_hour,SUM(extra_ot_result.amount) as total_amount
            from extra_ot_result
            GROUP BY extra_ot_result.department");
           return view('report.payroll.overtime.daily_overtime_dept_section',compact('total','monthss','department','total_basic_gross'));
        }


        

        if($request->type=='section_wise_data'){
            $section_workar=DB::table('employees')->where('empSection',$request->section_id)->count();
            $salary_total=DB::SELECT("SELECT SUM(DISTINCT(extra_ot_result.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_result.total_salary)) as gross_s,SUM(emp_hour) as hour,SUM(emp_minute) as minute,SUM(friday_hour) as f_hour
            FROM extra_ot_result 
            GROUP BY extra_ot_result.emp_id");
            //basic and gross of employee
            $insert_data=[]; 
            foreach($salary_total as $totals){
                $insert=[
                    'total_present'=>$totals->basic_s,
                    'total_absent'=>$totals->gross_s,
                    'emp_id'      =>$totals->hour+$totals->minute+$totals->f_hour
                ]; 
                $insert_data[] = $insert;
            }
            $data_insert = collect($insert_data); // Make a collection to use the chunk method
                
            // it will chunk the dataset in smaller collections containing 500 values each. 
            // Play with the value to get best result
            $chunks = $data_insert->chunk(500);
            
            foreach ($chunks as $chunk)
            {
               \DB::table('tb_total_present')->insert($chunk->toArray());
            }
           //end big data insert
            //basic and gross of employee
            $total_basic_gross=DB::SELECT("SELECT SUM(total_present) as total_basic,SUM(total_absent) as total_gross FROM tb_total_present where emp_id IS NOT NULL"); 
            $total=DB::SELECT("SELECT extra_ot_result.department,extra_ot_result.section,SUM(extra_ot_result.total_salary) as total_gross,SUM(extra_ot_result.basic_salary) as total_basic,SUM(extra_ot_result.emp_hour) as total_hour,SUM(extra_ot_result.emp_minute) as total_minute,SUM(extra_ot_result.friday_hour) as friday_hour,SUM(extra_ot_result.amount) as total_amount
            from extra_ot_result
            GROUP BY extra_ot_result.section");
           return view('report.payroll.overtime.daily_overtime_dept_section',compact('total','monthss','section_workar','section','total_basic_gross'));
        }

     }


    //extra overtime summery page
    public function extraovertimesummery(){
        $department=DB::table('departments')->select('id','departmentName')->get();
        $section=DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        return view("report.payroll.overtime.extra_ot_summery",compact('department','section'));
    }

    //extra ot summery show
    public function extraovertimesummeryshow(Request $request){
        DB::table('extra_ot_result')->delete();
        DB::table('extra_ot_resign_lefty')->delete();
        DB::table('tb_total_present')->delete();
        DB::table('lefty_resigned_basic_gross')->delete();
        $start_date=date('Y-m-d',strtotime($request->start));
        $end_date=date('Y-m-d',strtotime($request->end));
        $monthss=date('F-Y',strtotime($request->start));
        //round time function start
        function roundTimeSummery($time){
            $hour=date('G',strtotime($time));
            $minute=date('i',strtotime($time));
            if($minute<25){
                $minute=0;
            }
            elseif ($minute>=25 && $minute<55){
                $minute=.5;
            }
            else{
                $minute=0;
                $hour++;
            }
            $kind=number_format((float)($hour+$minute),1,'.','');
            return $kind;
        }
        //round time function end
        if($request->type=='dept_type_summary'){
            if($request->department_id==0){
              $extraot=DB::SELECT("SELECT attendance.*,
              dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
              attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
              FROM attendance
              JOIN employees ON attendance.emp_id=employees.id
              LEFT JOIN designations ON employees.empDesignationId=designations.id
              JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
              LEFT JOIN tblines ON tblines.id=employees.line_id
              WHERE attendance.date BETWEEN '$start_date' AND '$end_date' and employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times and employees.date_of_discontinuation IS NULL AND is_three_shift=0 GROUP BY attendance.emp_id,attendance.date,empDepartmentId");
              $lefty_query=DB::SELECT("SELECT attendance.*,
              dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
              attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
              FROM attendance
              JOIN employees ON attendance.emp_id=employees.id
              LEFT JOIN designations ON employees.empDesignationId=designations.id
              JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
              LEFT JOIN tblines ON tblines.id=employees.line_id
              WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=1 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times AND employees.date_of_discontinuation between '$start_date' AND '$end_date'  GROUP BY employees.empDepartmentId,attendance.emp_id,attendance.date");
              $resigned_query=DB::SELECT("SELECT attendance.*,
              dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
              attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
              FROM attendance
              JOIN employees ON attendance.emp_id=employees.id
              LEFT JOIN designations ON employees.empDesignationId=designations.id
              JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
              LEFT JOIN tblines ON tblines.id=employees.line_id
              WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=2 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times AND employees.date_of_discontinuation between '$start_date' AND '$end_date'  GROUP BY employees.empDepartmentId,attendance.emp_id,attendance.date");     
            }else{
                $extraot=DB::SELECT("SELECT attendance.*,
                dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
                attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
                FROM attendance
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                LEFT JOIN tblines ON tblines.id=employees.line_id
                WHERE attendance.date BETWEEN '$start_date' AND '$end_date' and employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times and employees.date_of_discontinuation IS NULL AND is_three_shift=0 AND empDepartmentId='$request->department_id' GROUP BY attendance.emp_id,attendance.date,empDepartmentId");
                $lefty_query=DB::SELECT("SELECT attendance.*,
                dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
                attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
                FROM attendance
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                LEFT JOIN tblines ON tblines.id=employees.line_id
                WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=1 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time  AND attendance.out_time>attendance.exit_times AND employees.date_of_discontinuation between '$start_date' AND '$end_date' AND empDepartmentId='$request->department_id'  GROUP BY attendance.emp_id,attendance.date");
                $resigned_query=DB::SELECT("SELECT attendance.*,
                dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
                attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
                FROM attendance
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                LEFT JOIN tblines ON tblines.id=employees.line_id
                WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=2 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times AND employees.date_of_discontinuation between '$start_date' AND '$end_date' AND empDepartmentId='$request->department_id'  GROUP BY attendance.emp_id,attendance.date");     
            }
        }
        //basic data insert start
        $insert_data=[];
        $totals_ot_resigned=0;
        $out_timee='00:00';
        $exit_timee='00:00';
        foreach($extraot as $extra){
          if($extra->out_time>$extra->exit_times){
            $out_time=$extra->out_time;
            $exit_time=$extra->exit_times;
            $tot_ot=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
          }else{
            $out_time=$out_timee; 
            $exit_time=$exit_timee; 
            $tot_ot=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
          }
          if($tot_ot>2){
            $totals_ot=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)))-2;
           }else{
           $totals_ot=0;
          }
          $total_ot=$totals_ot;
          $basic=$extra->basic_salary;
          $ot_rate=round($basic/104,2);
          $ot_amount=$total_ot*$ot_rate;
          $data = [
            'emp_id' =>$extra->emp_id,
            'emp_hour'=>$totals_ot,
            'basic_salary'=>$extra->basic_salary,
            'total_salary'=>$extra->total_employee_salary,
            'employee_id'=>$extra->employeeId,
            'name'=>$extra->empFirstName,
            'designation'=>$extra->designation,
            'department'=>$extra->empDepartmentId,              
            'line_no'=>$extra->line_no,
            'amount'=>$ot_amount,
         ];
          $insert_data[] = $data; 
        }
        $insert_data = collect($insert_data); 
        $chunks = $insert_data->chunk(800);
        foreach ($chunks as $chunk)
        {
        \DB::table('extra_ot_result')->insert($chunk->toArray());
        }
       //basic data insert end

       //lefty data insert start
       $insert_ldata=[];
        foreach($lefty_query as $lq){
          if($lq->out_time>$lq->exit_times){
            $out_time=$lq->out_time;
            $exit_time=$lq->exit_times;
            $tot_ot_left=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
          }else{
            $out_time=$out_timee; 
            $exit_time=$exit_timee; 
            $tot_ot_left=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
          }
          if($tot_ot_left>2){
                $totals_ot_lefty=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)))-2;
            }else{
              $totals_ot_lefty=0;
          }
          $total_ot_left=$totals_ot_lefty;
          $basic=$lq->basic_salary;
          $ot_rate=round($basic/104,2);
          $ot_amount=$total_ot_left*$ot_rate;
          $data = [
            'emp_id' =>$lq->emp_id,
            'emp_hour'=>$total_ot_left,
            'basic_salary'=>$lq->basic_salary,
            'total_salary'=>$lq->total_employee_salary,
            'employee_id'=>$lq->employeeId,
            'name'=>$lq->empFirstName,
            'designation'=>$lq->designation,
            'line_no'=>$lq->line_no,
            'amount'=>$ot_amount,
            'emp_type'=>1,
         ];
          $insert_ldata[] = $data; 
        }
        $insert_ldata = collect($insert_ldata); 
        $chunks = $insert_ldata->chunk(800);
        foreach ($chunks as $chunk)
        {
        \DB::table('extra_ot_resign_lefty')->insert($chunk->toArray());
        }
       //lefty data insert end 

        //resigned data insert start
        $insert_rdata=[];
        foreach($resigned_query as $rq){
          if($rq->out_time>$rq->exit_times){
            $out_time=$rq->out_time;
            $exit_time=$rq->exit_times;
            $tot_ot_resigned=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
          }else{
            $out_time=$out_timee; 
            $exit_time=$exit_timee; 
            $tot_ot_resigned=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
          }
          if($tot_ot_resigned>2){
                $totals_ot_resigned=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)))-2;
            }else{
              $totals_ot_resigned=0;
          }
          $basic=$rq->basic_salary;
          $ot_rate=round($basic/104,2);
          $ot_amount=$totals_ot_resigned*$ot_rate;
          $data = [
            'emp_id' =>$rq->emp_id,
            'emp_hour'=>$totals_ot_resigned,
            'basic_salary'=>$rq->basic_salary,
            'total_salary'=>$rq->total_employee_salary,
            'employee_id'=>$rq->employeeId,
            'name'=>$rq->empFirstName,
            'designation'=>$rq->designation,
            'line_no'=>$rq->line_no,
            'amount'=>$ot_amount,
            'emp_type'=>2,
         ];
          $insert_rdata[] = $data; 
        }
        $insert_rdata = collect($insert_rdata); 
        $chunks = $insert_rdata->chunk(800);
        foreach ($chunks as $chunk)
        {
        \DB::table('extra_ot_resign_lefty')->insert($chunk->toArray());
        }
       //resigned data insert end 

       //lefty basic gross insert start
       $lefty_data=DB::SELECT("SELECT extra_ot_resign_lefty.emp_id,SUM(DISTINCT(extra_ot_resign_lefty.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_resign_lefty.total_salary)) as gross_s,SUM(emp_hour) as total_hour,sum(amount) as total_amount from extra_ot_resign_lefty where extra_ot_resign_lefty.emp_type=1 GROUP BY extra_ot_resign_lefty.emp_id HAVING (SUM(emp_hour))> 0");
       $left_finish=[];
       foreach($lefty_data as $leftys_data){
         $data=[
            'emp_id'=>$leftys_data->emp_id,
            'basic' =>$leftys_data->basic_s,
            'gross'=>$leftys_data->gross_s,
            'ot_hour'=>$leftys_data->total_hour,
            'ot_amount'=>$leftys_data->total_amount,
            'type'=>1,
         ];
         $left_finish[]=$data;
       }
       $left_finish = collect($left_finish); 
       $chunks = $left_finish->chunk(800);
       foreach ($chunks as $chunk)
       {
       \DB::table('lefty_resigned_basic_gross')->insert($chunk->toArray());
       }
       //lefty basic gross insert end
       //resigned basic gross insert start
        $resigned_data=DB::SELECT("SELECT extra_ot_resign_lefty.emp_id,SUM(DISTINCT(extra_ot_resign_lefty.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_resign_lefty.total_salary)) as gross_s,SUM(emp_hour) as total_hour,sum(amount) as total_amount from extra_ot_resign_lefty where extra_ot_resign_lefty.emp_type=2 GROUP BY extra_ot_resign_lefty.emp_id HAVING (SUM(emp_hour))> 0");
        $resigned_finish=[];
        foreach($resigned_data as $resigneds_data){
          $data=[
              'emp_id'=>$resigneds_data->emp_id,
              'basic' =>$resigneds_data->basic_s,
              'gross'=>$resigneds_data->gross_s,
              'ot_hour'=>$resigneds_data->total_hour,
              'ot_amount'=>$resigneds_data->total_amount,
              'type'=>2,
           ];
           $resigned_finish[]=$data;
         }
         $resigned_finish = collect($resigned_finish); 
         $chunks = $resigned_finish->chunk(800);
         foreach ($chunks as $chunk)
         {
         \DB::table('lefty_resigned_basic_gross')->insert($chunk->toArray());
         }
         //resigned basic gross insert end

         //salary total insert  start
         $sal_total=[];
         $salary_total=DB::SELECT("SELECT extra_ot_result.department,SUM(DISTINCT(extra_ot_result.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_result.total_salary)) as gross_s 
         FROM extra_ot_result 
         GROUP BY extra_ot_result.emp_id
         HAVING (SUM(emp_hour))>0");
         foreach($salary_total as $st){
            $data=[
                'emp_id'=>$st->department,
                'total_present' =>$st->basic_s,
                'total_absent'=>$st->gross_s,
            ];
            $sal_total[]=$data;
         }
         $sal_total = collect($sal_total); 
         $chunks = $sal_total->chunk(800);
         foreach ($chunks as $chunk)
         {
         \DB::table('tb_total_present')->insert($chunk->toArray());
         }
         //salary total insert end

         //basic and gross of employee
         $total=DB::SELECT("SELECT departments.departmentName,extra_ot_result.department,SUM(extra_ot_result.emp_hour) as total_hour,SUM(extra_ot_result.emp_minute) as total_minute,SUM(extra_ot_result.friday_hour) as friday_hour,SUM(extra_ot_result.amount) as total_amount
         from extra_ot_result
         left join departments on extra_ot_result.department=departments.id
         GROUP BY extra_ot_result.department");
         $total_lefty=DB::SELECT("SELECT COUNT(*) as worker, SUM(basic) as basic,SUM(gross) as gross,SUM(ot_hour) as total_ot,SUM(ot_amount) as ot_amount
         FROM `lefty_resigned_basic_gross` WHERE type=1");
         $total_resigned=DB::SELECT("SELECT COUNT(*) as worker, SUM(basic) as basic,SUM(gross) as gross,SUM(ot_hour) as total_ot,SUM(ot_amount) as ot_amount
         FROM `lefty_resigned_basic_gross` WHERE type=2");
        if($request->dept_excel=='dept_excel'){
            $excelName=time()."_extra_ot_Sheet";
            Excel::create("$excelName", function($excel) use ($total,$monthss,$total_lefty,$total_resigned) {
                // Set the title
                $name=Auth::user()->name;
                $excel->setTitle("Extra overtime Summary");
                // Chain the setters
                $excel->setCreator($name);
                $excel->setDescription('Extra overtime Summary');
                $excel->sheet('Sheet 1', function ($sheet) use ($total,$monthss,$total_lefty,$total_resigned) {
                    $sheet->loadView('report.payroll.overtime.excel.extra_ot_summery_show_department_wise_all_excel')
                        ->with('total',$total)
                        ->with('monthss',$monthss)
                        ->with('total_lefty',$total_lefty)
                        ->with('total_resigned',$total_resigned);
                });
            })->download('xlsx');
        }else{
          return view('report.payroll.overtime.extra_ot_summery_show_department_wise',compact('total','monthss','request','total_lefty','total_resigned'));
        }
    }


    public function extraovertimesummeryshowSection(Request $request){
        DB::table('extra_ot_result')->delete();
        DB::table('extra_ot_resign_lefty')->delete();
        DB::table('tb_total_present')->delete();
        DB::table('lefty_resigned_basic_gross')->delete();
        $start_date=date('Y-m-d',strtotime($request->start_date));
        $end_date=date('Y-m-d',strtotime($request->end_date));
        $monthss=date('F-Y',strtotime($request->start_date));
        //round time function start
        function roundTimeSummery($time){
            $hour=date('G',strtotime($time));
            $minute=date('i',strtotime($time));
            if($minute<25){
                $minute=0;
            }
            elseif ($minute>=25 && $minute<55){
                $minute=.5;
            }
            else{
                $minute=0;
                $hour++;
            }
            $kind=number_format((float)($hour+$minute),1,'.','');
            return $kind;
        }
        //round time function end
            if($request->section_id=='all'){
              $extraot=DB::SELECT("SELECT attendance.*,employees.empSection,
              dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
              attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
              FROM attendance
              JOIN employees ON attendance.emp_id=employees.id
              LEFT JOIN designations ON employees.empDesignationId=designations.id
              JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
              LEFT JOIN tblines ON tblines.id=employees.line_id
              WHERE attendance.date BETWEEN '$start_date' AND '$end_date' and employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times and employees.date_of_discontinuation IS NULL AND is_three_shift=0 GROUP BY attendance.emp_id,attendance.date,employees.empSection");
              $lefty_query=DB::SELECT("SELECT attendance.*,employees.empSection,
              dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
              attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
              FROM attendance
              JOIN employees ON attendance.emp_id=employees.id
              LEFT JOIN designations ON employees.empDesignationId=designations.id
              JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
              LEFT JOIN tblines ON tblines.id=employees.line_id
              WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=1 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times AND employees.date_of_discontinuation between '$start_date' AND '$end_date'  GROUP BY employees.empSection,attendance.emp_id,attendance.date");
              $resigned_query=DB::SELECT("SELECT attendance.*,employees.empSection,
              dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
              attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
              FROM attendance
              JOIN employees ON attendance.emp_id=employees.id
              LEFT JOIN designations ON employees.empDesignationId=designations.id
              JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
              LEFT JOIN tblines ON tblines.id=employees.line_id
              WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=2 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times AND employees.date_of_discontinuation between '$start_date' AND '$end_date'  GROUP BY employees.empSection,attendance.emp_id,attendance.date");     
            }else{
                $extraot=DB::SELECT("SELECT attendance.*,employees.empSection,
                dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
                attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
                FROM attendance
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                LEFT JOIN tblines ON tblines.id=employees.line_id
                WHERE attendance.date BETWEEN '$start_date' AND '$end_date' and employees.empAccStatus=1 and employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 and attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times and employees.date_of_discontinuation IS NULL AND is_three_shift=0 AND employees.empSection='$request->section_id' GROUP BY attendance.emp_id,attendance.date");
                $lefty_query=DB::SELECT("SELECT attendance.*,employees.empSection,
                dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
                attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
                FROM attendance
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                LEFT JOIN tblines ON tblines.id=employees.line_id
                WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=1 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times AND employees.date_of_discontinuation between '$start_date' AND '$end_date' AND employees.empSection='$request->section_id'  GROUP BY attendance.emp_id,attendance.date");
                $resigned_query=DB::SELECT("SELECT attendance.*,employees.empSection,
                dayname(attendance.date)='Friday' as friday_attendance,empDepartmentId,
                attendance.emp_id,employees.employeeId,employees.empFirstName,employees.empJoiningDate,tblines.line_no,designations.designation,payroll_salary.basic_salary,payroll_salary.total_employee_salary,attendance.emp_id,attendance.date as att_date 
                FROM attendance
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                LEFT JOIN tblines ON tblines.id=employees.line_id
                WHERE attendance.date BETWEEN '$start_date' AND '$end_date' AND employees.empAccStatus=0 AND employees.discon_type=2 AND employees.empJoiningDate<='$end_date' AND employees.empOTStatus=1 AND attendance.in_time!=attendance.out_time AND attendance.out_time>attendance.exit_times AND employees.date_of_discontinuation between '$start_date' AND '$end_date' AND employees.empSection='$request->section_id'  GROUP BY attendance.emp_id,attendance.date");     
            }
    //basic data insert start
    $insert_data=[];
    $totals_ot_resigned=0;
    $out_timee='00:00';
    $exit_timee='00:00';
    foreach($extraot as $extra){
      if($extra->out_time>$extra->exit_times){
        $out_time=$extra->out_time;
        $exit_time=$extra->exit_times;
        $tot_ot=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
      }else{
        $out_time=$out_timee; 
        $exit_time=$exit_timee; 
        $tot_ot=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
      }
      if($tot_ot>2){
        $totals_ot=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)))-2;
       }else{
       $totals_ot=0;
      }
      $total_ot=$totals_ot;
      $basic=$extra->basic_salary;
      $ot_rate=round($basic/104,2);
      $ot_amount=$total_ot*$ot_rate;
      $data = [
        'emp_id' =>$extra->emp_id,
        'emp_hour'=>$totals_ot,
        'basic_salary'=>$extra->basic_salary,
        'total_salary'=>$extra->total_employee_salary,
        'employee_id'=>$extra->employeeId,
        'name'=>$extra->empFirstName,
        'designation'=>$extra->designation,
        'section'=>$extra->empSection,              
        'line_no'=>$extra->line_no,
        'amount'=>$ot_amount,
     ];
      $insert_data[] = $data; 
    }
    $insert_data = collect($insert_data); 
    $chunks = $insert_data->chunk(800);
    foreach ($chunks as $chunk)
    {
    \DB::table('extra_ot_result')->insert($chunk->toArray());
    }
   //basic data insert end

   //lefty data insert start
   $insert_ldata=[];
    foreach($lefty_query as $lq){
      if($lq->out_time>$lq->exit_times){
        $out_time=$lq->out_time;
        $exit_time=$lq->exit_times;
        $tot_ot_left=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
      }else{
        $out_time=$out_timee; 
        $exit_time=$exit_timee; 
        $tot_ot_left=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
      }
      if($tot_ot_left>2){
            $totals_ot_lefty=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)))-2;
        }else{
          $totals_ot_lefty=0;
      }
      $total_ot_left=$totals_ot_lefty;
      $basic=$lq->basic_salary;
      $ot_rate=round($basic/104,2);
      $ot_amount=$total_ot_left*$ot_rate;
      $data = [
        'emp_id' =>$lq->emp_id,
        'emp_hour'=>$total_ot_left,
        'basic_salary'=>$lq->basic_salary,
        'total_salary'=>$lq->total_employee_salary,
        'employee_id'=>$lq->employeeId,
        'name'=>$lq->empFirstName,
        'designation'=>$lq->designation,
        'line_no'=>$lq->line_no,
        'amount'=>$ot_amount,
        'emp_type'=>1,
     ];
      $insert_ldata[] = $data; 
    }
    $insert_ldata = collect($insert_ldata); 
    $chunks = $insert_ldata->chunk(800);
    foreach ($chunks as $chunk)
    {
    \DB::table('extra_ot_resign_lefty')->insert($chunk->toArray());
    }
   //lefty data insert end 

    //resigned data insert start
    $insert_rdata=[];
    foreach($resigned_query as $rq){
      if($rq->out_time>$rq->exit_times){
        $out_time=$rq->out_time;
        $exit_time=$rq->exit_times;
        $tot_ot_resigned=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
      }else{
        $out_time=$out_timee; 
        $exit_time=$exit_timee; 
        $tot_ot_resigned=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)));
      }
      if($tot_ot_resigned>2){
            $totals_ot_resigned=roundTimeSummery(date('G:i',strtotime($out_time)-strtotime($exit_time)))-2;
        }else{
          $totals_ot_resigned=0;
      }
      $basic=$rq->basic_salary;
      $ot_rate=round($basic/104,2);
      $ot_amount=$totals_ot_resigned*$ot_rate;
      $data = [
        'emp_id' =>$rq->emp_id,
        'emp_hour'=>$totals_ot_resigned,
        'basic_salary'=>$rq->basic_salary,
        'total_salary'=>$rq->total_employee_salary,
        'employee_id'=>$rq->employeeId,
        'name'=>$rq->empFirstName,
        'designation'=>$rq->designation,
        'line_no'=>$rq->line_no,
        'amount'=>$ot_amount,
        'emp_type'=>2,
     ];
      $insert_rdata[] = $data; 
    }
    $insert_rdata = collect($insert_rdata); 
    $chunks = $insert_rdata->chunk(800);
    foreach ($chunks as $chunk)
    {
    \DB::table('extra_ot_resign_lefty')->insert($chunk->toArray());
    }
   //resigned data insert end 

   //lefty basic gross insert start
   $lefty_data=DB::SELECT("SELECT extra_ot_resign_lefty.emp_id,SUM(DISTINCT(extra_ot_resign_lefty.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_resign_lefty.total_salary)) as gross_s,SUM(emp_hour) as total_hour,sum(amount) as total_amount from extra_ot_resign_lefty where extra_ot_resign_lefty.emp_type=1 GROUP BY extra_ot_resign_lefty.emp_id HAVING (SUM(emp_hour))> 0");
   $left_finish=[];
   foreach($lefty_data as $leftys_data){
     $data=[
        'emp_id'=>$leftys_data->emp_id,
        'basic' =>$leftys_data->basic_s,
        'gross'=>$leftys_data->gross_s,
        'ot_hour'=>$leftys_data->total_hour,
        'ot_amount'=>$leftys_data->total_amount,
        'type'=>1,
     ];
     $left_finish[]=$data;
   }
   $left_finish = collect($left_finish); 
   $chunks = $left_finish->chunk(800);
   foreach ($chunks as $chunk)
   {
   \DB::table('lefty_resigned_basic_gross')->insert($chunk->toArray());
   }
   //lefty basic gross insert end
   //resigned basic gross insert start
    $resigned_data=DB::SELECT("SELECT extra_ot_resign_lefty.emp_id,SUM(DISTINCT(extra_ot_resign_lefty.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_resign_lefty.total_salary)) as gross_s,SUM(emp_hour) as total_hour,sum(amount) as total_amount from extra_ot_resign_lefty where extra_ot_resign_lefty.emp_type=2 GROUP BY extra_ot_resign_lefty.emp_id HAVING (SUM(emp_hour))> 0");
    $resigned_finish=[];
    foreach($resigned_data as $resigneds_data){
      $data=[
          'emp_id'=>$resigneds_data->emp_id,
          'basic' =>$resigneds_data->basic_s,
          'gross'=>$resigneds_data->gross_s,
          'ot_hour'=>$resigneds_data->total_hour,
          'ot_amount'=>$resigneds_data->total_amount,
          'type'=>2,
       ];
       $resigned_finish[]=$data;
     }
     $resigned_finish = collect($resigned_finish); 
     $chunks = $resigned_finish->chunk(800);
     foreach ($chunks as $chunk)
     {
     \DB::table('lefty_resigned_basic_gross')->insert($chunk->toArray());
     }
     //resigned basic gross insert end

     //salary total insert  start
     $sal_total=[];
     $salary_total=DB::SELECT("SELECT extra_ot_result.section,SUM(DISTINCT(extra_ot_result.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_result.total_salary)) as gross_s 
     FROM extra_ot_result 
     GROUP BY extra_ot_result.emp_id
     HAVING (SUM(emp_hour))>0");
     foreach($salary_total as $st){
        $data=[
            'emp_id'=>$st->section,
            'total_present' =>$st->basic_s,
            'total_absent'=>$st->gross_s,
        ];
        $sal_total[]=$data;
     }
     $sal_total = collect($sal_total); 
     $chunks = $sal_total->chunk(800);
     foreach ($chunks as $chunk)
     {
     \DB::table('tb_total_present')->insert($chunk->toArray());
     }
     //salary total insert end

     //basic and gross of employee
     $total=DB::SELECT("SELECT section,extra_ot_result.department,SUM(extra_ot_result.emp_hour) as total_hour,SUM(extra_ot_result.emp_minute) as total_minute,SUM(extra_ot_result.friday_hour) as friday_hour,SUM(extra_ot_result.amount) as total_amount
     from extra_ot_result
     left join departments on extra_ot_result.department=departments.id
     GROUP BY extra_ot_result.section");
     $total_lefty=DB::SELECT("SELECT COUNT(*) as worker, SUM(basic) as basic,SUM(gross) as gross,SUM(ot_hour) as total_ot,SUM(ot_amount) as ot_amount
     FROM `lefty_resigned_basic_gross` WHERE type=1");
     $total_resigned=DB::SELECT("SELECT COUNT(*) as worker, SUM(basic) as basic,SUM(gross) as gross,SUM(ot_hour) as total_ot,SUM(ot_amount) as ot_amount
     FROM `lefty_resigned_basic_gross` WHERE type=2");
    if($request->section_excel=='section_excel'){
        $excelName=time()."_extra_ot_Sheet";
        Excel::create("$excelName", function($excel) use ($total,$monthss,$total_lefty,$total_resigned) {
            // Set the title
            $name=Auth::user()->name;
            $excel->setTitle("Extra overtime Summary");
            // Chain the setters
            $excel->setCreator($name);
            $excel->setDescription('Extra overtime Summary');
            $excel->sheet('Sheet 1', function ($sheet) use ($total,$monthss,$total_lefty,$total_resigned) {
                $sheet->loadView('report.payroll.overtime.excel.extra_ot_summery_show_sections_wise_all_excel')
                    ->with('total',$total)
                    ->with('monthss',$monthss)
                    ->with('total_lefty',$total_lefty)
                    ->with('total_resigned',$total_resigned);
            });
        })->download('xlsx');
    }else{
      return view('report.payroll.overtime.extra_ot_summery_show_sections_wise',compact('total','monthss','request','total_lefty','total_resigned'));
    }
    }
    
    //extra ot summery amount details show
    public function otAmountDetails($id){
        $id=base64_decode($id);
        $department=DB::table('departments')->where('id',$id)->select('departmentName')->first();
        $section=DB::table('employees')->where('empSection',$id)->select('empSection')->first();
        if(isset($department)){
            $details=DB::SELECT("SELECT SUM(emp_hour) as emp_hour,SUM(emp_minute) as emp_minute,SUM(friday_hour) as friday_hour,basic_salary,total_salary,employee_id,name,designation,line_no   
            FROM extra_ot_result
            WHERE department=$id
            GROUP BY extra_ot_result.emp_id ORDER BY employee_id ASC");
            return view('report.payroll.extra_ot_details.ot_details',compact('details','department','section')); 
        }else{
            $details=DB::SELECT("SELECT SUM(emp_hour) as emp_hour,SUM(emp_minute) as emp_minute,SUM(friday_hour) as friday_hour,basic_salary,total_salary,employee_id,name,designation,line_no   
            FROM extra_ot_result
            WHERE section='$id'
            GROUP BY extra_ot_result.emp_id ORDER BY employee_id ASC");
            return view('report.payroll.extra_ot_details.ot_details',compact('details','section'));
        }
      
    }



     //extra ot summery amount details show
     public function otAmountDetailsHr($id){
        $id=base64_decode($id);
        $department=DB::table('departments')->where('id',$id)->select('departmentName')->first();
        $section=DB::table('employees')->where('empSection',$id)->select('empSection')->first();
        if(isset($department)){
            $details=DB::SELECT("SELECT SUM(emp_hour) as emp_hour,SUM(emp_minute) as emp_minute,SUM(friday_hour) as friday_hour,basic_salary,total_salary,employee_id,name,designation,line_no   
            FROM extra_ot_result
            WHERE department=$id
            GROUP BY extra_ot_result.emp_id ORDER BY employee_id ASC");
            return view('report.payroll.ot_details_hr.ot_details_hr',compact('details','department','section')); 
        }else{
            $details=DB::SELECT("SELECT SUM(emp_hour) as emp_hour,SUM(emp_minute) as emp_minute,SUM(friday_hour) as friday_hour,basic_salary,total_salary,employee_id,name,designation,line_no   
            FROM extra_ot_result
            WHERE section='$id'
            GROUP BY extra_ot_result.emp_id ORDER BY employee_id ASC");
            return view('report.payroll.ot_details_hr.ot_details_hr',compact('details','section'));
        }
      
    }






    //overtime summary view hr
    public function overtime_summery_hr(){
        $department=DB::table('departments')->select('id','departmentName')->get();
        $section=DB::table('employees')->select('empSection')->whereNotNull('empSection')->groupBy('empSection')->get();
        return view("report.payroll.overtime.overtime_summary_hr",compact('department','section'));
    }

    //overtime summary hr report show
    public function overtime_summery_hr_show(Request $request){
            DB::table('extra_ot_result')->delete();
            DB::table('tb_total_present')->delete();
            $monthss=date('F-Y',strtotime($request->start));
            if($request->department_id=='all'){ 
                global  $set_date;
                $test_salary=0; 
                $start=date('Y-m-d',strtotime($request->start));
                $end=date('Y-m-d',strtotime($request->end));
                $start_one=date('d-m-Y',strtotime($request->start));
                $end_one=date('d-m-Y',strtotime($request->end));
                $regular_work_days =DB::table('tbweekend_regular_day')
                ->select(DB::raw('group_concat(weekend_date) as date'))
                ->where('weekend_month',date('Y-m-01',strtotime($request->start)))
                ->groupBy('weekend_month')
                ->first();
                if(isset($regular_work_days)){
                   $set_date=$regular_work_days->date;  
                }
                $overtime_result=DB::SELECT("SELECT payroll_salary.basic_salary,payroll_salary.total_employee_salary,designations.designation,employees.employeeId,employees.empDepartmentId,attendance.emp_id,employees.employeeId,employees.empFirstName,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time,HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance.exit_times) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM 
                attendance 
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
                LEFT JOIN departments ON employees.empDepartmentId=departments.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                WHERE attendance.date BETWEEN '$start' AND '$end' AND employees.empOTStatus=1
                AND (DAYNAME(attendance.date)!='Friday' OR attendance.date IN ('$set_date')) 
                AND time(attendance.out_time)>time(attendance_setup.exit_time) 
                AND attendance.in_time!=attendance.out_time 
                GROUP BY attendance.emp_id,attendance.date,employees.empDepartmentId");
                  
                    $ot_minute=0;
                    $data=0;
                    $insert_data = [];
                    foreach($overtime_result as $extraots){
                        $minute=0;
                    $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;
                    if($ot_hour>=2){
                        $ot_hours=2;
                    }
                    elseif($ot_hour<2){
                        $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time;
                        $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
                        if($ot_minute>=25 && $ot_minute<55){
                            $minute=.5;
                        }
                        elseif($ot_minute>=55){
                            $minute=1;
                        }
                        else{
                            $minute=0;
                        }
                    }
                    else{
                        $ot_hours=0;
                    }
                    $total_ot=$ot_hours+$minute;
                    $basic=$extraots->basic_salary;
                    $ot_rate=round($basic/104,2);
                    $ot_amount= $total_ot*$ot_rate;
                    $ot_data = [
                        'emp_id' =>$extraots->emp_id,
                        'emp_hour'=>$ot_hours,
                        'emp_minute'=>$minute,
                        'basic_salary'=>$extraots->basic_salary,
                        'total_salary'=>$extraots->total_employee_salary,
                        'employee_id'=>$extraots->employeeId,
                        'name'=>$extraots->empFirstName,
                        'designation'=>$extraots->designation,
                        'department'=>$extraots->empDepartmentId,   
                        'amount'=> $ot_amount,   
                    ];
                    $insert_data[] = $ot_data;
                }
                $insert_data = collect($insert_data); // Make a collection to use the chunk method
                // it will chunk the dataset in smaller collections containing 500 values each.
                // Play with the value to get best result
                $chunks = $insert_data->chunk(500);
                foreach ($chunks as $chunk)
                {
                    \DB::table('extra_ot_result')->insert($chunk->toArray());
                }
                //end big data insert
                $salary_total=DB::SELECT("SELECT extra_ot_result.department,SUM(DISTINCT(extra_ot_result.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_result.total_salary)) as gross_s FROM extra_ot_result GROUP BY extra_ot_result.emp_id HAVING (SUM(emp_hour) + SUM(emp_minute) + SUM(friday_hour))> 0");
                foreach($salary_total as $totals){
                    $insert[]=[
                        'emp_id'=>$totals->department,
                        'total_present' =>$totals->basic_s,
                        'total_absent'=>$totals->gross_s,
                    ]; 
                }
                if($test_salary%550==0){
                    if(!empty($insert)){
                        $insertData = DB::table('tb_total_present')->insert($insert);
                        unset($insert);
                    }
                }
                $test_salary++;
                //basic and gross of employee
                $total=DB::SELECT("SELECT departments.departmentName,extra_ot_result.department,SUM(extra_ot_result.emp_hour) as total_hour,SUM(extra_ot_result.emp_minute) as total_minute,SUM(extra_ot_result.friday_hour) as friday_hour,SUM(extra_ot_result.amount) as total_amount
                from extra_ot_result
                left join departments on extra_ot_result.department=departments.id
                GROUP BY extra_ot_result.department");
                if($request->dept_excel=='dept_excel'){
                    $excelName=time()."_extra_ot_Sheet";
                    Excel::create("$excelName", function($excel) use ($total,$monthss,$start_one,$end_one) {
                        // Set the title
                        $name=Auth::user()->name;
                        $excel->setTitle("Overtime Summary");
                        // Chain the setters
                        $excel->setCreator($name);
                        $excel->setDescription('Overtime Summary');
                        $excel->sheet('Sheet 1', function ($sheet) use ($total,$monthss,$start_one,$end_one) {
                            $sheet->loadView('report.payroll.overtime.excel.ot_summery_show_department_wise_hr_excel')
                                ->with('total',$total)
                                ->with('monthss',$monthss)
                                ->with('start_one',$start_one)
                                ->with('end_one',$end_one);
                        });
                    })->download('xlsx');
                 }else{
                    return view('report.payroll.overtime.ot_summery_show_department_wise_hr',compact('total','monthss','start_one','end_one'));
                 }
            }    

            if($request->type=='dept_type_summary'){
                global  $set_date;
                $department =DB::table('departments')->where('id',$request->department_id)->select('departmentName')->first();
                $test_salary=0; 
                $start=date('Y-m-d',strtotime($request->start));
                $end=date('Y-m-d',strtotime($request->end));
                $start_one=date('d-m-Y',strtotime($request->start));
                $end_one=date('d-m-Y',strtotime($request->end));
                $regular_work_days =DB::table('tbweekend_regular_day')
                ->select(DB::raw('group_concat(weekend_date) as date'))
                ->where('weekend_month',date('Y-m-01',strtotime($request->start)))
                ->groupBy('weekend_month')
                ->first();
                if(isset($regular_work_days)){
                   $set_date=$regular_work_days->date;  
                }
                $overtime_result=DB::SELECT("SELECT payroll_salary.basic_salary,payroll_salary.total_employee_salary,designations.designation,employees.employeeId,employees.empDepartmentId,attendance.emp_id,employees.employeeId,employees.empFirstName,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time,HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance.exit_times) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM 
                attendance 
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
                LEFT JOIN departments ON employees.empDepartmentId=departments.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                WHERE attendance.date BETWEEN '$start' AND '$end' AND employees.empOTStatus=1 
                AND employees.empDepartmentId=$request->department_id 
                AND (DAYNAME(attendance.date)!='Friday' OR attendance.date IN ('$set_date')) 
                AND time(attendance.out_time)>time(attendance.exit_times) 
                AND attendance.in_time!=attendance.out_time 
                GROUP BY attendance.emp_id,attendance.date,employees.empDepartmentId");
                  
                    $ot_minute=0;
                    $data=0;
                    $insert_data = [];
                    foreach($overtime_result as $extraots){
                        $minute=0;
                    $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;
                    if($ot_hour>=2){
                        $ot_hours=2;
                    }
                    elseif($ot_hour<2){
                        $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time;
                        $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
                        if($ot_minute>=25 && $ot_minute<55){
                            $minute=.5;
                        }
                        elseif($ot_minute>=55){
                            $minute=1;
                        }
                        else{
                            $minute=0;
                        }
                    }
                    else{
                        $ot_hours=0;
                    }
                    $total_ot=$ot_hours+$minute;
                    $basic=$extraots->basic_salary;
                    $ot_rate=round($basic/104,2);
                    $ot_amount= $total_ot*$ot_rate;
                    $ot_data = [
                        'emp_id' =>$extraots->emp_id,
                        'emp_hour'=>$ot_hours,
                        'emp_minute'=>$minute,
                        'basic_salary'=>$extraots->basic_salary,
                        'total_salary'=>$extraots->total_employee_salary,
                        'employee_id'=>$extraots->employeeId,
                        'name'=>$extraots->empFirstName,
                        'designation'=>$extraots->designation,
                        'department'=>$extraots->empDepartmentId,   
                        'amount'=> $ot_amount,   
                    ];
                    $insert_data[] = $ot_data;
                }
                $insert_data = collect($insert_data); // Make a collection to use the chunk method
                // it will chunk the dataset in smaller collections containing 500 values each.
                // Play with the value to get best result
                $chunks = $insert_data->chunk(500);
                foreach ($chunks as $chunk)
                {
                    \DB::table('extra_ot_result')->insert($chunk->toArray());
                }
                //end big data insert
                $salary_total=DB::SELECT("SELECT extra_ot_result.department,SUM(DISTINCT(extra_ot_result.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_result.total_salary)) as gross_s FROM extra_ot_result GROUP BY extra_ot_result.emp_id HAVING (SUM(emp_hour) + SUM(emp_minute) + SUM(friday_hour))> 0");
                foreach($salary_total as $totals){
                    $insert[]=[
                        'emp_id'=>$totals->department,
                        'total_present' =>$totals->basic_s,
                        'total_absent'=>$totals->gross_s,
                    ]; 
                }
                if($test_salary%550==0){
                    if(!empty($insert)){
                        $insertData = DB::table('tb_total_present')->insert($insert);
                        unset($insert);
                    }
                }
                $test_salary++;
                //basic and gross of employee
                $total=DB::SELECT("SELECT departments.departmentName,extra_ot_result.department,SUM(extra_ot_result.emp_hour) as total_hour,SUM(extra_ot_result.emp_minute) as total_minute,SUM(extra_ot_result.friday_hour) as friday_hour,SUM(extra_ot_result.amount) as total_amount
                from extra_ot_result
                left join departments on extra_ot_result.department=departments.id
                GROUP BY extra_ot_result.department");
                  if($request->dept_excel=='dept_excel'){
                    $excelName=time()."_extra_ot_Sheet";
                    Excel::create("$excelName", function($excel) use ($total,$monthss,$start_one,$end_one) {
                        // Set the title
                        $name=Auth::user()->name;
                        $excel->setTitle("Overtime Summary");
                        // Chain the setters
                        $excel->setCreator($name);
                        $excel->setDescription('Overtime Summary');
                        $excel->sheet('Sheet 1', function ($sheet) use ($total,$monthss,$start_one,$end_one) {
                            $sheet->loadView('report.payroll.overtime.excel.ot_summery_show_department_wise_hr_excel')
                                ->with('total',$total)
                                ->with('monthss',$monthss)
                                ->with('start_one',$start_one)
                                ->with('end_one',$end_one);
                        });
                    })->download('xlsx');
                 }else{
                    return view('report.payroll.overtime.ot_summery_show_department_wise_hr',compact('total','monthss','start_one','end_one'));
                 }
            }    

            if($request->section_id=='all'){
                global  $set_date;
                $section =DB::table('employees')->where('empSection',$request->section_id)->select('empSection')->first();
                $test_salary=0; 
                $start=date('Y-m-d',strtotime($request->start));
                $end=date('Y-m-d',strtotime($request->end));
                $start_one=date('d-m-Y',strtotime($request->start));
                $end_one=date('d-m-Y',strtotime($request->end));
                $regular_work_days =DB::table('tbweekend_regular_day')
                ->select(DB::raw('group_concat(weekend_date) as date'))
                ->where('weekend_month',date('Y-m-01',strtotime($request->start)))
                ->groupBy('weekend_month')
                ->first();
                if(isset($regular_work_days)){
                   $set_date=$regular_work_days->date;  
                }
                $overtime_result=DB::SELECT("SELECT employees.empsection,payroll_salary.basic_salary,payroll_salary.total_employee_salary,designations.designation,employees.employeeId,employees.empDepartmentId,attendance.emp_id,employees.employeeId,employees.empFirstName,attendance.emp_id,attendance.exit_times AS exit_time,attendance.out_time as out_time,HOUR(attendance.exit_times) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance_setup.exit_time) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM 
                attendance 
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
                LEFT JOIN departments ON employees.empDepartmentId=departments.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                WHERE attendance.date BETWEEN '$start' AND '$end' AND employees.empOTStatus=1
                AND (DAYNAME(attendance.date)!='Friday' OR attendance.date IN ('$set_date')) 
                AND time(attendance.out_time)>time(attendance_setup.exit_time) 
                AND attendance.in_time!=attendance.out_time 
                GROUP BY attendance.emp_id,attendance.date,employees.empsection");
                  
                    $ot_minute=0;
                    $data=0;
                    $insert_data = [];
                    foreach($overtime_result as $extraots){
                        $minute=0;
                    $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;
                    if($ot_hour>=2){
                        $ot_hours=2;
                    }
                    elseif($ot_hour<2){
                        $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time;
                        $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
                        if($ot_minute>=25 && $ot_minute<55){
                            $minute=.5;
                        }
                        elseif($ot_minute>=55){
                            $minute=1;
                        }
                        else{
                            $minute=0;
                        }
                    }
                    else{
                        $ot_hours=0;
                    }
                    $total_ot=$ot_hours+$minute;
                    $basic=$extraots->basic_salary;
                    $ot_rate=round($basic/104,2);
                    $ot_amount= $total_ot*$ot_rate;
                    $ot_data = [
                        'emp_id' =>$extraots->emp_id,
                        'emp_hour'=>$ot_hours,
                        'emp_minute'=>$minute,
                        'basic_salary'=>$extraots->basic_salary,
                        'total_salary'=>$extraots->total_employee_salary,
                        'employee_id'=>$extraots->employeeId,
                        'name'=>$extraots->empFirstName,
                        'designation'=>$extraots->designation, 
                        'section'=>$extraots->empsection,    
                        'amount'=> $ot_amount,   
                    ];
                    $insert_data[] = $ot_data;
                }
                $insert_data = collect($insert_data); // Make a collection to use the chunk method
                // it will chunk the dataset in smaller collections containing 500 values each.
                // Play with the value to get best result
                $chunks = $insert_data->chunk(500);
                foreach ($chunks as $chunk)
                {
                    \DB::table('extra_ot_result')->insert($chunk->toArray());
                }
                //end big data insert
                $salary_total=DB::SELECT("SELECT extra_ot_result.section,extra_ot_result.department,SUM(DISTINCT(extra_ot_result.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_result.total_salary)) as gross_s FROM extra_ot_result GROUP BY extra_ot_result.emp_id HAVING (SUM(emp_hour) + SUM(emp_minute) + SUM(friday_hour))> 0");
                foreach($salary_total as $totals){
                    $insert[]=[
                        'emp_id'=>$totals->section,
                        'total_present' =>$totals->basic_s,
                        'total_absent'=>$totals->gross_s,
                    ]; 
                }
                if($test_salary%550==0){
                    if(!empty($insert)){
                        $insertData = DB::table('tb_total_present')->insert($insert);
                        unset($insert);
                    }
                }
                $test_salary++;
                //basic and gross of employee
                $total=DB::SELECT("SELECT departments.departmentName,extra_ot_result.department,extra_ot_result.section,SUM(extra_ot_result.emp_hour) as total_hour,SUM(extra_ot_result.emp_minute) as total_minute,SUM(extra_ot_result.friday_hour) as friday_hour,SUM(extra_ot_result.amount) as total_amount
                from extra_ot_result
                left join departments on extra_ot_result.department=departments.id
                GROUP BY extra_ot_result.section");
                if($request->section_excel=='section_excel'){
                    $excelName=time()."_extra_ot_Sheet";
                    Excel::create("$excelName", function($excel) use ($total,$monthss,$start_one,$end_one) {
                        // Set the title
                        $name=Auth::user()->name;
                        $excel->setTitle("Overtime Summary");
                        // Chain the setters
                        $excel->setCreator($name);
                        $excel->setDescription('Overtime Summary');
                        $excel->sheet('Sheet 1', function ($sheet) use ($total,$monthss,$start_one,$end_one) {
                            $sheet->loadView('report.payroll.overtime.excel.ot_summary_show_section_wise_hr_excel')
                                ->with('total',$total)
                                ->with('monthss',$monthss)
                                ->with('start_one',$start_one)
                                ->with('end_one',$end_one);
                        });
                    })->download('xlsx');

                }else{
                    return view('report.payroll.overtime.ot_summary_show_section_wise_hr',compact('total','monthss','start_one','end_one'));
                }
            }

            if($request->type=='section_type_summary'){
                global  $set_date;
                $section =DB::table('employees')->where('empSection',$request->section_id)->select('empSection')->first();
                $test_salary=0; 
                $start=date('Y-m-d',strtotime($request->start));
                $end=date('Y-m-d',strtotime($request->end));
                $start_one=date('d-m-Y',strtotime($request->start));
                $end_one=date('d-m-Y',strtotime($request->end));
                $regular_work_days =DB::table('tbweekend_regular_day')
                ->select(DB::raw('group_concat(weekend_date) as date'))
                ->where('weekend_month',date('Y-m-01',strtotime($request->start)))
                ->groupBy('weekend_month')
                ->first();
                if(isset($regular_work_days)){
                   $set_date=$regular_work_days->date;  
                }
                $overtime_result=DB::SELECT("SELECT employees.empsection,payroll_salary.basic_salary,payroll_salary.total_employee_salary,designations.designation,employees.employeeId,employees.empDepartmentId,attendance.emp_id,employees.employeeId,employees.empFirstName,attendance.emp_id,attendance_setup.exit_time AS exit_time,attendance.out_time as out_time,HOUR(attendance_setup.exit_time) as hour_exit_time,HOUR(attendance.out_time) as hour_out_time,MINUTE(attendance_setup.exit_time) as minute_exit_time,MINUTE(attendance.out_time) as minute_out_time,attendance.date as att_date FROM 
                attendance 
                LEFT JOIN employees ON attendance.emp_id=employees.id
                LEFT JOIN attendance_setup ON employees.empShiftId=attendance_setup.id
                LEFT JOIN departments ON employees.empDepartmentId=departments.id
                LEFT JOIN designations ON employees.empDesignationId=designations.id
                LEFT JOIN payroll_salary ON attendance.emp_id=payroll_salary.emp_id
                WHERE attendance.date BETWEEN '$start' AND '$end' AND employees.empOTStatus=1
                AND employees.empsection='$request->section_id' 
                AND (DAYNAME(attendance.date)!='Friday' OR attendance.date IN ('$set_date')) 
                AND time(attendance.out_time)>time(attendance_setup.exit_time) 
                AND attendance.in_time!=attendance.out_time 
                GROUP BY attendance.emp_id,attendance.date,employees.empsection");
                  
                    $ot_minute=0;
                    $data=0;
                    $insert_data = [];
                    foreach($overtime_result as $extraots){
                        $minute=0;
                    $ot_hour=$extraots->hour_out_time-$extraots->hour_exit_time;
                    if($ot_hour>=2){
                        $ot_hours=2;
                    }
                    elseif($ot_hour<2){
                        $ot_hours=$extraots->hour_out_time-$extraots->hour_exit_time;
                        $ot_minute=$extraots->minute_out_time-$extraots->minute_exit_time;
                        if($ot_minute>=25 && $ot_minute<55){
                            $minute=.5;
                        }
                        elseif($ot_minute>=55){
                            $minute=1;
                        }
                        else{
                            $minute=0;
                        }
                    }
                    else{
                        $ot_hours=0;
                    }
                    $total_ot=$ot_hours+$minute;
                    $basic=$extraots->basic_salary;
                    $ot_rate=round($basic/104,2);
                    $ot_amount= $total_ot*$ot_rate;
                    $ot_data = [
                        'emp_id' =>$extraots->emp_id,
                        'emp_hour'=>$ot_hours,
                        'emp_minute'=>$minute,
                        'basic_salary'=>$extraots->basic_salary,
                        'total_salary'=>$extraots->total_employee_salary,
                        'employee_id'=>$extraots->employeeId,
                        'name'=>$extraots->empFirstName,
                        'designation'=>$extraots->designation, 
                        'section'=>$extraots->empsection,    
                        'amount'=> $ot_amount,   
                    ];
                    $insert_data[] = $ot_data;
                }
                $insert_data = collect($insert_data); // Make a collection to use the chunk method
                // it will chunk the dataset in smaller collections containing 500 values each.
                // Play with the value to get best result
                $chunks = $insert_data->chunk(500);
                foreach ($chunks as $chunk)
                {
                    \DB::table('extra_ot_result')->insert($chunk->toArray());
                }
                //end big data insert
                $salary_total=DB::SELECT("SELECT extra_ot_result.section,extra_ot_result.department,SUM(DISTINCT(extra_ot_result.basic_salary)) as basic_s,SUM(DISTINCT(extra_ot_result.total_salary)) as gross_s FROM extra_ot_result GROUP BY extra_ot_result.emp_id HAVING (SUM(emp_hour) + SUM(emp_minute) + SUM(friday_hour))> 0");
                foreach($salary_total as $totals){
                    $insert[]=[
                        'emp_id'=>$totals->section,
                        'total_present' =>$totals->basic_s,
                        'total_absent'=>$totals->gross_s,
                    ]; 
                }
                if($test_salary%550==0){
                    if(!empty($insert)){
                        $insertData = DB::table('tb_total_present')->insert($insert);
                        unset($insert);
                    }
                }
                $test_salary++;
                //basic and gross of employee
                $total=DB::SELECT("SELECT departments.departmentName,extra_ot_result.department,extra_ot_result.section,SUM(extra_ot_result.emp_hour) as total_hour,SUM(extra_ot_result.emp_minute) as total_minute,SUM(extra_ot_result.friday_hour) as friday_hour,SUM(extra_ot_result.amount) as total_amount
                from extra_ot_result
                left join departments on extra_ot_result.department=departments.id
                GROUP BY extra_ot_result.section");
                if($request->section_excel=='section_excel'){
                    $excelName=time()."_extra_ot_Sheet";
                    Excel::create("$excelName", function($excel) use ($total,$monthss,$start_one,$end_one) {
                        // Set the title
                        $name=Auth::user()->name;
                        $excel->setTitle("Overtime Summary");
                        // Chain the setters
                        $excel->setCreator($name);
                        $excel->setDescription('Overtime Summary');
                        $excel->sheet('Sheet 1', function ($sheet) use ($total,$monthss,$start_one,$end_one) {
                            $sheet->loadView('report.payroll.overtime.excel.ot_summary_show_section_wise_hr_excel')
                                ->with('total',$total)
                                ->with('monthss',$monthss)
                                ->with('start_one',$start_one)
                                ->with('end_one',$end_one);
                        });
                    })->download('xlsx');
                }else{
                    return view('report.payroll.overtime.ot_summary_show_section_wise_hr',compact('total','monthss','start_one','end_one'));
                }
            }

        }
  }



