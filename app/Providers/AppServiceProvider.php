<?php

namespace App\Providers;

use DB;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
       
//        // all employee count
        $employee=DB::table('employees')->count();
        view()->share('employee',$employee);

          //settings logo and copyright
        $copyright=DB::table('settings')->latest()->get();
        view()->share('copyright',$copyright);

        // Company Information
        $companyInformation=DB::table('tbcompany_information')->first();
        view()->share('companyInformation',$companyInformation);

        //all pending leave
        $pending_leave=DB::table('tb_leave_application')->where('status','=',0)->count();
        view()->share('pending_leave',$pending_leave);

        $meeting_date=DB::table('meetings')->where('date',date('Y-m-d'))->count();
        view()->share('meeting_date',$meeting_date);

        $metting_all=DB::table('meetings')->where('date',date('Y-m-d'))->get();
        view()->share('metting_all',$metting_all);

        //all pending leave
        $all_pending_leave_with_name=DB::table('tb_leave_application')->leftJoin('employees','tb_leave_application.employee_id','employees.id')->select('tb_leave_application.*','employees.empFirstName as emp_name')->where('status','=',0)->get();
        view()->share('all_pending_leave_with_name',$all_pending_leave_with_name);


        Blade::directive('money', function ($amount) {
            return "<?php echo '৳ ' . number_format($amount, 2); ?>";
        }
        );
    }

    /**
     * Register any application
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
