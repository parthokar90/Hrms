<?php

//Check Activation and Validity
Route::resource('/hrmsActivation', 'HrmsActivationController');
Route::get('/expired', function () {
    return view('activation.expired');
});
Route::get('/activation','HrmsActivationController@validity_form');
//Route for home login page and redirect to dashboard
Route::get('/AccessDenied','AccessController@AccessDenied');
Route::get('/','dashboardcontroller@dashboard_view');
Route::get('/dashboard','dashboardcontroller@dashboard_view');
Auth::routes();
//all necessary command
Route::get('commands', function () {
    \Artisan::call('migrate');
    return Redirect::back();
});

Route::get('/home', function () {
    return view('dashboard');
});

Route::get('/404', function () {
    return view('404_error');
});

Route::get('/info', function () {
    return view('phpinfo');
});

Route::get('/error', function () {
    return view('404');
});

Route::get('/500', function () {
    return view('404_error');
});

Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    return '<h1>Cache, Route, View cache cleared.</h1>';
});



Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/employee/details/{id}','EmployeeController@show');
//Route::get('/leave_show','leaveController@leave_type_show');
//Role wise permission route
Route::group(['middleware'=>'auth'], function () {
    //test
    Route::get('/up',['uses'=>'setupController@up']);
    Route::get('/process_deg',['uses'=>'setupController@process_deg']);
    Route::get('employee/employee_id_check/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@employee_id_check']);
    Route::get('employee/employee_card_number_check/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@employee_card_number_check']);
    Route::get('employee/employee_id_check_edit/{eid}/{rid}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@employee_id_check_edit']);
    Route::get('employee/employee_card_number_check_edit/{cid}/{rid}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@employee_card_number_check_edit']);
    //all training route start
    Route::get('/add_training',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'TrainingController@index']);
    Route::post('/add_training',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'TrainingController@storeTraining']);
    Route::post('/training_update',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'TrainingController@trainingUpdate']);
    Route::post('/employee_training',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'TrainingController@employeeTraining']);
    Route::get('/training_delete/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'TrainingController@training_delete']);
    Route::get('/training_history',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'TrainingController@training_history']);
    Route::get('/assign/training',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'TrainingController@training_assign']);
    Route::get('/assign/training/des_table/{designation}', 'TrainingController@DesignationTable');
    Route::get('/training/history/attendent/{id}', 'TrainingController@AttendentList');
    Route::get('/assign/training/addEmployee/{empid}', 'TrainingController@EmployeeTable');
    Route::get('/assign/training/delete/{del}', 'TrainingController@DeleteFromnTable');
    Route::get('/training/edit/{training_id}/{start_date}/{end_date}', 'TrainingController@edit')->name('training_history.edit');
    Route::Patch('/training/update', 'TrainingController@update_history');
    Route::post('/training/assign',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'TrainingController@training_assign_request']);
    //training route end
    //Dashboard 
    Route::get('/meetingdetails/{id}','WorkingExperienceController@showDelete')->name('workexperience.delete.show');
    //Employee route start
    Route::get('/grid_view',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@grid_view_index']);
    Route::POST('/employee_grid_filter',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@grid_filter']);
    Route::POST('/employee_grid_search',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@grid_employee_search']);
    Route::get('/employee_profile/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@employee_profile'])->name('employee_profile.show');
    Route::POST('/employee_filter',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@filter']);
    Route::POST('/employee_search',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@employee_search']);
    Route::get('/active_employee',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@active_employee_list']);
    Route::POST('/active_employee_filter',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@filter_active_employee']);
    Route::POST('/active_employee_search',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@active_employee_search']);
    Route::get('/inactive_employee',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@inactive_employee_list']);
    Route::POST('/inactive_employee_filter',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@filter_inactive_employee']);
    Route::POST('/inactive_employee_search',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@inactive_employee_search']);
    Route::get('/male_employee',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@male_employee_list']);
    Route::POST('/male_employee_filter',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@filter_male_employee']);
    Route::POST('/male_employee_search',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@male_employee_search']);
    Route::get('/female_employee',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@female_employee_list']);
    Route::POST('/female_employee_filter',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@filter_female_employee']);
    Route::POST('/female_employee_search',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@female_employee_search']);
    Route::get('/employee/bangla_information/{id}','EmployeeController@bangla_information')->name('employee.bangla_information');
    Route::POST('/employee/store_bangla_information',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@store_bangla_information'])->name('employee.store_bangla_information');
    Route::post('/employee_discontinuation',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'EmployeeController@UpdateResignationDate']);
    Route::resource('/employee','EmployeeController');
    Route::get('/delete_employee_list',['middleware'=>'check-permission:admin','uses'=>'EmployeeController@delete_employee_list'])->name('employee.delete_employee_list');
    Route::get('/employee_confirm_delete/{id}',['middleware'=>'check-permission:admin','uses'=>'EmployeeController@employee_confirm_delete'])->name('employee.employee_confirm_delete');
    Route::post('/employee_confirm_delete/{id}',['middleware'=>'check-permission:admin','uses'=>'EmployeeController@employee_confirmed_delete']);
    Route::get('/user/profile',['middleware'=>'check-permission:admin|hr|executive|accountant|hr-admin','uses'=>'dashboardcontroller@user_profile'])->name('user.profile');
    Route::resource('/employee/education','EmployeeEducationInfoController',['except'=>['index','show','edit']]);
    Route::resource('/employee/training','EmployeeTrainingController',['except'=>['index','show']]);
    Route::resource('/employee/nominee','NomineeController',['except'=>['index','show']]);
    Route::resource('/employee/workexpericence','WorkingExperienceController',['except'=>['show']]);
    Route::resource('/employee/skill','SkillTestController',['except'=>['show']]);
    Route::get('/employee/workexperience/delete/{id}','WorkingExperienceController@showDelete')->name('workexperience.delete.show');
    Route::get('/employee/skill/delete/{id}','SkillTestController@showDelete')->name('skill.delete.show');
    Route::get('/employee/nominee/delete/{id}','NomineeController@showDelete')->name('nominee.delete.show');
    Route::get('/employee/training/delete/{id}','EmployeeTrainingController@showDelete')->name('training.delete.show');
    Route::resource('/employee/attachments','OtherAttachmentController',['except'=>['create','index']]);
    Route::patch('/employee/password/{id}','EmployeeController@UpdatePassword')->name('employee.password.update');
    Route::patch('/employee/photo/{id}','EmployeeController@UpdatePhoto');
    Route::patch('/employee/proxy/{id}','EmployeeController@UpdateProxy');
    Route::get('/notification/read/{id}','EmployeeController@NotificationUpdate')->name('notification.read');
    Route::get('/employee/floor/line/{id}','EmployeeController@floor_line');
    Route::get('/employeelist/continuedAbsentList',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'EmployeeController@getContinueAbsentList'])->name('employee.continued_absent_list');
    Route::get('/employeelist/continued_absent_list_data',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'EmployeeController@continued_absent_list_data'])->name('employee.continued_absent_list_data');
    Route::post('/report/continuedAbsentListNotification',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'EmployeeController@continueAbsentEmployeeNotification']);
    Route::post('/inactive_continue_absent',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'EmployeeController@inactive_continue_absent']);
    Route::get('section-autocomplete-ajax',array('as'=>'employee.section.autocomplete.ajax','uses'=>'EmployeeController@sectionAjaxData'));
    Route::POST('/store_medical_history',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@store_medical_history']);
    Route::patch('/update_medical_history/{id}','EmployeeController@update_medical_history');
    Route::get('/employee_medical_history_pdf/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@download_ems'])->name('employee.download_ems');
    Route::get('/AbsentEmployees/sendAutoSMS','EmployeeController@sent_auto_sms')->name('employee.continue_absent_list_autosms');
    //Employee Route End
    //Settings route start
    Route::get('/user_settings',['middleware'=>'check-permission:admin','uses'=>'SystemsettingsController@index']);
    Route::post('/user_update',['middleware'=>'check-permission:admin','uses'=>'SystemsettingsController@user_pass_update']);
    Route::resource('/settings/country','CountryController',['except'=>['show','create','edit']]);
    Route::resource('/settings/maritialstatus','MaritialStatusController',['except'=>['show','create','edit']]);
    Route::resource('/settings/department','DepartmentController',['except'=>['show','create','edit']]);
    Route::resource('/settings/designation','DesignationController',['except'=>['show','create','edit']]);
    Route::resource('/settings/unit', 'UnitController',['except'=>['create']]);
    Route::resource('/settings/line', 'LineController',['except'=>['create']]);
    Route::resource('/settings/floor','FloorController',['except'=>['create']]);
    Route::resource('/settings/overtime','OvertimeController',['except'=>['create','store','show','edit','destroy']]);
    Route::resource('/settings/overtime-hour-count','OvertimeHourCountController',['except'=>['create','store','show','edit','destroy']]);
    Route::get('/settings/unit/delete/show/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'UnitController@showDelete'])->name('unit.delete.show');
    Route::get('/settings/line/delete/show/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'LineController@showDelete'])->name('line.delete.show');
    Route::get('/settings/floor/delete/show/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'FloorController@showDelete'])->name('floor.delete.show');
    // Settings route end
    //recruitment Route Start
    Route::resource('/recruitment/vacancy','RecruitmentController',['except'=>'edit']);
    Route::get('/recruitment/application/create','RecruitmentController@vacancyApplication')->name('application.create');
    Route::post('/recruitment/application','RecruitmentController@storeVacancyApplication')->name('application.store');
    Route::get('/recruitment/application','RecruitmentController@indexVacancyApplication')->name('application.index');
    Route::get('/recruitment/status','RecruitmentController@dailyRecruitment')->name('recruitment.status');
    Route::get('/recruitment/status/{date}', 'RecruitmentController@recruitmentStatus');
    Route::get('/recruitment/application/{id}', 'RecruitmentController@detailsVacancyApplication')->name('application.details');
    Route::patch('/recruitment/application/{id}','RecruitmentController@applicationUpdate')->name('application.update');
    Route::delete('/recruitment/application/{id}','RecruitmentController@applicationDelete')->name('application.delete');
    //recruitment Route end
    //payroll Route Start
    Route::get('/report/employee/daily/salary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@employeeDailysalary']);
    Route::post('/report/employee/daily/salary/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@employeeDailysalaryshow']);
    Route::get('/employee/grade/salary/update/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@gradeWisesalaryUpdate']);
    Route::get('/employee/salary/grade/modals/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@employeeSalaryEditmodal']);
    Route::post('/deduction_overtime',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@employee_absent_overtime']);
    Route::post('/payroll_grade_store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payroll_grade_store']);
    Route::get('/payroll/grade',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payroll_grade']);
    Route::get('/payslip',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payslip']);
    Route::get('/grade',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payroll_grade_show']);
    Route::post('/grade_update',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payroll_grade_update']);
    Route::get('/grade_delete/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@grade_delete']);
    Route::get('/salary_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@total_salary_view']);
    Route::get('/salary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payroll_salary']);
    Route::post('/gradeajax/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@show']);
    Route::post('/salary_store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@store']);
    Route::get('/salary_view',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@index'])->name('salary.View');
    Route::post('/salary_up',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@update']);
    Route::post('/employee_salary_update',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@update'])->name('salary.employee_salary_update');
    Route::get('/payroll/salary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payroll_salary']);
    Route::post('/employee/bonus/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@bonus_emloyee']);
    Route::post('/overtime_employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@employee_overtime']);
    Route::post('/overtime_employee_store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@overtime_amount_hour_store']);
    Route::post('/attendancebonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@AttendanceBonus']);
    Route::post('/festivalbonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@empFestivalbonus']);
    Route::post('/leave_employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@leave_employee']);
    Route::post('/pres_employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@employee_present_days_count']);
    Route::post('/leave_employee_store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@leave_employee_store']);
    Route::post('/employee_present_store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@employee_present_store']);
    Route::get('/report/bonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employee_month_wise_bonus_Report']);
    Route::get('/report/increment/bonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@incrementBonus']);
    Route::get('/report/attendance/bonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendanceBonus']);
    Route::get('/report/festival/bonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@festivalBonus']);
    Route::post('/report/employee/increment',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@incrementEmployeeBonusReport']);
    Route::post('/report/employee/attendance/bonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendanceEmployeeBonusReport']);
    Route::post('/report/employee/festival/bonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@festivalEmloyeeBonusReport']);
    Route::post('/report/employee/increment/bonus/pdf',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@incrementreportpdf']);
    Route::get('/report/employee/increment/bonus/pdf',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@incrementreportpdf']);
    Route::post('/report/employee/attendance/bonus/pdf',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendancereportpdf']);
    Route::get('/report/employee/attendance/bonus/pdf',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendancereportpdf']);
    Route::post('/report/year/bonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@one_year_bonus_Report']);
    Route::get('/report/payroll/employee/salary/sheet/save',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@view_salary_sheet']);
    Route::post('/process/date/wise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@date_wise_process']);
    Route::get('/check/process/month',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@CheckProcessMonth']);
    Route::get('/process/delete/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@CheckProcessMonthDelete']);
    Route::get('/report/payroll/employee/salary/summery',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salarySummery']);
    Route::get('/report/payroll/employee/salary/sheet',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@monthwisesalarysheetview']);
    Route::get('/report/payroll/bank_payment_sheet',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@bank_payment_sheet'])->name('payroll.bank_payment_sheet');
    Route::get('/report/payroll/bkash_payment_sheet',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@bkash_payment_sheet'])->name('payroll.bkash_payment_sheet');
    Route::post('/report/payroll/bank_payment_sheet_show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@bank_payment_sheet_show']);
    Route::post('/report/payroll/bkash_payment_sheet_show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@bkash_payment_sheet_show']);
    Route::get('/report/employee/salary/sheet/private',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@monthwisesalarysheetviewPrivate']);


    Route::post('/report/employee/salary/monthwise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@Monthwiseemployeesalarysheet']);
    Route::get('/report/employee/salary/monthwise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@Monthwiseemployeesalarysheet']);


    Route::post('/report/employee/salary/month/private',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@employee_private_salary']);
    Route::post('/report/employees/salarysummery',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@showSalarySummery']);
    Route::post('/report/employee/salarysheet',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@employee_SalarySheet']);
    Route::post('/report/employee/salarysheet/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'EmployeeThreeShiftController@shift_employee_SalarySheet']);
    Route::post('/report/employee/salarysheet/private',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@employee_SalarySheet_private']);
    Route::get('/report/payslip',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payslipGenerate']);
    Route::post('/report/payslip/generate',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payslipReportemployees']);
    Route::get('/report/payslip/generate',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payslipReportemployees']);
    Route::get('/bonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@employee_bonus_view']);
    Route::get('increment/bonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@incrementBonusview']);
//    Route::get('attendance/bonus',['middleware'=>'check-permission:admin|hr','uses'=>'PayrollController@attendanceBonuspage']);
    Route::get('festival/bonus',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@FestivalBonuspage'])->name('festival.view');
    Route::post('festival/bonus/work/group',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@fest_bonus_store']);
	Route::get('/process/delete/{month}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@CheckProcessMonthDelete']);
    Route::get('/process/update/{month}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@CheckProcessMonthUpdate']);
    //payroll Route end
    Route::get('/leave/addWeekends/{weekLeave}', 'leaveController@addweekLeave');
    Route::get('/leave_sett',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@leave_sett']);
    Route::get('leave/employee/leave_history/{id}', 'leaveController@employeeHistoryModal');
    //Route::get('/leave_sett',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@wkleavealternate']);
    Route::get('/leave/assign_leave',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@assignLeave']);
    Route::post('leave/assign/mannual_assign',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@mannualAssignLeave']);
    Route::get('/leave_request/delete/{id}','leaveController@delete_request')->name('leave_request.delete');
    Route::get('leave/employee_leave_history/{id}', 'leaveController@employeeLeaveLeft');
    Route::get('leave/employee_leave_type/{id}', 'leaveController@employeeLeaveType');
    Route::get('/leave',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@index']);
    Route::get('/leave_history',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'leaveController@leaveHistory']);
    Route::post('/leave_settings',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@leave_settings']);
    Route::post('/leave_update',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@leave_update']);
    Route::post('/festival_leave_update',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@festival_leave_update']);
    Route::get('/leave_delete/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@leave_delete']);
    Route::get('/leave/request/delete/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'leaveController@leaveRequestDelete']);
    Route::get('/festival_leave_delete/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@festival_leave_delete']);
    Route::post('/add_festival_leave',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@add_festival_leave']);
    Route::get('/attendance',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@index'])->name('attendance.file.create');
    Route::post('/attendance_store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@store']);
    Route::post('/csv_upload',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@csv_upload']);
    Route::get('/csv_view',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@csv_show']);
    Route::get('/attendance/files',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@filesView'])->name('files.show');
    Route::get('/attendance/files/show/delete/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@showDelete'])->name('file.show.delete');
    Route::delete('/attendance/files/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@destroyFile']);
    Route::get('/attendance/status/absent/{date}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@absentStatus']);
    Route::get('/attendance/employee/{id}/{date}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@addEmployee'])->name('employee.attendance.add');
    Route::get('/attendance/present/status/{id}/{date}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@addEditAttendance'])->name('employee.attendance.add');
    Route::post('/attendance/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@storeAttendance']);
    Route::patch('/attendance/update/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@updateAttendance']);
    Route::get('/attendance/update/dept',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@updateAttendancedept']);
    Route::get('/attendance/update/section',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@updateAttendancesection']);
    Route::post('/attendance/present/data/{date}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@presentAttendance']);
    // Route::get('/report/attendance/daily_attendance',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@presentStatus'])->name('attendance.present.status');
    Route::post('/attendance/manual/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@manualAttendance']);
    Route::post('/attendance/manual/store/dept',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@manualAttendancedept']);
    Route::post('/attendance/manual/store/section',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@manualAttendancesection']);
    Route::get('/attendance/present/status',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@presentStatus'])->name('attendance.present.status');
    Route::get('/attendance/create',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@createAttendance'])->name('attendance.manual.create');
    Route::get('/manual_attendance',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@manual_attendance'])->name('attendance.manual_attendance');
    Route::get('/attendance/manual_store_date',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@manual_store_date'])->name('attendance.manual_store_date');
    Route::post('/attendance/manual_store_date',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@manual_store_data']);
    Route::post('/attendance/manual_store_date_store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@manual_store_date_store']);
    Route::get('/attendance/edit',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@editAttendance'])->name('attendance.manual.edit');
    Route::get('/attendance/edit_date_wise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@editAttendanceDate'])->name('attendance.manual.edit_date_wise');
    Route::post('/attendance/edit_date_wise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@showEditAttendanceDate'])->name('attendance.manual.show_edit_date_wise');
    Route::post('/attendance/store_date_wise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@edit_store_date_store']);
    Route::get('/attendance/data/{value}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@dataAttendance']);
    Route::get('/data/employee',['middleware'=>'check-permission:admin|hr|executive|accountant|hr-admin','uses'=>'EmployeeController@data_employee'])->name('autocomplete.employee');
    Route::get('/attendance/delete',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@deleteAttendance'])->name('attendance.manual.delete');
    Route::get('/attendance/delete/page',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@deleteAttendanceProccess1'])->name('attendance.deleteAttendanceProccess1');
    Route::get('/attendance/confirm_delete/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@confirm_delete'])->name('attendance.delete_record');
    Route::get('/notifications',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@notification_list'])->name('notifications.index');
    //Route::get('/attendance/continiousAbsent',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@continiousAbsent']);
    //see all leave request
    Route::post('/leave_request',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@leave_request']);
    Route::get('/leave/maternityLeaveDetails',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@maternityLeaveHistory']);
    Route::get('/pay_first_installment/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@pay_first_installment']);
    Route::get('/pay_second_installment/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@pay_second_installment']);
    Route::get('/leave/earnLeaveDetails',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@earnLeave']);
    Route::get('/leave/earnLeaveDetails/data',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@earnLeave_data'])->name('leave.earnLeave_data');
    Route::get('/leave/earnLeave/payment/{emp_id}/{leave_available}/{sal_per_day}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@earnLeave_payment'])->name('leave.earnLeave_payment');
    Route::post('/leave/earnLeave/payment',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@earn_leave_payment']);
    Route::get('/leave_accepted/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@leave_accepted']);
    Route::get('/maternity_leave_benefits_of_person/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@personWiseMaternityLeaveBenefit']);
    //Route::get('/leave_accepted2/{id}',['middleware'=>'check-permission:admin|hr','uses'=>'leaveController@leave_accepted2']);
    Route::get('/leave_declined/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@leave_declined']);
    Route::get('/leave_declined2/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@leave_declined2']);
    Route::post('/complete_off',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@off_day']);
    //request for leave
    Route::get('/request_leave',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'leaveController@request_leave']);
    Route::post('/store_leave_request',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'leaveController@store_leave_request']);
    // Route::get('/leave_history',['middleware'=>'check-permission:admin|hr|executive','uses'=>'leaveController@employee_leave_history']);
    Route::get('/earn_leave/add',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@earn_leave_add'])->name('earn_leave.add');
    Route::get('/earn_leave/details',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@earn_leave_details'])->name('earn_leave.details');
    Route::post('/earn_leave/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@earn_leave_store']);
    Route::get('/attendance_bonus_category',['middleware'=>'check-permission:admin','uses'=>'AttendanceController@attendance_bonus']);
    Route::post('/attendance/bonus/store',['middleware'=>'check-permission:admin','uses'=>'AttendanceController@attendance_bonus_store']);
    Route::post('/attendance/bonus/update',['middleware'=>'check-permission:admin','uses'=>'AttendanceController@attendance_bonus_update']);
    Route::post('/attendance/bonus/delete',['middleware'=>'check-permission:admin','uses'=>'AttendanceController@attendance_bonus_delete']);
    Route::get('/attendance/settings',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@attendance_setup']);
    Route::post('/attendance/settings/store',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@settings_store']);
    Route::post('/attendance/settings/update',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@settings_update']);
    Route::post('/attendance/settings/delete',['middleware'=>'check-permission:admin|hr','uses'=>'AttendanceController@settings_delete']);
    //report route start
    Route::get('/report/pdf/employeeTrainingHistory',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employeeTrainingPDFReport']);
    Route::get('/report/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employeeReportDashboard']);
    Route::get('/report/employee_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employeeListReport']);
    Route::post('/report/view_employee_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewEmployeeListReport']);
    Route::get('/report/search_employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@search_employee'])->name('report.search_employee');
    Route::post('/report/search_employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@showEmployee']);
    Route::get('report/floor/line/{id}','ReportController@floor_line');
    Route::get('report/floor/line2/{id}','ReportController@floor_line2');
    Route::get('/report/employee_list_status',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employeeListByStatus']);
    Route::post('/report/view_employee_list_status',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewEmployeeListByStatus']);
    Route::get('/report/employee_list_gender',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employeeListByGender']);
    Route::post('/report/view_employee_list_gender',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewEmployeeListByGender']);
    Route::get('/report/employee_list_department',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employeeListByDepartment']);
    Route::post('/report/view_employee_list_department',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewEmployeeListByDepartment']);
    Route::get('/report/employee_list_designation',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employeeListByDesignation']);
    Route::post('/report/view_employee_list_designation',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewEmployeeListByDesignation']);
    Route::get('/report/floor_line_employee_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employeeListByFloorLine']);
    Route::post('/report/view_floor_line_employee_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewEmployeeListByFloorLine']);
    Route::get('/report/section_employee_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employeeListBySection']);
    Route::post('/report/view_section_employee_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewEmployeeListBySection']);
    Route::get('/report/new_employee_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@newEmployeeListReport']);
    Route::post('/report/view_new_employee_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewNewEmployeeList']);
    Route::get('/report/inactive_employee_list_report',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@inactiveEmployeeListReport']);
    Route::post('/report/inactive_employee_list_report',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewInactiveEmployeeList']);

    Route::get('/report/resign_lefty_employee_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@resign_lefty_employee_list']);
    Route::post('/report/resign_lefty_employee_list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@resign_lefty_employee_list_data']);



    Route::get('/report/payroll',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@payrollReportDashboard']);
    Route::get('/report/salary/summery',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salarySummeryReport']);
    Route::post('/report/salary/summery/monthwise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salarySummeryReportShow']);
    Route::post('/report/salary/summery/monthwise/department',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salarySummeryReportShowdepartment']);
    Route::post('/report/salary/summery/monthwise/designation',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salarySummeryReportShowdesignation']);

    Route::get('/report/salary/sheet/resigned/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@resignedEmployeesSalaryView']);
    Route::post('/report/salary/sheet/resigned/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@resignedEmployeesSalaryShow']);
    Route::get('/report/salary/sheet/resigned/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@resignedEmployeesSalaryShow']);

    Route::get('/report/salary/sheet/lefty/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@leftyEmployeesSalaryView']);
    Route::post('/report/salary/sheet/lefty/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@leftyEmployeesSalaryShow']);

    Route::get('/report/daily/employee/salary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@daily_employee_salary_view']);
    Route::post('/report/daily/employee/salary/data',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@daily_employee_salary_view_data']);
    Route::get('/report/attendance',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendanceReportDashboard']);
    Route::get('/report/attendance/date_wise_attendance',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendance_report_view']);
    Route::post('/attendance/report/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendance_report_show']);
    Route::get('/attendance/status',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@attendance_report_daily']);
    Route::post('/reports_daily',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_employee_attendance']);
    Route::get('/report/daily_late_present',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_late_present'])->name('report.daily.late.present');
    Route::post('/report/daily_late_present',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_late_present_data']);
    Route::get('/report/daily_present_report',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_present_report'])->name('report.daily_present_report');
    Route::post('/report/daily_present_report',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_present_report_data']);
    Route::get('/report/daily_absent_report',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_absent_report'])->name('report.daily_absent_report');
    Route::post('/report/daily_absent_report',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_absent_report_data']);
    Route::get('/report/public/daily_attendance',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@public_report_daily_attendance'])->name('report.public.daily_attendance');
    Route::post('/report/public/daily_attendance',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@public_view_report_daily_attendance']);
    Route::get('/report/date_wise_late',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@date_wise_late'])->name('report.date_wise_late');
    Route::post('/report/date_wise_late',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@date_wise_late_data']);
    Route::get('/report/public/date_wise_present',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@public_date_wise_present'])->name('report.public.date_wise_present');
    Route::post('/report/public/date_wise_present',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@public_date_wise_present_data']);
    Route::get('/report/date_wise_absent',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@date_wise_absent'])->name('report.date_wise_absent');
    Route::post('/report/date_wise_absent',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@date_wise_absent_data']);
    Route::get('/report/daily_overtime_report',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_overtime_report'])->name('report.daily_overtime_report');
    Route::post('/report/daily_overtime_report',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_overtime_data']);
    Route::get('/report/date_wise_overtime',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@date_wise_overtime'])->name('report.date_wise_overtime');
    Route::post('/report/date_wise_overtime',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@date_wise_overtime_data']);
    Route::get('/report/daily_attendance_exception',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_attendance_exception'])->name('report.daily_attendance_exception');
    Route::post('/report/daily_attendance_exception',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_attendance_exception_data']);
    Route::get('/report/date_wise_attendance_exception',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@date_wise_attendance_exception'])->name('report.date_wise_attendance_exception');
    Route::post('/report/date_wise_attendance_exception',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@date_wise_attendance_exception_data']);
    Route::get('/report/date_wise_inout_time_report',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@date_wise_inout_time_report'])->name('report.date_wise_inout_time_report');
    Route::post('/report/date_wise_inout_time_data',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@date_wise_inout_time_data'])->name('report.date_wise_inout_time_data');

    Route::get('/report/admin_date_wise_overtime',['middleware'=>'check-permission:admin','uses'=>'ReportController@admin_date_wise_overtime'])->name('report.admin_date_wise_overtime');
    Route::post('/report/admin_date_wise_overtime',['middleware'=>'check-permission:admin','uses'=>'ReportController@admin_date_wise_overtime_data']);
    Route::get('/report/daily_attendance',['middleware'=>'check-permission:admin','uses'=>'ReportController@report_daily_attendance'])->name('report.daily_attendance');
    Route::post('/report/daily_attendance',['middleware'=>'check-permission:admin','uses'=>'ReportController@view_report_daily_attendance']);
    Route::get('/report/date_wise_present',['middleware'=>'check-permission:admin','uses'=>'ReportController@date_wise_present'])->name('report.date_wise_present');
    Route::post('/report/date_wise_present',['middleware'=>'check-permission:admin','uses'=>'ReportController@date_wise_present_data']);
    Route::get('/report/attendance/card',['middleware'=>'check-permission:admin','uses'=>'ReportController@attendanceCard']);
    Route::get('/report/attendance/card/lefty',['middleware'=>'check-permission:admin','uses'=>'ReportController@attendanceCardLefty']);
    Route::get('/report/attendance/card_all/view',['middleware'=>'check-permission:admin','uses'=>'ReportController@attendanceCardAll']);
    Route::get('/report/attendance/card_all_b',['middleware'=>'check-permission:hr|hr-admin','uses'=>'ReportController@attendanceCardAllBuyer']);
    Route::get('/report/attendance/employee_attendance_summary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employee_attendance_summary']);
    Route::post('/report/attendance/search_attendance_summary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@search_attendance_summary']);
    Route::post('/report/attendance/employee_attendance_summary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employee_attendance_summary_data']);

    Route::post('/report/attendance/card_all_b',['middleware'=>'check-permission:hr|hr-admin','uses'=>'ReportController@job_card_all_data_buyer']);
    Route::post('/attendance/report/card',['middleware'=>'check-permission:admin','uses'=>'ReportController@attendanceCardReport']);
    Route::get('/report/attendance/summary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendance_summary'])->name('report.attendance_summery');
    Route::post('/report/attendance/summary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendance_summary_data']);
    Route::get('/report/leave',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@leave_report']);
    Route::get('/report/leave/dateWiseReport',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@dateWiseLeaveReport']);
    Route::get('/report/leave/onLeaveReport',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@onLeaveReport']);
    Route::get('/report/leave/onLeaveListPDF',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@onLeaveListPDF']);
    Route::get('/report/leave/earnLeaveReport',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@earnLeave']);
    Route::post('/report/leave/earnLeaveReport',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewEarnReport']);
    Route::get('/report/leave/maternityLeaveReport',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@maternityLeave']);
    Route::post('/report/leave/maternityLeaveReport',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewMaternityReport']);
    Route::get('/report/leave/availableLeaveReport',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@availableLeaveReport']);
    Route::post('/report/leave/availableLeaveReport',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@viewEmployeeAvailableLeave']);
    Route::get('/training/report',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@training_report']);
    Route::post('/leave/report/dateWiseReport/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@showDateWiseLeaveReport']);
    Route::post('/training/report/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@training_report_show']);
    Route::get('/report/vacancy',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@vacancyListFirst']);
    Route::post('/report/vacancy/vacancyList',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@vacancyList']);
    Route::get('/report/vacancy/applicantList',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@applicantList']);
    Route::post('/report/vacancy/applicantList/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@applicantListShow']);
    Route::get('/report/vacancy/acceptedList',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@acceptedList']);
    Route::get('/report/recruitment/acceptedListPDF',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@acceptedListPDF']);
    Route::get('/report/recruitment/acceptedListPDFBangla',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@acceptedListPDFBangla']);
    Route::get('/report/recruitment/dailyRecruitmentList',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@dailyRecruitmentList']);
    Route::get('/report/recruitment/dailyRecruitmentListPDF',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@dailyRecruitmentListPDF']);
    Route::get('/report/public/attendance/card',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendanceCardPublic'])->name('report.public.attendance_card');
//    Route::post('public/attendance/report/card',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@attendanceCardReportPublic']);
    Route::get('/report/leave',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@leave_report']);
    Route::post('/leave/report/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@leave_report_employee']);
    Route::get('/report/training',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@trainingReportDashboard']);
    Route::get('/report/training/ongoingTraining',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@ongoing_training_report_show']);
    Route::get('/report/training/completedTraining',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@completed_training_report_show']);
    Route::get('/report/training/completedTrainingPDF',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@completedTrainingPDF']);
    Route::get('/report/training/employeeHistory',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@employee_training_history_report']);
    Route::get('/report/training/trainingwiseEmployee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@training_wise_employee_report']);
    Route::get('/report/training/trainingWiseEmployeePDF',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@trainingWiseEmployeePDF']);
    Route::get('/report/training/trainingWiseEmployeeAjax', 'ReportController@training_report_show');
    Route::get('/report/recruitment',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@recruitmentReportDashboard']);
    //report route end
    Route::post('/employee/bonus/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@bonus_emloyee']);
    Route::post('/year/employee/bonus/update',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@year_employee_bonus']);
    Route::post('/year/employee/bonus/store/multiple',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@year_employee_bonus_multiple']);
    Route::get('/attendance/load',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'AttendanceController@request_leave']);
    Route::get('/report/production/bonus',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'PayrollController@ProductionBonusreport']);
    Route::post('/report/production/bonus/employee',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'PayrollController@ProductionBonusreportShow']);
    Route::get('/report/production/bonus/employee',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'PayrollController@ProductionBonusreportShow']);
    //meeting
//    Route::post('/meeting/create',['uses' => 'MeetingController@store']);
    Route::resource('/meeting', 'MeetingController');
//    Route::get('/create', 'MeetingController@create');
    //end meeting
    //expense routes
    Route::get('/expense/categories',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@expenseSettings']);
    Route::post('/expense/updateExpense',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@expenseUpdate']);
    Route::post('/expense/settings/addExpense',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@expenseAdd']);
    Route::get('/expense/settings/delete/{id}',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@expenseDelete']);
    Route::get('/expense/deleteDailyExpense/{id}',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@deleteDailyExpense']);
    Route::get('/expense/index',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@index']);
    Route::post('/expense/addDailyExpense',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@addDailyExpense']);
    Route::post('/expense/history',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@expenseHistory']);
    Route::get('/expense/selectDate',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@expenseDateSelect']);
    Route::get('/expense/report',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@expense_report'])->name('expense.report');
    Route::post('/expense/report',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'expenseController@expense_report_data']);
    //end expense routes
    Route::resource('booked', 'BookingController');
    Route::get('/booking/list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'BookingController@bookinglist'])->name('booking.list');
    Route::get('/booking/items',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'BookingController@index'])->name('booking.bookingitems');
    Route::get('/booking/new',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'BookingController@new_request'])->name('booking.new_request');
    Route::post('/booking/save_new_request',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'BookingController@save_new_request'])->name('booking.save_new_request');
    Route::POST('/booking/booking_cancel',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'BookingController@booking_cancel'])->name('booking.booking_cancel');
    Route::get('/booking/booking_report_view',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'BookingController@booking_report_view'])->name('booking.report_view');
    Route::POST('/booking/booking_report_data',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'BookingController@booking_report_data'])->name('booking.report.booking_report_data');
    // Vehicle Management Start
    Route::resource('vehicles', 'VehicleController');
    Route::get('/vehicle/list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@index'])->name('vehicle.list');
    Route::get('/vehicle/excategorylist',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@excatlist'])->name('vehicle.excatlist');
    Route::POST('/vehicle/excatstore',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@excatstore'])->name('vehicle.excatstore');
    Route::POST('/vehicle/excatupdate',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@excatupdate'])->name('vehicle.excatupdate');
    Route::POST('/vehicle/excatdelete',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@excatdelete'])->name('vehicle.excatdelete');
    Route::get('/vehicle/expenses',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@expenses'])->name('vehicle.expenses');
    Route::get('/vehicle/booking',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@booking_list'])->name('vehicle.booking');
    Route::POST('/vehicle/update_booking',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@update_booking'])->name('vehicle.update_booking');
    Route::POST('/vehicle/store_booking',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@store_booking'])->name('vehicle.store_booking');
    Route::POST('/vehicle/booking_cancel',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@booking_cancel'])->name('vehicle.booking_cancel');
    Route::get('/vehicle/report_view',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@report_view'])->name('vehicle.report_view');
    Route::get('/vehicle/vehicle_wise_report_view',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@vehicle_wise_report_view'])->name('vehicle.vehicle_wise_report_view');
    Route::POST('/vehicle/report/vehicle_wise_report_data',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@vehicle_wise_report_data'])->name('vehicle.report.vehicle_wise_report_data');
    Route::get('/vehicle/vehicle_booking_report_view',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@vehicle_booking_report_view'])->name('vehicle.vehicle_booking_report_view');
    Route::POST('/vehicle/report/vehicle_booking_report_data',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@vehicle_booking_report_data'])->name('vehicle.report.vehicle_booking_report_data');
    Route::POST('/vehicle/expenses/newfuelexpense',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@newfuelexpense'])->name('vehicle.expenses.newfuelexpense');
    Route::POST('/vehicle/expenses/newotherexpense',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'VehicleController@newotherexpense'])->name('vehicle.expenses.newotherexpense');
    // Vehicle Management End
    //settings route start
    Route::get('/system/settings',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'SettingsController@index']);
    Route::get('/system/style',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'SettingsController@style']);
    Route::POST('/change_style',['uses'=>'SettingsController@changeStyle']);
    Route::post('/settings_store',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'SettingsController@settings_store']);
    Route::post('/settings_update',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'SettingsController@settings_update']);
    Route::get('/settings_delete/{id}',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'SettingsController@settings_delete']);
    //settings route end
    //PDF VIEW
    Route::get('/attendance/card/pdf',['middleware'=>'check-permission:admin', 'uses'=>'ReportController@pdfAttendanceCard']);
//    Route::get('/attendance/card/pdf/lefty',['middleware'=>'check-permission:admin', 'uses'=>'ReportController@attendanceCardLeftyData']);
    Route::get('/public/attendance/card/pdf',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'ReportController@pdfPublicAttendanceCard']);
    Route::get('/report/training/ongoingTraining/pdf',['middleware'=>'check-permission:admin|hr|hr-admin', 'uses'=>'ReportController@pdfOngoingTrainingCard']);
    Route::get('/employee/id_card/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@id_card'])->name('employee.id_card');
    Route::get('/employee/appointment_latter/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@appointment_latter'])->name('employee.appointment_latter');
    Route::get('/employee/job_application_form/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@job_application_form'])->name('employee.job_application_form');
    Route::get('/employee/employee_background_check/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@employee_background_check'])->name('employee.employee_background_check');
    Route::get('/employee/training_subject_list/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@training_subject_list'])->name('employee.training_subject_list');
    Route::get('/employee/sewing_machine_responsibility_guide/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@sewing_machine_responsibility_guide'])->name('employee.sewing_machine_responsibility_guide');
    Route::get('/employee/quality_inspactor_responsibility_guide/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@quality_inspactor_responsibility_guide'])->name('employee.quality_inspactor_responsibility_guide');
    Route::get('/employee/line_ironman_responsibility_guide/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@line_ironman_responsibility_guide'])->name('employee.line_ironman_responsibility_guide');
    Route::get('/employee/sewing_assistant_machine_responsibility_guide/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@sewing_assistant_machine_responsibility_guide'])->name('employee.sewing_assistant_machine_responsibility_guide');
    Route::get('/employee/finishing_assistant_responsibility_guide/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@finishing_assistant_responsibility_guide'])->name('employee.finishing_assistant_responsibility_guide');
    Route::get('/employee/police_verification/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@generate_pva'])->name('employee.generate_pva');
    Route::get('/employee/police_verification_application/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@download_police_varification_application'])->name('employee.download_pva');
    Route::get('/employee/age_ability_certificate/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@age_ability_certificate'])->name('employee.age_ability_certificate');
    Route::get('/employee/nominee_form_pdf/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@nominee_form_pdf'])->name('employee.nominee_form_pdf');
    Route::POST('/police_verification_data',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@save_pva_data']);
    Route::get('/employee/resignation_letter/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@resignation_letter'])->name('employee.resignation_letter');
    Route::get('/employee/final_settlement/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'EmployeeController@final_settlement'])->name('employee.final_settlement');
    Route::get('/salary_data',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salary_data']);
    Route::get('/jop_application/vacancy/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'RecruitmentController@download_job_application'])->name('download_job_application');
    Route::get('/recruitment_policy',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'EmployeeController@recruitment_policy'])->name('company.recruitment_policy');
    //data save in history table
    Route::post('/salary_history_data',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'SalarySheet@save_history']);
    Route::get('/daily_attendance_summary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_attendance_summery'])->name('report.daily_attendance_summery');
    Route::get('/summary_report_date',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@summary_report_date'])->name('summary_report_date');
    Route::post('/summary_report_date',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@summary_report_date_data']);
    Route::post('/daily_attendance_summary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ReportController@daily_attendance_summary_data']);
    //Manual SMS/Email
    Route::get('/manual_sms',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'messageController@manual_sms']);
    Route::post('/manual_sms',['uses' => 'messageController@sent_sms']);
    Route::get('/manual_email',['middleware'=>'check-permission:admin|hr-admin','uses'=>'messageController@manual_email']);
    Route::post('/manual_email',['uses' => 'messageController@sent_email']);
    //payslip pdf report
    Route::get('/report/payslip/datewise/pdf/generate',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'PayrollController@payslip_pdf_report_generate_view']);
    Route::post('/report/payslip/datewise/pdf',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'PayrollController@payslip_pdf_report']);
    //company information
    Route::get('/company/information',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'SystemsettingsController@companyInformationview']);
    Route::post('/company/information/update',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'SystemsettingsController@companyInformationupdate']);
    Route::get('/attbonus/url/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'PayrollController@empGrossShow']);
    Route::post('/deduction/store',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'DeductionController@deductionStore']);
    Route::get('/advance',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'DeductionController@advancedDeduction']);
    Route::post('/loan/store',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'DeductionController@loanStore']);
    //notices
    Route::resource('/notices', 'noticeController');
    Route::get('/manual_bulk_sms',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'messageController@manual_bulk_sms_view']);
    Route::post('/proccess_bulk_sms',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'messageController@proccess_bulk_sms_view']);
     Route::POST('/sent_bulk_sms_to_employee',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'messageController@send_manual_bulk_sms']);
     Route::get('/aa',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'messageController@test1']);
    //All bonus route are start here
    Route::get('/designation/employee/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@Designationwiseemployeefestival']);
    Route::get('/tests',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@alldesignationemployee']);
    Route::get('/department/employee/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@Departmentwiseemployeefestival']);
    Route::get('/testsone',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@alldepartmentemployee']);
    Route::post('/designation/employee/bonus/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@designationBonusStore']);
    Route::post('/designation/employee/att/bonus/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@attdebonusstore']);
    Route::get('/inc/designation/all',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@incdesignationall']);
    Route::post('/inc/bonusss/multiple/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@incBonusStore']);
    Route::get('/production',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@ProductionBonus'])->name('production.view');
    Route::post('/production/bonus/work/group',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@ProductionBonusWorkgroup']);
    Route::post('/production/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@ProductionBonusStore']);
    Route::post('/production/store/work_group',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@ProductionBonusWorkgroupStore']);
    Route::post('/production/update/work_group/',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@ProductionBonusWorkgroupUpdate']);
    //All bonus route are end here
    //penalty advance report route start
      Route::get('/penalty',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'DeductionController@index']);
      Route::get('/report/advanced',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'DeductionController@loanEmployee']);
      Route::get('/report/penalty',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'DeductionController@Employeedeductionreportview']);
      Route::post('/report/penalty/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'DeductionController@Employeedeductionreportshow']);
      Route::get('/loan/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'DeductionController@Employeeloanreportshow']);
      Route::post('/normal/deduction/update',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'DeductionController@normalDeductionUpdate']);
    //penalty advance report route end
    //Arafat
    Route::get('/total_late_list',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'ReportController@total_late_list'])->name('report.total_late_list');
    Route::get('/total_absent_list',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'ReportController@total_absent_list'])->name('report.total_absent_list');
    Route::get('/total_absent_list/edit_absent/{id}/{date}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'ReportController@edit_absent'])->name('report.edit_absent');
    Route::post('/total_absent_list/update_absent',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'ReportController@update_absent']);
    Route::get('/total_present_list',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'ReportController@total_present_list'])->name('report.total_present_list');
    Route::get('/total_present_list/edit_present/{id}',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'ReportController@edit_present'])->name('report.edit_present');
    Route::post('/total_present_list/update_present',['middleware'=>'check-permission:admin|hr|executive|hr-admin','uses'=>'ReportController@update_present'])->name('report.update_present');
    //Arafat End
    //festival bonus report
    Route::post('/report/employee/festival/department',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@festivalreportworkgroup']);
    Route::post('/report/employee/festival/section',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@festivalreportworkgroupsection']);

    Route::post('/report/employee/festival/department/summary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@festivalreportworkgroupsummary']);
    Route::post('/report/employee/festival/section/summary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@festivalreportworkgroupsectionsummary']);
    //salary increment route
    Route::get('/employee/salary/increment',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@SalaryIncrementEmployees'])->name('increment_search_home');
    Route::post('/employee/year/search',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@SalaryIncrementEmployeesSearch']);
    Route::post('/employee/year/multiple/work/group/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@SalaryIncrementEmployeesBonusStore']);
    Route::post('/employee/year/next/increment/search',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@EmployeesNextIcrementSearch']);
    Route::post('/employee/year/next/increment/delete',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@Increment_delete']);
    Route::get('/employee/increment/letter/download/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@incrementLetter']);
    //Manual Overtime add employees
    Route::get('/manual/overtime',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@manual_overtime']);
    Route::post('/manual/overtime/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@manual_overtime_store']);
    Route::get('report/extra/overtime',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@extraovertimeReport'])->name('extraovertime');
    Route::post('report/extra/overtime/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@extraovertimeReportshow']);

    Route::get('report/salary/date/wise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@datewisesalaryreport'])->name('datewisesalaryemployees');
    Route::get('report/payroll/employee/salary/sheet/private',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@view_salary_sheet_private']);
    Route::post('report/salary/date/wise/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@datewisesalaryreportShow']);
    Route::get('report/extra/overtime/summery',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@extraovertimesummery'])->name('extraotsummery');
    Route::get('/access_log',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AccessController@index'])->name('access.index');
    // Activity Logs Area Start
    Route::get('/activity_logs/history_view',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ActivityLogsController@history_view'])->name('activity_logs.history_view');
    Route::get('/activity_logs/history_data',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ActivityLogsController@history_data'])->name('activity_logs.history_data');
    // Activity Logs Area End

    // Activity Logs Area Start
    Route::get('/weekend/list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'WeekendController@index'])->name('weekend.list');
    Route::POST('/weekend/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'WeekendController@store'])->name('weekend.store');
    Route::get('/weekend/destroy/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'WeekendController@destroy'])->name('weekend.destroy');
    // Activity Logs Area End

    // Activity Logs Area Start
    Route::get('/employee/temporary_employee_proxy/list',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'TemporaryEmployeeProxyController@index'])->name('employee.temporary_employee_proxy.list');
    Route::get('/employee/temporary_employee_proxy/merge',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'TemporaryEmployeeProxyController@merge_attendance'])->name('temporary_employee_proxy.merge_attendance');
    Route::POST('/employee/temporary_employee_proxy/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'TemporaryEmployeeProxyController@store'])->name('employee_proxy_card_list.store');
    Route::get('/employee/temporary_employee_proxy/destroy/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'TemporaryEmployeeProxyController@destroy'])->name('employee_proxy_card_list.destroy');
    // Activity Logs Area End

   // ot summary amount wise details start
    Route::get('department/employee/ot/amount/details/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@otAmountDetails']);
    Route::get('department/employee/ot/amount/details/hr/{id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@otAmountDetailsHr']);
   // ot summary amount wise details end


     // id card wise salary process
     Route::post('salary/process/idcard/wise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@id_card_process_salary']);
     Route::get('report/payroll/employee/salary/sheet/card',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@id_card_process_salary_report']);
     Route::post('/report/salary/card/wise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salary_month_wise_id_card_show']);
     // id card wise salary process end


    // id card wise salary process
    Route::post('/employee/search/',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@employeeSearch']);
    // id card wise salary process

    //advance salary 
    Route::get('/advance/salary/',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@advance_salary_pay']);
    Route::post('/advance/salary/store',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@advance_salary_pay_store']);
    //advance salary

    //leave summary
    Route::get('employee/leave/summary',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@leave_summary']);
    Route::post('employee/leave/summary/month/wise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@leaveSummary']);
    //leave summary

    //leave summary datewise
    Route::get('employee/leave/summary/datewise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@leave_summary_date_wise']);
    Route::post('employee/leave/summary/date/wise/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalaryController@leaveSummaryDatewiseShow']);
    //leave summary datewise

    //employee pending leave single view
    Route::get('employee/pending/leave/details/{emp_id}',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@employee_pending_leave_details']);
    //employee pending leave single view

    //extra overtime resign employee
    Route::get('report/extra/overtime/resign/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@extraovertimeReportResignEmployee']);
    Route::post('report/extra/overtime/resign/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@extraovertimeReportshowResignEmployee']);
    //extra overtime resign employee


    //extra overtime lefty employee
    Route::get('report/extra/overtime/lefty/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@extraovertimeReportLeftyEmployee']);
    Route::post('report/extra/overtime/lefty/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@extraovertimeReportshowLeftyEmployee']);
    //extra overtime lefty employee


    //salary summary lefty employee
      Route::get('report/salary/summary/lefty/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salarySummeryReportLefty']);
      Route::post('report/salary/summary/lefty/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salarySummeryReportLeftyShow']);
    //salary summary lefty employee


    //salary summary reigned employee
       Route::get('report/salary/summary/resigned/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salarySummeryReportResigned']);
       Route::post('report/salary/summary/resigned/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'SalarySheet@salarySummeryReportResignedShow']);
    //salary summary reigned employee

    
    //daily overtime summary employee
    Route::get('report/daily/overtime',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@daileOvertime']);
    Route::post('report/daily/overtime/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@dailyOvertimeShow']);
    //daily overtime summary employee

    //payslip report resigned employee
    Route::get('/report/payslip/resigned/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payslipGenerateResignedEmployee']);
    Route::post('/report/payslip/resigned/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payslipGenerateResignedEmployeeshow']);
    Route::get('/report/payslip/resigned/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payslipGenerateResignedEmployeeshow']);
    //payslip report resigned employee


    //payslip report lefty employee
    Route::get('/report/payslip/lefty/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payslipGenerateLeftyEmployee']);
    Route::post('/report/payslip/lefty/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payslipGenerateLeftyEmployeeshow']);
    Route::get('/report/payslip/lefty/employee/show',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'PayrollController@payslipGenerateLeftyEmployeeshow']);
    //payslip report lefty employee


    //cancel all leave 
    Route::post('cancel/all/leave/request',['middleware'=>'check-permission:admin|hr-admin|hr','uses'=>'leaveController@cancelAllLeave']);
   //cancel all leave 

   //approved all leave 
    Route::post('approve/all/leave/request',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@approveAllLeave']);
   //approved all leave 

   //approved all leave 
      Route::get('leave/employee/search',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'leaveController@leave_employee_search']);
   //approved all leave  

   //overtime summary hr role
   Route::get('overtime/summary/hr',['middleware'=>'check-permission:hr|hr-admin','uses'=>'datesalarycontroller@overtime_summery_hr']);
   Route::post('overtime/summary/hr/show',['middleware'=>'check-permission:hr|hr-admin','uses'=>'datesalarycontroller@overtime_summery_hr_show']);
   //overtime summary hr role

   //delete attendance date wise or department all
   Route::post('/attendance/delete/date/wise',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@delete_attendance_date_wise']);
   Route::get('/attendance/delete/date/wise/dept',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@delete_attendance_date_wise_dept']);
   Route::post('/attendance/delete/date/wise/dept/confirm',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'AttendanceController@delete_attendance_date_wise_dept_confirm']);
   //delete attendance date wise or department all


   //3-shift add/update/delete route start
   Route::get('emp/three/shift/add/view',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'EmployeeController@emp_three_shift_add_view'])->name('emp_three_shift_add');
   Route::get('emp/three/shift/add',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'EmployeeController@emp_three_shift_add_process']);
   Route::post('emp/three/shift/add/store',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'EmployeeController@emp_three_shift_add_process_store'])->name('three_shift_emp_store');
   Route::get('emp/three/shift/data/update',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'EmployeeController@emp_three_shift_update_process']);
   Route::post('emp/three/shift/data/update/final',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'EmployeeController@update_thre_shift'])->name('three_shift_emp_update');
   Route::get('emp/three/shift/data/delete/final/{id}',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'EmployeeController@delete_thre_shift']);
   Route::post('emp/three/shift/data/update/final/employee/all',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'EmployeeController@update_thre_shift_employee_final_all']);
   //3-shift add/update/delete route end

   //3-shift salary report start
   Route::get('/report/payroll/employee/salary/sheet/three_shift',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'ThreeShiftSalaryReport@emp_salary_report']);
   Route::post('/report/employee/salary/monthwise/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@Monthwiseemployeesalarysheet']);
   Route::get('/report/employee/salary/monthwise/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@Monthwiseemployeesalarysheet']);
   Route::get('/report/salary/sheet/lefty/employee/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@leftyEmployeesSalaryView']);
   Route::post('/report/salary/sheet/lefty/employee/show/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@leftyEmployeesSalaryShow']);
   Route::get('/report/salary/sheet/resigned/employee/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@resignedEmployeesSalaryView']);
   Route::post('/report/salary/sheet/resigned/employee/show/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@resignedEmployeesSalaryShow']);
   Route::get('/report/salary/sheet/resigned/employee/show/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@resignedEmployeesSalaryShow']);
   //3-shift salary report end

   //3-shift payslip report start
   Route::get('/report/payslip/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@payslipGenerate']);
   Route::post('/report/payslip/generate/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@payslipReportemployees']);
   Route::get('/report/payslip/generate/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@payslipReportemployees']);
   //3-shift payslip report end

    //3-shift payslip report resigned employee start 
    Route::get('/report/payslip/resigned/employee/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@payslipGenerateResignedEmployee']);
    Route::post('/report/payslip/resigned/employee/show/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@payslipGenerateResignedEmployeeshow']);
    Route::get('/report/payslip/resigned/employee/show/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@payslipGenerateResignedEmployeeshow']);
    //3-shift payslip report resigned employee end

    //3-shift payslip report lefty employee start
    Route::get('/report/payslip/lefty/employee/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@payslipGenerateLeftyEmployee']);
    Route::post('/report/payslip/lefty/employee/show/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@payslipGenerateLeftyEmployeeshow']);
    Route::get('/report/payslip/lefty/employee/show/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@payslipGenerateLeftyEmployeeshow']);
    //3-shift payslip report lefty employee end

    //3-shift bkash or bank payment sheet start
    Route::get('/report/payroll/bank_payment_sheet/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@bank_payment_sheet'])->name('payroll.bank_payment_sheet_three_shift');
    Route::get('/report/payroll/bkash_payment_sheet/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@bkash_payment_sheet'])->name('payroll.bkash_payment_sheet_three_shift');
    Route::post('/report/payroll/bank_payment_sheet_show/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@bank_payment_sheet_show']);
    Route::post('/report/payroll/bkash_payment_sheet_show/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@bkash_payment_sheet_show']);
    //3-shift bkash or bank payment sheet end 

    //3-shift salary summary start
    Route::get('/report/salary/summery/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@salarySummeryReport']);
    Route::post('/report/salary/summery/monthwise/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@salarySummeryReportShow']);
    //3-shift salary summary end

    //job card single 3-shift start
    Route::get('/report/attendance/card/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@emp_single_job_card']);
    Route::get('attendance/card/pdf/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@job_card_single']);
    Route::get('/report/attendance/card/lefty/resign/three/shift',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@emp_single_job_card_lefty_resign']);
    Route::get('attendance/card/pdf/three/shift/lefty/resign',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'ThreeShiftSalaryReport@left_resign_single_job_card']);
    //job card single 3-shft end

    //job card all 3-shift start
    Route::get('/report/attendance/card_all/view/three/shift',['middleware'=>'check-permission:admin','uses'=>'ThreeShiftSalaryReport@attendanceCardAll']);
    Route::post('/report/attendance/card_all/view/three/shift/data',['middleware'=>'check-permission:admin','uses'=>'ThreeShiftSalaryReport@attendanceCardAllshift']);
    //job card all 3-shift end


    //multiple employee wise shift  start
    Route::get('employee/assign/shift/',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'DepartmentWiseShift@multiple_emp_shift_data'])->name('multiple_emp_shift');
    Route::get('employee/assign/shift/list',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'DepartmentWiseShift@assign_shift_list'])->name('multiple_emp_shift_list');
    Route::post('employee/shift/multiple/data/update/department',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'DepartmentWiseShift@multiple_emp_shift_data_update'])->name('dept_wise_shift');
    Route::post('employee/shift/multiple/data/update/section',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'DepartmentWiseShift@multiple_emp_shift_data_update_section_wise'])->name('section_wise_shift');
    // Route::post('employee/shift/multiple/data/update/employee',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'DepartmentWiseShift@multiple_emp_shift_data_update_emp_wise'])->name('emp_wise_shift');



    Route::get('employee/assign/shift/delete/department/{id}/{start}/{end}',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'DepartmentWiseShift@assign_shift_delete_dept']);
    Route::get('employee/assign/shift/delete/section/{id}/{start}/{end}',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'DepartmentWiseShift@assign_shift_delete_section']);
    //multiple employee wise shift end

    //department/section wise holiday setup start
    Route::post('employee/holiday/setup/department/wise',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'DepartmentWiseShift@dept_wise_holiday'])->name('emp_dept_wise_holiday');
    Route::post('employee/holiday/setup/section/wise',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'DepartmentWiseShift@section_wise_holiday'])->name('emp_section_wise_holiday');
    Route::get('employee/holiday/delete/{medium}/{date}',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'DepartmentWiseShift@dept_section_holiday_delete']);
    //department/section wise holiday setup end

    //department/section wise attendace update start
    Route::post('att/setup/department/wise',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'AttendanceController@updateattdept']);
    //department/section wise attendace update end

    //department/section wise attendace update datewise start
    Route::get('att/setup/dept/section/date/wise',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'AttendanceController@manual_dept_section_datewise_data']);
    Route::post('att/setup/dept/section/date/wise/store',['middleware'=>'check-permission:hr|hr-admin|admin','uses'=>'AttendanceController@manual_dept_section_datewise_data_store']);
    //department/section wise attendace update datewise end

    //jobcard all admin mode without three shift
    Route::post('/report/attendance/card_all',['middleware'=>'check-permission:admin','uses'=>'ReportController@job_card_all_data']);
    //jobcard all admin mode without three shift

    Route::post('report/extra/overtime/summery/employee',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@extraovertimesummeryshow']);
    Route::post('report/extra/overtime/summery/employee/section',['middleware'=>'check-permission:admin|hr|hr-admin','uses'=>'datesalarycontroller@extraovertimesummeryshowSection']);


    Route::get('bonus/settings',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'PayrollController@bonus_settings'])->name('display_bonus_settings');
    Route::post('bonus/settings/store',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'PayrollController@bonus_settings_store']);
    Route::post('bonus/settings/update',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'PayrollController@bonus_settings_update']);
    Route::get('bonus/settings/edit/{id}',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'PayrollController@bonus_settings_edit']);


    //shift process deprtment,section wise
    Route::get('employees/shift/process/department/{id}/{start}/{end}/{m_time}/{e_time}',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'DepartmentWiseShift@emp_shift_process_dept']);
    Route::get('employees/shift/process/section/{id}/{start}/{end}/{m_time}/{e_time}',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'DepartmentWiseShift@emp_shift_process_section']);
    Route::get('employees/shift/',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'DepartmentWiseShift@employee_shift']);
    Route::post('employees/shift/department/update',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'DepartmentWiseShift@employee_shift_dept']);
    Route::post('employees/shift/section/update',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'DepartmentWiseShift@employee_shift_section']);

    //employee wise salary process
    Route::post('emp/wise/salary/process',['middleware'=>'check-permission:admin|hr|accountant|hr-admin','uses'=>'EmpSalaryProcess@employee_SalarySheet']);
    



});