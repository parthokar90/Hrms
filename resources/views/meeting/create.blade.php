@extends('layouts.master')
@section('title', 'Create New Meeting')
@section('content')
	<div class="page-content">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="panel panel-default no-bd">
					<div class="panel-header bg-dark">
						<h2 class="panel-title"><strong>Create</strong> New Meeting</h2>
					</div>
					<div class="panel-body bg-white">
						<div class="">
						<form action="{{url('/meeting')}}" method="post">
                            {{ csrf_field() }}
							<div class="required form-group">
                                    <label class="control-label">Meeting Subject</label>
                                    <div class="append-icon">
                                        <input type="text" name="msub" class="form-control" minlength="3" placeholder="Minimum 3 characters..." required>
                                	</div>
                             </div>
                             <div class="required form-group">
                                    <label class="control-label">Meeting Date</label>
                                    <div class="append-icon">
                                        <input type="text" name="date" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                	</div>
                             </div>
                             <div class="required form-group">
                                    <label class="control-label">Starting Time(HH:MM am/pm)</label>
                                    <div class="append-icon">
                                        <input type="time" name="stime" class="form-control" required>
                                	</div>
                             </div>
                             <div class="required form-group">
                                    <label class="control-label">Ending Time(HH:MM am/pm)</label>
                                    <div class="append-icon">
                                        <input type="time" name="etime" class="form-control" required>
                                	</div>
                             </div>

                            <div class="required form-group">
                                <label class="control-label">Meeting Venue</label>
                                <div class="append-icon">
                                    <input type="text" name="venue" placeholder="Enter meeting venue..." class="form-control" required>
                                </div>
                            </div>
                             <div class="required form-group">
                                    <label class="control-label">Description</label>
                                    <div class="append-icon">
                                    	<textarea rows="8" name="descrip" class="form-control" placeholder="Minimum 100 Character..." required></textarea>
                                	</div>
                             </div>
                             <div class="text-center  m-t-20">
                                        <button type="submit" class="btn btn-embossed btn-primary">Create</button>
                                        <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                                    </div>
                            
						</form>
						</div>

                       

					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>

@endsection