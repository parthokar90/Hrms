@extends('layouts.master')
@section('title', 'Error')
@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 ><i class="fa fa-exclamation-triangle"></i> Error</h3>
                        </div>
                        <div class="col-md-6" style="margin-top:8px;">
                        </div>
                    </div>
                </div>
                <div class="panel-content">
                    @if(Session::has('message'))
                        <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
                    @endif 
                </div>
            </div>
        </div>
    </div>
</div>

@include('include.copyright')
@endsection