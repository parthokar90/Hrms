@php
$total_amount=0;
$total_total_hour=0;
@endphp

@extends('layouts.master')
@section('title', 'Extra Overtime Report')


@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel panel-default">
                <div class="panel-heading"><b>Extra Overtime Report</div>
                    <div class="panel-body">

                        <p class="pull-right"><a href="{{route('extra_overtime_print')}}" style="margin-top: 17px;" class="btn btn-info pull-right" id="pdf_btn" type="button"><i class="fa fa-print"></i> Print</a></p>


                        <div id="print_area">
                        <h3 class="text-center">{{$companyInformation->company_name}}</h3>
                            <style>

                                .authority div span{
                                    display: inline-block;
                                    border-top: 1px solid #3a3a3a;
                                    padding: 6px 22px;
                                    margin-top: 30px;
                                }


                                @media    print {

                                    .table-bordered{
                                        border: 0px!important;
                                        border-top: 1px solid #ddd!important;
                                    }
                                    .border_none{
                                        border: none!important;
                                    }
                                }
                            </style>

                        @if(isset($department))
                        <h4 class="text-center">Department:{{ $department->departmentName}}</h4>
                        @endif 

                        @if(isset($section))
                        <h4 class="text-center">Section:{{ $section->empSection}}</h4>
                        @endif 

                        <p class="text-center"><b>Extra O.T sheet of ({{$request->start_date}} to {{$request->end_date}})</b></p>
                        <table class="table table-hover table-striped table-bordered" style="border: 0px;border-top: 1px solid #ddd;">

                            <thead>
                            <tr>
                                <th style="padding-left: 25px" scope="col">Sl No</th>
                                <th scope="col">Emp ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Designation</th>
                                <th scope="col">Basic</th>
                                <th scope="col">Gross</th>
                                <th scope="col">Overtime (Hr)</th>
                                <th scope="col">Rate</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Signature</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $order=0; $ot_hour=0; $total_ot=0; $total_amount=0; $basic=0; $gross=0; $ot_rates=0; @endphp
                            @foreach($extraotsss as $extra)
                            @php

                               $ot_rate=$extra->basic_salary/104;
                               $ac_rate=round($ot_rate,2);
                               $total_hour=$extra->total_hour+$extra->total_minute+$extra->friday_hour;
                               if($total_hour==0){
                                continue;
                               }
                               $order++;
                               $total_ot+=$total_hour;
                               $ot_rates+=$ac_rate;
                            @endphp
                            <tr>
                             <td style="padding-left: 25px" align="center">{{$order}}</td>
                             <td>{{$extra->employee_id}}</td>
                             <td>{{$extra->name}}</td>
                             <td>{{$extra->designation}}</td>
                             <td>{{$extra->basic_salary}}</td>
                             <td>{{$extra->total_salary}}</td>
                             <td>{{$total_hour}}</td>
                            <td>{{$ac_rate}}</td>
                             <td>
                                 @php
                                     $ot_amount=$ac_rate*$total_hour;
                                     $total_amount+=$ot_amount;
                                     $basic+=$extra->basic_salary;
                                     $gross+=$extra->total_salary;
                                 @endphp
                                 {{round($ot_amount,0)}}
                             </td>
                             <td width="150px"></td>
                            </tr>
                          @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="30" style="border: none;" class="border_none">
                                    <div class="ex_footer_1">
                                        <div class="authority" style="display: flex;">
                                            <div class="prepared_by" style="width: 22%;text-align: center">
                                                <span style="display: inline-block;
            border-top: 1px solid #3a3a3a;
            padding: 6px 22px;
            margin-top: 30px;">Prepared By</span>
                                            </div>
                                            <div class="audited_by" style="width: 22%;text-align: center"">
                                            <span style="display: inline-block;
            border-top: 1px solid #3a3a3a;
            padding: 6px 22px;
            margin-top: 30px;">Audited By</span>
                                        </div>
                                        <div class="recomended_by" style="width: 22%;text-align: center"">
                                        <span style="display: inline-block;
            border-top: 1px solid #3a3a3a;
            padding: 6px 22px;
            margin-top: 30px;">Recommended By</span>
                                    </div>
                                    <div class="approved_by" style="width: 22%;text-align: center"">
                                    <span style="display: inline-block;
            border-top: 1px solid #3a3a3a;
            padding: 6px 22px;
            margin-top: 30px;">Director</span>
                        </div>
                    </div>


                </div>
                                </td>
                            </tr>
                            </tfoot>

                                <tr>
                                  <td>Total:</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td>{{$basic}}</td>
                                <td>{{$gross}}</td>
                                <td>{{$total_ot}}</td>
                                  <td>{{round($ot_rates,2)}}</td>
                                <td>{{number_format($total_amount)}}</td>
                                  <td></td>
                                </tr>

                        </table>


{{--                            <div class="ex_footer_1">--}}
{{--                                        <div class="authority" style="display: flex;">--}}
{{--                                            <div class="prepared_by" style="width: 22%;text-align: center">--}}
{{--                                                <span style="display: inline-block;--}}
{{--            border-top: 1px solid #3a3a3a;--}}
{{--            padding: 6px 22px;--}}
{{--            margin-top: 30px;">Prepared By</span>--}}
{{--                                            </div>--}}
{{--                                            <div class="audited_by" style="width: 22%;text-align: center"">--}}
{{--                                                <span style="display: inline-block;--}}
{{--            border-top: 1px solid #3a3a3a;--}}
{{--            padding: 6px 22px;--}}
{{--            margin-top: 30px;">Audited By</span>--}}
{{--                                            </div>--}}
{{--                                            <div class="recomended_by" style="width: 22%;text-align: center"">--}}
{{--                                                <span style="display: inline-block;--}}
{{--            border-top: 1px solid #3a3a3a;--}}
{{--            padding: 6px 22px;--}}
{{--            margin-top: 30px;">Recommended By</span>--}}
{{--                                            </div>--}}
{{--                                            <div class="approved_by" style="width: 22%;text-align: center"">--}}
{{--                                                <span style="display: inline-block;--}}
{{--            border-top: 1px solid #3a3a3a;--}}
{{--            padding: 6px 22px;--}}
{{--            margin-top: 30px;">Director</span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}


{{--                            </div>--}}
                     </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
            function printDiv(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
        </script>
@include('include.copyright')
@endsection