@extends('layouts.master')
@section('title', 'Generate Salary')
@section('content')
    <div class="page-content">
        @if(Session::has('msg'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('msg') }}</p>
        @endif
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Generate Salary for 3-Shift Employees</b></div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group">
                                 <label>Select Type</label>
                                 <select id="ot_type" name="ot_type" class="form-control">
                                          <option value="">Select</option>
                                            <option value="1">Department Wise</option>
                                            <option value="2">Section Wise</option>
                                            <option value="3">Employee Wise</option>
                                            <option value="4">Month Wise</option>
                                  </select>
                                </div>
                            </div>
                                   <div class="col-md-12" id="department_type" style="display:none">
                                        {{Form::open(array('url' => '/report/employee/salary/monthwise/three/shift','method' => 'post','target' => '_blank'))}}
                                       <div class="col-md-4">
                                        <div class="form-group">
                                        <label>Select Department</label>
                                        <select class="form-control" name="department_id" data-search="true">
                                                @foreach($department as $departments)
                                                   <option value="{{$departments->id}}">{{$departments->departmentName}}</option>
                                                @endforeach
                                        </select>
                                        </div>
                                       </div>
                                       <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-label">Select Month <span style="color: darkred;font-size: 16px;">*</span></label>
                                                <div class="prepend-icon">
                                                    <input type="text" autocomplete="off" name="emp_sal_month" id="emp_sal_month" class="form-control format_date" required>
                                                    <i class="icon-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-label">Payment Date</label>
                                                <div class="prepend-icon">
                                                    <input type="text" autocomplete="off" name="salary_payment_date" id="salary_payment_date" class="form-control date-picker">
                                                    <i class="icon-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="type" value="dept_wise_data">
                                        <div class="col-md-12">
                                                <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Preview Salary</button>
                                                <button type="submit" value="salary_month_wise_excel_department" name="salary_month_wise_excel_department" class="btn btn-success">Export Excel</button>
                                            </div>
                                         {{ Form::close() }}
                                   </div>
                                   <div class="col-md-12" id="section_type" style="display:none">
                                        {{Form::open(array('url' => '/report/employee/salary/monthwise/three/shift','method' => 'post','target' => '_blank'))}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Select Section</label>
                                            <select class="form-control" name="section_id" data-search="true">
                                                    @foreach($section as $sections)
                                                       <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                                    @endforeach
                                            </select>
                                            </div>
                                           </div>
                                           <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-label">Select Month <span style="color: darkred;font-size: 16px;">*</span></label>
                                                    <div class="prepend-icon">
                                                        <input type="text" autocomplete="off" name="emp_sal_month" id="emp_sal_month_section" class="form-control format_date" required>
                                                        <i class="icon-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-label">Payment Date</label>
                                                    <div class="prepend-icon">
                                                        <input type="text" class="form-control date-picker" name="salary_payment_date" autocomplete="off">
                                                        <i class="icon-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="type" value="section_wise_data">
                                            <div class="col-md-12">
                                                    <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Preview Salary</button>
                                                    <button type="submit" value="salary_month_wise_excel_section"  name="salary_month_wise_excel_section" class="btn btn-success">Export Excel</button>
                                                </div>
                                             {{ Form::close() }}
                                       </div>
                                   <div class="col-md-12" id="employee_type" style="display:none">
                                        {{Form::open(array('url' => '/report/employee/salary/monthwise/three/shift','method' => 'post','target' => '_blank'))}}
                                             <div class="col-md-4">
                                                 <div class="form-group">
                                                   <label>Select Employee</label>  
                                                <select class="form-control" name="emp_id" data-search="true">
                                                        @foreach($employee as $employees)
                                                           <option value="{{$employees->id}}">({{$employees->employeeId}}) {{$employees->empFirstName}} {{$employees->empLastName}}</option>
                                                        @endforeach
                                                </select>
                                                 </div>
                                               </div>
                                           <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-label">Select Month <span style="color: darkred;font-size: 16px;">*</span></label>
                                                    <div class="prepend-icon">
                                                        <input type="text" autocomplete="off" name="emp_sal_month" id="emp_sal_month_emp" class="form-control format_date" required>
                                                        <i class="icon-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                            
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="form-label">Payment Date</label>
                                                    <div class="prepend-icon">
                                                        <input type="text" class="form-control date-picker" name="salary_payment_date" autocomplete="off">
                                                        <i class="icon-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                                <input type="hidden" name="type" value="employee_wise_data">
                                                <div class="col-md-12">
                                                        <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Preview Salary</button>
                                                        <button type="submit" value="salary_month_wise_excel_employee"  name="salary_month_wise_excel_employee" class="btn btn-success">Export Excel</button>
                                                    </div> 
                                                {{ Form::close() }}
                                        </div>
                                     <div class="col-md-12" id="month_wise" style="display:none">
                                            {{Form::open(array('url' => '/report/employee/salary/monthwise/three/shift','method' => 'post','target' => '_blank'))}}                                      
                                               <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Select Month <span style="color: darkred;font-size: 16px;">*</span></label>
                                                        <div class="prepend-icon">
                                                            <input type="text" autocomplete="off" name="emp_sal_month" id="emp_sal_monthss" class="form-control format_date" required>
                                                            <i class="icon-calendar"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Payment Date</label>
                                                        <div class="prepend-icon">
                                                            <input type="text" class="form-control date-picker" name="salary_payment_date" autocomplete="off">
                                                            <i class="icon-calendar"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                    <input type="hidden" name="type" value="month_wise_data">
                                                    <div class="col-md-12">
                                                            <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Preview Salary</button>
                                                            <button type="submit" value="salary_month_wise_excel_month"  name="salary_month_wise_excel_month" class="btn btn-success">Export Excel</button>
                                                        </div> 
                                                    {{ Form::close() }}
                                         </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#emp_sal_month').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "yy-mm",
                    showButtonPanel: true,
                    currentText: "This Month",
                    onChangeMonthYear: function (year, month, inst) {
                        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                    },
                    onClose: function(dateText, inst) {
                        var month = $(".ui-datepicker-month :selected").val();
                        var year = $(".ui-datepicker-year :selected").val();
                        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                    }
                }).focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });
                $('#emp_sal_month_section').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "yy-mm",
                    showButtonPanel: true,
                    currentText: "This Month",
                    onChangeMonthYear: function (year, month, inst) {
                        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                    },
                    onClose: function(dateText, inst) {
                        var month = $(".ui-datepicker-month :selected").val();
                        var year = $(".ui-datepicker-year :selected").val();
                        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                    }
                }).focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });

                $('#emp_sal_month_emp').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "yy-mm",
                    showButtonPanel: true,
                    currentText: "This Month",
                    onChangeMonthYear: function (year, month, inst) {
                        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                    },
                    onClose: function(dateText, inst) {
                        var month = $(".ui-datepicker-month :selected").val();
                        var year = $(".ui-datepicker-year :selected").val();
                        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                    }
                }).focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });

                
                $('#emp_sal_monthss').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "yy-mm",
                    showButtonPanel: true,
                    currentText: "This Month",
                    onChangeMonthYear: function (year, month, inst) {
                        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                    },
                    onClose: function(dateText, inst) {
                        var month = $(".ui-datepicker-month :selected").val();
                        var year = $(".ui-datepicker-year :selected").val();
                        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                    }
                }).focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });
             setTimeout(function() {
                    $('#alert_message').fadeOut('fast');
                }, 5000);  
          $("#ot_type").change(function(){
            var select_type= $("#ot_type").val();
            if(select_type==1){
              $("#department_type").show();
              $("#month_wise").hide();
              $("#section_type").hide();
              $("#employee_type").hide();
            }

            if(select_type==2){
              $("#section_type").show();
              $("#month_wise").hide();
              $("#department_type").hide();
              $("#employee_type").hide();
            }

            if(select_type==3){
              $("#employee_type").show();
              $("#month_wise").hide();
              $("#department_type").hide();
              $("#section_type").hide();
            }

            if(select_type==4){
              $("#month_wise").show();
              $("#employee_type").hide();
              $("#department_type").hide();
              $("#section_type").hide();
            }
          });
        });
    </script>
    @include('include.copyright')
@endsection