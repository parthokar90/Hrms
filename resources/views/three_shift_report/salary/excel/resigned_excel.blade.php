
<!DOCTYPE html>
<html>
<head>
    <title>Resigned Salary Sheet</title>
</head>
<body>
        {{-- <button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button> --}}
<div id="print_area">
        <div class="main_div">

            <div class="salary_table">
                <table class="table_1">
                    <thead>
                        <tr style="text-align: center;">
                            <tr><td></td><td colspan="16" align="center"><p><b>RESIGNED EMPLOYEE SALARY SHEET</b></p></td></tr>

                            <tr><td></td><td colspan="16" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
                            <tr><td></td><td colspan="16" align="center"><b>Pay Sheet for the month of  @if(!empty($monthname->month))
                                    <b>{{date('F-Y',strtotime($monthname->month))}} (3-Shift)</b>
                                @else
                                    No Data
                                @endif</b></td></tr>
                            <tr><td></td><td colspan="16" align="center"><p><b>Days in Month: @if(!empty($monthname->month))
                                    {{date('t',strtotime($monthname->month))}}
                                @endif</b></p></td></tr>
                            <tr><td></td></tr>
                        </tr>

                    
                            
                    {{-- <tr style="text-align: center;" class="company_heading_1">
                        <td colspan="30"> 
                            <p>LEFTY EMPLOYEE SALARY SHEET</p>
                         <h2>Fin Bangla Apparels Ltd.</h2>
                         <p>Pay Sheet for the month of  @if(!empty($monthname->month))
                                <b>{{date('F-Y',strtotime($monthname->month))}}</b>
                            @else
                                No Data
                            @endif</p>
                         <p>Days in Month: @if(!empty($monthname->month))
                                {{date('t',strtotime($monthname->month))}}
                            @endif</p>
                     </td>
                    </tr>  --}}
                    
                    <tr>
                        <th> SN </th>
                        <th> EmpID </th>
                        <th> Name</th>
                        <th> Section</th>
                        <th>Department</th>
                        <th>Designation</th>
                        <th>Grade</th>
                        <th>Basic</th>
                        <th>Gross</th>
                        <th>Leave </th>
                        <th>Work days</th>
                        <th>Abs Days</th>
                        <th>Weekend</th>
                        <th>Holiday</th>
                        <th>Payable days</th>
                        {{-- <th style="text-align: center" colspan="3" >Deduction</th> --}}
                        <th>Adv Deduction</th>
                        <th>Abs Deduction</th>
                        <th>Stm Deduction</th>
                        <th>Gross Pay</th>
                        {{-- <th style="text-align: center" colspan="3">Overtime</th> --}}
                        <th>Overtime Hrs</th>
                        <th>Overtime Rate</th>
                        <th>Overtime Amt</th>
                        <th >Att Bonus</th>
                        <th >Special Allow</th>
                        <th >Net Wages</th>
                        <th id="signature_id" >  Signature </th>
                    </tr>
                   
                    </thead>
                    <tbody>
                        @php $order=0; $total_basic=0; $total_gross=0; $total_gross_pay=0; $total_ot_hours=0; $t_amounts=0; $atts_bonus=0; $special_allow=0; $net_wages=0; @endphp
                        @foreach($data as $salary)
                        @php $order++; $total_basic+=$salary->basic_salary; $total_gross+=$salary->gross; $total_gross_pay+=$salary->gross_pay; $total_ot_hours+=$salary->overtime; $t_amounts+=$salary->overtime_amount;$atts_bonus+=$salary->attendance_bonus;$special_allow+=$salary->increment_bonus_percent;  $net_wages+=$salary->net_amount; @endphp
                        <tr>
                        <td style="text-align:left;">{{$order}}</td>
                            <td style="text-align:left;">{{$salary->s_emp_id}}</td>
                            <td style="text-align:left;">{{$salary->e_first_name}} 
                                <p> Join: {{date('d-m-Y',strtotime($salary->empJoiningDate))}}</p>
                            </td>
                            <td style="text-align:left;">{{$salary->empSection}}</td>
                            <td style="text-align:left;">{{$salary->departmentName}}</td>
                            <td style="text-align:left;">{{$salary->designation}}</td>
                            <td style="text-align:left;">{{$salary->grade_name}}</td>
                            <td style="text-align:left;">{{round($salary->basic_salary)}}</td>
                            <td style="text-align:left;">{{round($salary->gross)}}</td>
                            <td style="text-align:left;">
                             @if($salary->total=='')
                                 0
                               @else
                                {{$salary->total}}
                             @endif
                           </td>
                            <td style="text-align:left;">   
                                @if($salary->present=='')
                                    0
                                    @else
                                    {{$salary->present}}
                                @endif
                            </td>
                            <td style="text-align:left;">
                                {{$salary->absent}}
                            </td>
                            
                            <td style="text-align:left;">{{$salary->weekend}}</td>
                            <td style="text-align:left;"> @if($salary->holiday=='')
                                    0
                                    @else
                                    {{$salary->holiday}}
                                @endif
                            </td>
                            <td style="text-align:left;">@if($salary->leave==1) @php $payable=$salary->present+$salary->weekend+$salary->holiday; @endphp @else @php $payable=$salary->present+$salary->weekend+$salary->holiday+$salary->total; @endphp @endif {{$payable}}</td>
                            <td style="text-align:left;">
                                @if($salary->advanced_deduction=='')
                                    0
                                @else
                                    {{$salary->advanced_deduction}}
                                @endif
                            </td>
                            <td style="text-align:left;">
                               @if($salary->absent_deduction_amount=='')
                                    0
                                @else
                                    {{round($salary->absent_deduction_amount)}}
                                @endif
                            </td >
                            <td style="text-align:left;">
                                @if($salary->net_amount<=1000) 0
                                @else
                                    @if($salary->payment_mode=='Bank' || $salary->payment_mode=='bKash')
                                    0
                                @else
                                    10
                                @endif
                                @endif

                            </td>

                            <td style="text-align:left;">@if($salary->gross_pay=='')
                                    0
                                @else
                                    {{round($salary->gross_pay)}}
                                @endif
                            </td>
                            <td style="text-align:left;">
                                @if($salary->overtime=='')
                                    0
                                @else
                                    {{$salary->overtime}}
                                @endif
                            </td>
                            <td style="text-align:left;">
                                @if($salary->overtime_rate=='')
                                    0
                                @else
                                    {{round($salary->overtime_rate,2)}}
                                @endif
                            </td>
                            <td style="text-align:left;">
                                @if($salary->overtime_amount=='')
                                    0
                                @else
                                    {{round($salary->overtime_amount)}}
                                @endif
                            </td>
                            <td style="text-align:left;">{{$salary->attendance_bonus}}</td>
                            <td style="text-align:left;">  @if($salary->increment_bonus_percent=='')
                                    0
                                @else
                                    {{$salary->increment_bonus_percent}}
                                @endif
                            </td>
                            <td style="text-align:left;">
                                @php
                                    $total=$salary->net_amount
                                @endphp
                                {{round($total)}}
                            </td>
                            <td  style="text-align:left;"></td>
                        </tr>
                        @endforeach
                                                               
                        <tr style="padding: 3px 3px;">
                            <td style="border-bottom: 1px solid #555;text-align: left;" >Net</td>
                            <td  ></td>
                            <td  ></td>
                            <td  ></td>
                            <td  ></td>
                            <td  ></td>
                            <td  ></td>
                        <td  style="text-align:left;">{{round($total_basic)}}</td>
                        <td  style="text-align:left;">{{round($total_gross)}}</td>
                            <td  ></td>
                            <td   ></td>
                            <td   ></td>
                            <td   ></td>
                            <td   ></td>
                            <td  ></td>
                            <td  ></td>
                            <td  ></td>
                            <td  ></td>
                        <td  style="text-align:left;">{{round($total_gross_pay)}}</td>
                        <td  style="text-align:left;">{{$total_ot_hours}}</td>
                            <td  ></td>
                        <td  style="text-align:left;">{{round($t_amounts)}}</td>
                        <td style="text-align:left;">{{$atts_bonus}}</td>
                        <td  style="text-align:left;">{{$special_allow}}</td>
                        <td  style="text-align:left;">{{round($net_wages)}}</td>
                        <td  ></td>
                        </tr>
                    <tr></tr>
                    <tr></tr>
                       <tr>
                            <td colspan="2"></td>
                            <td colspan="2"><p>Prepared By</p></td>
                            <td colspan="1"><p>Audited By</p></td>
                            <td colspan="3"><p>Recommended By</p></td>
                            <td colspan="4"><p>Director</p></td>

                        </tr>
                
                    </tbody>
                    <tfoot>
                    {{-- <tr>
                        <td colspan="31" style="padding-top:20px;border:none;border-top:4px solid #ffffff;" >
                            <div class="authority">
                                <div class="prepared_by">
                                    <span>Prepared By</span>
                                </div>
                                <div class="audited_by">
                                    <span>Audited By</span>
                                </div>
                                <div class="recomended_by">
                                    <span>Recommended By</span>
                                </div>
                                <div class="approved_by">
                                    <span>Director</span>
                                </div>
                            </div>
                        </td>
                    </tr> --}}
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

</body>
</html>
