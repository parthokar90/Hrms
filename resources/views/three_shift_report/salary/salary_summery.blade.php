@extends('layouts.master')
@section('title', 'Employee Salary Summary Report')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span style="font-size: 14px;font-weight: bold;">Salary Summary Report(3-Shift)</span>
            </div>
            <div class="panel-body">
                    <div class="col-md-12">
                            <div class="form-group">
                                    <label>Select Type</label>
                                    <select id="ot_type" name="ot_type" class="form-control">
                                               <option value="">Select</option>
                                               <option value="4">Working Group</option>
                                               <option value="1">Department Wise</option>
                                               <option value="2">Section Wise</option>
                                               <option value="3">Month Wise</option>
                                </select>    
                          </div>
                    </div>
                    <div id="group_wise" style="padding: 0;display:none">
                            <div class="col-md-12">
                                {{Form::open(array('url' => 'report/salary/summery/monthwise/three/shift','method' => 'post'))}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                                <label>Select Working Group</label>
                                                <select class="form-control" name="group_id" required>
                                                    <option value="all_group">All</option>
                                                    @foreach($work_group as $group)
                                                    <option value="{{$group->work_group}}">{{$group->work_group}}</option>
                                                    @endforeach
                                                </select>    
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Month</label>
                                        <input type="text" class="form-control" name="salary_summery_monthsss" id="group_summary" placeholder="Select Month" autocomplete="off" value="{{date('Y-m')}}" required>
                                    </div>
                                </div>
                                <input type="hidden" name="type" value="group_wise_data">
                                <div class="col-md-12">
                                <button type="submit" id="salsummerymonth" name="salary_summery_month" class="btn btn-info">Preview</button>
                                <button type="submit" name="download_excel" value="download_excel"  class="btn btn-success">Download Excel</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div> 
                    <div id="department_wise" style="padding: 0;display:none">
                        <div class="col-md-12">
                            {{Form::open(array('url' => 'report/salary/summery/monthwise/three/shift','method' => 'post'))}}
                            <div class="col-md-6">
                                <div class="form-group">
                                            <label>Select Department</label>
                                            <select class="form-control" name="department_id" required>
                                                <option value="all_department">All</option>
                                                @foreach($department as $departments)
                                                <option value="{{$departments->id}}">{{$departments->departmentName}}</option>
                                                @endforeach
                                            </select>    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Select Month</label>
                                    <input type="text" class="form-control" name="salary_summery_monthsss" id="year_bonus_month" placeholder="Select Month" autocomplete="off" value="{{date('Y-m')}}" required>
                                </div>
                            </div>
                            <input type="hidden" name="type" value="department_wise_data">
                            <div class="col-md-12">
                            <button type="submit" id="salsummerymonth" name="salary_summery_month" class="btn btn-info">Preview</button>
                            <button type="submit" name="download_excel" value="download_excel"  class="btn btn-success">Download Excel</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>   
                    <div id="section_wise" style="padding: 0;display:none">
                      <div class="col-md-12">
                            {{Form::open(array('url' => 'report/salary/summery/monthwise/three/shift','method' => 'post'))}}
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label>Select Section</label>
                                        <select class="form-control" name="section_id">
                                        <option value="all_section">All</option>
                                        @foreach($section as $sections)
                                                 <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                        @endforeach
                                   </select>    
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                <label>Select Month</label>
                                <input type="text" class="form-control" name="salary_summery_monthsss" id="section_summary" placeholder="Select Month" autocomplete="off" value="{{date('Y-m')}}" required>
                            </div>
                            </div>
                            <input type="hidden" name="type" value="section_wise_data">
                            <div class="col-md-12">
                              <button type="submit" id="salsummerymonth" name="salary_summery_month" class="btn btn-info">Preview</button>
                              <button type="submit" name="download_excel" value="download_excel"  class="btn btn-success">Download Excel</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                     </div> 
                     <div id="month_wise" style="padding:0;display:none">
                            <div class="col-md-12">
                              {{Form::open(array('url' => 'report/salary/summery/monthwise/three/shift','method' => 'post'))}}
                               <div class="col-md-12" style="padding: 0">
                                   <div class="form-group">
                                      <label>Select Month</label>
                                      <input type="text" class="form-control" name="salary_summery_monthsss" id="month_summary" placeholder="Select Month" autocomplete="off" value="{{date('Y-m')}}" required>
                                   </div>
                                   <input type="hidden" name="type" value="all_data">
                                <button type="submit" id="salsummerymonth" name="salary_summery_month" class="btn btn-info">Preview</button>
                                <button type="submit" name="download_excel" value="download_excel"  class="btn btn-success">Download Excel</button>
                              </div>
                              {{ Form::close() }}
                         </div>
                     </div>
               </div>
          </div>
     </div>
    </div>
    @include('include.copyright')
    <script>
        $(document).ready(function() {
            $('#year_bonus_month').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
            $('#month_summary').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
            $('#section_summary').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });


            $('#group_summary').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });

             $("#ot_type").change(function(){
                        var select_type= $("#ot_type").val();
                        if(select_type==1){
                        $("#department_wise").show();
                        $("#section_wise").hide();
                        $("#month_wise").hide();
                        $("#group_wise").hide();
                        }

                        if(select_type==2){
                        $("#section_wise").show();
                        $("#month_wise").hide();
                        $("#department_wise").hide();
                        $("#group_wise").hide();
                        }

                        if(select_type==3){
                        $("#group_wise").hide();
                        $("#month_wise").show();
                        $("#department_wise").hide();
                        $("#section_wise").hide();
                        }

                        if(select_type==4){
                        $("#group_wise").show();
                        $("#department_wise").hide();
                        $("#section_wise").hide();
                        $("#month_wise").hide();
                        }
                });
        });
    </script>
@endsection