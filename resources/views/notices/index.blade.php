@extends('layouts.master')
@section('title', 'Notices List')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{Session::get('message')}}</p>
        @endif
        @if(Session::has('fmessage'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('fmessage')}}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Notices </strong> List</h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th> # </th>
                                <th width="25%">Notice</th>
                                <th>Published_Date</th>
                                <th>Starting_Date</th>
                                <th>Ending_Date</th>
                                <th>Status</th>
                                <th>Created_By</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($notices as $notice)
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td>
                                       <?php echo substr($notice->noticeDescription, 0, 120)."..."; ?>
                                    </td>
                                    <td>
                                        {{\Carbon\Carbon::parse($notice->created_at)->format('d-M-Y')}}
                                    </td>
                                    <td>
                                        {{\Carbon\Carbon::parse($notice->startDate)->format('d-M-Y')}}
                                    </td>
                                    <td>
                                        {{\Carbon\Carbon::parse($notice->endDate)->format('d-M-Y')}}
                                    </td>
                                    <td>
                                        <?php if($notice->status==1){ echo "<span class='label label-success'>Active</span>";}?>
                                        <?php if($notice->status==0){ echo "<span class='label label-danger'>Inactive</span>";}?>
                                    </td>
                                    <td>
                                        {{$notice->created_by}}
                                    </td>
                                    <td>
                                        <a href="#"  style="float:left;" class="btn btn-success btn-sm" data-toggle="modal" data-target="{{'#viewmodel'.$notice->id}}"><i class="fa fa-eye"></i></a>
                                        <a href="#" style="float:left;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="{{'#editmodal'.$notice->id}}"><i class="fa fa-edit"></i></a>
                                        <a href="#" style="float:left;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="{{'#deletemodel'.$notice->id}}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                                <div class="modal fade" id="{{'editmodal'.$notice->id}}" role="dialog">
                                    <div class="modal-dialog">

                                         <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title"><b>Edit Notice Information</b></h4>
                                            </div>
                                          <div class="modal-body">

                                              {!! Form::open(['method'=>'PATCH','action'=>['noticeController@update',$notice->id]]) !!}

                                              <div class="required form-group">
                                                <label class="control-label">Notice Starting Date</label>
                                                <div class="append-icon">
                                                    <input type="date" name="startDate" class="date-picker form-control" value="{{Carbon\Carbon::parse($notice->startDate)->format('Y-m-d')}}" required >
                                                </div>
                                            </div>

                                            <div class="required form-group">
                                                <label class="control-label">Notice Ending Date</label>
                                                <div class="append-icon">
                                                    <input type="date" name="endDate" class="date-picker form-control" value="{{Carbon\Carbon::parse($notice->endDate)->format('Y-m-d')}}" required >
                                                </div>
                                            </div>

                                            <div class="required form-group">
                                                <label class="control-label">Notice</label>
                                                <div class="append-icon">
                                                    <textarea rows="8" name="noticeDescription" class="form-control" placeholder="Minimum 100 Character..." required >{{$notice->noticeDescription}}</textarea>
                                                </div>
                                            </div>

                                            <div class="required form-group">
                                                <label class="control-label">Notice Status</label>
                                                <div class="append-icon">
                                                    <select name="status" class="form-control">
                                                        <option <?php if($notice->status==1){ echo "selected";}?> value="1">Active</option>
                                                        <option <?php if($notice->status==0){ echo "selected";}?> value="0">Inactive</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="text-center modal-footer">
                                                <button type="submit" class="btn btn-success">Update</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            
                                            {!! Form::close() !!}
                                        </div>

                                    </div>

                                </div>
                            </div>




                            <!-- Modal -->
                            <div class="modal fade" id="{{'deletemodel'.$notice->id}}" role="dialog">
                                <div class="modal-dialog">
                                
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title"><b>Delete Notice</b></h4>
                                        </div>
                                        <div class="modal-body">

                                        {!! Form::open(['method'=>'DELETE','action'=>['noticeController@destroy',$notice->id]]) !!}

                                            <div class="text-center modal-footer">
                                                <button type="submit" class="btn btn-danger">Confirm Delete</button>
                                                
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close!</button>
                                            </div>

                                        {!! Form::close() !!} 

                                      </div>
                                  
                                    </div>
                                </div>
                            </div>
  

                            <!-- Modal -->
                            <div class="modal fade" id="{{'viewmodel'.$notice->id}}" role="dialog">
                                <div class="modal-dialog">
                                
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title"><b>View Notice</b></h4>
                                        </div>
                                        <div class="modal-body">
                                        {{$notice->noticeDescription}}

                                      </div>
                                      <div class="modal-footer">

                                      </div>
                                  
                                    </div>
                                </div>
                            </div>
  

                                @endforeach
                            </tbody>                     
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

  @include('include.copyright')
@endsection