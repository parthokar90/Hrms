@extends('layouts.master')
@section('title', 'Vehicle Booking report')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Vehicle Booking report</b></h2>
                    <hr>
                </div>
            </div>
            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
            @endif
            {!! Form::open(['method'=>'POST','route'=>'vehicle.report.vehicle_booking_report_data']) !!}
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Start Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="start_date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select a date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">End Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="end_date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select a date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Vehicle<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="vehicleId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($vehicle_info as $vi)
                                <option value="{{$vi->id}}">{{$vi->vehicleName}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group padding-bottom-30-percent">
                    <div class="col-md-9 col-md-offset-3">
                        <hr>
                        <input type="submit" value="Preview" name="viewType" class="btn btn-success margin-top-10">
                    </div>
                </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>

    @include('include.copyright')
@endsection