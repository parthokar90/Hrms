@extends('layouts.master')
@section('title', 'Vehicle Expense List')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 ">
                <div class="panel">
                    <center><h3>Expense Report from <b>{{\Carbon\Carbon::parse($request->start_date)->format('d-M-Y')}}</b> to <b>{{\Carbon\Carbon::parse($request->end_date)->format('d-M-Y')}}</b></h3><br></center>
                </div>
            </div>
            <div class="col-lg-6 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-money"></i> <strong>Fuel Expenses </strong></h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                               
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic" border="1px">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Vehicle Name</th>
                                <th>Fuel Type</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; $totfuel=0; $toil=0;$tgas=0; $toilm=0;$tgasm=0; @endphp
                            @foreach($vfuel_expense as $vi)
                            @php $totfuel+=($vi->amount); @endphp
                                <tr>
                                    <td>
                                        {{\Carbon\Carbon::parse($vi->vexDate)->format('d-M-Y')}}
                                    </td>
                                    <td>{{$vi->vehicleName}} ({{$vi->registrationNumber}})</td>
                                    <td><?php if($vi->fuelType==1){ echo "Oil"; $toil+=$vi->quantity;$toilm+=$vi->amount;} if($vi->fuelType==2){ echo "Gas";$tgas+=$vi->quantity;$tgasm+=$vi->amount;} ?></td>
                                    <td>{{$vi->quantity}}<?php if($vi->fuelType==1){ echo " ltr";} ?></td>
                                    <td>@money($vi->amount)</td>
                                    <td>
                                        <a data-toggle="modal" title="Details" data-target="#fuelView{{$vi->id}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>

                                <div class="modal fade" id="fuelView{{$vi->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Expense Details</strong></h5>
                                                
                                            </div>
                                            <div class="modal-body">
                                                    <div class="form-group">
                                                      <label for="vehicleName"> Vehicle Name</label>
                                                      <input disabled="" value="{{$vi->vehicleName}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="fuelType"> Fuel Type</label>
                                                      <input disabled="" value="<?php if($vi->fuelType==1){ echo "Oil";} if($vi->fuelType==2){ echo "Gas";} ?>" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="quantity"> Quantity</label>
                                                      <input disabled="" value="{{$vi->quantity}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="amount"> Price Amount</label>
                                                      <input disabled="" value="{{$vi->amount}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexDate"> Date</label>
                                                      <input disabled="" value=" {{\Carbon\Carbon::parse($vi->vexDate)->format('d-M-Y')}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexReferences"> References</label>
                                                      <input disabled="" value=" {{$vi->vexReferences}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexDescription"> Description</label>
                                                      <textarea disabled="" value="" class="form-control" >{{$vi->vexDescription}}</textarea>
                                                    </div>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                            <thead>
                                <tr>
                                    <th colspan="3"></th>
                                    <th>Total</th>
                                    <th>@money($totfuel)</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-money"></i> <strong>Extra Expenses</strong></h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                               
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic" border="1px">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Vehicle Name</th>
                                <th>Category</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; $totvex=0; @endphp
                            @foreach($vehicle_expenses as $vi)
                            @php $totvex+=($vi->amount); @endphp
                                <tr>
                                    <td>
                                        {{\Carbon\Carbon::parse($vi->vexDate)->format('d-M-Y')}}
                                    </td>
                                    <td>{{$vi->vehicleName}} ({{$vi->registrationNumber}})</td>
                                    <td>{{$vi->vexName}}</td>
                                    <td>{{$vi->quantity}}</td>
                                    <td>@money($vi->amount)</td>
                                    <td>
                                        <a data-toggle="modal" title="Details" data-target="#xtraView{{$vi->id}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>

                                <div class="modal fade" id="xtra{{$vi->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Expense Details</strong></h5>
                                                
                                            </div>
                                            <div class="modal-body">
                                                    <div class="form-group">
                                                      <label for="vehicleName"> Vehicle Name</label>
                                                      <input disabled="" value="{{$vi->vehicleName}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="fuelType"> Expense Category</label>
                                                      <input disabled="" value="{{$vi->vexName}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="quantity"> Quantity</label>
                                                      <input disabled="" value="{{$vi->quantity}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="amount"> Price Amount</label>
                                                      <input disabled="" value="{{$vi->amount}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexDate"> Date</label>
                                                      <input disabled="" value=" {{\Carbon\Carbon::parse($vi->vexDate)->format('d-M-Y')}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexReferences"> References</label>
                                                      <input disabled="" value=" {{$vi->vexReferences}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexDescription"> Description</label>
                                                      <textarea disabled="" value="" class="form-control" >{{$vi->vexDescription}}</textarea>
                                                    </div>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                            <thead>
                                <tr>
                                    <th colspan="3"></th>
                                    <th>Total</th>
                                    <th>@money($totvex)</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-lg-offset-3 ">
                <div class="panel">
                    <center><h3>Total Oil: <b>{{$toil}} ltr </b> and Price Amount: <b>@money($toilm)</b></h3></center>
                    <center><h3>Total Gas: <b>{{$tgas}}  </b> and Price Amount: <b>@money($tgasm)</b></h3></center>
                    <center><h3>Total Extra Expenses: <b>@money($totvex)  </b></b></h3></center>
                </div>
            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection