@extends('layouts.master')
@section('title', 'Booked Vehicle List')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-car"></i> <strong>Booked Vehicle </strong> List</h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                                <a data-toggle="modal" data-target="#new_booking" class="btn btn-primary btn-round btn-sm"><i class="fa fa-plus"></i>New Booking</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Vehicle Name</th>
                                <th>Booked By</th>
                                <th>Driver</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Status</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($booking_list as $vi)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$vi->vehicleName}}</td>
                                    <td>{{$vi->bookedBy}}</td>
                                    <td>{{$vi->driverName}}</td>
                                    <td>{{$vi->vbDate}}</td>
                                    <td>{{date("h:i a", strtotime($vi->vbStartTime))}}-{{date("h:i a", strtotime($vi->vbEndTime))}}</td>
                                    <td><?php if($vi->status==0){ echo "<span style='color:green;'>Active</span>"; }
                                    if($vi->status==1){ echo "<span style='color:red;'>Cancelled</span>"; } ?></td>
                                    <td style="text-align: center;">
                                        <a data-toggle="modal" title="Purpose" data-target="#view{{$vi->id}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Purpose</a>
                                        <?php if($vi->status==0){ ?>
                                        <a data-toggle="modal" title="Update Booking Time" data-target="#edit{{$vi->id}}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Update </a>
                                        <a data-toggle="modal" title="Cancel Booking" data-target="#delete{{$vi->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</a>
                                        <?php } ?>
                                    </td>
                                </tr>


                                <div class="modal fade" id="view{{$vi->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Purpose of Booking</strong></h5>
                                            </div>
                                            <div class="modal-body">
                                                <h5><b>{{$vi->purpose}}</b></h5>
                                                <hr>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade" id="edit{{$vi->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Update Info</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['method'=>'POST', 'route'=>['vehicle.update_booking']]) !!}
                                                    <div class="form-group">
                                                      <label for="vexName"> Vehicle</label>
                                                     <select class="form-control" name="vehicleId"   data-search="true" required="" >
                                                         <option value="">Select Vehicle</option>
                                                        @foreach($vehicle_info as $vii)
                                                         <option <?php if($vii->id==$vi->vehicleId){ echo "selected";} ?> value="{{$vii->id}}">{{$vii->vehicleName}}</option>
                                                        @endforeach
                                                     </select>
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="bookedBy"> Booked By</label>
                                                      <input type="text" value="{{$vi->bookedBy}}" name="bookedBy" class="form-control" required="" placeholder="Booked By" >
                                                    </div>

                                                    <div class="form-group">
                                                      <label for="driverName"> Driver Name</label>
                                                      <input type="text" value="{{$vi->driverName}}" name="driverName" class="form-control" required="" placeholder="Driver Name" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vbDate"> Booking Date</label>
                                                      <input type="text" value="{{$vi->vbDate}}" name="vbDate" class="date-picker form-control" required="" placeholder="Date" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vbStartTime"> Booking Starting Time</label>
                                                      <input type="time" value="{{$vi->vbStartTime}}" name="vbStartTime" class="form-control" required="" placeholder="Booking Starting Time" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vbEndTime"> Booking Ending Time</label>
                                                      <input type="time" value="{{$vi->vbEndTime}}" name="vbEndTime" class="form-control" required="" placeholder="Booking Ending Time" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="purpose"> Purpose</label>
                                                      <textarea name="purpose" class="form-control" placeholder="Booking Purpose" >{{$vi->purpose}}</textarea>
                                                    </div>

                                                    <div class="form-group">
                                                    <input type="hidden" name="id" value="{{$vi->id}}">
                                                    <button type="submit" class="btn btn-primary">Update Changes</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!--End of Edit Model-->

                                <!-- Delete Modal -->
                                <div class="modal fade" id="delete{{$vi->id}}" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Confirm Cencel</strong></h5>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Do you want to cancel this booking?</strong></p>
                                                {!! Form::open(['method'=>'POST', 'route'=>['vehicle.booking_cancel']]) !!}
                                                <input type="hidden" name="id" value="{{$vi->id}}">
                                                <button type="submit" class="btn btn-danger">Confirm Cencel</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                {!! Form::close() !!}
                                            </div>
                                            
                                            <div class="modal-footer">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--Delete Modal Ended-->

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Add New Modal -->
    <div class="modal fade" id="new_booking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>New Booking Information</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                        {!! Form::open(['method'=>'POST', 'route'=>['vehicle.store_booking']]) !!}
                        <div class="form-group">
                          <label for="vexName"> Vehicle</label>
                         <select class="form-control" name="vehicleId"   data-search="true" required="" >
                             <option value="">Select Vehicle</option>
                            @foreach($vehicle_info as $vii)
                             <option value="{{$vii->id}}">{{$vii->vehicleName}}</option>
                            @endforeach
                         </select>
                        </div>
                        <div class="form-group">
                          <label for="bookedBy"> Booked By</label>
                          <input type="text" name="bookedBy" class="form-control" required="" placeholder="Booked By" >
                        </div>

                        <div class="form-group">
                          <label for="driverName"> Driver Name</label>
                          <input type="text" name="driverName" class="form-control" required="" placeholder="Driver Name" >
                        </div>
                        <div class="form-group">
                          <label for="vbDate"> Booking Date</label>
                          <input type="text" name="vbDate" class="date-picker form-control" required="" placeholder="Date" >
                        </div>
                        <div class="form-group">
                          <label for="vbStartTime"> Booking Starting Time</label>
                          <input type="time" name="vbStartTime" class="form-control" required="" placeholder="Booking Starting Time" >
                        </div>
                        <div class="form-group">
                          <label for="vbEndTime"> Booking Ending Time</label>
                          <input type="time" name="vbEndTime" class="form-control" required="" placeholder="Booking Ending Time" >
                        </div>
                        <div class="form-group">
                          <label for="purpose"> Purpose</label>
                          <textarea name="purpose" required="" class="form-control" placeholder="Booking Purpose" ></textarea>
                        </div>

                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">Confirm Booking</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut();
        }, 10000);
    </script>


    @include('include.copyright')
@endsection