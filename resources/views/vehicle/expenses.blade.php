@extends('layouts.master')
@section('title', 'Vehicle Expense List')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>
        @endif
        @if(Session::has('edit'))
            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>
        @endif
        @if(Session::has('fileSize'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('fileSize')}}</p>
        @endif

        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 ">
                <div class="panel" style="padding: 5px 5px; text-align: center;">
                    <h2><b>Today's expense list ({{date('d M Y')}})</b></h2>
                    <br>
                  <a data-toggle="modal" data-target="#newfuelexpense" class="btn btn-primary btn-round btn-md"><i class="fa fa-plus"></i> New Fuel Expense</a>
                  <a data-toggle="modal" data-target="#newextraexpense" class="btn btn-primary btn-round btn-md"><i class="fa fa-plus"></i> New Others Expense</a>
                    
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-money"></i> <strong>Fuel Expenses </strong></h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                               
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered  table-dynamic" border="1px">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Vehicle Name</th>
                                <th>Fuel Type</th>
                                <th>Quantity</th>
                                <th>Amount</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; $totfuel=0; @endphp
                            @foreach($vfuel_expense as $vi)
                            @php $totfuel+=($vi->amount); @endphp
                                <tr>
                                    <td>
                                        {{\Carbon\Carbon::parse($vi->vexDate)->format('d-M-Y')}}
                                    </td>
                                    <td>{{$vi->vehicleName}}</td>
                                    <td><?php if($vi->fuelType==1){ echo "Oil";} if($vi->fuelType==2){ echo "Gas";} ?></td>
                                    <td>{{$vi->quantity}}</td>
                                    <td>@money($vi->amount)</td>
                                    <td>
                                        <a data-toggle="modal" title="Details" data-target="#fuelView{{$vi->id}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>

                                <div class="modal fade" id="fuelView{{$vi->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Expense Details</strong></h5>
                                                
                                            </div>
                                            <div class="modal-body">
                                                    <div class="form-group">
                                                      <label for="vehicleName"> Vehicle Name</label>
                                                      <input disabled="" value="{{$vi->vehicleName}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="fuelType"> Fuel Type</label>
                                                      <input disabled="" value="<?php if($vi->fuelType==1){ echo "Oil";} if($vi->fuelType==2){ echo "Gas";} ?>" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="quantity"> Quantity</label>
                                                      <input disabled="" value="{{$vi->quantity}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="amount"> Price Amount</label>
                                                      <input disabled="" value="{{$vi->amount}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexDate"> Date</label>
                                                      <input disabled="" value=" {{\Carbon\Carbon::parse($vi->vexDate)->format('d-M-Y')}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexReferences"> References</label>
                                                      <input disabled="" value=" {{$vi->vexReferences}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexDescription"> Description</label>
                                                      <textarea disabled="" value="" class="form-control" >{{$vi->vexDescription}}</textarea>
                                                    </div>
                                                    @if($vi->vexAttachment)
                                                    <div class="form-group">
                                                       <a target="_blank" href="{{asset('vehicle_expenses/'.$vi->vexAttachment)}}"><p><button type="button" class="btn btn-custom-download"><i class="fa fa-download"></i> Download Attachment</button></p></a>
                                                    </div>
                                                    @endif
                                               
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                            <thead>
                                <tr>
                                    <th colspan="3"></th>
                                    <th>Total</th>
                                    <th>@money($totfuel)</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-money"></i> <strong>Other Expenses</strong></h3>
                            </div>
                            <div class="col-lg-6">
                               
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered  table-dynamic" border="1px">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Vehicle Name</th>
                                <th>Category</th>
                                <th>Amount</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; $totvexo=0; @endphp
                            @foreach($vehicle_expenses as $ve)
                            @php $totvexo+=($ve->amount); @endphp
                                <tr>
                                    <td>
                                        {{\Carbon\Carbon::parse($ve->vexDate)->format('d-M-Y')}}
                                    </td>
                                    <td>{{$ve->vehicleName}}</td>
                                    <td>{{$ve->vexName}}</td>
                                    <td>@money($ve->amount)</td>
                                    <td>
                                        <a data-toggle="modal" title="Details" data-target="#xtraView{{$ve->id}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>

                                <div class="modal fade" id="xtraView{{$ve->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Expense Details</strong></h5>
                                                
                                            </div>
                                            <div class="modal-body">
                                                    <div class="form-group">
                                                      <label for="vehicleName"> Vehicle Name</label>
                                                      <input disabled="" value="{{$ve->vehicleName}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="fuelType"> Expense Category</label>
                                                      <input disabled="" value="{{$ve->vexName}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="amount"> Price Amount</label>
                                                      <input disabled="" value="{{$ve->amount}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexDate"> Date</label>
                                                      <input disabled="" value=" {{\Carbon\Carbon::parse($ve->vexDate)->format('d-M-Y')}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexReferences"> References</label>
                                                      <input disabled="" value=" {{$ve->vexReferences}}" class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="vexDescription"> Description</label>
                                                      <textarea disabled="" value="" class="form-control" >{{$ve->vexDescription}}</textarea>
                                                    </div>

                                                    @if($ve->vexAttachment)
                                                    <div class="form-group">
                                                       <a target="_blank" href="{{asset('vehicle_expenses/'.$ve->vexAttachment)}}"><p><button type="button" class="btn btn-custom-download"><i class="fa fa-download"></i> Download Attachment</button></p></a>
                                                    </div>
                                                    @endif
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                            <thead>
                                <tr>
                                    <th colspan="2"></th>
                                    <th>Total</th>
                                    <th>@money($totvexo)</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <!-- Add New Modal -->
    <div class="modal fade" id="newfuelexpense" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>New Fuel Expense</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'route'=>'vehicle.expenses.newfuelexpense','files'=>true]) !!}

                    <div class="form-group">
                        <label >Vehicle</label>
                        <select required="" name="vehicleId" class="form-control"  data-search="true">
                            <option selected="" value="">Select</option>
                            @foreach($vehicle_info as $vi)
                                <option value="{{$vi->id}}">{{$vi->vehicleName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="fuelType"> Fuel Type</label>
                      <select required="" name="fuelType" class="form-control">
                          <option selected="" value=""> Select </option>
                          <option value="1"> Oil</option>
                          <option value="2"> Gas</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="quantity"> Quantity</label>
                      <input name="quantity" placeholder="Quantity"  class="form-control" type="number" step="any" required="" >
                    </div>
                    <div class="form-group">
                      <label for="amount"> Total Expense Amount</label>
                      <input name="amount" placeholder="Total Expense Amount" required="" type="number" step="any" class="form-control" >
                    </div>
                    <div class="form-group">
                      <label for="vexDate"> Date</label>
                      <input name="vexDate" placeholder="Click here to select date." value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" class="date-picker form-control" >
                    </div>
                    <div class="form-group">
                      <label for="vexReferences"> References</label>
                      <input name="vexReferences" placeholder="References"  class="form-control" >
                    </div>
                    <div class="form-group">
                      <label for="vexDescription"> Description</label>
                      <textarea name="vexDescription" placeholder="Description" class="form-control" ></textarea>
                    </div>
                    <div class="form-group">
                        {!! Form::label('newfuelAttachment','Choose Attachment:') !!} (Max Size:5mb)
                        {!! Form::file('newfuelAttachment', ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Save </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>


    <!-- Add New Modal -->
    <div class="modal fade" id="newextraexpense" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>New Extra Expense</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'route'=>'vehicle.expenses.newotherexpense','files'=>true]) !!}
                        <div class="form-group">
                            <label>Vehicle</label>
                                <select name="vehicleId" class="form-control" required="" data-search="true">
                                    <option selected="" value="">Select</option>
                                    @foreach($vehicle_info as $vi)
                                        <option value="{{$vi->id}}">{{$vi->vehicleName}}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="form-group">
                          <label for="vexCategoryId"> Expense Category</label>
                           <select name="vexCategoryId" class="form-control" required="" data-search="true">
                                <option selected="" value="">Select</option>
                                @foreach($vexcategory as $ec)
                                    <option value="{{$ec->id}}">{{$ec->vexName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="amount"> Total Expense Amount</label>
                          <input name="amount" placeholder="Total Expense Amount" required="" type="number" step="any" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label for="vexDate"> Date</label>
                            <input name="vexDate" placeholder="Click here to select date." value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" class="date-picker form-control" >
                        </div>

                        <div class="form-group">
                          <label for="vexReferences"> References</label>
                          <input name="vexReferences" placeholder="References" class="form-control" >
                        </div>
                        <div class="form-group">
                          <label for="vexDescription"> Description</label>
                          <textarea name="vexDescription" placeholder="Description" class="form-control" ></textarea>
                        </div>

                        <div class="form-group">
                            {!! Form::label('newOtherAttachment','Choose Attachment:') !!} (Max Size:5mb)
                            {!! Form::file('newOtherAttachment', ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save </button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection