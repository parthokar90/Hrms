@extends('layouts.master')
@section('title', 'Month Wise Employee Leave Summary')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading">
                            Month Wise Employee Leave Summary
                    </div>
                    <div class="panel-body">
                        {{Form::open(array('url' => 'employee/leave/summary/month/wise','method' => 'post','target'=>'_blank'))}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label">Select Month<span style="color:red;">*</span></label>
                                <div class="prepend-icon">
                                    <input type="text" autocomplete="off" name="salary_sheet_month" id="select_salary_sheet_month_card" class="form-control format_date" required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success btn-square">Generate</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
      $(document).ready(function() {
       $('#select_salary_sheet_month_card').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm-dd', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm-dd', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
        });   
    </script>
    @include('include.copyright')
@endsection