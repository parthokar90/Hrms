@extends('layouts.master')
@section('title', 'Expense History')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header">
                        <h3> <strong>Expense Histories</strong></h3>
                    </div>
                    <div class="panel-content">
                    @if(count($expenseHistory)==0)
                        <h5> <strong> No Expense Histories found from {{date("d M",strtotime($request->expense_start_date))}} to {{date("d M Y",strtotime($request->expense_end_date))}}</strong></h5>
                    @else
                        <h5> <strong>Expense Histories showing from {{date("d M",strtotime($request->expense_start_date))}} to {{date("d M Y",strtotime($request->expense_end_date))}}</strong></h5>
    
                        <div class="panel-content pagination2 table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Expense Catagory</th>
                                    <th>Reason</th>
                                    <th>Amount</th>
                                    <th>Reference</th>
                                    <th>Description</th>
                                    <th>Time</th>
                                    <th>Attachment</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                        @php
                                        $sum=0
                                        @endphp
                                @foreach($expenseHistory as $expenseData)
                                @php
                                
                                $sum=$sum+$expenseData->amount;
                                
                                @endphp
                                    <tr>
                                        <td>{{date("d-M-Y",strtotime($expenseData->expenseDate))}}</td>
                                        <td>{{$expenseData->categoryName}}</td>
                                        <td>{{$expenseData->title}}</td>
                                        <td>{{$expenseData->amount}}</td>
                                        <td>{{$expenseData->reference}}</td>
                                        <td>{{$expenseData->description}}</td>
                                        <td>{{date('h:i  A',strtotime($expenseData->created_at)+6*60*60)}}</td>
                                        <td>
                                                @if($expenseData->attachment)
                                                    <a target="_blank" href="{{asset('Expense_Attachment/'.$expenseData->attachment)}}"><p><button type="button"class="btn btn-custom-download btn-sm"><i class="fa fa-download"></i> Download</button></p></a>
                                                @else    
                                                    <h5>Not Available</h4>
                                                @endif
                                            </td>
                                    </tr>

                                    
                                @endforeach
                                </tbody>
                            </table>
                            @php 
                            function convertNumberToWord($num = false)
                            {
                                $num = str_replace(array(',', ' '), '' , trim($num));
                                if(! $num) {
                                    return false;
                                }
                                $num = (int) $num;
                                $words = array();
                                $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
                                    'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
                                );
                                $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
                                $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
                                    'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
                                    'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
                                );
                                $num_length = strlen($num);
                                $levels = (int) (($num_length + 2) / 3);
                                $max_length = $levels * 3;
                                $num = substr('00' . $num, -$max_length);
                                $num_levels = str_split($num, 3);
                                for ($i = 0; $i < count($num_levels); $i++) {
                                    $levels--;
                                    $hundreds = (int) ($num_levels[$i] / 100);
                                    $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
                                    $tens = (int) ($num_levels[$i] % 100);
                                    $singles = '';
                                    if ( $tens < 20 ) {
                                        $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
                                    } else {
                                        $tens = (int)($tens / 10);
                                        $tens = ' ' . $list2[$tens] . ' ';
                                        $singles = (int) ($num_levels[$i] % 10);
                                        $singles = ' ' . $list1[$singles] . ' ';
                                    }
                                    $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
                                } //end for loop
                                $commas = count($words);
                                if ($commas > 1) {
                                    $commas = $commas - 1;
                                }
                                return implode(' ', $words);
                            }
                            @endphp
                            <h3>Total expense is : <strong>{{number_format($sum,2)}}</strong> ({{convertNumberToWord($sum)}}) Taka Only</h3>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
                            
@include('include.copyright')
@endsection