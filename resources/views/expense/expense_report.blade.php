@extends('layouts.master')
@section('title', 'Expense Report')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Expense Report</b></h2>
                    <hr>
                </div>
            </div>

            {!! Form::open(['method'=>'POST','action'=>'expenseController@expense_report_data','target'=>'_blank']) !!}
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">
                @if(Session::has('message'))
                    <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
                @endif

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Start Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="start_date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select a date...">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">End Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="end_date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select a date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Category <span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="category">
                            <option  value="0">All</option>
                            @foreach($categoryList as $cl)
                                <option  value="{{$cl->id}}">{{$cl->categoryName}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{--<div class="form-group">--}}
                    {{--<div class="input-form-gap"></div>--}}
                    {{--<div class="input-form-gap"></div>--}}
                    {{--<label class="col-md-3">Page Size<span class="clon">:</span></label>--}}
                    {{--<div class="col-md-9">--}}
                        {{--<select class="form-control" name="pagesize">--}}
                            {{--<option selected value="A4">A4</option>--}}
                            {{--<option value="Legal">Legal</option>--}}
                            {{--<option value="Letter">Letter</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                {{--</div>--}}


                {{--<div class="form-group">--}}
                    {{--<div class="input-form-gap"></div>--}}
                    {{--<label class="col-md-3">Page Orientation<span class="clon">:</span></label>--}}
                    {{--<div class="col-md-9">--}}
                        {{--<select class="form-control" name="pageOrientation">--}}
                            {{--<option selected value="Portrait">Portrait</option>--}}
                            {{--<option value="Landscape">Landscape</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                {{--</div>--}}




                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Excel Extension<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="extensions">
                            <option selected value="xls">XLS</option>
                            <option value="xlsx">XLSX</option>
                        </select>
                    </div>
                </div>



                <div class="form-group padding-bottom-30-percent">
                    <div class="col-md-9 col-md-offset-3">
                        <hr>
                        <input type="submit" value="Preview" name="viewType" class="btn btn-success margin-top-10">

                        <span class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Generate/Export
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> &nbsp; Generate PDF </button></li>
                                    <li><button type="submit" value="Generate Excel" name="viewType" class="btn btn-blue"><i class="fa fa-file-excel-o"></i> &nbsp; Generate Excel </button></li>
                                </ul>
                            </span>


                        <input type="reset" value="Clear" class="btn btn-warning margin-top-10">
                    </div>
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>



    @include('include.copyright')
@endsection