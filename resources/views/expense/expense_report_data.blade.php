@extends('layouts.master')
@section('title', 'Expense Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3><strong>Expense Report</strong> From {{\Carbon\Carbon::parse($request->start_date)->format('d-m-Y')}} to {{\Carbon\Carbon::parse($request->end_date)->format('d-m-Y')}}</h3>

                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-dynamic table-hover table-striped table-bordered">
                            <thead style="font-size:15px;font-weight:bold;">
                                <tr>
                                    <th>Title</th>
                                    <th>Amount</th>
                                    <th>Reference</th>
                                    <th>Date</th>
                                    <th>Category</th>
                                    <th>Description</th>
                                    <th>Attachments</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                    <tr>
                                        <td>{{$d->title}}</td>
                                        <td>{{$d->amount}}</td>
                                        <td>{{$d->reference}}</td>
                                        <td>{{$d->expenseDate}}</td>
                                        <td>{{$d->categoryName}}</td>
                                        <td>{{$d->description}}</td>
                                        <td>
                                            @if($d->attachment==null)
                                                None
                                            @else
                                                <a target="_blank" href="{{asset('Expense_Attachment/'.$d->attachment)}}"><p><button type="button" class="btn btn-xs btn-custom-download"><i class="fa fa-download"></i> Download</button></p></a>

                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection