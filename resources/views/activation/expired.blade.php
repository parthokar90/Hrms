<title>HRMS | Trial Expired</title>
<style type="text/css" media="screen">
    *{
  padding: 0;
  margin: 0;
  box-sizing: border-box;
}

*::before,
*::after {
    content: '';
  position: absolute;
}

body{
    background: #1B0034;
    background-image: linear-gradient( 135deg, #1B0034 10%, #33265C 100%);
    background-attachment: fixed;
    background-size: cover;

}

.error {
    width: 100%;
    height: auto;
    margin: 50px auto;
    text-align: center;
    margin-bottom: 0;
}

.dracula{
    width: 230px;
    height: 300px;
    display: inline-block;
    margin: auto;
    overflowX: hidden;
}

.error .p {
/*  width: 10%; */
    /*height: 100%;*/
    color: #C0D7DD; 
/*  background: red; */
    font-size: 280px;
    margin: 50px;
    display: inline-block;
    font-family: 'Anton', sans-serif;
    font-family: 'Comfortaa', cursive;
    font-family: 'Combo', cursive;
/* font-family: 'Coming Soon', cursive; */
/* font-family: 'Jim Nightshade', cursive; */
}

/* page-ms */
.page-ms {transform: translateY(-50px);}

.error p.page-msg {
    text-align: center;
    color: #C0D7DD; 
    font-size: 30px;
    font-family: 'Combo', cursive;
    margin-bottom: 20px;
}
button.go-back {
     font-size: 20px;
    /*font-family: 'Combo', cursive;*/
    border: none;
    padding: 10px 20px;
    cursor: pointer;
    transition: 0.3s linear;
    z-index: 9;
    border-radius: 10px;
    background: #C0D7DD;
    color: #33265C;
    box-shadow: 0 0 10px 0 #C0D7DD;
    margin-top: 20px;
}
button:hover {box-shadow: 0 0 20px 0 #C0D7DD;}

audio {
/*  display: none; */
}
</style>
<div class="container">
    
    <div  class="error" style="padding-top: 100px;">
        <p class="p" style="font-size: 90px; color: white; text-shadow: 0 0 16px #000;"><b>FEITS HRMS</b></p>
        <br><br><br><br>
        <div class="page-ms">
            <p class="page-msg"> Oops, Trial version of the application has expired. </p>
            <br>
            <a href="http://www.feits.co" title=""><button class="go-back">&raquo; Contact with Developer</button></a>
            <br>
        </div>

</div>
    

    </div>