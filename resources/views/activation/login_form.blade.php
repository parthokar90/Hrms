@extends('layouts.guest')
@section('content')
    <div class="container" style="padding-top: 50px;">
        <div class="row animated tada">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
            	<h2 style="color:white;text-align: center;"><b>FEITS HRMS</b></h2>
            @if(Session::has('afailed'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('afailed')}}</p>
            @endif
              {!! Form::open(['method'=>'POST','action'=>['HrmsActivationController@store']]) !!}
            	<table class="table table-bordered">
            		<tr>
            			<td><input style="text-align: center;" type="text" name="username" placeholder="Username" class="form-control" required ></td>
            		</tr>
            		<tr>
            			<td><input style="text-align: center;" type="password" name="password" placeholder="Password" class="form-control" required ></td>
            		</tr>
            		<tr>
            			<td><input style="text-align: center;" type="password" name="accessKey" placeholder="Access Key" class="form-control" required ></td>
            		</tr>
            		<tr>
            			<td style="text-align: center;" >
            				<button type="submit" class="btn btn-success">&raquo; Procced </button>
            				<button type="reset" class="btn btn-warning"> Reset </button>
            			</td>
            		</tr>
            	</table>
               {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script>
	    setTimeout(function() {
	        $('#alert_message').fadeOut('fast');
	    }, 5000);
	 	     
	</script>
@endsection