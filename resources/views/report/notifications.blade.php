@extends('layouts.master')
@section('title', 'Notification')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>All notifications </strong></h2>
                    </div>
                    <div class="panel-body bg-white">
                        <div class=" pagination2 table-responsive">
                           <table class="table table-hover table-bordered table-dynamic">
                              <thead>
                                <tr>
                                  <th>SN</th>
                                  <th>Type</th>
                                  <th>Notification</th>
                                  <th>Created By</th>
                                  <th>Date</th>
                                </tr>
                              </thead>
                              <tbody>
                              @php $i=0; @endphp
                            @foreach($notifi as $n)
                               <tr>
                                   <td>{{++$i}}</td>
                                   <td>{{$n->type}}</td>
                                   <td>{{$n->message}}</td>
                                   <td>{{$n->userName}}</td>
                                   <td>{{\Carbon\Carbon::parse($n->created_at)->format('h:i (d-m-Y )')}}</td>
                               </tr>
                            @endforeach
                            </tbody>
                           </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

        $(function() {
            $("#date").datepicker({ dateFormat: "yy-mm-dd" });
        });
    </script>
    @include('include.copyright')

@endsection
