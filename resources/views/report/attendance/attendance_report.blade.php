@extends('layouts.master')
@section('title', 'Attendance Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Attendance </strong> Report</h3>
                    </div>
                    <div class="panel-content">
                        <div class="row" id="get-form">
                            {{Form::open(array('url' => 'attendance/report/show','method' => 'post'))}}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required form-label">Start Date</label>
                                    <div class="prepend-icon">
                                        <input type="text" name="start_date" id="start_date" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required form-label">End Date</label>
                                    <div class="prepend-icon">
                                        <input type="text" name="end_date" id="end_date" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Choose: </label>
                                    <div class="option-group">
                                        <select onchange="employeeCheck(this);" id="checkStatus" name="checkStatus" class="form-control">
                                            <option value="ae">All Employees</option>
                                            <option value="bd">By Department</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id='empShow' style="display: none;" class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Select Employee: </label>
                                    <div class="append-icon">
                                        <div class="option-group">
                                            {!! Form::select('emp_id[]',$employees,null,['class' => 'form-control','id'=>'empId', 'multiple' => '1'])!!}
                                        </div>
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div id='deptShow' style="display: none;" class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Select Department: </label>
                                    <div class="append-icon">
                                        <div class="option-group">
                                            {!! Form::select('dept_id',$departments,null,['class' => 'form-control'])!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 progress-demo">
                                <button type="button" data-style="zoom-in" id="attendance_report" class="btn btn-primary ladda-button">Generate Report</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="report-url_hidden_id" value="{{URL::to('/attendance/report/show')}}">
                    {{ Form::close() }}
                </div>
                <table class="table table-bordered panel table-striped">
                    <thead>
                    <tr>
                        <th>Employee</th>
                        <th>Employee Id</th>
                        <th>Designation</th>
                        <th>Date</th>
                        <th>In time</th>
                        <th>Out Time</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody id="attendance_display">
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <script>
        function employeeCheck(that) {
            if(that.value=='bd'){
                document.getElementById('deptShow').style.display='block';
                document.getElementById('empShow').style.display="none";
            }
            else{
                document.getElementById('deptShow').style.display='none';
                document.getElementById('empShow').style.display="none";

            }
        }

        $(document).ready(function(){


            $("#attendance_report").click(function(){
                var countDay='';
                var attendance_data = '';
                var url=$("#report-url_hidden_id").val();
                var form=$('#get-form').find('form');
                if($('#start_date').val()==''){
                    alert('Select starting date.');
                }
                if($('#end_date').val()==''){
                    alert('Select ending date.');
                }
                var late="";
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(),
                        dataType: "json",
                        success: function (data) {
                            countDay=data.length;
                            $.each(data, function (i, item) {
//                                countAbsent=item[2];
//                                console.log(item);
                                if(typeof (item.empFirstName)==='undefined' && typeof (item.empLastName)==='undefined' && typeof (item.employeeId)==='undefined'){
                                    if(item[2]==='weekend' || item[2]==='Festival Holiday' || item[2]==='On Leave'){
                                        attendance_data += '<tr>  <td>' + item[0].empFirstName + " " + item[0].empLastName +
                                            '</td>  <td>' + item[0].employeeId + '</td> <td>' + item[0].designation +
                                            '</td> <td>' + item["1"] + '</td> <td></td> <td> </td> <td class="">'+item['2']+'</td> </tr>';

                                    }
                                    else if(typeof (item[0].in_time)==='undefined' && typeof (item[0].out_time)==='undefined'){
                                        attendance_data += '<tr>  <td>' + item[0].empFirstName + " " + item[0].empLastName +
                                            '</td>  <td>' + item[0].employeeId + '</td> <td>' + item[0].designation +
                                            '</td> <td>' + item["1"] + '</td> <td></td> <td> </td> <td class="red-text">Absent</td> </tr>';

                                    }

                                    else {
                                        if (item[0].in_time > '09:15:00') {
                                            late = "Late";
                                        }
                                        else {
                                            late = "";
                                        }
                                        if (item[0].in_time === item[0].out_time) {
                                            item[0].out_time = '<p class="red-text">Not Given</p>';
                                        }
                                        attendance_data += '<tr>  <td>' + item[0].empFirstName + " " + item[0].empLastName +
                                            '</td>  <td>' + item[0].employeeId + '</td> <td>' + item[0].designation +
                                            '</td> <td>' + item[0].date + '</td> <td>' + item[0].in_time + '</td> <td>' +
                                            item[0].out_time + '</td> <td class="late-text">' + late + '</td> </tr>';

                                    }


                                }
                                else {
                                    if (item.in_time > '09:15:00') {
                                        late = "Late";
                                    }
                                    else {
                                        late = "";
                                    }
                                    if (item.in_time === item.out_time) {
                                        item.out_time = '<p class="red-text">Not Given</p>';
                                    }
                                    attendance_data += '<tr>  <td>' + item.empFirstName + " " + item.empLastName +
                                        '</td>  <td>' + item.employeeId + '</td> <td>' + item.designation +
                                        '</td> <td>' + item.date + '</td> <td>' + item.in_time + '</td> <td>'
                                        + item.out_time + '</td> <td class="late-text">' + late + '</td> </tr>';
                                }
                            });
                            $('#attendance_display').html(attendance_data);
                        },
                        error: function (data) {
//                            console.log('Error:', data);
                        }
                    });
                }
            });
        });
    </script>
    @include('include.copyright')
@endsection