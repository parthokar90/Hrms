@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Attendance Report')
@section('content')
    <div class="page-content page-content-hover-1">
        <div class="row panel myAnimation_1"  style="border:1px solid #dcdcdc;">

            <div class="col-xlg-12 col-lg-12  col-sm-12 animated bounceInRight">
                <div class="text-center" >
                    <h2><b>Attendance Report</b></h2>
                    <hr class="animated bounceInLeft">
                </div>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="{{route('report.public.daily_attendance')}}">
              <div class="panel">
                <div class="panel-content att_datr" >
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-th f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Attendance Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a  target="_BLANK" href="{{route('report.daily.late.present')}}">
              <div class="panel">
                <div class="panel-content att_dlp">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-history f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Late Present</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="{{route('report.daily_absent_report')}}">
              <div class="panel">
                <div class="panel-content att_dar">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-exclamation-triangle f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Absent Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a target="_BLANK" href="{{route('report.daily_present_report')}}">
              <div class="panel">
                <div class="panel-content att_dpr" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Present Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInLeft">
                <a target="_BLANK" href="{{route('report.daily_overtime_report')}}">
                    <div class="panel">
                        <div class="panel-content att_dor" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-line-chart f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Daily Overtime Report</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceIn">
                <a target="_BLANK" href="{{route('report.daily_attendance_exception')}}">
                    <div class="panel">
                        <div class="panel-content att_daex" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Daily Missing Attendance</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceIn">
              <a target="_BLANK" href="{{route('report.daily_attendance_summery')}}">
              <div class="panel">
                <div class="panel-content att_dasum" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-list f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Daily Attendance Summary</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
              <a target="_BLANK" href="{{route('report.date_wise_late')}}">
              <div class="panel">
                <div class="panel-content att_dwlr" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-history f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Date Wise Late Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a target="_BLANK" href="{{route('report.date_wise_absent')}}">
              <div class="panel">
                <div class="panel-content att_dwar" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-exclamation-circle f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Date Wise Absent Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="{{route('report.public.date_wise_present')}}">
              <div class="panel">
                <div class="panel-content att_dwpr" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Date Wise Present Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                <a target="_BLANK" href="{{route('report.date_wise_overtime')}}">
                    <div class="panel">
                        <div class="panel-content att_dwor" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-line-chart f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Date Wise Overtime Report</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>


            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="{{route('report.attendance_summery')}}">
              <div class="panel">
                <div class="panel-content att_dwas" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-calendar f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Date Wise Attendance Summary</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            @if(checkPermission(['admin']))

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                <a target="_BLANK" href="{{url('/report/attendance/card')}}">
                    <div class="panel">
                        <div class="panel-content att_jobcardp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Job Card Single</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}
{{-- 
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                <a target="_BLANK" href="{{url('/report/attendance/card/three/shift')}}">
                    <div class="panel">
                        <div class="panel-content att_jobcardp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Job Card Single(3-shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}



            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                <a target="_BLANK" href="{{url('/report/attendance/card/lefty')}}">
                    <div class="panel">
                        <div class="panel-content att_jobcardp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Job Card Single (Lefty/Resigned)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}


            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                <a target="_BLANK" href="{{url('/report/attendance/card/lefty/resign/three/shift')}}">
                    <div class="panel">
                        <div class="panel-content att_jobcardp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Job Card Single (Lefty/Resigned)(3-Shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}


            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                <a target="_BLANK" href="{{url('report/attendance/card_all/view')}}">
                    <div class="panel">
                        <div class="panel-content att_jobcardp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Job Card All</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                <a target="_BLANK" href="{{url('/report/attendance/card_all/view/three/shift')}}">
                    <div class="panel">
                        <div class="panel-content att_jobcardp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Job Card All(3-Shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}

            @endif


            @if(checkPermission(['hr']) || checkPermission(['hr-admin']))
                <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                    <a target="_BLANK" href="{{url('/report/attendance/card_all_b')}}">
                        <div class="panel">
                            <div class="panel-content att_jobcardp" style="">
                                <center>
                                    <div class="row" >
                                        <div class="">
                                            <center><i class="fa fa-file-text f-40"></i></center>
                                        </div>
                                        <br>
                                        <div class="">
                                            <center>
                                                <span class="f-14"><b>Job Card All</b></span>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </a>
                </div>
            @endif


            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                <a target="_BLANK" href="{{url('/report/attendance/employee_attendance_summary')}}">
                    <div class="panel">
                        <div class="panel-content att_darp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Employee Attendance Summary</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>


{{--            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInLeft">--}}
{{--              <a target="_BLANK" href="{{route('report.public.attendance_card')}}">--}}
{{--              <div class="panel">--}}
{{--                <div class="panel-content att_jobcard" style="">--}}
{{--                    <center>--}}
{{--                      <div class="row" >--}}
{{--                        <div class="">--}}
{{--                            <center><i class="fa fa-file-text f-40"></i></center>--}}
{{--                        </div>--}}
{{--                        <br>--}}
{{--                        <div class="">--}}
{{--                        <center>--}}
{{--                          <span class="f-14"><b>Job Card</b></span>--}}
{{--                          </center>--}}
{{--                        </div>--}}
{{--                      </div>--}}
{{--                    </center>--}}
{{--                </div>--}}
{{--              </div>--}}
{{--              </a>--}}
{{--            </div>--}}


            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{route('report.date_wise_attendance_exception')}}">
                    <div class="panel">
                        <div class="panel-content att_dwae" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Date Wise Missing Attendance</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
            @if(checkPermission(['admin']))
                <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                    <a target="_BLANK" href="{{route('report.date_wise_inout_time_report')}}">
                        <div class="panel">
                            <div class="panel-content att_dasum" style="">
                                <center>
                                    <div class="row" >
                                        <div class="">
                                            <center><i class="fa fa-file-text f-40"></i></center>
                                        </div>
                                        <br>
                                        <div class="">
                                            <center>
                                                <span class="f-14"><b>In & Out Time Report</b></span>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </a>
                </div>
            @endif

            @if(checkPermission(['admin']))
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                <a target="_BLANK" href="{{route('report.daily_attendance')}}">
                    <div class="panel">
                        <div class="panel-content att_darp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-th f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Daily Attendance Report (Private)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                <a target="_BLANK" href="{{route('report.date_wise_present')}}">
                    <div class="panel">
                        <div class="panel-content att_dwprp" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b> Date Wise Present Report (Private)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInLeft">
                <a target="_BLANK" href="{{route('report.admin_date_wise_overtime')}}">
                    <div class="panel">
                        <div class="panel-content att_dwop" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-list f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Date Wise Overtime (Private)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>


            @endif


        <div class="col-md-12 animated bounceInUp"><hr></div>
        <div class="col-md-6 animated bounceInUp">
       <div class="panel panel-default">
           <div class="panel-header ">
               <h5><i class="fa fa-area-chart"></i> <strong> Attendance Statistic (Last 15 Days)</strong></h5>
           </div>
           <div class="panel-body">
               <canvas id="attendancestatistic" style="max-width:100%;"></canvas>
           </div>
       </div>
     </div>

     <div class="col-md-6 animated bounceInDown">
       <div class="panel panel-default">
           <div class="panel-header">
               <h5><i class="icon-list"></i> <strong>Absent Statistic (Last 15 Days)</strong></h5>
           </div>
           <div class="panel-body">
               <canvas id="absentstatistic" style="max-width:100%;"></canvas>
           </div>
       </div>
     </div>

        </div>

    </div>
  

   @php

    function getLastNDays($days, $format = 'd/m'){
         date_default_timezone_set('Asia/Dhaka');

          $m = date("m"); 
          $de= date("d"); 
          $y= date("Y");
          $dateArray = array();
          for($i=0; $i<=$days-1; $i++){
              $dateArray[] = date($format, mktime(0,0,0,$m,($de-$i),$y)) ; 
          }
          return array_reverse($dateArray);
      }

    $days=getLastNDays(15,'Y-m-d');

    $dataPointsAttendenceStatistic = array();
    $dataSetsAttendenceStatistic = array();
    $dataSetsAbsentStatistic = array();

    foreach($days as $day){
      array_push($dataPointsAttendenceStatistic,$day);
      array_push($dataSetsAttendenceStatistic,dashboardcontroller::getAttendanceValueByDate($day));
      array_push($dataSetsAbsentStatistic,($employee-dashboardcontroller::getAttendanceValueByDate($day)));
    } 


      date_default_timezone_set('Asia/Dhaka');
      $datapointPresentAbsent=array();
      $present=dashboardcontroller::getAttendanceValueByDate(date("Y-m-d"));
      array_push($datapointPresentAbsent,$present);
      array_push($datapointPresentAbsent,($employee-$present));

   @endphp


     <script>
         window.onload = function() {

             var ctx = document.getElementById("attendancestatistic").getContext('2d');
             var myChart = new Chart(ctx, {
                 type: 'bar',
                 data: {
                    
                     labels: <?php echo json_encode($dataPointsAttendenceStatistic, JSON_NUMERIC_CHECK); ?>,
                     datasets: [{
                         label: '#',
                         data: <?php echo json_encode($dataSetsAttendenceStatistic, JSON_NUMERIC_CHECK); ?>,
                         backgroundColor: 
                             'rgba( 0, 200, 83, 0.8)',
                         borderColor: 
                             'rgba(1, 1, 1, 0.7)',
                         borderWidth: 1
                     }]
                 },
                 options: {
                     scales: {
                         yAxes: [{
                             ticks: {
                                 beginAtZero:true
                             }
                         }]
                     }
                 }
             });

             var ctx = document.getElementById("absentstatistic").getContext('2d');
             var myChart = new Chart(ctx, {
                 type: 'line',
                 data: {
                    
                     labels: <?php echo json_encode($dataPointsAttendenceStatistic, JSON_NUMERIC_CHECK); ?>,
                     datasets: [{
                         label: '#',
                         data: <?php echo json_encode($dataSetsAbsentStatistic, JSON_NUMERIC_CHECK); ?>,
                         backgroundColor: 
                             'rgba(229,57,47, 0.7)',
                         borderColor: 
                             'rgba(1 , 1, 1, 0.7)',
                         borderWidth: 1
                     }]
                 }, 
                 options: { 
                      scales: {
                         yAxes: [{
                             ticks: {
                                 beginAtZero:true
                             }
                         }]
                      }
                  },
             });
        }
     </script>
    @include('include.copyright')
@endsection