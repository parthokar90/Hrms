@extends('layouts.master')
@section('title', 'Daily Attendance Summary')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-8">
                                <h3><i class="fa fa-users "></i> Attendance Summery For <strong>{{ \Carbon\Carbon::parse($request->date)->format('d M Y (l)') }}</strong></h3>
                            </div>

                            {!! Form::open(['target'=>'_blank','method'=>'post']) !!}
                            <div class="col-md-4" style="margin-top:8px;">
                                <input type="hidden" name="reportType" value="{{$request->reportType}}">
                                <input type="hidden" name="date" value="{{$request->date}}">
                                <input type="hidden" name="pagesize" value="{{$request->pagesize}}">
                                <input type="hidden" name="pageOrientation" value="{{$request->pageOrientation}}">
                                <input type="hidden" name="extensions" value="{{$request->extensions}}">
                                <button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary btn-round btn-sm"  style="float:right;" ><i class="fa fa-file-pdf-o"></i> &nbsp Download PDF</button>
                                {{--                                <a href="#" data-toggle="modal" data-target="#search_employee" class="btn btn-success btn-round btn-sm"  style="float:right;" ><i class="fa fa-search"></i> Search Employee </a>--}}
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>
                    <div class="padLeft20 padTop10">
                        Total Employee={{$totalE}}<br>
                        Total Present={{$totalP}}<br>
                        Total Leave={{$totalL}}<br>
                        <a target="_blank" href="{{route('report.total_absent_list',"date=$request->date")}}">Total Absent={{$totalE-($totalL+$totalP)}}<br></a>
                        <a target="_blank" href="{{route('report.total_late_list',"date=$request->date")}}">Total Late={{ $Tlate }}</a><br>
                    </div>


                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-dynamic table-hover table-striped table-bordered">
                            <thead style="font-size:15px;font-weight:bold;">
                            <tr>
                                @if($request->reportType==1)
                                    <th>Department</th>
                                @elseif($request->reportType==2)
                                    <th>Section</th>
                                @elseif($request->reportType==3)
                                    <th>Floor</th>
                                    <th>Line</th>
                                @elseif($request->reportType==4)
                                    <th>Designation</th>
                                @elseif($request->reportType==5)
                                    <th>Line</th>
                                @endif
                                <th>Total</th>
                                <th>Present</th>
                                <th>Absent</th>
                                <th>Late</th>
                                <th>Leave</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $d)
                                <tr>
                                    <?php
                                    $id=$d['designationId'];

                                    ?>
                                    @if($request->reportType==3)
                                        <td>{{\Illuminate\Support\Facades\DB::table('floors')->where('id','=',$d['designationId'])->select('floor')->first()->floor}}</td>
                                    @endif
                                    <td>{{$d['designationName']}}</td>
                                    <td>{{$d['total']}}</td>
                                    <td>
                                        @if($d['present'] !=0)
                                            <a target="_blank" href="{{route('report.total_present_list',"type=$request->reportType&date=$request->date&id=$id")}}"> {{ $d['present']  }}</a>
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td>
                                        @if($d['total']-($d['present']+$d['leave'])!=0)
                                            <a target="_blank" href="{{route('report.total_absent_list',"type=$request->reportType&date=$request->date&id=$id")}}">{{ $d['total']-($d['present']+$d['leave']) }}</a>
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td>
                                        @if($d['late']!=0)
                                            <a target="_blank" href="{{route('report.total_late_list',"type=$request->reportType&date=$request->date&id=$id")}}">{{ $d['late'] }}</a>
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td>{{ $d['leave'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection
