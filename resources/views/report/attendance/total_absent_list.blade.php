@extends('layouts.master')
@section('title', 'Absent List')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i> Absent Report For <strong>{{ \Carbon\Carbon::parse($request->date)->format('d M Y (l)') }}</strong></h3>

                            </div>

                        </div>

                    </div>

                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-dynamic table-hover table-striped table-bordered">
                            <thead style="font-size:15px;font-weight:bold;">
                            <tr>
                                <th>SN</th>
                                <th>id</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Department</th>

                                @if(!empty($request->coljoiningdate))
                                    <th>Joining Date</th>
                                @endif
                                @if(!empty($request->colgender))
                                    <th>Gender</th>
                                @endif
                                @if(!empty($request->colunit))
                                    <th>Unit</th>
                                @endif
                                @if(!empty($request->colfloor))
                                    <th>Floor</th>
                                @endif
                                @if(!empty($request->colline))
                                    <th>Line</th>
                                @endif
                                <th>Section</th>

                                @if(!empty($request->colstatus))
                                    <th>Status</th>
                                @endif
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=0; ?>
                            @foreach( $absent as $employee)
                                <tr>
                                    <td><?php echo ++$i; ?></td>
                                    <td><a target="_blank" href="{{route('employee_profile.show',base64_encode($employee->id))}}">{{$employee->employeeId}}</a></td>
                                    <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                                    <td>{{$employee->designation}}</td>

                                    <td>{{$employee->departmentName}}</td>

                                    @if(!empty($request->coljoiningdate))
                                        <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                                    @endif
                                    @if(!empty($request->colgender))
                                        <td>
                                            @if($employee->empGenderId==1)
                                                Male
                                            @endif
                                            @if($employee->empGenderId==2)
                                                Female
                                            @endif
                                            @if($employee->empGenderId==3)
                                                Other
                                            @endif
                                        </td>
                                    @endif
                                    @if(!empty($request->colunit))
                                        <td>{{$employee->unitName}}</td>
                                    @endif
                                    @if(!empty($request->colfloor))
                                        <td>{{$employee->floorName}}</td>
                                    @endif
                                    @if(!empty($request->colline))
                                        <td>{{$employee->LineName}}</td>
                                    @endif
                                    <td>{{$employee->empSection}}</td>
                                    @if(!empty($request->colstatus))
                                        <td>
                                            @if($employee->empAccStatus==1)
                                                Active
                                            @elseif($employee->empAccStatus==0)
                                                Inactive
                                            @endif
                                        </td>
                                    @endif
                                    <td><span class="red-text">Absent</span></td>

                                    <td>
                                        <a target="_blank" href="{{route('report.edit_absent',[$employee->id, \Carbon\Carbon::parse($request->date)->toDateString()])}}" class="btn btn-danger btn-sm" title="edit"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection