@extends('layouts.master')
@section('title', 'Employee List')
@section('content')
    {{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 ><i class="fa fa-users "></i> <strong>Employee Attendance Summary @if(Carbon\Carbon::parse($request->start_date)->format('F')==Carbon\Carbon::parse($request->end_date)->format('F')) of {{ Carbon\Carbon::parse($request->start_date)->format('F')}} @endif</strong> </h3>

                            </div>
                            {!! Form::open(['method'=>'post']) !!}
                            <div class="col-md-4" style="margin-top:8px;">
                                <input type="hidden" name="start_date" value="{{\Carbon\Carbon::parse($request->start_date)->format('m/d/Y')}}">
                                <input type="hidden" name="end_date" value="{{\Carbon\Carbon::parse($request->end_date)->format('m/d/Y')}}">
                                <input type="hidden" name="unitId" value="{{$request->unitId}}">
                                <input type="hidden" name="floorId" value="{{$request->floorId}}">
                                <input type="hidden" name="sectionName" value="{{$request->sectionName}}">
                                <input type="hidden" name="departmentId" value="{{$request->departmentId}}">
                                <input type="hidden" name="designationId" value="{{$request->designationId}}">
                                <input type="hidden" name="accStatus" value="{{$request->accStatus}}">


                                <button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary btn-round btn-sm"  style="float:right;" ><i class="fa fa-file-pdf-o"></i> &nbsp Download PDF</button>
{{--                                <a href="#" data-toggle="modal" data-target="#search_employee" class="btn btn-success btn-round btn-sm"  style="float:right;" ><i class="fa fa-search"></i> Search Employee </a>--}}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-striped table-dynamic table-bordered">
                            <thead>
                                <tr>
                                    <th>Employee Id</th>
                                    <th>Name</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>Present</th>
{{--                                    <th>#</th>--}}
                                    <th>Absent</th>
                                    <th>Leave</th>
                                    <th>Late</th>

                                </tr>
                            </thead>
                            <tbody>
                            @foreach($employees as $employee)
                                <?php
                                        $percent=0;
                                if($request->accStatus==1){
                                        if($employee->empJoiningDate>\Carbon\Carbon::parse($request->start_date)){
                                            $days = \Carbon\Carbon::parse($employee->empJoiningDate)->diffInDays(\Carbon\Carbon::parse($request->end_date))+1;
                                            $w=App\Http\Controllers\ReportController::weekendCalculator($employee->empJoiningDate,$request->end_date)-App\Http\Controllers\ReportController::regular_day_count($employee->empJoiningDate,$request->end_date);
                                            $h=App\Http\Controllers\ReportController::holiday($employee->empJoiningDate,$request->end_date);
                                        }
                                        else{
                                            $days=$total_days;
                                            $w=$weekend;
                                            $h=$holidays;

                                        }
                                    }
                                else{
                                    if($employee->date_of_discontinuation<\Carbon\Carbon::parse($request->end_date)){
                                        $days = \Carbon\Carbon::parse($request->start_date)->diffInDays(\Carbon\Carbon::parse($employee->date_of_discontinuation))+1;
                                        $w=App\Http\Controllers\ReportController::weekendCalculator($request->start_date,$employee->date_of_discontinuation)-App\Http\Controllers\ReportController::regular_day_count($request->start_date,$employee->date_of_discontinuation);
                                        $h=App\Http\Controllers\ReportController::holiday($request->start_date,$employee->date_of_discontinuation);
                                    }
                                    else{
                                        $days=$total_days;
                                        $w=$weekend;
                                        $h=$holidays;

                                    }

                                }
                                ?>
                                <tr>
                                    <td>{{$employee->employeeId}}</td>
                                    <td>{{$employee->empFirstName ." ".$employee->empLastName}}</td>
                                    <td>{{$employee->departmentName}}</td>
                                    <td>{{$employee->designation}}</td>
                                    <td>
                                        <?php
                                            $percent=number_format((($employee->total_present)/$days)*100,2)
                                        ?>

                                        {{$employee->total_present." ($percent%) "}}</td>
                                    <?php
                                        $x=$employee->total_present-($employee->weekend_present-$employee->regular_present);
                                        $y=$w;
                                        $absent=max(($days-($x+$y+$h+$employee->total_leaves_taken)),0);
                                        $percent=number_format((($absent)/$days)*100,2);
                                    ?>
{{--                                    <td>{{max($total_days-(($employee->total_present-($employee->weekend_present-$employee->regular_present))+($weekend-$regular_days_setup_check)+$holidays+$employee->total_leaves_taken),0)}}</td>--}}
                                    <td>{{$absent." ($percent%) "}}</td>

                                    <td>{{$employee->total_leaves_taken}}</td>
                                    <td>
                                        <?php
                                            if($employee->total_present!=0){
                                                $percent=number_format((($employee->late)/$employee->total_present)*100,2);
                                                }
                                            else{
                                                $percent=0;
                                            }
                                        ?>
                                        {{$employee->late." ($percent%)"}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
{{--                        {{$employees->links()}}--}}
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script type="text/javascript">

        var url = "{{ route('employee.section.autocomplete.ajax') }}";
        $('#empSection').typeahead({
            source:  function (query, process) {
                return $.get(url, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
    </script>
    @include('include.copyright')
@endsection