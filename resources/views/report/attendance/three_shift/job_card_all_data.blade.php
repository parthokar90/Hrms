<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Attendance Card</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 90%;
            font-size: 11px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            /*border: 1px solid #ddd;*/
            font-size: 12px;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: center !important;

        }

        #customers th {
            text-align: left;
            padding: 5px;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }

        #cardFooter{
            border-collapse: collapse;
            font-size:11px;
            width:70%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */

        }
        .reportDateRange{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif  !important;
            font-size: 12px !important;
            font-weight: bold;
        }
        #cardFooter td {
            text-align: left !important;
        }
    </style>
</head>
<body>
    @php $total_att=0; $regular_present=0;$regular_leave=0; @endphp
@foreach($employee as $employees)
@php 
$total_att=0;
$total_leave=0;
$total_weekend=0;
$tot_holidays=0;
$tot_work_day=0;
$total_ot_hour=0;
$total_ot_minute=0;
$total_late_hour=0; 
$total_late_minute=0;
$em=DB::Table('employees')
     ->where('employees.id',$employees->id)
     ->where('is_three_shift',1)
     ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
     ->leftjoin('designations','employees.empDesignationId','=','designations.id')
     ->select('employeeId','empFirstName','empLastName','empJoiningDate','empSection','departmentName','designation','empOTStatus')
     ->first();
@endphp
<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
    <p class="reportDateRange">Job Card Report Month From <b>{{date('d-M-Y',strtotime($start))}}</b> to <b>{{date('d-M-Y',strtotime($end))}}(3-Shift) </b></p>
    </div>
 
    <table id="employeeDetails" >
        <tr>
        <td style="text-align:left" width="35%"><b>Employee ID :{{$em->employeeId}}</b> </td>
            <td style="text-align:left" width="35%"><b>Name : {{$em->empFirstName}} {{$em->empLastName}}</b> </td>
            <td style="text-align:left" width="30%"><b>Joining Date : {{date('d-M-Y',strtotime($em->empJoiningDate))}}</b> </td>
        </tr>
        <tr>
            <td style="text-align:left" width="35%"><b>Designation : {{$em->designation}}</b> </td>
            <td style="text-align:left" width="35%"><b>Department : {{$em->departmentName}}</b> </td>
            <td style="text-align:left" width="30%"><b> Section : {{$em->empSection}}</b></td>
        </tr>
    </table>

        <table id='customers' style="margin-top:15px;font-size:11px;" border="1px">
            <thead>
            <tr>
                <th>Date</th>
                <th>Day</th>
                <th>In Time </th>
                <th>Out Time</th>
                <th>OT</th>
                <th>Late Time</th>
                <th>Status</th>
                <th>Remarks</th>
            </tr>
            </thead>
            <tbody>
           @foreach($shift_emp[$employees->id] as $shift_emps)
           @php $l_holiday=0; @endphp
                <tr>
                    <td>{{date('d-M-Y',strtotime($shift_emps->shift_date))}}</td>
                    <td>{{date('l',strtotime($shift_emps->shift_date))}}</td>
                    <td>  
                       @php
                        $present=DB::table('attendance')
                        ->where('emp_id',$shift_emps->emp_id)
                        ->where('date',$shift_emps->shift_date)
                        ->get(); 
                       @endphp
                       @foreach($present as $p)
                       {{date('h:i:a',strtotime($p->in_time))}}
                       @endforeach
                    </td>
                    <td>
                        @foreach($present as $p)
                          {{date('h:i:a',strtotime($p->out_time))}}
                        @endforeach
                    </td>
                    <td>
                        @php 
                        $overtime=DB::table('three_shift_employee')
                        ->leftjoin('attendance','three_shift_employee.emp_id','=','attendance.emp_id')
                        ->leftjoin('attendance_setup','three_shift_employee.emp_shift','=','attendance_setup.id')
                        ->where('three_shift_employee.emp_id',$shift_emps->emp_id)
                        ->where('date',$shift_emps->shift_date)
                        ->where('shift_date',$shift_emps->shift_date)
                        ->select('out_time','attendance_setup.exit_time')
                        ->get();
                        @endphp
                       
                        @foreach($overtime as $ot)
                        @php 
                        $out_hour=date('h',strtotime($ot->out_time));
                        $shift_out_hour=date('h',strtotime($ot->exit_time));
                        $out_minute=date('i',strtotime($ot->out_time));
                        $shift_out_minute=date('i',strtotime($ot->exit_time));
                        @endphp
                        @if($out_hour>=$shift_out_hour)
                        @php 
                        $ot_hour=abs($out_hour-$shift_out_hour);
                        $ot_minute=abs($out_minute-$shift_out_minute);
                        @endphp

                        @if($ot_minute<25)
                         @php $ot_minute=0; @endphp
                         @elseif ($ot_minute>=25 && $ot_minute<55)
                            @php $ot_minute=.5; @endphp
                        @endif
                        @else 
                        @php $ot_hour=0; $ot_minute=0; @endphp
                        @endif
                        {{-- @if($employee->empOTStatus==1) --}}
                        {{$ot_hour}}:{{$ot_minute}}
                        {{-- @else 
                        00:00
                        @endif --}}
                        @php
                        $total_ot_hour+=$ot_hour;
                        $total_ot_minute+=$ot_minute; @endphp
                        @endforeach
						
                    </td>
                    <td>
                        @php
                        $late=DB::table('three_shift_employee')
                        ->leftjoin('attendance','three_shift_employee.emp_id','=','attendance.emp_id')
                        ->leftjoin('attendance_setup','three_shift_employee.emp_shift','=','attendance_setup.id')
                        ->where('three_shift_employee.emp_id',$shift_emps->emp_id)
                        ->where('date',$shift_emps->shift_date)
                        ->where('shift_date',$shift_emps->shift_date)
                        ->select('in_time','attendance_setup.max_entry_time')
                        ->get();
                        $tr=0;
                        @endphp  
                            @foreach($late as $l)
                            @php 
                            $in_hour=date('h',strtotime($l->in_time));
                            $shift_out_hour=date('h',strtotime($l->max_entry_time));
                            $in_minute=date('i',strtotime($l->in_time));
                            $shift_out_minute=date('i',strtotime($l->max_entry_time));
                            @endphp
                            @if($in_hour>=$shift_out_hour)
                            @php 
                               $late_hour=abs($in_hour-$shift_out_hour);
                               $late_minute=abs($in_minute-$shift_out_minute);
                               $total_late_hour+=$late_hour;
                               $total_late_minute+=$late_minute;
                            @endphp
                            @else 
                            @php $late_hour=0; $late_minute=0; @endphp
                            @endif
                            {{$late_hour}}:{{$late_minute}}
                            @endforeach
                      </td>
                    <td>
                        @php
                        $tot_working_day=DB::table('three_shift_employee')
                        ->where('emp_id',$shift_emps->emp_id)
                        ->where('shift_date',$shift_emps->shift_date)
                        ->count();
                        $tot_work_day+=$tot_working_day;
                        $att=DB::table('attendance')
                        ->where('emp_id',$shift_emps->emp_id)
                        ->where('date',$shift_emps->shift_date)
                        ->count();
                        $total_att+=$att;
                        $leave=DB::table('emp_shift_leave_attendance')
                        ->where('emp_id',$shift_emps->emp_id)
                        ->where('leave_date',$shift_emps->shift_date)
                        ->count();
                        $total_leave+=$leave; 
                        $weekend=DB::table('emp_shift_status')
                        ->where('emp_id',$shift_emps->emp_id)
                        ->where('shift_date',$shift_emps->shift_date)
                        ->where('present','=',NULL)
                        ->where('leave_date','=',NULL)
                        ->where('holiday','=',NULL)
                        ->where('regular','=',NULL)
                        ->count(); 
                        $total_weekend+=$weekend;

                        $emp_leavee=DB::table('emp_shift_leave_attendance')
                        ->where('emp_id',$shift_emps->emp_id)
                        ->where('leave_date',$shift_emps->shift_date)
                        ->select('leave_date')
                        ->get();
                        foreach($emp_leavee as $leaves){
                        $l_holiday+=DB::table('emp_shift_holiday')
                        ->where('holiday_date',$leaves->leave_date)
                        ->count();
                        }  
                        $total_holiday=DB::table('emp_shift_holiday')
                        ->where('holiday_date',$shift_emps->shift_date)
                        ->count()-$l_holiday;
                        $tot_holidays+=$total_holiday;
                        $regular=DB::table('tbweekend_regular_day')
                        ->where('weekend_date',$shift_emps->shift_date)
                        ->count();


                        $total_regular_date=DB::table('tbweekend_regular_day')
                        ->where('weekend_date',$shift_emps->shift_date)
                        ->select('weekend_date')
                        ->get();
                        foreach($total_regular_date as $rd){
                            $regular_present+=DB::table('attendance')
                            ->where('emp_id',$shift_emps->emp_id)
                            ->where('date',$rd->weekend_date)
                            ->count();
                            $regular_leave+=DB::table('emp_shift_leave_attendance')
                            ->where('emp_id',$shift_emps->emp_id)
                            ->where('leave_date',$rd->weekend_date)
                            ->count();
                        }
                        $regular=DB::table('tbweekend_regular_day')
                        ->where('weekend_date',$shift_emps->shift_date)
                        ->count()-$regular_present-$regular_leave;
                        @endphp
                         @if($att>0)
                         <span style="color:green">Present</span>
                         @elseif($leave>0)
                         <span>On Leave</span>
                         @elseif($weekend>0)
                         <span>Weekend</span>
                         @elseif($total_holiday>0)
                         <span>Holiday</span>
                         @elseif($regular>0)
                         <span>Regular Day</span>
                         @else
                         <span style="color:red">Absent</span>
                         @endif
                    </td>
                    <td></td>
                </tr>
           @endforeach
            <tr style="font-weight: bold">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            <td>{{$total_ot_hour}}:{{$total_ot_minute}}</td>
            <td>{{$total_late_hour}}:{{$total_late_minute}}</td>


                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
</div>
<div>
    <center>
        <table id="cardFooter">
            <tr>

                <td width="50%"><b>Total Days (In Month): </b>{{date('t',strtotime($start))}} Days</td>
            </tr>

            <tr>
            
                <td width="50%"><b>Holiday: </b> {{$tot_holidays}} Days</td>
            </tr>
            <tr>
                {{--$working_days--}}
                <td width="50%"><b>Weekends: </b> {{$total_weekend}} Days</td>
            </tr>
            <tr>
            <td  width="50%"><b>Total Present Days: </b> {{$total_att}} Days </td>
            </tr>
            <tr>
                <td class="padTop10"><b>Total Leave :</b> {{$total_leave}} Days</td></tr>
            <tr>
                <td class="padTop10"><b>Total Absent :</b>@php $abs=$tot_work_day-$total_att-$total_leave-$total_weekend-$tot_holidays; @endphp {{$abs}} Days</td>
            </tr>

        </table>
    </center>
</div>
@endforeach
</body>
</html>




