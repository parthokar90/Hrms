@extends('layouts.master')
@section('title', 'Job Card All')
@section('extra_css')

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />

@endsection


@section('extra_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

@endsection

@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Job Card All</b></h2>
                    <hr>
                </div>
            </div>
            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
            @endif


            {!! Form::open(['method'=>'POST','action'=>'ReportController@job_card_all_data','target'=>'_blank']) !!}
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Start Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="start_date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select Start date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">End Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="end_date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select End date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Select Single Employee<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select data-search="true" data-length="2" name="emp_id" class="form-control" onchange="checkEmployee(this)">
                            <option value="0">All</option>
                            @foreach($employees as $emp)
                                <option value="{{$emp->id}}">{{$emp->empFirstName." (".$emp->employeeId.")"}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div id="hiddenInput">

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Unit<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select name="unitId" class="form-control"  data-search="true">
                                <option value="0">All</option>
                                @foreach($units as $unit)
                                    <option value="{{$unit->id}}">{{$unit->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Floor<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select name="floorId" class="form-control floor"  data-search="true">
                                <option value="0">All</option>
                                @foreach($floors as $floor)
                                    <option value="{{$floor->id}}">{{$floor->floor}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Section<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select name="sectionName" class="form-control"  data-search="true">
                                <option value="0">All</option>
                                @foreach($sections as $section)
                                    <option value="{{$section->empSection}}">{{$section->empSection}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Department<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select name="departmentId" class="form-control"  data-search="true">
                                <option value="0">All</option>
                                @foreach($departments as $department)
                                    <option value="{{$department->id}}">{{$department->departmentName}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Employee Status<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select class="form-control" name="accStatus">
                                <option selected value="1">Active</option>
                                <option value="0">InActive</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group padding-bottom-30-percent">
                    <div class="col-md-9 col-md-offset-3">
                        <hr>
                        <input type="submit" value="Preview" name="viewType" class="btn btn-success margin-top-10">

                        <button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary margin-top-10"><i class="fa fa-file-pdf-o"></i> &nbsp; Generate PDF </button>


                        {{--<input type="reset" value="Clear " class="btn btn-warning margin-top-10">--}}
                    </div>
                </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <script>

        function checkEmployee(that) {
            // alert($(that).data('type'));
            if(that.value==='0'){
                document.getElementById('hiddenInput').style.display="block";
//                alert('ok');
            }
            else{
                document.getElementById('hiddenInput').style.display="none";

            }
//            alert(that.value);

        }

        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>



    @include('include.copyright')
@endsection

