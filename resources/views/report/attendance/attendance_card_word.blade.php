<?php

function roundTime($time){
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));

    if($minute<25){
        $minute=0;
    }
    elseif ($minute>=25 &&$minute<55){
        $minute=.5;
    }
    else{
        $minute=0;
        $hour++;
    }
    $kind=number_format((float)($hour+$minute),1,'.','');
    return $kind;

}

function late_time($pretime,$time){
    $phour=date('G',strtotime($pretime));
    $pminute=date('i',strtotime($pretime));
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));
    $thour=$phour+$hour;
    $tminute=$pminute+$minute;
    $h=floor($tminute/60);
    $tminute=sprintf("%02d",$tminute%60);
    $thour+=$h;
    $thour=sprintf("%02d",$thour);
    return "$thour:$tminute";

}

function end_time($time){
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));
    $minute=sprintf("%02d",$minute+rand(0,20));
    return "$hour:$minute";

}

$ot=0;
$late_time="00:00";

$output = ""; 
$absent=0;
$working_days=0;
$present=0;
$late=0;
$onleave=0;
$startTime=\App\Http\Controllers\OvertimeController::overtime_start();
$endTime=\App\Http\Controllers\OvertimeController::overtime_end();
$attendance_settings=\Illuminate\Support\Facades\DB::table('attendance_setup')->leftJoin('employees','attendance_setup.id','=','employees.empShiftId')->where('employees.id','=',$emp_id)->first();

if(isset($datas[0][0])){
$output .="
<div style='font-size:11px;'>
    <div style='text-align:center;'>
        <h2 style='padding:0px;margin:0px;'>".$companyInformation->company_name."</h2>
        <p style='padding:0px;margin:0px;'>".$companyInformation->company_address1."</p>
        <p style='padding:5px 0px;margin:0px;'>Job Card Report Month From <b>".date('d-m-Y', strtotime($start_date))."</b> to <b>".date('d-m-Y', strtotime($end_date))."</b></p>
    </div>
    <br /><br />
<table border='1' width='100%' >
        <tr>
            <td style='text-align:left' width='35%'><b>Employee ID :</b> ".$datas[0][0]->employeeId."</td>
            <td style='text-align:left' width='35%'><b>Name : </b> ".$datas[0][0]->empFirstName." ".$datas[0][0]->empLastName ."</td>
            <td style='text-align:left' width='30%'><b>Joining Date :</b> ".$datas[0][0]->empJoiningDate."</td>
        </tr>
        <tr>
            <td style='text-align:left' width='35%'><b>Designation :</b> ".$datas[0][0]->designation."</td>
            <td style='text-align:left' width='35%'><b>Department : </b> ".$datas[0][0]->departmentName."</td>
            <td style='text-align:left' width='30%'><b> Section : </b> ".$datas[0][0]->empSection."</td>
        </tr>
       
    </table>
    <br />
    <table border='1' width='100%' style='text-align:center;'>
        <thead>
        <tr>
            <th>Date</th>
            <th>Day</th>
            <th>In Time </th>
            <th>Status</th>
            <th>Late Time</th>
            <th>Out Time</th>
            <th>OT</th>
        </tr>
        </thead>
        <tbody>";
         foreach($datas as $d){
            $working_days++;
             $leave_check= explode('@',$d[2]);
            
            $output .="
            <tr>
                <td>".date('d-m-Y', strtotime($d[1]))."</td>
                <td>".date('l', strtotime($d[1]))."</td>
                <td>";
                   if(isset($d[0]->in_time)){
                        $present++;
                        $output .=
                        date('G:i', strtotime($d[0]->in_time));
                    }
            $output .="
                </td>
                <td>";
                if(isset($d[0]->in_time)){
                    if($d[0]->in_time>$attendance_settings->max_entry_time){
                        $late++; 
                        $output .="<span>Late</span>";
                    }else{
                        $output .="<span>Present</span>";
                    }
                }else{
                    if($d[2]=='weekend' || $d[2]=='Festival Holiday' || $leave_check[0]=='On Leave'){
                            if($leave_check[0]!='On Leave'){
                               $working_days--;
                            }else{
                                $onleave++;
                            }


                            if($leave_check[0]=='On Leave'){
                                if(isset($leave_check[1])){
                                    $output .="<span>".$leave_check[1]."</span>";

                                }
                            }
                            else{
                                $output .="<span>".$d[2]."</span>";
                            }


                        }else{
                            $output .="<span style='color:#FF0000'>Absent</span>";
                        }
                  }
                 $output .="</td>
                <td>";
                if(isset($d[0]->in_time)){
                    if($d[0]->in_time>$attendance_settings->max_entry_time){
                        $late_time=late_time($late_time,date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time)));
                        $output .=date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time));
                    }
                    else{
                        $output .="00:00";
                    }
                }
                $output .="</td>
                <td>";
                if(isset($d[0]->out_time)){
                    if($d[0]->in_time==$d[0]->out_time){
                        $output .="<span style='color: #FF0000;'>Not Given</span>";
                    }elseif($d[0]->out_time>$endTime){
                        $output .=end_time($endTime);
                    }else{
                        $output .=date('G:i', strtotime($d[0]->out_time));

                    }
                }

               $output .="</td>
                <td>";
                    if(isset($d[0]->out_time) && $d[0]->in_time!=$d[0]->out_time){
                        if($d[0]->out_time>$startTime && $d[0]->out_time<$endTime){

                            $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)));

                            $output .= roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)));
                        }elseif($d[0]->out_time>$endTime){
                            $ot+=roundTime(date('G:i', strtotime($endTime) - strtotime($attendance_settings->exit_time)));
                            $output .= roundTime(date('G:i', strtotime($endTime) - strtotime($attendance_settings->exit_time)));
                        }
                        else{
                            $output .="0.0";
                        }
                    }

                $output .= "</td>
            </tr>";

        }

         $output .= "
         <tr style='font-weight: bold'>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>".$late_time."</td>
                <td></td>
                <td>".$ot." hour</td>
            </tr>
        </tbody>
    </table>
    <div>
    <br /><br />
    <center>
        <table width='70%'>
            <tr>
                <td width='40%'><b>Total Working Days: </b> ".$working_days." Days</td>
                <td  width='50%'><b>Total Present Days: </b> ". $present ." Days</td>
            </tr>
            <tr>
                <td><b>Total Leave :</b> ". ($onleave) ." Days</td>
                <td><b>Total Absent :</b> ". ($working_days-($present+$onleave)) ." Days</td>
            </tr>
        </table>
    </center>
</div>
</div>
   ";
   }else{
    $output .="No data found.";
}

header("Content-Type:application/doc");
header("Content-Disposition:attachment;filename=employee_job_card.doc");
echo $output;
                              

?>