<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Absent Report</title>
</head>
<body>

<div>
    <table>
        <thead>
        </thead>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center">Absent report for <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y (l)')}} </b></td></tr>

        </tbody>
    </table>


    @foreach($departments as $line)
        <?php
        $query1=$query;
        $query1.=" AND departments.id='".$line->id."' GROUP BY employees.id ORDER BY attendance.date DESC";
        $absent=DB::select($query1);
        $countAbsent=count($absent);

        ?>
        @if($countAbsent!=0)
            <h4><b>Department: {{$line->departmentName}}</b></h4>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Emp. ID</th>
                    <th>Name</th>
                    @if(!empty($request->coldesignation))
                        <th>Designation</th>
                    @endif
                    @if(!empty($request->coldepartment))
                        <th>Department</th>
                    @endif
                    @if(!empty($request->coljoiningdate))
                        <th>Joining Date</th>
                    @endif
                    @if(!empty($request->colgender))
                        <th>Gender</th>
                    @endif
                    @if(!empty($request->colunit))
                        <th>Unit</th>
                    @endif
                    @if(!empty($request->colfloor))
                        <th>Floor</th>
                    @endif
                    @if(!empty($request->colline))
                        <th>Line</th>
                    @endif

                    @if(!empty($request->colPhone))
                        <th>Contact No.</th>
                    @endif

                    @if(!empty($request->colsection))
                        <th>Section</th>
                    @endif
                    @if(!empty($request->colstatus))
                        <th>Status</th>
                    @endif
                    @if(!empty($request->colcontinueabsent))
                        <th>Last Attend Date</th>
                        <th>Continue Absent</th>
                    @endif
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $absent as $key=> $employee)
                    <tr>
                        <td>
                            <b><a href="{{route('employee_profile.show',base64_encode($employee->id))}}" target="_blank">{{$employee->employeeId}}</a></b>
                        </td>
                        <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                        @if(!empty($request->coldesignation))
                            <td>{{$employee->designation}}</td>
                        @endif
                        @if(!empty($request->coldepartment))
                            <td>{{$employee->departmentName}}</td>
                        @endif
                        @if(!empty($request->coljoiningdate))
                            <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                        @endif
                        @if(!empty($request->colgender))
                            <td>
                                @if($employee->empGenderId==1)
                                    Male
                                @endif
                                @if($employee->empGenderId==2)
                                    Female
                                @endif
                                @if($employee->empGenderId==3)
                                    Other
                                @endif
                            </td>
                        @endif
                        @if(!empty($request->colunit))
                            <td>{{$employee->unitName}}</td>
                        @endif
                        @if(!empty($request->colfloor))
                            <td>{{$employee->floorName}}</td>
                        @endif
                        @if(!empty($request->colline))
                            <td>{{$employee->LineName}}</td>
                        @endif
                        @if(!empty($request->colPhone))
                            <td>{{$employee->empPhone}}</td>
                        @endif
                        @if(!empty($request->colsection))
                            <td>{{$employee->empSection}}</td>
                        @endif
                        @if(!empty($request->colstatus))
                            <td>
                                @if($employee->empAccStatus==1)
                                    Active
                                @elseif($employee->empAccStatus==0)
                                    Inactive
                                @endif
                            </td>
                        @endif
                        @if(!empty($request->colcontinueabsent))
                            <?php
                            $start_date=\Carbon\Carbon::parse($employee->date);
                            if(\Carbon\Carbon::parse($request->date)>$start_date){
                                $diff=\Carbon\Carbon::parse($request->date)->diffInDays($start_date);
                            }
                            else{
                                $diff=0;
                            }
                            ?>
                            <td>{{\Carbon\Carbon::parse($employee->date)->format('d M Y')}}</td>
                            <td> {{ $diff }}</td>
                        @endif
                        <td><span class="red-text">Absent</span></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif

    @endforeach


    <hr>

</div>

</body>
</html>




