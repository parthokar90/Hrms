<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Resign-Lefty Employee List</title>
</head>
<body>

<div class="container">
    <table>
        <thead>
        </thead>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"> <b>Resigned/Lefty Employee List</b></td></tr>

        </tbody>
    </table>
    <table>
        <thead>
        <tr>
            <th>Serial No.</th>
            <th>ID</th>
            <th>Name</th>
            @if(!empty($request->coldesignation))
                <th>Designation</th>
            @endif
            @if(!empty($request->coldepartment))
                <th>Department</th>
            @endif
            @if(!empty($request->coljoiningdate))
                <th>Joining Date</th>
            @endif
            @if(!empty($request->colgender))
                <th>Gender</th>
            @endif
            @if(!empty($request->colunit))
                <th>Branch</th>
            @endif
            @if(!empty($request->colsection))
                <th>Section</th>
            @endif

            @if(!empty($request->colskill_level))
                <th>Skill</th>
            @endif

            @if(!empty($request->colstatus))
                <th>Discontinuation Type</th>
            @endif
            @if(!empty($request->coldob))
                <th>DOB</th>
            @endif
            @if(!empty($request->colsalary))
                <th>Salary</th>
            @endif
            <th>Separation Date</th>
            <th>Remarks</th>
        </tr>
        </thead>
        <tbody>
        @foreach($employees as $key=>$employee)
            <tr>
                <td align="center">{{++$key}}</td>
                <td style="text-align: center;">{{$employee->employeeId}}</td>
                <td>{{$employee->empFirstName}}</td>
                @if(!empty($request->coldesignation))
                    <td>{{$employee->designation}}</td>
                @endif
                @if(!empty($request->coldepartment))
                    <td>{{$employee->departmentName}}</td>
                @endif
                @if(!empty($request->coljoiningdate))
                    <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('d-m-Y')}}</td>
                @endif
                @if(!empty($request->colgender))
                    <td style="text-align: center;">
                        @if($employee->empGenderId==1)
                            Male
                        @endif
                        @if($employee->empGenderId==2)
                            Female
                        @endif
                        @if($employee->empGenderId==3)
                            Other
                        @endif
                    </td>
                @endif
                @if(!empty($request->colunit))
                    <td>{{$employee->unitName}}</td>
                @endif
                @if(!empty($request->colsection))
                    <td>{{$employee->empSection}}</td>
                @endif

                @if(!empty($request->colskill_level))
                    <td>{{$employee->skill_level}}</td>
                @endif
                @if(!empty($request->colstatus))
                    <td>
                        @if($employee->discon_type==1)
                            Lefty
                        @elseif($employee->discon_type==2)
                            Resigned
                        @endif
                    </td>
                @endif

                @if(!empty($request->coldob))
                    <td>{{\Carbon\Carbon::parse($employee->empDOB)->format('d-m-Y')}}</td>
                @endif
                @if(!empty($request->colsalary))
                    <td>{{($employee->basic_salary+$employee->house_rant+$employee->medical+$employee->transport+$employee->food+$employee->other)}}</td>
                @endif
                <td>{{(\Carbon\Carbon::parse($employee->date_of_discontinuation)->format('d-m-Y'))}}</td>
                <td></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

</body>
</html>




