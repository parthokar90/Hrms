<!DOCTYPE html>
<html>
<head>
    <title>Salary Sheet Private</title>
</head>
<body>
<div id="print_area">
    <div class="company">
        <table>
            <thead>
            </thead>
            <tbody>
            <tr><td></td><td colspan="6" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
            <tr><td></td><td colspan="6" align="center"><h2><b>Pay Sheet for the month of</b></h2></td></tr>
            <tr><td></td><td colspan="6" align="center"><b>Payment date {{date('d-m-Y',strtotime($payment_date))}}</b></td></tr>
            <tr><td></td><td colspan="6" align="center"><b>@if($monthname){{date('F-Y',strtotime($monthname->month))}} @else NO Data Found @endif</b></td></tr>
            <tr><td></td><td colspan="6" align="center"><b>Days In Month:@if($monthname){{date('t',strtotime($monthname->month))}}@else @endif</b></td></tr>
            </tbody>
        </table>
    </div>
    @php $order=0; @endphp
    @foreach($department as $datas)
        @php
            $dept_id=$datas->department_id;
            $month=$months;
            $yearss=$ye;
            $monthss=$mo;
             $employees=DB::table('tb_salary_history_private')
           ->leftjoin('employees','tb_salary_history_private.emp_id','=','employees.id')
           ->leftjoin('payroll_grade','tb_salary_history_private.grade_id','=','payroll_grade.id')
           ->leftjoin('designations','tb_salary_history_private.designation_id','=','designations.id')
           ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
           ->leftjoin('tblines','employees.line_id','=','tblines.id')
           ->select('employees.employeeId as em_idss','tb_salary_history_private.emp_id','employees.work_group','tb_salary_history_private.present','tb_salary_history_private.weekend','tb_salary_history_private.holiday','tb_salary_history_private.total_payable_days','tb_salary_history_private.basic_salary','tb_salary_history_private.house_rant','tb_salary_history_private.medical','tb_salary_history_private.transport','tb_salary_history_private.food','tb_salary_history_private.gross','tb_salary_history_private.leave','tb_salary_history_private.total','tb_salary_history_private.working_day','tb_salary_history_private.absent','tb_salary_history_private.absent_deduction_amount','tb_salary_history_private.advanced_deduction','tb_salary_history_private.gross_pay','tb_salary_history_private.overtime','tb_salary_history_private.overtime_rate','tb_salary_history_private.overtime_amount','tb_salary_history_private.attendance_bonus','tb_salary_history_private.increment_bonus_percent','tb_salary_history_private.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','tb_salary_history_private.emp_id as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.employeeId')
           ->where('tb_salary_history_private.dates',$month)
           ->where('employees.date_of_discontinuation','=',null)
           ->where('tb_salary_history_private.department_id','=',$dept_id)
           ->get();
        @endphp
        <div class="main_div">
            <div class="department_text">
                <table>
                    <thead>
                    </thead>
                    <tbody>
                    <tr style="background: #000000;color: #ffffff;">
                        <td align="left">Department:</td>
                        <td align="left"><b>{{$datas->departmentName}}</b></td>
                        <td align="left">Section/Line:</td>
                        <td align="left"><b>{{$datas->line_no}}</b></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="salary_table">
                <table>
                    <tr>
                        <th>EmpID</th>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Grade</th>
                        <th>Basic</th>
                        <th>House</th>
                        <th>Medical</th>
                        <th>Food</th>
                        <th>Transport</th>
                        <th>Gross</th>
                        <th style="text-align: center" colspan="4">Leave</th>
                        <th>WorkDays</th>
                        <th>Abs Days</th>
                        <th>Weekend</th>
                        <th>Holiday</th>
                        <th>Total payable days</th>
                        <th colspan="3">Deduction</th>
                        <th>Gross Pay</th>
                        <th colspan="3">Overtime</th>
                        <th>Att Bonus</th>
                        <th>Special Allow</th>
                        <th>Net wages</th>
                        <th>Signature</th>
                    </tr>
                    @foreach($employees as $salary)
                        @php
                            $order++;
                            $test=$salary->emp_id;
                                  $leave_res=DB::SELECT("SELECT tb_salary_history_private.emp_id as main_id,leave_of_employee.leave_type_id,leave_of_employee.total
                                  FROM tb_salary_history_private
                                  LEFT JOIN leave_of_employee ON leave_of_employee.emp_id=tb_salary_history_private.emp_id
                                  WHERE YEAR(leave_of_employee.month)='$yearss' AND MONTH(leave_of_employee.month)='$monthss' AND tb_salary_history_private.dates='$month' AND tb_salary_history_private.emp_id=$test");
                        @endphp
                        <tr>
                            <td>{{$salary->em_idss}}</td>
                            <td>{{$salary->e_first_name}} <br>Join {{$salary->empJoiningDate}}</td>
                            <td>{{$salary->designation}}</td>
                            <td>{{$salary->grade_name}}</td>
                            <td>{{round($salary->basic_salary,0)}}</td>
                            <td>{{round($salary->house_rant,0)}}</td>
                            <td>{{round($salary->medical,0)}}</td>
                            <td>{{round($salary->food,0)}}</td>
                            <td>{{round($salary->transport,0)}}</td>
                            <td>{{round($salary->gross,0)}}</td>
                            @if($leave_res)
                                <td>
                                    @foreach($leave_res as $leave_type)
                                        @if($leave_type->leave_type_id==5)
                                            {{$leave_type->total}}
                                        @endif
                                    @endforeach
                                    @php
                                        $data=DB::table('leave_of_employee')->where('leave_type_id','=',5)->where('emp_id',$salary->emp_id)->count();
                                    @endphp
                                    @if($data==1)
                                    @else
                                        {{$data}}
                                    @endif
                                </td>
                                <td>
                                    @foreach($leave_res as $leave_type)
                                        @if($leave_type->leave_type_id==6)
                                            {{$leave_type->total}}
                                        @endif
                                    @endforeach
                                    @php
                                        $data=DB::table('leave_of_employee')->where('leave_type_id','=',6)->where('emp_id',$salary->emp_id)->count();
                                    @endphp
                                    @if($data==1)
                                    @else
                                        {{$data}}
                                    @endif
                                </td>
                                <td>
                                    @foreach($leave_res as $leave_type)
                                        @if($leave_type->leave_type_id==4)
                                            {{$leave_type->total}}
                                        @endif
                                    @endforeach
                                    @php
                                        $data=DB::table('leave_of_employee')->where('leave_type_id','=',4)->where('emp_id',$salary->emp_id)->count();
                                    @endphp
                                    @if($data==1)
                                    @else
                                        {{$data}}
                                    @endif
                                </td>
                                <td>
                                    0
                                </td>
                            @else
                                <td>
                                    0
                                </td>
                                <td>
                                    0
                                </td>
                                <td>
                                    0
                                </td>
                                <td>
                                    0
                                </td>
                            @endif
                            <td>
                                @if($salary->present=='')
                                    0
                                @else
                                    {{$salary->present}}
                                @endif
                            </td>
                            <td>
                                @if($salary->absent=='')
                                    0
                                @else
                                    {{$salary->absent}}
                                @endif
                            </td>
                            <td>{{$salary->weekend}}</td>
                            <td>
                                @if($salary->holiday=='')
                                    0
                                @else
                                    {{$salary->holiday}}
                                @endif
                            </td>
                            <td>{{$salary->total_payable_days}}</td>
                            <td>
                                @if($salary->advanced_deduction=='')
                                    Adv:0
                                @else
                                    Adv:{{round($salary->advanced_deduction,0)}}
                                @endif
                            </td>
                            <td>
                                @if($salary->absent_deduction_amount=='')
                                    Absent:0
                                @else
                                    Absent:{{round($salary->absent_deduction_amount,0)}}
                                @endif
                            </td>
                            <td>Stm: 10</td>
                            <td>
                                @if($salary->gross_pay=='')
                                    0
                                @else
                                    {{round($salary->gross_pay,0)}}
                                @endif
                            </td>
                            <td>
                                @if($salary->overtime=='')
                                    Hrs:0/hr
                                @else
                                    Hrs:{{$salary->overtime}}/hr
                                @endif
                            </td>
                            <td>
                                @if($salary->work_group=='Staff')
                                @else
                                    {{$salary->overtime_rate}}
                                @endif
                            </td>
                            <td>
                                @if($salary->overtime_amount=='')
                                    Amt:0
                                @else
                                    Amt:{{round($salary->overtime_amount,0)}}
                                @endif
                            </td>
                            <td>
                                @if($salary->absent <3 && $salary->total=='')
                                    {{round($salary->attendance_bonus,0)}}
                                @else
                                    0
                                @endif
                            </td>
                            <td>
                                @if($salary->increment_bonus_percent=='')
                                    0
                                @else
                                    {{round($salary->increment_bonus_percent,0)}}
                                @endif
                            </td>
                            <td>
                                @if($salary->absent <3 && $salary->total=='')
                                    {{round($salary->net_amount,0)}}
                                @else
                                    @php
                                        $net=$salary->net_amount;
                                        $bonus=$salary->attendance_bonus;
                                        $total=$net-$bonus
                                    @endphp
                                    {{round($total,0)}}
                                @endif
                            </td>
                            <td></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    @endforeach
    <div class="authority">
        <div class="prepared_by">
            <table>
                <thead>
                </thead>
                <tbody>
                <tr>
                    <td align="left">Prepared By:</td>
                    <td align="left">Audited By:</td>
                    <td align="left">Recommended By:</td>
                    <td align="left">Approved By:</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>