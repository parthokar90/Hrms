<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Date Wise Overtime</title>
</head>
<body>

<div>
    <table>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center">Overtime from <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y')}} </b> to <b> {{\Carbon\Carbon::parse($request->endDate)->format('d M Y')}} </b></td></tr>

        </tbody>
    </table>

        @if(!empty($present))
            <table>
                <thead>
                <tr>
                    <th align="center">ID</th>
                    <th>Name</th>
                    @if(!empty($request->coldesignation))
                        <th>Designation</th>
                    @endif
                    @if(!empty($request->coldepartment))
                        <th>Department</th>
                    @endif
                    @if(!empty($request->coljoiningdate))
                        <th>Joining Date</th>
                    @endif
                    @if(!empty($request->colgender))
                        <th>Gender</th>
                    @endif
                    @if(!empty($request->colunit))
                        <th>Unit</th>
                    @endif
                    @if(!empty($request->colfloor))
                        <th>Floor</th>
                    @endif
                    @if(!empty($request->colline))
                        <th>Line</th>
                    @endif
                    @if(!empty($request->colsection))
                        <th>Section</th>
                    @endif
                    @if(!empty($request->colstatus))
                        <th>Status</th>
                    @endif
                    <th>Date</th>
                    <th>In Time</th>
                    @if(!empty($request->collateTime))
                        <th>Late Time</th>
                    @endif
                    <th>Out Time</th>
                    <th>O.T.</th>
                </tr>
                </thead>
                <tbody>
                @foreach($present as $employee)
                    <tr>
                        <td align="center">{{$employee->employeeId}}</td>
                        <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                        @if(!empty($request->coldesignation))
                            <td>{{$employee->designation}}</td>
                        @endif
                        @if(!empty($request->coldepartment))
                            <td>{{$employee->departmentName}}</td>
                        @endif
                        @if(!empty($request->coljoiningdate))
                            <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                        @endif
                        @if(!empty($request->colgender))
                            <td>
                                @if($employee->empGenderId==1)
                                    Male
                                @endif
                                @if($employee->empGenderId==2)
                                    Female
                                @endif
                                @if($employee->empGenderId==3)
                                    Other
                                @endif
                            </td>
                        @endif
                        @if(!empty($request->colunit))
                            <td>{{$employee->unitName}}</td>
                        @endif
                        @if(!empty($request->colfloor))
                            <td>{{$employee->floorName}}</td>
                        @endif
                        @if(!empty($request->colline))
                            <td>{{$employee->LineName}}</td>
                        @endif
                        @if(!empty($request->colsection))
                            <td>{{$employee->empSection}}</td>
                        @endif
                        @if(!empty($request->colstatus))
                            <td>
                                @if($employee->empAccStatus==1)
                                    Active
                                @elseif($employee->empAccStatus==0)
                                    Inactive
                                @endif
                            </td>
                        @endif
                        <td>
                            {{ \Carbon\Carbon::parse($employee->date)->format('d M Y') }}
                        </td>
                        <td>
                            {{$employee->in_time}}
                        </td>
                        @if(!empty($request->collateTime))
                            <td>
                                @if($employee->in_time>$employee->max_entry_time)
                                    <span class="red-text">{{ date('H:i:s', strtotime($employee->in_time) - strtotime($employee->max_entry_time)) }}</span>
                                @endif
                            </td>
                        @endif
                        <td>
                            {{$employee->out_time}}
                        </td>

                        <td>
                            @if(isset($employee->out_time))
                                @if($employee->out_time>$employee->exit_time)
                                    {{ date('H:i:s', strtotime($employee->out_time) - strtotime($employee->exit_time)) }}
                                @endif
                            @endif
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>

        @else
            <hr>
            <h4 style="color:red;"><center> No record found.</center></h4>
    @endif

</div>

</body>
</html>




