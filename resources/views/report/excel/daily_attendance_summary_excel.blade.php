<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Attendance Summary</title>
</head>
<body>

<div>
    <table>
        <thead>
        </thead>
        <tbody>
        <tr><td></td><td colspan="4" align="center"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_address1}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_email}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center"><b>{{$companyInformation->company_phone}}</b></td></tr>
        <tr><td></td><td colspan="4" align="center">Daily attendance summary for <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y (l)')}} </b></td></tr>

        </tbody>
    </table>



    <center>

        @if(!empty($data))
            <table>
                <thead>
                <tr>
                    @if($request->reportType==1)
                        <th>Department</th>
                    @elseif($request->reportType==2)
                        <th>Section</th>
                    @elseif($request->reportType==3)
                        <th>Floor</th>
                        <th>Line</th>
                    @elseif($request->reportType==4)
                        <th>Designation</th>
                    @elseif($request->reportType==5)
                        <th>Line</th>
                    @endif
                    <th>Total</th>
                    <th>Present</th>
                    <th>Absent</th>
                    <th>Late</th>
                    <th>Leave</th>

                </tr>
                </thead>
                <tbody>
                @foreach($data as $d)
                    <tr>
                        @if($request->reportType==3)
                            <td>{{\Illuminate\Support\Facades\DB::table('floors')->where('id','=',$d['designationId'])->select('floor')->first()->floor}}</td>
                        @endif
                        <td>{{$d['designationName']}}</td>
                        <td>{{$d['total']}}</td>
                        <td>{{ $d['present']  }}</td>
                        <td>{{ $d['total']-($d['present']+$d['leave']) }}</td>
                        <td>{{ $d['late'] }}</td>
                        <td>{{ $d['leave'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div style="float:left;padding-top: 15px; font-size: 11px; text-align: left;">
                <span><b>Total Employee: </b> {{$totalE}}</span><br>
                <span><b>Total Present :</b> {{$totalP}}</span><br>
                <span><b>Total Absent Employee :</b> {{ $totalE-($totalL+$totalP) }}</span><br>
                <span><b>Total Late Employee :</b> {{ $Tlate }}</span><br>
                <span><b>Total Employee On Leave :</b> {{ $totalL }}</span><br>

            </div>

        @else
            <hr>
            <h4 style="color:red;"><center> No Absent data found.</center></h4>
    @endif

</div>

</body>
</html>




