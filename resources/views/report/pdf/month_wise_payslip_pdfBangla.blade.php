<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!DOCTYPE html>
<html>
<head>
    @if(isset($data[0]))
        <title> {{en2bnNumber(date('F-Y',strtotime($data[0]->month)))}} এর Payslip</title>
    @endif
    <style>
        body{
            font-family: 'bangla', sans-serif;
            font-size: 11px;

        }
        p{  
            line-height: 1px;
        }
        table td{
            padding-left: 4px;
            font-size: 17px;
        }
        section {
            width: 96%;
            height: 320px;
            margin: auto;
            padding: 3px;
        }
        div#one {
            width: 48%;
            height: 300px;
            background: #FFFFFF;
            float: left;
            border: 1px solid #000;
        }
        div#two {
            width: 48%;
            height: 300px;
            background: #FFFFFF;
            float: right;
            border: 1px solid #000;
        }

    </style>
</head>
<body>
@php
    $order=0;
    $page=0;
    $currencyprefix = "টাকা";
    $currencysuffix = "";
@endphp
@foreach($data as $key=>$payslip_report)
    @php
        $order++;
        $s_number = str_pad( "$order", 4, "0", STR_PAD_LEFT );
    @endphp
    <section>
        <div id="one">
            <table style="width:450px">
                <tbody>
                <tr>
                    <td><h4>ক্রমিক নং: {{en2bnNumber($s_number)}}</h4></td>
                    {{--  <td><h5 style="margin-bottom: 3px; margin-top:-10px; margin-left:10px;">{{$companyInformation->company_name}}</h5></td>  --}}
                    <td><h4>{{$companyInformation->company_name}}</h4></td>
                    <td>তারিখ: {{en2bnNumber(date('d-m-y'))}}</td>
                </tr>  
                </tbody>
            </table>
            <h5 style="margin-bottom: 3px; margin-top:0px; text-align: center;">Pay Slip</h5>
            <table style="width:450px">
                <tbody>
                <tr>
                    <td width="155px !important"><b style="margin-right: 2px;">মাস: </b>{{en2bnNumber(date('M Y',strtotime($payslip_report->month)))}}</td>
                    <td><b style="margin-right: 2px;">সেকশন: </b> {{$payslip_report->empSection}}</td>
                </tr>
                <tr>
                    <td><b style="font-weight: bold">আইডি: </b>{{$payslip_report->employeeId}}</td>
                    <td><b style="margin-right: 2px;">লাইন: </b>{{$payslip_report->line_no}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">গ্রেড: </b>{{$payslip_report->grade_name}}</td>
                    <td><b style="margin-right: 2px;">পদবী: </b>{{$payslip_report->designation}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">নাম: </b>{{$payslip_report->empFirstName." ".$payslip_report->empLastName}}</td>
                    <td><b style="margin-right: 2px;">যোগদানের তারিখ: </b>{{en2bnNumber(date('d-m-Y',strtotime($payslip_report->empJoiningDate)))}}</td>

                </tr>
                </tbody>
            </table>
            <div style="display:block;border-top: 1px solid #666;margin-top: 2px;"></div>
            <table style="width:450px">
                <tbody>
                <tr>
                    <td><b>মুল বেতন: </b> {{en2bnNumber($payslip_report->basic_salary)}} {{$currencyprefix}}</td>
                    <td><b>ওভারটাইম দর: </b>{{en2bnNumber($payslip_report->overtime_rate)}} প্রতি ঘন্টা</td>
                </tr>
                <tr>
                    <td> <b style="margin-right: 2px;">বাড়ি ভাড়া: </b>{{en2bnNumber($payslip_report->house_rant)}} {{$currencyprefix}} </td>
                    <td><b style="margin-right: 2px;">যাতায়াত ভাড়া: </b>{{en2bnNumber($payslip_report->transport)}} {{$currencyprefix}}</td>

                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">চিকিৎসা ভাতা: </b>{{en2bnNumber($payslip_report->medical)}} {{$currencyprefix}} </td>
                    <td><b style="margin-right: 2px;">ওভারটাইম: </b>@if($payslip_report->overtime=='') ০ @else {{en2bnNumber($payslip_report->overtime)}} @endif ঘন্টা</td>

                </tr>
                <tr>
                    <td width="155px !important"><b style="margin-right: 2px;">ওভারটাইম টাকা: </b>{{en2bnNumber($payslip_report->overtime_amount)}} {{$currencyprefix}}</td>
                    <td><b style="margin-right: 2px;">হাজিরা বোনাস: </b>{{en2bnNumber($payslip_report->attendance_bonus+$payslip_report->attendance_bonus_percent)}} {{$currencyprefix}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">বিশেষ ভাতা: </b>@if($payslip_report->increment_bonus_amount=='' && $payslip_report->increment_bonus_percent=='') ০ @else {{en2bnNumber($payslip_report->increment_bonus_amount+$payslip_report->increment_bonus_percent)}}@endif {{$currencyprefix}}</td>
                    <td><b style="margin-right: 2px;">খাদ্য ভাতা: </b>{{en2bnNumber($payslip_report->food)}} {{$currencyprefix}}</td>
                </tr>

                <tr>
                    <td><b style="margin-right: 2px;">মোট প্রদেয় টাকা: </b>{{en2bnNumber(round($payslip_report->gross_pay,0))}}{{$currencyprefix}}</td>
                    <td><b style="margin-right: 2px;">ছুটি: </b>@if($payslip_report->total=='') ০ @else {{en2bnNumber($payslip_report->total)}} @endif দিন</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">কর্তন: </b>{{en2bnNumber(round($payslip_report->absent_deduction_amount,0))}} {{$currencyprefix}} </td>
                    <td><b style="margin-right: 2px;">মোট কার্যদিবস: </b>{{en2bnNumber($payslip_report->working_day)}} দিন</td>

                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">উৎসব বোনাস: </b>{{en2bnNumber($payslip_report->festival_bonus_amount+$payslip_report->festival_bonus_percent)}} {{$currencyprefix}} </td>
                    <td><b style="margin-right: 2px;">সাপ্তাহিক ছুটি: </b>{{en2bnNumber($payslip_report->weekend)}} দিন</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">সর্বসাকুল্যে বেতন: </b>{{en2bnNumber(round($payslip_report->net_amount,0))}} {{$currencyprefix}}</td>
                    <td><b style="margin-right: 2px;">অনুপস্থিত: </b>{{en2bnNumber($payslip_report->absent)}} দিন</td>
                </tr>
                
                </tbody>
            </table>
            <br>
            <table style="width:450px">
                <tbody>
                <tr>
                    <td><h3>কর্তৃপক্ষ</h3></td>
                    <td><h5 style="margin-bottom: 3px; margin-top:-10px; text-align: center !important;">Office Copy</h5></td>
                    <td><h3>গ্রহীতা</h3></td>
                </tr>  
                </tbody>
            </table>
             {{--  <br><span style="padding-left: 20px; font-weight: bold;">কর্তৃপক্ষ</span> <span style="padding-left: 40px;">Office Copy</span><span style="font-weight:bold; padding-left: 30px;">গ্রহিতা</span></span>  --}}

        </div>
        <div id="two">
                <table style="width:450px">
                        <tbody>
                        <tr>
                            <td><h4>ক্রমিক নং: {{en2bnNumber($s_number)}}</h4></td>
                            {{--  <td><h5 style="margin-bottom: 3px; margin-top:-10px; margin-left:10px;">{{$companyInformation->company_name}}</h5></td>  --}}
                            <td><h4>{{$companyInformation->company_name}}</h4></td>
                            <td>তারিখ: {{en2bnNumber(date('d-m-y'))}}</td>
                        </tr>  
                        </tbody>
                    </table>
                    <h5 style="margin-bottom: 3px; margin-top:0px; text-align: center;">Pay Slip</h5>
                <table style="width:450px">
                    <tbody>
                    <tr>
                        <td width="155px !important"><b style="margin-right: 2px;">মাস: </b>{{en2bnNumber(date('M-Y',strtotime($payslip_report->month)))}}</td>
                        <td><b style="margin-right: 2px;">সেকশন: </b> {{$payslip_report->empSection}}</td>
                    </tr>
                    <tr>
                        <td><b style="margin-right: 2px;">আইডি: </b>{{$payslip_report->employeeId}}</td>
                        <td><b style="margin-right: 2px;">লাইন: </b>{{$payslip_report->line_no}}</td>
                    </tr>
                    <tr>
                        <td><b style="margin-right: 2px;">গ্রেড: </b>{{$payslip_report->grade_name}}</td>
                        <td><b style="margin-right: 2px;">পদবী: </b>{{$payslip_report->designation}}</td>
                    </tr>
                    <tr>
                        <td><b style="margin-right: 2px;">নাম: </b>{{$payslip_report->empFirstName." ".$payslip_report->empLastName}}</td>
                        <td><b style="margin-right: 2px;">যোগদানের তারিখ: </b>{{en2bnNumber(date('d-m-Y',strtotime($payslip_report->empJoiningDate)))}}</td>
    
                    </tr>
                    </tbody>
                </table>
                <div style="display:block;border-top: 1px solid #666;margin-top: 2px;"></div>
                <table style="width:450px">
                    <tbody>
                    <tr>
                        <td><b>মুল বেতন: </b> {{en2bnNumber($payslip_report->basic_salary)}} {{$currencyprefix}}</td>
                        <td><b>ওভারটাইম দর: </b>{{en2bnNumber($payslip_report->overtime_rate)}} প্রতি ঘন্টা</td>
                    </tr>
                    <tr>
                        <td> <b style="margin-right: 2px;">বাড়ি ভাড়া: </b>{{en2bnNumber($payslip_report->house_rant)}} {{$currencyprefix}} </td>
                        <td><b style="margin-right: 2px;">যাতায়াত ভাড়া: </b>{{en2bnNumber($payslip_report->transport)}} {{$currencyprefix}}</td>
    
                    </tr>
                    <tr>
                        <td><b style="margin-right: 2px;">চিকিৎসা ভাতা: </b>{{en2bnNumber($payslip_report->medical)}} {{$currencyprefix}} </td>
                        <td><b style="margin-right: 2px;">ওভারটাইম: </b>@if($payslip_report->overtime=='') ০ @else {{en2bnNumber($payslip_report->overtime)}} @endif ঘন্টা</td>
    
                    </tr>
                    <tr>
                        <td width="155px !important"><b style="margin-right: 2px;">ওভারটাইম টাকা: </b>{{en2bnNumber($payslip_report->overtime_amount)}} {{$currencyprefix}}</td>
                        <td><b style="margin-right: 2px;">হাজিরা বোনাস: </b>{{en2bnNumber($payslip_report->attendance_bonus+$payslip_report->attendance_bonus_percent)}} {{$currencyprefix}}</td>
                    </tr>
                    <tr>
                        <td><b style="margin-right: 2px;">বিশেষ ভাতা: </b>@if($payslip_report->increment_bonus_amount=='' && $payslip_report->increment_bonus_percent=='') ০ @else {{en2bnNumber($payslip_report->increment_bonus_amount+$payslip_report->increment_bonus_percent)}}@endif {{$currencyprefix}}</td>
                        <td><b style="margin-right: 2px;">খাদ্য ভাতা: </b>{{en2bnNumber($payslip_report->food)}} {{$currencyprefix}}</td>
                    </tr>
    
                    <tr>
                        <td><b style="margin-right: 2px;">মোট প্রদেয় টাকা: </b>{{en2bnNumber(round($payslip_report->gross_pay,0))}}{{$currencyprefix}}</td>
                        <td><b style="margin-right: 2px;">ছুটি: </b>@if($payslip_report->total=='') ০ @else {{en2bnNumber($payslip_report->total)}} @endif দিন</td>
                    </tr>
                    <tr>
                        <td><b style="margin-right: 2px;">কর্তন: </b>{{en2bnNumber(round($payslip_report->absent_deduction_amount,0))}} {{$currencyprefix}} </td>
                        <td><b style="margin-right: 2px;">মোট কার্যদিবস: </b>{{en2bnNumber($payslip_report->working_day)}} দিন</td>
    
                    </tr>
                    <tr>
                        <td><b style="margin-right: 2px;">উৎসব বোনাস: </b>{{en2bnNumber($payslip_report->festival_bonus_amount+$payslip_report->festival_bonus_percent)}} {{$currencyprefix}} </td>
                        <td><b style="margin-right: 2px;">সাপ্তাহিক ছুটি: </b>{{en2bnNumber($payslip_report->weekend)}} দিন</td>
                    </tr>
                    <tr>
                        <td><b style="margin-right: 2px;">সর্বসাকুল্যে বেতন: </b>{{en2bnNumber(round($payslip_report->net_amount,0))}} {{$currencyprefix}}</td>
                        <td><b style="margin-right: 2px;">অনুপস্থিত: </b>{{en2bnNumber($payslip_report->absent)}} দিন</td>
                    </tr>
                    
                    </tbody>
                </table>
                <br>
                <table style="width:450px">
                    <tbody>
                    <tr>
                        <td><h3>কর্তৃপক্ষ</h3></td>
                        <td><h5 style="margin-bottom: 3px; margin-top:-10px; text-align: center !important;">Employee Copy</h5></td>
                        <td><h3>গ্রহীতা</h3></td>
                    </tr>  
                    </tbody>
                </table>
                 {{--  <br><span style="padding-left: 20px; font-weight: bold;">কর্তৃপক্ষ</span> <span style="padding-left: 40px;">Office Copy</span><span style="font-weight:bold; padding-left: 30px;">গ্রহিতা</span></span>  --}}
    
            </div>
    </section>
@endforeach
</body>
</html>




