<!DOCTYPE html>
<html>
<head>
    @if(isset($data[0]))
        <title>Payslip of {{date('F-Y',strtotime($data[0]->month))}}</title>
    @endif
    <style>
        body{
            font-size: 12px;
        }
        table td{
            padding-left: 4px;
        }
        section {
            width: 95%;
            height: 319px;
            margin: auto;
            padding: 10px;
        }
        div#one {
            width: 48%;
            height: 328px;
            background: #FFFFFF;
            float: left;
            border: 1px solid #000;
        }
        div#two {
            width: 48%;
            height: 328px;
            background: #FFFFFF;
            float: right;
            border: 1px solid #000;
        }
        .page-break {
            page-break-after: always;
        }

    </style>
</head>
<body>
@php
    $order=0;
    $page=0;
    $currencyprefix = "Tk";
    $currencysuffix = "";
@endphp
@foreach($data as $key=>$payslip_report)
    @php
        $order++;
        $s_number = str_pad( "$order", 4, "0", STR_PAD_LEFT );
    @endphp
    <section>
        <div id="one">
            <p><span Style="margin-left:2%; font-size: 12px;">Sl. NO: {{$s_number}}</span><span style="padding-left: 30px; margin-right: 7%; font-weight:bold;font-size: 12px;">{{$companyInformation->company_name}}</span><span style="margin-right:1%; font-size: 12px;">Date:{{date('d-m-y')}}</span></p>
            <h5 style="margin-bottom: 3px; margin-top:-10px; text-align: center;">Pay Slip</h5>
            <table>
                <tbody>
                <tr>
                    <td width="155px !important"><b style="margin-right: 2px;">Month:</b>{{date('F-Y',strtotime($payslip_report->month))}}</td>
                    <td><b style="margin-right: 2px;">Section:</b> {{$payslip_report->empSection}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Id No:</b>{{$payslip_report->employeeId}}</td>
                    <td><b style="margin-right: 2px;">Line:</b>{{$payslip_report->line_no}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Grade:</b>{{$payslip_report->grade_name}}</td>
                    <td><b style="margin-right: 2px;">Des:</b>{{$payslip_report->designation}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Name:</b>{{$payslip_report->empFirstName." ".$payslip_report->empLastName}}</td>
                    <td><b style="margin-right: 2px;">Join Date:</b>{{date('d-m-Y',strtotime($payslip_report->empJoiningDate))}}</td>

                </tr>
                </tbody>
            </table>
            <div style="display:block;border-top: 1px solid #666;margin-top: 2px;"></div>
            <table>
                <tbody>
                <tr>
                    <td width="155px !important"><b style="margin-right: 2px;">Basic:</b> {{$currencyprefix}} {{$payslip_report->basic_salary}} {{$currencysuffix}}</td>
                    <td><b style="margin-right: 2px;">OT Rate:</b>{{$payslip_report->overtime_rate}}</td>
                </tr>
                <tr>
                    <td> <b style="margin-right: 2px;">House:</b>{{$currencyprefix}} {{$payslip_report->house_rant}} {{$currencysuffix}} </td>
                    <td><b style="margin-right: 2px;">Transport:</b>{{$currencyprefix}} {{$payslip_report->transport}} {{$currencysuffix}}</td>

                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Medical:</b>{{$currencyprefix}} {{$payslip_report->medical}} {{$currencysuffix}} </td>
                    <td><b style="margin-right: 2px;">Overtime:</b>@if($payslip_report->overtime=='') 0/h @else {{$payslip_report->overtime}}/h @endif </td>

                </tr>
                <tr>
                    <td width="155px !important"><b style="margin-right: 2px;">Ot Amount:</b>{{$currencyprefix}} {{$payslip_report->overtime_amount}} {{$currencysuffix}}</td>
                    <td><b style="margin-right: 2px;">Att Bonus:</b> {{$currencyprefix}} {{$payslip_report->attendance_bonus+$payslip_report->attendance_bonus_percent}}{{$currencysuffix}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Special Allow:</b>@if($payslip_report->increment_bonus_amount=='' && $payslip_report->increment_bonus_percent=='') 0 @else {{$payslip_report->increment_bonus_amount+$payslip_report->increment_bonus_percent}}@endif</td>
                    <td><b style="margin-right: 2px;"> Food:</b>{{$payslip_report->food}} </td>
                </tr>

                <tr>
                    <td><b style="margin-right: 2px;">Gross Pay:</b>{{$currencyprefix}} {{round($payslip_report->gross_pay,0)}}{{$currencysuffix}}</td>
                    <td><b style="margin-right: 2px;">Leave:</b>@if($payslip_report->total=='') 0 @else {{$payslip_report->total}} @endif</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Deduction:</b>{{$currencyprefix}} {{round($payslip_report->absent_deduction_amount,0)}}{{$currencysuffix}} </td>
                    <td><b style="margin-right: 2px;">Work days:</b>{{$payslip_report->working_day}}</td>

                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Fest Bonus:</b> {{$currencyprefix}} {{$payslip_report->festival_bonus_amount+$payslip_report->festival_bonus_percent}}{{$currencysuffix}} </td>
                    <td><b style="margin-right: 2px;"> Weekend:</b>{{$payslip_report->weekend}} </td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Net:</b>{{$currencyprefix}} {{round($payslip_report->net_amount,0)}} {{$currencysuffix}}</td>
                    <td><b style="margin-right: 2px;">Absent:</b>{{$payslip_report->absent}}</td>
                </tr>
                </tbody>
            </table>
            <br><span style="padding-left: 20px; font-weight: bold;">Authority</span> <span style="padding-left: 40px;">Office Copy</span><span style="font-weight:bold; padding-left: 30px;">The recipient</span></span>

        </div>
        <div id="two">
            <p><span Style="margin-left:2%; font-size: 12px;">Sl. NO: {{$s_number}}</span><span style="margin-left: 30px; font-weight:bold; margin-right:7%; font-size: 12px;">{{$companyInformation->company_name}}</span><span style="font-size: 12px;">Date:{{date('d-m-y')}}</span></p>
            <h5 style="margin-bottom:3px; margin-top:-10px; text-align: center;">Pay Slip</h5>
            <table>
                <tbody>
                <tr>
                    <td width="155px !important"><b style="margin-right: 2px;">Month:</b>{{date('F-Y',strtotime($payslip_report->month))}}</td>
                    <td><b style="margin-right: 2px;">Section:</b>{{$payslip_report->empSection}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Id No:</b>{{$payslip_report->employeeId}}</td>
                    <td><b style="margin-right: 2px;">Line:</b>{{$payslip_report->line_no}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Grade:</b>{{$payslip_report->grade_name}}</td>
                    <td><b style="margin-right: 2px;">Des:</b>{{$payslip_report->designation}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Name:</b>{{$payslip_report->empFirstName." ".$payslip_report->empLastName}}</td>
                    <td><b style="margin-right: 2px;">Join Date:</b>{{date('d-m-Y',strtotime($payslip_report->empJoiningDate))}}</td>
                </tr>
                </tbody>
            </table>
            <div style="display:block;border-top: 1px solid #666;margin-top: 2px;"></div>
            <table>
                <tbody>
                <tr>
                    <td width="155px !important"><b style="margin-right: 2px;">Basic:</b> {{$currencyprefix}} {{$payslip_report->basic_salary}} {{$currencysuffix}}</td>
                    <td><b style="margin-right: 2px;">OT Rate:</b>{{$payslip_report->overtime_rate}}</td>
                </tr>
                <tr>
                    <td> <b style="margin-right: 2px;">House:</b>{{$currencyprefix}} {{$payslip_report->house_rant}} {{$currencysuffix}} </td>
                    <td><b style="margin-right: 2px;">Transport:</b>{{$currencyprefix}} {{$payslip_report->transport}} {{$currencysuffix}}</td>

                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Medical:</b>{{$currencyprefix}} {{$payslip_report->medical}} {{$currencysuffix}} </td>
                    <td><b style="margin-right: 2px;">Overtime:</b>@if($payslip_report->overtime=='') 0/h @else {{$payslip_report->overtime}}/h @endif </td>

                </tr>
                <tr>
                    <td width="155px !important"><b style="margin-right: 2px;">Ot Amount:</b>{{$currencyprefix}} {{$payslip_report->overtime_amount}} {{$currencysuffix}}</td>
                    <td><b style="margin-right: 2px;">Att Bonus:</b> {{$currencyprefix}} {{$payslip_report->attendance_bonus+$payslip_report->attendance_bonus_percent}}{{$currencysuffix}}</td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Special Allow:</b>@if($payslip_report->increment_bonus_amount=='' && $payslip_report->increment_bonus_percent=='') 0 @else {{$payslip_report->increment_bonus_amount+$payslip_report->increment_bonus_percent}}@endif</td>
                    <td><b style="margin-right: 2px;"> Food:</b>{{$payslip_report->food}} </td>
                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Gross Pay:</b>{{$currencyprefix}} {{round($payslip_report->gross_pay,0)}}{{$currencysuffix}}</td>
                    <td><b style="margin-right: 2px;">Leave:</b>@if($payslip_report->total=='') 0 @else {{$payslip_report->total}} @endif</td>

                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Deduction:</b>{{$currencyprefix}} {{round($payslip_report->absent_deduction_amount,0)}}{{$currencysuffix}} </td>
                    <td><b style="margin-right: 2px;">Work days:</b>{{$payslip_report->working_day}}</td>

                </tr>
                <tr>
                    <td><b style="margin-right: 2px;">Fest Bonus:</b> {{$currencyprefix}} {{$payslip_report->festival_bonus_amount+$payslip_report->festival_bonus_percent}}{{$currencysuffix}} </td>
                    <td><b style="margin-right: 2px;"> Weekend:</b>{{$payslip_report->weekend}} </td>
                </tr>

                <tr>
                    <td><b style="margin-right: 2px;">Net:</b>{{$currencyprefix}} {{round($payslip_report->net_amount,0)}} {{$currencysuffix}}</td>
                    <td><b style="margin-right: 2px;">Absent:</b>{{$payslip_report->absent}}</td>
                </tr>
                </tbody>
            </table>
            <br><span style="padding-left: 15px; font-weight: bold;">Authority</span> <span style="padding-left: 30px;">Customer Copy</span><span style="padding-left: 30px; font-weight: bold;">The recipient</span></span>

        </div>
    </section>
    @if(count($data)!=$order)
        @if($order%3==0)
            <div class="page-break"></div>

        @endif
    @endif
@endforeach
</body>
</html>




