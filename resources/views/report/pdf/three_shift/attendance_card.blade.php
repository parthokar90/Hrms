<?php

function roundTime($time){
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));

    if($minute<25){
        $minute=0;
    }
    elseif ($minute>=25 && $minute<55){
        $minute=.5;
    }
    else{
        $minute=0;
        $hour++;
    }
    $kind=number_format((float)($hour+$minute),1,'.','');
    return $kind;

}

function late_time($pretime,$time){
    $phour=date('G',strtotime($pretime));
    $pminute=date('i',strtotime($pretime));
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));
    $thour=$phour+$hour;
    $tminute=$pminute+$minute;
    $h=floor($tminute/60);
    $tminute=sprintf("%02d",$tminute%60);
    $thour+=$h;
    $thour=sprintf("%02d",$thour);
    return "$thour:$tminute";

}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Attendance Card</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 90%;
            font-size: 11px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            /*border: 1px solid #ddd;*/
            font-size: 12px;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: center !important;

        }

        #customers th {
            text-align: left;
            padding: 5px;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }

        #cardFooter{
            border-collapse: collapse;
            font-size:11px;
            width:70%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */

        }
        .reportDateRange{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif  !important;
            font-size: 12px !important;
            font-weight: bold;
        }
        #cardFooter td {
            text-align: left !important;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportDateRange">Job Card Report Month From <b>date d-m-y</b> to <b>date d-m-y </b></p>
    </div>
 
        <table id="employeeDetails" >
            <tr>
                <td style="text-align:left" width="35%"><b>Employee ID :</b> </td>
                <td style="text-align:left" width="35%"><b>Name : </b> </td>
                <td style="text-align:left" width="30%"><b>Joining Date :</b> </td>
            </tr>
            <tr>
                <td style="text-align:left" width="35%"><b>Designation :</b> </td>
                <td style="text-align:left" width="35%"><b>Department : </b> </td>
                <td style="text-align:left" width="30%"><b> Section : </b></td>
            </tr>

        </table>

        <table id='customers' style="margin-top:15px;font-size:11px;" border="1px">
            <thead>
            <tr>
                <th>Date</th>
                <th>Day</th>
                <th>In Time </th>
                <th>Out Time</th>
                <th>Late Time</th>
                <th>Status</th>
                <th>Remarks</th>
            </tr>
            </thead>
            <tbody>
         
                <tr>
                    <td>rr</td>
                    <td>wer</td>
                    <td>
                      5
                    </td>
                    <td>
                     4
                    </td>
                    <td>
                        4
                    </td>


                    <td>
                        5
                    </td>
                    <td></td>
                </tr>
          
            <tr style="font-weight: bold">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>444</td>


                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
</div>
<div>
    <center>
        <table id="cardFooter">
            <tr>

                <td width="50%"><b>Total Days (In Month): </b>30 Days</td>
            </tr>

            <tr>
                {{--$working_days--}}
                <td width="50%"><b>Holiday: </b>1 Days</td>
            </tr>
            <tr>
                {{--$working_days--}}
                <td width="50%"><b>Weekends: </b>2 Days</td>
            </tr>
            <tr>
                <td  width="50%"><b>Total Present Days: </b> 3 Days</td>
            </tr>
            <tr>
                <td class="padTop10"><b>Total Leave :</b> 4 Days</td></tr>
            <tr>
                <td class="padTop10"><b>Total Absent :</b> 4 Days</td>
            </tr>

        </table>
    </center>
</div>


    <center><h4 style="color: #FF0000">No data found for the given date</h4></center>





</body>
</html>




