
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{--<link media="all" type="text/css" rel="stylesheet" href="http://localhost/Hrms/public/hrm_script/css/style.css">--}}

    <title>Completed Training Report</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: center !important;

        }

        #customers th {
            text-align: left;
            padding: 5px;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 13px;
        }

        #cardFooter{
            border-collapse: collapse;
            font-size:12px;
            width:25%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */

        }

        #cardFooter td {
            text-align: center !important;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">Far-East IT Solutions Limited</h2>
        <p class="reportHeader">House #51, Road #18 Sector #11, Uttara, Dhaka-1230</p>
        <p class="reportHeader">Email:  info@feits.co</p>
        <p class="reportHeader">Phone: +09678-771206, +880 1852 665521</p>
    </div>
    <center>
        <h4>Training Wise Employee list</h4>
        <h5><strong>Report showing from {{date("d-M-Y",strtotime($request->training_start))}} to {{date("d-M-Y",strtotime($request->training_end))}}</strong></h5> 
    </center>

    @if(count($trainingwiseEmployee)==0)
    <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @else
    <table id='customers' style="margin-top:15px;font-size:12px;" border="1px">
        <thead>
            <tr>
                <th>Order</th>
                <th>Training Name</th>
                <th>Starting Date</th>
                <th>Ending Date</th>
                <th>Attendents</th>
                <th>Status</th>
                
            </tr>
            </thead>
            <tbody>
            
            @php $order=0; @endphp
            @foreach($trainingwiseEmployee as $data)
            @php
            if(($data->training_starting_date)>=date("Y-m-d")&&($data->training_ending_date)<=date("Y-m-d")){
                $status='Active';
            }
            else if(($data->training_starting_date)<=date("Y-m-d")&&($data->training_ending_date)<=date("Y-m-d")){
                $status='Completed';
            }
            else if(($data->training_starting_date)>=date("Y-m-d")&&($data->training_ending_date)>=date("Y-m-d")){
                $status='Upcoming';
            }
            else if(($data->training_starting_date)<=date("Y-m-d")&&($data->training_ending_date)>=date("Y-m-d")){
                $status='Active';
            }
           
            //$sdate=date("d-m-Y",$data->training_starting_date);
            $order++; 
            @endphp
            <tr>
            <td>{{$order}}</td>
            <td>{{$data->training_name}}</td>
            <td>{{date("d M Y",strtotime($data->training_starting_date))}}</td>
            <td>{{date("d M Y",strtotime($data->training_ending_date))}}</td>
            <td>{{$data->employee_name}}</td>
            <td>{{$status}}</td>
            </tr>
        @endforeach
            </tbody>
        </table>
    @endif

</div>

</body>
</html>




