<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Download Job Application</title>
    <style>
        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 10px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 12px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
        .title-header{
            text-align: left;
        }
    </style>
</head>
<body>
    <div>
        <div class="reportHeaderArea">
            <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
            <p class="reportHeader">{{$companyInformation->company_address1}}</p>
            <p class="reportHeader">{{$companyInformation->company_email}}</p>
            <p class="reportHeader">{{$companyInformation->company_phone}}</p>
        </div>
        <p><b>Job Application Letter for {{$v->vacTitle}}</b></p>
        <p>To</p>
        <p>Managing Director,</p>
        <p><b>{{$companyInformation->company_name}}</b></p>
        <p>Date: </p>
        <p>Subject: Applying for the position of <b>{{$v->vacTitle}}</b></p>
        <p>Sir,</p>
        <p>I, <span>.........................................................................,</span> am applying for the position of a {{$v->vacTitle}} through the means of this job application letter in your reputed company and would consider myself fortunate to get an opportunity to work for you. I believe that my exceptional educational qualifications and my skills will help me to perform well at this position.</p>
        <p>Sir, I am enclosing a copy of my curriculum vitae as well as some of the supporting documents which shall help you take a fair decision about the recruitment process. I am not only hardworking but very well organized as well. I can handle several duties at the same time and this makes me highly suitable for the given job position. I hope that you consider me for this position as I am really looking forward to working for you.</p>
        <p>Thanking you</p>
        <p>Yours Sincerely,</p>
        <p>Name:<span>......................................................</span></p>
        <p>Contact Number:<span>......................................................</span></p>
        <p>Address:<span>......................................................</span></p>

    </div>

</body>
</html>