<!DOCTYPE html>
<html>
<head>
    <style>
        body{
            font-size: 12px;
        }
        #bordered {
            width: 100%;
            border: 2px solid black;
            border-collapse: collapse;
        }
        table, h4{
            margin: 0px;

        }
        table, td, th {
            border: 1px solid black;
        }
        .body{
            margin-top: 5px;
            border: 1px solid;
        }
        p{
            margin: 3px;
        }
        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 8px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
        .title-header{
            text-align: left;
        }
    </style>
</head>
<body>
    <div>
    <div style="text-align: center;">
        <div class="reportHeaderArea">
            <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
            <p class="reportHeader">{{$companyInformation->company_address1}}</p>
            <p class="reportHeader">{{$companyInformation->company_email}}</p>
            <p class="reportHeader">{{$companyInformation->company_phone}}</p>
        </div>
        <div class="title-header">
            <h4>Document Code: <span>A2R987</span></h4>
            <h4>TITLE: <span>Recruitment Policy</span></h4>
            <h4>Effected Date:<span></span></h4>
        </div>
    </div>


    <table id='bordered'>
        <tr>
            <th>Prepared by:</th>
            <th>Checked by: </th>
            <th>Approved by:</th>
        </tr>
        <tr>
            <td>Name:</td>
            <td>Name:</td>
            <td>Name:</td>
        </tr>
    </table>

    <div class="body">
        <h4>
            Recruitment and Job Confirmation Procedure:
        </h4>
        <hr style="margin: 3px;">
        <p>1.1 Advertisement in the newspapers, banner in the factory / EPZ gate or any media for recruitment.</p>
        <p>1.2 Short listed candidates shall be interviewed by an interview board comprising HR manager and Department head of concerned department. [Technical test shall be taken where necessary]</p>
        <p>1.3 Applicant should submit an application and bio-data along with 2 pass port size and 2 stamp size photographs, education and experience certificates if any.</p>
        <p>1.4 Selected candidate shall be given a company prescribed format [with questionnaire relating candidates background, past employment etc] which should be filled in and signed by the candidate.</p>
        <p>1.5 The candidates past employers shall be approached by the respective HR Dept and obtain confidential report against the concerned candidate. If the report against the candidate appears to be positive, the concerned candidate shall be appointed in the company.</p>
        <p>1.6 The company shall not engages/employ prison, or forced labor.</p>
        <p>1.7 A copy of appointment letter to be issued to appointed worker.</p>
        <p>1.8 No favoritism on nationality, religion, color for employment.</p>
        <p>1.9 Workers age shall not be below than 18 years. Child labor and young labor shall not be recruited. The selected candidate shall submit a fitness certificate and as well as age certificate from a registered doctor to the HR department.</p>
        <p>1.10 The skills, experience and educational qualification required performing each job to be identified in Job Specification.</p>
        <p>1.11 All new employees must meet at least the minimum level of competency and experience that is required for the job they are hired for.</p>
        <p>1.12 After recruitment every floor level worker will be assessed their performance by the department head or competent supervisor.</p>
        <p>1.13 If the recruitment procedure not completed before 12’o’clock then he or she will be entitled for a complementary lunch package. </p>
        <p>1.14 If the candidate/applicant are not satisfied with the company offered salary and the situation arises as like 1.13 stated above, he or she will be paid one day salary according to the rate of company offered package.</p>
        <p>1.15 Before and of the probation period H.R. Department will initiate an appraisal form to assess the level of performance of a worker and to take decision on his/her job confirmation. If this is not done within such probation period the particular employees his / her service will be treated as confirmed.</p>
        <p>1.16 The probation period can be extended for further period of three months in case the performances not satisfied to the management.</p>

    </div>

</div>

</body>
</html>
