<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee List</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #999;
            text-align: left;
            padding: 6px 5px;
        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }
        .topHead {
            padding: 7px 5px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
    <center>

    @if(!empty($employees))
    <?php

    function array_unique_deep($array, $key)
        {
          $values = array();
          foreach ($array as $k1 => $row)
          {
            foreach ($row as $k2 => $v)
            {
              if ($k2 == $key)
              {
                $values[ $k1 ] = $v;
                continue;
              }
            }
          }
          return array_unique($values);
        }
        $line_list = array_unique_deep($employees,"LineName");
        $floor_list = array_unique_deep($employees,"floorName");
        
    ?>

                 @php $col=3; @endphp

                @if(!empty($request->coldesignation))
                    @php $col++; @endphp
                @endif
                @if(!empty($request->coldepartment))
                    @php $col++; @endphp
                @endif
                @if(!empty($request->coljoiningdate))
                    @php $col++; @endphp
                @endif
                @if(!empty($request->colgender))
                    @php $col++; @endphp
                @endif
                @if(!empty($request->colunit))
                    @php $col++; @endphp
                @endif
                @if(!empty($request->colfloor))
                    @php $col++; @endphp
                @endif
                @if(!empty($request->colline))
                    @php $col++; @endphp
                @endif
                @if(!empty($request->colsection))
                    @php $col++; @endphp
                @endif
                @if(!empty($request->colstatus))
                    @php $col++; @endphp
                @endif

    <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
        <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>Name</th>
                @if(!empty($request->coldesignation))
                <th>Designation</th>
                @endif
                @if(!empty($request->coldepartment))
                <th>Department</th>
                @endif
                @if(!empty($request->coljoiningdate))
                <th>Joining Date</th>
                @endif
                @if(!empty($request->colgender))
                <th>Gender</th>
                @endif
                @if(!empty($request->colunit))
                <th>Unit</th>
                @endif
                @if(!empty($request->colfloor))
                <th>Floor</th>
                @endif
                @if(!empty($request->colline))
                <th>Line</th>
                @endif
                @if(!empty($request->colsection))
                <th>Section</th>
                @endif
                @if(!empty($request->colstatus))
                <th>Status</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @php $i=0 @endphp
            @foreach ($floor_list as $floor)
            <tr><td colspan="{{$col}}" class="topHead"><b>Floor: </b> {{$floor}}</td></tr> 
            @foreach ($line_list as $line)

                @php $l=0; @endphp
                  @foreach($employees as $employee)
                    @if($employee->LineName==$line && $employee->floorName==$floor)
                    @if($l==0)
                    @php $l=1 @endphp
                        <tr><td colspan="{{$col}}" class="topHead"><b>Line: </b> {{$line}}</td></tr>      
                    @endif
                    <tr>
                        <td>{{sprintf('%02d', ++$i)}}</td>
                        <td>{{$employee->employeeId}}</td>
                        <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                        @if(!empty($request->coldesignation))
                        <td>{{$employee->designation}}</td>
                        @endif
                        @if(!empty($request->coldepartment))
                        <td>{{$employee->departmentName}}</td>
                        @endif
                        @if(!empty($request->coljoiningdate))
                        <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                        @endif
                        @if(!empty($request->colgender))
                        <td>
                            @if($employee->empGenderId==1)
                                Male
                            @endif
                            @if($employee->empGenderId==2)
                                Female
                            @endif
                            @if($employee->empGenderId==3)
                                Other
                            @endif
                        </td>
                        @endif
                        @if(!empty($request->colunit))
                            <td>{{$employee->unitName}}</td>
                        @endif
                        @if(!empty($request->colfloor))
                            <td>{{$employee->floorName}}</td>
                        @endif
                        @if(!empty($request->colline))
                        <td>{{$employee->LineName}}</td>
                        @endif
                        @if(!empty($request->colsection))
                        <td>{{$employee->empSection}}</td>
                        @endif
                        @if(!empty($request->colstatus))
                        <td>
                            @if($employee->empAccStatus==1)
                                Active
                            @elseif($employee->empAccStatus==0)
                                Inactive
                            @endif
                        </td>
                        @endif
                    </tr>
                    @endif
                  @endforeach

        @endforeach
        @endforeach
        </tbody>
    </table>
    @else
        <hr>
        <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




