<?php
$b_time=0;
function roundTime($time){
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));

    if($minute<25){
        $minute=0;
    }
    elseif ($minute>=25 && $minute<55){
        $minute=.5;
    }
    else{
        $minute=0;
        $hour++;
    }
    $kind=number_format((float)($hour+$minute),1,'.','');
    return $kind;

}

function late_time($pretime,$time){
    $phour=date('G',strtotime($pretime));
    $pminute=date('i',strtotime($pretime));
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));
    $thour=$phour+$hour;
    $tminute=$pminute+$minute;
    $h=floor($tminute/60);
    $tminute=sprintf("%02d",$tminute%60);
    $thour+=$h;
    $thour=sprintf("%02d",$thour);
    return "$thour:$tminute";

}
$join_less=0;

if($datas[0][0]->date_of_discontinuation!=null){
    if(\Carbon\Carbon::parse($datas[0][0]->date_of_discontinuation)->format('m-Y')==\Carbon\Carbon::parse($end_date)->format('m-Y')){
        $total_days=\Carbon\Carbon::parse($start_date)->diffInDays($datas[0][0]->date_of_discontinuation)+1;
        $l=1;
    }
    else{
        $l=2;
    }

}
else{
    $l=0;
}


if(\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->format('m-Y')==\Carbon\Carbon::parse($end_date)->format('m-Y'))
{
    $total_days=\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->diffInDays($end_date)+1;
    $k=1;

}
else{
    $k=0;
}



$absent=0;
$working_days=0;
$weekends=0;
$present=0;
$late=0;
$onleave=0;
$ot=0;
$late_time="00:00";

$attendance_settings=\Illuminate\Support\Facades\DB::table('attendance_setup')->leftJoin('employees','attendance_setup.id','=','employees.empShiftId')->where('employees.id','=',$emp_id)->first();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Attendance Card</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 90%;
            font-size: 11px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            /*border: 1px solid #ddd;*/
            font-size: 12px;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: center !important;

        }

        #customers th {
            text-align: left;
            padding: 5px;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }

        #cardFooter{
            border-collapse: collapse;
            font-size:11px;
            width:70%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */

        }
        .reportDateRange{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif  !important;
            font-size: 12px !important;
            font-weight: bold;
        }
        #cardFooter td {
            text-align: left !important;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportDateRange">Job Card Report Month From <b>{{\Carbon\Carbon::parse($start_date)->format('d-M-Y')}}</b> to <b>{{\Carbon\Carbon::parse($end_date)->format('d-M-Y')}} </b></p>
    </div>
    @if(isset($datas[0][0]))
        <table id="employeeDetails" >
            <tr>
                <td style="text-align:left" width="35%"><b>Employee ID :</b> {{$datas[0][0]->employeeId}}</td>
                <td style="text-align:left" width="35%"><b>Name : </b> {{$datas[0][0]->empFirstName." ".$datas[0][0]->empLastName }}</td>
                <td style="text-align:left" width="30%"><b>Joining Date :</b> {{\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->format('d-M-Y')}}</td>
            </tr>
            <tr>
                <td style="text-align:left" width="35%"><b>Designation :</b> {{$datas[0][0]->designation}}</td>
                <td style="text-align:left" width="35%"><b>Department : </b> {{$datas[0][0]->departmentName}}</td>
                <td style="text-align:left" width="30%"><b> Section : </b> {{$datas[0][0]->empSection}}</td>
            </tr>

        </table>

        <table id='customers' style="margin-top:15px;font-size:11px;" border="1px">
            <thead>
            <tr>
                <th>Date</th>
                <th>Day</th>
                <th>In Time </th>
                <th>Out Time</th>
                <th>OT</th>
                <th>Late Time</th>

                <th>Status</th>
                <th>Remarks</th>
            </tr>
            </thead>
            <tbody>
            @foreach($datas as $d)
                <?php
                if($k==1){
                    if(\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->subDay()->gte(\Carbon\Carbon::parse($d[1]))){
                        $j=1;
                        continue;
                    }
                    else{
                        $j=0;
                    }

                }
                else{
                    $j=0;
                }
                if($l==1){
                    if(\Carbon\Carbon::parse(\Carbon\Carbon::parse($d[1]))->subDay()->gte($datas[0][0]->date_of_discontinuation)){
                        $m=1;
                        continue;
                    }
                    else{
                        $m=0;
                    }

                }
                else{
                    $m=0;
                }

                $working_days++;
                $leave_check= explode('@',$d[2]);
                $holidayPurpose= explode('@',$d[2]);
                $regular_day=0;
                ?>
                <tr>
                    <td>{{\Carbon\Carbon::parse($d[1])->format('d-M-Y')}}</td>
                    <td>{{\Carbon\Carbon::parse($d[1])->format('l')}}</td>
                    <td>
                        @if(isset($d[0]->in_time))
                            <?php
                            $present++;
                            ?>
                            {{ date('G:i', strtotime($d[0]->in_time)) }}
                        @endif
                    </td>

                    <td>
                        @if(isset($d[0]->out_time))
                            @if($d[0]->in_time==$d[0]->out_time)
                                <span style="color: #FF0000;">Not Given</span>
                            @else
                                {{ date('G:i', strtotime($d[0]->out_time)) }}
                            @endif
                        @endif
                    </td>


                    <td>
                        @if(isset($d[0]->out_time) && $d[0]->out_time!=$d[0]->in_time)
                                @php
                                $rama_dan=DB::table('attendance_setup_ramadan')
                                ->where('shift_id',$attendance_settings->id)
                                ->where('ramadan_date',$d[0]->date)
                                ->first(); 
                                @endphp
                               @if(isset($rama_dan->ramadan_date)==date('Y-m-d', strtotime($d[0]->date)))
                                @if($d[0]->out_time>$attendance_settings->exit_time_ramadan)
                                @if(\Carbon\Carbon::parse($d[1])->format('l')=="Friday")
                                    <?php
                                    $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$d[1])->count();
                                    ?>
                                    @if(!$regular_day)
                                        <?php
                                        $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan)))+8
                                        ?>
                                        {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan)))+8 }}
                                    @else
                                        <?php
                                        $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan)))
                                        ?>
                                        {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan))) }}
                                    @endif
                                @else
                                    <?php
                                    $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan)))
                                    ?>
                                    {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time_ramadan))) }}
                                @endif
                               @else
                                @if(\Carbon\Carbon::parse($d[1])->format('l')=="Friday")
                                    <?php
                                    $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$d[1])->count();
                                    ?>
                                    @if(!$regular_day)
                                        <?php
                                        $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($d[0]->in_time)))-1
                                        ?>
                                        {{roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($d[0]->in_time)))-1}}
                                    @else
                                        0.0
                                    @endif
                                @else
                                    0.0
                                @endif
                            @endif
                            <!--- Ramadan end -->
                           @else 
                         <!--- general start -->
                            @if($d[0]->out_time>$attendance_settings->exit_time)
                            @if(\Carbon\Carbon::parse($d[1])->format('l')=="Friday")
                                <?php
                                $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$d[1])->count();
                                ?>
                                @if(!$regular_day)
                                    <?php
                                    $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))+8
                                    ?>
                                    {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))+8 }}
                                @else
                                    <?php
                                    $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))
                                    ?>
                                    {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))}}
                                @endif
                            @else
                                <?php
                                $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))
                                ?>
                                {{ roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time))) }}
                            @endif
                           @else
                            @if(\Carbon\Carbon::parse($d[1])->format('l')=="Friday")
                                <?php
                                $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$d[1])->count();
                                ?>
                                @if(!$regular_day)
                                    <?php
                                    $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($d[0]->in_time)))-1
                                    ?>
                                    {{roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($d[0]->in_time)))-1}}
                                @else
                                    0.0
                                @endif
                            @else
                                0.0
                            @endif
                        @endif
                       <!--- general end -->

                        @endif
                        @endif
                        <!-- general overtime condition end -->
                    </td>

                    <td>
                        @if(isset($d[0]->in_time))

                        @php
                        $rama_dan=DB::table('attendance_setup_ramadan')
                        ->where('shift_id',$attendance_settings->id)
                        ->where('ramadan_date',$d[0]->date)
                        ->first(); 
                        @endphp
                      <!-- ramadan late condition start -->
                      @if(isset($rama_dan->ramadan_date)==date('Y-m-d', strtotime($d[0]->date)))
                            @if($d[0]->in_time>$attendance_settings->max_entry_time_ramadan)
                                <?php
                                $late_time=late_time($late_time,date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time_ramadan)));

                                ?>
                                {{ date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time_ramadan)) }}
                            @else
                                00:00
                            @endif
                            @else 
                      <!-- ramadan late condition end -->
                      <!-- general late condition end -->
                      @if($d[0]->in_time>$attendance_settings->max_entry_time)
                      <?php
                      $late_time=late_time($late_time,date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time)));
                      ?>
                      {{ date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time)) }}
                        @else
                            00:00
                        @endif
                      <!-- general late condition end -->
                          @endif
                        @endif
                    </td>

                    <td>
                        @if(isset($d[0]->in_time))
                            @if($d[0]->in_time>$attendance_settings->max_entry_time)
                                <?php $late++; ?>
                                <span>Late</span>
                            @else
                                <span>Present</span>
                            @endif
                        @else
                            @if($holidayPurpose[0]=='Holiday' || $leave_check[0]=='On Leave')
                                @if($leave_check[0]!='On Leave')
                                    <?php $working_days--; ?>
                                @else
                                    <?php $onleave++; ?>
                                @endif
                                @if($leave_check[0]=='On Leave')
                                    @if(isset($leave_check[1]))
                                        <span>{{$leave_check[1]}}</span>
                                    @endif
                                @elseif($holidayPurpose[0]=="Holiday")
                                    @if(isset($holidayPurpose[1]))
                                        <span>{{$holidayPurpose[1]}}</span>
                                    @endif

                                @else
                                    <span>{{$d[2]}}</span>
                                @endif
                            @else
                                @if(\Carbon\Carbon::parse($d[1])->format('l')=="Friday")
                                    <?php
                                    $regular_day=\Illuminate\Support\Facades\DB::table('tbweekend_regular_day')->where('weekend_date','=',$d[1])->count();
                                    ?>
                                    @if($regular_day && $l!=2)
                                        <?php
                                        $absent++;
                                        ?>
                                        <span style="color:#FF0000">Absent</span>
                                    @else
                                        <?php
                                        $weekends++;
                                        ?>
                                        <span>Weekend</span>
                                    @endif

                                @else
                                    @if($l!=2)
                                        <?php
                                        $absent++;
                                        ?>

                                        <span style="color:#FF0000">Absent</span>
                                    @endif

                                @endif

                            @endif

                        @endif
                    </td>
                    <th></th>

                </tr>
            @endforeach
            <tr style="font-weight: bold">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$ot}} hour</td>
                <td>{{$late_time}}</td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
</div>
<div>
    <center>
        <table id="cardFooter">
            <tr>

                <td width="50%"><b>Total Days (In Month): </b>{{ $total_days }} Days</td>
            </tr>

            <tr>
                {{--$working_days--}}
                <td width="50%"><b>Holiday: </b>{{$total_holiday}} Days</td>
            </tr>
            <tr>
                {{--$working_days--}}
                <td width="50%"><b>Weekends: </b>{{$weekends}} Days</td>
            </tr>
            <tr>
                <td  width="50%"><b>Total Present Days: </b> {{ $present }} Days</td>
            </tr>
            <tr>
                <td class="padTop10"><b>Total Leave :</b> {{ ($onleave) }} Days</td></tr>
            <tr>
                <td class="padTop10"><b>Total Absent :</b> {{ $absent }} Days</td>
            </tr>

        </table>
    </center>
</div>

@else
    <center><h4 style="color: #FF0000">No data found for the given date</h4></center>

@endif



</body>
</html>




