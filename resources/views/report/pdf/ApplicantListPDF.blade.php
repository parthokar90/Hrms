<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Applicant List</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h1>Applicant List</h1>
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
    <center>

    @if(!empty($applicantListShow))
    <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
        <thead>
            <tr>
                <th>SL</th>
                <th>Name</th>
                <th>Contact</th>
                <th>Position</th>
                @if(!empty($request->vacStatusCheck))
                <th>Vacancy Status</th>
                @endif
                @if(!empty($request->applicationDateCheck))
                <th>Application Date</th>
                @endif
                @if(!empty($request->priorityCheck))
                <th>Priority</th>
                @endif
                @if(!empty($request->eligibilityCheck))
                <th>Eligible For Interview</th>
                @endif
                @if(!empty($request->interviewDateCheck))
                <th>Interview Date</th>
                @endif
                @if(!empty($request->jobStatusCheck))
                <th>Job Status</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @php $i=0 @endphp
            @foreach($applicantListShow as $employee)
            <tr>
                <td>{{sprintf('%02d', ++$i)}}</td>
                <td>{{$employee->name}}</td>
                <td>{{$employee->phone}}</td>
                <td>{{$employee->vacTitle}}</td>
                @if(!empty($request->vacStatusCheck))
                <td>
                    @if($employee->vacStatus==0)
                        Inactive
                    @endif
                    @if($employee->vacStatus==1)
                        Active
                    @endif
                </td>
                @endif
                @if(!empty($request->applicationDateCheck))
                <td>{{date("d M Y",strtotime($employee->submittedDate))}}</td>
                @endif
                @if(!empty($request->priorityCheck))
                <td>{{$employee->priorityStatus}}</td>
                @endif
                @if(!empty($request->eligibilityCheck))
                <td>{{$employee->interviewStatus}}</td>
                @endif
                
                @if(!empty($request->interviewDateCheck))
                    <td>{{date("d M Y",strtotime($employee->interviewDate))}}</td>
                @endif
                @if(!empty($request->jobStatusCheck))
                    <td>{{$employee->jobStatus}}</td>
                @endif
                
            </tr>
            @endforeach
          </tbody> 
    </table>
    @else
        <hr>
        <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




