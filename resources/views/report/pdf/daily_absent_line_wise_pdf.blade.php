<?php
$total=0;
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Absent Report</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }
        table td p{
            margin: 0px;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
        <p class="reportDateRange">Absent report for <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y (l)')}} </b></p>

    </div>

        @foreach($lines as $line)
            <?php
            $query1=$query;
            $query1.=" AND tblines.id='".$line->id."' GROUP BY employees.id ORDER BY attendance.date DESC";

            $absent=DB::select($query1);
            $countAbsent=count($absent);
            $total+=$countAbsent;


            ?>
            @if($countAbsent!=0)
                <h4><b>Section/Line: {{$line->line_no}}</b></h4>
                <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
                    <thead>
                    <tr>
                        <th>Emp. ID</th>
                        <th>Proxy Code</th>
                        <th>Name</th>

                        @if(!empty($request->coldesignation))
                            <th>Designation</th>
                        @endif
                        @if(!empty($request->coldepartment))
                            <th>Department</th>
                        @endif
                        @if(!empty($request->coljoiningdate))
                            <th>Joining Date</th>
                        @endif
                        @if(!empty($request->colgender))
                            <th>Gender</th>
                        @endif
                        @if(!empty($request->colunit))
                            <th>Unit</th>
                        @endif
                        @if(!empty($request->colfloor))
                            <th>Floor</th>
                        @endif
                        @if(!empty($request->colline))
                            <th>Line</th>
                        @endif
                        @if(!empty($request->colsection))
                            <th>Section</th>
                        @endif
                        @if(!empty($request->colstatus))
                            <th>Status</th>
                        @endif
                        @if(!empty($request->colcontinueabsent))
                            <th>Last Attend Date</th>
                            <th>Continue Absent</th>
                        @endif

                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $absent as $key=> $employee)
                        <tr>
                            <td>
                                <b>{{$employee->employeeId}}</b>
                            </td>
                            <td>{{$employee->empCardNumber}}</td>
                            <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>

                            @if(!empty($request->coldesignation))
                                <td>{{$employee->designation}}</td>
                            @endif
                            @if(!empty($request->coldepartment))
                                <td>{{$employee->departmentName}}</td>
                            @endif
                            @if(!empty($request->coljoiningdate))
                                <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                            @endif
                            @if(!empty($request->colgender))
                                <td>
                                    @if($employee->empGenderId==1)
                                        Male
                                    @endif
                                    @if($employee->empGenderId==2)
                                        Female
                                    @endif
                                    @if($employee->empGenderId==3)
                                        Other
                                    @endif
                                </td>
                            @endif
                            @if(!empty($request->colunit))
                                <td>{{$employee->unitName}}</td>
                            @endif
                            @if(!empty($request->colfloor))
                                <td>{{$employee->floorName}}</td>
                            @endif
                            @if(!empty($request->colline))
                                <td>{{$employee->LineName}}</td>
                            @endif
                            @if(!empty($request->colsection))
                                <td>{{$employee->empSection}}</td>
                            @endif
                            @if(!empty($request->colstatus))
                                <td>
                                    @if($employee->empAccStatus==1)
                                        Active
                                    @elseif($employee->empAccStatus==0)
                                        Inactive
                                    @endif
                                </td>
                            @endif
                            @if(!empty($request->colcontinueabsent))
                                <?php
                                $start_date=\Carbon\Carbon::parse($employee->date);
                                if(\Carbon\Carbon::parse($request->date)>$start_date){
                                    $diff=\Carbon\Carbon::parse($request->date)->diffInDays($start_date);
                                }
                                else{
                                    $diff=0;
                                }
                                ?>
                                <td>{{\Carbon\Carbon::parse($employee->date)->format('d M Y')}}</td>
                                <td> {{ $diff }}</td>
                            @endif

                            <td><span class="red-text">Absent</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif

        @endforeach


        <hr>
            <div>
                <b>Total</b>:{{$total}}
            </div>

</div>

</body>
</html>


<script type="text/javascript">
    window.print();

</script>





