<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>মাতৃত্বকালীন ছুটির রিপোর্ট</title>
    <style>
        @page { sheet-size: A4-L; }
        body{
            font-family: 'bangla', sans-serif;
            font-size: 12px;

        }
        p{  
            line-height: 1px;
        }

        #employeeDetails{
            font-family: "kalpurush", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 90%;
            font-size: 11px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            /*border: 1px solid #ddd;*/
            font-size: 12px;

        }
        #customers {
           
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: center !important;

        }

        #customers th {
            text-align: left;
            padding: 5px;

        }

        table td {
            font-size: 12px;
            padding: 2px;
            margin: 0;
        }
        table th {
            font-size: 12px;
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }

        #cardFooter{
            border-collapse: collapse;
            font-size:11px;
            width:70%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */

        }
        .reportDateRange{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif  !important;
            font-size: 12px !important; 
            font-weight: bold;
        }
        #cardFooter td {
            text-align: left !important;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
    <div style="text-align: center">
        <h2><strong>মাতৃত্বকালীন ছুটির রিপোর্ট</strong></h2>  
    </div>
   
</div>

<div>
    @if(!empty($maternityLeave))
    <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
            <thead>
            <tr>
                <th>আইডি</th>
                <th>নাম</th>
                @if(!empty($request->coldesignation))
                <th>পদবী</th>
                @endif
                @if(!empty($request->coldepartment))
                <th>বিভাগ</th>
                @endif
                @if(!empty($request->coljoiningdate))
                <th>যোগদানের তারিখ</th>
                @endif
                @if(!empty($request->colunit))
                <th>ইউনিট</th>
                @endif
                @if(!empty($request->colfloor))
                <th>ফ্লোর</th>
                @endif
            
                @if(!empty($request->colsection))
                <th>সেকশন</th>
                @endif
                <th>ছুটির সময়কাল</th>
                @if(!empty($request->colprev3monthSal))
                <th>বিগত ৩ মাসের মোট বেতন</th>
                @endif
                @if(!empty($request->colprev3monthDay))
                <th>বিগত ৩ মাসের মোট কার্যদিবস</th>
                @endif
                <th>মোট প্রদেয় টাকা</th>
                <th>কিস্তি প্রতি টাকা</th>
                <th>প্রথম কিস্তি প্রদানের তারিখ</th>
                <th>দ্বিতীয় কিস্তি প্রদানের তারিখ</th>
                <th>বিবৃতি</th>
                <th>অনুমোদোন করেছেন</th>
                
            </tr>
            </thead>
            <tbody>
                    @foreach($maternityLeave as $employee)
                    @php 
                    if(($employee->other3)=="00"){
                        $status="প্রদান করা হয়নি";
                    }
                    elseif(($employee->other3)=="10"){
                        $status="প্রথম কিস্তি প্রদান হয়েছে";
                    }
                    elseif(($employee->other3)=="11"){
                        $status="সম্পূর্ণ প্রদান হয়েছে";
                    }
                    @endphp
                    <tr>
                        <td>{{$employee->employeeId}}</td>
                        <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                        @if(!empty($request->coldesignation))
                        <td>{{$employee->designation}}</td>
                        @endif
                        @if(!empty($request->coldepartment))
                        <td>{{$employee->departmentName}}</td>
                        @endif
                        @if(!empty($request->coljoiningdate))
                        <td>{{en2bnNumber(date("d M Y",strtotime($employee->empJoiningDate)))}}</td>
                        @endif
                    
                        @if(!empty($request->colunit))
                            <td>{{$employee->unitName}}</td>
                        @endif
                        @if(!empty($request->colfloor))
                            <td>{{$employee->floorName}}</td>
                        @endif
                    
                        @if(!empty($request->colsection))
                        <td>{{$employee->empSection}}</td>
                        @endif
                        <td>{{en2bnNumber(date("d M",strtotime($employee->leave_starting_date)))}} থেকে {{en2bnNumber(date("d M Y",strtotime($employee->leave_ending_date)))}}</td>
                        @if(!empty($request->colprev3monthSal))
                        <td>{{en2bnNumber(number_format(($employee->previous_three_month_salary)))}} টাকা</td>
                        @endif
                        @if(!empty($request->colprev3monthDay))
                        <td>{{en2bnNumber($employee->total_working_days)}} দিন</td>
                        @endif
                        <td>{{en2bnNumber(number_format(($employee->total_payable_amount)))}} টাকা</td>
                        <td>{{en2bnNumber(number_format(($employee->per_instalment)))}} টাকা</td>
                        
                        <td>
                            @if(!empty($employee->first_installment_pay))
                            {{en2bnNumber(date("d M Y",strtotime($employee->first_installment_pay)))}}
                            @else
                            <h3>N/A</h3>
                            @endif
                        </td>
                        <td>
                            @if(!empty($employee->second_installment_pay))
                            {{en2bnNumber(date("d M Y",strtotime($employee->second_installment_pay)))}}
                            @else
                            <h3>N/A</h3>
                            @endif
                        </td>
                        <td>{{$status}}</td>
                        <td>
                            @if(!empty($employee->approved_by))
                            {{$employee->approved_by}}
                            @else
                            <h3>N/A</h3>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>

    </table>


        @else
        <hr>
        <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif



</div>


</body>
</html>




