<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Expense Report</title>
    <style>

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;
            font-size: 9px;

        }

        table td {
            padding: 2px;
            margin: 0;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h1 class="reportHeaderCompany">{{$companyInformation->company_name}}</h1>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
    <center>
        <div class="col-md-6">
            <h5><strong>Expense Report</strong> From {{\Carbon\Carbon::parse($request->start_date)->format('d-m-Y')}} to {{\Carbon\Carbon::parse($request->end_date)->format('d-m-Y')}}</h5>
        </div>

        @if(count($data)!=0)
            <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
                <thead>
                <tr>
                    <th>Order</th>
                    <th>Title</th>
                    <th>Amount</th>
                    <th>Reference</th>
                    <th>Date</th>
                    <th>Category</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @php $order=0; @endphp
                @foreach($data as $item)
                    <?php
                        $total_expense=0;
                        $total_expense+=$item->amount;
                    ?>
                    <tr>
                        <td align="center">{{++$order}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->amount}} </td>
                        <td>{{$item->reference}}</td>
                        <td>{{\Carbon\Carbon::parse($item->expenseDate)->format('d M Y')}}</td>
                        <td>{{$item->categoryName}}</td>
                        <td>{{$item->description}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
                <p style="font-size: 11px; font-weight: bold">Total Expense {{$total_expense}} Taka Only.</p>
            </div>
        @else
            <hr>
            <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




