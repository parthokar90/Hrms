<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Maternity Leave Report</title>
    <style>
            #employeeDetails{
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 50%;
                text-align: center;
                border:1px;
                font-size: 12px;
                margin:0px auto;
                margin-top: 15px;
    
            }
    
            #employeeDetails td, #employeeDetails th {
                border: 1px solid #ddd;
                text-align: center !important;
    
            }
            #customers {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
                text-align: center;
            }
    
            #customers td, #customers th {
                border: 1px solid #ddd;
                text-align: left;
    
            }
    
            #customers th {
                text-align: left;
                padding: 5px;
                background:#eee;
                font-size: 9px;
    
            }
    
            table td {
                padding: 2px;
                margin: 0;
            }
    
            .reportHeaderArea{
                text-align: center;
            }
    
            .reportHeader{
                line-height: 4px;
            }
    
            .reportHeader{
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                font-size: 10px;
            }
    
            .reportHeaderCompany{
              font-size: 18px !important;
              
            }
        </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h1 class="reportHeaderCompany">{{$companyInformation->company_name}}</h1>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
    </div>
        <center>
            <div class="col-md-6">
                <h4><strong>Maternity Leave Report</strong></h4>  
            </div>
        </center>
</div>
<div>
    @if(!empty($maternityLeave))
    <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
        
            <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        @if(!empty($request->coldesignation))
                        <th>Designation</th>
                        @endif
                        @if(!empty($request->coldepartment))
                        <th>Department</th>
                        @endif
                        @if(!empty($request->coljoiningdate))
                        <th>Joining Date</th>
                        @endif
                        @if(!empty($request->colunit))
                        <th>Unit</th>
                        @endif
                        @if(!empty($request->colfloor))
                        <th>Floor</th>
                        @endif
                    
                        @if(!empty($request->colsection))
                        <th>Section</th>
                        @endif
                        <th>Leave Period</th>
                        @if(!empty($request->colprev3monthSal))
                        <th>Previous 3 Month Salary</th>
                        @endif
                        @if(!empty($request->colprev3monthDay))
                        <th>Previous 3 Month Working Days</th>
                        @endif
                        <th>Total Payable amount</th>
                        <th>Per Installment</th>
                        <th>First Installment Date</th>
                        <th>Second Installment date</th>
                        <th>Status</th>
                        <th>Approved By</th>
                        
                    
                    
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($maternityLeave as $employee)
                    @php 
                    if(($employee->other3)=="00"){
                        $status="Not Paid";
                    }
                    elseif(($employee->other3)=="10"){
                        $status="Half Paid";
                    }
                    elseif(($employee->other3)=="11"){
                        $status="Full Paid";
                    }
                    @endphp
                    <tr>
                        <td>{{$employee->employeeId}}</td>
                        <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                        @if(!empty($request->coldesignation))
                        <td>{{$employee->designation}}</td>
                        @endif
                        @if(!empty($request->coldepartment))
                        <td>{{$employee->departmentName}}</td>
                        @endif
                        @if(!empty($request->coljoiningdate))
                        <td>{{date("d-M-Y",strtotime($employee->empJoiningDate))}}</td>
                        @endif
                    
                        @if(!empty($request->colunit))
                            <td>{{$employee->unitName}}</td>
                        @endif
                        @if(!empty($request->colfloor))
                            <td>{{$employee->floorName}}</td>
                        @endif
                    
                        @if(!empty($request->colsection))
                        <td>{{$employee->empSection}}</td>
                        @endif
                        <td>{{date("d-M",strtotime($employee->leave_starting_date))}} {{date("d-M-Y",strtotime($employee->leave_ending_date))}}</td>
                        @if(!empty($request->colprev3monthSal))
                        <td>{{number_format(($employee->previous_three_month_salary))}} BDT</td>
                        @endif
                        @if(!empty($request->colprev3monthDay))
                        <td>{{$employee->total_working_days}}</td>
                        @endif
                        <td>{{number_format(($employee->total_payable_amount))}} BDT</td>
                        <td>{{number_format(($employee->per_instalment))}} BDT</td>
                        
                        <td>
                            @if(!empty($employee->first_installment_pay))
                            {{date("d-M-Y",strtotime($employee->first_installment_pay))}}
                            @else
                            <h3>N/A</h3>
                            @endif
                        </td>
                        <td>
                            @if(!empty($employee->second_installment_pay))
                            {{date("d-M-Y",strtotime($employee->second_installment_pay))}}
                            @else
                            <h3>N/A</h3>
                            @endif
                        </td>
                        <td>{{$status}}</td>
                        <td>
                            @if(!empty($employee->approved_by))
                            {{$employee->approved_by}}
                            @else
                            <h3>N/A</h3>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
    </table>
    @else
        <hr>
        <h4 style="color:red;"><center> No Matched data found.</center></h4>
    @endif

</div>

</body>
</html>




