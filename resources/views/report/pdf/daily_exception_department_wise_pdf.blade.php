<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Attendance Exception</title>
    <style>
        #employeeDetails{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            text-align: center;
            border:1px;
            font-size: 12px;
            margin:0px auto;
            margin-top: 15px;

        }

        #employeeDetails td, #employeeDetails th {
            border: 1px solid #ddd;
            text-align: center !important;

        }
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 2px;
            margin: 0;
        }
        table td p{
            margin: 0px;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
        <p class="reportDateRange">Attendance Exception for <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y (l)')}} </b></p>

    </div>



    <center>

        @foreach($departments as $line)
            <?php
            $query1=$query;
            $query1.=" AND departments.id='".$line->id."'";
            $absent=DB::select($query1);
            $countAbsent=count($absent);

            ?>
            @if($countAbsent!=0)
                <h4><b>Department: {{$line->departmentName}}</b></h4>
                <table id="customers" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Emp. ID</th>
                        <th>Name</th>
                        @if(!empty($request->coldesignation))
                            <th>Designation</th>
                        @endif
                        @if(!empty($request->coldepartment))
                            <th>Department</th>
                        @endif
                        @if(!empty($request->coljoiningdate))
                            <th>Joining Date</th>
                        @endif
                        @if(!empty($request->colgender))
                            <th>Gender</th>
                        @endif
                        @if(!empty($request->colunit))
                            <th>Unit</th>
                        @endif
                        @if(!empty($request->colfloor))
                            <th>Floor</th>
                        @endif
                        @if(!empty($request->colline))
                            <th>Line</th>
                        @endif
                        @if(!empty($request->colsection))
                            <th>Section</th>
                        @endif
                        @if(!empty($request->colstatus))
                            <th>Status</th>
                        @endif
                        @if(!empty($request->colcontinueabsent))
                            <th>Continue Absent</th>
                        @endif
                        <th>In Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $absent as $key=> $employee)
                        <?php
                        if(!empty($request->colcontinueabsent)){
                            $last_date=\Illuminate\Support\Facades\DB::select("SELECT MAX(date) as last_attend_date, emp_id FROM `attendance` WHERE emp_id=$employee->id GROUP BY emp_id");
                            if(isset($last_date[0])){
                                $last_attend_date=$last_date[0]->last_attend_date;
                                $start=\Carbon\Carbon::parse($request->date)->toDateString();
                                $now = strtotime(\Carbon\Carbon::parse($request->date));
                                $your_date = strtotime($last_attend_date);
                                $datediff = $now - $your_date;
                                $days= round($datediff / (60 * 60 * 24));
                                if($days<=0)
                                {
                                    $days=0;
                                }
                                $employee->absent_count=$days;
                            }
                            else{
                                $employee->absent_count=0;
                            }
                        }

                        ?>
                        <tr>
                            <td>
                                <b>{{$employee->employeeId}}</b>
                            </td>
                            <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                            @if(!empty($request->coldesignation))
                                <td>{{$employee->designation}}</td>
                            @endif
                            @if(!empty($request->coldepartment))
                                <td>{{$employee->departmentName}}</td>
                            @endif
                            @if(!empty($request->coljoiningdate))
                                <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</td>
                            @endif
                            @if(!empty($request->colgender))
                                <td>
                                    @if($employee->empGenderId==1)
                                        Male
                                    @endif
                                    @if($employee->empGenderId==2)
                                        Female
                                    @endif
                                    @if($employee->empGenderId==3)
                                        Other
                                    @endif
                                </td>
                            @endif
                            @if(!empty($request->colunit))
                                <td>{{$employee->unitName}}</td>
                            @endif
                            @if(!empty($request->colfloor))
                                <td>{{$employee->floorName}}</td>
                            @endif
                            @if(!empty($request->colline))
                                <td>{{$employee->LineName}}</td>
                            @endif
                            @if(!empty($request->colsection))
                                <td>{{$employee->empSection}}</td>
                            @endif
                            @if(!empty($request->colstatus))
                                <td>
                                    @if($employee->empAccStatus==1)
                                        Active
                                    @elseif($employee->empAccStatus==0)
                                        Inactive
                                    @endif
                                </td>
                            @endif
                            @if(!empty($request->colcontinueabsent))
                                <th> {{ $employee->absent_count }}</th>
                            @endif
                            <td><span class="red-text">{{$employee->in_time}}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                <b>Total: {{$countAbsent}}</b>
            @endif

        @endforeach


        <hr>

</div>

</body>
</html>

<script type="text/javascript">
    window.print();
</script>




