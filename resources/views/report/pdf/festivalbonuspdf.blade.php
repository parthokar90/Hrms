<!DOCTYPE html>
<html>
<head>
    <title>Festival Bonus</title>
    <style>
        table, th, td {
            border: 1px solid black;
            font-size: 12px;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<h4 style="text-align: center;font-weight: bold;">{{$companyInformation->company_name}}</h4>
<h5 style="text-align: center;font-weight: bold;">Festival bonus for the month of</h5>
<p style="text-align: center">
    @foreach($month_name as $month)
        {{date('F-d-Y',strtotime($month->created_at))}}
    @endforeach
</p>
<table style="width:100%">
    <tr>
        <th>Order</th>
        <th>Employee</th>
        <th>Gross</th>
        <th>Gross after bonus</th>
        <th>Percent wise amount</th>
        <th>Amount</th>
        <th>Percent</th>
        <th>Title</th>
        <th>Given by</th>
        <th>Role</th>
        <th>Email</th>
    </tr>
    @php
        $order=0;
    @endphp
    @foreach($data as $increment)
        @php
            $order++
        @endphp
        <tr>
            <td>{{$order}}</td>
            <td>{{$increment->empFirstName}} {{$increment->empLastName}}</td>
            <td>{{$increment->emp_gross}} Taka</td>
            <td>{{$increment->emp_gross+$increment->emp_bonus+$increment->emp_amount}} Taka</td>
            <td>{{$increment->emp_bonus}} Taka</td>
            <td>
                @if($increment->emp_amount=='')
                    0 Taka
                @else
                    {{$increment->emp_amount}} Taka
                @endif
            </td>
            <td>
                @if($increment->emp_total_percent=='')
                    0%
                @else
                    {{$increment->emp_total_percent}}%
                @endif
            </td>
            <td>{{$increment->bonus_title}}</td>
            <td>{{$increment->name}}</td>
            <td>
                @if($increment->is_permission==1)
                    Admin
                @endif
                @if($increment->is_permission==2)
                    Hr
                @endif
                @if($increment->is_permission==4)
                    Executive
                @endif
                @if($increment->is_permission==5)
                    Accountant
                @endif
            </td>
            <td>{{$increment->email}}</td>
        </tr>
    @endforeach
</table>
</body>
</html>