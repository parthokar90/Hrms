<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Attendance Summary</title>
    <style>
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            text-align: center;

        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: left;

        }

        #customers th {
            text-align: left;
            padding: 5px;
            background:#eee;

        }

        table td {
            padding: 5px;
            margin: 0;
        }
        table td p{
            margin: 0px;
        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }
    </style>
</head>
<body>

<div>
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">{{$companyInformation->company_email}}</p>
        <p class="reportHeader">{{$companyInformation->company_phone}}</p>
        <p class="reportDateRange">Attendance summary for <b>{{\Carbon\Carbon::parse($request->date)->format('d M Y (l)')}} </b></p>
    </div>

    <center>
        @if(!empty($data))
            <table id='customers' style="margin-top:10px;font-size:10px;" border="0px solid">
                <thead>
                    <tr>
                        <th>Floor</th>
                        <th>Line</th>
                        <th>Total</th>
                        <th>Present</th>
                        <th>Absent</th>
                        <th>Late</th>
                        <th>Leave</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($floors as $key=>$c)
                            @php
                                $row=\Illuminate\Support\Facades\DB::table('employees')->where('floor_id','=',$c->floor_id)->groupBy('line_id')->get();
                                $row=count($row);
                                $check=\Illuminate\Support\Facades\DB::select("SELECT * FROM employees WHERE empAccStatus=1 AND employees.floor_id=$c->floor_id AND employees.line_id is null");
                            @endphp
                            <tr>
                                <td style="padding: 0px;" rowspan="{{$row+1}}"><p>{{\Illuminate\Support\Facades\DB::table('floors')->where('id','=',$c->floor_id)->select('floor')->first()->floor}}</p></td>
                            </tr>
                            @if(!count($check))
                                @foreach($data as $key=>$d)
                                    @if($d['designationId']==$c->floor_id)
                                        <tr>
                                            <td>{{$d['designationName']}}</td>
                                            <td>{{$d['total']}}</td>
                                            <td>{{ $d['present']  }}</td>
                                            <td>{{ $d['total']-($d['present']+$d['leave']) }}</td>
                                            <td>{{ $d['late'] }}</td>
                                            <td>{{ $d['leave'] }}</td>
                                        </tr>
                                    @endif

                                @endforeach
                            @else
                                @php
                                    $date = \Carbon\Carbon::parse($request->date)->toDateString();
                                    $ld=\Carbon\Carbon::parse($request->date)->format('m/d/Y');
                                    $late_time=\App\Http\Controllers\AttendanceController::late_time();
                                    $total = DB::table('employees')->where('empAccStatus', '=', '1')->where('floor_id', '=', $c->floor_id)->count();
                                    $present = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('floor_id', '=', $c->floor_id)->where(['attendance.date' => $date])->count();
                                    $late = DB::table('employees')->join('attendance', 'employees.id', '=', 'attendance.emp_id')->where('employees.empAccStatus', '=', '1')->where('floor_id', '=', $c->floor_id)->where(['attendance.date' => $date])->where('attendance.in_time', '>', $late_time)->count();
                                    $leaveCount = DB::select("SELECT COUNT(*) as leaveCount FROM employees where id in (select employee_id from tb_leave_application WHERE '$ld'>=leave_starting_date and '$ld'<=leave_ending_date and status='1') and floor_id=" . $c->floor_id);
                                    $leaveCount = $leaveCount[0]->leaveCount;
                                @endphp
                                <tr>
                                    <td>None</td>
                                    <td>{{$total}}</td>
                                    <td>{{ $present }}</td>
                                    <td>{{ $total-($present+$leaveCount) }}</td>
                                    <td>{{ $late }}</td>
                                    <td>{{ $leaveCount }}</td>
                                </tr>
                            @endif
                        {{--@endif--}}
                    @endforeach
                </tbody>
            </table>

            <div style="float:left;padding-top: 15px; font-size: 11px; text-align: left;">
                <span><b>Total Employee: </b> {{$totalE}}</span><br>
                <span><b>Total Present :</b> {{$totalP}}</span><br>
                <span><b>Total Absent Employee :</b> {{ $totalE-($totalL+$totalP) }}</span><br>
                <span><b>Total Late Employee :</b> {{ $Tlate }}</span><br>
                <span><b>Total Employee On Leave :</b> {{ $totalL }}</span><br>

            </div>

        @else
            <hr>
            <h4 style="color:red;"><center> No Absent data found.</center></h4>
    @endif

</div>

</body>
</html>




