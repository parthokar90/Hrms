<?php
function roundTime($time){
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));

    if($minute<25){
        $minute=0;
    }
    elseif ($minute>=25 &&$minute<55){
        $minute=.5;
    }
    else{
        $minute=0;
        $hour++;
    }
    $kind=number_format((float)($hour+$minute),1,'.','');
    return $kind;

}

function late_time($pretime,$time){
    $phour=date('G',strtotime($pretime));
    $pminute=date('i',strtotime($pretime));
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));
    $thour=$phour+$hour;
    $tminute=$pminute+$minute;
    $h=floor($tminute/60);
    $tminute=sprintf("%02d",$tminute%60);
    $thour+=$h;
    $thour=sprintf("%02d",$thour);
    return "$thour:$tminute";

}

function end_time($time){
    $hour=date('G',strtotime($time));
    $minute=date('i',strtotime($time));
    $minute=sprintf("%02d",$minute+rand(0,20));
    return "$hour:$minute";

}

function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec','Sunday','Monday','Tuesday',
                        'Wednesday','Thursday','Friday','Saturday','weekend','Festival Holiday','On Leave@Sick Leave','On Leave@Casual Leave', 'On Leave@Earn Leave');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর',
                    'রবিবার','সোমবার','মঙ্গলবার','বুধবার','বৃহস্পতিবার','শুক্রবার','শনিবার','সাপ্তাহিক ছুটি','উৎসব ছুটি','সিক লিভ','নৈমত্তিক ছুটি','উপার্জিত ছুটি');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}

$absent=0;
$working_days=0;
$present=0;
$late=0;
$onleave=0;
$startTime=\App\Http\Controllers\OvertimeController::overtime_start();
$endTime=\App\Http\Controllers\OvertimeController::overtime_end();
$attendance_settings=\Illuminate\Support\Facades\DB::table('attendance_setup')->leftJoin('employees','attendance_setup.id','=','employees.empShiftId')->where('employees.id','=',$emp_id)->first();
$ot=0;
$late_time="00:00";
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>উপস্থিতি তালিকা</title>
    <style>
            body{
                font-family: 'bangla', sans-serif;
                font-size: 12px;
    
            }
            p{  
                line-height: 1px;
            }
    
            #employeeDetails{
                border-collapse: collapse;
                width: 90%;
                font-size: 11px;
                margin:0px auto;
                margin-top: 15px;
    
            }
    
            #employeeDetails td, #employeeDetails th {
                /*border: 1px solid #ddd;*/
                font-size: 14px;
    
            }
            #customers {
               
                border-collapse: collapse;
                width: 100%;
                text-align: center;
            }
    
            #customers td, #customers th {
                width:15px;
                overflow:hidden;
                word-wrap:break-word;
                border: 1px solid #ddd;
                text-align: center !important;
    
            }
    
            #customers th {
                
                
                padding: 5px;
    
            }
    
            table td {
                font-size: 14px;
                padding: 2px;
                margin: 0;
            }
            table th {
                
                font-size: 14px;
                padding: 2px;
                margin: 0;
            }
    
            .reportHeaderArea{
                text-align: center;
            }
    
            .reportHeader{
                line-height: 4px;
            }
            .reportHeader{

                font-size: 11px;
            }
            .reportHeader2{
                
                font-size: 10px;
            }
    
            .reportHeaderCompany{
              font-size: 18px !important;
              
            }
    
            #cardFooter{
                border-collapse: collapse;
                font-size:11px;
                width:70%;
                margin:0px auto;
                margin-top:15px;
                /*float:right; */
    
            }
            .reportDateRange{
               
                font-size: 16px !important; 
                font-weight: bold;
            }
            #cardFooter td {
                text-align: left !important;
            }

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">@if(!empty($companyInformation->bComName)) {{$companyInformation->bComName}} @else {{$companyInformation->company_name}} @endif</h2>
        <p class="reportHeader">@if(!empty($companyInformation->bComAddress)) {{$companyInformation->bComAddress}} @else {{$companyInformation->company_address1}} @endif</p>
        <p class="reportHeader2">জব কার্ড</p>
        <p class="reportDateRange"><b>{{en2bnNumber(\Carbon\Carbon::parse($start_date)->format('d M Y'))}}</b> থেকে <b>{{en2bnNumber(\Carbon\Carbon::parse($end_date)->format('d M Y'))}} তারিখ পর্যন্ত</b></p>
    </div>
    @if(isset($datas[0][0]))
    <table id="employeeDetails">
        <tr>
            <td style="text-align:left" width="35%"><b>কর্মচারী আইডি :</b> {{$datas[0][0]->employeeId}}</td>
            <td style="text-align:left" width="35%"><b>নাম : </b> {{$datas[0][0]->empFirstName." ".$datas[0][0]->empLastName }}</td>
            <td style="text-align:left" width="30%"><b>যোগদানের তারিখ :</b> {{en2bnNumber(\Carbon\Carbon::parse($datas[0][0]->empJoiningDate)->format('d M Y'))}}</td>
        </tr>
        <tr>
            <td style="text-align:left" width="35%"><b>পদবী :</b> {{$datas[0][0]->designation}}</td>
            <td style="text-align:left" width="35%"><b>বিভাগ : </b> {{$datas[0][0]->departmentName}}</td>
            <td style="text-align:left" width="30%"><b>সেকশন : </b> {{$datas[0][0]->empSection}}</td>
        </tr>

    </table>

    <table id='customers' style="margin-top:15px;font-size:11px;" border="1px">
        <thead>
        <tr>
            <th>তারিখ</th>
            <th>বার</th>
            <th>প্রবেশের সময় (ঘণ্টাঃমিনিট)</th>
            <th>বিবৃতি</th>
            <th>লেট (ঘণ্টাঃমিনিট)</th>
            <th>বাহির সময় (ঘণ্টাঃমিনিট)</th>
            <th>ওভারটাইম (ঘণ্টা)</th>
        </tr>
        </thead>
        <tbody>
        @foreach($datas as $d)
            <?php
            $working_days++;
            $leave_check= explode('@',$d[2]);
//            $leave_check[0];
            ?>
            <tr>
                <td>{{en2bnNumber(\Carbon\Carbon::parse($d[1])->format('d M Y'))}}</td>
                <td>{{en2bnNumber(\Carbon\Carbon::parse($d[1])->format('l'))}}</td>
                <td>
                    @if(isset($d[0]->in_time))
                        <?php
                        $present++;
                        ?>

                        {{en2bnNumber(date('G:i', strtotime($d[0]->in_time)))}}
                    @else
                    <span>-</span>
                     @endif
                </td>
                <td>
                    @if(isset($d[0]->in_time))
                        @if($d[0]->in_time>$attendance_settings->max_entry_time)
                            <?php $late++; ?>
                            <span>বিলম্ব</span>
                        @else
                            <span>উপস্থিত</span>
                        @endif
                    @else
                        @if($d[2]=='weekend' || $d[2]=='Festival Holiday' || $leave_check[0]=='On Leave')
                            @if($leave_check[0]!='On Leave')
                                <?php $working_days--; ?>
                            @else
                                <?php $onleave++; ?>
                            @endif
                            <span>{{en2bnNumber($d[2])}}</span>
                        @else
                            <span style="color:#FF0000">অনুপস্থিত</span>

                        @endif
                         @endif
                </td>
                <td>
                    @if(isset($d[0]->in_time))
                        @if($d[0]->in_time>$attendance_settings->max_entry_time)
                            <?php
                            $late_time=late_time($late_time,date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time)));

                            ?>
                            {{en2bnNumber(date('G:i', strtotime($d[0]->in_time) - strtotime($attendance_settings->max_entry_time))) }}
                        @else
                            {{en2bnNumber("00:00")}}
                        @endif
                    @else
                    <span>-</span>
                        
                    @endif
                </td>
                <td>
                    @if(isset($d[0]->out_time))
                        @if($d[0]->in_time==$d[0]->out_time)
                            <span style="color: #FF0000;">প্রদান করেনি</span>
                        @elseif($d[0]->out_time>$endTime)
                            {{en2bnNumber(end_time($endTime))}}

                        @else

                            {{ en2bnNumber(date('G:i', strtotime($d[0]->out_time))) }}

                        @endif
                    @else
                        <span>-</span>
                        
                    @endif
                </td>
                <td>
                    @if(isset($d[0]->out_time) && $d[0]->in_time!=$d[0]->out_time)
                        @if($d[0]->out_time>$startTime && $d[0]->out_time<$endTime)
                            <?php
                            $ot+=roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))
                            ?>
                            {{en2bnNumber(roundTime(date('G:i', strtotime($d[0]->out_time) - strtotime($attendance_settings->exit_time)))) }}

                        @elseif($d[0]->out_time>$endTime)
                            <?php
                            $ot+=roundTime(date('G:i', strtotime($endTime) - strtotime($attendance_settings->exit_time)))
                            ?>
                            {{ en2bnNumber(roundTime(date('G:i', strtotime($endTime) - strtotime($attendance_settings->exit_time)))) }}
                        @else
                            {{en2bnNumber("0.0")}}
                        @endif
                    @else
                        <span>-</span>
                        
                    @endif
                </td>
            </tr>
        @endforeach
        <tr style="font-weight: bold">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            {{--<td></td>--}}
            <td>{{en2bnNumber($late_time)}}</td>
            <td></td>
            <td>{{en2bnNumber($ot)}} ঘণ্টা</td>
        </tr>
        </tbody>
    </table>

</div>


<div>
    <center>
        <table id="cardFooter">
            <tr>
                <td width="50%"><b>মোট কার্যদিবস: </b>{{ en2bnNumber($working_days) }} দিন</td>
            </tr>
            <tr>
                <td  width="50%"><b>মোট উপস্থিতি: </b> {{ en2bnNumber($present) }} দিন</td></tr>

            <tr>
                <td class="padTop10"><b>মোট ছুটি :</b> {{ en2bnNumber($onleave) }} দিন</td></tr>
            <tr>
                <td class="padTop10"><b>মোট অনুপস্থিত :</b> {{ en2bnNumber($working_days-($present+$onleave)) }} দিন</td>
            </tr>

        </table>
    </center>
</div>

@else
    <center><h4 style="color: #FF0000">No data found for the given date</h4></center>

@endif

</body>
</html>




