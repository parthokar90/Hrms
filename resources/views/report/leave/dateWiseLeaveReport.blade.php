@extends('layouts.master')
@section('title', 'Date Wise Leave Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Leave </strong> Report</h3>
                    </div>
                    <div class="panel-content">
                        <div class="row">
                            {{--<div class="col-md-3"></div>--}}
                            {{--<div class="col-md-6"><div class="loader"></div></div>--}}
                            {{--<div class="col-md-3"></div>--}}
                         {{Form::open(array('url' => 'leave/report/dateWiseReport/show','method' => 'post'))}}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required form-label">Start Date</label>
                                    <div class="prepend-icon">
                                        <input type="text" id="leave_start_date" name="leave_start" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required form-label">End Date</label>
                                    <div class="prepend-icon">
                                        <input type="text" id="leave_end" name="leave_end" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="leave_report_url_hidden_id" value="{{URL::to('/leave/report/show')}}">
                            <div class="col-md-2"><button name="tests" type="submit" value="leave_report_generate" class="btn btn-embossed btn-info">Generate Report</button></div>
                            <div class="col-md-2"><button name="tests" type="submit" value"leave_report_pdf" class="btn btn-embossed btn-info">Download PDF</button></div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection