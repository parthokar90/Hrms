@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Earn Leave Report')
@section('content')
  <div class="page-content ">
      <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Earn Leave Report</b></h2>
                    <hr>
                </div>
            </div>


              {!! Form::open(['method'=>'POST','action'=>'ReportController@viewEarnReport']) !!}
                <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Unit<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select name="unitId" class="form-control"  data-search="true">
                              <option value="0">All</option>
                              @foreach($units as $unit)
                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Floor<span class="clon">:</span></label>
                         <div class="col-md-9">
                            <select name="floorId" class="form-control floor"  data-search="true">
                              <option value="0">All</option>
                              @foreach($floors as $floor)
                                <option value="{{$floor->id}}">{{$floor->floor}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Section<span class="clon">:</span></label>
                        <div class="col-md-9">
                          <select name="sectionName" class="form-control"  data-search="true">
                            <option value="">All</option>
                            @foreach($sections as $section)
                              <option value="{{$section->empSection}}">{{$section->empSection}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Department<span class="clon">:</span></label>
                        <div class="col-md-9">
                          <select name="departmentId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($departments as $department)
                              <option value="{{$department->id}}">{{$department->departmentName}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Designation<span class="clon">:</span></label>
                        <div class="col-md-9">
                          <select name="designationId" class="form-control"  data-search="true">
                            <option value="0">All</option>
                            @foreach($designations as $designation)
                              <option value="{{$designation->id}}">{{$designation->designation}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                        <label class="col-md-3">Gender<span class="clon">:</span></label>
                         <div class="col-md-9">
                            <select name="empGenderId" class="form-control"  data-search="true">
                              <option value="0">All</option>
                               <option value="1">Male</option>
                               <option value="2">Female</option>
                               <option value="3">Other</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Printable Column<span class="clon">:</span></label>
                      <div class="col-md-9">
                        <div class="form-control">
                          
                          <input type="checkbox" checked name="coldepartment" value="1"> Department  &nbsp;&nbsp;
                          <input type="checkbox" checked name="coldesignation" value="1"> Designation  &nbsp;&nbsp;
                  
                          <input type="checkbox" name="coljoiningdate" value="1"> Joiningdate  <br><br>
                          <input type="checkbox" name="colunit" value="1"> Unit  &nbsp;&nbsp;
                          <input type="checkbox" name="colfloor" value="1"> Floor &nbsp;&nbsp;
                      
                          <input type="checkbox" name="colsection" value="1"> Section &nbsp;&nbsp;
                        </div>  
                      </div>
                    </div>
                                    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Page Size<span class="clon">:</span></label>
                      <div class="col-md-9">
                          <select class="form-control" name="pagesize">
                            <option selected value="A4">A4</option>
                            <option value="Legal">Legal</option>
                            <option value="Letter">Letter</option>
                          </select>
                      </div>
                    </div>
    
                    <div class="form-group">
                      <div class="input-form-gap"></div>
                      <label class="col-md-3">Page Orientation<span class="clon">:</span></label>
                      <div class="col-md-9">
                          <select class="form-control" name="pageOrientation">
                            <option selected value="Portrait">Portrait</option>
                            <option value="Landscape">Landscape</option>
                          </select>
                      </div>  
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                          
                          <hr>
                          <input type="submit" value="Preview" name="viewType" class="btn btn-success margin-top-10">
        
                          <span class="dropdown">
                              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Generate PDF
                              <span class="caret"></span></button>
                              <ul class="dropdown-menu">
                                  <li><button type="submit" value="pdfenglish" name="viewType" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> &nbsp; English </button></li>
                                  <li><button type="submit" value="pdfbangla" name="viewType" class="btn btn-blue"><i class="fa fa-file-excel-o"></i> &nbsp; বাংলা </button></li>
                              </ul>
                          </span>
                          <hr>
                        </div>
                    </div>

                </div>
                </div>
             {!! Form::close() !!}
      </div>
  </div>


@include('include.copyright')
@endsection