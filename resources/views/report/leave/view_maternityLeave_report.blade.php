@extends('layouts.master')
@section('title', 'Maternity Leave Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i> <strong>Maternity Leave Report</strong></h3>
                            </div>
                        </div>
                    </div>
                        @if(count($maternityLeave)!=0)
                        <div class="panel-content pagination2 table-bordered">
                            <table class="table table-hover table-striped table-bordered">
                                <thead style="font-size:15px;font-weight:bold;">
                                <tr>
                                    <th>id</th>
                                    <th>Name</th>
                                    @if(!empty($request->coldesignation))
                                    <th>Designation</th>
                                    @endif
                                    @if(!empty($request->coldepartment))
                                    <th>Department</th>
                                    @endif
                                    @if(!empty($request->coljoiningdate))
                                    <th>Joining Date</th>
                                    @endif
                                    @if(!empty($request->colunit))
                                    <th>Unit</th>
                                    @endif
                                    @if(!empty($request->colfloor))
                                    <th>Floor</th>
                                    @endif
                                
                                    @if(!empty($request->colsection))
                                    <th>Section</th>
                                    @endif
                                    <th>Leave Period</th>
                                    @if(!empty($request->colprev3monthSal))
                                    <th>Previous three month salary</th>
                                    @endif
                                    @if(!empty($request->colprev3monthDay))
                                    <th>Previous three month working days</th>
                                    @endif
                                    <th>Total Payable amount</th>
                                    <th>Per Installment</th>
                                    <th>Status</th>
                                    
                                    <th>first installment Date</th>
                                   
                                    <th>second installment date</th>

                                    <th>Approved By</th>
                                    
                                
                                
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($maternityLeave as $employee)
                                @php 
                                if(($employee->other3)=="00"){
                                    $status="Not Paid";
                                }
                                elseif(($employee->other3)=="10"){
                                    $status="Half Paid";
                                }
                                elseif(($employee->other3)=="11"){
                                    $status="Full Paid";
                                }
                                @endphp
                                <tr>
                                    <td>
                                        <button type="submit" class="btn btn-success btn-sm">{{$employee->employeeId}}</button>
                                    </td>
                                    <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                                    @if(!empty($request->coldesignation))
                                    <td>{{$employee->designation}}</td>
                                    @endif
                                    @if(!empty($request->coldepartment))
                                    <td>{{$employee->departmentName}}</td>
                                    @endif
                                    @if(!empty($request->coljoiningdate))
                                    <td>{{date("d-M-Y",strtotime($employee->empJoiningDate))}}</td>
                                    @endif
                                
                                    @if(!empty($request->colunit))
                                        <td>{{$employee->unitName}}</td>
                                    @endif
                                    @if(!empty($request->colfloor))
                                        <td>{{$employee->floorName}}</td>
                                    @endif
                                
                                    @if(!empty($request->colsection))
                                    <td>{{$employee->empSection}}</td>
                                    @endif
                                    <td>{{date("d-M",strtotime($employee->leave_starting_date))}} {{date("d-M-Y",strtotime($employee->leave_ending_date))}}</td>
                                    @if(!empty($request->colprev3monthSal))
                                    <td>{{number_format(($employee->previous_three_month_salary))}} BDT</td>
                                    @endif
                                    @if(!empty($request->colprev3monthDay))
                                    <td>{{$employee->total_working_days}}</td>
                                    @endif
                                    <td>{{number_format(($employee->total_payable_amount))}} BDT</td>
                                    <td>{{number_format(($employee->per_instalment))}} BDT</td>
                                    <td>{{$status}}</td>
                                    <td>
                                        @if(!empty($employee->first_installment_pay))
                                        {{date("d-M-Y",strtotime($employee->first_installment_pay))}}
                                        @else
                                        <h3>N/A</h3>
                                        @endif
                                    </td>
                                    <td>
                                        @if(!empty($employee->second_installment_pay))
                                        {{date("d-M-Y",strtotime($employee->second_installment_pay))}}
                                        @else
                                        <h3>N/A</h3>
                                        @endif
                                    </td>
                                    <td>
                                        @if(!empty($employee->approved_by))
                                        {{$employee->approved_by}}
                                        @else
                                        <h3>N/A</h3>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                            @else
                            <h3>No maternity leave details found </h3>
                            @endif
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
  @include('include.copyright')
@endsection