@extends('layouts.master')
@section('title', 'Date Wise Leave Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Leave </strong> Report</h3>
                    </div>
                    <div class="panel-content">
                      <a href="{{url('report/leave/dateWiseReport')}}" class="btn btn-sm btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                        
                        @if(count($data)==0)
                        
                            <h3><p class="text-center">No leave Available</p></h3>
                        
                        @else
                        <h1 class="text-center">Leave Report</h1>
                        <p class="text-center">Current Month <?php echo date('M-Y'); ?></p>
                        <p class="text-center">Total day in month : <?php echo date('t'); ?></p>
            
                        

                            <table id="leave_display" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Employee</th>
                                    <th>Designation</th>
                                    <th>Leave Type</th>
                                    <th>Leave Taken</th>
                                    <th>Leave Available</th>
                                    <th>Leave Date</th>
                                    {{--<th>Date</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @php $order=0; @endphp
                                @foreach($data as $item)
                                @php $order++; @endphp
                                <tr>
                                <td>{{$order}}</td>
                                <td>{{$item->empFirstName}} {{$item->empLastName}}</td>
                                <td>{{$item->designation}}</td>
                                <td>
                                    @foreach($tb_leave_type as $all_leave_type)
                                    {{$all_leave_type->leave_type}} ({{$all_leave_type->total_days}})
                                    @endforeach
                                </td>
                                <td>
                                {{$item->names}}
                                </td>
                                
                                <td>
                                    {{$item->leave_available}}
                                </td>

                                <td>
                                Leave Start Date: {{$item->leave_sdays}} <br> Leave End Date:{{$item->leave_edays}}
                                </td>

                                {{--<td>--}}
                                    {{--{{\Carbon\Carbon::parse($item->report_date)->format('j F Y') }}--}}
                                {{--</td>--}}
                                </tr>
                            @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection