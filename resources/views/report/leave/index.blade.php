@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Leave Report')
@section('content')
    <div class="page-content page-content-hover-1">
        <div class="row panel myAnimation_1"  style="border:1px solid #dcdcdc;">

            <div class="col-xlg-12 col-lg-12  col-sm-12 animated bounceInRight" >
                <div class="text-center" >
                    <h2><b>Leave Report</b></h2>
                    <hr class="animated bounceInLeft">
                </div>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="leave/onLeaveReport">
              <div class="panel">
                <div class="panel-content leave_olemp">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-th f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>On Leave Employee List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a target="_BLANK" href='leave/dateWiseReport'>
              <div class="panel">
                <div class="panel-content leave_dwlr">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-history f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Date Wise Leave Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div> --}}

            {{--  <div class="col-xlg-3 col-lg-3 col-sm-3">
              <a href="leave/availableLeaveReport">
              <div class="panel">
                <div class="panel-content leave_alfe">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-exclamation-triangle f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Available Leave for Employee</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>  --}}

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="leave/earnLeaveReport">
              <div class="panel">
                <div class="panel-content leave_elr">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>  Earn Leave Report</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a target="_BLANK" href="leave/maternityLeaveReport">
                <div class="panel">
                    <div class="panel-content leave_mlel">
                      <center>
                        <div class="row" >
                          <div class="">
                              <center><i class="fa fa-th f-40"></i></center>
                          </div>
                          <br>
                          <div class="">
                          <center>
                            <span class="f-14"><b>Maternity Leave Employee List</b></span>
                            </center>
                          </div>
                        </div>
                      </center>
                  </div>
                </div>
              </a>
            </div>



            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
            <a target="_BLANK" href="{{url('employee/leave/summary')}}">
                  <div class="panel">
                      <div class="panel-content leave_mlel">
                        <center>
                          <div class="row" >
                            <div class="">
                                <center><i class="fa fa-th f-40"></i></center>
                            </div>
                            <br>
                            <div class="">
                            <center>
                              <span class="f-14"><b>Leave Summary</b></span>
                              </center>
                            </div>
                          </div>
                        </center>
                    </div>
                  </div>
                </a>
              </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                <a target="_BLANK" href="{{url('employee/leave/summary/datewise')}}">
                      <div class="panel">
                          <div class="panel-content leave_mlel">
                            <center>
                              <div class="row" >
                                <div class="">
                                    <center><i class="fa fa-th f-40"></i></center>
                                </div>
                                <br>
                                <div class="">
                                <center>
                                  <span class="f-14"><b>Leave Summary Datewise</b></span>
                                  </center>
                                </div>
                              </div>
                            </center>
                        </div>
                      </div>
                    </a>
                  </div>
            </div>
      </div>
@include('include.copyright')
@endsection