@extends('layouts.master')
@section('title', 'Daily Recruitment List')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Applicants List </strong></h3>
                    </div>
                    <div class="panel-content">
                      <a href="{{url('report/recruitment')}}" class="btn btn btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                      @if(count($dailyRecruitment)==0)
                        
                      <h3><p class="text-center">No recruitments found today</p></h3>
                      
                       
                        
                        @else
                        <a href="{{url('report/recruitment/dailyRecruitmentListPDF')}}" class="btn btn-success"><i class="fa fa-arrow-down">Download Report</i></a>
                        <h1 class="text-center">Applicants List</h1>
                        <p class="text-center">Current Date: <strong><?php echo date('d-M-Y'); ?></p></strong>
                        
                            <table id="leave_display" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Applicant Name</th>
                                    <th>Contact No.</th>
                                    <th>Position Applied</th>
                                    <th>Interview Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $order=0; @endphp
                                @foreach($dailyRecruitment as $item)
                                @php 
                                if(($item->interviewDate))
                                    $status='Approved';
                                
                                else
                                    $status='Rejected';

                                if($item->interviewDate)
                                    $interViewDate=date("d F Y",strtotime($item->interviewDate));
                                else 
                                $interViewDate='Not Available';
                                
                                $order++; 
                                @endphp
                                <tr>
                                <td>{{$order}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->phone}}</td>
                                <td>{{$item->vacTitle}}</td>
                                <td>{{$interViewDate}}</td>
                                </tr>
                            @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection