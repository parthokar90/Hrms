@extends('layouts.master')
@section('title', 'Vacancy List Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Vacancy List </strong></h3>
                    </div>
                    <div class="panel-content">
                      <a href="{{url('report/vacancy')}}" class="btn btn btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                       
                        @if(count($vacancyList)==0)
                        
                            <h3><p class="text-center">No Vacancies found</p></h3>
                        
                        @else
                        <h1 class="text-center">Vacancy List</h1>
                        <p class="text-center">Current Date: <strong><?php echo date('d-M-Y'); ?></p></strong>
                        <p class="text-center">Total day in month : <?php echo date('t'); ?></p>
            
                        

                            <table id="leave_display" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Post Name</th>
                                    <th>Description</th>
                                    <th>Vacancy number</th>
                                    <th>Vacancy Created on</th>
                                    <th>Last date of apply</th>
                                    <th>Joining Date</th>
                                    <th>Status</th>
                            
                                    
                                </tr>
                                </thead>
                                <tbody>
                                @php $order=0; @endphp
                                @foreach($vacancyList as $item)
                                @php 
                                if(($item->vacStatus)==1)
                                    $status='Active';
                                
                                else
                                    $status='Inactive';

                                if($item->vacDescription){
                                    $description=$item->vacDescription;
                                }
                                else{
                                    $description="N\A";
                                }
                                
                                $vacStart=strtotime($item->VacAnnounceStartingDate);
                                $vacEnd=strtotime($item->vacAnnounceEndingDate);
                                $joinEnd=strtotime($item->vacJoiningDate);
                                $order++; 
                                @endphp
                                <tr>
                                <td>{{$order}}</td>
                                <td>{{$item->vacTitle}}</td>
                                <td>{{$description}}</td>
                                <td>{{$item->vacNumber}}</td>
                                <td>{{date("d F Y", $vacStart)}}</td>
                                <td>{{date("d F Y", $vacEnd)}}</td>
                                <td>{{date("d F Y", $joinEnd)}}</td>
                                <td>{{$status}}</td>
                                </tr>
                            @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection