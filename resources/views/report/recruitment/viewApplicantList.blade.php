@extends('layouts.master')
@section('title', 'Application List Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i> <strong>Applicant </strong> List</h3>
                                
                            </div>
                        
                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                            <a href="{{url('report/vacancy/applicantList')}}" class="btn btn btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                        <table class="table table-hover table-striped table-bordered">
                            <thead style="font-size:15px;font-weight:bold;">
                            <tr>
                                <th>Name</th>
                                <th>Contact</th>
                                <th>Position</th>
                                @if(!empty($request->vacStatusCheck))
                                <th>Vacancy Status</th>
                                @endif
                                @if(!empty($request->applicationDateCheck))
                                <th>Application Date</th>
                                @endif
                                @if(!empty($request->priorityCheck))
                                <th>Priority</th>
                                @endif
                                @if(!empty($request->eligibilityCheck))
                                <th>Eligible For Interview</th>
                                @endif
                                @if(!empty($request->interviewDateCheck))
                                <th>Interview Date</th>
                                @endif
                                @if(!empty($request->jobStatusCheck))
                                <th>Job Status</th>
                                @endif
                                
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($applicantListShow as $employee)
                            <tr>
                                <td>{{$employee->name}}</td>
                                <td>{{$employee->phone}}</td>
                                <td>{{$employee->vacTitle}}</td>
                                @if(!empty($request->vacStatusCheck))
                                <td>
                                    @if($employee->vacStatus==0)
                                        Inactive
                                    @endif
                                    @if($employee->vacStatus==1)
                                        Active
                                    @endif
                                </td>
                                @endif
                                @if(!empty($request->applicationDateCheck))
                                <td>{{date("d M Y",strtotime($employee->submittedDate))}}</td>
                                @endif
                                @if(!empty($request->priorityCheck))
                                <td>{{$employee->priorityStatus}}</td>
                                @endif
                                @if(!empty($request->eligibilityCheck))
                                <td>{{$employee->interviewStatus}}</td>
                                @endif
                               
                                @if(!empty($request->interviewDateCheck))
                                    <td>{{date("d M Y",strtotime($employee->interviewDate))}}</td>
                                @endif
                                @if(!empty($request->jobStatusCheck))
                                    <td>{{$employee->jobStatus}}</td>
                                @endif
                                
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @include('include.copyright')
@endsection