@php
use Carbon\Carbon;
@endphp
@extends('layouts.master')
@section('title', 'Month Wise Salary')
@section('content')
    <style>
    .table>thead>tr>td.active, .table>tbody>tr>td.active, .table>tfoot>tr>td.active, .table>thead>tr>th.active, .table>tbody>tr>th.active, .table>tfoot>tr>th.active, .table>thead>tr.active>td, .table>tbody>tr.active>td, .table>tfoot>tr.active>td, .table>thead>tr.active>th, .table>tbody>tr.active>th, .table>tfoot>tr.active>th {
        /*background-color: #f5f5f5;*/
        font-size: 10px;
        text-align: center;
    }
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        padding: 1px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

    .paysheet{
        text-align: center;
    }

    .department-employee{
        width: 35%;
        float:left;
    }

    .department-details{
        width: 100%;
        /*float:right;*/
    }

    .table2{
        background-color: #f7f7f7;
    }

    .table2 td{
        padding:1px 3px;
    }
    .pagination li a {
        color: #ffffff;
        font-size: 13px;
        background: midnightblue;
    }
</style>
<div class="page-content">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 style="text-align: center;">
                    @foreach($paymonth as $monthname)
                        <b>Salary Sheet of : {{date('F Y',strtotime($monthname->month))}}</b>(
                        Days in Month: {{date('t',strtotime($monthname->month))}})
                    @endforeach

            </h4>
        </div>

        <div class="panel-body">
            <div id="customers">
                <table style="width: 100%" class="table" border="1px">
                    <thead>
                        <tr>
                            <th style="font-size: 8px;" width="2%">SN</th>
                            <th style="font-size: 8px;">Department</th>
                            <th style="font-size: 8px;">EmpID</th>
                            <th style="font-size: 8px;">Name</th>
                            <th style="font-size: 8px;">Designation</th>
                            <th style="font-size: 8px;">Grade</th>
                            <th style="font-size: 8px;">Basic</th>
                            <th style="font-size: 8px;">House</th>
                            <th style="font-size: 8px;">Medical</th>
                            <th style="font-size: 8px;">Food</th>
                            <th style="font-size: 8px;">Transport</th>
                            <th style="font-size: 8px;">Gross</th>
                            <th style="font-size: 8px;">Leave</th>
                            <th style="font-size: 8px;">WorkDays</th>
                            <th style="font-size: 8px;">Present</th>
                            <th style="font-size: 8px;">Abs days</th>
                            <th style="font-size: 8px;">Weekend</th>
                            <th style="font-size: 8px;">Holiday</th>
                            <th style="font-size: 8px;">Deduction</th>
                            <th style="font-size: 8px;">Normal Deduction</th>
                            <th style="font-size: 8px;">Advanced Deduction</th>
                            <th style="font-size: 8px;">Gross Pay</th>
                            <th colspan="3">
                                <center>Overtime</center>
                            </th>
                            <th style="font-size: 8px;">Att Bonus</th>
                            <th style="font-size: 8px;">Festival Bonus</th>
                            <th style="font-size: 8px;">Special allow</th>
                            <th style="font-size: 8px;">Production Bonus</th>
                            <th style="font-size: 8px;">Net Wages</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $order=0;
                        @endphp
                            @foreach($data as $salary_report)
                        @php
                        $order++;
                        @endphp
                        <tr class="active">
                            <td style="padding: 1px;">{{$order}}</td>
                            <td>{{$salary_report->departmentName}}</td>
                            <td>{{$salary_report->employeeId}}</td>
                            <td>
                                {{$salary_report->empFirstName}} {{$salary_report->empLastName}}<br>
                                Join: {{$salary_report->empJoiningDate}}
                            </td>
                            <td>{{$salary_report->designation}}</td>
                            <td>{{$salary_report->grade_name}}</td>
                            <td>{{$salary_report->basic_salary}}</td>
                            <td>{{$salary_report->house_rant}}</td>
                            <td>{{$salary_report->medical}}</td>
                            <td>{{$salary_report->food}}</td>
                            <td>{{$salary_report->transport}}</td>
                            <td>{{$salary_report->gross}}</td>
                            <td>
                                @if($salary_report->leave=='')
                                0
                                @else
                                {{$salary_report->leave}}
                                @endif
                            </td>
                            <td>{{$salary_report->working_day}}</td>
                            <td>
                                @if($salary_report->present=='')
                                0
                                @else
                                {{$salary_report->present}}
                                @endif
                            </td>
                            <td>{{$salary_report->absent}}</td>
                            <td>{{$salary_report->weekend}}</td>
                            <td>{{$salary_report->holiday}}</td>
                            <td>
                                <span style="border-bottom: 1px solid #000000;">Absent:</span>
                                {{$salary_report->absent_deduction_amount}}
                            </td>
                            <td>
                                @if($salary_report->normal_deduction=='')
                                0
                                @else
                                {{$salary_report->normal_deduction}}
                                @endif
                            </td>
                            <td>
                                @if($salary_report->advanced_deduction=='')
                                0
                                @else
                                {{$salary_report->advanced_deduction}}
                                @endif
                            </td>
                            <td>{{$salary_report->gross_pay}}</td>
                            <td>
                                <span style="border-bottom: 1px solid #000000;">Hrs:</span>
                                @if($salary_report->overtime=='')
                                0/hr
                                @else
                                {{$salary_report->overtime}}/hr
                                @endif
                            </td>
                            <td>
                                <span style="border-bottom: 1px solid #000000;"> Rat:</span>
                                {{$salary_report->overtime_rate}}
                            </td>
                            <td>
                                <span style="border-bottom: 1px solid #000000;">Amt:</span>
                                {{$salary_report->overtime_amount}}
                            </td>
                            <td>
                                @if($salary_report->attendance_bonus=='' && $salary_report->attendance_bonus_percent=='')
                                0
                                @else
                                {{$salary_report->attendance_bonus+$salary_report->attendance_bonus_percent}}
                                @endif
                            </td>

                            <td>
                                @if($salary_report->festival_bonus_amount=='' && $salary_report->festival_bonus_percent=='')
                                0
                                @else
                                {{$salary_report->festival_bonus_amount+$salary_report->festival_bonus_percent}}
                                @endif
                            </td>
                            <td>
                                @if($salary_report->increment_bonus_amount=='' && $salary_report->increment_bonus_percent=='')
                                0
                                @else
                                {{$salary_report->increment_bonus_amount+$salary_report->increment_bonus_percent}}
                                @endif
                            </td>
                            <td>
                                @if($salary_report->production_bonus=='')
                                    0
                                @else
                                    {{$salary_report->production_bonus}}
                                @endif
                            </td>
                            <td>{{$salary_report->net_amount}}</td>
                        </tr>
                            @endforeach
                    </tbody>
                </table>












            </div>
            <div style="font-size: 12px;">
<!--                 <div class="department-employee" >
                 Total Employee:
                 <table  border="1" width="50%">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Department Name</th>
                            <th>Employee Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i=0; @endphp
                        @foreach ($employees_count_department_wise as $total_employee)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>
                                {{ $total_employee->departmentName }}
                            </td>
                            <td>
                                {{ $total_employee->count }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> -->
            <div class="department-details">
                <table border="1" class="table2"  width="100%">
                    <tr>
                        <th></th>
                        <th style="text-align:center;">
                            Basic
                        </th>
                        <th style="text-align:center;">
                            House
                        </th>
                        <th style="text-align:center;">
                            Medical
                        </th>
                        <th style="text-align:center;">
                            Food
                        </th>
                        <th style="text-align:center;">
                            Transportation
                        </th>
                        <th style="text-align:center;">
                            Gross
                        </th>
                        <th style="text-align:center;">
                            Gross Pay
                        </th>
                        <th style="text-align:center;">
                            Overtime
                        </th>
                        <th style="text-align:center;">
                            Overtime Amount
                        </th>

                        <th style="text-align:center;">
                            Attendance Bonus
                        </th>
                        <th style="text-align:center;">
                            Special Bonus
                        </th>
                        <th style="text-align:center;">
                            Net Wages
                        </th>

                    </tr>
                    @foreach($departmentwise_total as $total_amount)
                    <tr>
                        <td style="font-weight:bold;">
                            {{$total_amount->departmentName}}
                        </td>
                        <td style="text-align:center;">
                            @money($total_amount->total_basic)
                        </td>
                         <td style="text-align:center;">
                            @money($total_amount->total_house)
                        </td>
                        <td style="text-align:center;">
                            @money($total_amount->total_medical)
                        </td>
                        <td style="text-align:center;">
                            @money($total_amount->total_food)
                        </td>
                        <td style="text-align:center;">
                            @money($total_amount->total_trasport)
                        </td>
                        <td style="text-align:center;">
                            @money($total_amount->total_gross)
                        </td>
                         <td style="text-align:center;">
                            @money($total_amount->total_gross_pay)
                        </td>
                        <td style="text-align:center;">
                            {{$total_amount->total_overtime_hour}}/hr
                        </td>
                        <td style="text-align:center;">
                            @money($total_amount->total_overtime_amount)
                        </td>
                        <td style="text-align:center;">
                            @money($total_amount->total_attendance_bous)
                        </td>
                        <td style="text-align:center;">
                            @money($total_amount->total_one_Year_bonus)
                        </td>
                        <td style="text-align:center;">
                            @money($total_amount->total_net_amount)
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
      </div>
</div>
</div>
</div>
@include('include.copyright')
@endsection