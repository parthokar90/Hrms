
<!DOCTYPE html>
<html>
<head>
    <title>Salary Summary Lefty</title>
    <style>
        @page        {
            size: landscape;
        }
        .verticalTableHeader {
            text-align:center;
            white-space:nowrap;
            g-origin:50% 50%;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);

        }
        .verticalTableHeader p {
            margin:0 -100% ;
            display:inline-block;
        }
        .verticalTableHeader p:before{
            content:'';
            width:0;
            padding-top:110%;/* takes width as reference, + 10% for faking some extra padding */
            display:inline-block;
            vertical-align:middle;
        }
        .company{
            text-align: center;
        }
        .company p{
            padding: 0;
            margin: 0;
            font-size: 17px;
        }
        .main_div{
            width:100%;
            margin: 0px;
        }
        .salary_table{
            width: 100%;
            margin-top:8px;
        }
        .authority{
            width:100%;
            padding-top: 47px;
        }
        .prepared_by{
            width: 25%;
            float: left;
        }
        .audited_by{
            width: 25%;
            float: left;
        }
        .recomended_by{
            width: 25%;
            float: left;
        }
        .approved_by{
            width: 25%;
            float: left;
        }
        .table_1{
            border-collapse: collapse;
            width: 100%;
            font-size:12px;
        }

        .table_1 tr th,td{
            border: 1px solid black;
            text-align: center;
        }

        .table_lefttd{
            /*padding-left:5px;*/
            text-align: center !important;
        }
        .table2{
            width: 100%;
            font-size:12px;
        }

        .table2 tr>td{
            border: none;
            text-align: left;
        }
        .main_foter{
            border: 1px solid #000000;
            margin-top: -68px;
            /*padding-left: 290px;*/
            font-size: 14px;
            height: 24px;
            line-height: 24px;
        }
        .main_foter span{
            margin-right:6px;
        }

        tfoot{
            width: 100%;
        }


        tfoot tr td{

            width: 100%;
            clear: both;
            border:none;
            padding-top: 0px!important;
            color:#000;
        }
        .authority span{
            border-top: 1px solid #000;
            padding-top: 3px;
            padding-left: 15px;
            padding-right: 15px;
        }

        tfoot tr td .main_foter{
            text-align: left;
            border:none;
        }
        tfoot tr td .main_foter span{
            margin-right: 10px;
        }
        .company_heading_1 td{
            border: none;
            line-height: .7;
        }

        .company_heading_2 td{
            border:none;

        }
        tfoot td{
            border:none;
        }
        td.signature_td{
            /*width:70px;
            height:81px;*/
        }
        .dayofmonth{
            display: inline-block;
            padding-right: 40px;
        }
        .company_heading_1 td span{
            display: inline-block;
            margin-bottom: 5px;
        }
        .company_heading_1 h3{
            margin-bottom:2px;
            margin-top: 5px;
        }
        .company_heading_1 p {
            margin-top: 0px;
            font-size: 14px;
            margin-bottom: 17px;
        }
        .main_foter span:nth-child(1){

            padding-right: 17%;
        }
        .main_foter span:nth-child(2){
            font-size:12px;
        }
        .main_foter span:nth-child(3){
            font-size:12px;
        }
        .main_foter span:nth-child(4){
            font-size:12px;
        }
        .main_foter span:nth-child(5){
            font-size:12px;
        }
        .main_foter span:nth-child(6){
            font-size:12px;
        }
        .main_foter span:nth-child(7){
            font-size:12px;
        }
        .main_foter span:nth-child(8){
            font-size:12px;
            margin-left:6%;
        }
        .main_foter span:nth-child(9){
            font-size:12px;
            margin-left: 9%;
        }
        .main_foter span:nth-child(10){
            font-size:12px;
        }
        .ex_footer{
            opacity:0;
        }


        #signature_11{
            padding-left: 35px;
            padding-right: 35px;
            
        }
        .signature_td{
                width: 130px!important;
                height: 70px!important;
        }
        .tbody_td td{
            height: 32px;
        }
        .tbody_td_foot td{
            height: 25px;
            font-weight: bold;
        }
        
        thead tr th{
            height: 30px;
        }

        @media    print {
            .btn_hidden{
                display: none;
            }
            .main_div{page-break-after: always;}
            table { page-break-inside:auto; }
            tr    { page-break-inside:avoid; page-break-after:auto;}
            thead {display: table-header-group;}
            tbody { page-break-after:always;
                display: table-row-group;}
            tfoot {
                display: table-row-group;
            }
            /*td.signature_td{
                width:70px;
                height:101px;
            }*/


            .ex_footer{
                opacity:0;
                position: fixed;
                bottom:0px;
                left:0;
                width:100%;
                /* padding-top:20px; */
                /* height:100px; */

            }
            .ex_footer .authority{
                padding-bottom: 0px;
                padding-top:20px;
                text-align:center;


            }
            .ex_footer .authority span{
                font-size:14px;
            }

            tfoot tr td .authority span{
                color:#fff;
                opacity:1;
            }
            tfoot tr td .authority span{
                border-top: 1px solid #fff;
                color:#fff;
                opacity:1;
            }

            .ex_footer tr td .authority span{
                color:#000;
            }
            .ex_footer .authority span{
                border-top: 1px solid #000;
                color:#000;
            }
            .signature_td{
                width: 130px!important;
                height: 70px!important;
            }

            .pageNumber1:after {
                counter-increment: page;
                content:"Page number: " counter(page);
                left: 0;
                top: 100%;
                white-space: nowrap;
                z-index: 20;
                -moz-border-radius: 5px;
                -moz-box-shadow: 0px 0px 4px #222;
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
            }

        }
    </style>





</head>
<body>
        <p class="pull-right"><button style="margin-top: 17px;" class="btn btn-info pull-right" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button></p>
<div id="print_area">
        <div class="main_div">

            <div class="salary_table">
                <table class="table_1">
                    <thead>
                    <tr class="table_heading">

                    <tr style="text-align: center;" class="company_heading_1">

                        <td colspan="30">
                         <h2>{{$companyInformation->company_name}}</h2>
                         <p>Salary Summary Lefty {{date('F-Y',strtotime($_POST['salary_summery_monthsss']))}}</p>
                     </td>
                    </tr>

                    <tr>

                        <th rowspan="2">SN</th>
                        <th rowspan="2">
                                @if(isset($dept_id))
                                Department
                                @endif
                                @if(isset($section_id))
                                 Section 
                                @endif
                                @if(isset($monthly_data))
                                Department
                                @endif
                                @if(isset($all_dept))
                                 Department
                                @endif
                                @if(isset($all_section))
                                Section 
                                @endif
                        </th>
                        <th rowspan="2">WORKER</th>
                        <th rowspan="2"   class="table_lefttd">BASIC</th>
                        <th rowspan="2" class="table_lefttd">HOUSE</th>
                        <th rowspan="2">MEDICAL</th>
                        <th rowspan="2">FOOD</th>
                        <th rowspan="2">TA/DA</th>
                        <th rowspan="2">GROSS </th>
                        <th rowspan="2">GROSS PAY</th>
                        <th rowspan="2">ABS</th>
                        <th rowspan="2">ADV</th>
                        <th rowspan="2" >OT</th>
                        <th rowspan="2">OT AMT</th>
                        <th style="text-align: center" rowspan="2" >ATT BONUS</th>
                        <th rowspan="2">SP ALLOW</th>
                        <th style="text-align: center" rowspan="2">NET</th>

                    </tr>
                    </tr>
                    </thead>
                       <tbody>
                            @php $m_power=0;$basic_total=0;$house_total=0;$total_medicals=0; $i=0;
                            $total_foods=0;$total_transports=0;$total_gorss=0;$total_gross_pay=0;
                            $abs_deduction_amount=0;$total_advance_deduction=0;$total_overtime=0;
                            $total_ot_amount=0;$total_att_bonus=0;$total_increment_bonus=0;$total_net_amount=0;
                            @endphp
                      
                      @foreach($data as $summery)
                        <tr class="tbody_td">
                            <td >{{++$i}}</td>
                            <td>
                                    @if(isset($dept_id))
                                    {{$summery->departmentName}}
                                    @endif
                                    @if(isset($section_id))
                                    {{$summery->section_id}}
                                    @endif
                                    @if(isset($monthly_data))
                                     {{$summery->departmentName}}
                                    @endif
                                    @if(isset($all_dept))
                                     {{$summery->departmentName}}
                                    @endif
                                    @if(isset($all_section))
                                    {{$summery->section_id}}
                                    @endif
                            </td>
                            <td class="table_lefttd">{{$summery->worker}} @php $m_power+=$summery->worker; @endphp</td>
                            <td class="table_lefttd" style="text-align:center">{{number_format(round($summery->basic))}} @php $basic_total+=$summery->basic; @endphp</td>
                            <td class="table_lefttd" style="text-align:center">{{number_format(round($summery->house))}} @php $house_total+=$summery->house; @endphp</td>
                            <td>{{number_format(round($summery->medicals))}} @php $total_medicals+=$summery->medicals; @endphp</td>
                            <td>{{number_format(round($summery->foods))}} @php $total_foods+=$summery->foods; @endphp</td>
                            <td>{{number_format(round($summery->transports))}} @php $total_transports+=$summery->transports; @endphp</td>
                            <td>{{number_format(round($summery->gorss))}} @php $total_gorss+=$summery->gorss; @endphp</td>
                            <td>{{number_format(round($summery->gross_pay))}} @php $total_gross_pay+=$summery->gross_pay; @endphp</td>
                            <td>{{number_format(round($summery->a_deduction_amount))}} @php $abs_deduction_amount+=$summery->a_deduction_amount; @endphp</td>
                            <td>@if($summery->advanced_deduction=='') 0 @else {{number_format(round($summery->advanced_deduction))}} @endif @php $total_advance_deduction+=$summery->advanced_deduction; @endphp</td>
                            <td>{{($summery->overtimes)}} @php $total_overtime+=$summery->overtimes; @endphp</td>
                            <td>{{number_format(round($summery->ot_amounts))}} @php $total_ot_amount+=$summery->ot_amounts; @endphp</td>
                            <td>{{number_format(round($summery->attendance_bonus))}} @php $total_att_bonus+=$summery->attendance_bonus; @endphp</td>
                            <td>@if($summery->total_increment=='') 0 @else {{number_format(round($summery->total_increment))}} @endif @php $total_increment_bonus+=$summery->total_increment; @endphp</td>
                            <td>{{number_format(round($summery->net_total))}} @php $total_net_amount+=$summery->net_total; @endphp</td>
                        </tr>
                        @endforeach


                                                                    
                        <tr style="padding: 3px 3px;" class="tbody_td_foot">
                            <td style="border-bottom: 1px solid #555;text-align: left;" colspan="2">Grand Total:</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($m_power)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($basic_total)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($house_total)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_medicals)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_foods)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_transports)}}</td>
                            <td  style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_gorss)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_gross_pay)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($abs_deduction_amount)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_advance_deduction)}}</td>
                            
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_overtime)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_ot_amount)}}</td>


                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_att_bonus)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_increment_bonus)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{number_format($total_net_amount)}}</td>
                        </tr>
                    <tr></tr>
                        <tr>
                            <td colspan="31" style="padding-top:35px;border:none;border-top:4px solid #ffffff;" >
                                <div class="authority">
                                    <div class="prepared_by">
                                        <span>Prepared By</span>
                                    </div>
                                    <div class="audited_by">
                                        <span>Audited By</span>
                                    </div>
                                    <div class="recomended_by">
                                        <span>Recommended By</span>
                                    </div>
                                    <div class="approved_by">
                                        <span>Approved By</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <script>
            function printDiv(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
        </script>
</body>
</html>
