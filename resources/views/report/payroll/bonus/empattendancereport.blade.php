@extends('layouts.master')
@section('title', 'Employee Attendance Bonus Report')
@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-md-12 portlets">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Attendance Bonus Report</b></div>
                <div class="panel-body">
                    <div class="col-md-12">
                        {{Form::open(array('url' => '/report/employee/attendance/bonus/pdf','method' => 'post'))}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Select Work Group</label>
                                <select class="form-control" name="att_bonus_work_group">
                                    <option value="Staff">Staff</option>
                                    <option value="Worker">Worker</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Select Month</label>
                                <div class="prepend-icon">
                                    <input type="text" name="year_bonus_month" id="year_bonus_month" class="form-control format_date" autocomplete="off" required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" id="year_bonus_report" class="btn btn-info">Generate</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#year_bonus_month').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm",
            showButtonPanel: true,
            currentText: "This Month",
            onChangeMonthYear: function (year, month, inst) {
                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
            },
            onClose: function(dateText, inst) {
                var month = $(".ui-datepicker-month :selected").val();
                var year = $(".ui-datepicker-year :selected").val();
                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
            }
        }).focus(function () {
            $(".ui-datepicker-calendar").hide();
        });
    });
</script>
@include('include.copyright')
@endsection
