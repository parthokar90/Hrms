@extends('layouts.master')
@section('title', 'Employee Bonus Report')
@section('content')
    <style>
        .pagination li a {
            color: #616161;
            font-size: 12px;
            background: deepskyblue;
            color: #ffffff;
        }
        .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
            background: darkgrey;
            border: none;
            color: #ffffff;
        }
    </style>
    <div class="page-content">
        <div class="col-md-8">
            {{ $data->appends(request()->except('page'))->links() }}
        </div>
        <div class="col-md-4">
            <button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')">Print</button>
        </div>

        <br>
        <br>
        <br>
        <br>
        <br>
        <div id="print_area">
            <h3 style="font-weight: bold" class="text-center">Attendance Bonus Report</h3>
            <h5 style="font-weight: bold" class="text-center">{{$companyInformation->company_name}}</h5>
            <p class="text-center">
               {{date('F-Y',strtotime($month))}}
            </p>
        <table id="salary_sheet_tbl" class="table table-bordered fontsss">
            <thead>
            <tr>
                <th>EmpID</th>
                <th>Name</th>
                <th>Work Group</th>
                <th>Bonus</th>
                <th>Absent</th>
                <th>Leave</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $bonus)
                <tr>
                    <td>{{$bonus->employeeId}}</td>
                    <td>{{$bonus->empFirstName}}</td>
                    <td>{{$bonus->work_group}}</td>
                    <td>
                         @if($bonus->absent <3 && $bonus->total=='')
                          {{$bonus->bonus_amount}}
                           @else
                             0
                         @endif
                    </td>
                    <td>{{$bonus->absent}}</td>
                    <td>
                        @if($bonus->total=='')
                            0
                            @else
                            {{$bonus->total}}
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
          </table>
        </div>
    </div>
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    @include('include.copyright')
@endsection