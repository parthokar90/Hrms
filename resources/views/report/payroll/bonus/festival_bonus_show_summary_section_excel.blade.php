<html>
    <body>
                                <table>
                                    <h2 style="font-weight: bold;text-transform: uppercase;font-size: 14px;" class="text-center">Festival Bonus Report</h2>

                                    <p style="font-size: 14px;" class="text-center">
                                      @if(isset($bonus_title)){{$bonus_title->bonus_title}} @endif
                                    </p>

                                    <p style="font-size:16px;font-weight: bold" class="text-center">
                                        @if(isset($bonus_title)) {{date('F-Y',strtotime($bonus_title->month))}} @endif
                                    </p>
                                    <h4 style="text-align: center;font-weight: bold;">
                                        {{$companyInformation->company_name}}
                                    </h4>
                                    <p style="font-size:14px;font-weight: bold" class="text-center">
                                        {{$companyInformation->company_address1}}
                                    </p>
                                    <thead>
                                    <tr>
                                        <th>Section</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $total_bonus=0; @endphp    
                                    @foreach($data as $item)
                                    @php $total_bonus+=$item->total; @endphp
                                        <tr>
                                            <td>{{$item->section_id}}</td>
                                            <td>{{$item->total}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                          <td>Total</td>
                                          <td>{{$total_bonus}}</td>
                                        </tr>
                                      </tfoot>
                                </table>
  </body>
</html> 