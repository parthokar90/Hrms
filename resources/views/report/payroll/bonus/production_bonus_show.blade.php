@extends('layouts.master')
@section('title', 'Employee Production Bonus Report')
@section('content')
    <style>
        .pagination li a {
            color: #616161;
            font-size: 12px;
            background: deepskyblue;
            color: #ffffff;
        }
        .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
            background: darkgrey;
            border: none;
            color: #ffffff;
        }
    </style>
<div class="page-content">
    <div class="row">
        <div class="col-md-12 portlets">
            <div class="panel panel-default">
                <div class="panel-heading"><h5><b>Production Bonus Report</b></h5></div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            {{ $data->appends(request()->except('page'))->links() }}
                        </div>
                        <div class="col-md-4">
                            <button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button>
                        </div>

                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div id="print_area">
                            <table id="year_bonus_display" class="table table-bordered">

                               <h2 style="font-weight: bold;text-transform: uppercase;font-size: 14px;" class="text-center">Production Bonus Report</h2>
                                <p style="font-size:16px;font-weight: bold" class="text-center">
                                        @if(!empty($month_name->created_at))
                                            {{date('F-Y',strtotime($month_name->created_at))}}
                                        @else
                                            No Data Found
                                        @endif
                                </p>
                                <h4 style="text-align: center;font-weight: bold;">
                                    {{$companyInformation->company_name}}
                                </h4>
                                <p style="font-size:14px;font-weight: bold" class="text-center">
                                    {{$companyInformation->company_address1}}
                                </p>
                                <thead>
                                <tr>
                                    <th>Employee Id</th>
                                    <th>Name</th>
                                    <th>Work Group</th>
                                    <th style="text-align: center" colspan="2">Bonus</th>
                                    <th>Percent</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $item)
                                <tr>
                                    <td>{{$item->employeeId}}</td>
                                    <td>{{$item->empFirstName}} {{$item->empLastName}} </td>
                                    <td>{{$item->work_group}}</td>
                                    <td>Amount:{{$item->production_amount}}</td>
                                    <td>Percent:{{$item->production_percent}}</td>
                                    <td>{{$item->production_percent_total}}</td>
                                </tr>
                                 @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
@include('include.copyright')
@endsection
