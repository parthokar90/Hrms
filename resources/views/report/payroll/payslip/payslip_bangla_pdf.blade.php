<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
        'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Payslip</title>
        <style>
            body{
            margin: 0;
            padding: 0;
            font-size: 13px;
        }
        .payslip{
            width: 100%;
            padding-top: 5px;
        }
        .payslip_page_one{
            background: #f9f9f99e;
            width: 48%;
            float: left;
            /*padding: 0px 15px;*/
        }
        .payslip_page_middle{
             width: 3%;
            float: left;
            padding: 0px 5px;
        }
        .company{
            text-align: center;
            margin: 10px 0;
        }
        .serial{
            float: left;
            padding: 1px 0px;
            width: 50%;
        }
        .date{
            width: 50%;
            float: right;
            text-align: right;
            padding: 1px 0px;
        }
        .month{
            width: 50%;
            float: left;
            padding: 1px 0px;
        }
        .section{
            width: 50%;
            float: right;
            text-align: right;
            padding: 1px 0px;
        }
        .empid{
            width: 50%;
            float: left;
            padding: 1px 0px;
        }
        .line{
            width: 50%;
            float: right;
            text-align: right;
            padding: 1px 0px;
        }
        .grade{
            width: 50%;
            float: left;
            padding: 1px 0px;
        }
        .designation{
            width: 50%;
            float: right;
            text-align: right;
            padding: 1px 0px;
        }
        .name{
            /*padding-bottom: 15px;*/
            width: 50%;
            float: left;
        }
        .joindate{
           /* padding-bottom: 15px;*/
            width: 50%;
            float: right;
            text-align: right;
        }
        .basic{
            border-top: 1px solid #999;
            width: 50%;
            float: left;
        }
        .ot_rate{
            border-top: 1px solid #999;
            width: 50%;
            float: right;
        }
        .house{
            border-top: 1px solid #999;
            width: 50%;
            float: left;
        }
        .transport{
            border-top: 1px solid #999;
            width: 50%;
            float: right;
        }
        .medical{
            border-top: 1px solid #999;
            width: 50%;
            float: left;
        }
        .overtime{
            border-top: 1px solid #999;
            width: 50%;
            float: right;
        }
        .gross{
            border-top: 1px solid #999;
            width: 50%;
            float: left;
        }
        .ot_amount{
            border-top: 1px solid #999;
            width: 50%;
            float: right;
        }
        .att_bonus{
            border-top: 1px solid #999;
            width: 50%;
            float: left;
        }
        .special_bonus{
            border-top: 1px solid #999;
            width: 50%;
            float: right;
        }
        .extra{
            border-top: 1px solid #999;
            width: 50%;
            float: left;
        }
        .food{
            border-top: 1px solid #999;
            width: 50%;
            float: right;
        }
        .gross_pay{
            border-top: 1px solid #999;
            width: 50%;
            float: left;
        }
        .leave{
            border-top: 1px solid #999;
            /* border-bottom: 1px solid #999; */
            width: 50%;
            float: right;
        }
        .deduction{
            border-top: 1px solid #999;
            width: 50%;
            float: left;
        }
        .work_days{
            border-top: 1px solid #999;
            width: 50%;
            float: right;
        }
        .net_wages{
            border-top: 1px solid #999;
            /*border-bottom: 1px solid #999;*/
            width: 50%;
            float: left;
        }
        .absent{
            border-top: 1px solid #999;
            /*border-bottom: 1px solid #999;*/
            width: 50%;
            float: right;
        }
        .authority{
            border-top: 1px solid #999;
            text-align: center;
            width: 33.33%;
            float: left;
            margin-top: 45px;
            margin-bottom: 10px;
        }
        .copy{
            text-align: center;
            width: 33.33%;
            float: left;
            margin-top: 45px;
            margin-bottom: 10px;
        }
        .recipient{
            border-top: 1px solid #999;
            text-align: center;
            width: 33.33%;
            float: left;
            margin-top: 45px;
            margin-bottom: 10px;
        }
        .pagination li a {
            color: #616161;
            font-size: 13px;
            background: deepskyblue;
            color: #ffffff;
        }
        .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
            background: darkgrey;
            border: none;
            color: #ffffff;
        }
        .all_col{
            width: 50%;
            text-align: left;
            padding-top: 1px;
            padding-bottom: 1px;
        }
        .all_col p{
            display: flex;
            font-size: 12px;
            margin-bottom: 0px;
        }
        .all_col p .span_1{
            width: 42%;
            margin-left: 2%;
        }
        .all_col p .span_2{
            width: 51%;

        }
        .net_wages.new_wages_1{
            border-top: 0px solid #999; 
            border-bottom: 0px solid #999;
        }
        .net_wages.new_wages_2{
             
            border-bottom: 1px solid #999;
        }
        .col_net_pay{
            width: 100%;
        }
        .col_net_pay p span.span_1{
            width: 21%;
            margin-left: 1%;
        }
        .emp_name{
            font-size: 11px;
        }
        @media  print {
            body{
                    font-size: 13px !important;
            }
            .hidden{
                display: none;
            }
            .pagebreak  { display: block; page-break-before: always; }
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
<div class="row hidden"  style="margin-top: 30px;" >
    <div class="col-md-3"></div>
    <div class="col-md-2">
    @if($data->isEmpty())
    @else
        <button class="btn btn-info" type="button"  onclick="printDiv('payslip_id')"> Print </button>
    @endif
    </div>
    <div class="col-md-7" style="float:right;">
        {{ $data->appends(request()->except('page'))->links() }}
    </div>
</div>
<div id="payslip_id">
    @php $order=0; @endphp
    @foreach($data as $payslip_report)
    @php
    $month_payslip=date('Y-m',strtotime($payslip_report->month));
    @endphp
        @php $order++ @endphp


        <div class="payslip">
            <div class="payslip_page_one" style="border: 1px solid #000000; margin-top: 25px;">
                <div class="company">
                    <h4 style="font-weight: bold;font-size: 18px;" class="text-center">
                        @if($companyInformation->bComName=='')
                        {{$companyInformation->company_name}}
                        @else
                        {{$companyInformation->bComName}}
                        @endif
                        </h4>
                        Pay Slip for the Month of {{date('F-Y',strtotime($payslip_report->month))}}
                </div>
                <div class="serial all_col">
                    <p> <span class="span_1">SL NO</span><span class="span_2">:&nbsp;&nbsp;{{$payslip_report->emp_id}}</span></p>
                </div>
                <div class="date all_col">
                    <p> <span class="span_1">তারিখ</span><span class="span_2">:&nbsp;&nbsp;{{ en2bnNumber(date('d-m-Y'))}}</span></p>
                    
                </div>
                 <div class="empid all_col">
                    <p> <span class="span_1">আইডি নং</span><span class="span_2">:&nbsp;&nbsp;@if($payslip_report->employeeBnId=='')
                        {{en2bnNumber($payslip_report->employeeId)}}
                    @else
                        {{en2bnNumber($payslip_report->employeeBnId)}}
                    @endif</span></p>
                                        
                </div>
                <div class="grade all_col">
                    <p> <span class="span_1">গ্রেড</span><span class="span_2">:&nbsp;&nbsp;{{$payslip_report->grade_name}}</span></p>
                   
                </div>
                <div class="joindate all_col" >
                    <p> <span class="span_1">যোগদানের তারিখ</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(date("d-m-Y", strtotime($payslip_report->empJoiningDate)))}}</span></p>
                </div>


                <div class="designation all_col">
                    <p> <span class="span_1">পদবি</span><span class="span_2">:&nbsp;&nbsp; @if($payslip_report->designationBangla=='')
                        {{$payslip_report->designation}}
                    @else
                        {{$payslip_report->designationBangla}}
                    @endif</span></p>
                 
                </div>
                
                <div class="name all_col" style="margin-bottom: 10px;">
                    <p> <span class="span_1">নাম</span><span class="span_2 emp_name">:&nbsp;&nbsp; @if($payslip_report->empBnFullName=='')
                        {{$payslip_report->empFirstName}}
                    @else
                        {{$payslip_report->empBnFullName}}
                    @endif</span></p>                       
                </div>
                <div class="net_wages new_wages_1 all_col" style="margin-bottom: 10px;">
                <p> <span class="span_1">সেকশন​</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber($payslip_report->empSection)}}</span></p>
                                                               
                </div>

                <div class="basic all_col">
                    <p> <span class="span_1">মুল বেতন</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->basic_salary,0))}}</span></p>
                    
                </div>
                <div class="ot_rate all_col">
                    <p> <span class="span_1">ওভারটাইম দর</span><span class="span_2">:&nbsp;&nbsp; @if($payslip_report->work_group=='Staff')
                        @else
                            &nbsp;&nbsp;{{en2bnNumber(round($payslip_report->overtime_rate,2))}}
                        @endif</span></p>
                </div>
                <div class="house all_col">
                    <p> <span class="span_1">বাড়ি ভাড়া</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->house_rant,0))}}</span></p>
                     
                </div>
                <div class="overtime all_col">
                    <p> <span class="span_1">ওভার টাইম ঘন্টা</span><span class="span_2">:&nbsp;&nbsp;@if($payslip_report->overtime==''){{en2bnNumber(0)}} @else {{en2bnNumber($payslip_report->overtime)}}  @endif</span></p>
                </div>
                <div class="medical all_col">
                    <p> <span class="span_1">চিকিৎসা ভাতা</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->medical,0))}}</span></p>
                
                </div>
                <div class="ot_amount all_col">
                    <p> <span class="span_1">ওভার টাইম টাকা</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->overtime_amount))}}</span></p>
                
                </div>
                <div class="deduction all_col">
                    <p> <span class="span_1">যাতায়াত ভাতা</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->transport,0))}}</span></p>
            
                </div>

                <div class="transport all_col">
                <p> <span class="span_1">প্রদান যোগ্য দিন</span><span class="span_2">:&nbsp;&nbsp;&nbsp; @if($payslip_report->leave==1) @php $payable=$payslip_report->weekend+$payslip_report->holiday+$payslip_report->present; @endphp @else @php $payable=$payslip_report->weekend+$payslip_report->holiday+$payslip_report->present+$payslip_report->total; @endphp  @endif {{en2bnNumber($payable)}}</span></p>
                     
                </div>
                <div class="absent all_col">
                    <p> <span class="span_1">অনুপস্থিত</span><span class="span_2">:&nbsp;&nbsp;@if($payslip_report->absent=='') {{en2bnNumber(0)}} @else {{en2bnNumber($payslip_report->absent)}} @endif</span></p>
                </div>
                
                
                <div class="food all_col">
                    <p> <span class="span_1">খাদ্য ভাতা</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->food,0))}}</span></p>
                    
                </div>
                
                <div class="gross all_col">
                    <p> <span class="span_1">মোট বেতন</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->gross,0))}}</span></p>
                </div>
                <div class="deduction all_col">
                    <p> <span class="span_1">কর্তন</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->absent_deduction_amount,0))}}</span></p>  
                </div>
                <div class="gross_pay all_col">
                    <p> <span class="span_1">মোট প্রদেয় টাকা</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber($payslip_report->gross_pay)}}</span></p>
                </div>
                <div class="att_bonus all_col">
                <p> <span class="span_1">বকেয়া</span><span class="span_2">:&nbsp;&nbsp;@if($payslip_report->advanced_deduction=='') {{en2bnNumber(0)}} @else {{en2bnNumber($payslip_report->advanced_deduction)}} @endif</span></p>                                                               
                </div>
                <div class="net_wages all_col">
                        <p> <span class="span_1">হাজিরা বোনাস</span><span class="span_2">:&nbsp;&nbsp; {{en2bnNumber($payslip_report->attendance_bonus)}}</span></p>
                    </div>
                    <div class=" special_bonus all_col">
                        <p> <span class="span_1">বিলম্ব হাজিরা দিন</span><span class="span_2">:&nbsp;&nbsp; @if($payslip_report->total_late=='') 0 @else {{en2bnNumber($payslip_report->total_late)}} @endif </span></p>
                                                                   
                    </div>
                

                    <div class="deduction all_col">
                        <p> <span class="span_1">স্ট্যাম্প</span><span class="span_2">:&nbsp;&nbsp; @if($payslip_report->payment_mode=='Bank' || $payslip_report->payment_mode=='bKash') ০ @else ১০ @endif   </span></p>  
                    </div>

          
                <div class="leave all_col">
                    <p> <span class="span_1">ছুটি</span><span class="span_2">:&nbsp;&nbsp; @if($payslip_report->total=='') 0 @else {{en2bnNumber($payslip_report->total)}} @endif</span></p>
                </div>
                <div class="net_wages all_col new_wages_2 col_net_pay">
                    <p> <span class="span_1">সর্বসাকুল্যে বেতন</span><span class="span_2">:&nbsp;&nbsp;
                        @php
                        $net=$payslip_report->net_amount;
                        $total=$net;
                       @endphp
                    {{en2bnNumber(round($total))}}
                </span></p>
                                                               
                </div>
                
                <div class="authority ">
                    কর্তৃপক্ষ
                </div>
                <div class="copy">
                  
                </div>
                <div class="recipient">
                    গ্রহীতা
                </div>
            </div>
    
            <div class="payslip_page_middle"  style="border: 1px solid #fff;"></div>
            <div class="payslip_page_one" style="border: 1px solid #000000; margin-top: 25px;">
                <div class="company">
                    <h4 style="font-weight: bold;font-size: 18px;" class="text-center">
                        @if($companyInformation->bComName=='')
                        {{$companyInformation->company_name}}
                        @else
                        {{$companyInformation->bComName}}
                        @endif
                        </h4>
                        Pay Slip for the Month of {{date('F-Y',strtotime($payslip_report->month))}}
                </div>
                <div class="serial all_col">
                    <p> <span class="span_1">SL NO</span><span class="span_2">:&nbsp;&nbsp;{{$payslip_report->emp_id}}</span></p>
                </div>
                <div class="date all_col">
                    <p> <span class="span_1">তারিখ</span><span class="span_2">:&nbsp;&nbsp;{{ en2bnNumber(date('d-m-Y'))}}</span></p>
                    
                </div>
                 <div class="empid all_col">
                    <p> <span class="span_1">আইডি নং</span><span class="span_2">:&nbsp;&nbsp;@if($payslip_report->employeeBnId=='')
                        {{en2bnNumber($payslip_report->employeeId)}}
                    @else
                        {{en2bnNumber($payslip_report->employeeBnId)}}
                    @endif</span></p>
                                        
                </div>
                <div class="grade all_col">
                    <p> <span class="span_1">গ্রেড</span><span class="span_2">:&nbsp;&nbsp;{{$payslip_report->grade_name}}</span></p>
                   
                </div>
                <div class="joindate all_col" >
                    <p> <span class="span_1">যোগদানের তারিখ</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(date("d-m-Y", strtotime($payslip_report->empJoiningDate)))}}</span></p>
                </div>


                <div class="designation all_col">
                    <p> <span class="span_1">পদবি</span><span class="span_2">:&nbsp;&nbsp; @if($payslip_report->designationBangla=='')
                        {{$payslip_report->designation}}
                    @else
                        {{$payslip_report->designationBangla}}
                    @endif</span></p>
                 
                </div>
                
                <div class="name all_col" style="margin-bottom: 10px;">
                    <p> <span class="span_1">নাম</span><span class="span_2 emp_name">:&nbsp;&nbsp; @if($payslip_report->empBnFullName=='')
                        {{$payslip_report->empFirstName}}
                    @else
                        {{$payslip_report->empBnFullName}}
                    @endif</span></p>                       
                </div>
                <div class="net_wages new_wages_1 all_col" style="margin-bottom: 10px;">
                <p> <span class="span_1">সেকশন​</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber($payslip_report->empSection)}}</span></p>
                                                               
                </div>

                <div class="basic all_col">
                    <p> <span class="span_1">মুল বেতন</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->basic_salary,0))}}</span></p>
                    
                </div>
                <div class="ot_rate all_col">
                    <p> <span class="span_1">ওভারটাইম দর</span><span class="span_2">:&nbsp;&nbsp; @if($payslip_report->work_group=='Staff')
                        @else
                            &nbsp;&nbsp;{{en2bnNumber(round($payslip_report->overtime_rate,2))}}
                        @endif</span></p>
                </div>
                <div class="house all_col">
                    <p> <span class="span_1">বাড়ি ভাড়া</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->house_rant,0))}}</span></p>
                     
                </div>
                <div class="overtime all_col">
                    <p> <span class="span_1">ওভার টাইম ঘন্টা</span><span class="span_2">:&nbsp;&nbsp;@if($payslip_report->overtime==''){{en2bnNumber(0)}} @else {{en2bnNumber($payslip_report->overtime)}}  @endif</span></p>
                </div>
                <div class="medical all_col">
                    <p> <span class="span_1">চিকিৎসা ভাতা</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->medical,0))}}</span></p>
                
                </div>
                <div class="ot_amount all_col">
                    <p> <span class="span_1">ওভার টাইম টাকা</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->overtime_amount))}}</span></p>
                
                </div>
                <div class="deduction all_col">
                    <p> <span class="span_1">যাতায়াত ভাতা</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->transport,0))}}</span></p>
            
                </div>

                <div class="transport all_col">
                <p> <span class="span_1">প্রদান যোগ্য দিন</span><span class="span_2">:&nbsp;&nbsp;&nbsp; @if($payslip_report->leave==1) @php $payable=$payslip_report->weekend+$payslip_report->holiday+$payslip_report->present; @endphp @else @php $payable=$payslip_report->weekend+$payslip_report->holiday+$payslip_report->present+$payslip_report->total; @endphp  @endif {{en2bnNumber($payable)}}</span></p>
                     
                </div>
                <div class="absent all_col">
                    <p> <span class="span_1">অনুপস্থিত</span><span class="span_2">:&nbsp;&nbsp;@if($payslip_report->absent=='') {{en2bnNumber(0)}} @else {{en2bnNumber($payslip_report->absent)}} @endif</span></p>
                </div>
                
                
                <div class="food all_col">
                    <p> <span class="span_1">খাদ্য ভাতা</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->food,0))}}</span></p>
                    
                </div>
                
                <div class="gross all_col">
                    <p> <span class="span_1">মোট বেতন</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->gross,0))}}</span></p>
                </div>
                <div class="deduction all_col">
                    <p> <span class="span_1">কর্তন</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber(round($payslip_report->absent_deduction_amount,0))}}</span></p>  
                </div>
                <div class="gross_pay all_col">
                    <p> <span class="span_1">মোট প্রদেয় টাকা</span><span class="span_2">:&nbsp;&nbsp;{{en2bnNumber($payslip_report->gross_pay)}}</span></p>
                </div>
                <div class="att_bonus all_col">
                <p> <span class="span_1">বকেয়া</span><span class="span_2">:&nbsp;&nbsp;@if($payslip_report->advanced_deduction=='') {{en2bnNumber(0)}} @else {{en2bnNumber($payslip_report->advanced_deduction)}} @endif</span></p>                                                               
                </div>
                <div class="net_wages all_col">
                    <p> <span class="span_1">হাজিরা বোনাস</span><span class="span_2">:&nbsp;&nbsp; {{en2bnNumber($payslip_report->attendance_bonus)}}</span></p>
                </div>
                <div class=" special_bonus all_col">
                    <p> <span class="span_1">বিলম্ব হাজিরা দিন</span><span class="span_2">:&nbsp;&nbsp; @if($payslip_report->total_late=='') 0 @else {{en2bnNumber($payslip_report->total_late)}} @endif </span></p>
                                                               
                </div>
                <div class="deduction all_col">
                    <p> <span class="span_1">স্ট্যাম্প</span><span class="span_2">:&nbsp;&nbsp; @if($payslip_report->payment_mode=='Bank' || $payslip_report->payment_mode=='bKash') ০ @else ১০ @endif   </span></p>  
                </div>
                
                <div class="leave all_col">
                    <p> <span class="span_1">ছুটি</span><span class="span_2">:&nbsp;&nbsp; @if($payslip_report->total=='') 0 @else {{en2bnNumber($payslip_report->total)}} @endif</span></p>
                </div>
                <div class="net_wages all_col new_wages_2 col_net_pay">
                    <p> <span class="span_1">সর্বসাকুল্যে বেতন</span><span class="span_2">:&nbsp;&nbsp;
                        @php
                        $net=$payslip_report->net_amount;
                        $total=$net;
                       @endphp
                    {{en2bnNumber(round($total))}}
                </span></p>
                                                               
                </div>
                
                <div class="authority ">
                    কর্তৃপক্ষ
                </div>
                <div class="copy">
                  
                </div>
                <div class="recipient">
                    গ্রহীতা
                </div>
            </div>
        </div>
        @if($order%3==0)
            <div class="pagebreak"></div>
        @endif
    @endforeach
</div>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
</body>
</html>