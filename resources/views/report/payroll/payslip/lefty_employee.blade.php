@extends('layouts.master')
@section('title', 'Lefty Employee Payslip')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading">Lefty Employee Payslip</div>
                    <div class="panel-body">

                     <div class="col-md-12">
                        <div class="form-group">
                                <label>Select Type</label>
                                <select id="ot_type" name="ot_type" class="form-control">
                                         <option value="">Select</option>
                                           <option value="1">Department Wise</option>
                                           <option value="2">Section Wise</option>
                                           <option value="3">Employee Wise</option>
                                           <option value="4">Month Wise</option>
                                 </select>    
                           </div>
                       </div>

                     <div class="col-md-12" id="department_type" style="display:none">
                            {{Form::open(array('url' => 'report/payslip/lefty/employee/show','method' => 'post','target' => '_blank'))}}
                           <div class="col-md-4">
                            <div class="form-group">
                            <label>Select Department</label>
                            <select class="form-control" name="department_id" data-search="true">
                                    @foreach($department as $departments)
                                       <option value="{{$departments->dept_id}}">{{$departments->departmentName}}</option>
                                    @endforeach
                            </select>
                            </div>
                           </div>
                           <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Select Month</label>
                                    <div class="prepend-icon">
                                        <input type="text" autocomplete="off" name="payslip_generate_report" id="payslip_generate_report" class="form-control format_date" required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label>Select Language</label>
                                    <select class="form-control" name="payslip_option">
                                        <option value="english">English</option>
                                        <option value="bangla">Bengali</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="type" value="dept_wise_data">
                            <div class="col-md-12">
                                    <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Generate</button>
                                    {{-- <button type="submit"  name="salary_month_wise_excel" class="btn btn-success">Export Excel</button> --}}
                                    {{--<button class="btn  btn-info" onclick="printDiv('print_area')" style="display: none" type="button" id="pdf_btn" class="btn btn-info"> <i class="fa fa-print"></i> Print </button>--}}
                                </div>
                             {{ Form::close() }}
                       </div>


                       
                     <div class="col-md-12" id="section_type" style="display:none">
                            {{Form::open(array('url' => 'report/payslip/lefty/employee/show','method' => 'post','target' => '_blank'))}}
                           <div class="col-md-4">
                            <div class="form-group">
                            <label>Select Section</label>
                            <select class="form-control" name="section_id" data-search="true">
                                    @foreach($section as $sections)
                                       <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                    @endforeach
                            </select>
                            </div>
                           </div>
                           <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Select Month</label>
                                    <div class="prepend-icon">
                                        <input type="text" autocomplete="off" name="payslip_generate_report" id="section_report" class="form-control format_date" required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label>Select Language</label>
                                    <select class="form-control" name="payslip_option">
                                        <option value="english">English</option>
                                        <option value="bangla">Bengali</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="type" value="section_wise_data">
                            <div class="col-md-12">
                                    <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Generate</button>
                                    {{-- <button type="submit"  name="salary_month_wise_excel" class="btn btn-success">Export Excel</button> --}}
                                    {{--<button class="btn  btn-info" onclick="printDiv('print_area')" style="display: none" type="button" id="pdf_btn" class="btn btn-info"> <i class="fa fa-print"></i> Print </button>--}}
                                </div>
                             {{ Form::close() }}
                       </div>


                       <div class="col-md-12" id="employee_type" style="display:none">
                            {{Form::open(array('url' => 'report/payslip/lefty/employee/show','method' => 'post','target' => '_blank'))}}
                           <div class="col-md-4">
                            <div class="form-group">
                            <label>Select Employee</label>
                            <select class="form-control" name="emp_id" data-search="true">
                                    @foreach($employee as $employees)
                            <option value="{{$employees->id}}">({{$employees->employeeId}}) {{$employees->empFirstName}} {{$employees->empLastName}}</option>
                                    @endforeach
                            </select>
                            </div>
                           </div>
                           <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Select Month</label>
                                    <div class="prepend-icon">
                                        <input type="text" autocomplete="off" name="payslip_generate_report" id="emp_report" class="form-control format_date" required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label>Select Language</label>
                                    <select class="form-control" name="payslip_option">
                                        <option value="english">English</option>
                                        <option value="bangla">Bengali</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="type" value="emp_wise_data">
                            <div class="col-md-12">
                                    <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Generate</button>
                                    {{-- <button type="submit"  name="salary_month_wise_excel" class="btn btn-success">Export Excel</button> --}}
                                    {{--<button class="btn  btn-info" onclick="printDiv('print_area')" style="display: none" type="button" id="pdf_btn" class="btn btn-info"> <i class="fa fa-print"></i> Print </button>--}}
                                </div>
                             {{ Form::close() }}
                       </div>

                       <div class="col-md-12" id="month_wise" style="display:none">
                            {{Form::open(array('url' => 'report/payslip/lefty/employee/show','method' => 'post','target' => '_blank'))}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Select Month</label>
                                <div class="prepend-icon">
                                    <input type="text" autocomplete="off" name="payslip_generate_report" id="month_report" class="form-control format_date" required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Select Language</label>
                                <select class="form-control" name="payslip_option">
                                    <option value="english">English</option>
                                    <option value="bangla">Bengali</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="type" value="month_wise_data">
                        <div class="col-md-12">
                                <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Generate</button>
                                {{-- <button type="submit"  name="salary_month_wise_excel" class="btn btn-success">Export Excel</button> --}}
                                {{--<button class="btn  btn-info" onclick="printDiv('print_area')" style="display: none" type="button" id="pdf_btn" class="btn btn-info"> <i class="fa fa-print"></i> Print </button>--}}
                            </div>
                        {{ Form::close() }}
                       </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        $('#payslip_generate_report').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "yy-mm",
                            showButtonPanel: true,
                            currentText: "This Month",
                            onChangeMonthYear: function (year, month, inst) {
                                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                            },
                            onClose: function(dateText, inst) {
                                var month = $(".ui-datepicker-month :selected").val();
                                var year = $(".ui-datepicker-year :selected").val();
                                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                            }
                        }).focus(function () {
                            $(".ui-datepicker-calendar").hide();
                        });


                        $('#section_report').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "yy-mm",
                            showButtonPanel: true,
                            currentText: "This Month",
                            onChangeMonthYear: function (year, month, inst) {
                                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                            },
                            onClose: function(dateText, inst) {
                                var month = $(".ui-datepicker-month :selected").val();
                                var year = $(".ui-datepicker-year :selected").val();
                                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                            }
                        }).focus(function () {
                            $(".ui-datepicker-calendar").hide();
                        });

                        $('#emp_report').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "yy-mm",
                            showButtonPanel: true,
                            currentText: "This Month",
                            onChangeMonthYear: function (year, month, inst) {
                                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                            },
                            onClose: function(dateText, inst) {
                                var month = $(".ui-datepicker-month :selected").val();
                                var year = $(".ui-datepicker-year :selected").val();
                                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                            }
                        }).focus(function () {
                            $(".ui-datepicker-calendar").hide();
                        });

                        $('#month_report').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "yy-mm",
                            showButtonPanel: true,
                            currentText: "This Month",
                            onChangeMonthYear: function (year, month, inst) {
                                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                            },
                            onClose: function(dateText, inst) {
                                var month = $(".ui-datepicker-month :selected").val();
                                var year = $(".ui-datepicker-year :selected").val();
                                $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                            }
                        }).focus(function () {
                            $(".ui-datepicker-calendar").hide();
                        });


                    
                        $("#ot_type").change(function(){
                        var select_type= $("#ot_type").val();
                        if(select_type==1){
                        $("#department_type").show();
                        $("#month_wise").hide();
                        $("#section_type").hide();
                        $("#employee_type").hide();
                        }

                        if(select_type==2){
                        $("#section_type").show();
                        $("#month_wise").hide();
                        $("#department_type").hide();
                        $("#employee_type").hide();
                        }

                        if(select_type==3){
                        $("#employee_type").show();
                        $("#month_wise").hide();
                        $("#department_type").hide();
                        $("#section_type").hide();
                        }

                        if(select_type==4){
                        $("#month_wise").show();
                        $("#employee_type").hide();
                        $("#department_type").hide();
                        $("#section_type").hide();
                        }
                    });

                 });
                </script>
    @include('include.copyright')
@endsection
