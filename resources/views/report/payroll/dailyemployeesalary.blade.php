@extends('layouts.master')
@section('title', 'Daily Employee Salary Report')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">Employee Daily Salary Report</div>
            <div class="panel-body">
                {{Form::open(array('url' => 'report/employee/daily/salary/show','method' => 'post'))}}
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="form-label">Select Date</label>
                        <div class="prepend-icon">
                            <input type="text" name="daily_salary_report" id="daily_salary_report" class="form-control date-picker">
                            <i class="icon-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <button type="submit" name="" class="btn btn-success" id="daily_salary_sheet_generate">Generate Report</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#daily_salary_sheet_generate').on('click', function() {
                var monthselect=$("#daily_salary_report").val();
                if(monthselect==''){
                    alert('please select date');
                    return false;
                }
            });
        });
    </script>
    @include('include.copyright')
@endsection