
<!DOCTYPE html>
<html>
<head>
    <title>Extra Overtime lefty </title>
</head>
<body>
        {{-- <button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button> --}}
<div id="print_area">
        <div class="main_div">
            <div class="salary_table">
                <table class="table_1">
                    <thead>
                        <tr style="text-align: center;">
                            <tr><td></td><td colspan="16" align="center"><h4><b>{{$companyInformation->company_name}}</b></h4></td></tr>
                            <tr><td></td><td colspan="16" align="center"><p style=""> <b>
                                @if(isset($department))
                                    Department:{{ $department->departmentName}}
                                @endif
    
    
                                @if(isset($section))
                                    Section:{{ $section->empSection}}
                                @endif 
                            </b> </p></td></tr>
                                <tr><td></td><td colspan="16" align="center"><p><b>Lefty Extra O.T sheet of   @if(isset($start_month)) ({{date('m/d/Y',strtotime($start_month))}} to {{date('m/d/Y',strtotime($end_month))}}) @else  {{$request->start_date}} to {{$request->end_date}}  @endif</b></p></td></tr>
                        </tr>
                        <tr></tr>
                        <tr>
                                <th>Sl No</th>
                                <th>Emp ID</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Basic</th>
                                <th>Gross</th>
                                <th>Overtime (Hr)</th>
                                <th>Rate</th>
                                <th>Amount</th>
                                @if($p_select==1)
                                <th>Payment Mode</th>
                                <th>Account No</th>
                                @endif
                                <th>Signature</th>
                        </tr>
                    </thead>
                    <tbody>  
                            @php $order=0; $ot_hour=0; $total_ot=0; $total_amount=0; $basic=0; $gross=0; $ot_rates=0; @endphp
                            @foreach($extraotsss as $extra)
                                @php

                                    $ot_rate=$extra->basic_salary/104;
                                    $ac_rate=round($ot_rate,2);
                                    $total_hour=$extra->total_hour+$extra->total_minute+$extra->friday_hour;
                                    if($total_hour==0){
                                     continue;
                                    }
                                    $order++;
                                    $total_ot+=$total_hour;
                                    $ot_rates+=$ac_rate;
                                @endphp
                                    <tr>
                                    <td>{{$order}}</td>
                                    <td>{{$extra->employee_id}}</td>
                                    <td>{{$extra->name}}</td>
                                    <td>{{$extra->designation}}</td>
                                    <td>{{$extra->basic_salary}}</td>
                                    <td>{{$extra->total_salary}}</td>
                                    <td>{{$total_hour}}</td>
                                    <td>{{$ac_rate}}</td>
                                      <td>
                                            @php
                                                $ot_amount=$ac_rate*$total_hour;
                                                $total_amount+=$ot_amount;
                                                $basic+=$extra->basic_salary;
                                                $gross+=$extra->total_salary;
                                            @endphp
                                            {{round($ot_amount,0)}}
                                        </td>
                                        @if($p_select==1)
                                        <td>{{$extra->payment_mode}}</td>
                                        <td>{{$extra->bank_account}}</td>
                                        @endif
                                    <td></td> 
                                    </tr>
                                    @endforeach
                                    <tr>
                                            <td>Total:</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{$basic}}</td>
                                            <td>{{$gross}}</td>
                                            <td>{{$total_ot}}</td>
                                            <td>{{round($ot_rates,2)}}</td>
                                            <td>{{round($total_amount)}}</td>
                                            <td></td>
                                        </tr>
                    <tr></tr>
                    <tr></tr>
                       <tr>
                            <td colspan="1"></td>
                            <td colspan="1"><p>Prepared By</p></td>
                            <td colspan="1"><p>Audited By</p></td>
                            <td colspan="3"><p>Recommended By</p></td>
                            <td colspan="4"><p>Director</p></td>
                        </tr>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

