
<!DOCTYPE html>
<html>
<head>
    <title>Extra Overtime Suymmary</title>
</head>
<body>
        {{-- <button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button> --}}
<div id="print_area">
        <div class="main_div">
            <div class="salary_table">
                <table class="table_1">
                    <thead>
                        <tr style="text-align: center;">
                            <tr><td></td><td colspan="16" align="center"><h4><b>{{$companyInformation->company_name}}</b></h4></td></tr>
                            <tr><td></td><td colspan="16" align="center"><p><b>Overtime Summary from {{$start_one}} to {{$end_one}}</b></p></p></td></tr>
                        </tr>
                        <tr></tr>
                        <tr>
                                <th>SL</th>
                                <th>DEPARTMENT</th>
                                <th>MAN POWER</th>
                                <th>BASIC</th>
                                <th>GROSS</th>
                                <th>OVERTIME (HOUR)</th>
                                <th>AMOUNT</th>
                                <th>REMARKS</th>
                        </tr>
                    </thead>
                    <tbody>  
                            @php $order=0; $basic=0; $gross=0;$overtime=0; $worker=0; $total_amounts=0; $man_power=0; @endphp
                            @foreach($total as $totals)
                            @php 
                            $order++;
                            $data=DB::SELECT("SELECT SUM(total_present) as t_basic,SUM(total_absent) as t_gross FROM `tb_total_present` WHERE emp_id=$totals->department");
                            $worker=DB::table('tb_total_present')->where('emp_id',$totals->department)->count();
                            @endphp
                            @php  $overtime+=$totals->total_hour+$totals->friday_hour+$totals->total_minute @endphp
                                    <tr>
                                    <td>{{$order}}</td>
                                    <td>{{$totals->departmentName}}</td>
                                    <td style="text-align:left">    
                                            {{$worker}}
                                            @php $man_power+=$worker; @endphp
                                    </td>
                                        <td style="text-align:left">
                                         @foreach( $data as $test)
                                                {{$test->t_basic}}
                                               @php $basic+=$test->t_basic; @endphp  
                                         @endforeach
                                        </td>
                                        <td style="text-align:left">
                                         @foreach( $data as $test)
                                                {{$test->t_gross}}
                                                @php $gross+=$test->t_gross; @endphp  
                                         @endforeach
                                        </td>
                                        <td style="text-align:left">{{$totals->total_hour+$totals->friday_hour+$totals->total_minute}}</td>

                                    <td style="text-align:left">{{round($totals->total_amount)}} @php $total_amounts+=$totals->total_amount; @endphp </td>
                                    <td></td> 
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="2">Grand Total:</td>
                                        <td style="text-align:left">{{$man_power}}</td>
                                        <td style="text-align:left">{{$basic}}</td>
                                        <td style="text-align:left">{{$gross}}</td>
                                        <td style="text-align:left">{{$overtime}}</td>
                                        <td style="text-align:left">{{round($total_amounts)}}</td>
                                        <td></td>
                                    </tr>

                    <tr></tr>
                    <tr></tr>
                       <tr>
                            <td colspan="1"></td>
                            <td colspan="1"><p>Prepared By</p></td>
                            <td colspan="1"><p>Audited By</p></td>
                            <td colspan="3"><p>Recommended By</p></td>
                            <td colspan="4"><p>Approved By</p></td>
                        </tr>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

