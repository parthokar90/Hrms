@extends('layouts.master')
@section('title', 'Overtime Summery report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">

                    <div class="modal fade" data-keyboard="false" data-backdrop="static" id="modelWindow" role="dialog">
                            <div class="modal-dialog modal-sm vertical-align-center">
                                <div class="modal-content">
                                    <div class="modal-body load_image">
                                    <img src="{{asset('hrm_script/images/process_two.gif')}}" width="100%" height="150px">
                                    </div>
                                </div>
                            </div>
                        </div>


                <div class="panel panel-default">
                    <div class="panel-heading"><b>Overtime Summery report</b></div>
                    <div class="panel-body">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Select Type</label>
                                    <select class="form-control" id="ot_type">
                                        <option value="">Select</option>
                                        <option value="1">Department Wise</option>
                                        <option value="2">Section Wise</option>
                                    </select>    
                                </div>
                            </div>


                        <div class="col-md-12" id="department_type" style="display:none">
                            {{Form::open(array('action' => 'datesalarycontroller@overtime_summery_hr_show','method' => 'post'))}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Select Department</label>
                                    <select class="form-control" name="department_id" data-search="true" required>
                                        <option value="all">All</option>
                                        @foreach($department as $departments)
                                           <option value="{{$departments->id}}">{{$departments->departmentName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


							
                            <div class="form-group col-md-4">
                                <label class="form-label">Start Date</label>
                                <div class="prepend-icon">
                                    <input id="dept_start" type="text" name="start"  class="form-control date-picker" autocomplete="off" required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label class="form-label">End Date</label>
                                <div class="prepend-icon">
                                    <input id="dept_end" type="text" name="end"  class="form-control date-picker" autocomplete="off" required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                            <input type="hidden" name="type" value="dept_type_summary">
                            <div class="col-md-12">
                               <button type="submit" id="year_bonus_report" class="btn btn-primary">Generate</button>
                               <button type="submit" id="year_bonus_report_excel" name="dept_excel" value="dept_excel" class="btn btn-success">Generate Excel</button>
                            </div>
                            {{ Form::close() }}
                        </div>



                        <div class="col-md-12" id="section_type" style="display:none">
                                {{Form::open(array('action' => 'datesalarycontroller@overtime_summery_hr_show','method' => 'post'))}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Select Section</label>
                                        <select class="form-control" name="section_id" data-search="true" required>
                                            <option value="all">All</option>
                                            @foreach($section as $sections)
                                               <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label class="form-label">Start Date</label>
                                    <div class="prepend-icon">
                                        <input id="section_start" type="text" name="start"  class="form-control date-picker" autocomplete="off" required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="form-label">End Date</label>
                                    <div class="prepend-icon">
                                        <input id="section_end" type="text" name="end"  class="form-control date-picker" autocomplete="off" required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>

                                <input type="hidden" name="type" value="section_type_summary">
                                <div class="col-md-12">
                                   <button type="submit" id="year_bonus_report_section" class="btn btn-primary">Generate</button>
                                   <button type="submit" id="year_bonus_report_excel_two" name="section_excel" value="section_excel" class="btn btn-success">Generate Excel</button>
                                </div>
                                {{ Form::close() }}
                          </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#ot_type").change(function(){
                var select_type= $("#ot_type").val();
                if(select_type==1){
                  $("#department_type").show();
                  $("#section_type").hide();
                }
                if(select_type==2){
                  $("#section_type").show();
                  $("#department_type").hide();
                }
              });

             $('#year_bonus_report').click(function(){
                 var dept_start=$("#dept_start").val();
                 var dept_end=$("#dept_end").val();
                 if(dept_start==''){
                   alert('Select Start Date');
                   return false;
                 }else if(dept_end==''){
                   alert('Select End Date');
                   return false;
                 }
                 else{
                    $('#modelWindow').modal('show');
                 }
              });

              $('#year_bonus_report_section').click(function(){
                var section_start=$("#section_start").val();
                 var section_end=$("#section_end").val();
                 if(section_start==''){
                   alert('Select Start Date');
                   return false;
                 }else if(section_end==''){
                   alert('Select End Date');
                   return false;
                 }
                 else{
                    $('#modelWindow').modal('show');
                 }
              });
        });
    </script>
    @include('include.copyright')
@endsection
