<style>

    table{
        border-collapse: collapse;
    }
    thead,
    tr,
    td,
    th {
        border: 1px solid #757575;

    }

    .heading-top {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .heading-top th {
        font-size: 13px;
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .heading-top-middle {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .heading-top-middle th {
        font-size: 13px;
    text-align: center;
    padding-top: 2px;
    padding-bottom: 3px;
    padding-left: 10px;
    padding-right: 10px;
    }

    .table-content td {
        font-size: 11px;
    }


    .heading-top th:first-child{
        border-right:0px;
    }
    .heading-top th:nth-child(2){
        border-left:0px;
        border-right:0px;
    }
    .heading-top th:nth-child(3 ){
        border-left:0px;
    }
    tr.heading-top th {
        font-size: 11px;
    }
    .heading-bottom {
        /*border: 0px;*/
        font-size: 11px;
        /*border:none;*/
    }
    .heading-bottom td{
        /*border: 0px;*/
        /*border-color: #fff;*/
        padding-top: 2px;
        padding-bottom: 2px;
        font-weight: bold;
    }

    .table_info_data tr td{
        padding-left: 10px;
        padding-right: 10px;

    }
    .table_info_data tr{
        height:23px;
    }
    tr.heading_top_1{
        border:0px;
    }
    tr.heading_top_1 th{
        border:0px;
    }
    .table_head{

        border-top: 0px;
        border-left:0px;

    }
    .footer_part{
        opacity: 0;
        display: none;
    }
    tfoot{
        opacity: 1;
    }
    .footer-space{
        padding-top: 20px;
    }

    .footer-part p{
        border-top: 1px solid #8a8585;
        text-align: center;
        padding-top: 10px;
        padding-left: 10px;
        padding-right: 10px;
        margin-top: 75px;
        width: 190px;
    }
    .heading_top_1 p {
        margin-top: 3px;
        margin-bottom: 3px;
    }
    .top_h {
        text-align: center;
    }
    .n_h{
        width:130px;
    }
    .d_h{
        width: 110px;
    }
    th.n_h_1{
        width: 130px;
    }
    th.d_h_1{
        width:110px;
    }
    a{
        text-decoration: none;
        color:#000000;
    }
    .authority div{
        width: 18%;
        margin-right: 5%;
        text-align: center;
        border-top: 1px solid #777171;
        padding-top: 1px;
        margin-top: 40px;
        margin-left: 2%;
    }
    .ex-footer{
        margin-top: 10px;
    }
    @page  {
        size: portrait;

    }
    @media print{

        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto;}
        thead {display: table-header-group;}
        tbody { page-break-after:always;
            display: table-row-group;}
        tfoot {
            display: table-row-group;

        }
        th.n_h_1 {
            width: 28%!important;
        }
    }
</style>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="fMlB0wRKfUGpCcLII5pcNpvL52VEcxIIDsiQ2cru">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="FEITS">

    <title>
        Extra Overtime</title>


</head>
<!-- BEGIN BODY -->

<body class="fixed-topbar fixed-sidebar theme-sdtl color-default">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<section>
    <!-- BEGIN SIDEBAR -->

    <div class="main-content">

<!--        <div class="heading_part">-->
<!--            <div class="top_h" style="margin-bottom: 1px; ">-->
<!--                <b>JUKI Bangladesh Ltd.</b>-->
<!--                <p style=""> <b>Salary-2019</b> </p>-->
<!--            </div>-->
<!--        </div>-->
<p class="pull-right"><button style="margin-top: 17px;" class="btn btn-info pull-right" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button></p>
        <div class="page-content ">
            <div class="container-fluid">
                <div class="" id="print_area">

                    <div>
                        <table class="thikana" style="width:100%;text-align:center;">
                            <thead class="table_head" style="border:0px!important;">
                                <tr  class="heading_top_1" style="border:0px;">
                                    <th colspan="19"  style="border:0px;">
                                        <div class="col-xlg-12 col-lg-12  col-sm-12" style="margin-bottom: 20px;">
                                            <div class="top_h" style="margin-bottom: 1px; ">
                                                <b>FIN BANGLA APPARELS LTD.</b>
                                                <p style=""> <b>Daily overtime for the date of</b> </p>
                                                <p><b>{{date('Y-F-d',strtotime($monthss))}}</b></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>

                                <tr class="heading-top-middle">
                                    <th rowspan="2">SL</th>
                                    @if(isset($section_workar))
                                    <th rowspan="2">Section</th>
                                    @else 
                                    <th rowspan="2">Department</th>
                                    @endif
                                    <th rowspan="2">Manpower</th>
                                    <th rowspan="2">Basic</th>
                                    <th rowspan="2">Gross Salary</th>
                                    <th rowspan="2">OT Hour</th>
                                    <th rowspan="2">OT Amount</th>
                                    <th rowspan="2">Remarks</th>
                                </tr>
                            </thead>
                            <tbody class="table-content table_info_data">
                                   @php $order=0; @endphp
                                    @foreach($total as $totals)
                                    @php $order++; @endphp
                                    <tr>
                                    <td>{{$order}}</td>  
                                    <td>
                                        @if(isset($section_workar))
                                          <a href="{{url('department/employee/ot/amount/details/'.base64_encode($totals->section))}}">{{$section->empSection}}</a>
                                          @else 
                                          <a href="{{url('department/employee/ot/amount/details/'.base64_encode($totals->department))}}">{{$department->departmentName}}</a>
                                        @endif
                                    </td>
                                        <td>
                                            @if(isset($section_workar))
                                                @php $worker=DB::table('tb_total_present')->count();  @endphp
                                                @else 
                                                @php $worker=DB::table('tb_total_present')->count(); @endphp
                                             @endif     
                                             {{$worker}}
                                        </td>
                                        <td> 
                                            @foreach($total_basic_gross as $test)
                                            {{number_format($test->total_basic)}}
                                              @php 
                                                $total_employee=DB::table('tb_total_present')->count();
                                                $multiply=$total_employee*104;
                                                $ot_rate=$test->total_basic/$multiply;
                                                $actual_ot_rate=round($ot_rate);
                                             @endphp
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($total_basic_gross as $test)
                                            {{number_format($test->total_gross)}}
                                            @endforeach
                                        </td>
                                        <td>{{$totals->total_hour+$totals->friday_hour+$totals->total_minute}} @php $total_ot=$totals->total_hour+$totals->friday_hour+$totals->total_minute; @endphp</td>
                                        <td>{{number_format($totals->total_amount)}}</td>
                                        <td></td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="ex-footer">
                            <div class="authority" style="display:flex;">
                                <div class="prepared_by">
                                    <span>Prepared By</span>
                                </div>
                                <div class="audited_by">
                                    <span>Audited By</span>
                                </div>
                                <div class="recomended_by">
                                    <span>Recommended By</span>
                                </div>
                                <div class="approved_by">
                                    <span>Approved By</span>
                                </div>
                            </div>
                        </div>


                </div>
            </div>
        </div>

    </div>
    <!-- END MAIN CONTENT -->
</section>
<script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
</body>
</html>
