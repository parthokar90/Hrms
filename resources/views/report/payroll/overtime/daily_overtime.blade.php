@extends('layouts.master')
@section('title', 'Daily Overtimne')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span style="font-size: 14px;font-weight: bold;">Daily Overtimne</span>
            </div>
            <div class="panel-body">

                    <div class="col-md-12">
                            <div class="form-group">
                                    <label>Select Type</label>
                                    <select id="ot_type" name="ot_type" class="form-control">
                                     <option value="">Select</option>
                                     <option value="1">Department Wise</option>
                                     <option value="2">Section Wise</option>
                                </select>    
                            </div>
                      </div>
                    <div id="department_wise" style="padding: 0;display:none">
                        <div class="col-md-12">
                            {{Form::open(array('url' => 'report/daily/overtime/show','method' => 'post'))}}
                            <div class="col-md-6">
                                <div class="form-group">
                                            <label>Select Department</label>
                                            <select class="form-control" name="department_id" required>
                                                <option value="all">All</option>
                                                @foreach($department as $departments)
                                                <option value="{{$departments->id}}">{{$departments->departmentName}}</option>
                                                @endforeach
                                            </select>    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Select Date</label>
                                    <input type="text" class="form-control date-picker" name="daily_ot_date" autocomplete="off" required>
                                </div>
                            </div>
                            <input type="hidden" name="type" value="department_wise_data">
                            <div class="col-md-12">
                            <button type="submit" id="salsummerymonth" name="salary_summery_month" class="btn btn-info">Preview</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>   




                    <div id="section_wise" style="padding: 0;display:none">
                      <div class="col-md-12">
                            {{Form::open(array('url' => 'report/daily/overtime/show','method' => 'post'))}}
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label>Select Section</label>
                                        <select class="form-control" name="section_id">
                                        <option value="all">All</option>
                                        @foreach($section as $sections)
                                                 <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                        @endforeach
                                   </select>    
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                <label>Select Date</label>
                                <input type="text" class="form-control date-picker" name="daily_ot_date" autocomplete="off" required>
                            </div>
                            </div>
                            <input type="hidden" name="type" value="section_wise_data">
                            <div class="col-md-12">
                              <button type="submit" id="salsummerymonth" name="salary_summery_month" class="btn btn-info">Preview</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                     </div> 
               </div>
          </div>
     </div>
    </div>
    @include('include.copyright')
    <script>
        $(document).ready(function() {
             $("#ot_type").change(function(){
                        var select_type= $("#ot_type").val();
                        if(select_type==1){
                        $("#department_wise").show();
                        $("#section_wise").hide();
                        $("#month_wise").hide();
                        }

                        if(select_type==2){
                        $("#section_wise").show();
                        $("#month_wise").hide();
                        $("#department_wise").hide();
                        }

                        if(select_type==3){
                        $("#month_wise").show();
                        $("#department_wise").hide();
                        $("#section_wise").hide();
                        }
                  });
        });
    </script>
@endsection