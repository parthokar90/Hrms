<!DOCTYPE html>
<html>
<head>
    <title>Salary Sheet Private</title>
    <style>
        @page    {
            size: landscape;
        }
        .verticalTableHeader {
            text-align:center;
            white-space:nowrap;
            g-origin:50% 50%;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);

        }
        .verticalTableHeader p {
            margin:0 -100% ;
            display:inline-block;
        }
        .verticalTableHeader p:before{
            content:'';
            width:0;
            padding-top:110%;/* takes width as reference, + 10% for faking some extra padding */
            display:inline-block;
            vertical-align:middle;
        }
        .company{
            text-align: center;
        }
        .company p{
            padding: 0;
            margin: 0;
            font-size: 17px;
        }
        .main_div{
            width:100%;
            margin: 0px;
        }
        .salary_table{
            width: 100%;
            margin-top:8px;
        }
        .authority{
            width:100%;
            padding-top: 47px;
        }
        .prepared_by{
            width: 25%;
            float: left;
        }
        .audited_by{
            width: 25%;
            float: left;
        }
        .recomended_by{
            width: 25%;
            float: left;
        }
        .approved_by{
            width: 25%;
            float: left;
        }
        .table_1{
            border-collapse: collapse;
            width: 100%;
            font-size:12px;
        }

        .table_1 tr th,td{
            border: 1px solid black;
            text-align: center;
        }

        .table_lefttd{
            padding-left:5px;
            text-align: left !important;
        }
        .table2{
            width: 100%;
            font-size:12px;
        }

        .table2 tr>td{
            border: none;
            text-align: left;
        }
        .main_foter{
            border: 1px solid #000000;
            margin-top: -68px;
            /*padding-left: 290px;*/
            font-size: 14px;
            height: 24px;
            line-height: 24px;
        }
        .main_foter span{
            margin-right:6px;
        }

        tfoot{
            width: 100%;
        }


        tfoot tr td{

            width: 100%;
            clear: both;
            border:none;
            padding-top: 0px!important;
            color:#fff;
        }
        .authority span{
            border-top: 1px solid #fff;
            padding-top: 3px;
            padding-left: 15px;
            padding-right: 15px;
        }

        tfoot tr td .main_foter{
            text-align: left;
            border:none;
        }
        tfoot tr td .main_foter span{
            margin-right: 10px;
        }
        .company_heading_1 td{
            border: none;
            line-height: .7;
        }

        .company_heading_2 td{
            border:none;

        }
        tfoot td{
            border:none;
        }
        td.signature_td{
            width:70px;
            height:81px;
        }
        .dayofmonth{
            display: inline-block;
            padding-right: 40px;
        }
        .company_heading_1 td span{
            display: inline-block;
            margin-bottom: 5px;
        }
        .company_heading_1 h3{
            margin-bottom:2px;
            margin-top: 5px;
        }
        .company_heading_1 p {
            margin-top: 0px;
        }
        .main_foter span:nth-child(1){

            padding-right: 17%;
        }
        .main_foter span:nth-child(2){
            font-size:12px;
        }
        .main_foter span:nth-child(3){
            font-size:12px;
        }
        .main_foter span:nth-child(4){
            font-size:12px;
        }
        .main_foter span:nth-child(5){
            font-size:12px;
        }
        .main_foter span:nth-child(6){
            font-size:12px;
        }
        .main_foter span:nth-child(7){
            font-size:12px;
        }
        .main_foter span:nth-child(8){
            font-size:12px;
            margin-left:6%;
        }
        .main_foter span:nth-child(9){
            font-size:12px;
            margin-left: 9%;
        }
        .main_foter span:nth-child(10){
            font-size:12px;
        }
        .ex_footer{
            opacity:0;
        }

        @media  print {
            .btn_hidden{
                display: none;
            }
            .main_div{page-break-after: always;}
            table { page-break-inside:auto; }
            tr    { page-break-inside:avoid; page-break-after:auto;}
            thead {display: table-header-group;}
            tbody { page-break-after:always;
                display: table-row-group;}
            tfoot {
                display: table-footer-group;
            }
            td.signature_td{
                width:70px;
                height:77px;
            }


            .ex_footer{
                opacity:1;
                position: fixed;
                bottom:0px;
                left:0;
                width:100%;
                /* padding-top:20px; */
                /* height:100px; */

            }
            .ex_footer .authority{
                padding-bottom: 0px;
                padding-top:20px;
                text-align:center;


            }
            .ex_footer .authority span{
                font-size:14px;
            }

            tfoot tr td .authority span{
                color:#fff;
                opacity:0;
            }
            tfoot tr td .authority span{
                border-top: 1px solid #fff;
                color:#fff;
                opacity:0;
            }

            .ex_footer tr td .authority span{
                color:#000;
            }
            .ex_footer .authority span{
                border-top: 1px solid #000;
                color:#000;
            }

            .pageNumber1:after {
                counter-increment: page;
                content:"Page number: " counter(page);
                left: 0;
                top: 100%;
                white-space: nowrap;
                z-index: 20;
                -moz-border-radius: 5px;
                -moz-box-shadow: 0px 0px 4px #222;
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
                background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
            }

        }
    </style>
</head>
<body>
<div id="print_area">
    @php $order=0;@endphp
    @foreach($department as $datas)
        @php
            $dept_id=$datas->department_id;
             $month=$months;
             $yearss=$ye;
             $monthss=$mo;
         $employees=DB::table('tb_salary_history_private')
         ->leftjoin('employees','tb_salary_history_private.emp_id','=','employees.id')
         ->leftjoin('payroll_grade','tb_salary_history_private.grade_id','=','payroll_grade.id')
         ->leftjoin('designations','tb_salary_history_private.designation_id','=','designations.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select('employees.employeeId as em_idss','employees.work_group','tb_salary_history_private.emp_id','tb_salary_history_private.present','tb_salary_history_private.weekend','tb_salary_history_private.holiday','tb_salary_history_private.total_payable_days','tb_salary_history_private.basic_salary','tb_salary_history_private.house_rant','tb_salary_history_private.medical','tb_salary_history_private.transport','tb_salary_history_private.food','tb_salary_history_private.gross','tb_salary_history_private.leave','tb_salary_history_private.total','tb_salary_history_private.working_day','tb_salary_history_private.absent','tb_salary_history_private.absent_deduction_amount','tb_salary_history_private.advanced_deduction','tb_salary_history_private.gross_pay','tb_salary_history_private.overtime','tb_salary_history_private.overtime_rate','tb_salary_history_private.overtime_amount','tb_salary_history_private.attendance_bonus','tb_salary_history_private.increment_bonus_percent','tb_salary_history_private.net_amount','payroll_grade.grade_name','designations.designation','departments.departmentName','tb_salary_history_private.emp_id as s_emp_id','employees.empFirstName as e_first_name','employees.empJoiningDate','employees.employeeId')
         ->where('tb_salary_history_private.dates',$month)
         ->where('employees.date_of_discontinuation','=',null)
         ->where('tb_salary_history_private.department_id','=',$dept_id)
         ->get();

         $emp_count_with_salary=DB::table('tb_salary_history_private')
         ->leftjoin('employees','tb_salary_history_private.emp_id','=','employees.id')
         ->leftjoin('departments','employees.empDepartmentId','=','departments.id')
         ->leftjoin('tblines','employees.line_id','=','tblines.id')
         ->select(DB::raw('count(tb_salary_history_private.emp_id) as emp_count'),DB::raw('SUM(tb_salary_history_private.basic_salary) as total_basic'),DB::raw('SUM(tb_salary_history_private.house_rant) as total_house'),DB::raw('SUM(tb_salary_history_private.medical) as total_medical'),DB::raw('SUM(tb_salary_history_private.transport) as total_transport'),DB::raw('SUM(tb_salary_history_private.food) as total_food'),DB::raw('SUM(tb_salary_history_private.gross) as total_gross'),DB::raw('SUM(tb_salary_history_private.increment_bonus_percent) as total_increment'),DB::raw('SUM(tb_salary_history_private.absent_deduction_amount) as total_deduction'), DB::raw('SUM(tb_salary_history_private.net_amount) as total_net') )
         ->where('tb_salary_history_private.dates',$month)
         ->where('employees.date_of_discontinuation','=',null)
         ->where('tb_salary_history_private.department_id','=',$dept_id)
         ->get();

        @endphp
        <div class="main_div">
            {{--<div class="company">--}}
            {{--<h3>{{$companyInformation->company_name}}</h3>--}}
            {{--<p>Pay Sheet for the month of</p>--}}
            {{--<p>--}}
            {{--@if($monthname)--}}
            {{--{{date('F-Y',strtotime($monthname->month))}}<br>--}}
            {{--<span style="text-align: center">Payment Date: <b>{{date('d-m-Y',strtotime($payment_date))}}</b></span>--}}
            {{--@else--}}
            {{--Sorry No Data Found--}}
            {{--@endif--}}
            {{--</p>--}}
            {{--</div>--}}

            <div class="salary_table">
                <table class="table_1">
                    <thead>


                    <tr class="table_heading">

                    <tr style="text-align: center;" class="company_heading_1">

                        <td colspan="30">  <h3>{{$companyInformation->company_name}}</h3></td>
                    </tr>
                    <tr  style="text-align: center;" class="company_heading_1">

                        <td colspan="5"  style="text-align: left;">

                            <span>Department:</span>
                            <span><b>{{$datas->departmentName}}</b></span><br>
                            <span>Section/Line: <b>{{$datas->line_no}}</b></span><br />
                        </td>
                        <td colspan="14" >
                            <p>Pay Sheet for the month of : <span> @if($monthname)
                                        {{date('F-Y',strtotime($monthname->month))}}<br>

                                    @else
                                        Sorry No Data Found
                                    @endif </span></p>

                        </td>

                        <td colspan="11" style="text-align: right;">
                            <span class="pageNumber"></span><br>
                            <span class="dayofmonth">Days in Month:
                                            <b>
                                            @if($monthname)
                                                    {{date('t',strtotime($monthname->month))}}
                                                @else
                                                @endif
                                        </b>
                                        </span><br />
                            <span>Payment Date:
                                                <b>
                                                {{date('d-m-Y',strtotime($payment_date))}}
                                                </b>
                                            </span><br />
                        </td>
                    </tr>


                    <tr>



                        <th rowspan="2">SN</th>
                        <th rowspan="2">EmpID</th>
                        <th rowspan="2" width="14%">Name</th>
                        <th rowspan="2"  width="10%" class="table_lefttd">Designation</th>
                        <th rowspan="2" class="table_lefttd">Grade</th>
                        <th rowspan="2">Basic</th>
                        <th rowspan="2">House</th>
                        <th rowspan="2">Medical</th>
                        <th rowspan="2">Food</th>
                        <th rowspan="2" width="3%">Transport</th>
                        <th rowspan="2">Gross</th>
                        <th colspan="4">Leave Day</th>
                        <th rowspan="2" class="verticalTableHeader">Work<br />Days</th>
                        <th rowspan="2">Abs Days</th>
                        <th rowspan="2" class="verticalTableHeader">Week<br />end</th>
                        <th rowspan="2" class="verticalTableHeader">Holiday</th>
                        <th rowspan="2">Total Payable<br /> days</th>
                        <th colspan="3">Deduction</th>
                        <th rowspan="2">Gross Pay</th>
                        <th colspan="3">Overtime</th>
                        <th rowspan="2">Att Bonus</th>
                        <th rowspan="2" >Special Allow</th>
                        <th rowspan="2">Net wages</th>
                        <th rowspan="2" width="16%">Signature</th>
                    </tr>
                    </tr>
                    <tr>
                        <th>
                            CL
                        </th>
                        <th>
                            SL
                        </th>
                        <th>
                            EL
                        </th>
                        <th>
                            ML
                        </th>
                        <th>
                            Adv
                        </th>
                        <th>
                            Abs
                        </th>
                        <th>Stm</th>
                        <th>
                            Hrs
                        </th>
                        <th>
                            Rate
                        </th>
                        <th>
                            Amt
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employees as $salary)
                        @php
                            $order++;
                            $test=$salary->emp_id;
                                  $leave_res=DB::SELECT("SELECT tb_salary_history_private.emp_id as main_id,leave_of_employee.leave_type_id,leave_of_employee.total
                                  FROM tb_salary_history_private
                                  LEFT JOIN leave_of_employee ON leave_of_employee.emp_id=tb_salary_history_private.emp_id
                                  WHERE YEAR(leave_of_employee.month)='$yearss' AND MONTH(leave_of_employee.month)='$monthss' AND tb_salary_history_private.dates='$month' AND tb_salary_history_private.emp_id=$test");
                        @endphp
                        <tr>
                            <td style="height: 73px;">{{$order}}</td>
                            <td>{{$salary->em_idss}}</td>
                            <td class="table_lefttd" WIDTH="8%">{{$salary->e_first_name}} <br>Join {{$salary->empJoiningDate}}</td>
                            <td class="table_lefttd">{{$salary->designation}}</td>
                            <td class="table_lefttd">{{$salary->grade_name}}</td>
                            <td>{{number_format(round($salary->basic_salary,0))}}</td>
                            <td>{{number_format(round($salary->house_rant,0))}}</td>
                            <td>{{number_format(round($salary->medical,0))}}</td>
                            <td>{{number_format(round($salary->food,0))}}</td>
                            <td>{{number_format(round($salary->transport,0))}}</td>
                            <td>{{number_format(round($salary->gross,0))}}</td>
                            @if($leave_res)
                                <td>
                                    @foreach($leave_res as $leave_type)
                                        @if($leave_type->leave_type_id==5)
                                            {{$leave_type->total}}
                                        @endif
                                    @endforeach
                                    @php
                                        $data=DB::table('leave_of_employee')->where('leave_type_id','=',5)->where('emp_id',$salary->emp_id)->where('leave_of_employee.month',$monthname->month)->count();
                                    @endphp
                                    @if($data==1)
                                    @else
                                        {{$data}}
                                    @endif
                                </td>
                                <td>
                                    @foreach($leave_res as $leave_type)
                                        @if($leave_type->leave_type_id==6)
                                            {{$leave_type->total}}
                                        @endif
                                    @endforeach
                                    @php
                                        $data=DB::table('leave_of_employee')->where('leave_type_id','=',6)->where('emp_id',$salary->emp_id)->where('leave_of_employee.month',$monthname->month)->count();
                                    @endphp
                                    @if($data==1)
                                    @else
                                        {{$data}}
                                    @endif
                                </td>
                                <td>
                                    @foreach($leave_res as $leave_type)
                                        @if($leave_type->leave_type_id==4)
                                            {{$leave_type->total}}
                                        @endif
                                    @endforeach
                                    @php
                                        $data=DB::table('leave_of_employee')->where('leave_type_id','=',4)->where('emp_id',$salary->emp_id)->where('leave_of_employee.month',$monthname->month)->count();
                                    @endphp
                                    @if($data==1)
                                    @else
                                        {{$data}}
                                    @endif
                                </td>
                                <td>
                                    0
                                </td>
                            @else
                                <td>
                                    0
                                </td>
                                <td>
                                    0
                                </td>
                                <td>
                                    0
                                </td>
                                <td>
                                    0
                                </td>
                            @endif
                            <td>
                                @if($salary->present=='')
                                    0
                                @else
                                    {{$salary->present}}
                                @endif
                            </td>
                            <td>
                                @if($salary->absent=='')
                                    0
                                @else
                                    {{$salary->absent}}
                                @endif
                            </td>
                            <td>{{$salary->weekend}}</td>
                            <td>
                                @if($salary->holiday=='')
                                    0
                                @else
                                    {{$salary->holiday}}
                                @endif
                            </td>
                            <td>{{$salary->total_payable_days}}</td>
                            <td>
                                @if($salary->advanced_deduction=='')
                                    0
                                @else
                                    {{number_format(round($salary->advanced_deduction,0))}}
                                @endif
                            </td>
                            <td>
                                @if($salary->absent_deduction_amount=='')
                                    0
                                @else
                                    {{number_format(round($salary->absent_deduction_amount,0))}}
                                @endif
                            </td>
                            <td>10</td>
                            <td>
                                @if($salary->gross_pay=='')
                                    0
                                @else
                                    {{number_format(round($salary->gross_pay,0))}}
                                @endif
                            </td>
                            <td>
                                @if($salary->overtime=='')
                                    0
                                @else
                                    {{$salary->overtime}}
                                @endif
                            </td>
                            <td>
                                @if($salary->work_group=='Staff')
                                @else
                                    {{$salary->overtime_rate}}
                                @endif
                            </td>
                            <td>
                                @if($salary->overtime_amount=='')
                                    0
                                @else
                                    {{number_format(round($salary->overtime_amount,0))}}
                                @endif
                            </td>
                            <td>
                                @if($salary->absent <3 && $salary->total=='')
                                    {{number_format(round($salary->attendance_bonus,0))}}
                                @else
                                    0
                                @endif
                            </td>
                            <td>
                                @if($salary->increment_bonus_percent=='')
                                    0
                                @else
                                    {{number_format(round($salary->increment_bonus_percent,0))}}
                                @endif
                            </td>
                            <td>
                                @if($salary->absent <3 && $salary->total=='')
                                    {{round($salary->net_amount,0)}}
                                @else
                                    @php
                                        $net=$salary->net_amount;
                                        $bonus=$salary->attendance_bonus;
                                        $total=$net-$bonus
                                    @endphp
                                    {{number_format(round($total,0))}}
                                @endif
                            </td>
                            <td class="signature_td"></td>
                        </tr>
                    @endforeach
                    @foreach($emp_count_with_salary as $total_counts)
                        <tr style="padding: 3px 3px;">
                            <td style="border-bottom: 1px solid #555;text-align: left;" colspan="5">Dept/Section:{{$total_counts->emp_count}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_counts->total_basic,0)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_counts->total_house,0)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_counts->total_medical,0)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_counts->total_food,0)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_counts->total_transport,0)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_counts->total_gross,0)}}</td>
                            <td  style="border-bottom: 1px solid #555;" colspan="9"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="3">{{round($total_counts->total_deduction,0)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="5"></td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">{{round($total_counts->total_increment,0)}}</td>
                            <td style="border-bottom: 1px solid #555;" colspan="1">@if($salary->absent <3 && $salary->total=='')
                                    @php $total=$total_counts->total_net; @endphp
                                    {{round($total,0)}}
                                @else
                                    @php
                                        $net=$total_counts->total_net;
                                        $bonus=$salary->attendance_bonus;
                                        $total=$net-$bonus
                                    @endphp
                                    {{round($total,0)}}
                                @endif</td>
                            <td style="border-bottom: 1px solid #555;">    </td>
                        </tr>
                        <tr></tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="31" style="padding-top:20px;border:none;border-top:4px solid #ffffff;" >
                            <div class="authority">
                                <div class="prepared_by">
                                    <span>Prepared By</span>
                                </div>
                                <div class="audited_by">
                                    <span>Audited By</span>
                                </div>
                                <div class="recomended_by">
                                    <span>Recommended By</span>
                                </div>
                                <div class="approved_by">
                                    <span>Approved By</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    @endforeach
</div>
<div class="ex_footer">
    <tr>
        <td colspan="31" style="padding-top:20px;border:none;border-top:4px solid #ffffff;" >

            <div class="authority">
                <div class="prepared_by">
                    <span>Prepared By</span>
                </div>
                <div class="audited_by">
                    <span>Audited By</span>
                </div>
                <div class="recomended_by">
                    <span>Recommended By</span>
                </div>
                <div class="approved_by">
                    <span>Approved By</span>
                </div>
            </div>
        </td>
    </tr>
</div>
<script>
    window.print();
    window.close();
</script>
</body>
</html>