@extends('layouts.master')
@section('title', 'Payroll Report')
@section('content')
    <style>
        .f-14{
            font-size: 13px !important;
        }
    </style>
    <div class="page-content page-content-hover-1">
        <div class="row panel myAnimation_1 "  style="border:1px solid #dcdcdc;">
            <div class="col-xlg-12 col-lg-12  col-sm-12 animated bounceInRight">
                <div class="text-center report_panel_heading" >
                    <h2><b>Payroll Report</b></h2>
                    <hr class="animated bounceInLeft">
                </div>
            </div>
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{url('/report/payroll/employee/salary/sheet')}}">
                    <div class="panel">
                        <div class="panel-content payroll_ssheet" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Salary Sheet</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{url('/report/payroll/employee/salary/sheet/three_shift')}}">
                    <div class="panel">
                        <div class="panel-content payroll_ssheet" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Salary Sheet(3-shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}



            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{url('/report/payslip')}}">
                    <div class="panel">
                        <div class="panel-content payroll_payslip" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text-o f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Payslip</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
{{-- 
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{url('/report/payslip/three/shift')}}">
                    <div class="panel">
                        <div class="panel-content payroll_payslip" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text-o f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Payslip(3-Shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}



            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{url('/report/payslip/resigned/employee')}}">
                    <div class="panel">
                        <div class="panel-content payroll_payslip" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text-o f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Payslip Resigned Employee</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{url('/report/payslip/resigned/employee/three/shift')}}">
                    <div class="panel">
                        <div class="panel-content payroll_payslip" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text-o f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Payslip Resigned Employee(3-shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{url('/report/payslip/lefty/employee')}}">
                    <div class="panel">
                        <div class="panel-content payroll_payslip" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text-o f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Payslip Lefty Employee</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{url('/report/payslip/lefty/employee/three/shift')}}">
                    <div class="panel">
                        <div class="panel-content payroll_payslip" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-file-text-o f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Payslip Lefty Employee(3-shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                <a target="_BLANK" href="{{url('/report/salary/sheet/lefty/employee')}}">
                    <div class="panel">
                        <div class="panel-content emp_elsec" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Lefty Employee Salary Sheet</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                <a target="_BLANK" href="{{url('/report/salary/sheet/lefty/employee/three/shift')}}">
                    <div class="panel">
                        <div class="panel-content emp_elsec" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Lefty Employee Salary Sheet(3-Shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                <a target="_BLANK" href="{{url('/report/salary/sheet/resigned/employee')}}">
                    <div class="panel">
                        <div class="panel-content emp_elbg" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Resigned Employee Salary Sheet</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
                <a target="_BLANK" href="{{url('/report/salary/sheet/resigned/employee/three/shift')}}">
                    <div class="panel">
                        <div class="panel-content emp_elbg" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Resigned Employee Salary(3-Shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                <a target="_BLANK" href="{{url('report/salary/summary/lefty/employee')}}">
                    <div class="panel">
                        <div class="panel-content emp_elbd" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span  class="f-14"><b>Salary Summary (Lefty Employee)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                <a target="_BLANK" href="{{url('report/salary/summary/resigned/employee')}}">
                    <div class="panel">
                        <div class="panel-content emp_elbd" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Salary Summary (Resigned Employee)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceIn">
                <a target="_BLANK" href="{{url('/report/salary/summery')}}">
                    <div class="panel">
                        <div class="panel-content payroll_ssummery" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Salary Summary</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceIn">
                <a target="_BLANK" href="{{url('/report/salary/summery/three/shift')}}">
                    <div class="panel">
                        <div class="panel-content payroll_ssummery" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Salary Summary(3-Shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}

            @if(checkPermission(['admin']))
                <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                    <a target="_BLANK" href="{{route('extraovertime')}}">
                        <div class="panel">
                            <div class="panel-content emp_elbd" style="">
                                <center>
                                    <div class="row" >
                                        <div class="">
                                            <center><i class="fa fa-users f-40"></i></center>
                                        </div>
                                        <br>
                                        <div class="">
                                            <center>
                                                <span class="f-14"><b>Extra Overtime</b></span>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </a>
                </div>


                <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                    <a target="_BLANK" href="{{route('extraotsummery')}}">
                        <div class="panel">
                            <div class="panel-content emp_elbd" style="">
                                <center>
                                    <div class="row" >
                                        <div class="">
                                            <center><i class="fa fa-users f-40"></i></center>
                                        </div>
                                        <br>
                                        <div class="">
                                            <center>
                                                <span class="f-14"><b>Extra Overtime Summary</b></span>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </a>
                </div>



                <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                    <a target="_BLANK" href="{{url('report/extra/overtime/lefty/employee')}}">
                        <div class="panel">
                            <div class="panel-content att_dpr" style="">
                                <center>
                                    <div class="row" >
                                        <div class="">
                                            <center><i class="fa fa-users f-40"></i></center>
                                        </div>
                                        <br>
                                        <div class="">
                                            <center>
                                                <span class="f-14"><b>Extra Overtime(Lefty Employee)</b></span>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                    <a target="_BLANK" href="{{url('report/extra/overtime/resign/employee')}}">
                        <div class="panel">
                            <div class="panel-content payroll_ssummery" style="">
                                <center>
                                    <div class="row" >
                                        <div class="">
                                            <center><i class="fa fa-users f-40"></i></center>
                                        </div>
                                        <br>
                                        <div class="">
                                            <center>
                                                <span class="f-14"><b>Extra Overtime(Resigned Employee)</b></span>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </a>
                </div>
            @endif

            @if(checkPermission(['hr']) || checkPermission(['hr-admin']))
                <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                    <a target="_BLANK" href="{{url('overtime/summary/hr')}}">
                        <div class="panel">
                            <div class="panel-content payroll_ssummery" style="">
                                <center>
                                    <div class="row" >
                                        <div class="">
                                            <center><i class="fa fa-users f-40"></i></center>
                                        </div>
                                        <br>
                                        <div class="">
                                            <center>
                                                <span class="f-14"><b>Overtime Summary</b></span>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </a>
                </div>
            @endif
            @if(checkPermission(['admin']))
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{url('report/daily/overtime')}}">
                    <div class="panel">
                        <div class="panel-content payroll_ssummery" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Daily Overtime</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
        @endif
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInLeft">
                <a target="_BLANK" href="{{route('payroll.bank_payment_sheet')}}">
                    <div class="panel">
                        <div class="panel-content payroll_advance" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-money f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Bank Payment Sheet</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInLeft">
                <a target="_BLANK" href="{{route('payroll.bkash_payment_sheet')}}">
                    <div class="panel">
                        <div class="panel-content payroll_advance" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-money f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Bkash Payment Sheet</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInLeft">
                <a target="_BLANK" href="{{route('payroll.bank_payment_sheet_three_shift')}}">
                    <div class="panel">
                        <div class="panel-content payroll_advance" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-money f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Bank Payment Sheet(3-shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}

            {{-- <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInLeft">
                <a target="_BLANK" href="{{route('payroll.bkash_payment_sheet_three_shift')}}">
                    <div class="panel">
                        <div class="panel-content payroll_advance" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-money f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Bkash Payment Sheet(3-shift)</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div> --}}

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
                <a target="_BLANK" href="{{url('/report/bonus')}}">
                    <div class="panel">
                        <div class="panel-content payroll_bonus" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-money f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Bonus</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInLeft">
                <a target="_BLANK" href="{{url('/report/advanced')}}">
                    <div class="panel">
                        <div class="panel-content payroll_advance" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-money f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Advance</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceIn">
                <a target="_BLANK" href="{{url('/report/penalty')}}">
                    <div class="panel">
                        <div class="panel-content payroll_penalty" style="">
                            <center>
                                <div class="row">
                                    <div class="">
                                        <center><i class="fa fa-times-circle f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Penalty</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>


































        </div>
    </div>
    @include('include.copyright')
@endsection