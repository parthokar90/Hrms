@extends('layouts.master')
@section('title', 'Daily Employee Salary')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                @if(Session::has('message'))
                    <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('message') }}</p>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Daily employee Salary</b></div>
                    <div class="panel-body">
                        {{Form::open(array('url' => '/report/daily/employee/salary/data','method' => 'post'))}}
                        <div class="form-group">
                            <label>Select date</label>
                            <input type="text" name="daily_date" autocomplete="off" class="form-control date-picker" placeholder="Enter Date">
                        </div>
                        <button type="submit" class="btn btn-success">Generate</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    @include('include.copyright')
@endsection
