













@extends('layouts.master')
@section('title', 'Resigned Employees Salary Sheet')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span style="font-weight: bold;font-size: 14px;">Generate Resigned Employee Salary Sheet</span>
            </div>
            <div class="panel-body">
               <div class="col-md-12">
                    <div class="form-group">
                            <label>Select Type</label>
                            <select id="ot_type" name="ot_type" class="form-control">
                                     <option value="">Select</option>
                                       <option value="1">Department Wise</option>
                                       <option value="2">Section Wise</option>
                                       <option value="3">Employee Wise</option>
                                       <option value="4">Month Wise</option>
                             </select>
                     </div>
               </div>
                        {{-- department start --}}
                        <div class="col-md-12" id="department_type" style="display:none">
                                {{Form::open(array('url' => '/report/salary/sheet/resigned/employee/show','method' => 'post','target' => '_blank'))}}
                               <div class="col-md-6" style="padding:0px">
                                <div class="form-group">
                                <label>Select Department</label>
                                <select class="form-control" name="department_id" data-search="true">
                                        <option value="">Select Department</label>
                                        @foreach($department as $departments)
                                           <option value="{{$departments->dept_id}}">{{$departments->departmentName}}</option>
                                        @endforeach
                                </select>
                                </div>
                               </div>
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Select Month <span style="color: darkred;font-size: 16px;">*</span></label>
                                        <div class="prepend-icon">
                                            <input type="text" autocomplete="off" name="emp_sal_month" id="emp_sal_month" class="form-control format_date" required>
                                            <i class="icon-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="type" value="dept_wise_data">
                                <div class="col-md-12" style="padding:0px">
                                        <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Generate</button>
                                        <button type="submit" name="dept_excel" value="dept_excel" class="btn btn-success">Generate Excel</button>
                                </div> 
                                 {{ Form::close() }}
                           </div>
                         {{-- department end --}}


                        {{-- section start --}}
                         <div class="col-md-12" id="section_type" style="display:none">
                                {{Form::open(array('url' => '/report/salary/sheet/resigned/employee/show','method' => 'post','target' => '_blank'))}}
                                <div class="col-md-6" style="padding:0px">
                                    <div class="form-group">
                                    <label>Select Section</label>
                                    <select class="form-control" name="section_id" data-search="true">
                                            <option value="">Select Section</label>
                                            @foreach($section as $sections)
                                               <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                            @endforeach
                                    </select>
                                    </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Select Month <span style="color: darkred;font-size: 16px;">*</span></label>
                                            <div class="prepend-icon">
                                                <input type="text" autocomplete="off" name="emp_sal_month" id="emp_sal_month_section" class="form-control format_date" required>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="type" value="section_wise_data">
                                    <div class="col-md-12" style="padding:0px">
                                            <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Generate</button>
                                            <button type="submit" name="section_excel" value="section_excel" class="btn btn-success">Generate Excel</button>
                                    </div> 
                                     {{ Form::close() }}
                            </div>
                          {{-- section end --}}



                          {{-- employee start --}}
                           <div class="col-md-12" id="employee_type" style="display:none">
                                {{Form::open(array('url' => '/report/salary/sheet/resigned/employee/show','method' => 'post','target' => '_blank'))}}
                                     <div class="col-md-6" style="padding:0px">
                                         <div class="form-group">
                                           <label>Select Employee</label>  
                                        <select class="form-control" name="emp_id" data-search="true">
                                                <option value="">Select Employee</label>
                                                @foreach($employee as $employees)
                                                   <option value="{{$employees->id}}">({{$employees->employeeId}}) {{$employees->empFirstName}} {{$employees->empLastName}}</option>
                                                @endforeach
                                        </select>
                                         </div>
                                       </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Select Month <span style="color: darkred;font-size: 16px;">*</span></label>
                                            <div class="prepend-icon">
                                                <input type="text" autocomplete="off" name="emp_sal_month" id="emp_sal_month_emp" class="form-control format_date" required>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                        <input type="hidden" name="type" value="employee_wise_data">
                                        <div class="col-md-12" style="padding:0px">
                                                <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Generate</button>
                                                <button type="submit" name="employee_excel" value="employee_excel" class="btn btn-success">Generate Excel</button>
                                        </div> 
                                        {{ Form::close() }}
                                </div>
                              {{-- employee end --}}


                            {{-- month start --}}
                              <div class="col-md-12" id="month_wise" style="display:none">
                                    {{Form::open(array('url' => '/report/salary/sheet/resigned/employee/show','method' => 'post','target' => '_blank'))}}                                      
                                       <div class="col-md-12" style="padding:0px">
                                            <div class="form-group">
                                                <label class="form-label">Select Month <span style="color: darkred;font-size: 16px;">*</span></label>
                                                <div class="prepend-icon">
                                                    <input type="text" autocomplete="off" name="emp_sal_month" id="emp_sal_monthss" class="form-control format_date" required>
                                                    <i class="icon-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                            <input type="hidden" name="type" value="month_wise_data">
                                            <div class="col-md-12" style="padding:0px">
                                                    <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Generate</button>
                                                    <button type="submit" name="month_excel" value="month_excel" class="btn btn-success">Generate Excel</button>
                                            </div> 
                                        {{ Form::close() }}
                              </div>
                             {{-- month start --}}
                     </div>
                </div>

    <script>
            $(document).ready(function(){
                $('#emp_sal_month').datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: "yy-mm",
                        showButtonPanel: true,
                        currentText: "This Month",
                        onChangeMonthYear: function (year, month, inst) {
                            $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                        },
                        onClose: function(dateText, inst) {
                            var month = $(".ui-datepicker-month :selected").val();
                            var year = $(".ui-datepicker-year :selected").val();
                            $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                        }
                    }).focus(function () {
                        $(".ui-datepicker-calendar").hide();
                    });
    
                    $('#emp_sal_month_section').datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: "yy-mm",
                        showButtonPanel: true,
                        currentText: "This Month",
                        onChangeMonthYear: function (year, month, inst) {
                            $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                        },
                        onClose: function(dateText, inst) {
                            var month = $(".ui-datepicker-month :selected").val();
                            var year = $(".ui-datepicker-year :selected").val();
                            $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                        }
                    }).focus(function () {
                        $(".ui-datepicker-calendar").hide();
                    });
    
                    $('#emp_sal_month_emp').datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: "yy-mm",
                        showButtonPanel: true,
                        currentText: "This Month",
                        onChangeMonthYear: function (year, month, inst) {
                            $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                        },
                        onClose: function(dateText, inst) {
                            var month = $(".ui-datepicker-month :selected").val();
                            var year = $(".ui-datepicker-year :selected").val();
                            $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                        }
                    }).focus(function () {
                        $(".ui-datepicker-calendar").hide();
                    });
    
                    
                    $('#emp_sal_monthss').datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: "yy-mm",
                        showButtonPanel: true,
                        currentText: "This Month",
                        onChangeMonthYear: function (year, month, inst) {
                            $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                        },
                        onClose: function(dateText, inst) {
                            var month = $(".ui-datepicker-month :selected").val();
                            var year = $(".ui-datepicker-year :selected").val();
                            $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                        }
                    }).focus(function () {
                        $(".ui-datepicker-calendar").hide();
                    });
                 setTimeout(function() {
                        $('#alert_message').fadeOut('fast');
                    }, 5000);  
              $("#ot_type").change(function(){
                var select_type= $("#ot_type").val();
                if(select_type==1){
                  $("#department_type").show();
                  $("#month_wise").hide();
                  $("#section_type").hide();
                  $("#employee_type").hide();
                }
    
                if(select_type==2){
                  $("#section_type").show();
                  $("#month_wise").hide();
                  $("#department_type").hide();
                  $("#employee_type").hide();
                }
    
                if(select_type==3){
                  $("#employee_type").show();
                  $("#month_wise").hide();
                  $("#department_type").hide();
                  $("#section_type").hide();
                }
    
                if(select_type==4){
                  $("#month_wise").show();
                  $("#employee_type").hide();
                  $("#department_type").hide();
                  $("#section_type").hide();
                }
              });
            });
        </script>
    @include('include.copyright')
@endsection