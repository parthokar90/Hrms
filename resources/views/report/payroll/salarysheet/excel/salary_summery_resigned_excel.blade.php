
<!DOCTYPE html>
<html>
<head>
    <title>Salary Summary Resigned</title>
</head>
<body>
<div id="print_area">
        <div class="main_div">
            <div class="salary_table">
                <table class="table_1">
                    <thead>
                   
                    

                    <tr style="text-align: center;">

                        <tr><td></td><td colspan="16" style="text-align: center;"><h2><b>{{$companyInformation->company_name}}</b></h2></td></tr>
                        <tr><td></td><td colspan="16" style="text-align: center;">
                             <p>Salary Summary Resigned {{date('F-Y',strtotime($_POST['salary_summery_monthsss']))}}</p>
                        </td></tr>
                      
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <th style="text-align:center;" >SN</th>
                        <th >
                                @if(isset($dept_id))
                                Department
                                @endif
                                @if(isset($section_id))
                                 Section 
                                @endif
                                @if(isset($monthly_data))
                                Department
                                @endif
                                @if(isset($all_dept))
                                 Department
                                @endif
                                @if(isset($all_section))
                                Section 
                                @endif
                        </th>
                        <th >WORKER </th>
                        <th >BASIC</th>
                        <th >HOUSE</th>
                        <th >MEDICAL</th>
                        <th >FOOD</th>
                        <th >TA/DA</th>
                        <th >GROSS </th>
                        <th >GROSS PAY</th>
                        <th >ABS</th>
                        <th >ADV</th>
                        <th  >OT</th>
                        <th >OT AMT</th>
                        <th >ATT BONUS</th>
                        <th >SP ALLOW</th>
                        <th >NET</th>

                    </tr>
                   
                    </thead>
                       <tbody>
                            @php $m_power=0;$basic_total=0;$house_total=0;$total_medicals=0; $i=0;
                            $total_foods=0;$total_transports=0;$total_gorss=0;$total_gross_pay=0;
                            $abs_deduction_amount=0;$total_advance_deduction=0;$total_overtime=0;
                            $total_ot_amount=0;$total_att_bonus=0;$total_increment_bonus=0;$total_net_amount=0;
                            @endphp
                      
                      @foreach($data as $summery)
                        <tr>
                            <td style="text-align: center;">{{++$i}}</td>
                            <td style="text-align: left;">
                                    @if(isset($dept_id))
                                    {{$summery->departmentName}}
                                    @endif
                                    @if(isset($section_id))
                                    {{$summery->section_id}}
                                    @endif
                                    @if(isset($monthly_data))
                                     {{$summery->departmentName}}
                                    @endif
                                    @if(isset($all_dept))
                                     {{$summery->departmentName}}
                                    @endif
                                    @if(isset($all_section))
                                    {{$summery->section_id}}
                                    @endif
                            </td>
                            <td style="text-align: left;">{{$summery->worker}} @php $m_power+=$summery->worker; @endphp</td>
                            <td style="text-align:left">{{(round($summery->basic))}} @php $basic_total+=$summery->basic; @endphp</td>
                            <td style="text-align:left">{{(round($summery->house))}} @php $house_total+=$summery->house; @endphp</td>
                            <td style="text-align: left;">{{(round($summery->medicals))}} @php $total_medicals+=$summery->medicals; @endphp</td>
                            <td style="text-align: left;">{{(round($summery->foods))}} @php $total_foods+=$summery->foods; @endphp</td>
                            <td style="text-align: left;">{{(round($summery->transports))}} @php $total_transports+=$summery->transports; @endphp</td>
                            <td style="text-align: left;">{{(round($summery->gorss))}} @php $total_gorss+=$summery->gorss; @endphp</td>
                            <td style="text-align: left;">{{(round($summery->gross_pay))}} @php $total_gross_pay+=$summery->gross_pay; @endphp</td>
                            <td style="text-align: left;">{{(round($summery->a_deduction_amount))}} @php $abs_deduction_amount+=$summery->a_deduction_amount; @endphp</td>
                            <td style="text-align: left;">@if($summery->advanced_deduction=='') 0 @else {{(round($summery->advanced_deduction))}} @endif @php $total_advance_deduction+=$summery->advanced_deduction; @endphp</td>
                            <td style="text-align: left;">{{($summery->overtimes)}} @php $total_overtime+=$summery->overtimes; @endphp</td>
                            <td style="text-align: left;">{{(round($summery->ot_amounts))}} @php $total_ot_amount+=$summery->ot_amounts; @endphp</td>
                            <td style="text-align: left;">{{(round($summery->attendance_bonus))}} @php $total_att_bonus+=$summery->attendance_bonus; @endphp</td>
                            <td style="text-align: left;">@if($summery->total_increment=='') 0 @else {{(round($summery->total_increment))}} @endif @php $total_increment_bonus+=$summery->total_increment; @endphp</td>
                            <td style="text-align: left;">{{(round($summery->net_total))}} @php $total_net_amount+=$summery->net_total; @endphp</td>
                        </tr>
                        @endforeach


                                                                    
                        <tr>
                            <td style="text-align:left;" >Grand Total:</td>
                            <td></td>
                            <td style="text-align:left;" >{{($m_power)}}</td>
                            <td style="text-align:left;" >{{($basic_total)}}</td>
                            <td style="text-align:left;" >{{($house_total)}}</td>
                            <td style="text-align:left;" >{{($total_medicals)}}</td>
                            <td style="text-align:left;" >{{($total_foods)}}</td>
                            <td style="text-align:left;" >{{($total_transports)}}</td>
                            <td  style="text-align:left;" >{{($total_gorss)}}</td>
                            <td style="text-align:left;" >{{($total_gross_pay)}}</td>
                            <td style="text-align:left;" >{{($abs_deduction_amount)}}</td>
                            <td style="text-align:left;" >{{($total_advance_deduction)}}</td>
                            
                            <td style="text-align:left;" >{{($total_overtime)}}</td>
                            <td style="text-align:left;" >{{($total_ot_amount)}}</td>


                            <td style="text-align:left;" >{{($total_att_bonus)}}</td>
                            <td style="text-align:left;" >{{($total_increment_bonus)}}</td>
                            <td style="text-align:left;" >{{($total_net_amount)}}</td>
                        </tr>
                    <tr></tr>
                    <tr></tr>
                       <tr>
                            <td colspan="2"></td>
                            <td colspan="3"><p>Prepared By</p></td>
                            <td colspan="3"><p>Audited By</p></td>
                            <td colspan="4"><p>Recommended By</p></td>
                            <td colspan="4"><p>Approved By</p></td>

                        </tr>
                        
                
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
