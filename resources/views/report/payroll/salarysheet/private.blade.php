@extends('layouts.master')
@section('title', 'Proccess Salary Private')
@section('content')
    <div class="page-content">
        @if(Session::has('salaryprocesscheck'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('salaryprocesscheck') }}</p>
        @endif
        @if(Session::has('salaryprocesscheckdatewise'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('salaryprocesscheckdatewise') }}</p>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">Process Employee Salary Private</div>
            <div class="panel-body">
                {{--<select class="form-control" id="process_type">--}}
                    {{--<option value="">Select Type</option>--}}
                    {{--<option value="1">Month wise</option>--}}
                    {{--<option value="2">Date Wise</option>--}}
                {{--</select>--}}
                {{--<br><br>--}}
                <div class="monthwise_process_show">
                    {{Form::open(array('url' => '/report/employee/salarysheet/private','method' => 'post'))}}
                    <div class="form-group">
                        <label class="form-label">Select Month<span style="color:red;">*</span></label>
                        <div class="prepend-icon">
                            <input type="text" autocomplete="off" name="salary_sheet_month" id="select_salary_sheet_month" class="form-control format_date" required>
                            <i class="icon-calendar"></i>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg" id="salary_sheet_generate">Process</button>
                    {{ Form::close() }}
                </div>
                {{--<div class="date_wise_process_show" style="display: none">--}}
                    {{--{{Form::open(array('url' => '/process/date/wise','method' => 'post'))}}--}}
                    {{--<div class="col-md-6" style="padding: 0px">--}}
                        {{--<div class="form-group">--}}
                            {{--<label>Start Date <span style="color:red;">*</span></label>--}}
                            {{--<input type="text" class="form-control date-picker" name="salary_start_date" autocomplete="off" required>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<label>End Date <span style="color:red;">*</span></label>--}}
                            {{--<input type="text" class="form-control date-picker" name="salary_end_date" autocomplete="off" required>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<button type="submit" name="salary_sheet_date_wise_process" class="btn btn-primary btn-lg" id="salary_sheet_date_wise_process">Process</button>--}}
                    {{--{{ Form::close() }}--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
//            $("#process_type").change(function(){
//                var proces_type=$("#process_type").val();
//                if(proces_type==1){
//                    $(".monthwise_process_show").show();
//                    $(".date_wise_process_show").hide();
//                }
//                if(proces_type==2){
//                    $(".date_wise_process_show").show();
//                    $(".monthwise_process_show").hide();
//                }
//            });
            $('#select_salary_sheet_month').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm-dd', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm-dd', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
        });
    </script>
    <div class="modal fade" data-keyboard="false" data-backdrop="static" id="modelWindow" role="dialog">
        <div class="modal-dialog modal-sm vertical-align-center">
            <div class="modal-content">
                <div class="modal-body load_image">
                    {{ Html::image("hrm_script/images/process.gif", "Logo") }}
                </div>
            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>

    <script>
        $('#salary_sheet_generate').click(function(){
            $('#modelWindow').modal('show');
        });
    </script>
    @include('include.copyright')
@endsection