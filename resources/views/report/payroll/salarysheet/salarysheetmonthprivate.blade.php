@extends('layouts.master')
@section('title', 'Generate Employee Private Salary Sheet')
@section('content')
    <style>
        .fontsss th{
            font-size: 8px;
        }
        .fontsss td{
            font-size: 10px;
        }
        @media print {
            .fontsss th{
                font-size: 8px;
            }
            .fontsss td{
                font-size: 5px;
            }
        }
    </style>
    <div class="page-content">
        @if(Session::has('msg'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('msg') }}</p>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">
                <span style="font-weight: bold;font-size: 14px;">Generate Employee Private Salary Sheet</span>
            </div>
            <div class="panel-body">
                {{Form::open(array('url' => '/report/employee/salary/month/private','method' => 'post','target' => '_blank'))}}
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Select Month <span style="color: darkred;font-size: 16px;">*</span></label>
                        <div class="prepend-icon">
                            <input type="text" autocomplete="off" name="emp_sal_month" id="emp_sal_month" class="form-control format_date" required>
                            <i class="icon-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Payment Date <span style="color: darkred;font-size: 16px;">*</span></label>
                        <div class="prepend-icon">
                            <input type="text" autocomplete="off" name="salary_payment_date" id="salary_payment_date" class="form-control date-picker" required>
                            <i class="icon-calendar"></i>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <button type="submit" id="please_select_group_monthwise" name="salarymontwise" class="btn btn-info">Preview Salary</button>
                    <button type="submit"  name="salary_month_wise_excel" class="btn btn-success">Export Excel</button>
                    {{--<button class="btn  btn-info" onclick="printDiv('print_area')" style="display: none" type="button" id="pdf_btn" class="btn btn-info"> <i class="fa fa-print"></i> Print </button>--}}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#emp_sal_month').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
            setTimeout(function() {
                $('#alert_message').fadeOut('fast');
            }, 5000);
        });
    </script>
    @include('include.copyright')
@endsection