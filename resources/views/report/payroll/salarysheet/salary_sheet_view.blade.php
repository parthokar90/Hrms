@php
    use Carbon\Carbon;
@endphp
@extends('layouts.master')
@section('content')
    {{Form::open(array('url' => '/salary_history_data','method' => 'post'))}}
    <div class="page-content">
        <div class="panel panel-default" style="display: none;">
            <div class="panel-heading">
                <input type="hidden" name="months" value="{{$month}}">
                @if(isset($_POST['salary_sheet_monthsss']))
                    <script>
                        $(function() {
                            $('#myModal').modal('show');
                            $('#myModal').modal({backdrop: 'static', keyboard: false})
                        });
                    </script>
                @endif
            </div>
            <div class="panel-body">
                <table style="width: 100%;display: none;" class="table" border="1px">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>EmpID</th>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Grade</th>
                        <th>Basic</th>
                        <th>House</th>
                        <th>Medical</th>
                        <th>Food</th>
                        <th>Transport</th>
                        <th>Gross</th>
                        <th>Leave</th>
                        <th>WorkDays</th>
                        <th>Present</th>
                        <th>Abs days</th>
                        <th>Weekend</th>
                        <th>Holiday</th>
                        {{--<th>Total payable days</th>--}}
                        <th>Deduction</th>
                        <th>Gross Pay</th>
                        <th colspan="3">
                            <center>Overtime</center>
                        </th>
                        <th>Att Bonus</th>
                        <th>Special allow</th>
                        <th>Net Wages</th>
                        <th>Signature</th>
                        {{----}}
                        {{--<th>Section</th>--}}
                        {{--<th>Line</th>--}}
                        {{--<th>Department</th>--}}
                        {{--<th>Designation</th>--}}
                        {{--<th>Work Group</th>--}}
                        {{--<th>Payment Method</th>--}}
                        {{--<th>Present</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    {{--@foreach($leave_result_two as $all_leave)--}}
                    {{--<input type="hidden" name="em_id[]" class="form-control" value="{{$all_leave->employee_id}}">--}}
                    {{--<input type="hidden" name="lev_id[]" class="form-control" value="{{$all_leave->leave_type_id}}">--}}
                    {{--<input type="hidden" name="lev_val[]" class="form-control" value="{{$all_leave->total}}">--}}
                    {{--<input type="hidden" name="months" value="{{$month}}">--}}
                    {{--@endforeach--}}
                    @php
                        $order=0;
                        function dayCalculator($d1,$d2){
                              $date1=strtotime($d1);
                              $date2=strtotime($d2);
                              $interval1=1+round(abs($date1-$date2)/86400);
                              $festivalLeave = DB::table('tb_festival_leave')->get();
                              $dcount=0;
                              foreach($festivalLeave as $fleave){
                              if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                              $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
                              }
                              else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                              $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
                              }
                              else{
                              continue;
                              }
                              }
                              $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
                              $start_date=date('Y-m-d', $date1);
                              $end_date=date('Y-m-d', $date2);
                              $key=0;
                              for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
                              $dates = $i->format('Y-m-d');
                              $timestamp = strtotime($dates);
                              $day = date('l', $timestamp);
                              for($j=0;$j<count($weekends);$j++)
                              {
                              if($day==$weekends[$j]){
                              $key++;
                              }
                              }
                              }
                              $interval=(int)$interval1-((int)$dcount+$key);
                              return $interval;
                              }

                              function holiday($d1,$d2){
                              $date1=strtotime($d1);
                              $date2=strtotime($d2);
                              $interval1=1+round(abs($date1-$date2)/86400);
                              $festivalLeave = DB::table('tb_festival_leave')->get();
                              $dcount=0;
                              foreach($festivalLeave as $fleave){
                              if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                              $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
                              }
                              else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                              $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
                              }
                              else{
                              continue;
                              }
                              }
                              return $dcount;
                              }









                              function weekdayCalculator($d1,$d2){
                              $date1=strtotime($d1);
                              $date2=strtotime($d2);
                              $interval1=1+round(abs($date1-$date2)/86400);
                              $festivalLeave = DB::table('tb_festival_leave')->get();
                              $dcount=0;
                              foreach($festivalLeave as $fleave){
                              if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                              $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);
                              }
                              else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                              $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);
                              }
                              else{
                              continue;
                              }
                              }
                              $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
                              $start_date=date('Y-m-d', $date1);
                              $end_date=date('Y-m-d', $date2);
                              $key=0;
                              for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
                              $dates = $i->format('Y-m-d');
                              $timestamp = strtotime($dates);
                              $day = date('l', $timestamp);
                              for($j=0;$j<count($weekends);$j++)
                              {
                              if($day==$weekends[$j]){
                              $key++;
                              }
                              }
                              }
                              $interval=(int)$interval1-((int)$dcount+$key);
                              return $key;
                              }



















                    @endphp
                    @foreach($data as $salary_report)
                        @php
                            $order++;
                            $gross=round($salary_report->total_employee_salary,2);
                            $current_month_first=date('Y-m-01',strtotime($workingmonth));
                            $current_month_last=date('Y-m-t',strtotime($workingmonth));
                            $current_month_day=date('t',strtotime($workingmonth));
                            $working_day=dayCalculator($current_month_first,$current_month_last);
                            $absentday=$working_day-$salary_report->total_leaves_taken-$salary_report->total_present;
                            $deduction=round($salary_report->basic_salary/$current_month_day*$absentday,2);
                            $gross_pay=$gross-$deduction-$salary_report->normal_deduction_amount-$salary_report->month_wise_deduction_amount;
                            $actual_gross_pay=round($gross_pay,2);
                            $overtime_rate=$salary_report->basic_salary/208*2;
                            $actual_overtime_rate=round($overtime_rate,2);
                            $overtime_amount=$actual_overtime_rate*$salary_report->overtime;
                            $net_wages=$gross_pay+$overtime_amount+$salary_report->attendance_bonus+$salary_report->bonus_percent+$salary_report->f_bonus+$salary_report->f_amount+$salary_report->production_amount;
                            $actual_net_wages=round($net_wages,2);
                            $current_month_first=date('Y-m-01',strtotime($workingmonth));
                            $current_month_last=date('Y-m-t',strtotime($workingmonth));
                            $weekend=weekdayCalculator($current_month_first,$current_month_last);
                            $holiday=holiday($current_month_first,$current_month_last);
                        @endphp
                        <tr class="active">
                            <td>{{$order}}</td>
                            <td>{{$salary_report->employeeId}}</td>
                            <input type="hidden" name="emp_id[]" value="{{$salary_report->emp_id}}">
                            <td>
                                {{$salary_report->empFirstName}} {{$salary_report->empLastName}}<br>
                                Join: {{$salary_report->empJoiningDate}}
                            </td>
                            <td>{{$salary_report->designation}}</td>
                            <input type="hidden" name="designation_id[]" value="{{$salary_report->empDesignationId}}">
                            <td>{{$salary_report->grade_name}}</td>
                            <input type="hidden" name="grade_id[]" value="{{$salary_report->grade_id}}">
                            <td>{{$salary_report->basic_salary}}</td>
                            <input type="hidden" name="basic_salary[]" value="{{$salary_report->basic_salary}}">
                            <td>{{$salary_report->house_rant}}</td>
                            <input type="hidden" name="house_rant[]" value="{{$salary_report->house_rant}}">
                            <td>{{$salary_report->medical}}</td>
                            <input type="hidden" name="medical[]" value="{{$salary_report->medical}}">
                            <td>{{$salary_report->food}}</td>
                            <input type="hidden" name="food[]" value="{{$salary_report->food}}">
                            <td>{{$salary_report->transport}}</td>
                            <input type="hidden" name="transport[]" value="{{$salary_report->transport}}">
                            <input type="hidden" name="other[]" value="{{$salary_report->other}}">
                            <td>{{$gross}}</td>
                            <input type="hidden" name="gross[]" value="{{$gross}}">
                            <td>{{$salary_report->leave_name_value}}</td>
                            <input type="hidden" name="leave_name_value_data[]" value="{{$salary_report->leave_name_value}}">
                            <input type="hidden" name="total_leaves_taken_valuessss[]" value="{{$salary_report->total_leaves_taken}}">
                            <input type="hidden" name="leave_type_id" value="{{$salary_report->leave_with_id}}">
                            <input type="hidden" name="leave_values" value="{{$salary_report->leave_with_total}}">
                            <td>{{$working_day}}</td>
                            <td>{{$salary_report->total_present}}</td>
                            <td>{{$absentday}}</td>
                            <td>{{$weekend}}</td>
                            <input type="hidden" name="weekend[]" value="{{$weekend}}">
                            <td>{{$holiday}}</td>
                            <input type="hidden" name="holiday[]" value="{{$holiday}}">
                            {{--<td>total payable days</td>--}}
                            <td>
                                <span style="border-bottom: 1px solid #000000;">Absent:</span>
                                {{$deduction}}
                            </td>
                            <td>{{$actual_gross_pay}}</td>
                            <td>
                                <span style="border-bottom: 1px solid #000000;">Hrs:</span>
                                @if($salary_report->overtime=='')
                                    0/hr
                                @else
                                    {{$salary_report->overtime}}/hr
                                @endif
                                <input type="hidden" name="overtime[]" value="{{$salary_report->overtime}}">
                            </td>
                            <td>
                                <span style="border-bottom: 1px solid #000000;"> Rat:</span>
                                {{$actual_overtime_rate}}
                                <input type="hidden" name="overtime_rate[]" value="{{$actual_overtime_rate}}">
                            </td>
                            <td>
                                <span style="border-bottom: 1px solid #000000;">Amt:</span>
                                {{$overtime_amount}}
                                <input type="hidden" name="overtime_amt[]" value="{{$overtime_amount}}">
                            </td>
                            <td>
                                <input type="hidden" name="att_bonus[]" value="{{$salary_report->attendance_bonus}}">
                                <input type="hidden" name="att_bonus_percent[]" value="{{$salary_report->bonus_percent}}">
                                <input type="hidden" name="festival_bonus_percent[]" value="{{$salary_report->f_bonus}}">
                                <input type="hidden" name="festival_bonus_amount[]" value="{{$salary_report->f_amount}}">
                                <input type="hidden" name="increment_bonus_percent[]" value="{{$salary_report->special_bonus}}">
                                <input type="hidden" name="increment_bonus_amount[]" value="{{$salary_report->emp_amount}}">
                                <input type="hidden" name="normal_deduction_amount[]" value="{{$salary_report->normal_deduction_amount}}">
                                <input type="hidden" name="advanced_deduction_amount[]" value="{{$salary_report->month_wise_deduction_amount}}">
                                <input type="hidden" name="production_bonus[]" value="{{$salary_report->production_amount}}">
                                @if($salary_report->attendance_bonus=='')
                                    0
                                @else
                                    {{$salary_report->attendance_bonus}}
                                @endif
                            </td>
                            <td>
                                <input type="hidden" name="special_bonus[]" value="{{$salary_report->special_bonus}}">
                                @if($salary_report->special_bonus=='')
                                    0
                                @else
                                    {{$salary_report->special_bonus}}
                                @endif
                            </td>
                            <input type="hidden" name="present[]" value="{{$salary_report->total_present}}">
                            <input type="hidden" name="absent[]" value="{{$absentday}}">
                            <input type="hidden" name="absent_amt[]" value="{{$deduction}}">
                            <input type="hidden" name="gross_pay[]" value="{{$gross_pay}}">
                            <input type="hidden" name="work_day[]" value="{{$working_day}}">
                            <input type="hidden" name="net_amount[]" value="{{$actual_net_wages}}">
                            <td>{{$actual_net_wages}}</td>
                            {{--<td>{{$salary_report->name}}</td>--}}
                            {{--<td>{{$salary_report->floor}}</td>--}}
                            {{--<td>{{$salary_report->empSection}}</td>--}}
                            {{--<td>{{$salary_report->line_no}}</td>--}}
                            {{--<td>{{$salary_report->departmentName}}</td>--}}
                            {{--<td>{{$salary_report->work_group}}</td>--}}
                            {{--<td>{{$salary_report->payment_mode}}</td>--}}
                            {{--<td>--}}
                            {{--@if($salary_report->total_present=='')--}}
                            {{--0--}}
                            {{--@else--}}
                            {{--{{$salary_report->total_present}}--}}
                            {{--@endif--}}
                            {{--</td>--}}
                            <td></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="myModal" style="margin-left: 100px;" class="modal fade" data-keyboard="false" data-backdrop="static" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <img style="display: none;margin-left: 5px; text-align: center;" id="load_img_data" width="400px" height="400px" src="../../hrm_script/images/designation.gif">
                <?php
                if(isset($_POST['salary_sheet_month'])){
                    echo '<center><span style="font-size: 16px">'.'Salary of'.'&nbsp' .date('M-Y',strtotime($_POST['salary_sheet_month'])).'</span></center>';
                }
                ?>
                <div class="modal-footer text-center">
                    <button id="save_his_data" type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
<script>
    $('#save_his_data').click(function(){
        $("#save_his_data").hide()
        $("#load_img_data").show();
    });
</script>
@endsection