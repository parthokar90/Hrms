@extends('layouts.master')
@section('title', 'Date Wise Salary')
@section('content')
    <div class="page-content">
        <div class="row panel"  style="border:1px solid #dcdcdc;">
            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="panel-heading" >
                    <b>Date Wise Salary</b>
                    <hr>
                </div>
                <div class="panel-body">
                    {{Form::open(array('action' => 'datesalarycontroller@datewisesalaryreportShow','method' => 'post'))}}
                    <div class="col-md-6">
                        <label>Start date</label>
                        <div class="form-group">
                            <input type="text" class="form-control date-picker" name="start_date" autocomplete="off" required>
                        </div>
                        <br><label>End date</label>
                        <div class="form-group">
                            <input type="text" class="form-control date-picker" name="end_date" autocomplete="off" required>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary">Generate</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection
