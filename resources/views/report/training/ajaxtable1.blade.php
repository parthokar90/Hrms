@if(count($trainingwiseEmployee)==0)
<h4><p class="text-center">No Training available between given dates</p></h4>
@else

        <div>
            <h3><i class="fa fa-table"></i> <strong>Training List </strong></h3>
        </div>
        <div class="panel-content">
          
                <table id="leave_display" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Order</th>
                        <th>Training Name</th>
                        <th>Starting Date</th>
                        <th>Ending Date</th>
                        <th>Attendents</th>
                        <th>Status</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    
                    @php $order=0; @endphp
                    @foreach($trainingwiseEmployee as $data)
                    @php
                    if(($data->training_starting_date)>=date("Y-m-d")&&($data->training_ending_date)<=date("Y-m-d")){
                        $status='Active';
                    }
                    else if(($data->training_starting_date)<=date("Y-m-d")&&($data->training_ending_date)<=date("Y-m-d")){
                        $status='Completed';
                    }
                    else if(($data->training_starting_date)>=date("Y-m-d")&&($data->training_ending_date)>=date("Y-m-d")){
                        $status='Upcoming';
                    }
                    else if(($data->training_starting_date)<=date("Y-m-d")&&($data->training_ending_date)>=date("Y-m-d")){
                        $status='Active';
                    }
                   
                    //$sdate=date("d-m-Y",$data->training_starting_date);
                    $order++; 
                    @endphp
                    <tr>
                    <td>{{$order}}</td>
                    <td>{{$data->training_name}}</td>
                    <td>{{date("d M Y",strtotime($data->training_starting_date))}}</td>
                    <td>{{date("d M Y",strtotime($data->training_ending_date))}}</td>
                    <td>{{$data->employee_name}}</td>
                    <td>{{$status}}</td>
                    </tr>
                @endforeach
                    </tbody>
                </table>
                
        </div>
   
    @endif