@extends('layouts.master')
@section('title', 'On Going Training List')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>On Going Training </strong> Report</h3>
                    </div>
                    <div class="panel-content">
                      <a href="{{url('report/training')}}" class="btn btn-sm btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                      @if(count($training)==0)
                      <h4><p class="text-center">No Ongoing Training Available</p></h4>
                      @else
                      <a href="{{url('report/training/ongoingTraining/pdf')}}"<button type="button" class="btn btn-success">Download Report</button></a>
                        <h1 class="text-center">Ongoing Training Report</h1>
                        <p class="text-center"><strong> Date: </strong><?php echo date('d-M-Y'); ?></p>
                       
                       
                            <table id="leave_display" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Training</th>
                                    <th>Starting Date</th>
                                    <th>ending Date</th>
                                    <th>Attendents</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                
                                @php $order=0; @endphp
                                @foreach($training as $data)
                        
                                @php $order++; @endphp
                                <tr>
                                <td>{{$order}}</td>
                                <td>{{$data->training_name}}</td>
                                <td>{{date("d M Y",strtotime($data->training_starting_date))}}</td>
                                <td>{{date("d M Y",strtotime($data->training_ending_date))}}</td>
                                <td>{{$data->employee_name}}</td>
                                </tr>
                            @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection