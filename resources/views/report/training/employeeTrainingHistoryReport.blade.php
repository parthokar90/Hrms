@extends('layouts.master')
@section('title', 'Employee Training History Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Employee Training History </strong> </h3>
                    </div>
                    <div class="panel-content">
                      <a href="{{url('report/training')}}" class="btn btn-sm btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                      <a href="{{url('report/pdf/employeeTrainingHistory')}}"<button type="button" class="btn btn-success">Download Report</button></a>
                      @if(count($training)==0)
                     
                      <h3><p class="text-center">No Employee Training History Available</p></h3>
                      @else
                      
                        <h1 class="text-center">Employee Training History</h1>
                        <p class="text-center"><strong> Date: </strong><?php echo date('d-M-Y'); ?></p>
                       
                        
                            <table id="leave_display" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Employee ID</th>
                                    <th>Name</th>
                                    <th>Designation</th>
                                    
                                    <th>Completed Trainings</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                
                                @php $order=0; @endphp
                                @foreach($training as $data)
                        
                                @php $order++; @endphp
                                <tr>
                                <td>{{$order}}</td>
                                <td>{{$data->employeeId}}</td>
                                <td>{{$data->empFirstName}} {{$data->empLastName}}</td>
                                <td>{{$data->designation}}</td>
                                
                                <td>{{$data->training_name}}</td>
                                </tr>
                            @endforeach
                            @foreach($training2 as $data)
                        
                                @php $order++; @endphp
                                <tr>
                                <td>{{$order}}</td>
                                <td>{{$data->employeeId}}</td>
                                <td>{{$data->empFirstName}} {{$data->empLastName}}</td>
                                <td>{{$data->designation}}</td>
                                
                                <td>No Training Taken</td>

                                </tr>

                                </tbody>
                                @endforeach
                            </table>
                            
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection