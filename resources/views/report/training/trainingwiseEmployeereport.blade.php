@extends('layouts.master')
@section('title', 'Training Wise Employee List Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Training </strong> Report</h3>
                    </div>
                    <div class="panel-content">
                        <a href="{{url('report/training')}}" class="btn btn-sm btn btn-default"><i class="fa fa-arrow-left"> Back</i></a>
                        <div class="row" id='get-form'>
                         {{Form::open(array('url' => '/report/training/trainingWiseEmployeePDF','method' => 'get'))}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="required form-label">Start Date</label>
                                    <div class="prepend-icon">
                                        <input type="text" id="training_start" name="training_start" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="required form-label">End Date</label>
                                    <div class="prepend-icon">
                                        <input type="text" id="training_end" name="training_end" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Choose Training</label>
                                       
                                        <div class="option-group">
                                            {!! Form::select('training_id',['0'=>'All']+$training_name,null,['class' => 'form-control','data-search'=>'true'])!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                            
                        </div>
                        <div class="row">
                            <div class="col-md-7 center-block">
                                <button type="button" id="training_report_generate" value="report" class="btn btn-embossed btn-info">Generate Report</button>
                                <button type="submit" id="training_report_pdf" value="pdf" class="btn btn-success">Download Report</button></a>
                            </div>
                            <input type="hidden" id="report-url_hidden_id" value="{{URL::to('/report/training/trainingWiseEmployeeAjax')}}">
                            {{ Form::close() }}
                        </div>
                        
                            <div id="table">
                                <div class="col-sm-12">
                                    @if(isset($trainingwiseEmployee))
                                        @include('report.training.ajaxtable1')
                                    @endif
                                </div>
                            </div>
    
                        
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
    <script>
            $("#training_report_generate").click(function(){
                {{--  $(this).preventDefault();  --}}
                var url=$("#report-url_hidden_id").val();
            
                var form=$('#get-form').find('form');
                //alert(url);
                $.ajax({
                    url: url, 
                    method:'GET',
                    data: form.serialize(),
                    dataType:'html',
                    success: function (data) {
                        $('#table').html(data);
    
                    },
                    error:function (xhr) {
                        console.log('failed');    
                    }
                });

            });

    </script>
@endsection