@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Training List Report')
@section('content')
    <div class="page-content page-content-hover-1">
        <div class="row panel myAnimation_1"  style="border:1px solid #dcdcdc;">

            <div class="col-xlg-12 col-lg-12  col-sm-12 animated bounceInRight">
                <div class="text-center" >
                    <h2><b>Training Report</b></h2>
                    <hr class="animated bounceInLeft">
                </div>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="training/ongoingTraining">
              <div class="panel">
                <div class="panel-content training_ogt">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-dashboard f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>On Going Training </b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a target="_BLANK" href="training/completedTraining">
              <div class="panel">
                <div class="panel-content training_ctl">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-check-circle f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b> Completed Training List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="training/trainingwiseEmployee">
              <div class="panel">
                <div class="panel-content training_twel">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Training Wise Employee List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a target="_BLANK" href="training/employeeHistory">
              <div class="panel">
                <div class="panel-content training_eth">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-street-view f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b> Employee Training History</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>


        <div class="col-md-12"><hr class="animated bounceInRight"></div>

        </div>
    </div>
@include('include.copyright')
@endsection