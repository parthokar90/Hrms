@php
use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Employee Report')
@section('content')
    <div class="page-content page-content-hover-1">
        <div class="row panel myAnimation_1"  style="border:1px solid #dcdcdc;">

            <div class="col-xlg-12 col-lg-12  col-sm-12 animated bounceInRight">
                <div class="text-center" >
                    <h2><b>Employee Report</b></h2>
                    <hr class="animated bounceInLeft">
                </div>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="{{route('report.search_employee')}}">
              <div class="panel">
                <div class="panel-content emp_sel" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-search f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Search Employee Profile</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a target="_BLANK" href="{{url('/report/employee_list')}}">
              <div class="panel">
                <div class="panel-content emp_elist" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b> Employee List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="{{url('/report/employee_list_status')}}">
              <div class="panel">
                <div class="panel-content emp_emlbas" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-exclamation-circle f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Employee List By Account Status</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a target="_BLANK" href="{{url('/report/employee_list_gender')}}">
              <div class="panel">
                <div class="panel-content emp_elbg" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-mars-double f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>Employee List By Gender</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInLeft">
              <a target="_BLANK" href="{{url('/report/employee_list_department')}}">
              <div class="panel">
                <div class="panel-content emp_elbd" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-server f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>  Department Wise Employee List </b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceIn">
              <a target="_BLANK" href="{{url('/report/employee_list_designation')}}">
              <div class="panel">
                <div class="panel-content emp_dwel" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-list-ul f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>  Designation Wise Employee List </b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>


            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceIn">
              <a target="_BLANK" href="{{url('report/floor_line_employee_list')}}">
              <div class="panel">
                <div class="panel-content emp_flwel" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-exchange f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b>  Floor/Line Wise Employee List </b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>


            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
              <a target="_BLANK" href="{{url('report/section_employee_list')}}">
              <div class="panel">
                <div class="panel-content emp_elsec" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-codepen f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b> Section wise Employee List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a target="_BLANK" href="{{url('/report/new_employee_list')}}">
              <div class="panel">
                <div class="panel-content emp_elist" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b> New Employee List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a target="_BLANK" href="{{url('/report/inactive_employee_list_report')}}">
              <div class="panel">
                <div class="panel-content emp_emlbas" style="">
                    <center>
                      <div class="row" >
                        <div class="">
                            <center><i class="fa fa-users f-40"></i></center>
                        </div>
                        <br>
                        <div class="">
                        <center>
                          <span class="f-14"><b> Inactive Employee List</b></span>
                          </center>
                        </div>
                      </div>
                    </center>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
                <a target="_BLANK" href="{{url('/report/resign_lefty_employee_list')}}">
                    <div class="panel">
                        <div class="panel-content emp_emlbas" style="">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-users f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b> Resign/Lefty Employee List</b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>


        <div class="col-md-12 animated bounceInUp">
          <hr>
        </div>
        </div>
    </div>
  
    @include('include.copyright')
@endsection