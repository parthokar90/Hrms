@extends('layouts.master')
@section('title', 'Inactive Employee Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i> Inactive <strong>Employee </strong> List</h3>
                            </div>
                        
                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered animated fadeIn">
                            <thead style="font-size:15px;font-weight:bold;">
                            <tr>
                                <th>emp id</th>
                                <th>Name</th>
                                @if(!empty($request->coldesignation))
                                <th>Designation</th>
                                @endif
                                @if(!empty($request->coldepartment))
                                <th>Department</th>
                                @endif
                                <th>Joining Date</th>
                                <th>Date of Discontinuation</th>
                                @if(!empty($request->colgender))
                                <th>Gender</th>
                                @endif
                                @if(!empty($request->colunit))
                                <th>Unit</th>
                                @endif
                                @if(!empty($request->colfloor))
                                <th>Floor</th>
                                @endif
                                @if(!empty($request->colline))
                                <th>Line</th>
                                @endif
                                @if(!empty($request->colsection))
                                <th>Section</th>
                                @endif
                                @if(!empty($request->colstatus))
                                <th>Status</th>
                                @endif
                                @if(!empty($request->coldob))
                                <th>DOB</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($employees as $employee)
                            <tr>
                                <td><b><a href="{{route('employee_profile.show',base64_encode($employee->id))}}" target="_blank">{{$employee->employeeId}}</a></b></td>
                                <td>{{$employee->empFirstName}}</td>
                                @if(!empty($request->coldesignation))
                                <td>{{$employee->designation}}</td>
                                @endif
                                @if(!empty($request->coldepartment))
                                <td>{{$employee->departmentName}}</td>
                                @endif
                                <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('d-m-Y')}}</td>
                                <td>{{\Carbon\Carbon::parse($employee->date_of_discontinuation)->format('d-m-Y')}}</td>
                                @if(!empty($request->colgender))
                                <td>
                                    @if($employee->empGenderId==1)
                                        Male
                                    @endif
                                    @if($employee->empGenderId==2)
                                        Female
                                    @endif
                                    @if($employee->empGenderId==3)
                                        Other
                                    @endif
                                </td>
                                @endif
                                @if(!empty($request->colunit))
                                    <td>{{$employee->unitName}}</td>
                                @endif
                                @if(!empty($request->colfloor))
                                    <td>{{$employee->floorName}}</td>
                                @endif
                                @if(!empty($request->colline))
                                <td>{{$employee->LineName}}</td>
                                @endif
                                @if(!empty($request->colsection))
                                <td>{{$employee->empSection}}</td>
                                @endif
                                @if(!empty($request->colstatus))
                                <td>
                                    @if($employee->empAccStatus==1)
                                        Active
                                    @elseif($employee->empAccStatus==0)
                                        Inactive
                                    @endif
                                </td>
                                @endif
                                @if(!empty($request->coldob))
                                <td>{{\Carbon\Carbon::parse($employee->empDOB)->format('d-m-Y')}}</td>
                                @endif
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @include('include.copyright')
@endsection