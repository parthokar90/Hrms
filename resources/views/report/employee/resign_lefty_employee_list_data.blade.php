<style>

    .reportHeaderArea{
        text-align: center;
        margin-bottom: 25px;
        margin-top: -5px;
    }

    .reportHeader{
        line-height: 4px;
    }

    .reportHeader{
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        font-size: 10px;
    }

    .reportHeaderCompany{
        font-size: 18px !important;

    }

</style>
<div class="reportHeaderArea">
    <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
    <p class="reportHeader">{{$companyInformation->company_address1}}</p>
    <p class="reportHeader">{{$companyInformation->company_email}}</p>
    <p class="reportHeader">{{$companyInformation->company_phone}}</p>
</div>
<table border="1" class="table table-hover table-striped table-bordered" style="background:#fff;border-collapse: collapse;" width="100%">
    <thead style="font-size:13px;font-weight:bold;">
    <tr>
        <th>Serial No.</th>
        <th>ID</th>
        <th>Name</th>
        @if(!empty($request->coldesignation))
            <th>Designation</th>
        @endif
        @if(!empty($request->coldepartment))
            <th>Department</th>
        @endif
        @if(!empty($request->coljoiningdate))
            <th>Joining Date</th>
        @endif
        @if(!empty($request->colgender))
            <th>Gender</th>
        @endif
        @if(!empty($request->colunit))
            <th>Branch</th>
        @endif
        @if(!empty($request->colsection))
            <th>Section</th>
        @endif
        @if(!empty($request->colskill_level))
            <th>Skill</th>
        @endif

        @if(!empty($request->colstatus))
            <th>Discontinuation Type</th>
        @endif
        @if(!empty($request->coldob))
            <th>DOB</th>
        @endif
        @if(!empty($request->colsalary))
            <th>Salary</th>
        @endif
        <th>Separation Date</th>
        <th>Remarks</th>

    </tr>
    </thead>
    <tbody style="font-size:11px;">
    @if(count($employees)!=0)
    @foreach($employees as $key=>$employee)
        <tr>
            <td align="center">{{++$key}}</td>
            <td style="text-align: center;">{{$employee->employeeId}}</td>
            <td>{{$employee->empFirstName}}</td>
            @if(!empty($request->coldesignation))
                <td>{{$employee->designation}}</td>
            @endif
            @if(!empty($request->coldepartment))
                <td>{{$employee->departmentName}}</td>
            @endif
            @if(!empty($request->coljoiningdate))
                <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('d-m-Y')}}</td>
            @endif
            @if(!empty($request->colgender))
                <td style="text-align: center;">
                    @if($employee->empGenderId==1)
                        Male
                    @endif
                    @if($employee->empGenderId==2)
                        Female
                    @endif
                    @if($employee->empGenderId==3)
                        Other
                    @endif
                </td>
            @endif
            @if(!empty($request->colunit))
                <td>{{$employee->unitName}}</td>
            @endif
            @if(!empty($request->colsection))
                <td>{{$employee->empSection}}</td>
            @endif
            @if(!empty($request->colskill_level))
                <td>{{$employee->skill_level}}</td>
            @endif
            @if(!empty($request->colstatus))
                <td>
                    @if($employee->discon_type==1)
                        Lefty
                    @elseif($employee->discon_type==2)
                        Resigned
                    @endif
                </td>
            @endif

            @if(!empty($request->coldob))
                <td>{{\Carbon\Carbon::parse($employee->empDOB)->format('d-m-Y')}}</td>
            @endif
            @if(!empty($request->colsalary))
                <td>{{($employee->basic_salary+$employee->house_rant+$employee->medical+$employee->transport+$employee->food+$employee->other)}}</td>
            @endif

            <td>{{(\Carbon\Carbon::parse($employee->date_of_discontinuation)->format('d-m-Y'))}}</td>
            <td></td>

        </tr>
    @endforeach
    @else
        <tr>
            <td align="center" colspan="8">
                <h2 style="color: red">No Data Found</h2>
            </td>
        </tr>

    @endif
    </tbody>
</table>