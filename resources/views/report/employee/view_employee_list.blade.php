@extends('layouts.master')
@section('title', 'Employee Report')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 ><i class="fa fa-users "></i> <strong>Employee </strong> List</h3>
                            </div>
                            {!! Form::open(['target'=>'_blank','method'=>'post']) !!}
                            <div class="col-md-4" style="margin-top:8px;">
                                <input type="hidden" name="reportType" value="{{$request->reportType}}">
                                <input type="hidden" name="date" value="{{$request->date}}">
                                <input type="hidden" name="unitId" value="{{$request->unitId}}">
                                <input type="hidden" name="floorId" value="{{$request->floorId}}">
                                <input type="hidden" name="line_id" value="{{$request->line_id}}">
                                <input type="hidden" name="departmentId" value="{{$request->departmentId}}">
                                <input type="hidden" name="designationId" value="{{$request->designationId}}">
                                <input type="hidden" name="accStatus" value="{{$request->accStatus}}">
                                <input type="hidden" name="empGenderId" value="{{$request->empGenderId}}">
                                <input type="hidden" name="skill_level" value="{{$request->skill_level}}">
                                <input type="hidden" name="sectionName" value="{{$request->sectionName}}">
                                @if(isset($request->colgender))
                                    <input type="hidden" name="colgender" value="{{$request->colgender}}">
                                @endif
                                @if(isset($request->coldepartment))
                                    <input type="hidden" name="coldepartment" value="{{$request->coldepartment}}">
                                @endif
                                @if(isset($request->coldesignation))
                                    <input type="hidden" name="coldesignation" value="{{$request->coldesignation}}">
                                @endif
                                @if(isset($request->colsection))
                                    <input type="hidden" name="colsection" value="{{$request->colsection}}">
                                @endif
                                @if(isset($request->coljoiningdate))
                                    <input type="hidden" name="coljoiningdate" value="{{$request->coljoiningdate}}">
                                @endif
                                @if(isset($request->colskill_level))
                                    <input type="hidden" name="colskill_level" value="{{$request->colskill_level}}">
                                @endif
                                @if(isset($request->colattstatus))
                                    <input type="hidden" name="colattstatus" value="{{$request->colattstatus}}">
                                @endif
                                @if(isset($request->coldob))
                                    <input type="hidden" name="coldob" value="{{$request->coldob}}">
                                @endif
                                @if(isset($request->colotstatus))
                                    <input type="hidden" name="colotstatus" value="{{$request->colotstatus}}">
                                @endif
                                @if(isset($request->colsalary))
                                    <input type="hidden" name="colsalary" value="{{$request->colsalary}}">
                                @endif
                                @if(isset($request->colline))
                                    <input type="hidden" name="colline" value="{{$request->colline}}">
                                @endif
                                @if(isset($request->colfloor))
                                    <input type="hidden" name="colfloor" value="{{$request->colfloor}}">
                                @endif
{{--                                <input type="hidden" name="pageOrientation" value="{{$request->pageOrientation}}">--}}
{{--                                <input type="hidden" name="pagesize" value="{{$request->pagesize}}">--}}
{{--                                <input type="hidden" name="extensions" value="{{$request->extensions}}">--}}
                                <button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary btn-round btn-sm"  style="float:right;" ><i class="fa fa-file-pdf-o"></i> &nbsp Download PDF</button>
                                {{--                                <a href="#" data-toggle="modal" data-target="#search_employee" class="btn btn-success btn-round btn-sm"  style="float:right;" ><i class="fa fa-search"></i> Search Employee </a>--}}
                            </div>
                            {!! Form::close() !!}
                        
                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead style="font-size:15px;font-weight:bold;">
                            <tr>
                                <th>emp id</th>
                                <th>Name</th>
                                @if(!empty($request->coldesignation))
                                <th>Designation</th>
                                @endif
                                @if(!empty($request->coldepartment))
                                <th>Department</th>
                                @endif
                                @if(!empty($request->coljoiningdate))
                                <th>Joining Date</th>
                                @endif
                                @if(!empty($request->colgender))
                                <th>Gender</th>
                                @endif
                                @if(!empty($request->colunit))
                                <th>Unit</th>
                                @endif
                                @if(!empty($request->colfloor))
                                <th>Floor</th>
                                @endif
                                @if(!empty($request->colline))
                                <th>Line</th>
                                @endif
                                @if(!empty($request->colsection))
                                <th>Section</th>
                                @endif
                                @if(!empty($request->colskill_level))
                                <th>Skill/Process</th>
                                @endif
                                @if(!empty($request->colstatus))
                                <th>Status</th>
                                @endif
                                @if(!empty($request->coldob))
                                <th>DOB</th>
                                @endif
                                @if(!empty($request->colsalary))
                                <th>Salary</th>
                                @endif
                                @if(!empty($request->colattstatus))
                                <th>Att. Status</th>
                                @endif
                                @if(!empty($request->colotstatus))
                                <th>OT Status</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($employees as $employee)
                            <tr>
                                <td><b><a href="{{route('employee_profile.show',base64_encode($employee->id))}}" target="_blank">{{$employee->employeeId}}</a></b></td>
                                <td>{{$employee->empFirstName}}</td>
                                @if(!empty($request->coldesignation))
                                <td>{{$employee->designation}}</td>
                                @endif
                                @if(!empty($request->coldepartment))
                                <td>{{$employee->departmentName}}</td>
                                @endif
                                @if(!empty($request->coljoiningdate))
                                <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('d-m-Y')}}</td>
                                @endif
                                @if(!empty($request->colgender))
                                <td>
                                    @if($employee->empGenderId==1)
                                        Male
                                    @endif
                                    @if($employee->empGenderId==2)
                                        Female
                                    @endif
                                    @if($employee->empGenderId==3)
                                        Other
                                    @endif
                                </td>
                                @endif
                                @if(!empty($request->colunit))
                                    <td>{{$employee->unitName}}</td>
                                @endif
                                @if(!empty($request->colfloor))
                                    <td>{{$employee->floorName}}</td>
                                @endif
                                @if(!empty($request->colline))
                                <td>{{$employee->LineName}}</td>
                                @endif
                                @if(!empty($request->colsection))
                                <td>{{$employee->empSection}}</td>
                                @endif
                                @if(!empty($request->colskill_level))
                                <td>{{$employee->skill_level}}</td>
                                @endif
                                @if(!empty($request->colstatus))
                                <td>
                                    @if($employee->empAccStatus==1)
                                        Active
                                    @elseif($employee->empAccStatus==0)
                                        Inactive
                                    @endif
                                </td>
                                @endif
                                @if(!empty($request->coldob))
                                <td>{{\Carbon\Carbon::parse($employee->empDOB)->format('d-m-Y')}}</td>
                                @endif
                                @if(!empty($request->colsalary))
                                <td>{{($employee->basic_salary+$employee->house_rant+$employee->medical+$employee->transport+$employee->food+$employee->other)}}</td>
                                @endif
                                @if(!empty($request->colattstatus))
                                <td><?php if($employee->empAttBonusId==0){ echo "No"; }else{ echo $employee->bTitle; } ?></td>
                                @endif
                                @if(!empty($request->colotstatus))
                                <td> 
                                    <?php if($employee->empOTStatus==1){ echo "Yes";} ?>
                                    <?php if($employee->empOTStatus==0){ echo "No";} ?>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @include('include.copyright')
@endsection