<div class="sidebar">
    <div class="logopanel">
        <h1>
            <a href="{{url('/dashboard')}}"><img src="{{asset("/file/fin.png")}}"></a>
        </h1>
    </div>
    <div class="sidebar-inner">
        <div class="sidebar-top">
            <div class="userlogged clearfix">
                <i class="icon icons-faces-users-01"></i>
                <div class="user-details">
                    <h4>{{ Auth::user()->name }}</h4>
                    <div class="dropdown user-login">
                        <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300">
                            <i class="online"></i><span>Online</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <ul  id="nav" class="nav nav-sidebar">
            <li class=""><a href="{{url('/dashboard')}}"><i class="icon-home"></i><span>Dashboard</span></a></li>
            @if(checkPermission(['admin']))
                <li class="nav-parent ">
                    <a href="#"><i class="fa fa-users"></i><span>Employee</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/employee')}}"> Employee List</a></li>
                        <li><a href="{{route('employee.create')}}"> Create Employee</a></li>
                        <li><a href="{{route('employee.delete_employee_list')}}"> Delete Employee</a></li>
                        <li><a href="{{url('employees/shift/')}}"> Employee Shift</a></li>
                        <li><a href="{{route('multiple_emp_shift')}}">Datewise Shift</a></li>
                        <li><a href="{{route('multiple_emp_shift_list')}}">Shift History</a></li>
                        {{-- <li><a href="{{route('emp_three_shift_add')}}">Add three shift</a></li> --}}
                        
                    </ul>
                </li>

                <li class="nav-parent">
                    <a href="#"><i class="fa fa-suitcase"></i><span>Recruitment</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('vacancy.index')}}"> Vacancy List</a></li>
                        <li><a href="{{route('vacancy.create')}}"> Vacancy Announcement</a></li>
                        <li><a href="{{route('application.create')}}"> New Job Application</a></li>
                        <li><a href="{{route('application.index')}}"> Job Application List</a></li>
                        <li><a href="{{route('recruitment.status')}}"> Daily Recruitment Status</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-credit-card"></i><span>Payroll</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/grade')}}">Grade</a></li>
                        <li><a href="{{url('/salary_view')}}">Salary</a></li>
                        <li><a href="{{url('/bonus')}}">Bonus</a></li>
                        <li><a href="{{url('/manual/overtime')}}">Manual Overtime</a></li>
                        <li><a href="{{url('employee/salary/increment')}}">Increment Salary</a></li>
                        <li><a href="{{url('/advance')}}">Loan</a></li>
                        <li><a href="{{url('/advance/salary')}}">Advance Salary</a></li>
                        <li><a href="{{url('/penalty')}}">Penalty</a></li>
                        <li><a href="{{url('/report/payroll/employee/salary/sheet/save')}}">Process Salary</a></li>
                        {{-- <li><a href="{{url('/report/payroll/employee/salary/sheet/private')}}">Process Salary Private</a></li> --}}
                        <li><a href="{{url('/check/process/month')}}">Check Process</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-calendar"></i><span>Attendance</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/attendance')}}">Upload</a></li>
                        <li><a href="{{route('files.show')}}">Attendance Files</a></li>
                        <li><a href="{{route('report.daily_absent_report')}}">Daily Absent Report</a></li>
                        <li><a href="{{route('report.public.daily_attendance')}}">Daily Attendance Sheet</a></li>
                        <li><a href="{{route('attendance.manual_attendance')}}">Manual Attendance</a></li>
                        <li><a href="{{route('summary_report_date')}}">Summary Report</a></li>
                        {{--<li><a href="{{route('attendance.manual_store_date')}}">Manual Attendance Date Wise</a></li>--}}
                        {{--<li><a href="{{route('attendance.manual.edit')}}">Manual Edit Attendance</a></li>--}}
                        <li><a href="{{route('report.daily_attendance_summery')}}">Daily Attendance Summary</a></li>
                        <li><a href="{{route('employee.continued_absent_list')}}"> Continued Absent List</a></li>
                        <li><a href="{{route('employee.temporary_employee_proxy.list')}}"> Manage Temp. Proxy </a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-support"></i><span>Leave</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/leave/assign_leave')}}">Manually Asssign Leave</a></li>
                        <!-- <li><a href="{{url('/request_leave')}}">Apply for Leave</a></li> -->
                        <li><a href="{{url('/leave')}}"> Leave Requests</a></li>
                        <li><a href="{{url('/leave/maternityLeaveDetails')}}">Maternity Leave Details</a></li>
                        <li><a href="{{url('/leave/earnLeaveDetails')}}">Earn Leave Details</a></li>
                        <li><a href="{{url('/leave_sett')}}"> Leave Settings</a></li>
                        <li><a href="{{route('earn_leave.add')}}"> Add Earn Leave</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="icon-note"></i><span>Training</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/add_training')}}">Add Training</a></li>
                        <li><a href="{{url('/assign/training')}}">Assign Training</a></li>
                        <li><a href="{{url('/training_history')}}">Training History</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-comments"></i><span>Meetings</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('meeting.index')}}"> Meeting List</a></li>
                        <li><a href="{{route('meeting.create')}}"> Create Meeting</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-bullhorn"></i><span>Notices</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('notices.index')}}"> Notices List</a></li>
                        <li><a href="{{route('notices.create')}}"> Create New Notice</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-money"></i><span>Expenses</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/expense/index')}}">Add Expenses</a></li>
                        <li><a href="{{url('/expense/categories')}}">Expense Categories</a></li>
                        <li><a href="{{url('/expense/selectDate')}}">History</a></li>
                        <li><a href="{{route('expense.report')}}">Expense Report</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-lock"></i><span>Booking Management</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('booking.list')}}">Booking List</a></li>
                        <li><a href="{{route('booking.new_request')}}">Booking Request</a></li>
                        <li><a href="{{route('booking.bookingitems')}}">Booking Item</a></li>
                        <li><a href="{{route('booking.report_view')}}">Report</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-car"></i><span>Vehicle Management</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('vehicle.list')}}">Vehicle List</a></li>
                        <li><a href="{{route('vehicle.booking')}}">Vehicle Booking</a></li>
                        <li><a href="{{route('vehicle.expenses')}}">Today's Vehicle Expenses</a></li>
                        <li><a href="{{route('vehicle.excatlist')}}">Expense Category</a></li>
                        <li><a href="{{route('vehicle.report_view')}}">Report</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-file"></i><span>Report</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/report/employee')}}">Employee</a></li>
                        <li><a href="{{url('/report/payroll')}}">Payroll</a></li>
                        <li><a href="{{url('/report/attendance')}}">Attendance</a></li>
                        <li><a href="{{url('/report/leave')}}">Leave</a></li>
                        <li><a href="{{url('/report/training')}}">Training</a></li>
                        <li><a href="{{url('/report/recruitment')}}">Recruitment</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-cog"></i><span>Settings</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/user_settings')}}">User</a></li>
                        <li><a href="{{url('/attendance/settings')}}">Work Shifts</a></li>
                        <li><a href="{{url('bonus/settings')}}">Bonus Setting</a></li>
                        <li><a href="{{url('/attendance_bonus_category')}}">Attendance Bonus category</a></li>
                        {{-- <li><a href="{{route('overtime.index')}}">Overtime</a></li> --}}
                        {{-- <li><a href="{{route('overtime-hour-count.index')}}">Overtime Hour Count</a></li> --}}
                        <li><a href="{{route('unit.index')}}">Unit List</a></li>
                        <li><a href="{{route('floor.index')}}">Floor List</a></li>
                        <li><a href="{{route('line.index')}}">Line List</a></li>
                        <li><a href="{{route('country.index')}}">Country List</a></li>
                        <li><a href="{{route('maritialstatus.index')}}">Marital Status</a></li>
                        <li><a href="{{route('department.index')}}">Departments</a></li>
                        <li><a href="{{route('designation.index')}}">Designation</a></li>
                        <li><a href="{{route('weekend.list')}}">Weekend Setup</a></li>
                        <li><a href="{{url('/company/information')}}">Company Information</a></li>
                        <!-- <li><a href="{{url('/system/settings')}}">Logo & Copyright</a></li> -->
                        <li><a href="{{url('/system/style')}}">Change Theme Style</a></li>
                        <li><a href="{{route('access.index')}}">Access Logs</a></li>
                        <li><a href="{{route('activity_logs.history_view')}}">Activity Logs</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-envelope"></i><span>Manual SMS/Email</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/manual_sms')}}">Send SMS</a></li>
                        <li><a href="{{url('/manual_bulk_sms')}}">Send Bulk SMS</a></li>
                        <li><a href="{{url('/manual_email')}}">Send Email</a></li>
                    </ul>
                </li>
            @endif
            @if(checkPermission(['hr']) || checkPermission(['hr-admin']))
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-users"></i><span>Employee</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/employee')}}"> Employee List</a></li>
                        <li><a href="{{route('employee.create')}}"> Create Employee</a></li>
                        <li><a href="{{url('employees/shift/')}}"> Employee Shift</a></li>
                        <li><a href="{{route('multiple_emp_shift')}}">Datewise Shift</a></li>
                        <li><a href="{{route('multiple_emp_shift_list')}}">Shift History</a></li>
                        {{-- <li><a href="{{route('emp_three_shift_add')}}">Add three shift</a></li> --}}
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-suitcase"></i><span>Recruitment</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('vacancy.index')}}"> Vacancy List</a></li>
                        <li><a href="{{route('vacancy.create')}}"> Vacancy Announcement</a></li>
                        <li><a href="{{route('application.create')}}"> New Job Application</a></li>
                        <li><a href="{{route('application.index')}}"> Job Application List</a></li>
                        <li><a href="{{route('recruitment.status')}}"> Daily Recruitment Status</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-credit-card"></i><span>Payroll</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        @if(checkPermission(['hr']))
                            <li><a href="{{url('/report/payroll/employee/salary/sheet/save')}}">Process Salary</a></li>
                        @endif
                        <li><a href="{{url('/bonus')}}">Bonus</a></li>
                        <li><a href="{{url('/payroll/grade')}}">Grade</a></li>
                        <li><a href="{{url('/salary_view')}}">Salary</a></li>
                        <li><a href="{{url('/report/payslip')}}">Payslip</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-credit-card"></i><span>Attendance</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/attendance')}}">Upload</a></li>
                        <li><a href="{{route('files.show')}}">Attendance Files</a></li>
                        <li><a href="{{route('report.daily_absent_report')}}">Daily Absent Report</a></li>
                        <li><a href="{{route('report.public.daily_attendance')}}">Daily Attendance Sheet</a></li>
                        <li><a href="{{route('attendance.manual_attendance')}}">Manual Attendance</a></li>
                        <li><a href="{{route('summary_report_date')}}">Summary Report</a></li>
                        {{--<li><a href="{{route('attendance.manual_store_date')}}">Manual Attendance Date Wise</a></li>--}}
                        {{--<li><a href="{{route('attendance.manual.edit')}}">Manual Edit Attendance</a></li>--}}
                        <li><a href="{{route('report.daily_attendance_summery')}}">Daily Attendance Summary</a></li>
{{--                        <li><a href="{{route('employee.continued_absent_list')}}"> Continued Absent List</a></li>--}}
                        <li><a href="{{route('employee.temporary_employee_proxy.list')}}"> Manage Temp. Proxy </a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="icon-note"></i><span>Leave</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                            <li><a href="{{url('/request_leave')}}">Apply Leave</a></li>
                        <li><a href="{{url('/leave')}}"> Leave Requests</a></li>
                        <li><a href="{{url('/leave_sett')}}"> Leave Settings</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="icon-note"></i><span>Training</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/add_training')}}">Add Training</a></li>
                        <li><a href="{{url('/training_history')}}">Training History</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-comments"></i><span>Meetings</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('meeting.index')}}"> Meeting List</a></li>
                        <li><a href="{{route('meeting.create')}}"> Create Meeting</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-money"></i><span>Expenses</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/expense/index')}}">Add Expenses</a></li>
                        <li><a href="{{url('/expense/categories')}}">Expense Categories</a></li>
                        <li><a href="{{url('/expense/selectDate')}}">History</a></li>
                        <li><a href="{{route('expense.report')}}">Expense Report</a></li>
                    </ul>
                </li>

                <li class="nav-parent">
                    <a href="#"><i class="fa fa-file"></i><span>Report</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/report/employee')}}">Employee</a></li>
                        <li><a href="{{url('/report/payroll')}}">Payroll</a></li>
                        <li><a href="{{url('/report/attendance')}}">Attendance</a></li>
                        <li><a href="{{url('/report/leave')}}">Leave</a></li>
                        <li><a href="{{url('/report/training')}}">Training</a></li>
                        <li><a href="{{url('/report/recruitment')}}">Recruitment</a></li>
                    </ul>
                </li>

                <li class="nav-parent">
                    <a href="#"><i class="fa fa-cog"></i><span>Settings</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
{{--                        <li><a href="{{url('/user_settings')}}">User</a></li>--}}
                        <li><a href="{{url('/attendance/settings')}}">Work Shifts</a></li>
                        <li><a href="{{url('bonus/settings')}}">Bonus Setting</a></li>
                        <li><a href="{{route('unit.index')}}">Unit List</a></li>
                        <li><a href="{{route('floor.index')}}">Floor List</a></li>
                        <li><a href="{{route('line.index')}}">Line List</a></li>
                        <li><a href="{{route('country.index')}}">Country List</a></li>
                        <li><a href="{{route('maritialstatus.index')}}">Marital Status</a></li>
                        <li><a href="{{route('department.index')}}">Departments</a></li>
                        <li><a href="{{route('designation.index')}}">Designation</a></li>
                        <li><a href="{{url('/system/style')}}">Change Theme Style</a></li>
                        <li><a href="{{route('access.index')}}">Access Logs</a></li>
                        <li><a href="{{route('activity_logs.history_view')}}">Activity Logs</a></li>
                    </ul>
                </li>
            @endif
            @if(checkPermission(['employee']))
                <li class="nav-parent">
                    <a href="#"><i class="icon-note"></i><span>Leave</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/request_leave')}}">Request Leave</a></li>
                        <li><a href="{{url('/leave_history')}}"> Leave History</a></li>
                    </ul>
                </li>
            @endif
            @if(checkPermission(['executive']))
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-users"></i><span>Employee</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/employee')}}"> Employee List</a></li>
                        <li><a href="{{route('employee.create')}}"> Create Employee</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-suitcase"></i><span>Recruitment</span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{route('vacancy.index')}}"> Vacancy List</a></li>
                        <li ><a href="{{route('vacancy.create')}}"> Vacancy Announcement</a></li>
                        <li><a href="{{route('application.create')}}"> New Job Application</a></li>
                        <li><a href="{{route('application.index')}}"> Job Application List</a></li>
                        <li><a href="{{route('recruitment.status')}}"> Daily Recruitment Status</a></li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="icon-note"></i><span>Leave</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/request_leave')}}"> Request Leave</a></li>
                        <li><a href="{{url('/leave_history')}}"> Leave History</a></li>
                    </ul>
                </li>
            @endif
            @if(checkPermission(['accountant']))
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-suitcase"></i><span>Accountant</span> <span class="fa arrow"></span></a>
                </li>
                <li class="nav-parent">
                    <a href="#"><i class="fa fa-money"></i><span>Expenses</span><span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/expense/index')}}">Add Expenses</a></li>
                        <li><a href="{{url('/expense/categories')}}">Expense Categories</a></li>
                        <li><a href="{{url('/expense/selectDate')}}">History</a></li>
                        <li><a href="{{route('expense.report')}}">Expense Report</a></li>
                    </ul>
                </li>
            @endif
        </ul>
        <!-- SIDEBAR WIDGET FOLDERS -->
        <div class="sidebar-widgets">
            <p class="menu-title widget-title"> &nbsp;<span class="pull-right"><a href="{{url('/logout')}}" class="new-folder"> </a></span></p>
        </div>
        <div class="sidebar-footer clearfix">

        </div>
    </div>
</div>