<?php
use Illuminate\Support\Facades\Auth;
?>

<link rel="shortcut icon" href="{{url('hrm_script/images/favicon.png')}}" type="image/png">
{{ Html::script('hrm_script/js/j-query.js') }}
{{ Html::style('hrm_script/css/bootstrap-select.css') }}
{{ Html::script('hrm_script/js/bootstrap-select.js') }}
{{ Html::script('hrm_script/js/chart.js') }}
<?php if(Auth::user()->theme_style==0){ ?>
	{{ Html::style('hrm_script/css/style.css') }}
	{{ Html::style('hrm_script/css/theme.css') }}
	{{ Html::style('hrm_script/css/ui.css') }}
	{{ Html::style('hrm_script/css/layout.css') }}
<?php }elseif(Auth::user()->theme_style==1) { ?>
	{{ Html::style('hrm_script/css/light_style.css') }}
	{{ Html::style('hrm_script/css/light_theme.css') }}
	{{ Html::style('hrm_script/css/light_ui.css') }}
	{{ Html::style('hrm_script/css/light_layout.css') }}
<?php }elseif(Auth::user()->theme_style==2) { ?>
	{{ Html::style('hrm_script/css/dark_style.css') }}
	{{ Html::style('hrm_script/css/dark_theme.css') }}
	{{ Html::style('hrm_script/css/dark_ui.css') }}
	{{ Html::style('hrm_script/css/dark_layout.css') }}
<?php }elseif(Auth::user()->theme_style==3) { ?>
	{{ Html::style('hrm_script/css/color_style.css') }}
	{{ Html::style('hrm_script/css/color_theme.css') }}
	{{ Html::style('hrm_script/css/color_ui.css') }}
	{{ Html::style('hrm_script/css/color_layout.css') }}
<?php } ?>
{{ Html::script('hrm_script/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js') }}


