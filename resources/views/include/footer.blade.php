
<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
{{ Html::script('hrm_script/plugins/jquery/jquery-1.11.1.min.js') }}
{{ Html::script('hrm_script/plugins/jquery/jquery-migrate-1.2.1.min.js') }}
{{ Html::script('hrm_script/plugins/jquery-ui/jquery-ui-1.11.2.min.js') }}
{{ Html::script('hrm_script/plugins/bootstrap/js/bootstrap.min.js') }}
{{ Html::script('hrm_script/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}
{{ Html::script('hrm_script/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js') }}
{{ Html::script('hrm_script/plugins/charts-sparkline/sparkline.min.js') }}
{{ Html::script('hrm_script/plugins/retina/retina.min.js') }}
{{ Html::script('hrm_script/plugins/select2/select2.min.js') }}
{{ Html::script('hrm_script/plugins/backstretch/backstretch.min.js') }}
{{ Html::script('hrm_script/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js') }}
{{ Html::script('hrm_script/plugins/bootstrap-loading/lada.min.js') }}
{{ Html::script('hrm_script/js/sidebar_hover.js') }}
{{ Html::script('hrm_script/js/jspdf.min.js') }}
{{ Html::script('hrm_script/js/canvasjs.min.js') }}
{{ Html::script('hrm_script/js/application.js') }}
{{ Html::script('hrm_script/js/widgets/notes.js') }}
{{ Html::script('hrm_script/js/quickview.js') }}
{{ Html::script('hrm_script/js/pages/search.js') }}
{{ Html::script('hrm_script/js/sidebar_hover.js') }}
{{ Html::script('hrm_script/plugins/bootstrap-editable/js/bootstrap-editable.min.js') }}
{{ Html::script('hrm_script/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js') }}
{{ Html::script('hrm_script/plugins/multidatepicker/multidatespicker.min.js') }}
{{ Html::script('hrm_script/js/widgets/todo_list.js') }}
{{ Html::script('hrm_script/plugins/metrojs/metrojs.min.js') }}
{{ Html::script('hrm_script/plugins/charts-highstock/js/highstock.min.js') }}
{{ Html::script('hrm_script/plugins/charts-highstock/js/modules/exporting.min.js') }}
{{ Html::script('hrm_script/plugins/maps-amcharts/ammap/ammap.min.js') }}
{{ Html::script('hrm_script/plugins/skycons/skycons.min.js') }}
{{ Html::script('hrm_script/js/layout.js') }}
{{ Html::script('hrm_script/js/search.js') }}
{{ Html::script('hrm_script/plugins/datatables/jquery.dataTables.min.js') }}
<!-- {{ Html::script('hrm_script/js/table_dynamic.js') }} -->
{{ Html::script('hrm_script/js/plugins.js') }}
{{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}
<!-- BEGIN PAGE SCRIPT -->
{{ Html::script('js/sweetalert.min.js') }}
<script>
  $(window).load(function(){
	 $('[data-toggle="tooltip"]').tooltip();
	var url = window.location;
      $('ul.nav li').removeClass('active'); //remove active class from all li
      $('ul.nav#leav_sett_tab li:first-child').addClass('active');
      $('ul.nav#leave-nav-tab li:first-child').addClass('active');
      $('ul.nav#employee-profile-nav-tabs li:first-child').addClass('active');
      $('ul.nav#employ-details-nav-tab-show li:first-child').addClass('active');





      $('ul.nav a[href="'+ url +'"]').parents('li').addClass('active');
      // $('ul.nav li').removeClass('active');
  });
</script>

@if(Session::has('message'))
  <script type="text/javascript">
      var message = '<?php echo Session::get("message"); ?>';
      swal("Success!",message, "success");
     
  </script>
@endif

@if(Session::has('applySuccess'))
  <script type="text/javascript">
      var applySuccess = '<?php echo Session::get("applySuccess"); ?>';
      swal("Success!",applySuccess, "success");
     
  </script>
@endif

@if(Session::has('failedMessage'))
  <script type="text/javascript">
      var message = '<?php echo Session::get("failedMessage"); ?>';
      swal("Failed!",message, "error");
     
  </script>
@endif

@if(Session::has('error'))
  <script type="text/javascript">
      var error = '<?php echo Session::get("error"); ?>';
      swal("Failed!",error, "error");
     
  </script>
@endif

@if(Session::has('error_message'))
  <script type="text/javascript">
      var error_message = '<?php echo Session::get("error_message"); ?>';
      swal("Failed!",error_message, "error");
     
  </script>
@endif

@if(Session::has('success'))
  <script type="text/javascript">
      var success = '<?php echo Session::get("success"); ?>';
      swal("Success!",success, "success");
     
  </script>
@endif



