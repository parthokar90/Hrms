<div class="footer">
    <div class="copyright">
        <p class="pull-left sm-pull-reset">
            
			
            <span>
			@foreach($copyright as $data)
			<!-- {{$data->copyright}} -->

			@endforeach

            Copyright &copy; <a href="http://feits.co" target="_BLANK"><b>FEITS</b></a>. All right reserved.
			
			</span>.
            
        </p>
        <p class="pull-right sm-pull-reset">
            Developed By <a href="http://feits.co" target="_BLANK"><b>FEITS</b></a>
            <!-- <span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span> -->
        </p>
    </div>
</div>