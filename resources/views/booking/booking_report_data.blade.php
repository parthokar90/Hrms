@extends('layouts.master')
@section('title', 'Booking List')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-12">
                                 <h3><i class="fa fa-lock"></i> <strong>Booked </strong> List from <b>{{\Carbon\Carbon::parse($request->start_date)->format('d-M-Y')}}</b> to <b>{{\Carbon\Carbon::parse($request->end_date)->format('d-M-Y')}}</b></h3>
                            </div>
                           
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Booked By</th>
                                <th width="20%">Purpose</th>
                                <th>Date</th>
                                <th>Starting Time</th>
                                <th>Ending Time</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($booking_history as $bh)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$bh->biName}}</td>
                                    <td>{{$bh->bookedBy}}</td>
                                    <td>{{$bh->purpose}}</td>
                                    <td>{{\Carbon\Carbon::parse($bh->biDate)->format('d-M-Y')}}</td>
                                    <td>{{date('h:i:s a',strtotime($bh->biStartTime))}}</td>
                                    <td>{{date('h:i:s a',strtotime($bh->biEndTime))}}</td>
                                    <td><?php if($bh->status==0){ echo "<span style='color:green;'>Active</span>"; }
                                    if($bh->status==1){ echo "<span style='color:red;'>Cancelled</span>"; } ?></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection