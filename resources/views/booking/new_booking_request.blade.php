@extends('layouts.master')
@section('title', 'New Booking Request')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>
        @endif
        @if(Session::has('edit'))
            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-lock"></i> New <strong>Booking Request</strong></h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        {!! Form::open(['method'=>'POST', 'route'=>'booking.save_new_request']) !!}
                        <div class="form-group">
                            <label >Booking Item</label>
                            <select required="" name="biId" class="form-control"  data-search="true">
                                <option selected="" value="">Select</option>
                                @foreach($booking_items as $bi)
                                    <option value="{{$bi->id}}">{{$bi->biName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="bookedBy"> Booked By</label>
                          <input type="text" name="bookedBy" class="form-control" required="" placeholder="Booked By" >
                        </div>
                        <div class="form-group">
                          <label for="purpose"> Purpose</label>
                          <textarea name="purpose" class="form-control" placeholder="Purpose" ></textarea>
                        </div>
                        <div class="form-group">
                          <label for="biDate"> Booking Date</label>
                          <input type="text" name="biDate" class="date-picker form-control" required="" placeholder="Date" >
                        </div>
                        <div class="form-group">
                          <label for="vbStartTime"> Booking Starting Time</label>
                          <input type="time" name="biStartTime" class="form-control" required="" placeholder="Booking Starting Time" >
                        </div>
                        <div class="form-group">
                          <label for="vbEndTime"> Booking Ending Time</label>
                          <input type="time" name="biEndTime" class="form-control" required="" placeholder="Booking Ending Time" >
                        </div>
                        <div class="form-group">
                            <hr>
                            <button type="Submit" class="btn btn-primary">Submit Booking Request</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection