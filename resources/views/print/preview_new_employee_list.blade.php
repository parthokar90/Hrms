<style>

    .reportHeaderArea{
        text-align: center;
        margin-bottom: 25px;
        margin-top: -5px;
    }

    .reportHeader{
        line-height: 4px;
    }

    .reportHeader{
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        font-size: 10px;
    }

    .reportHeaderCompany{
        font-size: 18px !important;

    }

</style>
<title>New Employee List</title>
<div class="reportHeaderArea">
    <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
    <p class="reportHeader">{{$companyInformation->company_address1}}</p>
    <p class="reportHeader">{{$companyInformation->company_email}}</p>
    <p class="reportHeader">{{$companyInformation->company_phone}}</p>
</div>
<table border="1" class="table table-hover table-striped table-bordered" style="background:#fff;border-collapse: collapse;" width="100%">
    <thead style="font-size:13px;font-weight:bold;">
    <tr>
        <th>Serial No.</th>
        <th>ID</th>
        <th>Name</th>
        @if(!empty($request->coldesignation))
            <th>Designation</th>
        @endif
        @if(!empty($request->coldepartment))
            <th>Department</th>
        @endif
        <th>Joining Date</th>
        @if(!empty($request->colgender))
            <th>Gender</th>
        @endif
        @if(!empty($request->colunit))
            <th>Branch</th>
        @endif
        @if(!empty($request->colline))
            <th>Line</th>
        @endif
        @if(!empty($request->colsection))
            <th>Section</th>
        @endif
        @if(!empty($request->colstatus))
            <th>Status</th>
        @endif
        @if(!empty($request->coldob))
            <th>DOB</th>
        @endif
        <th>Salary</th>
    </tr>
    </thead>
    <tbody style="font-size:11px;">
    @foreach($employees as $key=>$employee)
        <tr>
            <td>{{++$key}}</td>
            <td>{{$employee->employeeId}}</td>
            <td>{{$employee->empFirstName}}</td>
            @if(!empty($request->coldesignation))
                <td>{{$employee->designation}}</td>
            @endif
            @if(!empty($request->coldepartment))
                <td>{{$employee->departmentName}}</td>
            @endif
            <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('d-M-Y')}}</td>
            @if(!empty($request->colgender))
                <td>
                    @if($employee->empGenderId==1)
                        Male
                    @endif
                    @if($employee->empGenderId==2)
                        Female
                    @endif
                    @if($employee->empGenderId==3)
                        Other
                    @endif
                </td>
            @endif
            @if(!empty($request->colunit))
                <td>{{$employee->unitName}}</td>
            @endif
            @if(!empty($request->colline))
                <td>{{$employee->LineName}}</td>
            @endif
            @if(!empty($request->colsection))
                <td>{{$employee->empSection}}</td>
            @endif

            @if(!empty($request->colstatus))
                <td>
                    @if($employee->empAccStatus==1)
                        Active
                    @elseif($employee->empAccStatus==0)
                        Inactive
                    @endif
                </td>
            @endif

            @if(!empty($request->coldob))
                <td>{{\Carbon\Carbon::parse($employee->empDOB)->format('d-M-Y')}}</td>
            @endif
            <td>{{$employee->total_employee_salary}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

{{--<script type="text/javascript">--}}
{{--     window.print();--}}
{{--     window.close();--}}
{{-- </script>--}}