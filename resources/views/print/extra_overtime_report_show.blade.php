      <style>
        .table{
          font-size:12px;
        }
        .reportHeaderArea{
            text-align: center;
            margin-bottom: 25px;
            margin-top: -5px;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
          font-size: 18px !important;
          
        }
        table tr>td{
          padding:4px;
        }
    </style>
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader"><b>Extra O.T sheet of ({{$request->start_date}} to {{$request->end_date}})</b></p>
    </div>
  <table border="1" class="table table-hover table-striped table-bordered" style="background:#fff;border-collapse: collapse;" width="100%">
      <thead>
      <tr>
          <th scope="col">Sl No</th>
          <th scope="col">Emp ID</th>
          <th scope="col">Name</th>
          <th scope="col">Designation</th>
          <th scope="col">Line No</th>
          <th scope="col">Date Of Join</th>
          <th scope="col">Salary</th>
          <th scope="col">O.T Hour</th>
          <th scope="col">O.T Rate</th>
          <th scope="col">Amount</th>
          <th scope="col">Signature</th>
      </tr>
      </thead>
      <tbody>
      @php $order=0; $ot_hour=0; @endphp
      @foreach($extraot as $extra)
          @php
              $order++; $ot_rate=round($extra->basic_salary/104,2);
              $total_hour=$extra->total_hour-$extra->total_date_count+$extra->total_minute;
          @endphp

          @if($friday)
              @foreach($friday as $weekend)
                  @if($weekend->friday_id==$extra->emp_id)
                      @php
                          $ot_hour=$total_hour+$weekend->total_hour_weekend;
                      @endphp
                  @else
                      @php $ot_hour=$total_hour; @endphp
                  @endif
              @endforeach
          @endif
          <tr>
              <td>{{$order}}</td>
              <td>{{$extra->employeeId}}</td>
              <td>{{$extra->empFirstName}}</td>
              <td>{{$extra->designation}}</td>
              <td>{{$extra->line_no}}</td>
              <td>{{$extra->empJoiningDate}}</td>
              <td>{{$extra->total_employee_salary}}</td>
              <td>{{round($ot_hour,0)}}</td>
              <td>{{$ot_rate}}</td>
              <td>
                  @php
                      $ot_amount=$ot_rate*$ot_hour;
                  @endphp
                  {{round($ot_amount,0)}}
              </td>
              <td></td>
          </tr>
      @endforeach
      </tbody>
  </table>

<script type="text/javascript">
     window.print();
 </script>