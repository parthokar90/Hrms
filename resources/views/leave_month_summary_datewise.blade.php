@extends('layouts.master')
@section('title', 'Date Wise Employee Leave Summary')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Date Wise Employee Leave Summary
                    </div>
                    <div class="panel-body">
                        {{Form::open(array('url' => 'employee/leave/summary/date/wise/show','method' => 'post','target'=>'_blank'))}}
                        <div class="col-md-12">
                           <div class="col-md-6" style="padding:0px">
                              <div class="form-group">
                                  <label class="form-label">Start Date<span style="color:red;">*</span></label>
                                  <div class="prepend-icon">
                                      <input type="text" autocomplete="off" name="start"  class="form-control date-picker" required>
                                      <i class="icon-calendar"></i>
                                  </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">End Date<span style="color:red;">*</span></label>
                                <div class="prepend-icon">
                                    <input type="text" autocomplete="off" name="end"  class="form-control date-picker" required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                           </div> 
                           <button type="submit" class="btn btn-success btn-square">Generate</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection