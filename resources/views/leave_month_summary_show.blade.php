@extends('layouts.master')
@section('title', 'Month Wise Employee Leave Summary')
@section('content')
<style>
        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 8px;
        }
        
        tr:nth-child(even) {
          background-color: #dddddd;
        }
        </style>
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <button style="margin-top: 17px;" class="btn btn-info" id="pdf_btn" type="button" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button>
                            <div id="print_area">
                            <table class="table table-bordered">
                                <h3 class="text-center">{{$companyInformation->company_name}}</h3>
                                <p class="text-center">{{$companyInformation->company_address1}}</p>
                                <p class="text-center">Month Wise Leave Summary</p>
                                <p class="text-center">  {{date('F-Y',strtotime($month))}}</p>
                                   <tr>
                                     <th>Sl</th>
                                     <th>Employee Id</th>
                                     <th>Employee</th>
                                     <th>Department</th>
                                     <th>Designation</th>
                                     <th>Leave Type</th>
                                     <th>Date</th>
                                     <th>Total</th>
                                   </tr>
                                   @php $order=0; @endphp
                                   @foreach($data as $leave)
                                   <tr>
                                     <td>{{++$order}}</td>
                                     <td>{{$leave->employeeId}}</td>
                                     <td>{{$leave->empFirstName}}{{$leave->empLastName}}</td>
                                     <td>{{$leave->departmentName}}</td>
                                     <td>{{$leave->designation}}</td>
                                     <td>{{$leave->leave_type}}</td>
                                     <td>{{$leave->leave_date}}</td>
                                     <td>{{$leave->total_leave}}</td>
                                   </tr>
                                   @endforeach
                               </table>
                              </div>
                               </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
@endsection