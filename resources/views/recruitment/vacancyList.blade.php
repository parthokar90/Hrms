@extends('layouts.master')
@section('title', 'Vacancy List')
@section('content')
    <div class="page-content">
        @if(Session::has('store'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('store') }}</p>
        @elseif(Session::has('msg'))
            <p id="alert_message" class="alert alert-danger">{{ Session::get('msg') }}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Vacancy </strong> List</h3>
                    </div>
                    <a href="{{route('vacancy.create')}}" class="btn custom-btn btn-xs ">Add New</a>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Vacancy No.</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Joining Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vacancies as $v)
                                <tr>
                                    <td>{{$v->id}}</td>
                                    <td>{{$v->vacTitle}}</td>
                                    <td>{{$v->vacNumber}}</td>
                                    <td>{{\Carbon\Carbon::parse($v->VacAnnounceStartingDate)->format('Y-m-d')}}</td>
                                    <td>{{\Carbon\Carbon::parse($v->vacAnnounceEndingDate)->format('Y-m-d')}}</td>
                                    <td>{{\Carbon\Carbon::parse($v->vacJoiningDate)->format('Y-m-d')}}</td>
                                    <td>
                                        @if($v->vacStatus==1)
                                            Active
                                        @endif
                                        @if($v->vacStatus==0)
                                            <span class="text-red">Inactive</span>
                                        @endif
                                    </td>

                                    <td>
                                        <a href="{{route('vacancy.show',$v->id)}}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection