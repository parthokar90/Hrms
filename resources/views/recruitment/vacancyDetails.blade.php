@extends('layouts.master')
@section('title', 'Vacancy Details')
@section('content')
    <!--Employee Education Index-->
    <div class="page-content">

        @if(Session::has('up'))
            <p id="alert_message" class="alert alert-info">{{ Session::get('up') }}</p>
        @endif

        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
        @endif

        @if(Session::has('store'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('store') }}</p>
        @endif
        {{--@if(Session::has('delete'))--}}
            {{--<p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>--}}
        {{--@endif--}}
        {{--@if(Session::has('edit'))--}}
            {{--<p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>--}}
        {{--@endif--}}
        {{--@if(Session::has('createEmployee'))--}}
            {{--<p id="alert_message" class="alert alert-success">{{ Session::get('createEmployee') }}</p>--}}
        {{--@endif--}}
        {{--@if(Session::has('fileSize'))--}}
            {{--<p id="alert_message" class="alert alert-danger">{{Session::get('fileSize')}}</p>--}}
        {{--@endif--}}



            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3>  <strong>Vacancy</strong> Announcement</h3>
                    </div>
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#tab2_1" data-toggle="tab"><i class="icon-home"></i> Details</a></li>
                            <li><a href="#tab2_2" data-toggle="tab"><i class="icon-user"></i> Application List</a></li>
                            <li><a href="#tab2_3" data-toggle="tab"><i class="icon-cloud-download"></i> Other Tab</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab2_1">
                                <div class="row">
                                    <div  class="col-sm-4 text-end">
                                       <h4><strong>Job Title: </strong></h4>
                                    </div>
                                    <div class="col-sm-4">
                                        <h4><strong>{{$v->vacTitle}}</strong></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 text-end">
                                        <h5><strong>Number of vacancies:</strong></h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <h5><strong>{{$v->vacNumber}}</strong></h5>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-4 text-end">
                                        <h5><strong>Announcement Date:</strong></h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <h5><strong>{{\Carbon\Carbon::parse($v->VacAnnounceStartingDate)->format('j F Y')}}</strong></h5>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-4 text-end">
                                        <h5><strong>End Date:</strong></h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <h5><strong>{{\Carbon\Carbon::parse($v->vacAnnounceEndingDate)->format('j F Y')}}</strong></h5>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-4 text-end">
                                        <h5><strong>Joining Date:</strong></h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <h5><strong>{{\Carbon\Carbon::parse($v->vacJoiningDate)->format('j F Y')}}</strong></h5>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-4 text-end">
                                        <h5><strong>Status:</strong></h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <h5><strong>
                                                @if($v->vacStatus==1)
                                                    Active
                                                @endif
                                                @if($v->vacStatus==0)
                                                        <span class="text-red">Inactive</span>
                                                @endif
                                            </strong></h5>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-4 text-end">
                                        <h5><strong>Announcement Details</strong></h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <h5><strong>{{$v->vacDescription}}</strong></h5>
                                    </div>

                                </div>
                                <div class="row padTop30">
                                    <div class="col-sm-5">
                                    <a href="{{url('recruitment/vacancy')}}"> <button class="btn btn-default">Back To List</button></a>
                                    </div>
                                    <div class="col-sm-5">
                                        <button class="btn btn-success" data-toggle="modal" data-target="#editVac">Edit Info</button>
                                        <button type="button" data-toggle="modal" data-target="#deleteVac" class="btn btn-danger padLeft20">Delete Vacancy</button>
                                    </div>
                                </div>


                            </div>



                            <div class="tab-pane fade" id="tab2_2">

                                {{--<div class="page-content">--}}
                                    @if(Session::has('store'))
                                        <p id="alert_message" class="alert alert-success">{{ Session::get('store') }}</p>
                                    @endif
                                    {{--@elseif(Session::has('file_size'))--}}
                                    {{--<p id="alert_message" class="alert alert-danger">{{ Session::get('file_size') }}</p>--}}
                                    {{--@endif--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-lg-12 portlets">--}}
                                            <div class="panel">
                                                <div class="panel-header panel-controls">
                                                    <h3><i class="fa fa-table"></i> <strong>Application </strong> List</h3>
                                                </div>
                                                <div class="panel-content pagination2 table-responsive">
                                                    <table class="table table-hover table-dynamic">
                                                        <thead>
                                                        <tr>
                                                            <th>id</th>
                                                            <th>Name</th>
                                                            <th>Phone</th>
                                                            <th>Submitted Date</th>
                                                            <th>Interview Status</th>
                                                            <th>Interview Date</th>
                                                            <th>Priority</th>
                                                            {{--<th>Joining Date</th>--}}
                                                            {{--<th>Status</th>--}}
                                                            <th>Action</th>
                                                            <th>Attachment</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($applications as $a)
                                                            <tr>
                                                                <td>{{$a->id}}</td>
                                                                <td>{{$a->name}}</td>
                                                                <td>{{$a->phone}}</td>
                                                                <td>{{\Carbon\Carbon::parse($a->submittedDate)->format('Y-m-d')}}</td>
                                                                <td>{{$a->interviewStatus}}</td>
                                                                <td>{{\Carbon\Carbon::parse($a->interviewDate)->format('Y-m-d')}}</td>
                                                                <td>{{$a->priorityStatus}}</td>
                                                                <td>
                                                                    <a href="{{route('application.details',$a->id)}}"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i>View</button></a>
                                                                </td>
                                                                {{--<a target="_blank" href="{{asset('Educational_Certificates/'.$ec->empCertificate)}}"><p><button type="button" class="btn btn-default">Download Attachment</button></p></a>--}}
                                                                <td> <a target="_blank" href="{{asset('Applications/'.$a->attachment)}}"> <button type="button" class="btn btn-default btn-xs"><i class="fa fa-download"></i></button></a></td>

                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            </div>
                            <div class="tab-pane fade padTop10" id="tab2_3">
                                <p class="download-btn-text">Download Job Application :</p>
                                <p class="padLeft20 padTop10"><a href="{{route('download_job_application',$v->id)}}"  class="btn btn-blue btn-md"><i class="fa fa-download"></i>Download Job Application</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    </div>




    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>




    <div class="modal fade" id="deleteVac" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong>Confirm Delete</strong></h4>
                </div>
                <div class="modal-body">
                    <p>You sure you want to delete <strong>{{$v->vacTitle}}</strong> vacancy. </p>
                </div>
                {!! Form::open(['method'=>'DELETE','action'=>['RecruitmentController@destroy',$v->id]]) !!}
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Confirm</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>



    <div class="modal fade" id="editVac" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                    <h4 class="modal-title"><strong>Edit</strong> Vacancy</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            {!! Form::open(['method'=>'PATCH','action'=>['RecruitmentController@update',$v->id]]) !!}

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="required form-group">
                                        {!! Form::label('vacTitle','Job Position',['class'=>'control-label']) !!}
                                        <div class="append-icon">
                                            <input type="text" name="vacTitle" value="{{$v->vacTitle}}" class="form-control" placeholder="Enter Title..." required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="required form-group">
                                        {!! Form::label('vacNumber','Number of posts',['class'=>'control-label']) !!}
                                        <div class="append-icon">
                                            <input type="number" name="vacNumber" value="{{$v->vacNumber}}" class="form-control" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="required form-label">Announced Date</label>
                                        <div class="prepend-icon">
                                            <input type="text" name="VacAnnounceStartingDate" class="date-picker form-control" value="{{\Carbon\Carbon::parse($v->VacAnnounceStartingDate)->format('m/d/Y')}}" required>
                                            <i class="icon-calendar"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="required form-label">Ending Date</label>
                                        <div class="prepend-icon">
                                            <input type="text" name="vacAnnounceEndingDate" class="date-picker form-control" value="{{ \Carbon\Carbon::parse($v->vacAnnounceEndingDate)->format('m/d/Y') }}" placeholder="Select a date..." required>
                                            <i class="icon-calendar"></i>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="required form-label">Joining Date</label>
                                        <div class="prepend-icon">
                                            <input type="text" name="vacJoiningDate" class="date-picker form-control" value="{{\Carbon\Carbon::parse($v->vacJoiningDate)->format('m/d/Y')}}" placeholder="Select a date...">
                                            <i class="icon-calendar"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="required form-group">
                                        <label class="control-label">Status</label>
                                        <div class="option-group">
                                            <select id="language" name="vacStatus" class="language" required>
                                                <option {{ $v->vacStatus ? "selected" : "" }} value="1">Active</option>
                                                <option {{ ! $v->vacStatus ? "selected" : "" }} value="0">Inactive</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Details</label>
                                        <div class="append-icon">
                                            <textarea name="vacDescription" class="form-control" placeholder="Enter the details..." rows="4"> {{$v->vacDescription}} </textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>



                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary btn-embossed">Save changes</button>
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>



@endsection




