@extends('layouts.master')
@section('title', 'Create Vacancy ')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('fileSize'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('fileSize')}}</p>
            @elseif(Session::has('message'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
            @endif
            <div class="col-md-12">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>New Vacancy</strong> Announcement</h2>
                    </div>
                    <div class="panel-body bg-white">

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                {!! Form::open(['method'=>'POST','action'=>'RecruitmentController@store']) !!}

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="required form-group">
                                            {!! Form::label('vacTitle','Job Position',['class'=>'control-label']) !!}
                                            <div class="append-icon">
                                                <input type="text" name="vacTitle" class="form-control" placeholder="Enter Title..." required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="required form-group">
                                            {!! Form::label('vacNumber','Number of posts',['class'=>'control-label']) !!}
                                            <div class="append-icon">
                                                <input type="number" name="vacNumber" class="form-control" placeholder="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="required form-label">Announced Date</label>
                                            <div class="prepend-icon">
                                                <input type="text" name="VacAnnounceStartingDate" autocomplete="off" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="required form-label">Ending Date</label>
                                            <div class="prepend-icon">
                                                <input type="text" name="vacAnnounceEndingDate" autocomplete="off" class="date-picker form-control" placeholder="Select a date..." required>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="required form-label">Joining Date</label>
                                            <div class="prepend-icon">
                                                <input type="text" name="vacJoiningDate" autocomplete="off" class="date-picker form-control" placeholder="Select a date...">
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="required form-group">
                                            <label class="control-label">Status</label>
                                            <div class="option-group">
                                                <select id="language" name="vacStatus" class="language" required>
                                                    <option value="">Select Status</option>
                                                    <option selected value="1">Active</option>
                                                    <option value="0">Not Active</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Details</label>
                                            <div class="append-icon">
                                                <textarea name="vacDescription" class="form-control" placeholder="Enter the details..." rows="4"></textarea>
                                                <i class="icon-lock"></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>



                                <div class="text-center  m-t-20">
                                    <button type="submit" class="btn btn-embossed btn-primary">Create</button>
                                    <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                                </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

    </script>
    @include('include.copyright')
@endsection