@extends('layouts.master')
@section('title', 'Employee Salary')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3>Pay<strong>roll/Salary</strong></h3>
                    </div>
                    <div class="panel-content">
                        {{Form::open(array('url' => 'salary_store','method' => 'post'))}}
                        <div class="form-group">
                        <input type="hidden" name="emp_select" value="{{session('emp_id')}}">
                        <input type="hidden" name="grade_select" value="{{session('gradeId')}}">
                        <input type="hidden" name="current_month" value="{{date('m-Y')}}">
                        <div class="form-group">
                            <label for="">Grade Name</label>
                            <input class="form-control" type="text" value="{{$data1->grade_name}}" disabled >
                        </div>
                        <div class="form-group">
                            <label for="gross"><b>Gross Salary</b></label>
                            <input type="number" autofocus="" onkeyup="recalculatesalary()" step="any" name="gross" class="form-control form-white" id="gross" placeholder="Amount" required >
                        </div>
                        <div class="form-group">
                            <label for="basic">Basic</label>
                            <input type="number" readonly value="{{$data1->basic_salary}}" step="any" name="basic" class="form-control form-white" id="basic" placeholder="Amount" required >
                        </div>
                        <div class="form-group">
                            <label for="house">House Rent</label>
                            <input type="number" readonly value="{{$data1->house_rant}}" step="any" name="houserant" class="form-control form-white" id="house" placeholder="Amount" required >
                        </div>
                        <div class="form-group">
                            <label for="medical">Medical</label>
                            <input type="number" readonly onkeyup="recalculatesalary1()"  value="{{$data1->medical}}" step="any" name="medical" class="form-control form-white" id="medical" placeholder="Amount" required >
                        </div>
                        <div class="form-group">
                            <label for="transport">Transportation</label>
                            <input type="number" readonly onkeyup="recalculatesalary1()"  value="{{$data1->transport}}" step="any" name="transport" class="form-control form-white" id="transport" placeholder="Amount" required >
                        </div>
                        <div class="form-group">
                            <label for="food">Food</label>
                            <input type="number" readonly onkeyup="recalculatesalary1()"  value="{{$data1->food}}" step="any" name="foods" class="form-control form-white" id="food" placeholder="Amount" required >
                        </div>
                        <div class="form-group">
                            <label for="other">Others</label>
                            <input type="number" readonly onkeyup="recalculatesalary1()"  value="{{$data1->others}}" step="any" name="others" class="form-control form-white" id="other" placeholder="Amount" required >
                        </div>
                        <button id="salary_check" type="submit" class="btn btn-primary">Save Salary</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
            function recalculategross(){

                var basic=parseFloat($("#basic").val());
                var house=parseFloat($("#house").val());
                var medical=parseFloat($("#medical").val());
                var transport=parseFloat($("#transport").val());
                var food=parseFloat($("#food").val());
                var other=parseFloat($("#other").val());

                var gross = (basic+house+medical+transport+food+other);
                
                $("#gross").val(gross.toFixed(2));

            }
            recalculategross();

            function recalculatesalary(){
                var gross=parseFloat($("#gross").val());
                $("#medical").val('600.00');
                $("#transport").val('350.00');
                $("#food").val('900.00');
                $("#other").val('0.00');


                var medical=parseFloat($("#medical").val());
                var transport=parseFloat($("#transport").val());
                var food=parseFloat($("#food").val());
                var other=parseFloat($("#other").val());
                var deduction = (medical+transport+food+other);
                var basic = ((gross-deduction)/1.5);
                $("#basic").val(basic.toFixed());
                var house = ((basic/100)*50);
                $("#house").val(house.toFixed());


            }

            function recalculatesalary1(){
                var gross=parseFloat($("#gross").val());
                var medical=parseFloat($("#medical").val());
                var transport=parseFloat($("#transport").val());
                var food=parseFloat($("#food").val());
                var other=parseFloat($("#other").val());
                var deduction = (medical+transport+food+other);
                var basic = ((gross-deduction)/1.5);
                $("#basic").val(basic.toFixed());
                var house = ((basic/100)*50);
                $("#house").val(house.toFixed());


            }

    </script>
@endsection