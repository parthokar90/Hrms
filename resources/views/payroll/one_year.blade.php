@extends('layouts.master')
@section('title', 'Increment Employee Salary')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5>Incrementable Employees List</h5>
            </div>
            <div class="panel-body">
                <p style="font-size: 14px;" class="text-center"> <b>Search result for:{{date('F-Y',strtotime($requestmonth))}}</b> </p>
               <div class="col-md-12" style="padding: 0">
                   {{Form::open(array('url' => 'employee/year/multiple/work/group/store','method' => 'post'))}}
                   <div class="col-md-6" >
                       <div class="form-group">
                           <label>Enter Percent</label>
                           <input type="number" class="form-control amount_value" id="amount_value" required>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                            <button style="margin-top: 21px;" id="increment_bonus_button" type="submit" class="btn btn-info">Save</button>
                       </div>
                   </div>
               </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>EmpId</th>
                        <th>Name</th>
                        <th>Join</th>
                        <th>Group</th>
                        <th>Year</th>
                        <th>Salary</th>
                        <th>Deduction(-1850)</th>
                        <th>Percent</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($increment as $increments)
                        @if($increments->status==1)
                            @else
                            <tr>
                                <td>{{$increments->employeeId}}</td>
                                <td>{{$increments->empFirstName}}</td>
                                <td>{{date('F-Y',strtotime($increments->empJoiningDate))}}</td>
                                <td>{{$increments->work_group}}</td>
                                <td>{{$increments->Year_difference}}</td>
                                <td><input type="text" name="gross_salary[]" class="form-control" value="{{round($increments->cur_salary,0)}}" readonly></td>
                                <td><input type="text" name="after_deduction[]" class="form-control" value="{{round($increments->after_deduction,0)}}" readonly></td>
                                <td>
                                    @if($increments->status==1)
                                        Paid
                                    @else
                                        <input type="text" class="form-control amount"  name="emp_percent[]" placeholder="Percent">
                                    @endif
                                    <input type="hidden" name="emp_id[]" value="{{$increments->em_id}}">
                                </td>
                                <td>@if($increments->status==1) Paid @else <span style="color: green;">Unpaid</span> @endif</td>
                            </tr>
                        @endif
                     @endforeach
                    </tbody>
                </table>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#amount_value').keyup(function () {
                $("#production_button").show();
                $("#title_id").show();
                var input = document.getElementsByClassName("amount");
                for (var i = 0; i < input.length; i++){
                    input[i].value = document.getElementById('amount_value').value;
                }
            });
        });
    </script>
    @include('include.copyright')
@endsection