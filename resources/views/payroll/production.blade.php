@extends('layouts.master')
@section('title', 'Production Bonus')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
            @if(Session::has('production_bonus_work_group'))
                <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('production_bonus_work_group') }}</p>
            @endif
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 style="font-weight: bold">Production Bonus</h5>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <label>Select Work Group</label>
                    {{Form::open(array('url' => '/production/bonus/work/group','method' => 'post'))}}
                    <div class="form-group">
                        <select id="production_submission" name="work_group_employee" onchange="this.form.submit()" class="form-control">
                            <option>Select Work Group</option>
                            <option value="Staff">Staff</option>
                            <option value="Worker">Worker</option>
                        </select>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#production_submission").change(function(){
                $(this).closest('form').attr('target', '_blank').submit();
            });
        });
    </script>
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    @include('include.copyright')
@endsection