<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
        'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Increment Letter</title>
    <style>
        @page { sheet-size: A4; }
        body{
            font-family: 'bangla', sans-serif;
            font-size: 12px;
        }
        h1{
            text-align: center;
            font-size: 30px!important;
            font-weight: bold!important;;
        }
        p{
            line-height: 1px;
            text-align: center;
        }

        .col1{
            width: 50%;
            float: left;
        }
        li{
            list-style: none;
        }
        li span{
            font-size: 18px;
        }
        ul {
            padding: 0;
            list-style-type: none;
        }
        .subject{
            float: left;
            font-weight: bold;
            width: 20%;
            font-size: 18px;
        }
        .subject_heading{
            float: right;
            font-weight: bold;
            width: 80%;
            font-size: 18px;
        }
        .description{
            font-size: 16px;
        }
        .incr_div{
            width: 100%;
            border: 1px solid #000000;
        }
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }
        .foter1{
            float: left;
            width: 50%;
        }
        .foter2{
            float: left;
            width: 50%;
        }
    </style>
</head>
<body>
    <div class="reportHeaderArea">
            @if($companyInformation->bComName=='')
            <h1>{{$companyInformation->company_name}}</h1>
            <p>{{$companyInformation->company_address1}}</p>
                @else
            <h1> {{$companyInformation->bComName}}</h1>
            <p>{{$companyInformation->bComAddress}}</p>
            @endif
    </div>
   <div class="col1 description">
    সুত্র নংঃএফ বি এ এল/বেঃবঃ/
   </div>
    <br><br>
    @if($employeeData)
            <ul>
                <li><span>তারিখঃ </span>      {{date('d-m-Y')}}</li>
                <li><span>নামঃ</span>         {{$employeeData->empFirstName}}{{$employeeData->empLastName}}</li>
                <li><span>পদবীঃ</span>        @if($employeeData->designationBangla=='') {{$employeeData->designation}} @else {{$employeeData->designationBangla}} @endif</li>
                <li><span>কার্ড নংঃ</span>     {{$employeeData->empCardNumber}}</li>
                <li><span>সেকশনঃ</span>       {{$employeeData->empSection}}</li>
                <li><span>যোগদান তারিখঃ</span> {{date('d-m-Y',strtotime($employeeData->empJoiningDate))}}</li>
            </ul>
            <br><br>
<div class="subject">বিষয়ঃ</div>  <div class="subject_heading">বেতন বৃদ্ধি/পদোন্নতি/বেতন সমন্বয় প্রসঙ্গে ।</div>
            <br><br><br>
      <div class="description">
          জনাব/জনাবা,<br>
          এই মর্মে আপনাকে জানানো যাচ্ছে যে,বাংলাদেশ সরকার ঘোষিত সর্বশেষ মজুরী বৃদ্ধি-২০১৮ ইং মোতাবেক
          আপনার বর্তমান বেতন এর সাথে নিম্নে বনিত ছক মোতাবেক আপনার েতন বৃদ্ধি/পদোন্নতি/বেতন সমন্বয় করা হইল যাহা<br>
          যাহা ঃ-
      </div>
          <br><br>
    <div class="incr_div">
        <div style="float: left;width: 10%">
            ক
        </div>
        <div style="float: left;width: 40%">
            <table>
                <tr>
                    <th>পূর্বের বেতন কাঠামোর বিবরণ</th>
                </tr>
                <tr>
                    <th>মজুরীর বিবরণ</th>
                </tr>
                <tr>
                    <td>মোট বেতন=@if($employeeData->employees_previous_basic=='' && $employeeData->previous_house_rent=='' && $employeeData->previous_medical=='' && $employeeData->previous_transport=='' && $employeeData->previous_food=='')
                            {{en2bnNumber(round($employeeData->current_basic+$employeeData->current_houserent+$employeeData->current_medical+$employeeData->current_transport+$employeeData->current_food,0))}}
                        @else
                            {{en2bnNumber(round($employeeData->employees_previous_basic+$employeeData->previous_house_rent+$employeeData->previous_medical+$employeeData->previous_transport+$employeeData->previous_food,0))}}
                        @endif গ্রেড {{$employeeData->grade_name}}</td>
                </tr>
                <tr>
                    <td>পদবীঃ  @if($employeeData->designationBangla=='') {{$employeeData->designation}} @else {{$employeeData->designationBangla}} @endif</td>
                </tr>
                <tr>
                    <td>মূল বেতন   @if($employeeData->employees_previous_basic=='')
                            {{en2bnNumber(round($employeeData->current_basic,0))}}
                        @else
                            {{en2bnNumber(round($employeeData->employees_previous_basic,0))}}
                        @endif</td>
                </tr>
                <tr>
                    <td>বাড়ী ভাড়া      @if($employeeData->previous_house_rent=='')
                            {{en2bnNumber(round($employeeData->current_houserent,0))}}
                        @else
                            {{en2bnNumber(round($employeeData->previous_house_rent,0))}}
                        @endif</td>
                </tr>
                <tr>
                    <td>চিকিৎসা ভাতা   @if($employeeData->previous_medical=='')
                            {{en2bnNumber(round($employeeData->current_medical,0))}}
                        @else
                            {{en2bnNumber(round($employeeData->previous_medical,0))}}
                        @endif</td>
                </tr>
                <tr>
                    <td>যাতায়াত  @if($employeeData->previous_transport=='')
                            {{en2bnNumber(round($employeeData->current_transport,0))}}
                        @else
                            {{en2bnNumber(round($employeeData->previous_transport,0))}}
                        @endif</td>
                </tr>
                <tr>
                    <td>খাদ্য ভাতা   @if($employeeData->previous_food=='')
                            {{en2bnNumber(round($employeeData->current_food,0))}}
                        @else
                            {{en2bnNumber(round($employeeData->previous_food,0))}}
                        @endif</td>
                </tr>
                <tr>
                    <td>মোটঃ    @if($employeeData->employees_previous_basic=='' && $employeeData->previous_house_rent=='' && $employeeData->previous_medical=='' && $employeeData->previous_transport=='' && $employeeData->previous_food=='')
                            {{en2bnNumber(round($employeeData->current_basic+$employeeData->current_houserent+$employeeData->current_medical+$employeeData->current_transport+$employeeData->current_food,0))}}
                        @else
                            {{en2bnNumber(round($employeeData->employees_previous_basic+$employeeData->previous_house_rent+$employeeData->previous_medical+$employeeData->previous_transport+$employeeData->previous_food,0))}}
                        @endif</td>
                </tr>
            </table>
        </div>
        <div style="float: left;width: 10%;">
            <span style="margin-top: 50px">খ</span>
        </div>
        <div style="float: left;width: 40%">
            <table>
                <tr>
                    <th>১ লা ডিসেম্বর-১৮ হইতে নতুন বেতন কাঠামোর বিবরন</th>
                </tr>
                <tr>
                    <th>মজুরীর বিবরণ</th>
                </tr>
                <tr>
                    <td>মোট বেতন= {{en2bnNumber(round($employeeData->current_basic+$employeeData->current_houserent+$employeeData->current_medical+$employeeData->current_transport+$employeeData->current_food+$employeeData->emp_bonus,0))}} গ্রেড {{$employeeData->grade_name}}</td>
                </tr>
                <tr>
                    <td>পদবীঃ  @if($employeeData->designationBangla=='') {{$employeeData->designation}} @else {{$employeeData->designationBangla}} @endif</td>
                </tr>
                <tr>
                    <td>মূল বেতন {{en2bnNumber(round($employeeData->current_basic,0))}}</td>
                </tr>
                <tr>
                    <td>বাড়ী ভাড়া {{en2bnNumber(round($employeeData->current_houserent,0))}}</td>
                </tr>
                <tr>
                    <td>চিকিৎসা ভাতা {{en2bnNumber(round($employeeData->current_medical,0))}}</td>
                </tr>
                <tr>
                    <td>যাতায়াত {{en2bnNumber(round($employeeData->current_transport,0))}}</td>
                </tr>
                <tr>
                    <td>খাদ্য ভাতা {{en2bnNumber(round($employeeData->current_food,0))}}</td>
                </tr>
                <tr>
                    <td>মোটঃ {{en2bnNumber(round($employeeData->current_basic+$employeeData->current_houserent+$employeeData->current_medical+$employeeData->current_transport+$employeeData->current_food+$employeeData->emp_bonus,0))}}</td>
                </tr>
            </table>
        </div>
    </div>
  <br>
  <div class="description">
      উপরোল্লিখিত ছক (খ) মোতাবেক আপনার বর্তমান বেতন কাঠমো করা হইল । যা ০১ লা ডিসেম্বর ২০১৮ ইং তারিখ হইতে  করা হইবে ।
  </div>

 <div class="description">
     <br><br><br>
     ধন্যবাদান্তে
     <br><br>
     কর্তৃপক্ষ<br><br>
     অনুলিপিঃ<br><br>
 </div>
  <div class="foter1 description">
      ১) মানব সম্পদ বিভাগ ।<br>
      ২) হিসাব বিভাগ ।<br>
      ৩) ব্যক্তিগত নথি ।
  </div>

  <div class="foter2 description">
     <h4> কপি বুঝিয়া পাইয়া নিম্নে স্বাক্ষর করিলাম</h4>
      শ্রমিক স্বাক্ষর
  </div>
        @else
        increment Bonus Not Found
   @endif
</body>
</html>




