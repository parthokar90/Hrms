@extends('layouts.master')
@section('title', 'Salary Process Month')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                @if(Session::has('procesdeletemonth'))
                    <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('procesdeletemonth') }}</p>
                @endif

                @if(Session::has('procesupdate'))
                    <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('procesupdate') }}</p>
                @endif
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong> Salary Process Completed Month List </strong></h3>
                    </div>
                    <div class="panel-content table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Order</th>
                                <th>Process By</th>
                                <th>Month</th>
                                <th>Time</th>
                                <th>Date</th>
                                <th>Total Process</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $order=0; @endphp
                            @foreach($data as $process)
                                @php $order++; @endphp
                                <tr>
                                    <td>{{$order}}</td>
                                    <td>{{$process->name}}</td>
                                    <td>{{date('F-Y',strtotime($process->month))}}</td>
                                    <td>{{ \Carbon\Carbon::parse($process->created_at)->diffForHumans() }}</td>
                                    <td>{{date('d-m-Y',strtotime($process->created_at))}}</td>
                                    <td>{{$process->total}} Times</td>
                                    <td>
                                        @if($process->status==1)
                                            <span style="background: green;color: #ffffff;padding: 4px">Complete</span>
                                        @else
                                            <span style="background:darkred;color: #ffffff;padding: 4px">Processing</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($process->status==1)
                                            <a onclick="return confirm('are you sure??')" href="{{ url('/process/update/'.$process->month )}}" class="btn btn-danger btn-sm">Processing</a>
                                        @else
                                            <a onclick="return confirm('are you sure??')" href="{{url('process/update/'.$process->month)}}" class="btn btn-success btn-sm">Complete</a>
                                        @endif
                                        <a href="{{ url('/process/delete/'.$process->month) }}" onclick="return confirm('Are you sure to delete?')" title="Delete" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{--{{$data->links()}}--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    @include('include.copyright')
@endsection