@php
use Carbon\Carbon;
@endphp
@extends('layouts.master')
@section('title', 'Payslip')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3>Employee<strong> PaySlip</strong></h3>
                    </div>
                    <a class="btn btn-success btn-sm">Generate Payslip</a>
                    <!-- Modal -->
                    <div class="modal fade" id="off_day" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Weekend Holiday</h4>
                                </div>
                                <div class="modal-body">
                                    {{Form::open(array('url' => 'complete_off','method' => 'post'))}}
                                    <div class="form-group">
                                        <label for="pwd">Weekend:</label>
                                        <input type="number" name="weekend" class="form-control form-white" placeholder="only number" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">Holiday:</label>
                                        <input type="number" name="holiday" class="form-control form-white" placeholder="only number" required>
                                    </div>
                                    <button type="submit" class="btn btn-success">Create</button>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content">
                        <div class="panel-content pagination2 table-responsive">
                            <table class="table table-hover table-dynamic">
                                <p class="text-center">Current Month <?php echo date('d-M-Y'); ?></p>
                                <p class="text-center">Total day in month : <?php echo date('t'); ?></p>
                                <thead>
                                <tr>
                                    <th>Sn</th>
                                    <th>Name</th>
                                    <th>Grade</th>
                                    <th>Basic</th>
                                    <th>House</th>
                                    <!--<th>Medical</th>-->
                                     <!--<th>Food</th>-->
                                    <!--<th>Transport</th>-->
                                    <!--<th>Others</th>-->
                                    <th>Gross</th>
                                     <!--<th>Leave</th>-->
                                     <th>work days</th>
                                     <!--<th>weekend</th>-->
                                     <!--<th>holiday</th>-->
                                    <th>leave</th>
                                    <th>Present</th>
                                    <th>Absent</th>
                                    <th>Deduction</th>
                                    {{--<th>total pay days</th>--}}
                                    {{--<th>deduction adv</th>--}}
                                    {{--<th>deduction absent</th>--}}
                                    <th>gross pay</th>
                                    <th>Overtime</th>
                                    <th>rat overtime</th>
                                    <th>amt overtime</th>
                                    <th>Bonus</th>
                                    <th>net wages</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--@php--}}
                                    {{--$order++;--}}
                                    {{--$current_month_days=date('t');--}}
                                    {{--$basic=$payslip->basic_salary;--}}
                                    {{--$final_house_rant=$basic/100*40;--}}
                                    {{--$workdays=$current_month_days-$off_day->weekend-$off_day->holiday-5-2;--}}
                                    {{--$gross=$payslip->basic_salary+$final_house_rant+$payslip->medical+$payslip->food+$payslip->transport+$payslip->other;--}}
                                    {{--$start=$payslip->total_overtime;--}}
                                    {{--$company_working_day=$current_month_days-$off_day->weekend-$off_day->holiday;--}}
                                    {{--$total_over_time=floatval($payslip->total_overtime);--}}
                                    {{--$overtime_rate=$payslip->basic_salary/$company_working_day/8*2;--}}
                                    {{--$total_overtime_amount=$total_over_time*$overtime_rate;--}}
                                    {{--$absent_amount=300;--}}
                                    {{--$gross_pay=$gross-$absent_amount;--}}
                                    {{--$bonus=$payslip->total_employee_bonus;--}}
                                    {{--$net_wages=$gross_pay+$total_overtime_amount+$bonus;--}}
                                {{--@endphp--}}
                                @php $order=0;
                                     function dayCalculator($d1,$d2){
                                    $date1=strtotime($d1);
                                    $date2=strtotime($d2);
                                    $interval1=1+round(abs($date1-$date2)/86400);
                                    $festivalLeave = DB::table('tb_festival_leave')->get();
                                    $dcount=0;
                                    foreach($festivalLeave as $fleave){
                                        if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->end_date) <= $date2){
                                            $dcount+=1+round(abs(strtotime($fleave->start_date)-strtotime($fleave->end_date))/86400);

                                        }
                                        else if(strtotime($fleave->start_date) >= $date1 && strtotime($fleave->start_date) <= $date2 && strtotime($fleave->end_date) > $date2){
                                            $dcount+=1+round(abs(strtotime($fleave->start_date)-$date2)/86400);

                                        }
                                        else{
                                            continue;
                                        }
                                    }
                                    $weekends=DB::table('week_leave')->where(['status'=>1])->pluck('day')->unique()->toArray();
                                    $start_date=date('Y-m-d', $date1);
                                    $end_date=date('Y-m-d', $date2);
                                   $key=0;
                                    for ($i = Carbon::createFromFormat('Y-m-d', $start_date);$i->lte(Carbon::createFromFormat('Y-m-d', $end_date)); $i->addDay(1)) {
                                        $dates = $i->format('Y-m-d');
                                        $timestamp = strtotime($dates);
                                        $day = date('l', $timestamp);
                                        for($j=0;$j<count($weekends);$j++)
                                        {
                                            if($day==$weekends[$j]){
                                                $key++;
                                            }
                                        }
                                    }
                                    $interval=(int)$interval1-((int)$dcount+$key);
                                    return $interval;
                                }
                                @endphp
                                @foreach($data as $payslip)
                                    @php
                                        $order++;
                                        $current_month_days=date('t');
                                        $basic=$payslip->basic_salary;
                                        $final_house_rant=$payslip->house_rant;
                                        $gross=$payslip->basic_salary+$final_house_rant+$payslip->medical+$payslip->food+$payslip->transport+$payslip->other;
                                        $current_month_day=date('t');
                                        $current_month_first=date('Y-m-01');
                                        $current_month_last=date('Y-m-t');
                                        $working_day=dayCalculator($current_month_first,$current_month_last);
                                        $actual_present_days=$payslip->employee_present_days;
                                        $absentday=$working_day-$payslip->total_leaves_taken-$payslip->employee_present_days;
                                        $deduction=$payslip->basic_salary/$current_month_day*$absentday;
                                        $actual_deduction=round($deduction,2);
                                        $overtime_rate=$payslip->basic_salary/208*2;
                                        $actual_overtime_rate=round($overtime_rate,2);
                                        $overtime_amount=$actual_overtime_rate*$payslip->overtime;
                                        $gross_pay=$gross-$deduction;
                                        $actual_gross_pay=round($gross_pay,2);
                                        $bonus=$payslip->total_employee_bonus;
                                        $net_wages=$gross_pay+$bonus+$overtime_amount+$payslip->bonus_amount;
                                        $actual_net_wages=round($net_wages,2);
                                    @endphp
                                    <tr>
                                        <td>{{$order}}</td>
                                        <td>{{$payslip->empFirstName}} {{$payslip->empLastName}}</td>
                                        <td>{{$payslip->grade_name}}</td>
                                        <td>{{$payslip->basic_salary}}</td>
                                        <td>{{$final_house_rant}}</td>
                                        <!--<td>{{$payslip->medical}}</td>-->
                                         <!--<td>{{$payslip->food}}</td>-->
                                        <!--<td>{{$payslip->transport}}</td>-->
                                         <!--<td>{{$payslip->other}}</td>-->
                                        <td>{{$gross}}</td>
                                         <!--<td>5</td>-->
                                         <td>{{$working_day}}</td>
                                        {{--<td>{{$employee_working_days}}</td>--}}
                                        <td>
                                            @if($payslip->total_leaves_taken=='')
                                                0
                                            @else
                                                {{$payslip->total_leaves_taken}}
                                            @endif
                                        </td>
                                        <td>{{$payslip->employee_present_days}}</td>
                                        <td>{{$absentday}}</td>
                                        <td>৳ {{$actual_deduction}}</td>
                                        <td>৳ {{$actual_gross_pay}}</td>
                                        <td>
                                            @if($payslip->overtime=='')
                                                0 Hour
                                                @else
                                                {{$payslip->overtime}} Hour
                                                @endif
                                        </td>
                                        <td>{{$actual_overtime_rate}}</td>
                                        <td>{{$overtime_amount}} Taka</td>
                                        <td>{{$bonus}}</td>
                                        <td>৳ {{$actual_net_wages}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection