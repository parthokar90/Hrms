@extends('layouts.master')
@section('title', 'Bonus Settings')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
          @if(Session::has('message'))
              <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
           @endif
                <div class="panel">
                    <div class="panel-header">
                          <div class="row">
                            <div class="col-md-4">
                                <h3>Bonus <strong> Settings</strong></h3>
                            </div>
                          </div>
                    </div>
                       <div class="panel-content">
                            {{Form::open(array('url' => 'bonus/settings/update','method' => 'post'))}}
                                    <div class="form-group">
                                        <label for="gradename">Select Work Group</label>
                                        <select  name="work_group" class="form-control form-white">
                                            @foreach($work_groups as $cat)
                                            @php
                                            $selected = '';
                                            if($cat->work_group == $data->work_group)    // Any Id
                                            {
                                            $selected = 'selected="selected"';
                                            }
                                            @endphp
                                              <option value='{{ $cat->work_group }}' {{$selected}} >{{$cat->work_group }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="basicamount">Title</label>
                                        <input type="text" value="{{$data->fest_title}}" name="fest_title" class="form-control form-white"  placeholder="Title" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="basicamount">Festival Start</label>
                                        <input type="text" value="{{$data->fest_start}}" name="fest_start" class="form-control form-white date-picker"  placeholder="Select date" required>
                                    </div>
                                   <div class="form-group">
                                       <label for="medicalamount">Year</label>
                                       <input type="number" step="any" value="{{$data->condition_year}}" name="condition_year" step="any" class="form-control form-white"  placeholder="Year" required>
                                   </div>
                                   <div class="form-group">
                                    <label for="foodamount">Percentage Year</label>
                                    <input type="number" step="any" value="{{$data->year_percent}}"  name="year_percent"  class="form-control form-white"  placeholder="Percent" required>
                                   </div>
                                   <div class="form-group">
                                       <label for="transportamount">Month</label>
                                       <input type="number" step="any" value="{{$data->condition_month}}"  name="condition_month" step="any" class="form-control form-white"  placeholder="Month" required>
                                   </div>
                                   <div class="form-group">
                                    <label for="foodamount">Percentage Month</label>
                                    <input type="number" step="any" value="{{$data->month_percent}}"  name="month_percent"  class="form-control form-white"  placeholder="Percent" required>
                                   </div>
                                   <input type="hidden" name="id" value="{{$data->id}}">
                                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                             {{ Form::close() }}
                         </div>
                    </div>
             </div>
    @include('include.copyright')
@endsection