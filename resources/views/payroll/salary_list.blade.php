@extends('layouts.master')
@section('title', 'Employee Salary')
@section('content')
    {{--<script>--}}
        {{--setTimeout(function() {--}}
            {{--$('#alert_message').fadeOut('fast');--}}
        {{--}, 5000);--}}
    {{--</script>--}}
    <div class="page-content">
        {{--@if(Session::has('message'))--}}
            {{--<p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>--}}
        {{--@endif--}}
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> <strong>Salary </strong> Sheet</h3>
                        <a href="{{url('payroll/grade')}}" class="btn btn-success btn-sm">Add More</a>
                    </div>

                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>EmpId</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Gross</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>1</td>
                                    <td>{{$item->employeeId}}</td>
                                    <td>{{$item->empFirstName}}</td>
                                    <td>{{$item->designation}}</td>
                                    <td>1</td>
                                    <td><a class="btn btn-default btn-sm" href=""><i class="fa fa-eye"></i></a></td>
                                </tr>

                                <!-- Modal -->
                                <div class="modal fade" id="id" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Update Grade</h4>
                                            </div>
                                            <div class="modal-body">
                                                {{Form::open(array('url' => 'grade_update','method' => 'post'))}}
                                                <div class="form-group">
                                                    <label for="gradename">Grade Name</label>
                                                    <input type="text" name="grade_name" class="form-control form-white" id="gradename" value="" placeholder="Enter Grade Name" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="basicamount">Basic</label>
                                                    <input type="number" name="basic_amount" step="any" class="form-control form-white" id="basicamount" value="" placeholder="Amount" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="rantamount">House Rent</label>
                                                    <input type="number" name="house_amount" step="any" class="form-control form-white" id="rantamount" value="" placeholder="Amount" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="medicalamount">Medical</label>
                                                    <input type="number" name="medical_amount" step="any" class="form-control form-white" id="medicalamount" value="" placeholder="Amount" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="transportamount">Transportation</label>
                                                    <input type="number" name="transport_amount" step="any" class="form-control form-white" id="transportamount" value="" placeholder="Amount" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="foodamount">Food</label>
                                                    <input type="number" name="food_amount" step="any" class="form-control form-white" id="foodamount" value="" placeholder="Amount" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="othersamount">Others</label>
                                                    <input type="number" name="others_amount" step="any" class="form-control form-white" id="othersamount" value="" placeholder="Amount" required>
                                                </div>
                                                <input type="hidden" name="user_name" value="{{auth::user()->name}}">
                                                <input type="hidden" name="grade_id" value="">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection