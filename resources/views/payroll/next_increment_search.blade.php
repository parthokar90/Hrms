@extends('layouts.master')
@section('title', 'Increment Employee Salary')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">
                Incrementable Employees List
            </div>
            <div class="panel-body">
                <div class="col-md-12" style="padding: 0">
                    {{Form::open(array('url' => 'employee/year/next/increment/delete','method' => 'post'))}}
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>EmpId</th>
                        <th>Name</th>
                        <th>Join</th>
                        <th>Group</th>
                        <th>Year</th>
                        <th>Salary</th>
                        <th>Next Increment</th>
                        <th>Increment letter</th>
                        {{--<th>Status</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($next_increment as $increments)
                            <tr>
                                <td>{{$increments->employeeId}}</td>
                                <td>{{$increments->empFirstName}}</td>
                                <td>{{$increments->empJoiningDate}}</td>
                                <td>{{$increments->work_group}}</td>
                                <td>{{$increments->year}}</td>
                                <td>
                                    {{$increments->cur_salary}}
                                    <input type="hidden" name="delete_next_increment" value="{{$increments->next_increment}}">
                                </td>
                                <td>{{date('F-Y',strtotime($increments->next_increment))}}</td>
                                <td><a href="{{url('employee/increment/letter/download/'.$increments->ac_emp_id)}}"> <i class="btn btn-success btn-sm fa fa-download"></i> </a></td>
                            </tr>
                    @endforeach
                    </tbody>
                </table>
                <button onclick="return confirm('Are you sure??')" style="background: red;width: 100%" type="submit" class="btn btn-danger btn-lg">Delete</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection