@extends('layouts.master')
@section('title', 'Bonus Settings')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
          @if(Session::has('message'))
              <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
           @endif
                <div class="panel">
                    <div class="panel-header">
                          <div class="row">
                            <div class="col-md-4">
                                <h3>Bonus <strong> Settings</strong></h3>
                            </div>
                          </div>
                    </div>
                       <div class="panel-content">
                           <table class="table table-bordered">
                               <tr>
                                   <th>Work Group</th>
                                   <th>Title</th>
                                   <th>Festival Start</th>
                                   <th>Year</th>
                                   <th>Percentage Year</th>
                                   <th>Month</th>
                                   <th>Percentage Month</th>
                                   <th>Action</th>
                               </tr>
                            @foreach($list as $item)
                             <tr>
                             <td>{{$item->work_group}}</td>
                             <td>{{$item->fest_title}}</td>
                                <td>@if($item->fest_start!=''){{date('d-F-Y',strtotime($item->fest_start))}}@endif</td>
                                <td>{{$item->condition_year}}</td>
                                <td>{{$item->year_percent}} %</td>
                                <td>{{$item->condition_month}}</td>
                                <td>{{$item->month_percent}} %</td>
                             <td><a class="btn btn-sm btn-success" href="{{url('bonus/settings/edit/'.$item->id)}}"><i class="fa fa-pencil"></i></a></td>
                             </tr>
                            @endforeach 
                           </table>
                           @if($list->count()<2)
                            {{Form::open(array('url' => 'bonus/settings/store','method' => 'post'))}}
                                    <div class="form-group">
                                        <label for="gradename">Select Work Group</label>
                                        <select  name="work_group" class="form-control form-white" required>
                                            <option value="">Select</option>
                                            @foreach($work_groups as $groups)
                                              <option value="{{$groups->work_group}}">{{$groups->work_group}}</option>   
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="basicamount">Title</label>
                                        <input type="text" required name="fest_title" class="form-control form-white"  placeholder="Title">
                                    </div>
                                    <div class="form-group">
                                        <label for="basicamount">Festival Start</label>
                                        <input type="text" required name="fest_start" class="form-control form-white date-picker"  placeholder="Select date" autocomplete="off">
                                    </div>
                                   <div class="form-group">
                                       <label for="medicalamount">Year</label>
                                       <input type="number" step="any" required name="condition_year"  class="form-control form-white"  placeholder="Year">
                                   </div>
                                   <div class="form-group">
                                    <label for="foodamount">Percentage Year</label>
                                    <input type="number" step="any"  name="year_percent"  class="form-control form-white"  placeholder="Percent" required>
                                   </div>
                                   <div class="form-group">
                                       <label for="transportamount">Month</label>
                                       <input type="number" step="any" required name="condition_month"  class="form-control form-white"  placeholder="Month">
                                   </div>
                                   <div class="form-group">
                                    <label for="foodamount">Percentage Month</label>
                                    <input type="number" step="any"  name="month_percent"  class="form-control form-white"  placeholder="Percent" required>
                                </div>
                                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                             {{ Form::close() }}
                             @endif
                         </div>
                    </div>
             </div>
    @include('include.copyright')
@endsection