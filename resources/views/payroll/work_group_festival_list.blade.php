@extends('layouts.master')
@section('title', 'Festival Bonus')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 style="font-weight: bold">Festival Bonus</h5>
            </div>
            <div class="panel-body">
                {{Form::open(array('url' => '/festival/bonus/store','method' => 'post'))}}
                <div class="col-md-2">
                    <button style="margin-top:10px" type="submit" name="percentt_btn" id="percent_button" class="btn btn-success"><i class="fa fa-save"></i> Save Bonus</button>
                </div>
                <div id="title_id" class="col-md-4">
                    <div class="form-group">
                        <input type="hidden" value="{{$condition->fest_title}}" class="form-control" name="festival_title" placeholder="Title">
                    <input type="hidden" name="fest_start_day" value="{{$condition->fest_start}}">
                    </div>
                </div>
        
                <div class="panel-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Sl</th>
                            <th>Employee Id</th>
                            <th>Employee</th>
                            <th>Work Group</th>
                            <th>Joining Date</th>
                            <th>Salary</th>
                            <th>Percentage Year</th>
                            <th>Percentage Month</th>
                            <th>Year</th>
                            <th>Month</th>
                            <th>Total Bonus</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $order=0; @endphp    
                        @foreach($data as $employee)
                           @php
                             $fest_one=$condition->fest_start; 
                             $order++;   
                           @endphp
                           @php 
                           $date1s = new DateTime($employee->empJoiningDate);
                           $date2s = new DateTime($fest_one);
                           $intervals = $date1s->diff($date2s);
                           $year_condition=$intervals->y; 
                           $month_condition=$intervals->m; 
                           @endphp
                            <tr>
                            <td>{{$order}}</td>
                                <td>{{$employee->employeeId}}</td>
                                <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                                <td>{{$employee->work_group}}</td>
                                <td>{{$employee->empJoiningDate}}</td>
                                <td>{{$employee->total_employee_salary}}</td>
                                <td>{{$condition->year_percent}}</td>
                                <td>{{$condition->month_percent}}</td>
                            <td>
                                @php 
                                    $date1 = new DateTime($employee->empJoiningDate);
                                    $date2 = new DateTime($fest_one);
                                    $interval = $date1->diff($date2);
                                    echo $interval->y. "years"; 
                                @endphp
                            </td>

                            <td>
                                @php 
                                    $date1 = new DateTime($employee->empJoiningDate);
                                    $date2 = new DateTime($fest_one);
                                    $interval = $date1->diff($date2);
                                    echo $interval->m. "Month"; 
                                @endphp
                            </td>
                               <td>
                                @if($year_condition>=$condition->condition_year) 
                                    @php 
                                        $total_bonus=$employee->total_employee_salary/100*$condition->year_percent;  
                                    @endphp
                                  @elseif($month_condition>=$condition->condition_month)
                                    @php 
                                    $total_bonus=$employee->total_employee_salary/100*$condition->month_percent;  
                                    @endphp
                                    @else 
                                    @php $total_bonus=0; @endphp
                                  @endif
                                   {{$total_bonus}}
                               <input type="hidden" name="total_bonus[]" value="{{$total_bonus}}">
                                </td>
                            </tr>
                            <input type="hidden" name="dept_id[]" value="{{$employee->dept_id}}">
                            <input type="hidden" name="section_id[]" value="{{$employee->empSection}}">
                            <input type="hidden" name="work_group" value="{{ $condition->work_group}}">
                            <input type="hidden" name="emp_id[]" value="{{$employee->em_id}}">
                            <input type="hidden" name="total_employee_salary[]" value="{{$employee->total_employee_salary}}">
                            <input type="hidden" class="form-control" name="emp_gross" value="{{$employee->total_employee_salary}}"> 
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    {{-- <script>
        $(document).ready(function() {
            // $('#amount_value').keyup(function () {
            //     $("#production_button").show();
            //     $("#percent_div").hide();
            //     $(".percent").hide();
            //     $("#p_title").hide();
            //     $("#title_id").show();
            //     var input = document.getElementsByClassName("amount");
            //     for (var i = 0; i < input.length; i++){
            //         input[i].value = document.getElementById('amount_value').value;
            //     }
            // });

            // $('#percent_val').keyup(function () {
            //     $("#percent_button").show();
            //     $("#amount_div").hide();
            //     $(".amount").hide();
            //     $("#amount_title").hide();
            //     $("#title_id").show();
            //     var input = document.getElementsByClassName("percent");
            //     for (var i = 0; i < input.length; i++){
            //         input[i].value = document.getElementById('percent_val').value;
            //     }
            // });
        });
    </script> --}}


    <script>
        $(document).ready(function() {
            $('#year_bonus_month').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
        });
    </script>
    @include('include.copyright')
@endsection