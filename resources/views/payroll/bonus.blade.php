@extends('layouts.master')
@section('content')
    <div class="page-content page-content-hover-1">
        <div class="row panel myAnimation"  style="border:1px solid #999">
            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Employee Bonus</b></h2>
                    <hr>
                </div>
            </div>
            {{--<div class="col-xlg-4 col-lg-4 col-sm-4">--}}
                {{--<a target="_BLANK" href="{{url('increment/bonus')}}">--}}
                    {{--<div class="panel">--}}
                        {{--<div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(0,137,123);color:white;">--}}
                            {{--<center>--}}
                                {{--<div class="row" >--}}
                                    {{--<div class="">--}}
                                        {{--<center><i class="fa fa-money f-40"></i></center>--}}
                                    {{--</div>--}}
                                    {{--<br>--}}
                                    {{--<div class="">--}}
                                        {{--<center>--}}
                                            {{--<span class="f-14"><b>Increment in Salary</b></span>--}}
                                        {{--</center>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</center>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="col-xlg-3 col-lg-3 col-sm-3">--}}
                {{--<a target="_BLANK" href="{{url('attendance/bonus')}}">--}}
                    {{--<div class="panel">--}}
                        {{--<div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:rgb(106,27,154);color:white;">--}}
                            {{--<center>--}}
                                {{--<div class="row" >--}}
                                    {{--<div class="">--}}
                                        {{--<center><i class="fa fa-money f-40"></i></center>--}}
                                    {{--</div>--}}
                                    {{--<br>--}}
                                    {{--<div class="">--}}
                                        {{--<center>--}}
                                            {{--<span class="f-14"><b>Attendance Bonus</b></span>--}}
                                        {{--</center>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</center>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
            <div class="col-xlg-3 col-lg-3 col-md-3 animated bounceIn">
                <a target="_BLANK" href="{{url('festival/bonus')}}">
                    <div class="panel">
                        <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:#c0392b;color:white;">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-money f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Festival Bonus </b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xlg-3 col-lg-3 col-md-3 animated bounceIn">
                <a target="_BLANK" href="{{url('production')}}">
                    <div class="panel">
                        <div class="panel-content" style="border: 1px solid #777;border-radius:3px;text-align:center;background:green;color:white;">
                            <center>
                                <div class="row" >
                                    <div class="">
                                        <center><i class="fa fa-money f-40"></i></center>
                                    </div>
                                    <br>
                                    <div class="">
                                        <center>
                                            <span class="f-14"><b>Production Bonus </b></span>
                                        </center>
                                    </div>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('body').on("keyup", "input[name=inc_percents]", function() {
               var basic=$(this).attr('data-basic');
               alert(basic);
               var percent=$(this).val();
                var id=$(this).attr('id');
                alert(id);
               var total_amount=basic/100*percent;
            });
        });
    </script>
    @include('include.copyright')
@endsection