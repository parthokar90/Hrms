@extends('layouts.master')
@section('title', 'Menual Overtime')
@section('content')
    <div class="page-content">
        @if(Session::has('overtime_add_manually'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('overtime_add_manually') }}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Employee Manual Overtime
                    </div>
                    <div class="panel-body">
                        {{Form::open(array('url' => '/manual/overtime/store','method' => 'post'))}}
                        <div class="col-md-4">
                           <label>Select Employee</label>
                            <select class="form-control" data-search="true" name="emp_id">
                               @foreach($employees as $employee)
                                <option value="{{$employee->id}}">{{$employee->empFirstName}} ({{$employee->employeeId}})</option>
                               @endforeach
                          </select>
                        </div>

                        <div class="col-md-4">
                            <label>Enter Overtime Hour(eg:2 Hour)</label>
                            {{--<input type="text" size="12" pattern="\d{1,2}:\d{2}([ap]m)?" name="overtime" class="form-control" autocomplete="off">--}}
                            <input type="number" name="overtime" class="form-control" autocomplete="off" required>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">Select Month</label>
                                <div class="prepend-icon">
                                    <input type="text" name="ot_bonus_month" id="ot_bonus_month" class="form-control format_date" autocomplete="off" required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#ot_bonus_month').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
                },
                onClose: function(dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
                }
            }).focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
            setTimeout(function() {
                $('#alert_message').fadeOut('fast');
            }, 5000);
        });
    </script>
    @include('include.copyright')
@endsection