@extends('layouts.master')
@section('title', 'Production Bonus')
@section('content')
    <div class="page-content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 style="font-weight: bold">Production Bonus ({{date('F-Y')}})</h5>
            </div>
            <div class="panel-body">
                <div class="col-md-2" id="amount_div">
                    <div class="form-group">
                        <label>Type Amount</label>
                        <input type="number" class="form-control amount_value" id="amount_value" required>
                    </div>
                </div>
                <div class="col-md-2" id="percent_div">
                    <div class="form-group">
                        <label>Type Percent</label>
                        <input type="number" id="percent_val" class="form-control percent_val" id="percent_value" required>
                    </div>
                </div>
                {{Form::open(array('url' => 'production/store/work_group','method' => 'post'))}}
                <div class="col-md-2">
                    <button style="display: none;margin-top: 22px;" type="submit" name="amountt_btn" id="production_button" class="btn btn-success">Save</button>
                </div>

                <div class="col-md-2">
                    <button style="display: none;margin-top: 22px;" type="submit" name="percentt_btn" id="percent_button" class="btn btn-success">Save</button>
                </div>
                <div class="panel-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Employee Id</th>
                            <th>Employee</th>
                            <th>Work Group</th>
                            <th id="amount_title">Amount</th>
                            <th id="p_title">Percent</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $employee)
                            <tr>
                                <td>{{$employee->employeeId}}</td>
                                <td>{{$employee->empFirstName}} {{$employee->empLastName}}</td>
                                <td>{{$employee->work_group}}</td>
                                <td><input type="text" class="form-control amount"  name="emp_amount[]" placeholder="amount"></td>
                                <td><input type="text" class="form-control percent"  name="emp_percent[]" placeholder="percent"></td>
                            </tr>
                            <input type="hidden" name="emp_id[]" value="{{$employee->id}}">
                            <input type="hidden" class="form-control" name="emp_gross[]" value="{{$employee->total_employee_salary}}">
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#amount_value').keyup(function () {
                $("#production_button").show();
                $("#percent_div").hide();
                $(".percent").hide();
                $("#p_title").hide();
                var input = document.getElementsByClassName("amount");
                for (var i = 0; i < input.length; i++){
                    input[i].value = document.getElementById('amount_value').value;
                }
            });

            $('#percent_val').keyup(function () {
                $("#percent_button").show();
                $("#amount_div").hide();
                $(".amount").hide();
                $("#amount_title").hide();
                var input = document.getElementsByClassName("percent");
                for (var i = 0; i < input.length; i++){
                    input[i].value = document.getElementById('percent_val').value;
                }
            });
        });
    </script>
    @include('include.copyright')
@endsection