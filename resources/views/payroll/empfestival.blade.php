@extends('layouts.master')
@section('title', 'Festival Bonus')
@section('content')
    <div class="page-content">
        @if(Session::has('festival_bonus_work_group'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('festival_bonus_work_group') }}</p>
        @endif
        @if(Session::has('error'))
        <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 style="font-weight: bold">Festival Bonus</h5>
            </div>
            <div class="panel-body">
                    <div class="col-md-12">
                        {{Form::open(array('url' => '/festival/bonus/work/group','method' => 'post'))}}
                        <div class="col-md-6">
                        <div class="form-group">
                            <label>Select Work Group</label>
                            <select name="work_group_employee"  class="form-control" required>
                                @foreach($work_groups as $groups)
                                 <option value="{{$groups->work_group}}">{{$groups->work_group}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Select Festival Date</label>
                            <select name="fest_date"  class="form-control" required>
                                @foreach($fest_dates as $fest_date)
                                 <option value="{{$fest_date->fest_start}}">{{date('d-M-Y',strtotime($fest_date->fest_start))}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>    
                        <button style="margin-left:12px" type="submit" class="btn btn-success"><i class="fa fa-save"></i>Save</button>
                        {{ Form::close() }}
                    </div>
               </div>
           </div>
       </div>
    {{-- <script>
        $(document).ready(function() {
            $("#production_submission").change(function(){
                $(this).closest('form').attr('target', '_blank').submit();
            });
        });
    </script> --}}
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    @include('include.copyright')
@endsection