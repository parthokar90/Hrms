@extends('layouts.master')
@section('title', 'Activity Log History Report')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Activity Log History</b></h2>
                    <hr>
                </div>
            </div>

            {!! Form::open(['method'=>'get','route'=>'activity_logs.history_data']) !!}
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Start Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="startDate" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select a date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">End Date<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <input type="text" name="endDate" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select a date...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-form-gap"></div>
                    <label class="col-md-3">Select User<span class="clon">:</span></label>
                    <div class="col-md-9">
                        <select name="userId" class="form-control" data-search="true">
                            <option value="{{base64_encode('all')}}">All</option>
                            @foreach($users as $user)
                                <option value="{{base64_encode($user->id)}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group padding-bottom-30-percent">
                    <div class="col-md-9 col-md-offset-3">
                        <hr>
                        <input type="submit" value="Preview" name="viewType" class="btn btn-success margin-top-10">

                        <!-- 
                            <span class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Generate/Export
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> &nbsp; Generate PDF </button></li>
                                    <li><button type="submit" value="Generate Excel" name="viewType" class="btn btn-blue"><i class="fa fa-file-excel-o"></i> &nbsp; Generate Excel </button></li>
                                </ul>
                            </span> 
                        -->

                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>

    @include('include.copyright')
@endsection