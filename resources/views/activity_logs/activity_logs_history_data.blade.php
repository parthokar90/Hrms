@extends('layouts.master')
@section('title', 'Activity Log History Report')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Activity Log History</b></h2>
                    <hr>
                </div>
            </div>

            <div class="col-xlg-12 col-md-12">
                <table id="datatable-responsive" class="table table-striped table-bordered animated fadeIn">
                    <thead>
                      <tr>
                        <th width="5%">Serial</th>
                        <th>Headtag</th>
                        <th width="50%">Details</th>
                        <th>Date</th>
                        <th>User</th>
                      </tr>
                    </thead>


                    <tbody>
                    @php $i=0; @endphp
                    @foreach($activity_logs_history as $alh)
                      <tr>
                        <td>{{++$i}}</td>
                        <td>{{$alh->acType}}</td>
                        <td>{{$alh->details}}</td>
                        <td>{{date('d-M-Y', strtotime($alh->created_at))}}</td>
                        <td>{{$alh->name}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
                <div style="text-align: center;">
                    {{$activity_logs_history->links()}}
                </div>
            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>

    @include('include.copyright')
@endsection