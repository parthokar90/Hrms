@extends('layouts.master')
@section('title', 'Maternity Leave History')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('message1'))
                <p id="alert_message" class="alert alert-success">{{Session::get('message1')}}</p>
            @endif
            @if(Session::has('message2'))
            <p id="alert_message" class="alert alert-success">{{Session::get('message2')}}</p>
            @endif
                <div class="col-lg-12 portlets">
                    <div class="panel">
                        <div class="panel-header ">
                            <h3><i class="fa fa-table"></i> <strong>Maternity leave history</strong></h3>
                        </div>
                        <div class="panel-content pagination2 table-dyanamic">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Employee ID</th>
                                    <th>Name</th>
                                    <th>Designation</th>
                                    <th>Leave Period</th>
                                    <th>Previous Three Months Salary</th>
                                    <th>Total Working Days</th>
                                    <th>Per Day Salary</th>
                                    <th>Total Payable Amount</th>
                                    <th>Per Instalment</th>
                                    <th>Payment</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php $order=0 @endphp
                                    @foreach($maternityLeaveHistory as $leave)
                                        @php
                                        
                                        $order++
                                        @endphp
                                        <tr>
                                            <td>{{$order}}</td>
                                            <td>{{$leave->employeeId}}</td>
                                            <td>{{$leave->empFirstName}} {{$leave->empLastName}}</td>
                                            <td>{{$leave->designation}}</td>
                                            <td>{{date("d M", strtotime($leave->leave_starting_date))}}  to  {{date("d M Y", strtotime($leave->leave_ending_date))}}</td>
                                            <td>{{number_format(($leave->previous_three_month_salary))}} BDT</td>
                                            <td>{{$leave->total_working_days}} days</td>
                                            <td>{{number_format((float)(($leave->previous_three_month_salary)/($leave->total_working_days)), 2, '.', '')}} BDT</td>
                                            <td>{{number_format(($leave->total_payable_amount),2)}} BDT</td>
                                            <td>{{number_format(($leave->per_instalment),2)}} BDT</td>
                                            <td>
                                                @if(($leave->other3)=='00')
                                                <a href= "{{url('pay_first_installment/'.$leave->id)}}"><button type="submit" class="btn btn-success btn-sm">Pay First Installment</button></a>
                                                @elseif(($leave->other3)=='10')
                                                <a href= "{{url('pay_second_installment/'.$leave->id)}}"><button type="submit" class="btn btn-success btn-sm">Pay Second Installment</button></a>
                                                @elseif(($leave->other3)=='11')
                                                <h3>paid</h3>
                                                @endif
                                            </td>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @include('include.copyright')
@endsection