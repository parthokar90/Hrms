@extends('layouts.master')
@section('title', 'Apply for leave')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('leavereq'))
                <p id="alert_message" class="alert alert-success">{{Session::get('leavereq')}}</p>
            @endif
             @if(Session::has('leaveDelete'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('leaveDelete')}}</p>
            @endif
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header">
                        <h3> <strong>Request for Leave</strong></h3>
                    </div>
                    <div class="panel-body bg-white">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                {!! Form::open(['method'=>'POST','url' => 'store_leave_request','request_leave', 'files'=>true]) !!}
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="required form-group">
                                                <label class="control-label">Choose Leave Type</label>
                                                <select onchange="interviewCheck(this);" required name="leave_type_id" id="leave_type_id" class="form-control">
                                                    <option value="grade_null">Select leave</option>
                                                    @foreach($leaveType as $leave)
                                                     <option value="{{$leave->id}}">{{$leave->leave_type}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                   
                                    
                                        <div id="CD2"> 
                                        <div class="col-sm-4">
                                            <div class="required form-group">
                                                <label class="control-label">Starting Date</label>
                                                <div class="append-icon">
                                                    <input type="text" class="date-picker form-control" name="leave_starting_date" class="form-control" placeholder="Select your leave starting date"  required>
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    
                                   
                                    <div id="CD1">
                                        <div class="col-sm-4">
                                            <div class="required form-group">
                                                <label class="control-label">Ending Date</label>
                                                <div class="append-icon">
                                                    <input type="text" class="date-picker form-control" name="leave_ending_date" class="form-control" placeholder="Select your leave ending date" >
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="  form-label">Enter Description</label>
                                            <div class="prepend-icon">
                                                <input type="text-area" id="description" name="description" class="form-control" placeholder="Enter description..">
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                                <label for="attachment">Attachment</label>
                                                <input type="file" name="attachment" step="any" class="form-control form-white" id="attachment"  placeholder="Upload Attachment (If any)">
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="user_name" value="{{auth()->user()->name}}">
                                <input type="hidden" name="emp_id" value="{{auth()->user()->emp_id}}">
                                <div class="text">
                                    <button type="submit" class="btn btn-embossed btn-primary">Apply</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            </div>
                        </div>
                   
                   @if(count($leave_history)!=0)
                        <div class="panel-header">
                            <h3><i class="fa fa-table"></i> <strong>your pending leave requests</strong></h3>
                        </div>
                        <div class="panel-content pagination2 table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Period</th>
                                    <th>Applied For</th>
                                    <th>Reason</th>
                                    <th>Applied At</th>
                                    <th>Current Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($leave_history as $leave)
                                        @php
                                        $applyDate=strtotime($leave->created_at);
                                        $leaveStart=strtotime($leave->leave_starting_date);
                                        $leaveEnd=strtotime($leave->leave_ending_date);
                                        
                                        if(($leave->status)==0){
                                            $status='Pending';
                                        }
                                        elseif(($leave->status)==1){
                                            $status='Approved';
                                        }
                                        elseif(($leave->status)==2){
                                            $status='Rejected';
                                        }
                                        @endphp
                                        <tr>
                                            <td>{{date("d F", $leaveStart)}}  to  {{date("d F", $leaveEnd)}}</td>
                                            <td>{{$leave->leave_type}}</td>
                                            <td><h5>Not available</h5></td>
                                            <td>{{date("d F Y", $applyDate)}}</td>
                                            <td>{{$status}}</td>
                                            @if(($leave->status)==0)
                                            <td><a href="{{ url('/leave/request/delete/'.$leave->id) }}"  class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></td>
                                            @else
                                            <td><h5>Not available</h5></td>
                                            @endif
                                    @endforeach

                                
                                </tbody>
                            </table>
                        </div>
                        @endif
                    
                </div>         
            </div>
        </div>
    </div>
    
    @include('include.copyright')

                        <script>
                            function interviewCheck(that) {
                                if(that.value==1){
                                   // alert('accepted');
                                   document.getElementById('CD2').style.display="block";
                                    document.getElementById('CD1').style.display="none";
                                }
                                else{
                                    // alert('accepted');
                                     document.getElementById('CD1').style.display="block";
                                     document.getElementById('CD2').style.display="block";
                                 }
                            }
                        </script>
@endsection