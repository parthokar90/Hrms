@extends('layouts.master')
@section('title', 'Assign Leave')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('applySuccess'))
                <p id="alert_message" class="alert alert-success">{{Session::get('applySuccess')}}</p>
            @endif

            @if(Session::has('error'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
            @endif
            
            @if ($errors->any())
                <div id="alert_message" class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Manually Assign Leave</strong></h2>
                    </div>
                    <div class="panel-body bg-white">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                {!! Form::open(['method'=>'POST','url' =>'leave/assign/mannual_assign', 'files'=>true]) !!}
                                <div class="row">

                                        <div class="col-sm-4">
                                            <div class="required form-group">

                                                {!! Form::label('employee_id','Employees',['class'=>'control-label']) !!}
                                                <div class="option-group">
                                                    {!! Form::select('employee_id',[''=>'Select Employee']+$employees,null,['class' => 'form-control employee_id','data-search'=>"true" ,'onchange'=>'interviewCheck()'])!!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="required form-group">
                                                <label class="control-label">Choose Leave Type</label>
                                                <select onchange="interviewCheck();" required name="leave_type_id" id="leave_type_id" class="form-control">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Attachment</label>
                                            <div class="">
                                                <input type="file" name="attachment" step="any" class="form-control form-white" id="attachment"  placeholder="Upload Attachment (If any)">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="CD1">
                                    <div class="col-sm-4">
                                        <div class="required form-group">
                                            <label> Leave Start Date </label>
                                            <div class="">
                                                <input type="text" id="leave_start_date" class="date-picker form-control" autocomplete="off" required name="startDate"   placeholder="Click here for pick start date ..." />
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div id="CD2">
                                        <div class="col-sm-4">
                                            <div class="required form-group">
                                                <label> Leave End Date </label>
                                                <div class="">
                                                    <input type="text" id="leave_end_date" class="date-picker form-control" autocomplete="off" name="endDate"  placeholder="Click here for pick end date ..." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Total Days</label>
                                                <div class="">
                                                    <input type="text" id="total_d" name="total_d" class="form-control form-white" id="attachment" readonly="true"  placeholder="Total Days">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> Reason for Leave</label>
                                            <div class="">
                                                    <textarea class="form-control" rows="4" name="leaveReason" placeholder="Enter Leave Reason..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <hr>
                                </div>
                                   
                                <div class="row">
                                        <div id="table">
                                            <div class="col-sm-6 portlets">
                                                @if(isset($data))
                                                    @include('leave.employeeLeaveAjazTable')
                                                @elseif(isset($leaveData))
                                                    @include('leave.employeeLeaveAjazTable2')
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12 center-block">
                                        <center>
                                            <button type="submit" class="btn btn-primary "><i class="fa fa-plus"></i> Add Application</button>
                                        </center>
                                        </div>


                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')

    <script>
        $( document ).ready(function() {
            // $('#leave_end_date').Click(function(event){
            //     alert('asd');
            // });
            $("#leave_end_date").on("change",function(){
                var leave_end_date = $(this).val();
                var leave_start_date=$('#leave_start_date').val();
                var date1 = new Date(leave_start_date);
                var date2 = new Date(leave_end_date);

                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                $('#total_d').val(diffDays+1);
                if(date1>date2){
                    $('#total_d').val(0);
                }
            });
            $('.employee_id').change(function (event) {
                var id=event.val;
//                alert(id);
                $.ajax({
                    url:"employee_leave_history/"+id,
                    method:'GET',
                    dataType:'html',
                    success: function (data) {
                        $('#table').html(data);

                    },
                    error:function (xhr) {
                        console.log('failed');
                    }
                });

            });
            $('.employee_id').change(function (event) {
                var id=event.val;
                $.ajax({
                    url:"employee_leave_type/"+id,
                    method:"get",
                    success:function (data) {
                        $('#leave_type_id').html(data);

                    }
                });

            });
        });
            function interviewCheck(val) {

                if($("#leave_type_id").val()!=1){
                    //alert('accepted');
                   document.getElementById('CD1').style.display="block";
                    document.getElementById('CD2').style.display="block";
                }
                else{
                    //alert('accepted');
                     document.getElementById('CD1').style.display="block";
                     document.getElementById('CD2').style.display="none";
                 }
                    var empID = $("#employee_id").val();
                    var div = $("#leave_type_id").val();
                    var endDate = $("#endDate").val();
                    var concate=empID+',' + div;
                    //var concate=empID+',' + div+','endDate;
                    //alert(concate);

                    $.ajax({
                        url:"employee_leave_history/"+concate,
                        method:'GET',
                        dataType:'html',
                        success: function (data) {
                            $('#table').html(data);

                        },
                        error:function (xhr) {
                            console.log('failed');
                        }
                    });

            }
        </script>
@endsection