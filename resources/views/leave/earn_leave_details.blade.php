<div class="container">
    <div class="row">
        <div class="col-sm-9 col-sm-offset-1">
            {!! Form::open(['method'=>'POST','action'=>['leaveController@earn_leave_store',1],'files'=>true]) !!}
            {!! Form::hidden('emp_id',$employee->id) !!}
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-3">{!! Form::label('empAttachmentTitle','Full Name:') !!}</div>
                    <div class="col-sm-8">{!! Form::text('empAttachmentTitle',$employee->empFirstName.' '.$employee->empLastName,['class'=>'form-control','readonly','required type'=>'text', 'placeholder'=>'Enter Title']) !!}</div>

                </div>
            </div>

            <br>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-3">{!! Form::label('empAttachmentTitle','Employee ID:') !!}</div>
                    <div class="col-sm-8">{!! Form::text('empAttachmentTitle',"$employee->employeeId",['class'=>'form-control','required type'=>'text', 'readonly','placeholder'=>'Enter Title']) !!}</div>

                </div>
            </div>

            <br>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-3">{!! Form::label('empAttachmentTitle','Joining Date:') !!}</div>
                    <div class="col-sm-8">{!! Form::text('empAttachmentTitle',\Carbon\Carbon::parse($employee->empJoiningDate)->format('j M Y'),['class'=>'form-control','readonly','required type'=>'text', 'placeholder'=>'Enter Title']) !!}</div>

                </div>
            </div>

            <br>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-3">{!! Form::label('empAttachmentTitle','Total Earn Leave:') !!}</div>
                    <div class="col-sm-8">{!! Form::text('empAttachmentTitle',floor($days/18)+$previous_earn_leave,['class'=>'form-control','readonly','required type'=>'text', 'placeholder'=>'Enter Title']) !!}</div>

                </div>
            </div>

            <br>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-3">{!! Form::label('earn_leave','Add Earn Leave:') !!}</div>
                    <div class="col-sm-8">{!! Form::text('earn_leave',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Number Of Days']) !!}</div>

                </div>
            </div>



            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary">Add</button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>