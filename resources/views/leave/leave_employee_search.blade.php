@extends('layouts.master')
@section('title', 'Employee Leave management')
@section('content')
    <style>
        .pagination li a {
            color: #A2A2A2;
            font-size: 12px;
            background: darkblue;
            color: #ffffff;
        }
    </style>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-4">
                                <h3><i class="fa fa-table"></i> <strong>Leave </strong> List</h3>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content table-responsive">
                            <form method="GET" action="{{url('leave/employee/search')}}">
                                {{@csrf_field()}}
                                <div class="form-group">
                                    <label>Enter Employee Id Or Name</label>
                                    <input type="text" class="form-control" name="keyword" required>
                                </div>    
                                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Search </button>
                                </form>
                      
                            @if($data->count()>0)
                            <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Leave type</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Reason</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $leavereq)
                                    <tr>
                                    <td>{{$leavereq->employeeId}}</td>
                                    <td>{{$leavereq->empFirstName}} {{$leavereq->empLastName}}</td>
                                    <td>{{$leavereq->designation}}</td>
                                    <td>{{$leavereq->leave_type}}</td>
                                    <td>{{date('d-m-Y',strtotime($leavereq->leave_starting_date))}}</td>
                                    <td>{{date('d-m-Y',strtotime($leavereq->leave_ending_date))}}</td>
                                    <td>{{$leavereq->description}}</td>
                                    <td>{{$leavereq->actual_days}}</td>
                                    <td>@if($leavereq->status==0) Pending @elseif($leavereq->status==1) Approved @else Rejected @endif</td>
                                        <td style="display: flex;">
                                            @if($leavereq->status==0)
                                            <a onclick="return confirm('Are you sure to Accept?')" href= "{{url('leave_accepted/'.$leavereq->leaves_id)}}"><button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Accept</button></a>
                                            <a onclick="return confirm('Are you sure to Reject?')" href= "{{url('leave_declined2/'.$leavereq->leaves_id)}}"><button type="button" class="btn btn-warning btn-sm" ><i class="fa fa-ban"></i> Reject</button>
                                            <a href="{{route('leave_request.delete',base64_encode($leavereq->leaves_id))}}" onclick="return confirm('Are you sure to delete?')" title="Delete" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                            @endif
                                            @if($leavereq->status==1)
                                                    <a onclick="return confirm('Are you sure to cancel?')" href= "{{url('leave_declined/'.$leavereq->leaves_id)}}"><button type="button" class="btn btn-danger btn-sm" ><i class="fa fa-ban"></i> Cancel</button>
                                            @endif
                                            @if($leavereq->status==2)
                                                <a onclick="return confirm('Are you sure to Accept?')" href= "{{url('leave_accepted/'.$leavereq->leaves_id)}}"><button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Accept</button></a>
                                          @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                 @else 
                                 <h4 class="text-center" style="color:red">No Leave Data Found</h4>
                                @endif
                               <div class="text-center">{{$data->links()}}</div>
                            </div>
                      </div>
                </div>
          </div>
     </div>
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    @include('include.copyright')
@endsection