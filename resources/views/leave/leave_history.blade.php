@extends('layouts.master')
@section('title', 'Leave History')
@section('content')
    <div class="page-content">
        <div class="row">
                <div class="col-lg-12 portlets">
                    <div class="panel">
                        <div class="panel-header panel-controls">
                            <h3><i class="fa fa-table"></i> <strong>your leave history</strong></h3>
                        </div>
                        <div class="panel-content pagination2 table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Period</th>
                                    <th>Applied For</th>
                                    <th>Reason</th>
                                    <th>Applied At</th>
                                    <th>Current Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($leave_history as $leave)
                                        @php
                                        $applyDate=strtotime($leave->created_at);
                                        $leaveStart=strtotime($leave->leave_starting_date);
                                        $leaveEnd=strtotime($leave->leave_ending_date);
                                        
                                        if(($leave->status)==0){
                                            $status='Pending';
                                        }
                                        elseif(($leave->status)==1){
                                            $status='Approved';
                                        }
                                        elseif(($leave->status)==2){
                                            $status='Rejected';
                                        }
                                        @endphp
                                        <tr>
                                            <td>{{date("d F", $leaveStart)}}  to  {{date("d F", $leaveEnd)}}</td>
                                            <td>{{$leave->leave_type}}</td>
                                            <td><h5>Not available</h5></td>
                                            <td>{{date("d F Y", $applyDate)}}</td>
                                            <td>{{$status}}</td>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @include('include.copyright')
@endsection