@extends('layouts.master')
@section('title', 'Leave Settings')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('f_leave'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('f_leave') }}</p>
        @endif
            @if(Session::has('message'))
                <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header">
                        <h3> <strong>Leave Settings</strong></h3>
                    </div>
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary" id="leav_sett_tab">
                            <li class="active"><a href="#tab2_1" data-toggle="tab"><i class="icon-user"></i>Leaves</a></li>
                            <li><a href="#tab2_2" data-toggle="tab"><i class="icon-settings"></i>Weekends & Festival</a></li>
                            <li><a href="#tab2_3" data-toggle="tab"><i class="icon-settings"></i>Department/Section wise holiday</a></li>
                        </ul>
                        {{--//leave request start--}}

                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab2_1">
                                <h3><i class="fa fa-table"></i> <strong>Leave List</strong></h3>

                                <div class="panel-content pagination2 table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Leave Type</th>
                                            <th>Total Days</th>
                                            <th>Policy</th>
                                            <th>Modified By</th>
                                            <th>Modified at</th>
                                            <th>Update/Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $leavetype)
                                            <tr>
                                                <td>{{$leavetype->leave_type}}</td>
                                                <td>{{$leavetype->total_days}}</td>
                                                <td>{{$leavetype->policy}}</td>
                                                <td>{{$leavetype->modified_by}}</td>
                                                <td>{{$leavetype->updated_at}}</td>
                                                <td>
                                                    <a data-toggle="modal" data-target="#{{$leavetype->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>
                                                    {{--<a href="{{ url('/leave_delete/'.$leavetype->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>--}}
                                                </td>
                                            </tr>

                                            {{--//table end--}}


                                            {{--//table edit modal start--}}
                                            <div class="modal fade" id="{{$leavetype->id}}" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Update Info</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {{Form::open(array('url' => 'leave_update','method' => 'post'))}}
                                                            <div class="form-group">
                                                                <div class="form-group">
                                                                    <label for="leavename">Leave Name</label>
                                                                    <input type="text" name="leave_type" class="form-control form-white" id="leavename" value="{{$leavetype->leave_type}}" placeholder="Enter Leave Name" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="totaldays">Total Days</label>
                                                                    <input type="number" name="total_days" step="any" class="form-control form-white" id="totaldays" value="{{$leavetype->total_days}}" placeholder="Enter Number of Days" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="policy">Policy</label>
                                                                    <input type="text" name="policy" step="any" class="form-control form-white" id="policy" value="{{$leavetype->policy}}" placeholder="Enter Policy" required>
                                                                </div>
                                                                <input type="hidden" name="user_name" value="{{auth::user()->name}}">
                                                                <input type="hidden" name="leave_id" value="{{$leavetype->id}}">
                                                                <button type="submit" class="btn btn-primary">Update</button>
                                                                {{ Form::close() }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                {{--//table edit modal end--}}

                                {{--//add new leave modal start--}}

                                <div class="panel-content">
                                    <div class="form-group">
                                        <a data-toggle="modal" data-target="#addLeave" class="btn btn-success btn-md" type="submit"><i class="fa fa-edit"></i>Add Another Leave</a>
                                        <div class="modal fade" id="addLeave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">

                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        {{Form::open(array('url' => 'leave_settings','method' => 'post'))}}
                                                        <div class="form-group">
                                                            <label for="leavename">Leave Name</label>
                                                            <input type="text" name="leave_type" class="form-control form-white" id="leavename"  placeholder="Enter Leave Name" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="totaldays">Total Days</label>
                                                            <input type="number" name="total_days" step="any" class="form-control form-white" id="totaldays"  placeholder="Enter Number of Days" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="policy">Policy</label>
                                                            <input type="text" name="policy" step="any" class="form-control form-white" id="policy"  placeholder="Enter Policy" required>
                                                        </div>
                                                        <input type="hidden" name="user_name" value="{{auth::user()->name}}">
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--//leave request end--}}
                            <div class="tab-pane fade" id="tab2_2">
                                <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label>Select Weekly leave days</label>
                                        <select onchange=weekLeave(this) multiple class="form-control"  placeholder="{{$placeholder}}">
                                            <option value="6">Friday</option>
                                            <option value="7">Saturday</option>
                                            <option value="1">Sunday</option>
                                            <option value="2">Monday</option>
                                            <option value="3">Tuesday</option>
                                            <option value="4">Wednesday</option>
                                            <option value="5">Thursday</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                    <div id='wkLeave'>
                                        <div class="form-group">
                                            @if(isset($week)) 
                                                @include('leave.weekLeave')
                                            @else 
                                            <h5><strong>Currently setted weekends are:</strong><p class="text-success">
                                                    @foreach($week2 as $data9)
                                                    <strong>{{$data9->day}}</strong>,
                                                    @endforeach
                                                    </p>
                                                </h5>
                                            @endif
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <h3><i class="fa fa-table"></i> <strong>Festival Leave List</strong></h3>

                                <div class="panel-content pagination2 table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Start Day</th>
                                            <th>End Day</th>
                                            <th>Purpose</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($festival_data as $festive_leave)
                                            
                                        @php
                                        $startDate=$festive_leave->start_date;
                                        $endDate=$festive_leave->end_date;

                                        $leaveStart=strtotime($startDate);
                                        $leaveEnd=strtotime($endDate);
                                        @endphp
                                    <tr>
                                        
                                        <td>{{date("d F", $leaveStart)}}</td>
                                        <td>{{date("d F", $leaveEnd)}}</td>
                                        <td>{{$festive_leave->purpose}}</td>
                                        <td>
                                            {{--<a data-toggle="modal" data-target="#{{$festive_leave->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>--}}
                                            <a href="{{ url('/festival_leave_delete/'.$festive_leave->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                            {{--//table edit modal start--}}
                                            <div class="modal fade" id="{{$festive_leave->id}}" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Update Festival Leave</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            {{Form::open(array('url' => 'festival_leave_update','method' => 'post'))}}
                                                            <div class="form-group">

                                                                <div class="form-group">
                                                                    <label> Vacation Start Date </label>
                                                                    <div class="">
                                                                        <input type="text" class="date-picker form-control" required name="startDate" value="{{$festive_leave->start_date}}"  placeholder="Click here for pick start date ..." />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label> Vacation End Date </label>
                                                                    <div class="">
                                                                        <input type="text" class="date-picker form-control" required name="endDate" value="{{$festive_leave->end_date}}" placeholder="Click here for pick end date ..." />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label> Vacation Purpose </label>
                                                                    <div class="">
                                                                        {{--<textarea name="purpose"  value="{{$festive_leave->purpose}}" class="form-control"></textarea>--}}
                                                                        <input type="text" name="purpose" step="any" class="form-control form-white" id="purpose" value="{{$festive_leave->purpose}}" placeholder="Enter Policy".Policy>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="user_name" value="{{auth::user()->name}}">
                                                                <input type="hidden" name="festival_leave_id" value="{{$festive_leave->id}}">
                                                                <button type="submit" class="btn btn-primary">Update</button>
                                                                {{ Form::close() }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div>
                                    <a data-toggle="modal" data-target="#addFestiveLeave" class="btn btn-success btn-md" type="submit"><i class="fa fa-edit"></i>Add Festive Leave</a>
                                    <div class="modal fade" id="addFestiveLeave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Add Festival</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    {{Form::open(array('url' => 'add_festival_leave','method' => 'post'))}}
                                                    <form action="save_vacation.php" method="POST">
                                                        
                                                        <div class="form-group">
                                                            <label> Vacation Start Date </label>
                                                            <div class="">
                                                                <input type="text" class="date-picker form-control" required name="startDate" autocomplete="off"  placeholder="Click here for pick start date ..." />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label> Vacation End Date </label>
                                                            <div class="">
                                                                <input type="text" class="date-picker form-control" required name="endDate" autocomplete="off" placeholder="Click here for pick end date ..." />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label> Vacation Purpose </label>
                                                            <div class="">
                                                                <input type="text" name="purpose" step="any" class="form-control form-white" id="purpose"  placeholder="Enter Purpose" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label></label>
                                                            <div class="">
                                                                <input type="submit" class="btn btn-primary"  value="Save" />
                                                            </div>
                                                        </div>
                                                    </form>
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>





                            <div class="tab-pane fade in" id="tab2_3">
                                <form action="{{route('emp_dept_wise_holiday')}}" method="post">
                                    @csrf 
                                     <div class="col-md-4">
                                         <div class="form-group">
                                             <label for="pwd">Select Department:</label>
                                             <select class="form-control" name="dept_id" autocomplete="off" data-search="true" required>
                                                 <option value="">Select Department</option>
                                                     @foreach($department as $departments)
                                                     <option value="{{$departments->id}}">{{$departments->departmentName}}</option>
                                                     @endforeach
                                             </select>
                                         </div>
                                     </div>
                                     <div class="col-md-4">
                                         <div class="form-group">
                                             <label for="pwd">Select Date:</label>
                                             <input type="text" class="date-picker form-control" name="holi_date" required autocomplete="off">
                                         </div>
                                     </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pwd">Perpose:</label>
                                            <input type="text" class="form-control" name="perpose" required autocomplete="off">
                                        </div>
                                    </div>
                                     <button style="margin-left:15px" type="submit" class="btn btn-success"><i class="fa fa-save"></i>Save Information</button>                   
                                 </form>
         
         
                                 <form action="{{route('emp_section_wise_holiday')}}" method="post">
                                     @csrf 
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label for="pwd">Select Section:</label>
                                              <select class="form-control" name="section_id" autocomplete="off" data-search="true" required>
                                                  <option value="">Select Section</option>
                                                      @foreach($section as $sections)
                                                      <option value="{{$sections->empSection}}">{{$sections->empSection}}</option>
                                                      @endforeach
                                              </select>
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label for="pwd">Select date:</label>
                                              <input type="text" class="date-picker form-control" name="holi_date" required autocomplete="off">
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pwd">Perpose:</label>
                                            <input type="text" class="form-control" name="perpose" required autocomplete="off">
                                        </div>
                                    </div>
                                      <button style="margin-left:15px" type="submit" class="btn btn-success"><i class="fa fa-save"></i>Save Information</button>                   
                                  </form>
                                  <h3><i class="fa fa-table"></i> <strong>Holiday List</strong></h3>
                                <div class="panel-content pagination2 table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Department</th>
                                            <th>Section</th>
                                            <th>Holiday</th>
                                            <th>Perpose</th>
                                            <th>Month</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($holiday_list as $h_item)
                                            <tr>
                                            <td>{{$h_item->dept}}</td>
                                                <td>{{$h_item->section}}</td>
                                                <td>{{$h_item->perpose}}</td>
                                                <td>{{date('d-M-Y',strtotime($h_item->holiday_date))}}</td>
                                                <td>{{date('F-Y',strtotime($h_item->holiday_month))}}</td>
                                            <td>
                                                @if($h_item->dept=='') @php $item=$h_item->section;  @endphp @else @php $item=$h_item->dept;  @endphp  @endif
                                                <a class="btn btn-danger btn-sm" href="{{url('employee/holiday/delete/'.$item.'/'.$h_item->holiday_date)}}"><i class="fa fa-trash"></i></a>
                                            </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{--//table edit modal end--}}
                                {{--//add new leave modal start--}}
                                <div class="panel-content">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                                <script>

                                    // $(window).load(function(){
                                    //     $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                                    //         localStorage.setItem('activeTab', $(e.target).attr('href'));
                                    //     });
                                    //     var activeTab = localStorage.getItem('activeTab');
                                    //     if(activeTab){
                                    //         $('#leav_sett_tab a[href="' + activeTab + '"]').tab('show');
                                    //     }
                                    //
                                    // });

                                    function weekLeave(that){ 
                                        const weekLeave= $(that).val();
                                        //alert(weekLeave);

                                        $.ajax({
                                            url:"leave/addWeekends/"+weekLeave, 
                                            method:'GET',
                                            dataType:'html',
                                            success: function (data) {
                                            $('#wkLeave').html(data);
                                            //alert("Weekly off Days Succesfully updated")

                                            },
                                            error:function (xhr) {
                                                console.log('failed');    
                                            }
                                        });
                                    }

                                </script>
    @include('include.copyright')
@endsection