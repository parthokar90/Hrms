@extends('layouts.master')
@section('title', 'Earn Leave Benefit')
@section('content')
    <div class="page-content">
        <div class="row">
                <div class="col-lg-12 portlets">
                    <div class="panel">
                        <div class="panel-header">
                            <h3><i class="fa fa-table"></i> <strong>Earn Leave Benefit</strong></h3>
                        </div>
                        
                        <div class="panel-content pagination2 table-dyanamic">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Employee ID</th>
                                    <th>Name</th>
                                    <th>Designation</th>
                                    <th>Joining Date</th>
                                    <th>Total Earn Leave</th>
                                    <th>Enjoyed</th>
                                    <th>Available</th>
                                    <th>Previous Month Salary</th>
                                    <th>Per Day Salary</th>
                                    <th>Payable Earn Leave Benefit</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php $order=0 @endphp
                                    @foreach($employee_benefit as $leave)
                                        @php
                                        $previousMonthDay=date("t", mktime(0,0,0, date("n") - 1));
                                        $payPerDay=($leave->net_amount)/$previousMonthDay;
                                        $totalBenefit=($leave->leave_available)*$payPerDay;
                                        $order++
                                        @endphp
                                        <tr>
                                            <td>{{$order}}</td>
                                            <td>{{$leave->employeeId}}</td>
                                            <td>{{$leave->empFirstName}} {{$leave->empLastName}}</td>
                                            <td>{{$leave->designation}}</td>
                                            <td>{{date("d M Y", strtotime($leave->empJoiningDate))}}</td>
                                            <td>{{$leave->total_days}}</td>
                                            <td>{{$leave->enjoyed}}</td>
                                            <td>{{$leave->leave_available}}</td>
                                            <td>{{number_format(($leave->net_amount))}} BDT</td>
                                            <td>{{number_format(($payPerDay),2)}} BDT</td>
                                            <td>{{number_format(($totalBenefit),2)}} BDT</td>
                                        </tr>
                                           
                                    @endforeach
                           
                                    @foreach($employee_without_leave_benefit as $leave)
                                        @php
                                        $previousMonthDay=date("t", mktime(0,0,0, date("n") - 1));
                                        $payPerDay=($leave->net_amount)/$previousMonthDay;
                                       
                                        $order++
                                        @endphp
                                        <tr>
                                            <td>{{$order}}</td>
                                            <td>{{$leave->employeeId}}</td>
                                            <td>{{$leave->empFirstName}} {{$leave->empLastName}}</td>
                                            <td>{{$leave->designation}}</td>
                                            <td>{{date("d M Y", strtotime($leave->empJoiningDate))}}</td>
                                            @foreach($earnLeave as $earn)
                                            @php 
                                            $totalBenefit=($earn->total_days)*$payPerDay;
                                            @endphp
                                            <td>{{$earn->total_days}}</td>
                                            <td>0</td>
                                            <td>{{$earn->total_days}}</td>
                                            <td>{{number_format(($leave->net_amount))}} BDT</td>
                                            <td>{{number_format(($payPerDay),2)}} BDT</td>
                                            <td>{{number_format(($totalBenefit),2)}} BDT</td>
                                        </tr>
                                        @endforeach
                                           
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @include('include.copyright')
@endsection