 
  
       @extends('layouts.master')
       @section('title', 'Leave Request Management')
       @section('content')
       <style>
       .pagination li a {
            font-size: 16px;
            background: blue;
            color: #ffffff;
        }
       </style>
           <div class="page-content">
               @if(Session::has('message'))
                   <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
               @endif
               <div class="row">
                   <div class="col-md-12 portlets">
                       <div class="panel">
                           <div class="panel-header">
                               <h3> <strong>Leave Request Management</strong></h3>
                           </div>

                            <div class="col-md-12">
                            <form method="GET" action="{{url('leave/employee/search')}}">
                                {{@csrf_field()}}
                                <div class="form-group">
                                    <label>Enter Employee Id Or Name</label>
                                    <input type="text" class="form-control" name="keyword" required>
                                </div>    
                                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Search </button>
                                </form>
                            </div>

                           <div class="panel-content">
                               <ul class="nav nav-tabs nav-primary" id="leave-nav-tab">
                                   <li class="active"><a href="#tab2_1" data-toggle="tab"><i class="fa fa-pause"></i>Pending Request</a> </li>
                                   <li><a href="#tab2_2" data-toggle="tab"><i class="fa fa-check"></i>Approved</a></li>
                                   <li><a href="#tab2_3" data-toggle="tab"><i class="fa fa-times"></i>Rejected</a></li>
                               </ul>
       
                               <div class="tab-content">
                                   <div class="tab-pane fade active in" id="tab2_1">
                                       @if(count($data)!=0)
                                       <h3><i id="pendingTable1" class="fa fa-table"></i> <strong>Pending List</strong></h3>
       
                                       <div class="table-responsive">
                                           <table class="table table-bordered">
                                               <thead>
                                               <tr>
                                                   <th>Previous Leave</th>
                                                   <th>ID</th>
                                                   <th>Name</th>
                                                   <th>Designation</th>
                                                   <th>Leave type</th>
                                                   <th>Leave reason</th>
                                                   <th>Attachment</th>
                                                   <th>Period</th>
                                                   <th>Total Days</th>
                                                   <th>Available</th>
                                                   <th>Action</th>
                                               </tr>
                                               </thead>
                                               <tbody>
                                               @foreach($data as $leavereq)
                                           
                                                   @php
                                                           $taken=0;
                                                           $start_date=\Carbon\Carbon::parse($leavereq->empJoiningDate);
                                                           $now = \Carbon\Carbon::now();
                                                           $diff = $start_date->diffInDays($now);
                                                           $previous_earn_leave=\Illuminate\Support\Facades\DB::table('manual_earn_leave')->where('emp_id','=',$leavereq->eID)->sum('earn_leave_days');
                                                           $paid_leave=\Illuminate\Support\Facades\DB::table('earn_leave_payment')->where('emp_id','=',$leavereq->eID)->sum('leave_to_cash');
       
                                                           if($diff<364){
                                                               $days=0;
                                                           }
                                                           else{
                                                               $days=\Illuminate\Support\Facades\DB::select("select count(*) as working_days from `attendance` where `emp_id` = $leavereq->eID");
                                                               $days= $days[0]->working_days;
                                                           }
       
                                                           if($leavereq->leave_type=="Earn Leave"){
                                                               //$available=\App\Http\Controllers\leaveController::totalLeave($leavereq->leave_type_id);
                                                               $taken=\Illuminate\Support\Facades\DB::table('tb_employee_leave')->where('employee_id','=',$leavereq->eID)->where('leave_type_id','=',4)->sum('leave_taken');
                                                               $available=floor($days/18)+$previous_earn_leave-$paid_leave;
                                                           }
                                                           elseif($leavereq->leave_available){
                                                               $available=\App\Http\Controllers\leaveController::totalLeave($leavereq->leave_type_id);
                                                               $taken=$available-$leavereq->leave_available;
       
                                                           }
                                                           else{
                                                               $available=\App\Http\Controllers\leaveController::totalLeave($leavereq->leave_type_id);
                                                           }
                                                           if(isset($leavereq->empJoiningDate)){
                                                               if(\Carbon\Carbon::parse($leavereq->empJoiningDate)->format('Y')==\Carbon\Carbon::now()->format('Y'))
                                                               {
                                                                   $mr=13-\Carbon\Carbon::parse($leavereq->empJoiningDate)->format('m');
                                                                   $available=floor(($available/12)*$mr);
                                                                   $available-=$taken;
                                                               }
                                                           }
       
                                                           $date1=$leavereq->leave_starting_date;
                                                           $date2=$leavereq->leave_ending_date;
                                                           $interval1=1+round(abs(strtotime($date1)-strtotime($date2))/86400);
       
       
                                                           $leaveStart=strtotime($date1);
                                                               $leaveEnd=strtotime($date2);
       
                                                           if(($leavereq->status)==0){
                                                               $status='Pending';
                                                           }
                                                           elseif(($leavereq->status)==1){
                                                               $status='Approved';
                                                           }
                                                   @endphp
                                               <tr>
                                                   {!! Form::hidden('id',$leavereq->id) !!}
                                                  
                                                   <td><button value="{{$leavereq->eID}}" onclick="dosomething(this)" id="empID" type="button" class="btn btn-success btn-sm fa fa-eye"></button></td>   
                                                   <td>{{$leavereq->employeeId}}    
                                                   </td>
                                                   <td>{{$leavereq->empFirstName}} {{$leavereq->empLastName}}</td>
                                                   <td>{{$leavereq->designation}}</td>
                                                   <td>{{$leavereq->leave_type}}</td>
                                                   <td>
                                                       @if($leavereq->description)
                                                       {{$leavereq->description}}
                                                       @else    
                                                           <p>N/A</p>
                                                       @endif
                                                   </td>
                                                   <td>
                                                       @if($leavereq->attachment)
                                                           <a target="_blank" href="{{asset('Leave_request_attachment/'.$leavereq->attachment)}}"><p><button type="button"class="btn btn-custom-download btn-sm"><i class="fa fa-download"></i> Download</button></p></a>
                                                       @else    
                                                           <h4>N/A</h4>
                                                       @endif                                            </td>
                                                   <td>{{date("d F", $leaveStart)}} -to- {{date("d F Y", $leaveEnd)}}</td>
                                                   <td>{{$interval1}}</td>
                                                   <td>{{$available-$taken}} </td>
                                                   <td style="display: flex;">
                                                       <a onclick="return confirm('Are you sure to Accept?')" href= "{{url('leave_accepted/'.$leavereq->id)}}"><button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Accept</button></a>
                                                       <a onclick="return confirm('Are you sure to Reject?')" href= "{{url('leave_declined2/'.$leavereq->id)}}"><button type="button" class="btn btn-warning btn-sm" ><i class="fa fa-ban"></i> Reject</button>
                                                       <a href="{{route('leave_request.delete',base64_encode($leavereq->id))}}" onclick="return confirm('Are you sure to delete?')" title="Delete" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                                   </td>
                                               </tr>
       
                                               <div class="modal fade" id="{{$leavereq->employeeId}}" role="dialog">
                                                   <div class="modal-dialog">
                                                       <!-- Modal content-->
                                                       <div class="modal-content">
                                                           <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                               <h4 class="modal-title">Employee Leave History</h4>
                                                           </div>
                                                           <div class="modal-body">
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
       
                                               @endforeach
                                               {{$data->links()}}
                                               </tbody>
                                           </table>
                                           @include('leave.employee_leave_history_modal')
                                           <input type="hidden" name="user_name" value="{{auth()->user()->name}}">
                                           <input type="hidden" name="user_id" value="{{auth()->user()->id}}"> 
                                                                         
                                       </div>
                                       @else
                                           <h3>No pending Leave Request</h3>
                                       @endif  
                                   </div>
                                  
                                   <div class="tab-pane fade" id="tab2_2">
                                       @if(count($data2)!=0)
                                           <h3><i class="fa fa-table"></i> <strong>Approved List</strong></h3>
                                          <form method="post" action="{{url('cancel/all/leave/request')}}">
                                           {{@csrf_field()}}
                                           <button onclick="return confirm('Are you sure??')"  type="submit" class="btn btn-danger"><i class="fa fa-ban"></i> Cancel All </button>
                                          </form>
                                           <div class="table-responsive">
                                               <table class="table table-bordered">
                                                   <thead>
                                                   <tr>
                                                       <th>ID</th>
                                                       <th>Name</th>
                                                       <th>Designation</th>
                                                       <th>Leave type</th>
                                                       <th>Period</th>
                                                       <th>Work Days</th>
                                                       <th>Approved By</th>
                                                       <th>Action</th>
                                                   </tr>
                                                   </thead>
                                                   <tbody>
       
                                                   @foreach($data2 as $leavereq)
       
                                                       @php
                                                       $date1=$leavereq->leave_starting_date;
                                                       $date2=$leavereq->leave_ending_date;
                                                       $interval=1+round(abs(strtotime($date1)-strtotime($date2))/86400);
       
                                                       $leaveStart=strtotime($date1);
                                                       $leaveEnd=strtotime($date2);
                                                       
                                                       if(($leavereq->status)==0){
                                                           $status='Pending';
                                                       }
                                                       elseif(($leavereq->status)==1){
                                                           $status='Approved';
                                                       }
                                                       @endphp
                                                   <tr>
                                                       {!! Form::hidden('id',$leavereq->id) !!}
                                                       <td>{{$leavereq->employeeId}}</td>
                                                       <td>{{$leavereq->empFirstName}} {{$leavereq->empLastName}}</td>
                                                       <td>{{$leavereq->designation}}</td>
                                                       <td>{{$leavereq->leave_type}}</td>
                                                       <td>{{date("d F", $leaveStart)}} -to- {{date("d F", $leaveEnd)}}</td>
                                                       <td>{{$interval}}</td>
                                                       <td>{{$leavereq->approved_by}}</td>
                                                       <td>
                                                           <a onclick="return confirm('Are you sure to cancel?')" href= "{{url('leave_declined/'.$leavereq->id)}}"><button type="button" class="btn btn-danger btn-sm" ><i class="fa fa-ban"></i> Cancel</button>
                                                       </td>
                                                       </tr>
                                                   @endforeach
                                                   {{$data2->links()}}
                                                   </tbody>
                                               </table>
                                               <input type="hidden" name="user_name" value="{{auth()->user()->name}}">
                                               <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                                               
                                           </div>
                                    
                                           @else
                                           <h3>No Accepted Request</h3>
                                            @endif
                                       </div>
       
                                   <div class="tab-pane fade" id="tab2_3">
                                      
                                       @if(count($data3)!=0)
                                           <h3><i class="fa fa-table"></i> <strong>Declined List</strong></h3>
                                           <form method="post" action="{{url('approve/all/leave/request')}}">
                                               {{@csrf_field()}}
                                               <button onclick="return confirm('Are you sure??')"  type="submit" class="btn btn-success"><i class="fa fa-check"></i>Approved All </button>
                                           </form>
       
                                           {{--<div class="panel-content pagination2 table-responsive">--}}
                                               {{--<table class="table table-hover table-dynamic">--}}
           
                                           <div class="table-responsive">
                                               <table class="table table-bordered">
                                                   <thead>
                                                   <tr>
                                                       <th>ID</th>
                                                       <th>Name</th>
                                                       <th>Designation</th>
                                                       <th>Leave type</th>
                                                       <th>Period</th>
                                                       <th>Total Days</th>
                                                       <th>Declined By</th>
                                                       <th>Action</th>
                                                   </tr>
                                                   </thead>
                                                   <tbody>
                                                   @foreach($data3 as $leavereq)
           
                                                       @php
                                                       $date1=$leavereq->leave_starting_date;
                                                       $date2=$leavereq->leave_ending_date;
                                                       $interval=1+round(abs(strtotime($date1)-strtotime($date2))/86400);
       
                                                       $leaveStart=strtotime($date1);
                                                       $leaveEnd=strtotime($date2);
                                                       
                                                       if(($leavereq->status)==0){
                                                           $status='Pending';
                                                       }
                                                       elseif(($leavereq->status)==1){
                                                           $status='Approved';
                                                       }
                                                       @endphp
                                                   <tr>
                                                       {!! Form::hidden('id',$leavereq->id) !!}
                                                      
                                                       <td>{{$leavereq->employeeId}}</td>
                                                       <td>{{$leavereq->empFirstName}} {{$leavereq->empLastName}}</td>
                                                       <td>{{$leavereq->designation}}</td>
                                                       <td>{{$leavereq->leave_type}}</td>
                                                       <td>{{date("d F", $leaveStart)}} -to- {{date("d F", $leaveEnd)}}</td>
                                                       <td>{{$interval}}</td>
                                                       <td>{{$leavereq->approved_by}}</td>
                                                       <td>
                                                           <a onclick="return confirm('Are you sure to Approve?')" href= "{{url('leave_accepted/'.$leavereq->id)}}"><button type="submit" class="btn btn-success btn-sm"> <i class="fa fa-check"></i> Approve</button></a>
                                                           
                                                       </td>
                                                       </tr>
                                                   @endforeach
                                                   {{$data3->links()}}
                                                   </tbody>
                                               </table>
                                               @else
                                               <h3>No Declined Request</h3>
                                           @endif
                                               <input type="hidden" name="user_name" value="{{auth()->user()->name}}">
                                               <input type="hidden" name="user_id" value="{{auth()->user()->id}}"> 
                                           </div>
                                       </div>
       
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
       
           </div>
           @include('include.copyright')
       
           <script>
                   function dosomething(that){
                       const data= $(that).val();
                       //alert(data);
           
                       $.ajax({
                           url:"leave/employee/leave_history/"+data, 
                           method:'GET',
                           dataType:'json',
                          
                           success: function(data){
                               $("#modalshow1").modal('show');
                               //var line = "<h4>"+ val.empFirstName+' '+val.empLastName+"</h4>";
                               var row = "<tr>";
                               
                                   row += " <td>" + "LEAVE PERIOD" + "</td>";
                              
                                   row += " <td>" + "LEAVE TYPE" + "</td>";
                                   row += " <td>" + "REASON" + "</td>";
                                   row += "</tr>"
                                   $.each(data, function(i, val) {
                                       row += " <td>" + val.leave_starting_date+' to '+val.leave_ending_date + "</td>";
                                       row += "<td>" + val.leave_type+ "</td>";
                                       row += "<td>" + val.description + "</td>";
                                       row += "</tr>";
                                      
                                   } );
           
                               $("#tableID").html(row);
                                   
                                                          
           
                           }
                           {{-- error:function (something) {
                               console.log('failed');    
                           }
                            --}}
                       });
                     }
                     setTimeout(function() {
                   $('#alert_message').fadeOut('fast');
               }, 5000);

               $(document).ready(function(){
           $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
               localStorage.setItem('activeTab', $(e.target).attr('href'));
           });
           var activeTab = localStorage.getItem('activeTab');
           if(activeTab){
               $('#leave-nav-tab a[href="' + activeTab + '"]').tab('show');
           }
       });
           </script>
       @endsection
               