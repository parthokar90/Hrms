@php
    use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Manual Attendance')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Earn Leave Details</b></h2>
                    <hr>
                </div>
            </div>



            {{--{!! Form::open(['method'=>'POST','action'=>'AttendanceController@manual_store_data']) !!}--}}
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">
                @if(Session::has('success'))
                    <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
                @elseif(Session::has('error'))
                    <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
                @endif


                <div class="form-group">

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label for="emp_id" class="col-md-3">Select Single Employee<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select name="emp_id" id="emp_id" required class="form-control" data-search="true">
                                <option value="">Select Employee</option>
                                @foreach($employees as $emp)
                                    <option value="{{$emp->id}}">{{$emp->empFirstName." (".$emp->employeeId.")"}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <hr>
                            <a href="{{route('leave.earnLeave_data')}}" id="process-btn" class="btn btn-success"><i class="fa fa-list"></i> &nbsp;Process</a>
                            {{--<button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary"><i class="fa fa-download"></i> &nbsp;Download as PDF</button>--}}
                            <hr>
                        </div>
                    </div>

                </div>

            </div>

            <div class="earn-details">


            </div>

            {{--{!! Form::close() !!}--}}
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#process-btn').click(function (event) {
                var emp_id=$('#emp_id').val();
//                alert(emp_id);
                if(emp_id.length===0){
                    alert('Select a Employee First');
                    event.preventDefault();
                }
                else {
                    event.preventDefault();
                    var url=$('#process-btn').attr('href')+"?id="+emp_id;
                    $.ajax({

                        url:url,
                        method:'get',
                        success:function (response) {
                            $('.earn-details').html(response);


                        }
                    });

                }

            });

            setTimeout(function() {
                $('#alert_message').fadeOut('fast');
            }, 5000);

        });

    </script>

    @include('include.copyright')
@endsection