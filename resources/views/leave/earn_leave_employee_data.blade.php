<div class="panel-content pagination2 table-dyanamic">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Employee ID</th>
            <th>Name</th>
            <th>Joining Date</th>
            <th>Total Earn Leave</th>
            <th>Enjoyed</th>
            <th>Leave Paid</th>
            <th>Available</th>
            <th>Previous Month Salary</th>
            <th>Per Day Salary</th>
            <th>Payable Earn Leave Benefit</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            @php
            if(!isset($earn_details)){
                $earn=0;
                $working_day=1;
            }
            else{
                $earn=$earn_details->net_amount;
                $working_day=$earn_details->working_day;
            }
                $earn_leave=floor($days/18);
                $available=$earn_leave-$enjoyed-$paid_leave+$previous_earn_leave;
                $sal_per_day=round(($earn/$working_day),2);

            @endphp
            <td>{{$employee->employeeId}}</td>
            <td>{{$employee->empFirstName." ".$employee->empLastName}}</td>
            <td>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('j M Y')}}</td>
            <td>{{$earn_leave}}</td>
            <td>{{$enjoyed}}</td>
            <td>{{$paid_leave}}</td>
            <td>{{$available}}</td>
            <td>{{$earn}}</td>
            <td>{{$sal_per_day}}</td>
            <td>{{(round(($available*$sal_per_day),2))}}</td>
            <td><a href="{{route('leave.earnLeave_payment',[base64_encode($employee->id),base64_encode($available),base64_encode($sal_per_day)])}}" id="payment_modal" title="leave Payment" class="btn btn-blue btn-sm"><i class="fa fa-edit"></i>Payment</a></td>
        </tr>
        </tbody>
    </table>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Earn Leave Payment</h4>
            </div>
            <div class="modal-body">

            </div>
        </div>

    </div>
</div>

<script>
    $("#payment_modal").click(function (event) {
        event.preventDefault();
        var url=$(this).attr('href');
        $.ajax({
            url:url,
            method:"get",
            success:function (response) {
                $('.modal-body').html(response);

            }
        });
        $('#myModal').modal();

    });

</script>