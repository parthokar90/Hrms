@extends('layouts.master')
@section('title', 'Company Information')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('companymessage'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('companymessage') }}</p>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Update Company Information</h3></div>
            <div class="panel-body">
                {{Form::open(array('url' => '/company/information/update','method' => 'post'))}}
                @foreach($data as $company)
                <div class="col-md-12">
                  <div class="form-group">
                      <label>Company Name (English)<span style="color: red">*</span> </label>
                      <input type="text" class="form-control" name="company_name" placeholder="Company Name" value="{{$company->company_name}}" required>
                  </div>
                </div>
                 <div class="col-md-12">
                  <div class="form-group">
                      <label>Company Name (Bangla)<span style="color: red">*</span> </label>
                      <input type="text" class="form-control" name="bComName" placeholder="Company Name (Bangla)" value="{{$company->bComName}}" >
                  </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Company Phone (English)<span style="color: red">*</span> </label>
                        <input type="text" class="form-control" name="company_phone" placeholder="Company Phone (English)" value="{{$company->company_phone}}" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Company Phone (Bangla)<span style="color: red">*</span> </label>
                        <input type="text" class="form-control" name="bComPhone" placeholder="Company Phone (Bangla)" value="{{$company->bComPhone}}" >
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Company Email <span style="color: red">*</span> </label>
                        <input type="text" class="form-control" name="company_email" placeholder="Company Email" value="{{$company->company_email}}" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Company Address 1</label>
                        <textarea class="form-control" name="company_address_one">{{$company->company_address1}}</textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Company Address 2</label>
                        <textarea class="form-control" name="company_address_two">{{$company->company_address2}}</textarea>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Company Address (Bangla)</label>
                        <textarea class="form-control" name="bComAddress" placeholder="Company Address (Bangla)">{{$company->bComAddress}}</textarea>
                    </div>
                </div>
                <input type="hidden" name="company_hidden_id" value="{{$company->id}}">
                @endforeach
                <div class="col-md-6">
                    @if(checkPermission(['admin']))
                    <button type="submit" name="company_info" class="btn btn-success">Update Information</button>
                    @endif
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection