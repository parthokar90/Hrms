{!! Form::open(['method'=>'DELETE','action'=>['LineController@destroy',$line->id]]) !!}

<div class="container">
    <p>You sure you want to delete <strong>{{ $line->line_no }}</strong> ??</p>
</div>


<div class="text-center  m-t-20">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" id="update-unit" class="btn btn-embossed btn-primary">Confirm Delete</button>
</div>

{!! Form::close() !!}