@extends('layouts.master')
@section('title', 'Line List')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-4">
                                <h3><i class="fa fa-table"></i> <strong>Line </strong> List</h3>
                            </div>
                            <div class="col-md-8" style="margin-top:8px; text-align: right;">
                               <a data-toggle="modal" data-target="#linecreate" class="btn btn-blue btn-sm"><i class="fa fa-plus"></i> Add New</a>
                            </div>
                          </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th width="5%">id</th>
                                <th>Line Number</th>
                                <th>Floor</th>
                                <th>Description</th>
                                <th width="20%" style="text-align: center;">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($lines as $line)
                                <tr>
                                    <td>{{$line->id}}</td>
                                    <td>{{$line->line_no}}</td>
                                    <td>{{$line->floor}}</td>
                                    <td>{{$line->lineDescription}}</td>
                                    <td style="text-align: center;">
                                        <a href="{{route('line.show',$line->id)}}" title="View" class="btn btn-primary btn-sm view-line"><i class="fa fa-eye"></i></a>

                                        @if(checkPermission(['admin']))
                                        <a href="{{route('line.edit',$line->id)}}" title="Edit" class="btn btn-default btn-sm edit-line"><i class="fa fa-edit"></i></a>
                                        <a href="{{route('line.delete.show',$line->id)}}" title="Delete Data" class="btn btn-danger btn-sm delete-line"><i class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Add New Modal -->
    <div class="modal fade" id="linecreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>Add New Line</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>

                </div>
                <div id="create-new" class="modal-body">
                    {!! Form::open(['method'=>'POST', 'action'=>'LineController@store']) !!}
                    <div class="form-group">
                        {!! Form::label('line_no','Line Number:') !!}
                        {!! Form::text('line_no',null,['class'=>'form-control','required'=>'', 'placeholder'=>'Enter Line Number']) !!}

                    </div>
                    <div class="form-group">
                        {!! Form::label('floor_id','Floor:') !!}
                        {!! Form::select('floor_id',[''=>'Select Floor']+$floors,null,['class'=>'form-control','required'=>'', 'data-search'=>'true']) !!}

                    </div>

                    <div class="form-group">
                        {!! Form::label('lineDescription','Description:') !!}
                        {!! Form::textarea('lineDescription',null,['class'=>'form-control', 'placeholder'=>'Enter Description']) !!}

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="Submit" id="addUnit" class="btn btn-primary">Save Information</button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-line-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                    <h4 class="modal-title"><strong>Edit Line</strong> Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="edit-form col-md-12 col-sm-12 col-xs-12">


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="delete-line-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                    <h4 class="modal-title"><strong>Confirm Delete</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="delete-form col-md-12 col-sm-12 col-xs-12">


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="view-line-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                    <h4 class="modal-title"><strong>Line Details</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="view-form col-md-12 col-sm-12 col-xs-12">


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <script>

        $('.document').ready(function () {
            $(document.body).on('click','.view-line',function (event) {
                event.preventDefault();
                var url=$(this).attr('href'),
                    method='GET';

                $.ajax({
                    url:url,
                    method:method,
                    dataType:'html',
                    success:function (response) {
                        $('.view-form').html(response);


                    },
                    error:function (xhr) {

                    }
                })
                $('#view-line-modal').modal('show');

            });
            $(document.body).on('click','.edit-line',function (event) {
//                alert('k');
                event.preventDefault();
                var url=$(this).attr('href');
//                alert(url);
                $.ajax({
                    url:url,
                    method:'GET',
                    dataType:'html',
                    success:function (response) {
                        $('.edit-form').html(response);

                    },
                    error:function (xhr) {
                        console.log(xhr);

                    }
                });


                $('#edit-line-modal').modal('show');

            });

            $(document.body).on('click','.delete-line',function (event) {
                event.preventDefault();
                var url=$(this).attr('href');
//                alert(url);
                $.ajax({
                    url:url,
                    method:'get',
                    dataType:'html',
                    success:function (response) {
                        $('.delete-form').html(response);

                    },
                    error:function (xhr) {

                    }
                });
                $('#delete-line-modal').modal('show');


            });

        });


        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);


    </script>


    @include('include.copyright')
@endsection