<div class="row">
    <div class="col-sm-12">
        <div class="required form-group">
            {!! Form::label('name','Name',['class'=>'control-label']) !!}
            <div class="append-icon">
                <input type="text" name="name" value="{{$line->line_no}}" class="form-control" placeholder="Enter line number..." required disabled>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="required form-group">
            <div class="form-group">
                {!! Form::label('floor_id','Floor:') !!}
                {!! Form::text('floor_id',$line->floor,['class'=>'form-control','required'=>'', 'disabled'=>'','data-search'=>'true']) !!}

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="control-label">Description</label>
            <div class="append-icon">
                <textarea name="address" class="form-control" placeholder="Enter description..." rows="4" required disabled> {{$line->lineDescription}}</textarea>
                <i class="icon-lock"></i>
            </div>
        </div>
    </div>

</div>


<div class="text-center  m-t-20">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

