@extends('layouts.master')
@section('title', 'Access Logs')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Access log </strong> history</h3>
                    </div>



                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th width="8%">SN</th>
                                <th>User Name</th>
                                <th>User IP</th>
                                <th>Login Status</th>
                                <th>Time</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; ?>
                            @foreach($access_logs as $user)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->ip_address}}</td>
                                    <td>
                                        @if($user->status==1)
                                            <span style="color: green;">Successful</span>
                                        @else
                                            <span style="color: red;">Failed</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{\Carbon\Carbon::parse($user->created_at)->format('j M Y h:i:s')}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection