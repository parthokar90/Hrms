@extends('layouts.master')
@section('title', 'Regular Day (Weekend)')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('failedMessage'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
        @endif

        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-table"></i> <strong>Regular Day (Weekend) </strong></h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                                <a data-toggle="modal" data-target="#usercreate" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>Add New</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>Serial</th>
                                <th>Date</th>
                                <th>Day</th>
                                <th>Month</th>
                                <th>Created By</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php $i=0; @endphp
                            @foreach($weekends as $weekend)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{date('d-m-Y', strtotime($weekend->weekend_date))}}</td>
                                    <td>{{date('l', strtotime($weekend->weekend_date))}}</td>
                                    <td>{{date('F-Y', strtotime($weekend->weekend_month))}}</td>
                                    <td>{{$weekend->created_by}}</td>
                                    <td>
                                        <a title="destroy" onclick="return confirm('Are you sure to destroy?')" href="{{route('weekend.destroy', base64_encode($weekend->id))}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Add New Modal -->
    <div class="modal fade" id="usercreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>New Regular Day (Weekend)  </strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'route'=>'weekend.store']) !!}
                        <div class="form-group">
                          <label for="date"> Date</label>
                          <input type="text" name="weekend_date" autocomplete="off" id="datepicker1" class="form-control" required="" placeholder="Weekend Date" >
                        </div>
                        <div class="modal-footer">
                            <hr>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="Submit" class="btn btn-primary">Add New</button>
                        </div>
                        {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

          $( function() {
            $( "#datepicker1" ).datepicker({

            });
          } );
    </script>


    @include('include.copyright')
@endsection