@extends('layouts.master')
@section('title', 'Overtime Hour Count Settings Buyer Mood')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-table"></i> <strong>Overtime Hour Count Settings </strong> Buyer Mood</h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                             
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Hours</th>
                                <th>Minutes</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($overtime_hour_count as $o)
                                <tr>
                                    <td>{{$o->id}}</td>
                                    <td>{{$o->overtime_hours}}</td>
                                    <td>{{$o->overtime_minutes}}</td>
                                    <td>
                                        <a data-toggle="modal" title="Edit" data-target="#{{$o->id}}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>

                                <!-- Edit Modal -->
                                <div class="modal fade" id="{{$o->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Update Overtime Settings</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['method'=>'PATCH', 'action'=>['OvertimeHourCountController@update', $o->id]]) !!}
                                                <div class="required form-group">
                                                    <label class="control-label">Hours</label>
                                                    <div class="append-icon">
                                                        <input type="number" name="overtime_hours" class="form-control" value="{{$o->overtime_hours}}" min="0" max="12" required>
                                                    </div>
                                                </div>
                                                <div class="required form-group">
                                                    <label class="control-label">Minutes</label>
                                                    <input type="number" name="overtime_minutes" class="form-control" value="{{$o->overtime_minutes}}" min="0" max="59" required>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Update Changes</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>


    @include('include.copyright')
@endsection