@extends('layouts.master')
@section('title', 'Designations')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))

            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>

        @endif
        @if(Session::has('edit'))

            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>

        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-table"></i> <strong>Designation </strong> List</h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                                <a data-toggle="modal" data-target="#usercreate" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>Add New</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Designation Name(English)</th>
                                <th>Designation Name(Bangla)</th>
                                <th>Description</th>
                                <th>Grade</th>
                                <th>Employees</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($designations as $designation)
                                <tr>
                                    <td>{{$designation->id}}</td>
                                    <td>{{$designation->designation}}</td>
                                    <td>{{$designation->designationBangla}}</td>
                                    <td>{{$designation->description}}</td>
                                    <td>{{$designation->gradeName}}</td>
                                    <td>{{\Illuminate\Support\Facades\DB::table('employees')->where('empDesignationId','=',$designation->id)->count()}}</td>
                                    <td>
                                        @if(checkPermission(['admin']))

                                        <a data-toggle="modal" title="Edit" data-target="#{{$designation->id}}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>
                                        <a data-toggle="modal" title="Delete Data" data-target="#{{"delete".$designation->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>

                                <!-- Edit Modal -->
                                <div class="modal fade" id="{{$designation->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Update Info</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['method'=>'PATCH', 'action'=>['DesignationController@update', $designation->id]]) !!}
                                                <div class="form-group">
                                                    {!! Form::label('name','Designation Name (English)') !!}
                                                    <input type="text" class="form-control" required="" id="name" aria-describedby="emailHelp" name="name" value="{{$designation->designation}}">
                                                </div> 
                                                <div class="form-group">
                                                    {!! Form::label('designationBangla','Designation Name (Bangla)') !!}
                  
                                                    <input type="text" class="form-control" id="designationBangla" aria-describedby="emailHelp" name="designationBangla" value="{{$designation->designationBangla}}">
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('description', 'Description') !!}
                                                    <textarea name='description' class="form-control" rows="5" id="description">{{$designation->description}}</textarea>

                                                </div>

                                                <div class="form-group">
                                                    <label for="gradeId">Grade</label>
                                                    <select name='gradeId' class="form-control" id="gradeId" required >
                                                        <option value=""> Select Grade</option>
                                                        <?php 
                                                        if(($payroll_grade)){  
                                                        foreach($payroll_grade as $pg){ ?>
                                                            <option <?php if(($designation->gradeId)==($pg->id)){ echo "selected"; } ?> value="{{$pg->id}}"> {{$pg->grade_name}}</option>
                                                        <?php }} ?>
                                                    </select>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Update Changes</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                {{--@endif--}}

                                <!--End of Edit Model-->





                                <!-- Delete Modal -->
                                <div class="modal fade" id="{{"delete".$designation->id}}" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Confirm Delete</strong></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Do you want to delete <strong>{{$designation->designation}}?</strong></p>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::open(['method'=>'DELETE', 'action'=>['DesignationController@destroy', $designation->id]]) !!}
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-danger">Confirm</button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--Delete Modal Ended-->

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Add New Modal -->
    <div class="modal fade" id="usercreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>New Designation</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                    </h5>

                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'action'=>'DesignationController@store']) !!}
                    <div class="form-group">
                        {!! Form::label('name','Designation Name (English):') !!}
                        {!! Form::text('name',null,['class'=>'form-control', 'required'=>'', 'placeholder'=>'Enter Designation Name (English)']) !!}

                    </div>

                    <div class="form-group">
                        {!! Form::label('designationBangla','Designation Name (Bangla):') !!}

                        <input type="text" class="form-control" placeholder="Enter Designation Name (Bangla)" id="designationBangla" aria-describedby="emailHelp" name="designationBangla" required="" >
                    </div>
                    <div class="form-group">
                        {!! Form::label('description','Description:') !!}
                        {!! Form::textarea('description', null,['class'=>'form-control', 'placeholder'=>'Enter Description']) !!}

                    </div>
                    <div class="form-group">
                        <label for="gradeId">Grade</label>
                        <select name='gradeId' class="form-control" id="gradeId" required >
                            <option value=""> Select Grade</option>
                            <?php 
                            if(($payroll_grade)){  
                            foreach($payroll_grade as $pg){ ?>
                                <option value="{{$pg->id}}"> {{$pg->grade_name}}</option>
                            <?php }} ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="Submit" class="btn btn-primary">Add New</button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

    </script>


    @include('include.copyright')
@endsection