{!! Form::open(['method'=>'DELETE','action'=>['UnitController@destroy',$unit->id]]) !!}

<div class="container">
    <p>You sure you want to delete <strong>{{ $unit->name }}</strong> unit ??</p>
</div>


<div class="text-center  m-t-20">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" id="update-unit" class="btn btn-embossed btn-primary">Confirm Delete</button>
</div>

{!! Form::close() !!}