@extends('layouts.master')
@section('title', 'System Settings')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Logo & Copyright Settings</div>
                    <div class="panel-body">
                        @if($settings->isEmpty())
                        {{Form::open(array('url' => 'settings_store','method' => 'post','files' =>true))}}
                        <div class="form-group">
                                <label for="logo_name">Logo</label>
                                <input type="file" name="logo_name" class="form-control form-white" id="logo_name" placeholder="Enter Grade Name" required>
                         </div>
                         <div class="form-group">
                                <label for="copyright_name">Copyright</label>
                                <input type="text" name="copyright" class="form-control form-white" id="copyright_name" placeholder="Enter Copyright">
                         </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                        {{ Form::close() }}
                            @else
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Order</th>
                                    <th scope="col">Logo</th>
                                    <th scope="col">Copyright</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
								
								
                                @foreach($settings as $setting)
                                <tr>
                                    <th scope="row">1</th>
                                    <td>
                                        <img width="50px" height="50px"   src="{{url('../file/'.$setting->logo)}}" ></td>
                                    </td>
                                    <td>
                                        {{$setting->copyright}}
                                    </td>
                                     <td>
                                        <a data-toggle="modal" data-target="#{{$setting->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>
                                        <a onclick="return confirm('are you sure?')" href="{{ url('/settings_delete/'.$setting->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="{{$setting->id}}" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Update Logo & Copyright</h4>
                                            </div>
                                            <div class="modal-body">
                                                {{Form::open(array('url' => 'settings_update','method' => 'post','files' =>true))}}
                                                <div class="form-group">
                                                    <img width="50px" height="50px" src="{{url('../file/'.$setting->logo)}}">
                                                    <br>
                                                    <label for="logo_name">Logo</label>
                                                    <input type="file" name="logo_name" class="form-control form-white" id="logo_name" placeholder="Enter Grade Name" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="copyright_name">Copyright</label>
                                                    <input type="text" name="copyright" class="form-control form-white" id="copyright_name" value="{{$setting->copyright}}" placeholder="Enter Copyright">
                                                </div>
                                                <hr />
                                                <input type="hidden" name="settings_hidden_id" value="{{$setting->id}}">
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-save "></i> &nbsp; Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> &nbsp; Close</button>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                </tbody>
                            </table>
                         @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection