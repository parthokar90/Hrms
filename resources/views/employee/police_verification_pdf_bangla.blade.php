<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
        'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>পুলিশী তদন্ত</title>
    <style>
        @page { sheet-size: A4; }
        body{
            font-family: 'bangla', sans-serif;
            font-size: 14px;

        }

        .basicInfo{
            width: 100%;
        }

        .basicInfo td{
            height: 25px;
        }


        .reportHeaderArea{
            text-align: center;
        }

        p{
            line-height: 15px;
        }

        .reportHeader p{
            line-height: 10px;
        }

        .appApply{
            text-align:left;

        }
        .family_info {
            border: 1px solid #999;
            padding-left:25px !important;
        }
        table {
            border-collapse: collapse;
        } 
        table tr>td{
            padding: 5px;
        } 
        
    </style>
</head>
<body>

    <div class="container">
        <div class="reportHeaderArea">
            <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
            <p class="reportHeader">{{$companyInformation->company_address1}} <br />
        </div>

        <div>



            <div>
                @foreach($police_verification_data as $data)
                <p>তারিখঃ <?php echo en2bnNumber(date("d-m-Y", strtotime($data->pvDate))); ?><br/> 
                    বরাবর,<br/> 
                    ভারপ্রাপ্ত কর্মকর্তা<br/> 
                    থানা: {{$data->vpkt}}<br/> 
                    জেলা: {{$data->vpkz}}</p> 
                    <p>মাধ্যম,<br/> 
                        পুলিশ সুপার<br/> 
                        বিশেষ শাখা<br/> 
                        জেলাঃ {{$data->mpsz}}</p>
                    </div>
                    <div class="body">
                        @foreach($employee as $emp)
                        <p><b>বিষয়ঃ চরিত্র, তথ্য ও স্থায়ী ঠিকানা যাচাই প্রসঙ্গে ।</b></p>

                        <p>প্রিয় মহোদয়,<br />
                            অত্র গার্মেন্টস প্রতিষ্ঠানের, নামঃ <b>{{$companyInformation->company_name}}</b> কর্মকর্তা/ কর্মচারী / শ্রমিক এর পূর্ণ নামঃ  <b><span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span></b> কার্ড নং <b><span class="boldText"><?php if(!empty($emp->employeeBnId)){echo $emp->employeeBnId;}else{ echo en2bnNumber($emp->employeeId); } ?></span></b> পদবীঃ <b><?php if(!empty($emp->designationBangla)){echo $emp->designationBangla;}else{ echo $emp->designation; } ?></span> </b> হিসাবে কর্মরত আছেন।
                        </p>
                        <br />
                        <div class="family_info">
                         <table height="100px">
                          <tr>
                          <td> ১। &nbsp; ক) পিতা/স্বামীর পূর্ণ নাম</td>
                              <td>: <b><span class="boldText"><?php if(!empty($emp->empBnFatherName)){echo $emp->empBnFatherName;}else{ echo $emp->empFatherName; } ?></span></b></td>
                          </tr>
                          <tr>
                              <td>&nbsp; &nbsp; &nbsp; খ) পিতা/স্বামী চাকরীতে থাকিলে পদের নাম</td>
                              <td>: {{$data->fathersJob}} </td>
                          </tr>
                          <tr>
                              <td>&nbsp; &nbsp; &nbsp; গ) জাতীয়তা</td>
                              <td>: {{$data->fathersNationality}} </td>
                          </tr>
                      </table>

                  </div>
                  <div  class="family_info">
                     <p>২। স্থায়ী ঠিকানাঃ  <span class="boldText"><?php if(!empty($emp->empBnParAddress)){echo $emp->empBnParAddress;}else{ echo $emp->empParAddress; } ?></span></p>
                 </div>
                 <div class="family_info">
                     <p>৩। বর্তমান বাসস্থানের ঠিকানাঃ  <span class="boldText"><?php if(!empty($emp->empBnCurrentAddress)){echo $emp->empBnCurrentAddress;}else{ echo $emp->empCurrentAddress; } ?></span></p>
                 </div>
                 <p style="padding:25px 26px !important;" >৪। প্রার্থী যে সব স্থানে পাঁচ বৎসরে ছয় মাসের অধিক অবস্থান করিয়াছেন সেই সব স্থানের ঠিকানাঃ  </p>
                 <div >
                  <table class="addTable" border="1" width="100%">
                    <tr style="font-weight:bold;">
                        <th>ঠিকানা </th>
                        <th>তারিখ হইতে </th>
                        <th>তারিখ পর্যন্ত </th>
                        <th width="20%"></th>
                    </tr>
                    @foreach($living_history as $data1)
                    <tr>
                        <td style="text-align:center;">{{$data1->address}}</td>
                        <td style="text-align:center;"><?php echo en2bnNumber(date("d-m-Y", strtotime($data1->fromAddress))); ?></td>
                        <td style="text-align:center;"><?php echo en2bnNumber(date("d-m-Y", strtotime($data1->toAddress))); ?></td>
                        <td style="text-align:center;">{{$data1->remarks}}</td>
                    </tr>
                    @endforeach

                </table>
            </div>
            <br />
            <br />
            <p>জন্ম তারিখ (মাধ্যমিক স্কুল পরীক্ষার সার্টিফিকেট অনুযায়ী ) <?php echo en2bnNumber(date("d-m-Y", strtotime($emp->empDOB))); ?> গার্মেন্টস শিল্পের বিশেষ নিরাপত্তার কারনে পুলিশী তদন্তের জন্য পত্রটি আপনার থানায় প্রেরন করা হল। </p>

            <br />
            <p>অতএব, সরজমিনে তদন্ত সাপেক্ষে নিম্ন স্বাক্ষরকারীর বরাবরে লিখিত তদন্ত প্রতিবেদন প্রেরণ করার জন্য অনুরোধ করা হল। </p>



        </div>
        @endforeach
        @endforeach
        <table width="100%" style="text-align:center;">
          <tr>
              <td width="50%"><br /><br /><br />প্রশাসন বিভাগ</td>
              <td><br /><br /><br /></td>
          </tr>
        </table>



    </div>
</div>

</body>
</html>
