<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>প্রশিক্ষনের বিষয়সমূহ</title>
        <style>
            @page { sheet-size: A4; }
            body{
                font-family: 'bangla', sans-serif;
                font-size: 14px;

            }
            p{  
                line-height: 1px;
            }

            .basicInfo{
                width: 100%;
            }

            .basicInfo td{
                height: 22px;
            }


            .reportHeaderArea{
                text-align: center;
            }
            .reportHeader p{
                line-height: 9px;
                padding-top: -10px;
            }
            .container{
                text-align: left;
            }

            .salary{
                text-align:left;
                width: 95%;
            }

            .head{
                font-size: 14px;
                font-weight: bold;
                text-align:left;
            }

            .appApply{
                text-align:left;
                font-size:17px;
                padding-top: -10px; 
                padding-bottom: 25px;
            }

            .content p{
                line-height: 15px;
            }

            .niyogkar p{
                line-height: 15px;
            }

            .boldText{
                font-weight: bold !important;
                font-size: 14px !important;
            }

            .reportHeaderCompany{
                font-size: 35px !important;
                padding: 0px 0px;
                padding-bottom: -18px;
            }
            .h1pad{
                padding-bottom: -10px;
            }
            #table1{
                width: 100%;
            }
            #table1 th,td{
                border:1px;
                border-collapse: collapse;
            }

            .basicInfo th,td{
                border:none;
            }

        </style>
    </head>
    <body>

        <div class="container">
            <div class="reportHeaderArea">
                <h1 class="reportHeaderCompany">{{$companyInformation->bComName}}</h1>
                <p class="reportHeader">{{$companyInformation->bComAddress}}</p>
                <hr>
            </div>

            <div style="text-align: center">
                @foreach($employee as $emp)
                <table class="basicInfo" >
                    <tr>
                        <td style="font-size:16px;">ইউনিট নামঃ <span class="boldText"> <?php if(!empty($emp->bnName)){echo $emp->bnName;}else{ echo $emp->unitName; } ?></span></td>
                    </tr>
                    <tr>
                        <td>নামঃ <span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span></td>
                    </tr>
                    <tr>
                        <td>কার্ড নং:  <span class="boldText"><?php if(!empty($emp->employeeBnId)){echo $emp->employeeBnId;}else{ echo $emp->employeeId; } ?></span></td>
                    </tr>

                    <tr>
                        <td>পদবীঃ <span class="boldText"><?php if(!empty($emp->designationBangla)){echo $emp->designationBangla;}else{ echo $emp->designation; } ?></span></td>
                    </tr>

                    <tr>
                        <td>যোগদানের তারিখঃ <span class="boldText"><?php echo en2bnNumber(date("d-m-Y", strtotime($emp->empJoiningDate)));?> ইং</span></td>
                    </tr>

                </table>
                @endforeach

                <h1 class="h1pad" style="text-align:left;">প্রশিক্ষনের বিষয়সমূহঃ-</h1>
                <div class="appApply">
                   <ul style="list-style-type: none;">
                        <li>১. কর্মঘন্টা/ শ্রমিকদের কর্ম মূল্যায়ন। </li>
                        <li>২. অগ্নি নিরাপত্তা/ অগ্নি নির্বাপন পরিকল্পনা।</li>
                        <li>৩. বৈদ্যুতিক নিরাপত্তা।</li>
                        <li>৪. স্বাস্থ্য ও পয়ঃ নিষ্কাশন।</li>
                        <li>৫. আত্নরক্ষা মুলক ব্যবস্থা।</li>
                        <li>৬. ছুটির পদ্ধতি/ মাতৃত্বকালীন ছুটির পদ্ধতি।</li>
                        <li>৭. সুযোগ সুবিধা।</li>
                        <li>৮. হয়রানী ও উৎপীড়ন মুলক নীতি।</li>
                        <li>৯. ফাইল নীতি।</li>
                        <li>১০. হাউজ কিপিং / পরিস্কার পরিচ্ছতা।</li>
                        <li>১১. নিরাপত্তাজনীত নীতিমালা।</li>
                        <li>১২. ওভারটাইম হিসাব করার পদ্ধতি।</li>
                        <li>১৩. অভিযোগ নীতি</li>
                        <li>১৪. শৃঙ্খলা জনিত বিধিমালা</li>
                        <li>১৫. ফ্যাক্টরী সিকিউরিটি পলিসি।</li>

                   </ul>
                </div>
                <div>
                    <table style="border-collapse: collapse;padding-top: 30px;"  width="100%">
                        <tr>
                           <td width="5%" style="border:1px solid #666;text-align: center;">ক্রমিক নং</td>
                           <td style="border:1px solid #666;text-align: center;">তারিখ</td>
                           <td style="border:1px solid #666;text-align: center;" width="25%" colspan="5">বিষয় সমূহের কোড নং</td>
                           <td style="border:1px solid #666;text-align: center;">শ্রমিকের স্বাক্ষর </td>
                           <td style="border:1px solid #666;text-align: center;">প্রশিক্ষকের স্বাক্ষর</td>
                        </tr> 
                        <tr>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">১</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">২</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">৩</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">৪</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">৫</td>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                        </tr>
                        <tr>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">৬</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">৭</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">৮</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">৯</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">১০</td>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                        </tr>
                        <tr>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">১১</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">১২</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">১৩</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">১৪</td>
                           <td style="border:1px solid #666;padding:8px 0px;text-align: center;">১৫</td>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                           <td style="border:1px solid #666;padding:8px 0px;"></td>
                        </tr>
                    </table>
                </div>
                
            </div>
        </div>

    </body>
</html>
