{{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><strong>Edit Employee Information</strong>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h4>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        {!! Form::open( ['method'=>'PATCH','action'=>['EmployeeController@update',$employee->id],'files'=>true]) !!}

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    {!! Form::label('empFirstName','First Name',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empFirstName',$employee->empFirstName, ['class'=>'form-control','minlength'=>'3', 'placeholder'=>'Minimum 3 characters...','required'=>'']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    {!! Form::label('empLastName','Last Name',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empLastName',$employee->empLastName, ['class'=>'form-control','minlength'=>'3', 'placeholder'=>'Minimum 3 characters...','required'=>'']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    <label class="control-label">Employee Id</label>
                                    <div class="append-icon">
                                        {!! Form::text('employeeId',$employee->employeeId,['class'=>'form-control','required'=>'','placeholder'=>'Enter Employee Id']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    <label class="control-label">Choose Designation</label>
                                    <div class="option-group">
                                        {!! Form::select('empDesignationId', $designations,$employee->empDesignationId,['class'=>'form-control','required'=>'']) !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    <label class="control-label">Select Gender</label>
                                    <div class="option-group">
                                        <select class="form-control" name="empGenderId" required>
                                            <option value="1" {{ $employee->empGenderId==1 ? "selected" :"" }} >Male</option>
                                            <option value="2" {{ $employee->empGenderId==2 ? "selected" :"" }} >Female</option>
                                            <option value="3" {{ $employee->empGenderId==3 ? "selected" :"" }} >Other</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    <label class="control-label">Choose Role</label>
                                    <div class="option-group">
                                        <select name="empRole" class="form-control" required>
                                            <option value="1" {{ $employee->empRole==1 ? "selected": "" }}>Admin</option>
                                            <option value="2" {{ $employee->empRole==2 ? "selected": "" }}>Human Resource</option>
                                            <option value="4" {{ $employee->empRole==4 ? "selected": "" }}>Executive</option>
                                            <option value="5" {{ $employee->empRole==5 ? "selected": "" }}>Accountant</option>
                                            <option value="6" {{ $employee->empRole==6 ? "selected": "" }}>Worker</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="required control-label">Select Department</label>
                                    <div class="option-group">
                                        {!! Form::select('empDepartmentId',$departments,$employee->empDepartmentId,['class'=>'form-control','required'=>'']) !!}

                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="required form-group">
                                    <label class="control-label">Account Status</label>
                                    <div class="option-group">
                                        @if(checkPermission(['admin']))
                                            <select name="empAccStatus" class="language" required>
                                                <option value="1" {{ $employee->empAccStatus==1 ? "selected":'' }}>Active</option>
                                                <option value="0" {{ $employee->empAccStatus==0 ? "selected":''}}>Not Active</option>

                                            </select>
                                        @elseif(checkPermission(['executive']))
                                            {{ $employee->empAccStatus }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="required form-label">Joining Date</label>
                                    <div class="prepend-icon">
                                        {{--<input type="text" name="empJoiningDate" class="date-picker form-control" placeholder="Select a date..." required>--}}
                                        {!! Form::text('empJoiningDate', \Carbon\Carbon::parse($employee->empJoiningDate)->format('m/d/Y'),['class'=>'form-control date-picker','required'=>'']) !!}
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="required control-label">Marital Status</label>
                                    <div class="option-group">
                                        {!! Form::select('empMaritalStatusId', $maritalStatuses,$employee->empMaritalStatusId,['class'=>'form-control','required'=>'']) !!}

                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Email Address</label>
                                    <div class="append-icon">
                                        {!! Form::email('empEmail',$employee->empEmail,['class'=>'form-control','placeholder'=>'Enter your email..']) !!}
                                        <i class="icon-envelope"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Phone Number</label>
                                    <div class="append-icon">

                                        {!! Form::text('empPhone',$employee->empPhone,['class'=>'form-control','placeholder'=>'Mobile Number']) !!}
                                        <i class="icon-screen-smartphone"></i>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empCurrentAddress','Current Address',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::textarea('empCurrentAddress',$employee->empCurrentAddress,['class'=>'form-control','rows'=>'3','placeholder'=>'write your current address']) !!}
                                    </div>
                                </div>
                            </div>



                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empFatherName',"Father's Name",['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empFatherName',$employee->empFatherName,['class'=>'form-control','placeholder'=>"Father's Name" ]) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empParAddress','Permanent Address',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::textarea('empParAddress',$employee->empParAddress,['class'=>'form-control','rows'=>'3','placeholder'=>'Write Your Permanent Address']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label">Date Of Birth</label>
                                    <div class="prepend-icon">
                                        {!! Form::text('empDOB',\Carbon\Carbon::parse($employee->empDOB)->format('m/d/Y'),['class'=>'form-control date-picker']) !!}
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empMotherName',"Mother's Name",['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empMotherName',$employee->empMotherName,['class'=>'form-control','placeholder'=>'Enter mothers name']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empEcontactName','Emergency Contact Name',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empEcontactName',$employee->empEcontactName,['class'=>'form-control']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('emergencyPhone','Emergency Phone',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('emergencyPhone',$employee->emergencyPhone,['class'=>'form-control']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('emergencyAddress','Emergency Contact Address',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::textarea('emergencyAddress',$employee->emergencyAddress,['class'=>'form-control','rows'=>'3', 'placeholder'=>'Write emergency address']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('emergency_contact_relation','Emergency Contact Relation',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('emergency_contact_relation',$employee->emergency_contact_relation,['class'=>'form-control','maxlength'=>'4','placeholder'=>'Maximum 4 letters']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>




                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empBloodGroup','Blood Group',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empBloodGroup',$employee->empBloodGroup,['class'=>'form-control','maxlength'=>'4','placeholder'=>'Maximum 4 letters']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empBloodGroup','Blood Group',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empBloodGroup',$employee->empBloodGroup,['class'=>'form-control','maxlength'=>'4','placeholder'=>'Maximum 4 letters']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>




                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empReligion','Religion',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empReligion',$employee->empReligion,['class'=>'form-control']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empNid','National Id Number',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empNid',$employee->empNid,['class'=>'form-control']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empNationalityId','Nationality',['class'=>'control-label']) !!}
                                    <div class="option-group">
                                        {!! Form::select('empNationalityId', $nationalities,null,['class'=>'form-control']) !!}

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empGlobalId','Biometric ID',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empGlobalId',$employee->empGlobalId,['class'=>'form-control']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empSection','Section',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empSection',$employee->empSection,['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" name="editProfile" class="btn btn-primary">Save Changes</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript">

    var url = "{{ route('employee.section.autocomplete.ajax') }}";

    $('#empSection').typeahead({

        source:  function (query, process) {

            return $.get(url, { query: query }, function (data) {

                return process(data);

            });

        }

    });

</script>