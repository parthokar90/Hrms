<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>শ্রমিক নিয়োগ পত্র</title>
        <style>
            @page { sheet-size: A4; }
            body{
                font-family: 'bangla', sans-serif;
                font-size: 12.5px;

            }
            p{  
                line-height: 1px;
            }

            .basicInfo{
                width: 100%;
            }

            .basicInfo td{
                height: 22px;
            }


            .reportHeaderArea{
                text-align: center;
            }
            .reportHeader p{
                line-height: 9px;
            }
            .container{
                text-align: left;
            }

            .salary{
                text-align:left;
                width: 95%;
            }

            .head{
                font-size: 14px;
                font-weight: bold;
                text-align:left;
            }

            .appApply{
                text-align:left;
            }

            .content p{
                line-height: 15px;
            }

            .niyogkar p{
                line-height: 15px;
            }

            .boldText{
                font-weight: bold !important;
                font-size: 14px !important;
            }

            .reportHeaderCompany{
                font-size: 30px !important;
                padding: 0px 0px;
            }

        </style>
    </head>
    <body>

        <div class="container">
            <div class="reportHeaderArea">
                <h2 class="reportHeaderCompany">{{$companyInformation->bComName}}</h2>
                <p class="reportHeader">{{$companyInformation->bComAddress}}</p>
                <h2 style="text-decoration:underline;"><b>শ্রমিক নিয়োগ পত্র</b></h2>
            </div>

            <div style="text-align: center">
                @foreach($employee as $emp)
                <table class="basicInfo">
                    <tr>
                        <td style="font-size:16px;">ইউনিট নামঃ <span class="boldText"> <?php if(!empty($emp->bnName)){echo $emp->bnName;}else{ echo $emp->unitName; } ?></span></td>
                    </tr>
                    <tr>
                        <td>১। নামঃ <span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span></td>
                        <td>পদবীঃ <span class="boldText"><?php if(!empty($emp->designationBangla)){echo $emp->designationBangla;}else{ echo $emp->designation; } ?></span></td>
                    </tr>
                    <tr>
                        <td>২। পিতার নামঃ  <span class="boldText"><?php if(!empty($emp->empBnFatherName)){echo $emp->empBnFatherName;}else{ echo $emp->empFatherName; } ?></span></td>
                        <td>মাতার নামঃ <span class="boldText"><?php if(!empty($emp->empBnMotherName)){echo $emp->empBnMotherName;}else{ echo $emp->empMotherName; } ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">৩। স্বামী/স্ত্রীর নামঃ  <span>{{$emp->empEcontactName}}</span></td>
                    </tr>
                    <tr>
                        <td>৪। স্থায়ী ঠিকানাঃ <span class="boldText"><?php if(!empty($emp->empBnParAddress)){echo $emp->empBnParAddress;}else{ echo $emp->empParAddress; } ?></span></td>
                    </tr>
                    <tr>
                        <td>৫। বর্তমান ঠিকানাঃ  <span class="boldText"><?php if(!empty($emp->empBnCurrentAddress)){echo $emp->empBnCurrentAddress;}else{ echo $emp->empCurrentAddress; } ?></span></td>
                    </tr>
                    <tr>
                        <td>৬। শিক্ষাগত যোগ্যতাঃ    <span class="boldText"><?php if(!empty($emp->empBnEduQuality)){echo $emp->empBnEduQuality;}else{ echo ''; } ?></span></td>
                        <td>অভিজ্ঞতাঃ (যদি থাকে)  <span class="boldText"><?php if(!empty($emp->empPreExperience)){echo $emp->empPreExperience;}else{ echo ''; } ?></span></td>
                    </tr>
                    <tr>
                        <td>৭। জন্ম তারিখঃ  <span class="boldText"><?php echo en2bnNumber(date("d-m-Y", strtotime($emp->empDOB))); ?></span></td>
                        <td>জাতীয় পরিচয় পত্র নং:   <span class="boldText">{{en2bnNumber($emp->empNid)}}</span></td>
                    </tr>
                </table>
                <table class="basicInfo">
                    <tr>
                        <td>৬। কাজের ধরন:  <span class="boldText">{{$emp->empBnWorkingType}}</span></td>
                        <td>শ্রমিকের শ্রেণীঃ <span class="boldText">শ্রমিক</span>  </td>
                        <td>বায়োমেট্রিক নং:  <span class="boldText">{{en2bnNumber($emp->empGlobalId)}}</span></td>
                    </tr>
                </table>
                <?php 
                    $empJoiningDate = $emp->empJoiningDate;
                    $probation_period = $emp->probation_period;
                    $grade = $emp->probation_period;
                ?>
                @endforeach

                <h2 style="text-align:center;">শর্ত ও নিয়মাবলীঃ</h2>
                <div class="appApply">
                    <p>১. নিয়োগের কার্যকারিতাঃ</p>
                    <p style="line-height:15px">এই নিয়োগ পত্র অদ্য <?php echo en2bnNumber(date("d-m-Y", strtotime($empJoiningDate)));?> ইং (যোগদানের তারিখ) হতে কার্যকরী হবে এবং পরবর্তী <?php echo en2bnNumber($probation_period);?> মাস (যোগদানের তারিখ হতে) অবেক্ষাধীন (Probation) কাল হিসাবে গণ্য হবে। আপনার কর্ম দক্ষতা মান সম্মত না হয় তা হলে অবেক্ষাধীন (Probation) সময় আরো তিন মাস বৃদ্ধি করা যেতে পারে। এই সময় পরে আপনাকে নিয়মিত শ্রমিক হিসাবে গণ্য করা হবে এবং এ জন্য কোন চিঠি দেওয়া হবে না। </p>
                </div>
                <div class="salary">
                    <p class="head">২. বেতনঃ আপনার দক্ষতা অনুযায়ী গ্রেড ভিত্তিতে নিম্নরূপ বেতন নির্ধারণ হবে। </p>
                    @foreach($salary as $sl)
                    <center>
                        <table style="margin-left:50px;" width="90%">

                            <tr>
                                <td colspan="5"><span class="boldText">গ্রেডঃ</span> {{en2bnNumber($sl->gradeName)}} </td>
                            </tr>
                            <tr>
                                <td>ক) মূল বেতন</td>
                                <td>: {{en2bnNumber($sl->basic_salary)}} টাকা</td>
                                <td></td>
                                <td>খ) বাড়ী ভাড়া টাকা</td>
                                <td>: {{en2bnNumber($sl->house_rant)}} টাকা</td>
                            </tr>
                            <tr>
                                <td>গ) চিকিৎসা ভাতা</td>
                                <td>: {{en2bnNumber($sl->medical)}} টাকা</td>
                                <td></td>
                                <td>ঘ) যাতায়েত ভাতা</td>
                                <td>: {{en2bnNumber($sl->transport)}} টাকা</td>
                            </tr>
                            <tr>
                                <td>ঙ) খাদ্য ভাতা</td>
                                <td>: {{en2bnNumber($sl->food)}} টাকা</td>
                                <td></td>
                                <td>চ) অন্যান্য ভাতা</td>
                                <td>: {{en2bnNumber($sl->other)}} টাকা</td>
                            </tr>
                            <tr>
                                <td colspan="5" style="font-size:15px;text-align:center;"><br />সর্বসাকুল্যে মোট বেতন : {{en2bnNumber($sl->total_employee_salary)}} টাকা </td>
                            </tr>
                        </table>
                    </center>
                    @endforeach
                </div>
                <div class="content">
                    <p class="head">৩। বেতন ও মজুরী দেওয়ার তারিখঃ</p>

                    <table style="margin-left:35px;padding-top:-13px;" width="98%">
                        <tr>
                            <td>প্রত্যেক শ্রমিকের পাওনাদি পরবর্তী মাসের ৭ কর্মদিবসের মধ্যে প্রদান করতে হবে।</td>
                        </tr>
                    </table>

                    <p class="head">৪। অতিরিক্ত সময়ঃ</p>

                    <table style="margin-left:35px;padding-top:-13px;" width="98%">
                        <tr>
                            <td>(ক) অতিরিক্ত সময়ের মজুরী মূল বেতনের দ্বিগুণ হারে করা হবে। উহা কোন ভাবেই সর্বসাকুল্যে বেতনের সাথে সম্পর্কিত হবে না।</td>
                        </tr>
                        <tr>
                            <td>(খ) এটা সাধারণ বেতন পাওনার মত একই দিনে পরিশোধ করা হয়।</td>
                        </tr>
                    </table>

                    <p style="text-align:left;">৫। মজুরী বৃদ্ধিঃ আপনার যোগদান তারিখ হতে বছর পূর্ণ হলে মূল মজুরীর ৫% হারে বাৎসরিক ভিত্তিতে মজুরী বৃদ্ধি করা হবে। </p>
                    

                    <p class="head">৬। ছুটি এবং বন্ধঃ </p>

                    <table style="margin-left:35px;padding-top:-13px;" width="98%">
                        <tr>
                            <td>(ক) শ্রমিকদের সপ্তাহে ১ (এক) দিন (সাপ্তাহিক) ছুটি প্রদান করা হয়।</td>
                        </tr>
                        <tr>
                            <td>(খ) শ্রমিকদের নিম্নলিখিত ছুটি গুলো প্রদান করা হয়।
                            <br>
                                <ul style="padding-left:30px;">
                                   <li> উৎসব ছুটি – ১১ দিন</li>
                                   <li> বাৎসরিক ছুটি – প্রতি ১৮ কর্মদিবসের একদিন (চাকুরীর সময়কাল ১ বছর পূর্ণ হলে) বৎসরান্তে আপনার বাৎসরিক ছুটি জমা থাকলে, কোম্পানি আপনাকে আপনার অর্ধেক ছুটির টাকা বছর শেষে নগদ প্রদান করবে। বাকি অর্ধেক ছুটি বছরের বিভিন্ন সময়ে বিভিন্ন পর্ব ইত্যাদি উপলক্ষে প্রদান করা হবে।</li>
                               </ul> 
                               এছাড়াও  
                                <ul style="padding-left:30px;">
                                   <li> নৈমিত্তিক ছুটি – ১০ দিন (পূর্ণ বেতনে)</li>
                                   <li> অসুস্থতার ছুটি – ১৪ দিন(পূর্ণ বেতনে)</li>
                                   <li> মাতৃত্বকালীন ছুটি – ১৬ সপ্তাহ( চাকুরীর সময়কাল ৬মাস পূর্ণহলে) (পূর্ণ বেতনে)</li>
                               </ul>   
                             </td>
                        </tr>
                        <tr>
                            <td>ঊল্লেখ্য যে, কোন মহিলাকে উক্ত রুপ সুবিধা প্রদেয় হবে না যদি তার সন্তান প্রসবের সময়তার ২ (দুই) বা ততোধিক সন্তান জীবিত থাকে, তবে এক্ষেত্রে সে কোন ছুটি পাওয়ার অধিকারী হলে তা পাবে।</td>
                        </tr>
                    </table>

                    <p style="text-align:left;">৭। শ্রমিকের মধ্যে যাহারা নিরবিছিন্নভাবে ১ (এক) বৎসর চাকুরি পূর্ণ করিয়াছেন তাহাদের বৎসরে দুইটি উৎসব ভাতা মোট বেতনের ৫০% হারে প্রদান করা হবে।</p>
                    <p style="text-align:left;">৮। কোম্পানীর নিতিমালা অনুযায়ী প্রতিটি শ্রমিকের হাজিরা বোনাস প্রদান করা হয়।</p>
                    <p style="text-align:left;">৯। কর্মসময়ঃ দৈনিক কর্মসময় ৮ ঘন্টা এবং ওভার টাইম ২ ঘন্টা (ঐচ্ছিক) </p>
                    <p style="text-align:left;">১০। বিশ্রাম বা আহারের বিরতিঃ প্রত্যেক শ্রমিক প্রত্যেক কর্মদিবসে বিশ্রাম বা আহারের জন্য ১ ঘন্টা বিরতি পাবে।  </p>
                    <p style="text-align:left;">১১। হাজিরা কার্ড / পাঞ্চ কার্ডঃ
                    প্রবেশ এবং বাহিরের সময় (অতিরিক্ত সময় সহ) হাজিরা কার্ডে উল্লেখ থাকবে। পাঞ্চ এর ক্ষেত্রে প্রবেশ এবং বাহিরের সময় পাঞ্চ করতে হবে।
                    </p>
                    <p class="head">১২। শ্রমিক কর্তৃক চাকুরী পরিসমাপ্তিঃ </p>

                    <table style="margin-left:35px;padding-top:-13px;" width="98%">
                        <tr>
                            <td> স্থায়ী শ্রমিকের ক্ষেত্রে ৬০ দিন এবং অস্থায়ী শ্রমিকদের ক্ষেত্রে ৩০ দিনের লিখিত নোটিশ বা নোটিশ মেয়াদের মজুরী প্রদান পূর্বক শ্রমিক চাকুরী থেকে পরিসমাপ্তি করতে পারবে। </td>
                        </tr>
                    </table>

                    <p style="text-align:left;">১৩। বাংলাদেশ সরকারের শ্রমআইন ২০০৬ এর ধারা ২৭ এর (৪) মোতাবেক। যে ক্ষেত্রে এই ধারার অধীন কোন স্থায়ী শ্রমিক চাকুরী হইতে ইস্তফা দেন সেক্ষেত্রে, উক্ত শ্রমিককে ক্ষতিপূরণ হিসেবে তাহার প্রত্যেক সম্পূর্ণ বৎসরের চাকুরীর জন্য নিমোক্ত সুবিধা প্রদান করা হয়ঃ</p>

                    <table style="margin-left:35px;padding-top:-13px;" width="98%">
                        <tr>
                            <td>(ক) যদি তিনি ৫ (পাঁচ) বৎসর বা ততোধিক, কিন্তু ১০ (দশ) বৎসরের কমসময় অবিচ্ছিনভাবে মালিকের অধীনে চাকুরী করিয়া থাকেন তাহলে ১৪ (চৌদ্দ) দিনের মজুরী পাবে। </td>
                        </tr>
                        <tr>
                            <td>(খ) যদি তিনি ১০ (দশ) বৎসর বা ততোধিক, সময় মালিকের অধীনে অবিছিন্নভাবে চাকুরী করে থাকেন তাহা হইলে, ৩০ (ত্রিশ) দিনের মজুরী পাবে।  </td>
                        </tr>
                    </table>

                    <p class="head">১৪।  গ্রুপ ইন্সুরেন্সঃ</p>

                    <table style="margin-left:35px;padding-top:-13px;" width="98%">
                        <tr>
                            <td> অবেক্ষাধীন (Probation) কাল সম্পন্ন করার পর সকল শ্রমিক গ্রুপ ইন্সুরেন্স এর আওতায় আসবে। </td>
                        </tr>
                    </table>
                    <p class="head">১৫। শৃঙ্খলা মূলক ব্যবস্থা সমূহঃ (বাংলাদেশ শ্রম আইন ২০০৬ এর ধারা ২৩ অনুযায়ী নেওয়া হয়)</p>
                    <p  style="text-align:left;">কোন শ্রমিক বরখাস্ত বা ছাঁটাই করার ক্ষেত্রে বাংলাদেশ শ্রম আইন মেনে চলা হয় ধারা ২০ এর উপধারা (৪) অনুযায়ী নিম্নলিখিত কাজগুলো শ্রমআইন অনুযায়ী অপরাধ ও অসদাচরণ হিসেবে গন্য হবে। নিম্নে উক্ত অসদাচনের বর্ণনা দেয়া হল।</p>

                    <table style="margin-left:35px;padding-top:-13px;" width="98%">
                        <tr>
                            <td>(ক) উপরস্থের কোন আইনসংগত আদেশ মানার ক্ষেত্রে এককভাবে বা অন্যের সঙ্গে সংঘবদ্ধ হইয়া ইচ্ছাকৃত ভাবে অবাধ্যতা।</td>
                        </tr>
                        <tr>
                            <td>(খ) প্রতিষ্ঠানের ব্যবসা বা সম্পত্তি সম্পর্কে চুরি, প্রতারণা বা অসাধুতা,</td>
                        </tr>
                        <tr>
                            <td>(গ) প্রতিষ্ঠানের অধীনে তাহার বা অন্যকোন শ্রমিকের চাকুরী সংক্রান্ত ঘুষ গ্রহন বা প্রদান,</td>
                        </tr>
                        <tr>
                            <td>(ঘ) বিনা ছুটিতে অভ্যাসগত অনুপস্থিতি অথবা ছুটি না নিয়ে একসঙ্গে ২৭ (সাতাশ) দিনের অধিক সময় অনুপস্থিতি,</td>
                        </tr>
                        <tr>
                            <td>(ঙ) অভ্যাসগত বিলম্বে উপস্থিতি,</td>
                        </tr>
                        <tr>
                            <td>(চ) প্রতিষ্ঠানে প্রযোজ্য কোন আইন, বিধি, বা প্রবিধানের অভ্যাসগত লংঘন,</td>
                        </tr>
                        <tr>    
                            <td>(ছ) প্রতিষ্ঠানে উচ্ছৃঙ্খলবা দাঙ্গা হাঙ্গামা মূলক আচরণ, অথবা শৃঙ্খলা হানিকর কোন কর্ম</td>
                        </tr>
                        <tr>
                            <td>(জ) কাজে কর্মে গাফিলতি,</td>
                        </tr>
                        <tr>
                            <td>(ঝ) প্রধান পরিদর্শক কর্তৃক অনুমোদিত চাকুরী সংক্রান্ত, শৃঙ্খলা বা আচরণসহ, যেকোন বিধি লঙ্ঘন,</td>
                        </tr>
                        <tr>
                            <td>(ঞ) প্রতিষ্ঠানের অফিসিয়াল রেকর্ডের রদবদল, জালকরণ, অন্যায় পরিবর্তন, ক্ষতিকরণ বা হারিয়ে ফেলা।</td>
                        </tr>
                    </table>

                    <p class="head">১৬। ফ্যাক্টরিতে কর্মরত প্রত্যেক কর্মী কোম্পানির নিম্নলিখিত নিয়ম-কানুন মেনে চলতে হবে:-</p>

                    <table style="margin-left:35px;padding-top:-10px;" width="98%">
                        <tr>
                            <td>(ক) সঠিক সময়ে কাজে উপস্থিত</td>
                        </tr>
                        <tr>
                            <td>(খ) ফ্যাক্টরিতে প্রবেশের সময় সকলকর্মী কোম্পানি কর্তৃক প্রদত্ত আই ডি কার্ড/ ইউনিফর্ম পরিধান করতে হবে।</td>
                        </tr>
                    </table>

                    <p  style="text-align:left;">১৭। আপনার চাকুরী অত্র কারখানায় যেকোন সেকশন বা কোম্পানি অধীনস্ত যে কোন কারখনায় বদলি যোগ্য ।</p>
                    <p  style="text-align:left;">আমি এইপত্রের উপরোক্ত শর্তাদি জেনে, বুঝে, শুনে ও স্বজ্ঞানে, সকল শর্তমেনে এই পত্রে স্বাক্ষর করে চাকুরীতে যোগদান করলাম এবং এর একটি কপি কতৃপক্ষের নিকট হতে সংগ্রহ করলাম। উপরে প্রদত্ত সকল তথ্য নির্ভুল ও সঠিক, কোন ভুল ও অসত্যতথ্যের জন্য আমি দায়ী থাকব।</p>
                    <br>

                    <table style="margin-left:35px;padding-top:-10px;" width="98%">
                        <tr>
                            <td width="70%" style="text-align:right;"><br /><br />যোগদানকারীর স্বাক্ষর: </td>
                            <td><br /><br />....................................</td>
                        </tr>
                        <tr>
                            <td style="text-align:right;"> যোগদানের তারিখঃ </td>
                            <td><span class="boldText"><?php echo en2bnNumber(date("d-m-Y", strtotime($empJoiningDate)));?></span> </td>
                        </tr>
                    </table>

                    <br>

                    <table style="margin-left:35px;padding-top:40px;" width="98%">
                        <tr>
                            <td width="20%" style="text-align:center;"><hr>স্বাক্ষর <br /> পার্সোনেল সেকশন</td>
                            <td width="5%" style="text-align:center;"></td>
                            <td width="20%" style="text-align:center;"><hr>স্বাক্ষর <br /> মানবসম্পদ / প্রশাসন</td>
                            <td width="5%" style="text-align:center;"></td>
                            <td width="20%" style="text-align:center;"><hr>স্বাক্ষর <br /> চীফ অপারেটিং অফিসার </td>
                            <td width="5%" style="text-align:center;"></td>
                            <td width="20%" style="text-align:center;"><hr>স্বাক্ষর <br /> পরিচালক </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </body>
</html>
