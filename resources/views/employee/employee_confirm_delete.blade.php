@extends('layouts.master')
@section('title', 'Employee Confirm Delete')
@section('content')
    {{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 ><i class="fa fa-users "></i><strong>Confirm Delete </strong></h3>

                            </div>

                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        {!! Form::open(['method'=>'post','action'=>['EmployeeController@employee_confirmed_delete',base64_encode($employee->id)]]) !!}
                        <div class="tab-pane fade active in" id="tab2_1">
                            <div class="row">
                                <div  class="col-sm-4 text-end">
                                    <h4><strong>Employee Name: </strong></h4>
                                </div>
                                <div class="col-sm-4">
                                    <h4><strong>{{$employee->empFirstName." ".$employee->empLastName}}</strong></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 text-end">
                                    <h5><strong>Employee ID:</strong></h5>
                                </div>
                                <div class="col-sm-4">
                                    <h5><strong>{{$employee->employeeId}}</strong></h5>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4 text-end">
                                    <h5><strong>Card Number:</strong></h5>
                                </div>
                                <div class="col-sm-4">
                                    <h5><strong>{{$employee->empCardNumber}}</strong></h5>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-4 text-end">
                                    <h5><strong>Department:</strong></h5>
                                </div>
                                <div class="col-sm-4">
                                    <h5><strong>{{$employee->departmentName}}</strong></h5>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-4 text-end">
                                    <h5><strong>Designation:</strong></h5>
                                </div>
                                <div class="col-sm-4">
                                    <h5><strong>{{$employee->designation}}</strong></h5>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-4 text-end">
                                    <h5><strong>Joining Date:</strong></h5>
                                </div>
                                <div class="col-sm-4">
                                    <h5><strong>{{\Carbon\Carbon::parse($employee->empJoiningDate)->format('d-m-Y')}}</strong></h5>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-4 text-end">
                                    <h5><strong>Date of Discontinuation:</strong></h5>
                                </div>
                                <div class="col-sm-4">
                                    <h5><strong>{{\Carbon\Carbon::parse($employee->date_of_discontinuation)->format('d-m-Y')}}</strong></h5>
                                </div>

                            </div>

                            <br>

                            <div class="danger">
                                <p><strong class="red-text">Warning***</strong> Deleting a employee will permanently delete the employee and all his related information (attendance, payroll, leave, bonus) ... It will not be
                                possible to get back the deleted data. So please be sure before deleting any employee.</p>
                            </div>

                            <div class="row padTop30">
                                <div class="col-sm-5">
                                    <a class="btn btn-default" href="{{route('employee.delete_employee_list')}}"> Back To List</a>
                                </div>
                                <div class="col-sm-5">
                                    <button type="submit" class="btn btn-danger padLeft20">Confirm Delete</button>
                                </div>
                            </div>


                        </div>
                        {!! Form::close() !!}



                    </div>
                </div>
            </div>
        </div>
    </div>

@include('include.copyright')
@endsection