@extends('layouts.master')
@section('title', 'Shift List')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('createMeeting'))
            <p id="alert_message" class="alert alert-success">{{Session::get('createMeeting')}}</p>
        @endif
        @if(Session::has('meetingUpdate'))
            <p id="alert_message" class="alert alert-success">{{Session::get('meetingUpdate')}}</p>
        @endif
        @if(Session::has('meetingdeleted'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('meetingdeleted')}}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-table"></i> <strong>Shift </strong> List</h3>
                    </div>


                   <div class="row">
                   <form method="get" action="{{url('employee/assign/shift/list')}}">
                       <div class="col-md-12">
                       <div class="col-md-6">
                          <label>Start Date</label>
                          <input type="text" name="start" class="form-control date-picker" autocomplete="off" required>
                       </div>
                       <div class="col-md-6">
                           <label>End Date</label>
                           <input type="text" name="end" class="form-control date-picker" autocomplete="off" required>
                       </div>
                       <button style="margin:10px 12px" type="submit" class="btn btn-success">Search</button>
                    </form>
                    </div>
                   </div>


                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Department</th>
                                <th>Shift</th>
                                <th>Max Entry</th>
                                <th>Break Time</th>
                                <th>Exit Time</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Process</th>
                                <th>Last Process</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php $order=0; @endphp
                                @foreach($list_dept as $list_depts)
                                @php $order++; @endphp
                                  <tr>
                                  <td>{{$order}}</td> 
                                  <td>{{$list_depts->departmentName}}</td> 
                                   <td>{{$list_depts->shiftName}}</td> 
                                   <td>{{date('H:i:a',strtotime($list_depts->m_entry))}}</td> 
                                   <td>{{$list_depts->b_time}}</td> 
                                   <td>{{date('H:i:a',strtotime($list_depts->e_time))}}</td> 
                                   <td>{{date('d-M-Y',strtotime($list_depts->start_date))}}</td> 
                                   <td>{{date('d-M-Y',strtotime($list_depts->end_date))}}</td> 
                                   <td>@if($list_depts->process_status==1) Complete @else Incomplete @endif</td>
                                   <td>@if($list_depts->process_status==1) {{ \Carbon\Carbon::parse($list_depts->created_at)->diffForHumans() }} {{date('d-M-Y',strtotime($list_depts->created_at))}} @endif</td>
                                   <td>
                                   <a class="btn btn-danger btn-sm" href="{{url('employee/assign/shift/delete/department/'.$list_depts->dept_id.'/'.$list_depts->start_date.'/'.$list_depts->end_date)}}"><i class="fa fa-trash"></i></a>
                                   <a href="{{url('employees/shift/process/department/'.$list_depts->dept_id.'/'.$list_depts->start_date.'/'.$list_depts->end_date.'/'.$list_depts->m_entry.'/'.$list_depts->e_time)}}" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Process</a>
                                    </td> 
                                 </tr>
                                @endforeach
                            </tbody>                     
                        </table>
                    </div>



                    

                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Section</th>
                                <th>Shift</th>
                                <th>Max Entry</th>
                                <th>Break Time</th>
                                <th>Exit Time</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Process</th>
                                <th>Last Process</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php $order=0; @endphp
                                @foreach($list_section as $list_sections)
                                @php $order++; @endphp
                                  <tr>
                                  <td>{{$order}}</td> 
                                  <td>{{$list_sections->section_id}}</td> 
                                   <td>{{$list_sections->shiftName}}</td> 
                                   <td>{{date('H:i:a',strtotime($list_sections->m_entry))}}</td> 
                                   <td>{{$list_sections->b_time}}</td> 
                                   <td>{{date('H:i:a',strtotime($list_sections->e_time))}}</td> 
                                   <td>{{date('d-M-Y',strtotime($list_sections->start_date))}}</td> 
                                   <td>{{date('d-M-Y',strtotime($list_sections->end_date))}}</td> 
                                   <td>@if($list_sections->process_status==1) Complete @else Incomplete @endif</td>
                                   <td>@if($list_sections->process_status==1) {{ \Carbon\Carbon::parse($list_sections->created_at)->diffForHumans() }} {{date('d-M-Y',strtotime($list_sections->created_at))}} @endif</td>
                                   <td>
                                       <a class="btn btn-danger btn-sm" href="{{url('employee/assign/shift/delete/section/'.$list_sections->section_id.'/'.$list_sections->start_date.'/'.$list_sections->end_date)}}"><i class="fa fa-trash"></i></a>
                                       <a href="{{url('employees/shift/process/section/'.$list_sections->section_id.'/'.$list_sections->start_date.'/'.$list_sections->end_date.'/'.$list_sections->m_entry.'/'.$list_sections->e_time)}}" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Process</a>
                                    </td> 
                                 </tr>
                                @endforeach
                            </tbody>                     
                        </table>
                    </div>



                    {{-- <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>EmployeeId</th>
                                <th>Name</th>
                                <th>Shift</th>
                                <th>Max Entry</th>
                                <th>Break Time</th>
                                <th>Exit Time</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Attendance</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php $order=0; @endphp
                                @foreach($list_employee as $list_employees)
                                @php $order++; @endphp
                                @php $att=DB::table('attendance')
                                ->whereBetween('date',[$list_employees->start_date,$list_employees->end_date])->where('attendance.emp_id',$list_employees->emp_id)->count();  @endphp
                                @if($att>0) @php $att_status="Found"; @endphp @else @php $att_status="Not Found" @endphp @endif
                                  <tr>
                                  <td>{{$order}}</td> 
                                  <td>{{$list_employees->employeeId}}</td> 
                                  <td>{{$list_employees->empFirstName}}</td> 
                                   <td>{{$list_employees->shiftName}}</td> 
                                   <td>{{date('H:i:a',strtotime($list_employees->m_entry))}}</td> 
                                   <td>{{$list_employees->b_time}}</td> 
                                   <td>{{date('H:i:a',strtotime($list_employees->e_time))}}</td> 
                                   <td>{{date('d-M-Y',strtotime($list_employees->start_date))}}</td> 
                                   <td>{{date('d-M-Y',strtotime($list_employees->end_date))}}</</td> 
                                  <td>{{$att_status}}</td> 
                                   <td>
                                       <a class="btn btn-warning btn-sm" href=""><i class="fa fa-pencil"></i></a>
                                       <a class="btn btn-danger btn-sm" href=""><i class="fa fa-trash"></i></a>
                                       <button class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Process</button>
                                    </td> 
                                 </tr>
                                @endforeach
                            </tbody>                     
                        </table>
                    </div> --}}









                </div>
            </div>
        </div>
    </div>
  @include('include.copyright')
@endsection