@extends('layouts.master')
@section('title', 'Employee List')
@section('content')
{{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}
<div class="page-content">
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 ><i class="fa fa-filter "></i> Search <strong>Employee </strong> List</h3>
                            
                        </div>
                        <div class="col-md-6" style="margin-top:8px;">
                            <a href="{{url('/grid_view')}}" class="btn btn-info btn-round btn-sm"  style="float:right;" ><i class="fa fa-th"></i> Employee List</a>
                            <a href="#"  data-toggle="modal" data-target="#search_employee" class="btn btn-success btn-round btn-sm"  style="float:right;" ><i class="fa fa-search"></i> Search Employee </a>
                        </div>
                    </div>

                </div>
                <div class="panel-content pagination2">
                    <div class="row">
                        @if(!empty($employees))
                            @foreach($employees as $employee)
                                <div class="col-lg-4 col-sm-6">
                                   <a href="{{route('employee.show',$employee->id)}}"   title="View Employee Details" target="_BLANK" >
                                  <div class="panel widget-member" style="color:black !important;background:#eeeeeeab;">
                                    <div class="row">
                                      <div class="col-xs-3">
                                      <br />
                                      <img class="pull-left img-responsive" src="
                                            @if($employee->empPhoto ==null)
                                        @if($employee->empGenderId==1 || $employee->empGenderId==3)
                                        {{asset("Employee_Profile_Pic/male.png")}}
                                        @endif
                                        @if($employee->empGenderId==2)
                                        {{asset("Employee_Profile_Pic/female.jpg")}}
                                        @endif

                                        @endif
                                        @if($employee->empPhoto!=null)
                                        {{asset("Employee_Profile_Pic/".$employee->empPhoto)}}
                                        @endif
                                        ">
                                      </div>
                                      <div class="col-xs-9">
                                        <h3 class="m-t-0 member-name"><strong>{{$employee->empFirstName ." ".$employee->empLastName}}</strong></h3>
                                        <p class="member-job">
                                        @if($employee->departmentName!=null)
                                            <i class="fa fa-server c-gray-light p-r-10" style="color:black;"></i>  <b>{{$employee->departmentName}}</b>
                                        @else
                                            &nbsp;
                                        @endif
                                        </p>
                                        <p class="member-job"><i class="fa fa-anchor c-gray-light p-r-10" style="color:black;"></i> <b>{{$employee->designation}}</b></p>
                                        <div class="row">
                                          <div class="col-xlg-6 col-lg-12 col-sm-6">
                                            <p><i class="fa fa-credit-card c-gray-light p-r-10" style="color:black;"></i> {{$employee->employeeId}}</p>
                                            <p><i class="icon-calendar c-gray-light p-r-10" style="color:black;"></i> {{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</p>
                                          </div>
                                          <div class="col-xlg-6 col-lg-12 col-sm-6 align-right">
                                            <p><i class="fa fa-phone c-gray-light p-r-10" style="color:black;"></i> {{$employee->empPhone}}</p>
                                            <p><i class="fa fa-codepen c-gray-light p-r-10" style="color:black;"></i> {{$employee->empSection}}</p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  </a>
                                </div>

                            @endforeach
                            @else
                            <h4 style="text-align:center;">No matched data available.</h4><br />
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="search_employee" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Search Employee</strong></h4>
            </div>
            <div class="modal-body" style="border-top:1px solid #ddd;">
             <form action="{{url('/employee_grid_search')}}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                  <label for="employeeInfo">Keyword</label>

                <?php if(isset($request)&&!empty($request)){ ?>
                    <input name="keyword" value="{{$request->keyword}}" id="keyword" class="form-control" >
                <?php }else{ ?>
                    <input name="keyword"  placeholder="Write search keyword here."  id="keyword" class="form-control" >
                <?php } ?>

                </div>
            <hr>
            <div class="form-group">
                <button type="submit" class="btn btn-success" ><i class="fa fa-search"></i> Search</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>

</div>
</div>


@include('include.copyright')
@endsection