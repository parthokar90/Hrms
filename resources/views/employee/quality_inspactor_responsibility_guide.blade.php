<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>কোয়ালিটি ইন্সপেক্টর এর দায়িত্ব ও কর্তব্য</title>
        <style>
            @page { sheet-size: A4; }
            body{
                font-family: 'bangla', sans-serif;
                font-size: 17px;

            }
            p{  
                line-height: 1px;
            }

            .basicInfo{
                width: 100%;
            }

            .basicInfo td{
                height: 22px;
            }


            .reportHeaderArea{
                text-align: center;
            }
            .reportHeader p{
                line-height: 9px;
                padding-top: -10px;
            }
            .container{
                text-align: left;
            }

            .salary{
                text-align:left;
                width: 95%;
            }

            .head{
                font-size: 14px;
                font-weight: bold;
                text-align:left;
            }

            .appApply{
                text-align:left;
                font-size:17px;
                padding-top: 15px; 
                padding-bottom: 35px;
            }

            .content p{
                line-height: 15px;
            }

            .niyogkar p{
                line-height: 15px;
            }

            .boldText{
                font-weight: bold !important;
                font-size: 16px !important;
            }

            .reportHeaderCompany{
                font-size: 35px !important;
                padding: 0px 0px;
                padding-bottom: -18px;
            }
            .h1pad{
                padding-bottom: -10px;
            }
            #table1{
                width: 100%;
            }
            #table1 th,td{
                border:1px;
                border-collapse: collapse;
            }
            
            .basicInfo{
                font-size: 16px;
            }
            .basicInfo th,td{
                border:none;
            }

            li{
                padding: 3px 0px;
            }
            ul{
                padding: 0px 0px;
            }

        </style>
    </head>
    <body>

        <div class="container">
            <div class="reportHeaderArea">
                <h1 class="reportHeaderCompany">{{$companyInformation->bComName}}</h1>
                <p class="reportHeader">{{$companyInformation->bComAddress}}</p>
                <hr>
            </div>
            <div>
                <h3 style="text-decoration: underline;text-align: center;">কোয়ালিটি ইন্সপেক্টর এর দায়িত্ব ও কর্তব্য</h3>
            </div>
            <div style="text-align: center">
                @foreach($employee as $emp)
                <table class="basicInfo" >
                    <tr>
                        <td>নামঃ <span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span></td>
                        <td>কার্ড নং:  <span class="boldText"><?php if(!empty($emp->employeeBnId)){echo $emp->employeeBnId;}else{ echo $emp->employeeId; } ?></span></td>
                        <td>সেকশনঃ <span class="boldText"><?php if(!empty($emp->empSection)){echo $emp->empSection;}else{ echo ""; } ?></span></td>
                        <td style="font-size:16px;">ইউনিট নামঃ <span class="boldText"> <?php if(!empty($emp->bnName)){echo $emp->bnName;}else{ echo $emp->unitName; } ?></span></td>
                    </tr>

                </table>
                @endforeach

                <div class="appApply">
                   <ul style="list-style-type: none;">
                        <li>১। যথা সময়ে কর্মস্থলে উপস্থিত ও যাওয়ার সময় সঠিক ভাবে কার্ড পান্চ করে হাজিরা নিশ্চিত করা।</li>
                        <li>২। উরধতন কর্মকর্তাদের নির্দেশ অনুযায়ী আওতাধীন ইউনিট/ ইউনিট সমূহের কোয়ালিটি কন্ট্রোলার ও কোয়ালিটি </li>ইন্সপেক্টরদেরপোশাকেরমানসম্পর্কেনিশ্চিতধারনাদেওয়া।
                        <li>৩। উৎপাদনের প্রতিটি ক্ষেত্রে বায়ারের চাহিদা অনুযায়ী নিরাপত্তা মূলক ব্যবস্থা গ্রহণ ও নথিপত্র সংরক্ষন </li>করা।
                        <li>৪। উৎপাদনের প্রতিটি পর্যায়ে পোশাকের মান নিশ্চিত করার ক্ষেত্রে ইউনিট কোয়ালিটি কন্ট্রোলারের উপদেশ </li>মেনে কাজ করা।
                        <li>৫। বিশেষ কোন সমস্যার উদ্ভব হলে ঊর্ধ্বতন কর্তৃপক্ষের পরামর্শ গ্রহণ ও সে অনুযায়ী কাজ করা।</li>
                        <li>৬। প্রতি ঘণ্টার ডিফেক্ট মাল গুলো সুইং সেকশনের কর্মকর্তাকে বুঝিয়ে দেওয়া এবং সুইং পরবর্তী সমস্যা </li>সমাধানের পর তাদের কাছ থেকে বুঝে নেয়া। 
                        <li>৭। ইন্সপেকশন করার সময় অমনোযোগী হয়ে কাজ করা যাবে না। </li>
                        <li>৮। অন্য কোন কর্মীর সাথে কথা বলতে বলতে কাজ করা যাবে না এতে করে কাজে ভুল হতে পারে।</li>
                        <li>৯। প্রতিটি অর্ডারের ক্ষেত্রে স্যাম্পল সংরক্ষণ ও সে অনুযায়ী পোশাক তৈরি হচ্ছে কি না নিরীক্ষণ করা। </li>
                        <li> ১০। পোশাকের মান উন্নয়নের ক্ষেত্রে কার্যকারী ব্যবস্থাগ্রহণ করা।</li>
                        <li>১১। আর কিউ এস সংক্রান্ত যাবতীয় নথিপত্র যথাযথ তৈরি ও রক্ষণাবেক্ষণ করা হচ্ছে কিনা তা নিশ্চিত করা। </li>
                        <li> ১২। ব্যবহৃত বা ভাঙ্গা সংরক্ষণ সংক্রান্ত নথিপত্র যথাযথ তৈরি ও রক্ষণাবেক্ষণ করা হচ্ছে কিনা তা নিশ্চিত করা।</li>
                        <li>১৩। আইডি কার্ড ও মাক্স পরিধান করে কাজ করা।</li>
                        <li>১৪। সিজার, কাটার, ভোমর যথাযথা স্থানে বেধে কাজ করা।</li>
                        <li>১৫। মেজারমেন্ট টেপ সব সময় সংঙ্গে রাখা।</li>
                        <li>১৬। চেকিং লে আউট মেনে কাজ করা।</li>
                        <li>১৭। প্রতিষ্ঠানের নিয়মনীতি মেনে কাজ করা।</li>
                        <li>১৮। কমপ্লায়েন্স সংক্রান্ত কাজে সহযোগিতা করা।</li>
                        <li>১৯। বায়ারের কাজ যথাযথ সময়ে নির্ভুলে ভাবে চেক করে শিপমেন্ট কাজে সহযোগিতা করা। </li>
                   </ul>
                </div>
                <table width="100%" style="padding-top: 30px;">
                    <tr>
                        <td>-------------------- <br > কর্তৃপক্ষ <br> পিনাকী গ্রুপ</td>
                        <td style="text-align: right;">------------------------------------- <br >কোয়ালিটি ইন্সপেক্টর স্বাক্ষর </td>
                    </tr>
                </table>
                
            </div>
        </div>

    </body>
</html>
