<!DOCTYPE html>
<html>
<head>
    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            max-width: 300px;
            margin: auto;
            text-align: center;
            font-family: arial;
        }

        button {
            border: none;
            outline: 0;
            display: inline-block;
            padding: 8px;
            color: white;
            background-color: #000;
            text-align: center;
            cursor: pointer;
            width: 100%;
            font-size: 17px;
        }

        a {
            text-decoration: none;
            font-size: 18px;
            color: black;
        }

        button:hover, a:hover {
            opacity: 0.7;
        }
    </style>
</head>
<body>

<h2 style="text-align:center">Id Card</h2>

<div class="card">
    <img src="{{asset("Employee_Profile_Pic/".$employee->empPhoto)}}" alt="{{ $employee->empFirstName." ".$employee->empLastName }}" style="width:80%">
    <h3>{{ $employee->empFirstName." ".$employee->empLastName }}</h3>
    <div style="text-align: left;padding-left: 10px;font-size: 14px;">
        <p>Designation:{{ $employee->designation }}</p>
        <p>Card No:{{ $employee->empCardNumber }}</p>
        <p>Employee Id:{{ $employee->employeeId }}</p>
        <p>Department:{{$employee->departmentName}}</p>
        <p>Joining Date:@if($employee->empJoiningDate!=null){{\Carbon\Carbon::parse($employee->empJoiningDate)->format('d-m-Y')}}@endif</p>
        <p>Issued Date:{{\Carbon\Carbon::parse(now())->format('d-m-Y')}}</p>
    </div>
    <div>
        <div style="font-size: 13px;text-align: left;padding-left: 5px;position: relative;top: 29px;">Employee Signature</div>
        <div style="    text-align: right;
    position: relative;
    top: 13px;
    font-size: 14px;">M.D. Signature</div>
    </div>
    <p><button>Far-East IT Solution</button></p>
</div>


</body>
</html>
