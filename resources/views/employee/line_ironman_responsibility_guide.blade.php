<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>লাইন আইরনম্যান দায়িত্ব ও কর্তব্য </title>
        <style>
            @page { sheet-size: A4; }
            body{
                font-family: 'bangla', sans-serif;
                font-size: 17px;

            }
            p{  
                line-height: 1px;
            }

            .basicInfo{
                width: 100%;
            }

            .basicInfo td{
                height: 22px;
            }


            .reportHeaderArea{
                text-align: center;
            }
            .reportHeader p{
                line-height: 9px;
                padding-top: -10px;
            }
            .container{
                text-align: left;
            }

            .salary{
                text-align:left;
                width: 95%;
            }

            .head{
                font-size: 14px;
                font-weight: bold;
                text-align:left;
            }

            .appApply{
                text-align:left;
                font-size:17px;
                padding-top: 15px; 
                padding-bottom: 35px;
            }

            .content p{
                line-height: 15px;
            }

            .niyogkar p{
                line-height: 15px;
            }

            .boldText{
                font-weight: bold !important;
                font-size: 16px !important;
            }

            .reportHeaderCompany{
                font-size: 35px !important;
                padding: 0px 0px;
                padding-bottom: -18px;
            }
            .h1pad{
                padding-bottom: -10px;
            }
            #table1{
                width: 100%;
            }
            #table1 th,td{
                border:1px;
                border-collapse: collapse;
            }
            
            .basicInfo{
                font-size: 16px;
            }
            .basicInfo th,td{
                border:none;
            }

            li{
                padding: 3px 0px;
            }
            ul{
                padding: 0px 0px;
            }

        </style>
    </head>
    <body>

        <div class="container">
            <div class="reportHeaderArea">
                <h1 class="reportHeaderCompany">{{$companyInformation->bComName}}</h1>
                <p class="reportHeader">{{$companyInformation->bComAddress}}</p>
                <hr>
            </div>
            <div>
                <h3 style="text-decoration: underline;text-align: center;">লাইন আইরনম্যান দায়িত্ব ও কর্তব্য  </h3>
            </div>
            <div style="text-align: center">
                @foreach($employee as $emp)
                <table class="basicInfo" >
                    <tr>
                        <td>নামঃ <span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span></td>
                        <td>কার্ড নং:  <span class="boldText"><?php if(!empty($emp->employeeBnId)){echo $emp->employeeBnId;}else{ echo $emp->employeeId; } ?></span></td>
                        <td>সেকশনঃ <span class="boldText"><?php if(!empty($emp->empSection)){echo $emp->empSection;}else{ echo ""; } ?></span></td>
                        <td style="font-size:16px;">ইউনিট নামঃ <span class="boldText"> <?php if(!empty($emp->bnName)){echo $emp->bnName;}else{ echo $emp->unitName; } ?></span></td>
                    </tr>

                </table>
                @endforeach

                <div class="appApply">
                   <ul style="list-style-type: none;">
                        <li>১. যথা সময়ে কর্মস্থলে উপস্থিত এবং যাওয়ার সময় সঠিক ভাবে কার্ড পাঞ্চ করে হাজিরা নিশ্চিত করা।</li>
                        <li>২. লাইন চীফ বা সুপারভাইজারের নির্দেশ মোতাবেক কাজ করা। </li>
                        <li>৩. সাইজ ও নাম্বার অনুযায়ী পোশাকের অংশ পরবর্তী প্রসেসে প্রেরন করা হবে।</li>
                        <li>৪. ব্যবহৃত আয়রন এর রক্ষনাবেক্ষন নিশ্চিত করা।</li>
                        <li>৫. আয়রন ব্যবহারের ক্ষেত্রে প্রয়োজনীয় সতর্কতামূলক ব্যবস্থা গ্রহণ করা।</li>
                        <li>৬. ব্যবহার শেষে উত্তপ্ত আয়রন ঠাণ্ডা করে নিরাপদ স্থান স্ট্যান্ডের উপর রেখে কর্মস্থল ত্যাগ করা।</li>
                        <li>৭. পায়ের নিচে রাবার ম্যাট ব্যবহার করা।</li>
                        <li>৮. আইডি কার্ড পরিধান করে কাজে আসতে হবে।</li>
                        <li>৯. কালার ও সাইজ অনুযায়ী আলাদা আলাদা করে আয়রন করা।</li>
                        <li>১০. আয়রনের গেইট আপ ঠিক রেখে আয়রন করে।</li>
                        <li>১১. ম্যাজারমেন্ট ঠিক রেখে আয়রন করা।</li>
                        <li>১২. আয়রন করার সময় বৈদুত্যিক লাইনের দিকে খেয়াল রেখে কাজ করা।</li>
                        <li>১৩. আয়রন রাখার স্থান ও কাপড় পরিষ্কার রাখা।</li>
                        <li>১৪. আয়রন মেশিনের তার এলোমেলো না রাখা।</li>
                        <li>১৫.  ডার্ক ফেব্রিক্স আয়রন করার সময় আয়রন সু ব্যবহার করা।</li>
                        <li>১৬. হাত পরিষ্কার রেখে কাজ করা।</li>
                        <li>১৭. ঊর্ধ্বতনের নির্দেশ মোতাবেক কাজ করা।</li>
                        <li>১৮. প্রতিষ্ঠানের নিয়ম নীতি মেনে কাজ করা।</li>
                        <li>১৯. কমপ্লায়েন্স সংক্রান্ত কাজে সহযোগিতা করা। </li>

                   </ul>
                </div>
                <table width="100%" style="padding-top: 40px;">
                    <tr>
                        <td>-------------------- <br > কর্তৃপক্ষ <br> পিনাকী গ্রুপ</td>
                        <td style="text-align: right;">------------------------------------- <br >লাইন আয়রনম্যানের স্বাক্ষর </td>
                    </tr>
                </table>
                
            </div>
        </div>

    </body>
</html>
