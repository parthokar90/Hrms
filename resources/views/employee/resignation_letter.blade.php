<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
        'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
        <!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>শ্রমিক অব্যহতি পত্র</title>
    <style>
        @page { sheet-size: A4; }
        body{
            font-family: 'bangla', sans-serif;
            font-size: 12px;

        }
        p{
            line-height: 1px;
        }


        .basicInfo .basicInfa td{
            height: 25px;
        }


        .reportHeaderArea{
            text-align: center;
        }
        .reportHeader p{
            line-height: 10px;
        }
        .container{
            text-align: left;
        }
        .salary{
            text-align:left;
            width: 400px;
        }

        .appApply{
            text-align:left;

        }
        .basicInfo{
            width: 100%;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}}</p>
        <p class="reportHeader">ইমেইলঃ {{$companyInformation->company_email}}</p>
        <p class="reportHeader">ফোনঃ {{$companyInformation->company_phone}}</p>
        <h3 style="margin-top: 0px; text-decoration:underline;"><b>অব্যহতি পত্র</b></h3>
    </div>
</div>
<div class="content">
    <p style="font-size:16px;">ইউনিটঃ {{$employee->unitName}}</p>
    <p style="line-height: 18px">আমি {{$employee->empFirstName." ".$employee->empLastName}} কার্ড নং: {{$employee->employeeId}} পদবীঃ {{$employee->designation}} সেকশনঃ {{$employee->empSection}}, ব্যাক্তিগত/পারিবারিক কারনে আমার পক্ষে চাকুরী করা সম্ভব হচ্ছে না, বিধায় আমি স্বেচ্ছায় চাকুরী থেকে ইস্তফা প্রদান করিলাম। </p>
    <table class="basicInfa">
        <tr>
            <td>১)  চাকুরীতে যোগদানের তারিখঃ</td>
            <td><?php echo en2bnNumber(date("d-m-Y", strtotime($employee->empJoiningDate)));?></td>
        </tr>
        <tr>
            <td>২)  ইস্তফা প্রদানের তারিখঃ </td>
            <td><?php echo en2bnNumber(date("d-m-Y", strtotime(\Carbon\Carbon::now()))) ?></td>
        </tr>
        <tr>
            <td>৩)  ইস্তফা কার্যকর হওয়ার তারিখঃ</td>
            <td>.....................................</td>
        </tr>
    </table>

    <p>স্বাক্ষরঃ ................................</p>
    <p>নামঃ {{$employee->empFirstName." ".$employee->empLastName}}</p>
    <div style="margin-bottom: 40px;">
        <table>
            <tr>
                <td>মন্তব্য সহকারে </td>
                <td>.........................................................................................................................................................................................................</td>
            </tr>
            <tr>
                <td> </td>
                <td>.........................................................................................................................................................................................................</td>
            </tr>
        </table>
    </div>
    <div style="margin-bottom: 60px;">
        <table>
            <tr>
                <td width="43%;">সুপার ভাইজার</td>
                <td width="43%;">সেকশন প্রধান</td>
                <td>স্বাক্ষর পি.এম./কিউ.এ.এম</td>
            </tr>
        </table>
    </div>
    <p style="font-weight: bold; font-size: 17px; text-align: center; text-decoration: underline">কেবল মাত্র অফিসের ব্যাবহারের জন্য</p>
    <table style="margin-bottom: 50px;">
        <tr>
            <td style="height: 30px;">১)  অব্যাহতি পত্র গ্রহণ করা হল/হলনা ।</td>

        </tr>
        <tr>
            <td style="height: 30px;">২)  অব্যাহতি পত্র কার্যকর হওয়ার তারিখঃ..........................................</td>
        </tr>
    </table>
    <div>
        <table>
            <tr>
                <td width="41%;">.............................</td>
                <td width="41%;">.............................</td>
                <td>.................................</td>
            </tr>
            <tr>
                <td width="41%;">স্বাক্ষর <br> টাইম সেকশন</td>
                <td width="41%;">সস্বাক্ষর <br> এইচ, আর/প্রশাসন বিভাগ</td>
                <td>স্বাক্ষর <br>জেনারেল ম্যানেজার/পরিচালক</td>
            </tr>
        </table>
    </div>


</div>

</body>
</html>
