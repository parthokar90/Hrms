<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>অর্থ পরিশোধের ঘোষনা ও মনোনয়নের ফরম</title>
        <style>
            @page { sheet-size: A4; }
            body{
                font-family: 'bangla', sans-serif;
                font-size: 15px;

            }
            p{  
                line-height: 18px;
            }

            .container{
                text-align: left;
            }

            .content{
                line-height: 30px;
            }

            .boldText{
                font-weight: bold !important;
                font-size: 12px !important;
                font-weight: 600;
            }

            table tr td{
                padding:3px;
            }

            .table_heading{
                width: 100%;
                font-size: 11px;
                font-weight: 500;
                text-align: center;
                line-height: .5;
            }
            .table_heading1{
                width: 100%;
                font-size: 12px;
            }

            .table_heading2{
                width: 100%;
                font-size: 11px;
                border-collapse: collapse;
            }
            .table_heading2 tr td{
                border:1px solid #444;

            }
            .table_heading_dhara{
                width: 100%;
                font-size: 10px;
                font-weight: 400;
            }
            .th1_text p{
                line-height: 2;
            }

        </style>
    </head>
    <body>

    <div class="container">
        <table class="table_heading">
           <tr>
               <td><h3>ফরম-৪১</h3></td>
           </tr>
           <tr>
               <td class="table_heading_dhara">[ধারা ১৯, ১৩১ (১) (ক), ১৫৫ (২), ২৩৪, ২৬৪, ২৬৫ ও ২৭৩ এবং বিধি ১১৮ (১), ২৩২ (২), ২৬২ (১) ও ৩২১(১) দ্রষ্টব্য]</td>
           </tr>
           <tr>
              <td><p>জমা ও বিভিন্নখাতে প্রাপ্য অর্থ পরিশোধের ঘোষনা ও মনোনয়নের ফরম</p></td>
           </tr>
        </table>
        <table class="table_heading1">
           <tr>
               <td width="73%">
                <p class="th1_text">
                    <br>
                    <br>
                    ১। কারখানা/প্রতিষ্ঠানের নামঃ <span class="boldText">{{$companyInformation->bComName}}</span>
                    <br>
                    <br>
                    ২। কারখানা/প্রতিষ্ঠানের ঠিকানাঃ <span class="boldText">{{$companyInformation->bComAddress}}</span>
                    <br>
                    <br>
                    ৩। শ্রমিকের নামঃ  <span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span>, <br /> &nbsp;&nbsp;&nbsp; শ্রমিকের ঠিকানাঃ <span class="boldText"><?php if(!empty($emp->empBnCurrentAddress)){echo $emp->empBnCurrentAddress;}else{ echo $emp->empCurrentAddress; } ?></span>
                    <br>
                    <br>
                    ৪। পিতা/মাতা/স্বামী/স্ত্রীর নামঃ 
                    <span class="boldText"><?php if(!empty($emp->empBnFatherName)){echo $emp->empBnFatherName;}else{ echo $emp->empFatherName; } ?></span>
                    <br>
                    <br>
                    ৫। জন্ম তারিখঃ <span class="boldText"><?php echo en2bnNumber(date("d-m-Y", strtotime($emp->empDOB))); ?></span>
                    <br>
                    <br>
                    ৬। সনাক্তকরণ চিহ্ন (যদি থাকে)
                    <br>
                    <br>
                    ৭। স্থায়ী ঠিকানাঃ<span class="boldText"><?php if(!empty($emp->empBnParAddress)){echo $emp->empBnParAddress;}else{ echo $emp->empParAddress; } ?></span>
                    <br>
                    <br>
                    ৮। চাকরীতে নিযুক্তির তারিখঃ 
                        <span class="boldText"><?php echo en2bnNumber(date("d-m-Y", strtotime($emp->empJoiningDate))); ?></span>
                    <br>
                    <br>
                        <?php
                        $designation = $emp->designation;
                        $designationBangla = $emp->designationBangla;
                        ?>
                    ৯। পদের নামঃ <span class="boldText"><?php if(!empty($designationBangla)){echo $designationBangla;}else{ echo  $designation; } ?> 
                    <br>
                    <br><br><br>
                    <br><br><br>
                    <br><br><br>
                </p>

                </td>
               <td>
                   <table class="table_heading2">
                       <tr>
                         <td><center>শ্রমিকের ছবি </center></td>
                       </tr>
                       <tr>
                         <td><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
                       </tr>
                        <tr>
                           <td><center>মনোনীত ব্যক্তির ছবি </center></td>
                       </tr>
                       <tr>
                           <td><br><br><br><br><br><br><br><br><br><br> <br><br><br><br></td>
                       </tr>
                   </table>
               </td>
           </tr>
           <tr>
               <td colspan="2">
                   আমি এতদ্বারা ঘোষণা করিতেছি যে, আমার মৃত্যু হইলে বা আমার অবর্তমানে, আমার অনুকূলে জমা ও বিভিন্নখাতে প্রাপ্য টাকা গ্রহণের জন্য আমি নিম্মবর্নিত ব্যক্তিকে/ব্যক্তিগণকে মনোনয়ন দান করিতেছি এবং নির্দেশ দিচ্ছি যে, উক্ত টাকা নিম্মবর্নিত পদ্ধতিতে মনোনীত ব্যক্তিদের মধ্যে বণ্টন করিতে হইবে। 
               </td>
           </tr>
        </table>
        <table class="table_heading2">
            <tr style="text-align: center;">
                <td  style="text-align:center;">মনোনীত ব্যক্তি বা ব্যক্তিদের নাম, ঠিকানা ও ছবি (নমিনির ছবি ও স্বাক্ষর শ্রমিক কর্তৃক সত্যায়িত )<br />এন আই ডি নং</td>
                <td  style="text-align:center;">সদস্যদের সহিত মনোনীত ব্যক্তিদের সম্পর্ক</td>
                <td  style="text-align:center;">বয়স</td>
                <td  style="text-align:center;" colspan="2">প্রত্যেক মনোনীত ব্যক্তিকে দেয় অংশ </td>
            </tr>
            <tr >
                <td style="text-align: center;" width="26%">(১)</td>
                <td style="text-align: center;" width="24%">(২)</td>
                <td style="text-align: center;" width="10%">(৩)</td>
                <td style="text-align: center;" width="16%"></td>
                <td style="text-align: center;" width="25%">(৪)</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:center;">জমাখাত</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:center;">বকেয়া মজুরী</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:center;">প্রভিডেন্ট ফান্ড</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:center;">বীমা</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:center;">দুর্ঘটনার ক্ষতি পূরণ</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td  style="text-align:center;">লভ্যাংশ</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:center;">অন্যান্য</td>
                <td></td>
            </tr>
        </table>
        <table class="table_heading1">
            <tr>
                <td colspan="2">
                    <br>
                প্রত্যয়ন করিতেছি যে, আমার উপস্থিতিতে জনাব ..................................................................... লিপিবদ্ধ বিবরণসমূহ পাঠ করিবার পর উক্ত ঘোষনা স্বাক্ষর করিয়াছেন।
                   <br><br><br><br><br><br><br>
                </td>
            </tr>
            <tr>
                <td width="65%"></td>
                <td style="margin-top: 0px; border-top: 1px solid #444;">
                মনোনয়ন প্রদানকারী শ্রমিকের স্বাক্ষর, টিপসহি ও তারিখ 
                </td>
            </tr>
        </table>
        <br><br><br>
        <table class="table_heading1">
            <tr>
                <td width="20%">
                    তারিখসহ মনোনীত ব্যক্তিগনের স্বাক্ষর অথবা টিপসহি
                    <br>
                    (শ্রমিক কর্তৃক সত্যায়িত ছবি)
                </td>
                <td width="50%">
                    
                </td>
                <td width="30%">
                    
                    মালিকের বা প্রধিকার প্রাপ্ত কর্মকর্তার স্বাক্ষর
                    <br>
                    <br>
                    তারিখ:..........................................

                </td>
            </tr>
        </table>
    </div>

    </body>
</html>
