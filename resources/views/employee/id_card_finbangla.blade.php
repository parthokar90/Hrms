<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
        'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>


<!DOCTYPE html>
<html>
<head>
    <style>
    *{

    }
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            max-width: 300px;
            max-height: 500px;
            text-align: center;
            float: left;
            width: 80%;
            font-size: 11px;
            height: 370px;
        }
        p{
            margin-bottom: 6px;
            margin-top: 6px;
        }

        button {
            border: none;
            outline: 0;
            display: inline-block;
            padding: 8px;
            color: white;
            background-color: #000;
            text-align: center;
            cursor: pointer;
            width: 100%;
            font-size: 17px;
        }

        a {
            text-decoration: none;
            font-size: 18px;
            color: black;
        }

        button:hover, a:hover {
            opacity: 0.7;
        }
    </style>
{{ Html::script('hrm_script/plugins/jquery/jquery-1.11.1.min.js') }}
</head>
<body>

<div class="card">
        <?php $img1="logo/clogo.jpg"; ?>
    <img style=" float:left;width: 56px;height: 62px;" src="{{asset($img1)}}"/>
    <h4 style="margin-bottom: 10px;position: relative;left: -28px;">ফিন-বাংলা এ্যাপারেলস্ লিঃ</h4>
    <div style="background: #0d007a;color: #ffffff;border-radius: 12px; width: 30%; margin-left: 33%;"> পরিচয়পত্র</div>
    <div style="padding-top: 18px;">
        <div style="float: left">আইডি কার্ড নং: @if($employee->employeeBnId!=null) {{ $employee->employeeBnId }} @else {{ $employee->employeeId }} @endif</div>
        <div style="float: right; padding-right: 5px;">ইস্যুর তারিখ: @if($employee->empJoiningDate!=null){{en2bnNumber(date("d-m-Y", strtotime(\Carbon\Carbon::parse($employee->empJoiningDate))))}}@endif </div>
    </div>
    <div style="margin-top: 25px;">
        <!-- <img src="{{asset("Employee_Profile_Pic/".$employee->empPhoto)}}" alt="{{ $employee->empFirstName." ".$employee->empLastName }}" style="width:100px;"> -->
        <?php $img="Employee_Profile_Pic/demo.jpg"; ?>
        <img width="100px" src="{{asset($img)}}" alt="{{$employee->empFirstName." ".$employee->empLastName}}" >
    </div>
    <div style="text-align: left;padding-left: 10px;font-size: 11px;">
        <p>নামঃ @if($employee->empBnFullName!=null)
            {{ $employee->empBnFullName}}
            @else{{ $employee->empFirstName. " ".$employee->empLastName }}@endif</p>
        <p>পদবীঃ @if($employee->designationBangla!=null){{ $employee->designationBangla }}@else {{ $employee->designation}}  @endif</p>
        <table width="90%" style="padding-left: 0px; margin-left: 0px;">
            <tr>
                <td>কার্ড নংঃ</td>
                <td>@if($employee->employeeBnId!=null) {{ $employee->employeeBnId }} @else {{ $employee->employeeId }} @endif</td>
                <td style="padding-left: 15px;" align="right">বিভাগ/শাখাঃ</td>
                <td>{{$employee->empSection}}</td>
            </tr>
        </table>
        <p>কাজের ধরণঃ {{$employee->empBnWorkingType}}</p>
        <p>যোগদানের তারিখঃ @if($employee->empJoiningDate!=null){{en2bnNumber(date("d-m-Y", strtotime(\Carbon\Carbon::parse($employee->empJoiningDate))))}}@endif</p>
        {{--<p>যোগদানের তারিখঃ@if($employee->empJoiningDate!=null){{en2bnNumber(\Carbon\Carbon::parse($employee->empJoiningDate))}}@endif</p>--}}

    </div>
    <div>
        <div style="text-align: left;padding-left: 5px;position: relative;top: 29px;">শ্রমিকের স্বাক্ষর  </div>
        <div style="text-align: right;
    position: relative;
    top: 13px; padding-right: 5px;">মালিক/ব্যাবস্থাপকের স্বাক্ষর</div>
    </div>
</div>

<div class="card">
    <div style="text-align: left;padding-left: 10px;font-size: 10px;">
        <table width="80%">
            <tr>
                <td align="left">রক্তের গ্রুপঃ {{ $employee->empBloodGroup }}</td>

               <!--  <td style="padding-left: 30px;" align="right">মেয়াদঃ</td>
                <td>{{en2bnNumber(date("d-m-Y", strtotime(\Carbon\Carbon::now()->toDateString())))}}</td> -->
            </tr>
        </table>
        <p style="text-align: center; ">
            প্রতিষ্ঠানের ঠিকানাঃ ৭২, ডেগেরচালা রোড, ছয়দানা, জাতীয় বিশ্ববিদ্যালয়, গাজীপুর ।
        </p>
        <p>স্থায়ী ঠিকানাঃ  {{ $employee->empBnParAddress }}</p>
        <p>জরুরী যোগদানের জন্য ফোন নংঃ  {{en2bnNumber($employee->emergencyPhone)}}</p>

        <p>জাতীয় পরিচয় পত্র নংঃ {{en2bnNumber($employee->empNid)}}</p>
        <div style="border: 1px solid;border-radius: 11px;padding: 3px 5px; margin: 13px;">
            <p style="color: #0d007a; font-weight: bold">জরুরী নাম্বার সমূহঃ</p>
            <span style="color: red; font-size: 9px;">
            ফায়ার সার্ভিসঃ
                <table width="100%">
                <tr>
                    <td>টে: থেকেঃ৯৬৬৬৬৬৬</td>
                    <td>মো: থেকেঃ০২-৯৫৫৬৬৬৬-৭</td>
                </tr>
                </table>


                গাজীপুর থানাঃ
                <table width="100%">
                    <tr>
                        <td>টে: থেকেঃ৯৬৬৬৬৬৬</td>
                        <td>মো: থেকেঃ০২-৯৫৫৬৬৬৬-৭</td>
                    </tr>
                </table>


                <table width="100%">
                    <tr><td>গাজীপুর সদর হাসপাতালঃ</td>
                        <td>টঙ্গি হাসপাতালঃ</td>
                    </tr>
                    <tr>
                        <td>টে: থেকেঃ৯৬৬৬৬৬৬</td>
                        <td>মো: থেকেঃ০২-৯৫৫৬৬৬৬-৭</td>
                    </tr>
                </table>

            </span>

        </div>
        <span style="font-size: 9px;">
            <p>১। কর্তব্যরত অবস্থায় সার্বক্ষণিক এই কার্ডটি ঝুলন্ত অবস্থায় সাথে রাখতে হবে ।</p>
            <p>২। এই কার্ডটি হারাইয়া গেলে তাৎক্ষনিক ব্যবস্থাপনা কর্তৃপক্ষকে জানাতে হবে ।</p>
            <p>৩। চাহিবামাত্র বা অব্যহতিরপর এই কার্ড কর্তৃপক্ষের নিকট ফেরত দিতে হবে ।</p>
            <p style="font-size: 7px; padding-top: 6px;">বিঃ দ্রঃ এই কার্ডটি কারো হস্তগত হইলে উপরোক্ত ঠিকানায় প্রেরণ করার জন্য অনুরোধ করা হল ।</p>
        </span>

    </div>
</div>
</body>
</html>



<script>

$(document).ready(function() {
        window.print();
});
    
</script>