<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
        'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Medical History</title>
    <style>
        @page { sheet-size: A4; }
        body{
            font-family: 'bangla', sans-serif;
            font-size: 12px;

        }

        .basicInfo{
            width: 100%;
        }

        .basicInfo td{
            height: 25px;
        }


        .reportHeaderArea{
            text-align: center;
        }

        p{
            line-height: 15px;
        }

        .reportHeader p{
            line-height: 10px;
        }

        .appApply{
            text-align:left;

        }
        .family_info {
            border: 1px solid #999;
            padding-left:25px !important;
        }
        table {
            border-collapse: collapse;
        } 
        table tr>td{
            padding: 5px;
        } 
        
    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
        <p class="reportHeader">{{$companyInformation->company_address1}} <br />
        ইমেইলঃ {{$companyInformation->company_email}} <br />
        ফোনঃ {{$companyInformation->company_phone}}</p>
    </div>

    <div>

      <h3 style="text-align:center;"><strong>Medical History of {{$employee->empFirstName}} {{$employee->empLastName}} , ID: {{$employee->employeeId}}</strong></h3>
        <hr>
        <p><strong>1. Do employee have any of the following that could affect his/her performance in this job? If “Yes”, please explain- <br />
        (নিম্নলিখিত কোনটির দ্বারা কর্মকর্তা/ কর্মচারীর কার্যক্ষমতা প্রভাবিত হতে পারে? যদি ‘হ্যাঁ’ হয় তবে ব্যাখ্যা করুন।)</strong></p>
        <table width="100%" style="margin-left:35px;">
            <tbody>
                <tr>
                    <td colspan="2">
                        <br>
                        <p>Current and/or reoccurring (chronic) medical/health conditions (বর্তমান অথবা দীর্ঘস্থায়ী চিকিৎসা/স্বাস্থ্য অবস্থা):</p>
                    </td>
                </tr>
                <tr>
                    <td width="12%" style="text-align:center;">
                        <?php if(!empty($medical_history->curHCs)&&($medical_history->curHCs)==1){ echo "Yes (হ্যাঁ) ";} ?>
                        <?php if(!empty($medical_history->curHCs)&&($medical_history->curHCs)==0){ echo "No (না)";}  if(empty($medical_history->curHCs)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->curHCt)&&($medical_history->curHCt)!=NULL){ echo ">> ".$medical_history->curHCt;} ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <br>
                        <p>Current medications (বর্তমান চিকিৎসা):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->curMedicationss)&&($medical_history->curMedicationss)==1){ echo "Yes (হ্যাঁ) ";} ?>
                       <?php if(!empty($medical_history->curMedicationss)&&($medical_history->curMedicationss)==0){ echo "No (না)";}  if(empty($medical_history->curMedicationss)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->curMedicationst)&&($medical_history->curMedicationst)!=NULL){ echo ">> ".$medical_history->curMedicationst;} ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <br>
                        <p>Any medical conditions that make you prone to infections? (কোন মেডিকেল শর্তাবলি আপনার সংক্রমনের প্রবনতা বৃদ্ধি করে?):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->mcpis)&&($medical_history->mcpis)==1){ echo "Yes (হ্যাঁ) ";} ?> 
                        <?php if(!empty($medical_history->mcpis)&&($medical_history->mcpis)==0){ echo "No (না)";}   if(empty($medical_history->mcpis)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->mcpit)&&($medical_history->mcpit)!=NULL){ echo ">> ".$medical_history->mcpit;} ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <br>
                        <p>Past surgery (পূর্বে কোন সার্জারি):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->pastSurgerys)&&($medical_history->pastSurgerys)==1){ echo "Yes (হ্যাঁ)";} ?>
                        <?php if(!empty($medical_history->pastSurgerys)&&($medical_history->pastSurgerys)==0){ echo "No (না)";}  if(empty($medical_history->pastSurgerys)){  echo "No (না)";} ?> 
                    </td>
                    <td>
                       <?php if(!empty($medical_history->pastSurgeryt)&&($medical_history->pastSurgeryt)!=NULL){ echo ">> ".$medical_history->pastSurgeryt;} ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <br>
                        <p>Past and/or present musculoskeletal injuries/problems (back, shoulder, neck, hand, wrist, hip, knee, etc) (অতীত অথবা বর্তমানে কোনরকম পেশী আঘাত/সমস্যা (কাঁধ, ঘাড়, হাত, কব্জি, হাঁটু ইত্যাদি):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->ppmips)&&($medical_history->ppmips)==1){ echo "Yes (হ্যাঁ)";} ?>
                        <?php if(!empty($medical_history->ppmips)&&($medical_history->ppmips)==0){ echo "No (না)";}  if(empty($medical_history->ppmips)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->ppmipt)&&($medical_history->ppmipt)!=NULL){ echo ">> ".$medical_history->ppmipt;} ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <br>
                        <p>Vision problems (দৃষ্টি সমসশা):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->visionProblems)&&($medical_history->visionProblems)==1){ echo "Yes (হ্যাঁ)";} ?>
                        <?php if(!empty($medical_history->visionProblems)&&($medical_history->visionProblems)==0){ echo "No (না)";}  if(empty($medical_history->visionProblems)){  echo "No (না)";} ?> 
                    </td>
                    <td>
                        <?php if(!empty($medical_history->visionProblemt)&&($medical_history->visionProblemt)!=NULL){ echo ">> ".$medical_history->visionProblemt;} ?>
                    </td>
                </tr>


                <tr>
                    <td colspan="2">
                        <br>
                        <p>Hearing problems (শ্রবণ সমসশা):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                      <?php if(!empty($medical_history->hearProblems)&&($medical_history->hearProblems)==1){ echo "Yes (হ্যাঁ)";} ?>
                      <?php if(!empty($medical_history->hearProblems)&&($medical_history->hearProblems)==0){ echo "No (না)";}  if(empty($medical_history->hearProblems)){  echo "No (না)";} ?> 
                    </td>
                    <td>
                        <?php if(!empty($medical_history->hearProblemt)&&($medical_history->hearProblemt)!=NULL){ echo ">> ".$medical_history->hearProblemt;} ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <br>
                        <p>Skin conditions (চর্ম সমসশাঃ):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->skillConditions)&&($medical_history->skillConditions)==1){ echo "Yes (হ্যাঁ)";} ?>
                        <?php if(!empty($medical_history->skillConditions)&&($medical_history->skillConditions)==0){ echo "No (না)";} if(empty($medical_history->skillConditions)){  echo "No (না)";} ?> 
                    </td>
                    <td>
                        <?php if(!empty($medical_history->skillConditiont)&&($medical_history->skillConditiont)!=NULL){ echo ">> ".$medical_history->skillConditiont;} ?>
                    </td>
                </tr>

            </tbody>
        </table>
        <br />
        <br />
        <p><strong>2. Have you had exposure to any of the following hazards without use of recommended Personal Protective Equipment(PPE). If yes, please explain<br /> 
        (আপনি কি সুপারিশকৃত বাক্তিগত সুরক্ষামূলক সরঞ্জাম ব্যবহার না করে নিম্নলিখিত ঝুঁকি কোনটির সংস্পর্শে এসেছেন?)</strong></p>

        <table width="100%" style="margin-left:35px;">
            <tbody>
                <tr>
                    <td colspan="2">
                        <br>
                        <p>Chemicals (রাসায়নিক পদার্থ):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;" width="12%">
                        <?php if(!empty($medical_history->chemicals)&&($medical_history->chemicals)==1){ echo "Yes (হ্যাঁ) ";} ?> 
                        <?php if(!empty($medical_history->chemicals)&&($medical_history->chemicals)==0){ echo "No (না)";} if(empty($medical_history->chemicals)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->chemicalt)&&($medical_history->chemicalt)!=NULL){ echo ">> ".$medical_history->chemicalt;} ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <br>
                        <p>Noise (কোলাহল):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->noises)&&($medical_history->noises)==1){ echo "Yes (হ্যাঁ)";} ?>
                        <?php if(!empty($medical_history->noises)&&($medical_history->noises)==0){ echo "No (না)";} if(empty($medical_history->noises)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->noiset)&&($medical_history->noiset)!=NULL){ echo ">> ".$medical_history->noiset;} ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <br>
                        <p>Radiation (বিকিরণ/রশ্মিবিচ্ছুরন):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->radiations)&&($medical_history->radiations)==1){ echo "Yes (হ্যাঁ)";} ?>
                        <?php if(!empty($medical_history->radiations)&&($medical_history->radiations)==0){ echo "No (না)";} if(empty($medical_history->radiations)){  echo "No (না)";} ?>
                    </td>
                    <td>
                       <?php if(!empty($medical_history->radiationt)&&($medical_history->radiationt)!=NULL){ echo ">> ".$medical_history->radiationt;} ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
        <br />
        <p><strong>3. Allergies and/or Sensitivities  
        (এলার্জি এবং/অথবা সংবেদনশীলতা): </strong></p>

        <table width="100%" style="margin-left:35px;">
            <tbody>
                <tr>
                    <td  colspan="2">
                        <br>
                        <p>Latex (তরুক্ষীর):</p>
                    </td>
                </tr>
                <tr>
                    <td width="12%" style="text-align:center;">
                        <?php if(!empty($medical_history->latexs)&&($medical_history->latexs)==1){ echo "Yes (হ্যাঁ)";} ?>
                        <?php if(!empty($medical_history->latexs)&&($medical_history->latexs)==0){ echo "No (না)";} if(empty($medical_history->latexs)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->latext)&&($medical_history->latext)!=NULL){ echo ">> ".$medical_history->latext;} ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                        <p>Drugs (ঔষধ):</p>
                    </td>

                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->drugss)&&($medical_history->drugss)==1){ echo "Yes (হ্যাঁ)";} ?>
                        <?php if(!empty($medical_history->drugss)&&($medical_history->drugss)==0){ echo "No (না)";} if(empty($medical_history->drugss)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->drugst)&&($medical_history->drugst)!=NULL){ echo ">> ".$medical_history->drugst;} ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                        <p>Chemicals (রাসায়নিক পদার্থ):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->chemicalass)&&($medical_history->chemicalass)==1){ echo "Yes (হ্যাঁ)";} ?>
                        <?php if(!empty($medical_history->chemicalass)&&($medical_history->chemicalass)==0){ echo " No (না)";} if(empty($medical_history->chemicalass)){  echo " No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->chemicalast)&&($medical_history->chemicalast)!=NULL){ echo ">> ".$medical_history->chemicalast;} ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                        <p>Insect Stings (পোকা):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->insects)&&($medical_history->insects)==1){ echo "Yes (হ্যাঁ)";} ?> 
                        <?php if(!empty($medical_history->insects)&&($medical_history->insects)==0){ echo "No (না)";} if(empty($medical_history->insects)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->insectt)&&($medical_history->insectt)!=NULL){ echo ">> ".$medical_history->insectt;} ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                        <p>Fragrances (সুগন্ধী):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->fragrancess)&&($medical_history->fragrancess)==1){ echo "Yes (হ্যাঁ)";} ?>
                        <?php if(!empty($medical_history->fragrancess)&&($medical_history->fragrancess)==0){ echo "No (না)";} if(empty($medical_history->fragrancess)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->fragrancest)&&($medical_history->fragrancest)!=NULL){ echo ">> ".$medical_history->fragrancest;} ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                        <p>Others (অন্যান্য।):</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        <?php if(!empty($medical_history->others)&&($medical_history->others)==1){ echo "Yes (হ্যাঁ)";} ?> 
                        <?php if(!empty($medical_history->others)&&($medical_history->others)==0){ echo "No (না)";} if(empty($medical_history->others)){  echo "No (না)";} ?>
                    </td>
                    <td>
                        <?php if(!empty($medical_history->othert)&&($medical_history->othert)!=NULL){ echo ">> ".$medical_history->othert;} ?>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
</div>

</body>
</html>
