{!! Form::open(['method'=>'PATCH','id'=>'editFrom','action'=>['SkillTestController@update',$id],'files'=>true]) !!}
<div class="form-group required">
    {!! Form::label('skill_name','Skill Test:') !!}
    <input class="form-control" required="" type="text" placeholder="Enter Test Title" name="skill_name" id="skill_name" value="{{$we->skill_name}}">

</div>
<div class="form-group required">
    {!! Form::label('performance','Performance:') !!}
    <input class="form-control" required="" type="text" placeholder="Enter Job Title" name="performance" id="performance" value="{{$we->performance}}">

</div>

<div class="form-group">
    <label class="form-label">Test Date</label>
    <div class="prepend-icon">
        {{--<input type="text" name="empJoiningDate" class="date-picker form-control" placeholder="Select a date..." required>--}}
        {!! Form::text('test_date', \Carbon\Carbon::parse($we->test_date)->format('m/d/Y'),['class'=>'form-control date-picker']) !!}
        <i class="icon-calendar"></i>
    </div>
</div>

<div class="form-group">
    {!! Form::label('skill_Description','Description:') !!}
    <textarea class="form-control" placeholder="Enter Description" name="skill_Description" id="skill_Description">{{$we->skill_Description}}</textarea>

</div>

<div class="form-group">
    {!! Form::label('attachment','Attachment:') !!}

    @if($we->attachment)
        <a href="{{asset('Employee_skill_test/'.$we->attachment)}}" class="btn btn-dark btn-sm" target="_blank"><i class="fa fa-eye"></i> &nbsp; Attachment Available</a>

    @endif

    @if(!$we->attachment)
        <button class="btn btn-danger btn-sm disabled">No Attachment</button>
    @endif
    {!! Form::file('attachment',null,['class'=>'form-control-file','required type'=>'text']) !!}

</div>

</div>
<div class="modal-footer">
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="Submit" id='update-working-experience' class="btn btn-primary">Update</button>
    </div>

{!! Form::close() !!}