<div class="modal fade" id="editProxy" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Change Proxy Number</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'PATCH','action'=>['EmployeeController@UpdateProxy',$employee->id],'files'=>true]) !!}

                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('empCardNumber','Card Number',['class'=>'control-label']) !!}
                        <div class="append-icon">
                            <input value="{{$employee->id}}" id="empRid" hidden >
                            <input type="text" id="empCardNumberCheck" required name="empCardNumber" minlength="10" value="{{$employee->empCardNumber}}" class="form-control">

                            <div id="empCardNumber_meg"></div>
                        </div>
                    </div>
                </div>
            

            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit"  id="savebtn1"  class="btn btn-primary">Update Proxy</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<script type="text/javascript">
         $(document).ready(function () {
        $("#empCardNumberCheck").keyup(function () {
            var empCardNumber= $("#empCardNumberCheck").val();
            var empRid= $("#empRid").val();
            $.ajax({
                url: 'employee_card_number_check_edit/' + empCardNumber +'/'+ empRid,
                method: 'get',
                dataType: 'html',
                success: function (response) {
                    // alert(response);
                    // console.log(response);
                    if(response>0){
                        $('#empCardNumber_meg').html("<span style='color:red;'><b>Employee card number already exits.</b></span>");
                        $('#savebtn1').prop("disabled",true);
                    }else{
                        $('#empCardNumber_meg').html('');
                        $('#savebtn1').prop("disabled",false);
                    }
                }
            });
        });
     });
</script>