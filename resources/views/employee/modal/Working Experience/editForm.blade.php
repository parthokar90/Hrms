{!! Form::open(['method'=>'PATCH','id'=>'editFrom','action'=>['WorkingExperienceController@update',$id],'files'=>true]) !!}
<div class="form-group">
    {!! Form::label('empCompanyName','Company Name:') !!}
    <input class="form-control" required="" type="text" placeholder="Enter Company Name" name="empCompanyName" id="empCompanyName" value="{{$we->empCompanyName}}">

</div>
<div class="form-group">
    {!! Form::label('empJobTile','Job Title:') !!}
    <input class="form-control" required="" type="text" placeholder="Enter Job Title" name="empJobTile" id="empJobTile" value="{{$we->empJobTile}}">

</div>

<div class="form-group">
    <label class="required form-label">Joining Date</label>
    <div class="prepend-icon">
        {{--<input type="text" name="empJoiningDate" class="date-picker form-control" placeholder="Select a date..." required>--}}
        {!! Form::text('empJoiningDate', \Carbon\Carbon::parse($we->empJoiningDate)->format('m/d/Y'),['class'=>'form-control date-picker hasDatePicker','required'=>'']) !!}
        <i class="icon-calendar"></i>
    </div>
</div>

<div class="form-group">
    {!! Form::label('empLeaveDate','Leaving Date:') !!}
    <input class="form-control date-picker" required="" type="text" placeholder="Enter Leaving Date" name="empLeaveDate" id="empLeaveDate" value="{{\Carbon\Carbon::parse($we->empLeaveDate)->format('m/d/Y')}}">

</div>

<div class="form-group">
    {!! Form::label('empWHDescription','Description:') !!}
    <textarea class="form-control" required="" placeholder="Enter Description" name="empWHDescription" id="empWHDescription">{{$we->empWHDescription}}</textarea>

</div>

<div class="form-group">
    {!! Form::label('empWHattachment','Attachment:') !!}

    @if($we->empWHattachment)
        <a href="{{asset('Employee_Working_Experience/'.$we->empWHattachment)}}" class="btn btn-dark btn-sm" target="_blank"><i class="fa fa-eye"></i> &nbsp; Attachment Available</a>

    @endif

    @if(!$we->empWHattachment)
        <button class="btn btn-danger btn-sm disabled">No Attachment</button>
    @endif
    {!! Form::file('empWHattachment',null,['class'=>'form-control-file','required type'=>'text']) !!}

</div>

</div>
<div class="modal-footer">
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="Submit" id='update-working-experience' class="btn btn-primary">Update</button>
    </div>

{!! Form::close() !!}