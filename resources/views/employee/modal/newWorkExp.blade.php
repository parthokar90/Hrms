<div class="modal fade" id="newWork" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Previous Working Information</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST','action'=>['WorkingExperienceController@store'],'files'=>true]) !!}
                {!! Form::hidden('emp_id',$emp_id) !!}

                <div class="form-group">
                    {!! Form::label('empCompanyName','Company Name:') !!}
                    {!! Form::text('empCompanyName',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Company Name']) !!}

                </div>
                <div class="form-group">
                    {!! Form::label('empJobTile','Job Title:') !!}
                    {!! Form::text('empJobTile',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter InstitutionName']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('empJoiningDate','Joining Date:') !!}
                    {!! Form::text('empJoiningDate',null,['class'=>'date-picker form-control', 'placeholder'=>'Enter Joining Date']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('empLeaveDate','Leaving Date:') !!}
                    {!! Form::text('empLeaveDate',null,['class'=>'date-picker form-control', 'placeholder'=>'Enter Leaving Date']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('empWHDescription','Other Details:') !!}
                    {!! Form::textarea('empWHDescription',null,['class'=>'form-control','rows'=>7, 'placeholder'=>'Enter Other Details Here']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('empWHattachment','Attachment:') !!} (Max Size:5mb)
                    {!! Form::file('empWHattachment', null, ['class'=>'form-control-file','required type'=>'text']) !!}

                </div>


            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary">Add New</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>