<div class="modal fade" id="newNominee" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Add New Nominee</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST','action'=>['NomineeController@store'],'files'=>true]) !!}
                {!! Form::hidden('emp_id',$emp_id) !!}
                <div class="form-group">
                    {!! Form::label('nominee_name','Nominee Name:', ['class'=>'required']) !!}
                    <input class="form-control" required="" type="text" placeholder="Enter Nominee Name" name="nominee_name" id="nominee_name">

                </div>
                <div class="form-group">
                    {!! Form::label('nominee_phone','Phone Number:') !!}
                    <input class="form-control" type="text" placeholder="Enter Nominee Phone Number" name="nominee_phone" id="nominee_phone">

                </div>

                <div class="form-group">
                    <label class="required form-label">Nominee Address</label>
                    <textarea class="form-control" required="" placeholder="Enter Address" name="nominee_address" id="nominee_address"></textarea>



                </div>

                <div class="form-group">
                    {!! Form::label('priority','Nominee Priority:') !!}
                    <div class="option-group">
                        <select class="form-control" name="priority">
                            <option value="First">First</option>
                            <option value="Second">Second</option>
                            <option value="Third">Third</option>

                        </select>
                    </div>

                </div>

                <div class="form-group">
                    {!! Form::label('nominee_details','Description:') !!}
                    <textarea class="form-control" placeholder="Enter Description" name="nominee_details" id="nominee_details"></textarea>

                </div>

                <div class="form-group">
                    {!! Form::label('nominee_attachments','Attachment:') !!}
                    {!! Form::file('nominee_attachments',null,['class'=>'form-control-file','type'=>'text']) !!}

                </div>

            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close</button>
                    <button type="Submit" class="btn btn-primary"><i class="fa fa-save"></i> &nbsp; Save </button>
                </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>