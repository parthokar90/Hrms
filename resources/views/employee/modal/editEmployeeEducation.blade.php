<div class="modal fade" id="{{'educationEdit'.$ec->id}}" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Edit Information</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'PATCH','action'=>['EmployeeEducationInfoController@update',$ec->id],'files'=>true]) !!}
                <div class="form-group">
                    {!! Form::label('empExamTitle','Examination Name:') !!}
                    <input class="form-control" required="" type="text" placeholder="Enter Examination Name" name="empExamTitle" id="empExamTitle" value="{{$ec->empExamTitle}}">

                </div>
                <div class="form-group">
                    {!! Form::label('empInstitution','Institution:') !!}
                    <input class="form-control" required="" type="text" placeholder="Enter Institution Name" name="empInstitution" id="empInstitution" value="{{$ec->empInstitution}}">

                </div>

                <div class="form-group">
                    {!! Form::label('empResult','Exam Result:') !!}
                    <input class="form-control" required="" type="text" placeholder="Enter Result" name="empResult" id="empResult" value="{{$ec->empResult}}">

                </div>

                <div class="form-group">
                    {!! Form::label('empScale','Scale:') !!}
                    <input class="form-control" required="" type="text" placeholder="Enter Result Scale" name="empScale" id="empScale" value="{{$ec->empScale}}">

                </div>

                <div class="form-group">
                    {!! Form::label('empPassYear','Passing Year:') !!}
                    <input class="form-control" required="" type="text" placeholder="Enter Passing Year" name="empPassYear" id="empPassYear" value="{{$ec->empPassYear}}">

                </div>

                <div class="form-group">
                    {!! Form::label('empCertificate','Attachment:') !!}

                    @if($ec->empCertificate)
                        <a href="{{asset('Educational_Certificates/'.$ec->empCertificate)}}" class="btn btn-dark btn-sm" target="_blank"><i class="fa fa-eye"></i> &nbsp; Attachment Available</a>

                    @endif

                    @if(!$ec->empCertificate)
                        <button class="btn btn-danger btn-sm disabled">No Attachment</button>
                    @endif
                    {!! Form::file('empCertificate',null,['class'=>'form-control-file','required type'=>'text']) !!}

                </div>

            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary">Update</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>