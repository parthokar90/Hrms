<div class="modal fade" id="dod" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Confirm Discontinuation/Resignation Date: </strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST','action'=>['EmployeeController@UpdateResignationDate']]) !!}

                {!! Form::hidden('emp_id',session('emp_id')) !!}
                <div class="form-group">
                    <label class="required form-label">Date of Discontinuation</label>
                    <div class="prepend-icon">
                        {{--<input type="text" name="empJoiningDate" class="date-picker form-control" placeholder="Select a date..." required>--}}
                        {!! Form::text('date_of_discontinuation', \Carbon\Carbon::now()->format('m/d/Y'),['class'=>'form-control date-picker','required'=>'']) !!}
                        <i class="icon-calendar"></i>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('reason_of_discontinuation','Reason of Discontinuation',['class'=>'control-label']) !!}
                    <div class="append-icon">
                        <textarea id='reason_of_discontinuations' name="reason_of_discontinuation" rows="3" class="form-control" placeholder="Write the reason for discontinuation... "></textarea>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary">Update Info</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>