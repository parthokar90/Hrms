{!! Form::open(['method'=>'PATCH','id'=>'editFrom','action'=>['EmployeeTrainingController@update',$data->id],'files'=>true]) !!}
<div class="form-group">
    {!! Form::label('training_name','Training Name:', ['class'=>'required']) !!}
    <input class="form-control" required="" value="{{$data->training_name}}" type="text" placeholder="Enter Training Name" name="training_name" id="training_name">

</div>
<div class="form-group">
    {!! Form::label('training_institution','Institution/Company:', ['class'=>'required']) !!}
    <input class="form-control" type="text" value="{{$data->training_institution}}" required="" placeholder="Enter Training Institution" name="training_institution" id="training_institution">

</div>

<div class="form-group">
    {!! Form::label('training_start','Start Date:',['class'=>'required']) !!}
    {!! Form::text('training_start',\Carbon\Carbon::parse($data->training_start)->format('d M Y'),['class'=>'date-picker form-control', 'required'=>'', 'placeholder'=>'Enter Start Date']) !!}

</div>

<div class="form-group">
    {!! Form::label('training_end','Training End:',['class'=>'required']) !!}
    {!! Form::text('training_end',\Carbon\Carbon::parse($data->training_end)->format('d M Y'),['class'=>'date-picker form-control', 'required'=>'', 'placeholder'=>'Enter Leaving Date']) !!}

</div>

<div class="form-group">
    <label class="form-label">Description</label>
    <textarea class="form-control" placeholder="Enter Description" name="training_description" id="training_description">{{$data->training_description}}</textarea>

</div>
</div>

<div class="form-group">
    {!! Form::label('training_attachment','Attachment:') !!}

    @if($data->training_attachment)
        <a href="{{asset('Employee_Nominee_Attachments/'.$data->training_attachment)}}" class="btn btn-dark btn-sm" target="_blank"><i class="fa fa-eye"></i> &nbsp; Attachment Available</a>

    @endif

    @if(!$data->training_attachment)
        <button class="btn btn-danger btn-sm disabled">No Attachment</button>
    @endif
    {!! Form::file('training_attachment',null,['class'=>'form-control-file','type'=>'text']) !!}

</div>

</div>
<div class="modal-footer">
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="Submit" id='update-working-experience' class="btn btn-primary">Update</button>
    </div>

{!! Form::close() !!}