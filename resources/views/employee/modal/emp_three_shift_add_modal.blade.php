<div class="modal fade" id="emp_three_shift_modal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="fa fa-plus"> Add New Shift </i></h4>
        </div>
        <div class="modal-body">
      
          <div class="row">
              <div class="col-md-4">
                    <div class="form-group">
                     <label>Select Shift <span style="color:red">*</span></label>
                        <select class="form-control" name="shift_id[]" autocomplete="off" required>
                           <option value="">Select</option>
                             @foreach($shifts as $shift)
                               <option value="{{$shift->id}}">{{$shift->shiftName}} </option>
                             @endforeach
                        </select>
                    </div>
              </div>  
              <div class="col-md-4">
                <div class="form-group">
                  <label>Select Weekend <span style="color:red">*</span></label>
                     <select class="form-control" name="shift_id" autocomplete="off" required>
                        <option value="">Select</option>
                          @foreach($weekday as $weekdays)
                            <option value="{{$weekdays->id}}">{{$weekdays->day}} </option>
                          @endforeach
                     </select>
                 </div>
               </div> 
               <input type="hidden" name="emp_id" value="{{$employee->id}}"> 
               <div class="col-md-6">
                 <button id="three_shift_add_btn" type="button" class="btn btn-success"><i class="fa fa-save"> Save Information</i> </button>
               </div>
            
            </div> 
        </div>
      </div>
    </div>
  </div>


  