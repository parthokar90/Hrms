<div class="modal fade" id="skillTest" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Employee Skill Test</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST','action'=>['SkillTestController@store'],'files'=>true]) !!}
                {!! Form::hidden('emp_id',$emp_id) !!}

                <div class="form-group">
                    {!! Form::label('skill_name','Skill Name:') !!}
                    {!! Form::text('skill_name',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Skill Name']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('performance','Performance:') !!}
                    {!! Form::text('performance',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Score']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('test_date','Test Date:') !!}
                    {!! Form::text('test_date',null,['class'=>'date-picker form-control', 'placeholder'=>'Enter Skill taken Date']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('skill_Description','Description:') !!}
                    {!! Form::textarea('skill_Description',null,['class'=>'form-control','rows'=>7, 'placeholder'=>'Enter Other Details Here']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('attachment','Attachment:') !!} (Max Size:5mb)
                    {!! Form::file('attachment', null, ['class'=>'form-control-file','required type'=>'text']) !!}

                </div>


            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary">Add New</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>