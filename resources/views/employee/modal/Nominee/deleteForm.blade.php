{!! Form::open(['method'=>'DELETE','action'=>['NomineeController@destroy',$data->id]]) !!}

<p>You sure you want to delete <strong>{{ $data->priority }}</strong> Nominee ??</p>

<div class="modal-footer">
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="Submit" id='update-working-experience' class="btn btn-primary">Confirm Delete</button>
    </div>

</div>


{!! Form::close() !!}