{!! Form::open(['method'=>'PATCH','id'=>'editFrom','action'=>['NomineeController@update',$nominee->id],'files'=>true]) !!}
<div class="form-group">
    {!! Form::label('nominee_name','Nominee Name:', ['class'=>'required']) !!}
    <input class="form-control" required="" type="text" placeholder="Enter Nominee Name" name="nominee_name" id="nominee_name" value="{{$nominee->nominee_name}}">

</div>
<div class="form-group">
    {!! Form::label('nominee_phone','Phone Number:') !!}
    <input class="form-control" type="text" placeholder="Enter Nominee Phone Number" name="nominee_phone" id="nominee_phone" value="{{$nominee->nominee_phone}}">

</div>

<div class="form-group">
    <label class="required form-label">Nominee Address</label>
    <textarea class="form-control" required="" placeholder="Enter Address" name="nominee_address" id="nominee_address">{{$nominee->nominee_address}}</textarea>



</div>

<div class="form-group">
    {!! Form::label('priority','Nominee Priority:') !!}
    <div class="option-group">
        <select class="form-control" name="priority">
            <option value="First" {{$nominee->priority=='First' ? 'selected' : '' }}>First</option>
            <option value="Second" {{$nominee->priority=='Second' ? 'selected' : '' }}>Second</option>
            <option value="Third" {{$nominee->priority=='Third' ? 'selected' : '' }}>Third</option>

        </select>
    </div>

</div>

<div class="form-group">
    {!! Form::label('nominee_details','Description:') !!}
    <textarea class="form-control" placeholder="Enter Description" name="nominee_details" id="nominee_details">{{$nominee->nominee_details}}</textarea>

</div>

<div class="form-group">
    {!! Form::label('nominee_attachments','Attachment:') !!}

    @if($nominee->nominee_attachments)
        <a href="{{asset('Nominee/'.$nominee->nominee_attachments)}}" class="btn btn-dark btn-sm" target="_blank"><i class="fa fa-eye"></i> &nbsp; Attachment Available</a>

    @endif

    @if(!$nominee->nominee_attachments)
        <button class="btn btn-danger btn-sm disabled">No Attachment</button>
    @endif
    {!! Form::file('nominee_attachments',null,['class'=>'form-control-file','type'=>'text']) !!}

</div>

</div>
<div class="modal-footer">
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="Submit" id='update-working-experience' class="btn btn-primary">Update</button>
    </div>

{!! Form::close() !!}