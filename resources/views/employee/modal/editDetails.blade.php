

{{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit"></i> <strong>Edit Employee Information</strong>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h4>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        {!! Form::open( ['method'=>'PATCH','action'=>['EmployeeController@update',$employee->id],'files'=>true]) !!}

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    {!! Form::label('empFirstName','Full Name',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empFirstName',$employee->empFirstName, ['class'=>'form-control','minlength'=>'3', 'placeholder'=>'Minimum 3 characters...','required'=>'']) !!}
                                        {!! Form::hidden('empLastName'," ", ['class'=>'form-control', 'placeholder'=>'Enter last name ...']) !!}

                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--{!! Form::label('empLastName','Last Name',['class'=>'control-label']) !!}--}}
                                    {{--<div class="append-icon">--}}
                                        {{--<i class="icon-user"></i>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    <label class="control-label">Employee ID</label>
                                    <div class="append-icon">
                                        <input id="employeeIdEdit" name="employeeId" class="form-control" value="{{$employee->employeeId}}" required="" placeholder="Enter Employee ID" >
                                        <br>
                                       <div id="employeeId_meg"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    <label class="control-label">Designation</label>
                                    <div class="option-group">
                                        {!! Form::select('empDesignationId', $designations,$employee->empDesignationId,['class'=>'form-control','required'=>'']) !!}

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    <label class="control-label">Gender</label>
                                    <div class="option-group">
                                        <select class="form-control" name="empGenderId" required>
                                            <option value="1" {{ $employee->empGenderId==1 ? "selected" :"" }} >Male</option>
                                            <option value="2" {{ $employee->empGenderId==2 ? "selected" :"" }} >Female</option>
                                            <option value="3" {{ $employee->empGenderId==3 ? "selected" :"" }} >Other</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="required form-group">
                                    <label class="control-label">Role</label>
                                    <div class="option-group">
                                        <select name="empRole" class="form-control" required>
                                            @if(checkPermission(['admin']))
                                                <option value="1" {{ $employee->empRole==1 ? "selected": "" }}>Admin</option>
                                                <option value="2" {{ $employee->empRole==2 ? "selected": "" }}>Human Resource</option>
                                                <option value="7" {{ $employee->empRole==7 ? "selected": "" }}>Hr Admin</option>
                                                <option value="4" {{ $employee->empRole==4 ? "selected": "" }}>Executive</option>
                                                <option value="5" {{ $employee->empRole==5 ? "selected": "" }}>Accountant</option>
                                                <option value="6" {{ $employee->empRole==6 ? "selected": "" }}>Worker</option>
                                            @elseif(checkPermission(['hr']))
                                                <option value="2" {{ $employee->empRole==2 ? "selected": "" }}>Human Resource</option>
                                                <option value="4" {{ $employee->empRole==4 ? "selected": "" }}>Executive</option>
                                                <option value="5" {{ $employee->empRole==5 ? "selected": "" }}>Accountant</option>
                                                <option value="6" {{ $employee->empRole==6 ? "selected": "" }}>Worker</option>
                                            @elseif(checkPermission(['hr-admin']))
                                                <option value="7" {{ $employee->empRole==7 ? "selected": "" }}>Hr Admin</option>
                                                <option value="4" {{ $employee->empRole==4 ? "selected": "" }}>Executive</option>
                                                <option value="5" {{ $employee->empRole==5 ? "selected": "" }}>Accountant</option>
                                                <option value="6" {{ $employee->empRole==6 ? "selected": "" }}>Worker</option>

                                            @elseif((checkPermission(['executive'])))
                                                <option value="6" {{ $employee->empRole==6 ? "selected": "" }}>Worker</option>

                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="required control-label">Department</label>
                                    <div class="option-group">
                                        {!! Form::select('empDepartmentId',$departments,$employee->empDepartmentId,['class'=>'form-control','required'=>'']) !!}

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="required control-label">Marital Status</label>
                                    <div class="option-group">
                                        {!! Form::select('empMaritalStatusId', $maritalStatuses,$employee->empMaritalStatusId,['class'=>'form-control','required'=>'']) !!}

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="required form-label">Joining Date</label>
                                    <div class="prepend-icon">
                                        {{--<input type="text" name="empJoiningDate" class="date-picker form-control" placeholder="Select a date..." required>--}}
                                        {!! Form::date('empJoiningDate', $employee->empJoiningDate,['class'=>'form-control','required'=>'']) !!}
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="required form-group">
                                    <label class="control-label">Account Status</label>
                                    <div class="option-group">
                                        <select id="language" name="empAccStatus" onchange="statusFunc(this)" class="language" required>
                                            @if(checkPermission(['admin']))
                                                <option value="1" {{ $employee->empAccStatus==1 ? "selected":'' }}>Active</option>
                                                <option value="0" {{ $employee->empAccStatus==0 ? "selected":''}}>Not Active</option>
                                            @elseif(checkPermission(['hr']) || checkPermission(['hr-admin']))
                                                <option value="1" {{ $employee->empAccStatus==1 ? "selected":'' }}>Active</option>
                                                <option value="0" {{ $employee->empAccStatus==0 ? "selected":''}}>Not Active</option>
                                            @elseif(checkPermission(['executive']))
                                                <option value="{{$employee->empAccStatus}}"> Restricted </option>

                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label">Select Work group</label>
                                <div class="option-group">
                                    <select class="form-control" name="work_group">
                                        <option value="" {{ $employee->work_group=='' ? "selected" :"" }} >Select One</option>
                                        <option value="Staff" {{ $employee->work_group=='Staff' ? "selected" :"" }} >Staff</option>
                                        <option value="Worker" {{ $employee->work_group=='Worker' ? "selected" :"" }} >Worker</option>
                                    </select>
                                </div>
                            </div>
     
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label">3 Shift</label>
                                <div class="form-group">
                                   <select class="form-control" name="is_three_shift">
                                       @if($employee->is_three_shift==0)
                                        <option value="0" selected>Off</option>
                                        <option value="1">On</option>
                                         @else 
                                         <option value="0">Off</option>
                                         <option value="1" selected>On</option>
                                       @endif
                                   </select>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Email Address</label>
                                    <div class="append-icon">
                                        {!! Form::email('empEmail',$employee->empEmail,['class'=>'form-control','placeholder'=>'Enter your email..']) !!}
                                        <i class="icon-envelope"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Phone Number</label>
                                    <div class="append-icon">

                                        {!! Form::text('empPhone',$employee->empPhone,['class'=>'form-control','placeholder'=>'Mobile Number']) !!}
                                        <i class="icon-screen-smartphone"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empCurrentAddress','Current Address',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::textarea('empCurrentAddress',$employee->empCurrentAddress,['class'=>'form-control','rows'=>'3','placeholder'=>'write your current address']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empParAddress','Permanent Address',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::textarea('empParAddress',$employee->empParAddress,['class'=>'form-control','rows'=>'3','placeholder'=>'Write Your Permanent Address']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empFatherName',"Father's Name",['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empFatherName',$employee->empFatherName,['class'=>'form-control','placeholder'=>"Father's Name" ]) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empMotherName',"Mother's Name",['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empMotherName',$employee->empMotherName,['class'=>'form-control','placeholder'=>'Enter mothers name']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label">Date Of Birth</label>
                                    <div class="prepend-icon">
                                        @if($employee->empDOB!=null)
                                            {!! Form::date('empDOB',$employee->empDOB,['class'=>'form-control']) !!}
                                        @else
                                            {!! Form::date('empDOB',null,['class'=>'form-control']) !!}
                                        @endif
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('probation_period',"Probation Period (Enter the month number only)",['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        <input type="number" name="probation_period" maxlength="2" class="form-control" placeholder="By default probation period is 12 Months" value="{{$employee->probation_period}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empEcontactName','Emergency Contact Name',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empEcontactName',$employee->empEcontactName,['class'=>'form-control']) !!}
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('emergencyPhone','Emergency Phone',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('emergencyPhone',$employee->emergencyPhone,['class'=>'form-control']) !!}
                                        <i class="icon-screen-smartphone"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('emergencyAddress','Emergency Contact Address',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::textarea('emergencyAddress',$employee->emergencyAddress,['class'=>'form-control','rows'=>'3', 'placeholder'=>'Write emergency address']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('emergency_contact_relation','Emergency Contact Relation',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('emergency_contact_relation',$employee->emergency_contact_relation,['class'=>'form-control','placeholder'=>'Enter relation with emergency contact']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">OT Status</label>
                                    <select class="form-control" name="empOTStatus">
                                        <option value="">Select one</option>
                                        <option {{ $employee->empOTStatus=='1' ? "selected" :"" }} value="1">Yes</option>
                                        <option {{ $employee->empOTStatus=='0' ? "selected" :"" }} value="0">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label">Attendance Bonus</label>
                                    <div class="prepend-icon">
                                        <select name="empAttBonusId" required="" class="form-control">
                                                <option selected="" value="0">No</option>
                                            @foreach($attendance_bonus as $ab)
                                                <option  <?php if($ab->id==$employee->empAttBonusId){ echo "selected"; } ?>  value="{{$ab->id}}">{{$ab->bTitle}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Unit Name</label>
                                    <div class="option-group">
                                        {!! Form::select('unit_id',[''=>'Select Unit']+$units,$employee->unit_id,['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('floor_name','Floor',['class'=>'control-label']) !!}
                                    <div class="option-group">
                                        {!! Form::select('floor_name', [''=>'Select Floor']+$floors,$employee->floor_id,['class'=>'form-control floor','data-search'=>'true']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('line_id','Line Number',['class'=>'control-label']) !!}
                                    @if(isset($employee->floor_id))
                                        <div class="option-group">
                                            {!! Form::select('line_id', [''=>'Select Line']+$lines,$employee->line_id,['class'=>'form-control floor_lines','data-search'=>'true']) !!}
                                        </div>
                                    @else
                                        <div class="option-group">
                                            <select class="form-control floor_lines">
                                                <option value="">Select Floor First</option>
                                            </select>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label">Select Salary Type</label>
                                <div class="option-group">
                                    <select class="form-control" name="salary_type">
                                        <option value="" {{ $employee->salary_type=='' ? "selected" :"" }} >Select One</option>
                                        <option value="Monthly" {{ $employee->salary_type=='Monthly' ? "selected" :"" }} >Monthly</option>
                                        <option value="Weekly" {{ $employee->salary_type=='Weekly' ? "selected" :"" }} >Weekly</option>
                                        <option value="Piecewise" {{ $employee->salary_type=='Piecewise' ? "selected" :"" }} >Piecewise</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('skill_level',"Skill Level",['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        <input type="text" name="skill_level" value="{{$employee->skill_level}}" class="form-control" placeholder="Skill Level">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label">Payment Mode</label>
                                <div class="option-group">
                                    <select class="form-control" name="payment_mode">
                                        <option value="Cash" {{ $employee->payment_mode=='Cash' ? "selected" :"" }} >Cash</option>
                                        <option value="Bank" {{ $employee->payment_mode=='Bank' ? "selected" :"" }} >Bank</option>
                                        <option value="bKash" {{ $employee->payment_mode=='bKash' ? "selected" :"" }} >bKash</option>
                                        <option value="Rocket" {{ $employee->payment_mode=='Rocket' ? "selected" :"" }} >Rocket(DBBL)</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empBloodGroup','Blood Group',['class'=>'control-label']) !!}
                                    <div class="append-icon">

                                        <select id="empBloodGroup" name="empBloodGroup" class="form-control">
                                            <option value="">Select a Blood Group</option>
                                                <option {{ $employee->empBloodGroup=='A+' ? "selected" :"" }} value="A+">A+</option>
                                                <option {{ $employee->empBloodGroup=='B+' ? "selected" :"" }}  value="B+">B+</option>
                                                <option {{ $employee->empBloodGroup=='AB+' ? "selected" :"" }}  value="AB+">AB+</option>
                                                <option {{ $employee->empBloodGroup=='O+' ? "selected" :"" }}  value="O+">O+</option>
                                                <option {{ $employee->empBloodGroup=='A-' ? "selected" :"" }}  value="A-">A-</option>
                                                <option {{ $employee->empBloodGroup=='B-' ? "selected" :"" }}  value="B-">B-</option>
                                                <option {{ $employee->empBloodGroup=='AB-' ? "selected" :"" }}  value="AB-">AB-</option>
                                                <option {{ $employee->empBloodGroup=='O-' ? "selected" :"" }}  value="O-">O-</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('bank_account','Account',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('bank_account',$employee->bank_account,['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('bank_info','Account Information',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::textarea('bank_info',$employee->bank_info,['class'=>'form-control','rows'=>'3']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empReligion','Religion',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empReligion',$employee->empReligion,['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empNid','National Id Number',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empNid',$employee->empNid,['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empNationalityId','Nationality',['class'=>'control-label']) !!}
                                    <div class="option-group">
                                        {!! Form::select('empNationalityId', $nationalities,null,['class'=>'form-control']) !!}

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empGlobalId','Biometric ID',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empGlobalId',$employee->empGlobalId,['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('empSection','Section',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        {!! Form::text('empSection',$employee->empSection,['class'=>'form-control','id'=>'empSection']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Date of Discontinuation</label>
                                    <div class="append-icon">
                                        {!! Form::text('date_of_discontinuation',$employee->date_of_discontinuation,['class'=>'form-control date-picker','id'=>'date_of_discontinuation']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Reason of Discontinuation</label>
                                    <div class="append-icon">

                                        {!! Form::text('reason_of_discontinuation',$employee->reason_of_discontinuation,['class'=>'form-control','id'=>'reason_of_discontinuation','placeholder'=>'Reason of discontinuation']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                 <div class="form-group">
                                    <label class="control-label">Discontinuation Type</label>
                                    <div class="option-group">
                                        <select class="form-control" name="discon_type">
                                            <option ></option>
                                            <option value="1" {{ $employee->discon_type=='1' ? 'selected':'' }}>Lefty</option>
                                            <option value="2" {{$employee->discon_type=='2' ? 'selected':'' }}>Resign</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                 <div class="form-group">
                                    {!! Form::label('reference_type','Reference Type',['class'=>'control-label']) !!}
                                    <div class="option-group">
                                        <select class="form-control" name="reference_type">
                                            <option value="None" {{ $employee->reference_type=='None' ? 'selected':'' }}>None</option>
                                            <option value="Internal" {{$employee->reference_type=='Internal' ? 'selected':'' }}>Internal</option>
                                            <option value="Others"{{$employee->reference_type=='Others'?'selected':''}}>Others</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                               <div class="form-group">
                                    {!! Form::label('reference_details','Reference Details',['class'=>'control-label']) !!}
                                    <div class="append-icon">
                                        <textarea id='reference_details' name="reference_details" rows="3" class="form-control" placeholder="Write the reference details... ">{{$employee->reference_description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                
                            </div>
                        </div>

                    </div>
                </div>
                    <center><div id="error_message"></div></center>
            </div>
            <hr>

            


            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" id="savebtn" name="editProfile" class="btn btn-primary">Update Changes</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("select.floor").change(function () {
            var floor= $(".floor option:selected").val();
            if(!floor){
                var rr='<select class="form-control floor_lines" name="line_id">'+
                    '<option value="">Select Floor First</option>'+

                    '</select>';
                $('.floor_lines').html(rr);
            }
            else {
                $.ajax({
                    url: 'floor/line/' + floor,
                    method: 'get',
                    dataType: 'html',
                    success: function (response) {
                        $('.floor_lines').html(response);


                    }
                });
            }

        });


    });


    function statusFunc(that) {
        if(that.value==='0'){
            document.getElementById('date_of_discontinuation').disabled=false;
            document.getElementById('reason_of_discontinuation').disabled=false;
        }
        else {
            document.getElementById('date_of_discontinuation').disabled=true;
            document.getElementById('reason_of_discontinuation').disabled=true;

        }

    }
</script>
<script type="text/javascript">
     $(document).ready(function () {
        $("#employeeIdEdit").keyup(function () {
            var employeeId= $("#employeeIdEdit").val();
            var empRid= $("#empRid").val();
            $.ajax({
                url: 'employee_id_check_edit/' + employeeId +'/'+ empRid,
                method: 'get',
                dataType: 'html',
                success: function (response) {
                    // alert(response);
                    // console.log(response);
                    if(response>0){
                        $('#error_message').html("<span style='color:red;text-align:center;'><b>Please solve the error before submit.</b></span>");
                        $('#employeeId_meg').html("<span style='color:red;'><b>Employee ID already exits.</b></span>");
                        $('#savebtn').prop("disabled",true);
                    }else{
                        $('#error_message').html("");
                        $('#employeeId_meg').html('');
                        $('#savebtn').prop("disabled",false);
                    }
                }
            });
        });
     });

    var url = "{{ route('employee.section.autocomplete.ajax') }}";

    $('#empSection').typeahead({

        source:  function (query, process) {

            return $.get(url, { query: query }, function (data) {

                return process(data);

            });

        }

    });

</script>