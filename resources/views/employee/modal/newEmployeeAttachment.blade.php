<div class="modal fade" id="employeeAttachment" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Additional Attachments</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST','action'=>['OtherAttachmentController@store'],'files'=>true]) !!}
                {!! Form::hidden('emp_id',$emp_id) !!}

                <div class="form-group">
                    {!! Form::label('empAttachmentTitle','Attachment Title:') !!}
                    {!! Form::text('empAttachmentTitle',null,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Title']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('empAttachmentDescription','Add Description:') !!}
                    {!! Form::textarea('empAttachmentDescription',null,['class'=>'form-control','rows'=>7, 'required'=>'', 'placeholder'=>'Enter Other Details Here']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('empAttachment','Choose Attachment:') !!} (Max Size:5mb)
                    {!! Form::file('empAttachment', ['class'=>'form-control','required'=>'']) !!}

                </div>


            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary">Add Attachment</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>