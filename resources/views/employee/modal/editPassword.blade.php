<div class="modal fade" id="editPassword" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Change User Password</strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'PATCH','action'=>['EmployeeController@UpdatePassword',$employee->id]]) !!}

                <div class="form-group">
                    {!! Form::label('empPassword','Password:') !!}
                    {!! Form::password('empPassword',['class'=>'form-control','required'=>'', 'minlength'=>'4','maxlength'=>'16', 'placeholder'=>'Enter between 4 to 16 chars']) !!}

                </div>

            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary">Update Password</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>