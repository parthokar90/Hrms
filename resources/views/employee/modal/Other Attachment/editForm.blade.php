
{!! Form::open(['method'=>'PATCH','action'=>['OtherAttachmentController@update',$otherAttachment->id],'files'=>true]) !!}
{{--{!! Form::hidden('emp_id',$emp_id) !!}--}}

<div class="form-group">
    {!! Form::label('empAttachmentTitle','Attachment Title:') !!}
    {!! Form::text('empAttachmentTitle',$otherAttachment->empAttachmentTitle,['class'=>'form-control','required type'=>'text', 'placeholder'=>'Enter Title']) !!}

</div>

<div class="form-group">
    {!! Form::label('empAttachmentDescription','Add Description:') !!}
    {!! Form::textarea('empAttachmentDescription',$otherAttachment->empAttachmentDescription,['class'=>'form-control','rows'=>7, 'required'=>'', 'placeholder'=>'Enter Other Details Here']) !!}

</div>

<div class="form-group">
    {!! Form::label('empAttachment','Choose Attachment:') !!} (Max Size:5mb)
    @if($otherAttachment->empAttachment)
        <a href="{{asset('Employee_Attachments/'.$otherAttachment->empAttachment)}}" class="btn btn-dark btn-sm" target="_blank"><i class="fa fa-eye"></i> &nbsp; Attachment Available</a>

    @endif
    {!! Form::file('empAttachment', ['class'=>'form-control']) !!}

</div>

<div class="modal-footer">
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="Submit" class="btn btn-primary">Update</button>
</div>
</div>

{!! Form::close() !!}

