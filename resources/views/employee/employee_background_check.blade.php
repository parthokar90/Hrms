<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>গুরুত্বপূর্ণ তথ্যাবলী (Background Check)</title>
        <style>
            @page { sheet-size: A4; }
            body{
                font-family: 'bangla', sans-serif;
                font-size: 13px;

            }
            p{  
                line-height: 18px;
            }

            .basicInfo{
                width: 100%;
            }

            .basicInfo td{
                height: 22px;
            }


            .reportHeaderArea{
                text-align: center;
            }
            .reportHeader p{
                line-height: 9px;
                padding-top: -10px;
                margin-top: 0px;
            }
            .container{
                text-align: left;
            }

            .salary{
                text-align:left;
                width: 95%;
            }

            .head{
                font-size: 14px;
                font-weight: bold;
                text-align:left;
            }

            .appApply{
                text-align:left;
                font-size:17px;
                padding-top: -10px; 
                padding-bottom: 25px;
            }

            .content p{
                line-height: 15px;
            }

            .niyogkar p{
                line-height: 15px;
            }

            .boldText{
                font-weight: bold !important;
                font-size: 14px !important;
                padding-left: 5px;
            }

            .reportHeaderCompany{
                font-size: 32px !important;
                padding: -5px -5px;
                padding-bottom: -20px;
            }
            .h1pad{
                padding-bottom: -10px;
            }
            .table1{
                width: 100%;
                border-collapse: collapse;
                font-size: 13px;
            }
            .table2{
                width: 100%;
            }
            .table1 tr td{
                border: 1px solid #555;
            }

            .basicInfo th,td{
                border:none;
            }

            .table3{
                width: 100%;
            }

            .table3 tr td{
                border:none;
            }
        </style>
    </head>
    <body>

        <div class="container">
            <div class="reportHeaderArea">
                <h1 class="reportHeaderCompany">{{$companyInformation->bComName}}</h1>
                <p class="reportHeader">{{$companyInformation->bComAddress}}</p>
                <hr style="padding: 0px; margin:0px; border:1px solid #444;" >
            </div>

            <div style="text-align: left;">
               <table class="table3"  style="padding-top: 8px;padding-bottom: 8px;">
                   <tr>
                        <td style="text-align: left;">তারিখঃ  @if($emp->empJoiningDate!=null){{en2bnNumber(date("d-m-Y", strtotime(\Carbon\Carbon::parse($emp->empJoiningDate))))}}@endif</td>
                       <td style="text-align: center;"> 
                            <h3  style="text-align: center; padding-top: 0px; padding-bottom: -6px;font-weight: 600;">গুরুত্বপূর্ণ তথ্যাবলী</h3>
                            <p style="text-align: center; font-size: 10px; padding-top: -10px;padding-bottom: 0px;font-weight: 500;">(Employee Background Check)</p>
                        </td>
                        <td width="26%"></td>
                   </tr>
               </table>
                
                <table class="table1">
                    <tr>
                        <td width="6%" style="text-align:center;" >০১</td>
                        <td style="padding-left:5px;" width="44%">প্রার্থীর পুরো নাম (ডাক নাম সহ)</td>
                        <td width="50%"> &nbsp;&nbsp;<span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span></td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >০২</td>
                        <td style="padding-left:5px;">পদবী ও কার্ড নং</td>
                        <td>
                            <span class="boldText"> &nbsp;&nbsp;<?php if(!empty($emp->designationBangla)){echo $emp->designationBangla;}else{ echo $emp->designation; } ?></span>, কার্ড: <span class="boldText">@if($emp->employeeBnId!=null) {{ $emp->employeeBnId }} @else {{ en2bnNumber($emp->employeeId) }} @endif</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >০৩</td>
                        <td style="padding-left:5px;">প্রার্থীর যোগাযোগের নম্বর (যদি থাকে)</td>
                        <td><span class="boldText"> &nbsp;&nbsp;<?php if(!empty($emp->empPhone)){echo en2bnNumber($emp->empPhone);}else{ echo ''; } ?></span></td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >০৪</td>
                        <td style="padding-left:5px;">আপনি পূর্বে কোন কারখানায় চাকুরী করেছেন কি?    </td>
                        <td>
                            <table class="table3" style="border-collapse: collapse;padding-top: 0px;margin-top: -2px;margin-left: -2px;margin-right: -2px;" >
                                <tr>
                                    <td style="border:1px solid #555;"></td>
                                    <td style="border:1px solid #555;text-align: center;" width="10%">হ্যাঁ</td>
                                    <td style="border:1px solid #555;"></td>
                                    <td style="border:1px solid #555;"></td>
                                    <td style="border:1px solid #555;text-align: center;" width="10%">না</td>
                                    <td style="border:1px solid #555;"></td>
                                </tr>
                            </table>
                            <table class="table3">
                                <tr>
                                    <td>
                                        <p>
                                        কারখানার নামঃ
                                        <br>
                                        পদবীঃ
                                        <br>
                                        মাসিক বেতন/ মজুরীঃ
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >০৫</td>
                        <td style="padding-left:5px;">পূর্বতন কারখানায় (সর্বশেষ) আপনার উর্ধ্বতন কর্মকর্তা কে ছিলেন?</td>
                        <td>
                            <table class="table3">
                                <tr>
                                    <td>
                                        <p>
                                        উর্ধ্বতন কর্মকর্তার নামঃ 
                                        <br>
                                        পদবীঃ
                                        <br>
                                        ফোন নম্বরঃ
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >০৬</td>
                        <td style="padding-left:5px;">আপনার পূর্বের কারখানা ত্যাগের কারন কি?</td>
                        <td>
                            <table class="table3" style="border-collapse: collapse;padding-top: 0px; margin:-2px;" >
                                <tr>
                                    <td width="25%" style="border:1px solid #555;text-align: center;">অসুস্থতা </td>
                                    <td width="25%" style="border:1px solid #555;text-align: center;">বহিস্কার </td>
                                    <td width="25%" style="border:1px solid #555;text-align: center;">বেতন কম </td>
                                    <td width="25%" style="border:1px solid #555;text-align: center;">বাস স্থানবদল</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="border:1px solid #555;text-align: center;">বেতন সময়মত না হওয়া  </td>
                                    <td colspan="2" style="border:1px solid #555;text-align: left;padding-left: 5px;">অন্যান্যঃ </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >০৭</td>
                        <td style="padding-left:5px;">আপনার কোন নিকট আত্নীয় অত্র কারখানায় চাকুরী করেন কি?
                            <br>উত্তর হ্যাঁ হলে লিখুন </td>
                        <td>
                            <table class="table3">
                                <tr>
                                    <td>
                                        <p>
                                        নামঃ
                                        <br>
                                        ঠিকানাঃ
                                        <br>
                                        <br>
                                        ফোন নম্বরঃ
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >০৮</td>
                        <td style="padding-left:5px;">বর্তমান ঠিকানায় আপনার বাসস্থানে আর কেউ থাকে কি? 
                            <br>উত্তর হ্যাঁ হলে লিখুন </td>
                        <td> 
                            <table class="table3" style="border-collapse: collapse;padding-top: 0px;margin-top: -2px;margin-left: -2px;margin-right: -2px;" >
                                <tr>
                                    <td style="border:1px solid #555;"></td>
                                    <td style="border:1px solid #555;text-align: center;" width="10%">হ্যাঁ</td>
                                    <td style="border:1px solid #555;"></td>
                                    <td style="border:1px solid #555;"></td>
                                    <td style="border:1px solid #555;text-align: center;" width="10%">না</td>
                                    <td style="border:1px solid #555;"></td>
                                </tr>
                            </table>
                            <table class="table3">
                                <tr>
                                    <td>
                                        <p>
                                        নামঃ
                                        <br>
                                        সম্পর্কঃ
                                        <br>
                                        ফোন নম্বরঃ
                                        </p>
                                    </td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >০৯</td>
                        <td style="padding-left:5px;">বর্তমান বাড়িওয়ালার তথ্যবলী । </td>
                        <td>
                            <table class="table3">
                                <tr>
                                    <td>
                                        <p>
                                        নামঃ
                                        <br>
                                        ঠিকানাঃ
                                        <br>
                                        <br>
                                        </p>
                                    </td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >১০</td>
                        <td style="padding-left:5px;">আপনার জাতীয় পরিচয় পত্র আছে কি? (পরিচয় পত্র থাকলে ব্যক্তিগত নথিতে ফতকপি সংযুক্ত করতে হবে)  </td>
                        <td>
                            <table class="table3" style="border-collapse: collapse;padding-top: 5px; margin-top: -12px;margin-left: -2px;margin-right: -2px;margin-bottom: -11px;" >
                                <tr>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                    <td style="padding:6px auto; border:1px solid #555;text-align: center;" width="10%">হ্যাঁ</td>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                    <td style="padding:6px auto; border:1px solid #555;text-align: center;" width="10%">না</td>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >১১</td>
                        <td style="padding-left:5px;">আপনার চারিত্রিক/ নাগরিকত্ব সনদপত্র আছে কি? (চেয়ারম্যান অথবা ওয়ার্ড কমিশনার কর্তৃক) </td>
                        <td>
                            <table class="table3" style="border-collapse: collapse; padding-top: 0px; margin-top: -11px; margin-left: -2px; margin-right: -2px;margin-bottom: -10px;" >
                                <tr>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                    <td style="padding:6px auto; border:1px solid #555;text-align: center;" width="10%">হ্যাঁ</td>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                    <td style="padding:6px auto; border:1px solid #555;text-align: center;" width="10%">না</td>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >১২</td>
                        <td style="padding-left:5px;">অত্র শহর/ এলাকায় আপনার নিকটতম আত্নীয় / অভিভাবক / পরিচিত ব্যক্তির সম্পর্ক লিখুন। </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >১৩</td>
                        <td style="padding-left:5px;">পূর্বে আপনি কোন ফৌজদারী/ দেওয়ানী মামলায় সাজাপ্রাপ্ত হয়ে ছিলেন কি? হ্যাঁ হলে লিখুন। </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >১৪</td>
                        <td style="padding-left:5px;">বর্তমানে আপনি কোন ফৌজদারী মামলায় জরিত আছেন কি? </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" >১৫</td>
                        <td style="padding-left:5px;">আপনি কখন রাষ্ট্র বিরোধী কার্যকলাপে জরিত ছিলেন কি? </td>
                        <td>
                            <table class="table3" style="border-collapse: collapse;padding-top: 0px;margin-top: -2px;margin-left: -2px;margin-right: -2px;margin-bottom: -2px;" >
                                <tr>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                    <td style="padding:6px auto; border:1px solid #555;text-align: center;" width="10%">হ্যাঁ</td>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                    <td style="padding:6px auto; border:1px solid #555;text-align: center;" width="10%">না</td>
                                    <td style="padding:6px auto; border:1px solid #555;"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br>
                <br>
                <table class="table2">
                    <tr>
                        <td>আমার জানামতে উপরোক্ত তথ্যবলী সত্য ও নির্ভুল। উপরোক্ত তথ্যবলী মিথ্যা/ ভুল প্রমানিত হলে কোম্পানী নিয়ম অনুযায়ী যে কোন আইনানুগ কার্যকরী ব্যবস্থা গ্রহণ করতে পারবে। </td>
                    </tr>
                    <tr>
                        <td><br><br><br><br><br><br><br> </td>
                    </tr>
                    <tr>
                        <td>প্রার্থীর স্বাক্ষর ও তারিখ </td>
                        <td style="text-align: right;">কর্তৃপক্ষের সীল ও স্বাক্ষর </td>
                    </tr>
                </table>
            </div>
        </div>

    </body>
</html>
