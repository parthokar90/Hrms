<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>চাকুরীর আবেদন পত্র</title>
        <style>
            @page { sheet-size: A4; }
            body{
                font-family: 'bangla', sans-serif;
                font-size: 15px;

            }
            p{  
                line-height: 18px;
            }

            .basicInfo{
                width: 100%;
            }

            .basicInfo td{
                height: 22px;
            }


            .reportHeaderArea{
                text-align: center;
            }
            .reportHeader p{
                line-height: 9px;
                padding-top: -10px;
            }
            .container{
                text-align: left;
            }

            .salary{
                text-align:left;
                width: 95%;
            }

            .head{
                font-size: 14px;
                font-weight: bold;
                text-align:left;
            }

            .appApply{
                text-align:left;
                font-size:17px;
                padding-top: -10px; 
                padding-bottom: 25px;
            }

            .content p{
                line-height: 15px;
            }

            .niyogkar p{
                line-height: 15px;
            }

            .boldText{
                font-weight: bold !important;
                font-size: 14px !important;
            }

            .reportHeaderCompany{
                font-size: 35px !important;
                padding: 0px 0px;
                padding-bottom: -18px;
            }
            .h1pad{
                padding-bottom: -10px;
            }
            #table1{
                width: 100%;
            }
            table tr td{
                padding:6px;
            }

            .basicInfo th,td{
                border:none;
            }
            .box{
                padding:5px; 
                width: 30px; 
                border:1px solid #999;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="reportHeaderArea">
                <h1 class="reportHeaderCompany">{{$companyInformation->bComName}}</h1>
                <p class="reportHeader">{{$companyInformation->bComAddress}}</p>
                <h2  style="text-decoration: underline;text-align: center;padding-top: -10px;padding-bottom: -10px;">চাকুরীর আবেদন পত্র </h2>
            </div>

            <div style="text-align: left;">
                <p>বরাবর,<br/> 
                ব্যবস্থাপনা পরিচালক/ ব্যবস্থাপক মানব সম্পদ,
                <br />
                ইউনিট নামঃ <span class="boldText"> <?php if(!empty($emp->bnName)){echo $emp->bnName;}else{ echo $emp->unitName; } ?></span></p>

                <p>বিষয়ঃ <span class="boldText"><?php if(!empty($emp->designationBangla)){echo $emp->designationBangla;}else{ echo $emp->designation; } ?></span> পদে চাকুরীর জন্য আবেদনপত্র।</p>
                <p>জনাব,</p>
                <p>
                সবিনয় বিনীত নিবেদন এই যে, বিশ্বস্ত সুত্রে জানিতে পারিলাম, আপনার প্রতিষ্ঠানে <span class="boldText"><?php if(!empty($emp->designationBangla)){echo $emp->designationBangla;}else{ echo $emp->designation; } ?></span> পদে লোক নিয়োগ করা হইবে। আমি উক্ত পদে একজন প্রার্থী/প্রার্থিনী হিসাবে আবেদন করিতেছি। নিম্নে আমার জীবন বৃত্তান্ত উল্লেখ করা হইলঃ
                </p>
                <table width="100%" class="table1">
                    <tr>
                        <td colspan="2">১। নামঃ  <span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">২। পিতা/ স্বামীর নামঃ <span class="boldText"><?php if(!empty($emp->empBnFatherName)){echo $emp->empBnFatherName;}else{ echo $emp->empFatherName; } ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">৩। মাতার নামঃ  <span class="boldText"><?php if(!empty($emp->empBnMotherName)){echo $emp->empBnMotherName;}else{ echo $emp->empMotherName; } ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">৪। স্থায়ী ঠিকানাঃ   <span class="boldText"><?php if(!empty($emp->empBnParAddress)){echo $emp->empBnParAddress;}else{ echo $emp->empParAddress; } ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">৫। বর্তমান ঠিকানাঃ  <span class="boldText"><?php if(!empty($emp->empBnCurrentAddress)){echo $emp->empBnCurrentAddress;}else{ echo $emp->empCurrentAddress; } ?></span> </td>
                    </tr>
                    <tr>
                        <td> ৬। শিক্ষাগত যোগ্যতাঃ   <span class="boldText"><?php if(!empty($emp->empBnEduQuality)){echo $emp->empBnEduQuality;}else{ echo ''; } ?></span> </td>
                        <td>সনদপত্র সংযুক্তঃ  হ্যাঁ / না    </td>
                    </tr>
                    <tr>
                        <td> ৭। জন্ম তারিখঃ <span class="boldText"><?php echo en2bnNumber(date("d-m-Y", strtotime($emp->empDOB))); ?></span>
                            
                               </td>
                        <td> ৮। বয়স: <?php   
                              $birth_date = strtotime($emp->empDOB);
                              $now = time();
                              $age = $now-$birth_date;
                              $a = $age/60/60/24/365.25;
                              echo  en2bnNumber(floor($a))." বৎসর"; 
                            ?></td>
                    </tr>
                    <tr>
                        <td> ৯। অভিজ্ঞতাঃ (যদি থাকে)  <span class="boldText"><?php if(!empty($emp->empPreExperience)){echo $emp->empPreExperience;}else{ echo ''; } ?></span>     </td>
                        <td>সনদপত্র সংযুক্তঃ হ্যাঁ / না    </td>
                    </tr>
                    <tr>
                        <td> ১০।পরিচয়কারীর নামঃ   </td>
                        <td> পদবীঃ   <br></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: auto 5px;"> পরিচয়কারীর স্বাক্ষরঃ   </td>
                    </tr>
                    <tr>
                        <td> ১১।জাতীয় পরিচয়পত্র নং: <span class="boldText"><?php echo en2bnNumber($emp->empNid); ?></span> </td>
                        <td> ১২। বায়োমেট্রিক নং: <span class="boldText">{{en2bnNumber($emp->empGlobalId)}}</span></td>
                    </tr>
                    <tr>
                        <td colspan="2">১৩। বৈবাহিক অবস্থাঃ <?php if(!empty($emp->bnMeritalStatus)){echo $emp->bnMeritalStatus;}else{ echo $emp->enMeritalStatus; } ?> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; সন্তান সংখ্যাঃ <span class="box">&nbsp;&nbsp;<?php echo ($emp->empChildrenNumber); ?>&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;ছেলে: <span class="box">&nbsp;&nbsp;<?php echo ($emp->empBoyNumber); ?>&nbsp;&nbsp;</span>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; মেয়ে: <span  class="box">&nbsp;&nbsp;<?php echo ($emp->empGirlNumber); ?>&nbsp;&nbsp;</span> </td>
                    </tr>
                </table>
                <p>অতএব, বিনীত নিবেদন এই যে, আপনি অনুগ্রহ পূর্বক উক্ত পদে আমাকে নিয়োগ করিলে কোম্পানির উন্নতিকল্পে অক্লান্ত পরিশ্রম করিতে বাধ্য থাকিব এবং অত্র প্রতিষ্ঠানের চাকুরীর বিধিমালা/ শর্তাবলী মানিয়া চলিব। উপরে প্রদত্ত সকল তথ্য নির্ভুল ও সঠিক। কোন ভুল ও অসত্য তথ্যের জন্য আমি দায়ী থাকিব।</p>
                <br><br><br>
                <table width="100%">
                    <tr>
                        <td width="50%">তারিখঃ</td>
                        <td style="text-align:right;">বিনীত নিবেদক/নিবেদিকা<br /><br />স্বাক্ষরঃ...........................</td>
                    </tr>
                </table>
            </div>
        </div>

    </body>
</html>
