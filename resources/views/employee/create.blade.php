@extends('layouts.master')
@section('title', 'Create New Employee')
@section('content')
{{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}

    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
            @if(Session::has('fileSize'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('fileSize')}}</p>
            @elseif(Session::has('message'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
            @endif
            @if ($errors->any())
                <div>
                    <ul style="list-style-type: none;padding: auto 9px;margin: auto 9px;">
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            </div>
            <div class="col-md-12">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Create</strong> New Employee</h2>
                    </div>
                    <div class="panel-body bg-white">


                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                {!! Form::open(['method'=>'POST','action'=>'EmployeeController@store','files'=>true]) !!}

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                {!! Form::label('empFirstName','Full Name',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empFirstName" class="form-control" minlength="3" placeholder="Minimum 3 characters..." required>
                                                    <i class="icon-user"></i>
                                                    <input name="empLastName" hidden="" value=" ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                <label class="control-label">Employee Id</label>
                                                <div class="append-icon">
                                                    <input type="text" name="employeeId" class="form-control" placeholder="Enter Employee Id..." required id="employeeId" >
                                                    <br>
                                                    <div id="employeeId_meg"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                <label class="control-label">Designation</label>
                                                <div class="">
                                                    {!! Form::select('empDesignationId',[''=>'Select Designation']+ $designations,null,['class'=>'form-control','required'=>'','data-search'=>'true']) !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                <label class="control-label">Gender</label>
                                                <div class="">
                                                    <select class="form-control" name="empGenderId" required >
                                                        <option value="">Select Gender</option>
                                                        <option value="1">Male</option>
                                                        <option value="2">Female</option>
                                                        <option value="3">Other</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="required control-label">Marital Status</label>
                                                <div class="option-group">
                                                    {!! Form::select('empMaritalStatusId',[''=>'Select Status']+ $maritalStatuses,null,['class'=>'form-control','required'=>'','data-search'=>'true']) !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="required control-label">Department</label>
                                                <div class="option-group">
                                                    {!! Form::select('empDepartmentId',[''=>'Select Department']+ $departments,null,['class'=>'form-control','required'=>'','data-search'=>'true']) !!}

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                <label class="control-label">Account Status</label>
                                                <div class="option-group">
                                                    <select onchange="statusFunc(this)" name="empAccStatus" class="form-control" required>
                                                        <option value="">Select Status</option>
                                                        <option selected value="1">Active</option>
                                                        <option value="0">Not Active</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="required form-label">Joining Date</label>
                                                <div class="prepend-icon">
                                                    <input type="date" name="empJoiningDate" autocomplete="off" class="form-control" placeholder="Select a date..." required>
                                                    <i class="icon-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                <label class="control-label">Role</label>
                                                @if(checkPermission(['admin']))
                                                    <div class="">
                                                        <select name="empRole" class="form-control" required>
                                                            <option value="">Select Role</option>
                                                            <option value="1">Admin</option>
                                                            <option value="2">Human Resource</option>
                                                            <option value="4">Executive</option>
                                                            <option value="5">Accountant</option>
                                                            <option selected value="6">Worker</option>
                                                        </select>
                                                    </div>
                                                @elseif(checkPermission(['hr']))
                                                    <div class="">
                                                        <select name="empRole" class="form-control" required  >
                                                            <option value="">Select Role</option>
                                                            <option value="2">Human Resource</option>
                                                            <option value="4">Executive</option>
                                                            <option value="5">Accountant</option>
                                                            <option selected value="6">Worker</option>
                                                        </select>
                                                    </div>
                                                @elseif(checkPermission(['executive']))
                                                    <div class="">
                                                        <select name="empRole" class="form-control" required  >
                                                            <option value="">Select Role</option>
                                                            {{--<option value="5">Accountant</option>--}}
                                                            <option selected value="6">Worker</option>
                                                        </select>
                                                    </div>

                                                @endif
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group required">
                                                {!! Form::label('work_group','Work Group',['class'=>'control-label']) !!}
                                                <div class="option-group">
                                                    <select class="form-control" name="work_group" required >
                                                        <option value="">Select One</option>
                                                        <option value="Staff">Staff</option>
                                                        <option value="Worker">Worker</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                     

                                    </div>


                                    <div class="row">
                                        
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Phone Number</label>
                                                <div class="append-icon">
                                                    <input type="text" name="empPhone" class="form-control" placeholder="Mobile Number...">
                                                    <i class="icon-screen-smartphone"></i>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Upload your avatar (Max Size:2mb)</label>
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">Choose File</span>
                                                        <input type="file" class="custom-file" name="empPhoto" id="avatar" accept="image/*" onchange="document.getElementById('uploader').value = this.value;">
                                                        <input type="text" class="form-control" id="uploader" placeholder="Image format should be png/jpg/jpeg" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Email Address</label>
                                                <div class="append-icon">
                                                    <input type="email" name="empEmail" autocomplete="off" class="form-control" placeholder="Enter your email...">
                                                    <i class="icon-envelope"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Password</label>
                                                <div class="append-icon">
                                                    <input type="password" name="empPassword" autocomplete="new-password" id="empPassword" class="form-control" placeholder="Between 4 and 16 characters">
                                                    <i class="icon-lock"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empCurrentAddress','Current Address',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <textarea name="empCurrentAddress" rows="3" class="form-control" placeholder="Write your current address... "></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empParAddress','Permanent Address',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <textarea name="empParAddress" rows="3" class="form-control" placeholder="Write your permanent address... "></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-sm-6">
                                            
                                            <div class="form-group">
                                                {!! Form::label('empFatherName',"Father's Name",['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empFatherName" class="form-control" placeholder="Father's Name" >
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empMotherName',"Mother's Name",['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empMotherName" class="form-control" placeholder="Mother's Name" >
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empReligion','Religion',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empReligion" class="form-control" placeholder="Religion" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empNid','National Id Number',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empNid" class="form-control" placeholder="National Id Number" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empNationalityId','Nationality',['class'=>'control-label']) !!}
                                                <div class="option-group">
                                                    {!! Form::select('empNationalityId',[''=>'Select Nationality']+ $nationalities,null,['class'=>'form-control','id'=>'language','data-search'=>'true']) !!}

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empGlobalId','Global Id',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empGlobalId" class="form-control" placeholder="Global Id">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empSection','Section',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empSection" placeholder="Section" class="form-control" id="empSection" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empBloodGroup','Blood Group',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <select id="empBloodGroup" name="empBloodGroup" class="form-control">
                                                        <option value="">Select a Blood Group</option>
                                                            <option value="A+">A+</option>
                                                            <option value="B+">B+</option>
                                                            <option value="AB+">AB+</option>
                                                            <option value="O+">O+</option>
                                                            <option value="A-">A-</option>
                                                            <option value="B-">B-</option>
                                                            <option value="AB-">AB-</option>
                                                            <option value="O-">O-</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empCardNumber','Card Number',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" id="empCardNumber" name="empCardNumber" class="form-control" minlength="10"  placeholder="Card Number">
                                                    <div id="empCardNumber_meg"></div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('unit_id','Unit Name',['class'=>'control-label']) !!}
                                                <div class="option-group">
                                                    {!! Form::select('unit_id',[''=>'Select Unit']+ $units,null,['class'=>'form-control','data-search'=>'true']) !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('floor_name','Floor',['class'=>'control-label']) !!}
                                                <div class="option-group">
                                                    {!! Form::select('floor_name',[''=>'Select Floor']+ $floors,null,['class'=>'form-control floor','data-search'=>'true']) !!}

                                                </div>
                                            </div>
                                        </div>



                                        <div class="col-sm-6">
                                            {{--<div class="form-group">--}}
                                                {{--{!! Form::label('line_id','Line Number',['class'=>'control-label']) !!}--}}
                                                {{--<div class="option-group">--}}
                                                    {{--<select class="form-control floor_lines" name="line_id"  data-search="true"></select>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                            <div class="form-group">
                                                {!! Form::label('line_id','Line Number',['class'=>'control-label']) !!}
                                                <div class="option-group">
                                                    <select class="form-control floor_lines">
                                                        <option value="">Select Floor First</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empEcontactName','Emergency Contact Name',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empEcontactName" class="form-control" placeholder="Emergency Contact Name" >
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('emergencyPhone','Emergency Phone',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="emergencyPhone" class="form-control" placeholder="Emergency Phone" >
                                                    <i class="icon-screen-smartphone"></i>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('emergency_contact_relation','Emergency Contact Relation',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="emergency_contact_relation" placeholder="Emergency Contact Relation" class="form-control">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="required form-label">Attendance Bonus</label>
                                                <div class="prepend-icon">
                                                    <select name="empAttBonusId" required="" class="form-control">
                                                            <option selected="" value="0">No</option>
                                                        @foreach($attendance_bonus as $ab)
                                                            <option value="{{$ab->id}}">{{$ab->bTitle}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('emergencyAddress','Emergency Contact Address',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <textarea name="emergencyAddress" rows="5" class="form-control" placeholder="Write emergency address... "></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                <label class="control-label">OT Status</label>
                                                <select class="form-control" name="empOTStatus" >
                                                    <option value="">Select one</option>
                                                    <option selected="" value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                            
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('salary_type','Salary Type',['class'=>'control-label']) !!}
                                                <div class="option-group">
                                                    <select class="form-control" name="salary_type">
                                                        <option value="">Salary Type</option>
                                                        <option selected value="Monthly">Monthly</option>
                                                        <option value="Weekly">Weekly</option>
                                                        <option value="Piecewise">Piecewise</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('payment_mode','Payment Mode',['class'=>'control-label']) !!}
                                                <div class="option-group">
                                                    <select class="form-control" name="payment_mode">
                                                        <option selected value="Cash">Cash</option>
                                                        <option value="Bank">Bank</option>
                                                        <option value="bKash">bKash</option>
                                                        <option value="Rocket">Rocket(DBBL)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('bank_account',"A/C No",['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="bank_account" class="form-control" placeholder="Account Number">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('bank_info','Account Information',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <textarea name="bank_info" rows="3" class="form-control" placeholder="Write your account details... "></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="form-label">Date Of Birth</label>
                                                <div class="prepend-icon">
                                                    <input type="date" name="empDOB" autocomplete="off" class="form-control" placeholder="Select a date...">
                                                    <i class="icon-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('reference_type','Reference Type',['class'=>'control-label']) !!}
                                                <div class="option-group">
                                                    <select class="form-control" name="reference_type">
                                                        <option value="None">None</option>
                                                        <option value="Internal">Internal</option>
                                                        <option value="Others">Others</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('reference_details','Reference Details',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <textarea id='reference_details' name="reference_details" rows="3" class="form-control" placeholder="Write the reference details... "></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('skill_level',"Skill Level/Process",['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="skill_level" class="form-control" placeholder="Skill Level">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('probation_period',"Probation Period (Enter the month number only)",['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="number" name="probation_period" maxlength="2" class="form-control" placeholder="By default probation period is 6 Months">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="form-label">Date of Discontinuation</label>
                                                <div class="prepend-icon">
                                                    <input type="text" id='date_of_discontinuation' autocomplete="off" name="date_of_discontinuation" disabled="" class="date-picker form-control" placeholder="Select a date...">
                                                    <i class="icon-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('reason_of_discontinuation','Reason of Discontinuation',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <textarea id='reason_of_discontinuation' name="reason_of_discontinuation" rows="3" disabled class="form-control" placeholder="Write the reason for discontinuation... "></textarea>
                                                </div>
                                            </div>

                                        </div>


                                    </div>

                                    <div class="text-center  m-t-20">
                                        <button type="submit" id="savebtn" class="btn btn-embossed btn-primary">Save Information</button>
                                        <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
//        function lineFunc(that) {
//            alert(that.value);
//
//        }
        $(document).ready(function () {
            $("select.floor").change(function () {
                var floor= $(".floor option:selected").val();
                if(!floor){
                    var rr='<select class="form-control floor_lines" name="line_id">'+
                    '<option value="">Select Floor First</option>'+

                    '</select>';
                    $('.floor_lines').html(rr);
                }
                else {
                    $.ajax({
                        url: 'floor/line/' + floor,
                        method: 'get',
                        dataType: 'html',
                        success: function (response) {
                            $('.floor_lines').html(response);
                        }
                    });
                }
            });

            $("#employeeId").keyup(function () {
                var employeeId= $("#employeeId").val();
            
                $.ajax({
                    url: 'employee_id_check/' + employeeId,
                    method: 'get',
                    dataType: 'html',
                    success: function (response) {
                        if(response>0){
                            $('#employeeId_meg').html("<span style='color:red;'><b>Employee ID already exits.</b></span>");
                            $('#savebtn').prop("disabled",true);
                        }else{
                            $('#employeeId_meg').html('');
                            $('#savebtn').prop("disabled",false);
                        }
                    }
                });
            });


            $("#empCardNumber").keyup(function () {
                var empCardNumber= $("#empCardNumber").val();
            
                $.ajax({
                    url: 'employee_card_number_check/' + empCardNumber,
                    method: 'get',
                    dataType: 'html',
                    success: function (response) {
                        if(response>0){
                            $('#empCardNumber_meg').html("<span style='color:red;'><b>Employee card number already exits.</b></span>");
                            $('#savebtn').prop("disabled",true);
                        }else{
                            $('#empCardNumber_meg').html('');
                            $('#savebtn').prop("disabled",false);
                        }
                    }
                });
            });

        });

        function statusFunc(that) {
            if(that.value==='0'){
                document.getElementById('date_of_discontinuation').disabled=false;
                document.getElementById('reason_of_discontinuation').disabled=false;
            }
            else {
                document.getElementById('date_of_discontinuation').disabled=true;
                document.getElementById('reason_of_discontinuation').disabled=true;

            }

        }

        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 9000);

    </script>

    <script type="text/javascript">

        var url = "{{ route('employee.section.autocomplete.ajax') }}";

        $('#empSection').typeahead({

            source:  function (query, process) {

            return $.get(url, { query: query }, function (data) {

                    return process(data);

                });

            }

        });

    </script>
@include('include.copyright')

@endsection