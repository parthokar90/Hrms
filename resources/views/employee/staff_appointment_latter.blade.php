<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
        'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>কর্মকর্তা/ কর্মচারী নিয়োগপত্র</title>
    <style>
        @page { sheet-size: A4; }
        body{
            font-family: 'bangla', sans-serif;
            font-size: 14px;

        }
        p{  
            line-height: 1px;
        }

        .basicInfo{
            width: 100%;
        }

        .basicInfo td{
            height: 25px;
        }


        .reportHeaderArea{
            text-align: center;
        }
        .reportHeader p{
            line-height: 10px;
        }
        .container{
            text-align: left;
        }
        .salary{
            text-align:left;
            width: 800px;
        }

        .appApply{
            text-align:left;

        }
        .appApply p{
            line-height: 18px;

        }
        .pheight{
            line-height: 18px;

        }
    </style>
</head>
<body>

    <div class="container">
        <div class="reportHeaderArea">
            <h2 class="reportHeaderCompany">{{$companyInformation->company_name}}</h2>
            <p class="reportHeader">{{$companyInformation->company_address1}}</p>
            <p class="reportHeader">ইমেইলঃ {{$companyInformation->company_email}}</p>
            <p class="reportHeader">ফোনঃ {{$companyInformation->company_phone}}</p>
            <h2 style="text-decoration:underline;"><b>কর্মকর্তা/ কর্মচারী নিয়োগপত্র</b></h2>
        </div>

        <div style="text-align: center">
            @foreach($employee as $emp)
            <table class="basicInfo">
                <tr>
                    <td style="font-size:16px;"><b> কারখানার নামঃ <span class="boldText"> <?php if(!empty($emp->bnName)){echo $emp->bnName;}else{ echo $emp->unitName; } ?></span></b></td>
                </tr>
                <tr>
                    <td colspan="2"> নামঃ   <span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span></td>
                </tr>
                <tr>
                    <td> পিতার নামঃ <span class="boldText"><?php if(!empty($emp->empBnFatherName)){echo $emp->empBnFatherName;}else{ echo $emp->empFatherName; } ?></span></td>
                    <td> মাতার নামঃ <span class="boldText"><?php if(!empty($emp->empBnMotherName)){echo $emp->empBnMotherName;}else{ echo $emp->empMotherName; } ?></span></td>
                </tr>
                <tr>
                    <td colspan="2"> বর্তমান ঠিকানাঃ <span class="boldText"><?php if(!empty($emp->empBnCurrentAddress)){echo $emp->empBnCurrentAddress;}else{ echo $emp->empCurrentAddress; } ?></span></td>
                </tr>
                <tr>
                    <td> যোগদানের তারিখঃ <?php echo en2bnNumber(date("d-m-Y", strtotime($emp->empJoiningDate))); ?></td>
                    <td> জন্ম তারিখঃ <?php echo en2bnNumber(date("d-m-Y", strtotime($emp->empDOB))); ?></td>
                </tr>
            </table>
            <?php 
            $probation_period = $emp->probation_period;
            $designation = $emp->designation;
            $designationBangla = $emp->designationBangla;
            ?>
            @endforeach
            <?php 
            $empBnFullName=$emp->empBnFullName;
            $name=$emp->empFirstName ." ".$emp->empLastName;
            ?>

            <h2 style="text-align:center;">শর্ত ও নিয়মাবলীঃ</h2>
            <div class="appApply">
                <p> <strong>১। নিয়োগের কার্যকারিতাঃ</strong>
                    <br />আপনার আবেদনপত্র ও সাক্ষাৎকারের পরিপেক্ষিতে আপনাকে  <span class="boldText"><?php if(!empty($designationBangla)){echo $designationBangla;}else{ echo  $designation; } ?> পদে অবেক্ষাধীন (Probationar) কর্মকর্তা/কর্মচারী হিসাবে নিযুক্ত করা হল।
                    উক্ত সময়কাল  <?php echo en2bnNumber($probation_period);?> মাস হবে । উল্লেখিত সময়ে আপনার কাজের মান সন্তোষজনক না হলে আপনাকে বিনা নোটিশে চাকুরী হতে অপসারণ করা হবে।</p>
                </div>
                <div class="salary">
                    <p>২। বেতনঃ আপনার দক্ষতা ও যোগ্যতা অনুযায়ী নিম্মরুপ বেতন নির্ধারণ হবে।</p>
                    @foreach($salary as $sl)
                    <center>
                        <table style="margin-left:50px;" width="90%">

                            <tr>
                                <td width="20%">ক) মূল বেতন</td>
                                <td width="20%">: {{en2bnNumber($sl->basic_salary)}} টাকা</td>
                                <td></td>
                                <td>খ) বাড়ী ভাড়া টাকা</td>
                                <td>: {{en2bnNumber($sl->house_rant)}} টাকা <small>(মূল বেতনের ৫০%)</small>    </td>
                            </tr>
                            <tr>
                                <td>গ) চিকিৎসা ভাতা</td>
                                <td>: {{en2bnNumber($sl->medical)}} টাকা</td>
                                <td></td>
                                <td>ঘ) যাতায়েত ভাতা</td>
                                <td>: {{en2bnNumber($sl->transport)}} টাকা</td>
                            </tr>
                            <tr>
                                <td>ঙ) খাদ্য ভাতা</td>
                                <td>: {{en2bnNumber($sl->food)}} টাকা</td>
                                <td></td>
                                <td>চ) অন্যান্য ভাতা</td>
                                <td>: {{en2bnNumber($sl->other)}} টাকা</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="font-size:18px;">সর্বসাকুল্যে মোট বেতন : {{en2bnNumber($sl->total_employee_salary)}} টাকা </td>
                                <td></td>
                                <td colspan="2" style="font-size:13px;"><span class="boldText">গ্রেডঃ</span> {{en2bnNumber($sl->gradeName)}} </td>
                            </tr>
                        </table>
                    </center>
                    @endforeach
                    <br />
                    <p>৩। চাকুরীতে যোগদানঃ চাকুরীতে যোগদানকালে নিম্মুক্ত কাগজপত্র সহ নির্ধারিত আবেদন ফরম পূরণ করে মানব সম্পদ বিভাগে জমা দিতে হবে।</p>

                    <table style="margin-left:35px;" width="98%">
                        <tr>
                            <td>ক) ০৪ (চার) কপি পাসপোর্ট সাইজ ছবি এবং ০২(দুই) কপি স্ট্যাম্প সাইজ ছবি </td>
                            <td>খ) চেয়ারম্যান/ কমিশনার /কাউন্সিলর কর্তৃক প্রদত্ত নাগরিকত্ব সনদ ।</td>
                        </tr>
                        <tr>
                            <td>গ) শিক্ষাগত যোগ্যতার সনদ বা স্কুল সার্টিফিকেট।</td>
                            <td>ঘ) বয়স নিরূপণ সনদ বা মেডিকেল চেক-আপ রিপোর্ট।</td>
                        </tr>
                        <tr>
                            <td>ঙ) জাতীয় ভোটার পরিচয় পত্রের ফটোকপি। </td>
                            <td></td>
                        </tr>
                    </table>
                    <br>
                    <p> <strong>৪। চাকরী ছাড়ার নিয়মঃ</strong></p>
                    <table style="margin-left:35px;" width="95%">
                        <tr>
                            <td>ক) স্বেচ্ছায় চাকুরীর অবসান করতে চাইলে নিয়মানুযায়ী চাকুরী ছাড়ার ০২ মাস (৬০ দিন) পূর্বে কর্তৃপক্ষকে লিখিত নোটিশ প্রদান করতে হবে। পক্ষান্তরে, কর্তৃপক্ষ আপনার চাকুরী অবসান করতে চাইলে ০৪ মাস (১২০ দিন) পূর্বে লিখিত নোটিশ প্রদান করবে। অন্যথায় নোটিশ মেয়াদের সমপরিমাণ মজুরীর অর্থ উভয়ই পক্ষ প্রদান করে ইহা সম্পাদন করতে পারবে। </td>
                        </tr>
                        <tr>
                            <td>খ) অসুস্থতার কারনে চাকুরী ছেড়ে দিতে হলে মেডিকেল রিপোর্ট দাখিল করতে হবে।</td>
                        </tr>

                        <tr>
                            <td>গ) চাকুরী ছাড়ার সময় কোম্পানীর প্রদত্ত সরঞ্জাম অর্থাৎ আই ডি কার্ড, ইউনিফর্ম, কাটার, টেপ এবং আরো অন্যান্য সরঞ্জামাদি (যদি থাকে) কোম্পানির দায়িত্ব প্রাপ্ত লোকের কাছে জমা দিয়ে কারখানা থেকে ক্লিয়ারেন্স নিতে হবে। </td>
                        </tr>

                    </table>

                    <br>

                    <p> <strong>৫। ছুটিঃ</strong></p>
                    <table style="margin-left:35px;" width="95%">
                        <tr>
                            <td>ক) সাপ্তাহিক ছুটি সপ্তাহে ০১(এক) দিন। খ) উৎসব ছুটি বছরে ১১(এগার) দিন(পূর্ণ বেতনে) গ) নৈমিত্তিক ছুটি বছরে ১০(দশ) দিন (পূর্ণ বেতনে) ঘ) অসুস্থতা ছুটি বছরে ১৪ (চৌদ্দ) দিন (পূর্ণ বেতনে) ঙ) অর্জিত ছুটি ১৮(আঠার) কর্ম দিবসে ০১ (এক) দিন। চাকুরীর সময়কাল কমপক্ষে ০১ (এক) বৎসর পূর্ণ হলে। চ) মাতৃকালীন ছুটি ১৬ সপ্তাহ (১১২ দিন) (শুধুমাত্র মহিলাদের ক্ষেত্রে প্রযোজ্য) (আইনানুগ ও নগদে) চাকুরীর সময়কাল কমপক্ষে ০৬(ছয়) মাস পূর্ণ হলে উক্ত ছুটি প্রযোজ্য হবে/ কিন্তু দুই বা ততোধিক সন্তান জীবিত থাকলে কোন সুবিধা প্রযোজ্য হবে না, তবে কোন ছুটি পাওয়ার যোগ্য হলে তা পাবে।</td>
                        </tr>
                    </table>

                    <br>

                    <p> <strong>৬। ছুটির নিয়মাবলীঃ</strong></p>
                    <table style="margin-left:35px;" width="95%">
                        <tr>
                            <td>ক) কোন কর্মকর্তা/কর্মচারীর ছুটির প্রয়োজন হলে অবশ্যই কোম্পানির নির্ধারিত ছুটির আবেদনপত্র পূরণ করে এইচ.আর(HR) বা মানব সম্পদ বিভাগে জমা দিতে হবে। </td>
                        </tr>
                        <tr>
                            <td>খ) ছুটির আবেদন মঞ্জুর হলেই কেবলমাত্র কোন কর্মকর্তা/কর্মচারী ছুটি ভোগ করতে পারবে। </td>
                        </tr>
                        <tr>
                            <td>গ) কোন কর্মকর্তা/কর্মচারী কর্তৃপক্ষের বিনা অনুমতিতে কারখানার ১০(দশ) দিনের বেশি অনুপস্থিত থাকলে তার বিরুদ্দে আইনানুগ ব্যবস্থা গ্রহন করা হবে।</td>
                        </tr>
                    </table>

                    <br>

                    <p> <strong>৭। সুবিধাঃ</strong></p>
                    <table style="margin-left:35px;" width="95%">
                        <tr>
                            <td>ক) ১১১ এর উপবিধি (৫) অনুযায়ী কর্মকর্তা/কর্মচারীদের নিরবিচ্ছিন্ন ভাবে ০১(এক) বৎসর চাকুরীর সময়কাল পূর্ণ হলে বৎসরে দুটি উৎসব ভাতা প্রদান করা হয়।</td>
                        </tr>
                        <tr>
                            <td>খ) বিনা খরচে কারখানার ডাক্তার এবং নার্সের মাধ্যমে চিকিৎসা প্রদান করা হয়।</td>
                        </tr>
                        <tr>
                            <td>গ) সকল কর্মকর্তা/কর্মচারীদের জন্য গ্রুপ ইন্সুরেন্স এর ব্যবস্থা করা হয় (চাকুরীর সময়কাল কমপক্ষে ০৩(তিন) মাস পূর্ণ হলে)।</td>
                        </tr>
                        <tr>
                            <td>ঘ) সকল কর্মকর্তা/কর্মচারীদের বাৎসরিক যোগ্যতার ভিত্তিতে বেতন বৃদ্ধি ও পদোন্নতি প্রদান করা হয়।</td>
                        </tr>
                    </table>

                    <br>

                    <p class="pheight"> <strong>৮। বাংলাদেশ সরকারের শ্রম আইন ২০০৬ এর ধারা ২৭ এর (৪) মোতাবেক, যে ক্ষেত্রে এই ধারার অধীনে কোন স্থায়ী শ্রমিক চাকুরী হইতে ইস্তফা দেন সে ক্ষেত্রে, উক্ত শ্রমিককে <br /> ক্ষতিপূরণ হিসাবে তার প্রত্যেক সম্পূর্ণ বৎসরের চাকুরীর জন্য নিম্মোক্ত সুবিধা প্রদান করা হয়ঃ</strong></p>
                    <table style="margin-left:35px;" width="95%">
                        <tr>
                            <td>ক) যদি তিনি ০৫(পাঁচ) বৎসর বা তদূর্ধ্ব, কিন্তু ১০(দশ) বৎসরের কম সময় অবিচ্ছিন্নভাবে মালিকের অধীনে চাকরী করেন তাহলে, ১৪(চৌদ্দ) দিনের মজুরী পাবে।</td>
                        </tr>
                        <tr>
                            <td>খ) যদি তিনি ১০(দশ) বৎসর বা তদূর্ধ্ব সময় মালিকের অধীনে অবিচ্ছিন্নভাবে চাকুরী করেন তা হলে, ৩০ (ত্রিশ) দিনের মজুরী পাবে।</td>
                        </tr>
                    </table>

                    <br>

                    <p class="appApply">৯। আপনার চাকুরী অত্র কারখানার যে কোন সেকশনে বা কোম্পানির অধীনস্ত যে কোন কারখানায় বদলিযোগ্য।</p>
                    <p class="pheight">
                        আমি এই মর্মে অঙ্গীকার করছি যে, আমি কারো দ্বারা প্ররোচিত না হয়ে, কারো দ্বারা প্রলুব্ধিত না হয়ে, কারো কোনরূপ জোর-জবরদস্তি ছাড়াই, স্বেচ্ছায়, স্বজ্ঞানে ও নিজ ইচ্ছায় চাকুরীতে যোগদান করছি। আমি আরো অঙ্গীকার করছি যে, অত্র প্রতিষ্ঠানের সকল শর্ত ও নিয়ম কানুন মেনে চলব।
                    </p>

                    <p> <strong>আমি <?php if(!empty($empBnFullName)){echo $empBnFullName;}else{ echo $name; } ?> স্বজ্ঞানে এ চুক্তিপত্রের সকল শর্ত জেনে, বুঝে ও মেনে স্বাক্ষর করলাম এবং এর একটি কপি বুঝে পেলাম।
                    </strong></p>

                    <table style="padding-top:55px;" width="95%">
                        <tr>
                            <td width="70%"></td>
                            <td  style=" text-align:center;">
                                <hr>
                                কর্মকর্তা/কর্মচারীর স্বাক্ষর ও তারিখ
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <table  width="100%">
                        <tr>
                            <td style="text-align:center;" width="25%">
                                <p style="border-top:1px solid #999;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;স্বাক্ষর&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
                                
                                পার্সোনেল সেকশন
                            </td>
                            <td style="text-align:center;" width="25%">
                                <p style="border-top:1px solid #999;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;স্বাক্ষর&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
                                মানব সম্পদ / প্রশাসন

                            </td>
                            
                            <td style="text-align:center;" width="25%">
                                <p style="border-top:1px solid #999;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;স্বাক্ষর&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
                                চীফ অপারেটিং অফিসার 
                            </td>
                            <td style="text-align:center;" width="25%">
                                <p style="border-top:1px solid #999;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;স্বাক্ষর&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
                                পরিচালক
                            </td>
                        </tr>
                    </table>


                </div>
            </div>
        </div>

    </body>
    </html>
