<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
<!doctype html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>বয়স ও সক্ষমতার প্রত্যয়ন পত্র</title>
        <style>
            @page { sheet-size: A4; }
            body{
                font-family: 'bangla', sans-serif;
                font-size: 15px;

            }
            p{  
                line-height: 18px;
            }

            .basicInfo{
                width: 100%;
            }

            .basicInfo td{
                height: 22px;
            }


            .reportHeaderArea{
                text-align: left;
            }
            .reportHeader p{
                line-height: 9px;
                padding-top: -10px;
            }
            .reportHeader1 p{
                line-height: 10px;
                font-size: 12px;
                padding-top: -10px;
            }
            .container{
                text-align: left;
            }

            .salary{
                text-align:left;
                width: 95%;
            }

            .head{
                font-size: 14px;
                font-weight: bold;
                text-align:left;
            }

            .appApply{
                text-align:left;
                font-size:17px;
                padding-top: -10px; 
                padding-bottom: 25px;
            }

            .content{
                line-height: 30px;
            }

            .niyogkar p{
                line-height: 15px;
            }

            .boldText{
                font-weight: bold !important;
                font-size: 15px !important;
                font-weight: 600;
            }

            .reportHeaderCompany{
                font-size: 22px !important;
                padding: 0px 0px;
                padding-bottom: -18px;
                font-weight: 800;
            }
            .h1pad{
                padding-bottom: -10px;
            }
            #table1{
                width: 100%;
            }
            table tr td{
                padding:6px;
            }

            .basicInfo th,td{
                border:none;
            }
            .table_heading{
                width: 100%;
                font-size: 11px;
                font-weight: 500;
            }
            .table_heading1{
                width: 100%;
                border:1px;
                border-collapse: collapse;
            }
            .table_heading1 tr td{
                border:0.5px solid #444; 
            }
            .table_heading1_tr{
                text-align: center;
                font-size: 20px;
                font-weight: 700;
            }
            .h1{
                font-size: 32px;
            }
            .table_heading tr td{
                border:none; 
            }

            .table_heading2{
                width: 100%;
                font-size: 13px;
                font-weight: 600;
            }

            .table_heading2 tr td{
                border:none; 
            }

        </style>
    </head>
    <body>

    <div class="container">
        <table class="table_heading">
            <tr>
                <td width="70%">
                    <div class="reportHeaderArea" >
                            <?php $signature="logo/clogo.png"; ?>
                            <img height="80px" width="80px" src="{{asset($signature)}}">
                            <br /><br />
                        <p class="reportHeader1">
                            Dr. Nazme Zakia (Jui)
                            <br >
                            Reg: 66768, Medical Officer,
                            <br >
                            Fin-Bangla Apparels Ltd.
                            <br >
                            PINAKI GROUP
                        </p>
                    </div>
                </td>

                <td>
                    <div class="reportHeaderArea">
                        <h1 class="reportHeaderCompany">
                            PINAKI GROUP</h1>
                        <p class="reportHeader">
                        
                        29, Gareb-E-Newaz Avenue
                        <br>
                        Sector-11, Uttara, 
                        <br>
                        Dhaka-1230, Bangladesh
                        <br>
                        Phone: +88-02-8931996, 8931998
                        <br>
                        Fax: +88-02-8920831
                        <br>
                        Email: info@pinakigroup.com
                        </p>
                    </div>
                </td>
            </tr>
        </table>
        <hr style="padding: 0;margin:0;">
        <table class="table_heading">
            <tr>
                <td><center><h1 class="h1">বয়স ও সক্ষমতার প্রত্যয়ন পত্র </h1></center></td>
            </tr>
        </table>
        <table class="table_heading1">
            <tr>
               <td class="table_heading1_tr" colspan="2">বয়স ও সক্ষমতার প্রত্যয়ন পত্র</td>
               <td class="table_heading1_tr" colspan="2">বয়স ও সক্ষমতার প্রত্যয়ন পত্র</td>
            </tr>
            <tr>
                <td colspan="2" class="content" width="50%" >
                        ০১। ক্রমিক নং:-
                    <br>
                        তারিখ: 
                    <br>
                    <br>
                        ০২। নামঃ  <span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span>
                    <br>
                        ০৩। পিতার নামঃ <span class="boldText"><?php if(!empty($emp->empBnFatherName)){echo $emp->empBnFatherName;}else{ echo $emp->empFatherName; } ?></span>
                    <br>
                        ০৪। মাতার নামঃ <span class="boldText"><?php if(!empty($emp->empBnMotherName)){echo $emp->empBnMotherName;}else{ echo $emp->empMotherName; } ?></span>
                    <br>
                        ০৫। লিঙ্গঃ 
                        <?php 
                        if($emp->empGenderId==1){ echo "পুরুষ";}
                        if($emp->empGenderId==2){ echo "মহিলা";} 
                        if($emp->empGenderId==3){ echo "অন্যান্য";} 

                        ?>
                    <br>
                        ০৬। স্থায়ী ঠিকানা:  <span class="boldText"><?php if(!empty($emp->empBnParAddress)){echo $emp->empBnParAddress;}else{ echo $emp->empParAddress; } ?></span>
                    <br>
                        ০৭। অস্থায়ী/যোগাযোগের ঠিকানাঃ <span class="boldText"><?php if(!empty($emp->empBnCurrentAddress)){echo $emp->empBnCurrentAddress;}else{ echo $emp->empCurrentAddress; } ?></span>
                    <br>
                        ০৮। জন্ম সনদ/ শিক্ষা সনদ অনুসারে বয়স/জন্ম তারিখঃ 
                        <br> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span  class="boldText"><?php echo en2bnNumber(date("d-m-Y", strtotime($emp->empDOB))); ?></span>
                          

                    <br>
                    <br>
                        ০৯। দৈহিক সক্ষমতাঃ
                    <br>
                        ১০। সনাক্তকরণ চিহ্নঃ
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <table class="table_heading2" >
                        <tr>
                            <td style="border-top:1px solid #111;">
                                <center>সংশ্লিষ্ট ব্যক্তির স্বাক্ষর/টিপসহি</center> 
                            </td>
                            <td style="border-top:1px solid #111;">
                                <center>রেজিষ্টার্ড চিকিৎসকের স্বাক্ষর</center>
                            </td>
                        </tr>
                    </table>
                    
                </td>
                <td colspan="2" class="content" width="50%"  >
                        ক্রমিক নং:-
                    <br>
                        তারিখ: 
                    
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    আমি এই মর্মে প্রত্যয়ন করিতেছি যে,
                    <br>
                        নামঃ  <span class="boldText"><?php if(!empty($emp->empBnFullName)){echo $emp->empBnFullName;}else{ echo $emp->empFirstName ." ".$emp->empLastName; } ?></span>
                    <br>
                        পিতার নামঃ <span class="boldText"><?php if(!empty($emp->empBnFatherName)){echo $emp->empBnFatherName;}else{ echo $emp->empFatherName; } ?></span>
                    <br>
                       মাতার নামঃ <span class="boldText"><?php if(!empty($emp->empBnMotherName)){echo $emp->empBnMotherName;}else{ echo $emp->empMotherName; } ?></span>
                    <br>
                       ঠিকানা:  <span class="boldText"><?php if(!empty($emp->empBnParAddress)){echo $emp->empBnParAddress;}else{ echo $emp->empParAddress; } ?></span>
                    <br>
                      কে আমি পরীক্ষা করিয়াছি। 
                    <br>
                    <br>
                    তিনি প্রতিষ্ঠানে নিযুক্ত হইতে ইচ্ছুক, এবং আমার পরীক্ষা হইতে এইরূপ পাওয়া গিয়াছে যে তাহার বয়স ............... বৎসর এবং তিনি প্রতিষ্ঠানে প্রাপ্ত বয়স্ক / কিশোর হিসাবে নিযুক্ত হইবার যোগ্য।
                    <br>
                    <br>
                    তাহার সনাক্তকনের চিহ্নঃ 

                    <br>
                    <br>
                    <br>
                    <table class="table_heading2">
                        <tr>
                            <td style="border-top:1px solid #111;">
                                <center>সংশ্লিষ্ট ব্যক্তির স্বাক্ষর/টিপসহি</center> 
                            </td>
                            <td style="border-top:1px solid #111;">
                                <center>রেজিষ্টার্ড চিকিৎসকের স্বাক্ষর</center>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        </table>
        <div style="text-align: left;">
            
        </div>
    </div>

    </body>
</html>
