@extends('layouts.master')
@section('title', 'Employee List')
@section('content')
{{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}
<div class="page-content">
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header">
                    <div class="row">
                        <div class="col-md-5 col-lg-5">
                            <h3 ><i class="fa fa-users "></i> <strong>Employee </strong> List</h3>
                            
                        </div>
                        <div class="col-md-7 col-lg-7" style="margin-top:8px;">
                            <a href="{{route('employee.create')}}" class="btn btn-primary btn-round btn-sm"  style="float:right;" ><i class="fa fa-plus"></i> Add New Employee</a>
                            <a href="{{route('employee.index')}}" class="btn btn-info btn-round btn-sm"  style="float:right;" ><i class="fa fa-list"></i> List View</a>
                            <a href="#"  data-toggle="modal" data-target="#filter" class="btn btn-danger btn-round btn-sm"  style="float:right;" ><i class="fa fa-filter"></i> Filter </a>
                            <a href="#"  data-toggle="modal" data-target="#search_employee" class="btn btn-success btn-round btn-sm"  style="float:right;" ><i class="fa fa-search"></i> Search Employee </a>
                        </div>


                    </div>

                </div>
                <div class="panel-content grid-view">
                        <div class="row">
                            @foreach($employees as $employee)
                                <div class="col-lg-4 col-sm-6">
                                   <a href="{{route('employee.show',$employee->id)}}" title="View Employee Details" target="_BLANK" >
                                  <div class="panel widget-member grid_view_employee my_panel_22 grid_view_employee_s" style="">
                                    <div class="row">
                                      <div class="col-xs-3">
                                      <img class="pull-left img-responsive" src="
                                            @if($employee->empPhoto ==null)
                                        @if($employee->empGenderId==1 || $employee->empGenderId==3)
                                        {{asset("Employee_Profile_Pic/male.jpg")}}
                                        @endif
                                        @if($employee->empGenderId==2)
                                        {{asset("Employee_Profile_Pic/female.jpg")}}
                                        @endif

                                        @endif
                                        @if($employee->empPhoto!=null)
                                        {{asset("Employee_Profile_Pic/".$employee->empPhoto)}}
                                        @endif
                                        ">
                                      </div>
                                      <div class="col-xs-9">
                                        <h3 class="m-t-0 member-name"><strong>{{$employee->empFirstName ." ".$employee->empLastName}}</strong></h3>
                                        <p class="member-job">
                                        @if($employee->departmentName!=null)
                                            <i class="fa fa-server c-gray-light p-r-10 grid_view_employee_fa"></i>  <b>{{$employee->departmentName}}</b>
                                        @else
                                            &nbsp;
                                        @endif
                                        </p>
                                        <p class="member-job"><i class="fa fa-anchor c-gray-light p-r-10 grid_view_employee_fa" ></i> <b>{{$employee->designation}}</b></p>
                                        <div class="row">
                                          <div class="col-xlg-6 col-lg-12 col-sm-6">
                                            <p><i class="fa fa-credit-card c-gray-light p-r-10 grid_view_employee_fa"></i> {{$employee->employeeId}}</p>
                                            <p><i class="fa fa-calendar c-gray-light p-r-10 grid_view_employee_fa" ></i> {{\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y-m-d')}}</p>
                                          </div>
                                          <div class="col-xlg-6 col-lg-12 col-sm-6 align-right">
                                            <p><i class="fa fa-phone c-gray-light p-r-10 grid_view_employee_fa" ></i> {{$employee->empPhone}}</p>
                                            <p><i class="fa fa-codepen c-gray-light p-r-10 grid_view_employee_fa" ></i> {{$employee->empSection}}</p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  </a>
                                </div>

                            @endforeach

                        <div class="col-md-12 col-lg-12 col-sm-12">
                        <hr>
                            <center> {{$employees->links()}}</center>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="filter" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Filter Data</strong></h4>
            </div>
            <div class="modal-body" style="border-top:1px solid #ddd;">
             <form action="{{url('/employee_grid_filter')}}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                  <label for="departmentId">Employee Department</label>
                  <select name="departmentId" id="departmentId" class="form-control" >
                    <option value="">All Department</option>
                    @foreach($departments as $department)

                    <?php if(isset($request)&&!empty($request)){ ?>
                    <option <?php if($request->departmentId==$department->id){ echo "selected"; } ?> value="{{$department->id}}">{{$department->departmentName}}</option>
                    <?php }else{ ?>
                    <option value="{{$department->id}}">{{$department->departmentName}}</option>
                    <?php } ?>

                    @endforeach
                </select>
            </div>
            <div class="form-group">
              <label for="designationId">Employee Designation</label>
              <select name="designationId" id="designationId" class="form-control" >
                <option value="">All Designation</option>
                @foreach($designations as $d)

                <?php if(isset($request)&&!empty($request)){ ?>
                <option <?php if($request->designationId==$d->id){ echo "selected"; } ?> value="{{$d->id}}">{{$d->designation}}</option>
                <?php }else{ ?>
                <option value="{{$d->id}}">{{$d->designation}}</option>
                <?php } ?>
                @endforeach
                </select>
            </div>

            <div class="form-group">
              <label for="unitId">Unit</label>
              <select name="unitId" id="unitId" class="form-control" >
                <option value="">All Unit</option>
                @foreach($units as $unit)

                <?php if(isset($request)&&!empty($request)){ ?>
                <option <?php if($request->unitId==$unit->id){ echo "selected"; } ?> value="{{$unit->id}}">{{$unit->name}}</option>
                <?php }else{ ?>
                <option value="{{$unit->id}}">{{$unit->name}}</option>
                <?php } ?>
                @endforeach
                </select>
            </div>

            <div class="form-group">
              <label for="empSection">Employee Section</label>

                <?php if(isset($request)&&!empty($request)){ ?>
                    <input name="empSection" value="{{$request->empSection}}" id="empSection" class="form-control" >
                <?php }else{ ?>
                    <input name="empSection" placeholder="Employee Section" id="empSection" class="form-control" >
                <?php } ?>
        </div>

        <hr>
        <div class="form-group">
            <button type="submit" class="btn btn-danger" ><i class="fa fa-filter"></i> Filter</button>
            <button type="button" class="btn btn-default" data-dismiss="modal"> Close</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

</div>
</div>


<div class="modal fade" id="search_employee" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Search Employee</strong></h4>
            </div>
            <div class="modal-body" style="border-top:1px solid #ddd;">
             <form action="{{url('/employee_grid_search')}}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                  <label for="employeeInfo">Keyword</label>
                  <input name="keyword" placeholder="Write search keyword here." id="employeeInfo" class="form-control" />
                </div>
            <hr>
            <div class="form-group">
                <button type="submit" class="btn btn-success" ><i class="fa fa-search"></i> Search</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>

</div>
</div>


<script type="text/javascript">

    var url = "{{ route('employee.section.autocomplete.ajax') }}";

    $('#empSection').typeahead({

        source:  function (query, process) {

            return $.get(url, { query: query }, function (data) {

                return process(data);

            });

        }

    });

</script>
@include('include.copyright')
@endsection