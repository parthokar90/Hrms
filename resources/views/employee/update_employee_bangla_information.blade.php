@extends('layouts.master')
@section('title', 'Update Employee Information(Bangla)')
@section('content')
{{ Html::script('hrm_script/js/bootstrap3-typeahead.js') }}

    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Update Employee Information (Bangla)</strong></h2>
                    </div>
                    <div class="panel-body bg-white">

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                {!! Form::open(['method'=>'POST','route'=>'employee.store_bangla_information']) !!}

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                {!! Form::label('empFirstName','Employee Fullname (English)',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input name="empId" value="{{base64_encode($employee->id)}}" hidden >
                                                    <input type="text" name="" class="form-control" value="{{$employee->empFirstName}} {{$employee->empLastName}}" minlength="3" placeholder="Minimum 3 characters..." disabled="" >
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                <label class="control-label">Employee ID (English)</label>
                                                <div class="append-icon">
                                                    <input type="text" name="employeeId" class="form-control" value="{{$employee->employeeId}}" disabled=""  placeholder="Enter Employee Id..." >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                {!! Form::label('empBnFullName','Employee Fullname (Bangla)',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empBnFullName" class="form-control" value="{{$employee->empBnFullName}}" minlength="3" placeholder="Enter Employee Fullname in Bangla.." required >
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                <label class="control-label">Employee ID/Card No (Bangla)</label>
                                                <div class="append-icon">
                                                    <input type="text" name="employeeBnId" class="form-control" value="{{$employee->employeeBnId}}" placeholder="Enter Employee ID/Card no (Bangla) ..." >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            
                                            <div class="form-group">
                                                {!! Form::label('empFatherName',"Father's Name (English)",['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empFatherName" class="form-control" disabled="" value="{{$employee->empFatherName}}" >
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empMotherName',"Mother's Name (English)",['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" value="{{$employee->empMotherName}}" name="empMotherName" class="form-control" disabled="" >
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            
                                            <div class="form-group">
                                                {!! Form::label('empFatherName',"Father's Name (Bangla)",['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empBnFatherName" class="form-control" value="{{$employee->empBnFatherName}}" placeholder="Father's Name" >
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empMotherName',"Mother's Name (Bangla)",['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <input type="text" name="empBnMotherName" class="form-control" value="{{$employee->empBnMotherName}}" placeholder="Mother's Name" >
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empCurrentAddress','Current Address (English)',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <textarea name="empCurrentAddress" rows="3" class="form-control" disabled="" >{{$employee->empCurrentAddress}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empParAddress','Permanent Address (English)',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <textarea name="empParAddress" rows="3" class="form-control" disabled="" >{{$employee->empParAddress}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empBnCurrentAddress','Current Address (Bangla)',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <textarea name="empBnCurrentAddress" rows="3" class="form-control" placeholder="Write your current address(Bangla)... ">{{$employee->empBnCurrentAddress}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {!! Form::label('empBnParAddress','Permanent Address (Bangla)',['class'=>'control-label']) !!}
                                                <div class="append-icon">
                                                    <textarea name="empBnParAddress" rows="3" class="form-control" placeholder="Write your permanent address(Bangla)... ">{{$employee->empBnParAddress}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                               <label>Educational Qualification (Bangla)</label>
                                                <div class="append-icon">
                                                    <input type="text" name="empBnEduQuality" class="form-control" value="{{$employee->empBnEduQuality}}" placeholder="Educational Qualification (Bangla)" >
                                                    <i class="fa fa-cap"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                               <label>Previous Work Experience (If any) (Bangla)</label>
                                                <div class="append-icon">
                                                    <input type="text" name="empPreExperience" class="form-control"  value="{{$employee->empPreExperience}}" placeholder="Previous Work Experience (Bangla)" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Working Type</label>
                                                <div class="append-icon">
                                                    <input type="text" name="empBnWorkingType" placeholder="Working Type" value="{{$employee->empBnWorkingType}}"  class="form-control" id="empBnWorkingType" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                           
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Number of Children (সন্তান সংখ্যা)</label>
                                                <div class="append-icon">
                                                    <input type="text" maxlength="2" min="0" name="empChildrenNumber" placeholder="Number of Children (সন্তান সংখ্যা)" value="{{$employee->empChildrenNumber}}"  class="form-control" id="empChildrenNumber" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Number of Boy (ছেলে সন্তানের সংখ্যা)</label>
                                                <div class="append-icon">
                                                    <input type="text" maxlength="2" min="0" name="empBoyNumber" placeholder="Number of Boy (ছেলে সন্তানের সংখ্যা)" value="{{$employee->empBoyNumber}}"  class="form-control" id="empBoyNumber" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Number of Girl (মেয়ে সন্তানের সংখ্যা)</label>
                                                <div class="append-icon">
                                                    <input type="text" maxlength="2" min="0" name="empGirlNumber" placeholder="Number of Girl (মেয়ে সন্তানের সংখ্যা)" value="{{$employee->empGirlNumber}}"  class="form-control" id="empGirlNumber" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center  m-t-20">
                                        <button type="submit" class="btn btn-embossed btn-primary"><i class="fa fa-save"></i> &nbsp; Save Information</button>
                                        <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0"> <i class="fa fa-refresh"></i> &nbsp; Cancel</button>
                                    </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
//        function lineFunc(that) {
//            alert(that.value);
//
//        }
        $(document).ready(function () {
            $("select.floor").change(function () {
                var floor= $(".floor option:selected").val();
                if(!floor){
                    var rr='<select class="form-control floor_lines" name="line_id">'+
                    '<option value="">Select Floor First</option>'+

                    '</select>';
                    $('.floor_lines').html(rr);
                }
                else {
                    $.ajax({
                        url: 'floor/line/' + floor,
                        method: 'get',
                        dataType: 'html',
                        success: function (response) {
                            $('.floor_lines').html(response);


                        }
                    });
                }

            });

            
        });
        function statusFunc(that) {
            if(that.value==='0'){
                document.getElementById('date_of_discontinuation').disabled=false;
                document.getElementById('reason_of_discontinuation').disabled=false;
            }
            else {
                document.getElementById('date_of_discontinuation').disabled=true;
                document.getElementById('reason_of_discontinuation').disabled=true;

            }

        }

        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

    </script>

    <script type="text/javascript">

        var url = "{{ route('employee.section.autocomplete.ajax') }}";

        $('#empSection').typeahead({

            source:  function (query, process) {

            return $.get(url, { query: query }, function (data) {

                    return process(data);

                });

            }

        });

    </script>
@include('include.copyright')

@endsection