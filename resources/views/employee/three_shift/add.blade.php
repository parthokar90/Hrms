@extends('layouts.master')
@section('title', 'Manage Employee Shift')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('success'))
        <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
        @endif
        @if(Session::has('error'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
        @endif
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-gavel"></i> <strong>Employee Shift</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="panel-content well">
                        <h3><i class="fa fa-plus-square"></i> <b>Add Employee Shift</b></h3><hr>
                        <form action="{{url('emp/three/shift/add')}}" method="get">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="pwd">Select Employee:</label>
                                    <select class="form-control" name="emp_id" autocomplete="off" data-search="true" required>
                                        <option value="">Select Employee</option>
                                        @foreach($employee as $emp)
                                          <option value="{{$emp->id}}">{{$emp->employeeId}}-{{$emp->empFirstName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="required form-group">
                                    <label class="control-label">Start Date</label>
                                    <div class="append-icon">
                                        <input type="text" name="start_date" autocomplete="off" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="required form-group">
                                    <label class="control-label">End Date</label>
                                    <div class="append-icon">
                                        <input type="text" name="end_date" autocomplete="off" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <button style="margin-left:15px" type="submit" class="btn btn-success"><i class="fa fa-save"></i>Process</button>                   
                            </form>
                        </div>
                        <div class="panel-content well">
                            <h3><i class="fa fa-pencil-square"></i> <b>Update Employee Shift</b></h3><hr>
                            <form action="{{url('emp/three/shift/data/update')}}" method="get">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pwd">Select Employee:</label>
                                        <select class="form-control" name="emp_id" autocomplete="off" data-search="true" required>
                                            <option value="">Select Employee</option>
                                            @foreach($employee as $emp)
                                              <option value="{{$emp->id}}">{{$emp->employeeId}}-{{$emp->empFirstName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="required form-group">
                                        <label class="control-label">Start Date</label>
                                        <div class="append-icon">
                                            <input type="text" name="start_date" autocomplete="off" class="date-picker form-control" placeholder="Select a date..." required>
                                            <i class="icon-calendar"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="required form-group">
                                        <label class="control-label">End Date</label>
                                        <div class="append-icon">
                                            <input type="text" name="end_date" autocomplete="off" class="date-picker form-control" placeholder="Select a date..." required>
                                            <i class="icon-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <button style="margin-left:15px" type="submit" class="btn btn-success"><i class="fa fa-save"></i>Process</button>                   
                                </form>
                            </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection

