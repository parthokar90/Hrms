@extends('layouts.master')
@section('title', 'Update Employee Shifts')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @include('employee.three_shift.shift_add_modal')
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-gavel"></i> <strong>Update Employee Shift</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="panel-content well">
                        <h3><i class="fa fa-plus-square"></i> <b>Update Employee Shift</b></h3><hr>
                           <h4 class="text-center">Employee Information</h4>
                            <p style="font-size:20px" class="text-center">Id:{{$employee->employeeId}}</p>
                            <p style="font-size:20px" class="text-center">Name:{{$employee->empFirstName}}</p>
                            <a href="{{url('emp/three/shift/add/view')}}" class="btn btn-success btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                            <button type="button" id="process_btn" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></button>
                            <table style="width:100%" class="table table-border">
                                <tr>
                                  <th>Sl</th>
                                  <th>Date</th>
                                  <th>Shift</th>
                                  <th>Weekend</th>
                                  <th>Action</th>
                                </tr>
                                @php $order=0; @endphp
                                <form method="post" action="{{url('emp/three/shift/data/update/final/employee/all')}}">
                                    @csrf 
                                <input type="hidden" name="emp_id" value="{{$employee->id}}">
                               
                               @foreach($data as $emp)
                                <input type="hidden" name="shift_date[]" value="{{$emp->shift_date}}">
                               @php $order++; @endphp
                                <tr>
                                <td>{{$order}}</td>    
                                <td>{{date('Y-m-d',strtotime($emp->shift_date))}}->{{date('l',strtotime($emp->shift_date))}}</td>
                                    <td>
                                     <select name="emp_shift[]" class="form-control">
                                        <option value="">Select</option> 
                                        @foreach($shift as $shifts)
                                        @php
                                            $selected = '';
                                            if($shifts->id == $emp->emp_shift)    // Any Id
                                            {
                                            $selected = 'selected="selected"';
                                            }
                                        @endphp
                                        <option value='{{ $shifts->id }}' {{$selected}} >{{ $shifts->shiftName }}</option>
                                       @endforeach
                                     </select>
                                    </td>
                                    <td>
                                        <select name="weekend[]" class="form-control">
                                            @if($emp->weekend=='')
                                            <option value="">Select</option> 
                                            <option value='w'>Weekend</option>
                                            @else 
                                            <option value="">Select</option> 
                                            <option value='w' selected>Weekend</option>
                                            @endif
                                         </select>
                                    </td>
                                    <td>
                                        <div class="col-md-2">
                                            <a onclick="return confirm('are you sure??')" class="btn btn-danger btn-sm" href="{{url('emp/three/shift/data/delete/final/'.$emp->id)}}"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                  </tr>
                                  @endforeach
                                  @if($check>0)
                                  <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Update All</button>
                                  @endif
                                </form>
                              </table> 
                              <div class="append_div"></div>               
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection