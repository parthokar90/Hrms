 <!-- Modal content-->
 <!-- Modal -->
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New</h4>
        </div>
        <div class="modal-body">
          <form action="{{route('three_shift_emp_update')}}" method="post">
            @csrf 
          <input type="hidden" name="emp_id" value="{{$employee->id}}">
            <div class="col-md-4">
                <div class="required form-group">
                    <label class="control-label">Date</label>
                    <div class="append-icon">
                        <input type="text" name="shift_date" autocomplete="off" class="date-picker form-control" placeholder="Select a date..." required>
                        <i class="icon-calendar"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
              <label class="control-label">Shift</label>
              <select name="emp_shift" class="form-control">
                <option value="">Select</option> 
                @foreach($shift as $shifts)
                  <option value='{{ $shifts->id }}'>{{ $shifts->shiftName }}</option>
                @endforeach
             </select>
            </div>
            <div class="col-md-4">
              <label class="control-label">Weekend</label>
              <select name="weekend" class="form-control">
                <option value="">Select</option> 
                  <option value='w'>Weekend</option>
             </select>
            </div>
        </div>
        <div class="modal-footer">
          <button style="margin-top:5px"  type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>   
        </div>
      </form>
      </div>
    </div>
  </div>