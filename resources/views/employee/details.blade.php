<?php
function en2bnNumber($number){
    $search_array= array('0','1','2','3','4','5','6','7','8','9','Jan','Feb','Mar','Apr','May',
                        'Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    $replace_array= array('০','১','২','৩','৪','৫','৬','৭','৮','৯','জানুয়ারী','ফেব্রুয়ারী','মার্চ',
                    'এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
    $output = str_replace($search_array, $replace_array, $number);
    return $output;
}
?>
@php
    $expectedDate=\Carbon\Carbon::now()->subMonths($employee->probation_period);
    $joiningDate=\Carbon\Carbon::parse($employee->empJoiningDate);
    $difference = $joiningDate->diffInMonths($expectedDate);
@endphp
@section('title', 'Employee Profile')
@extends('layouts.master')

@section('content')
    <div class="page-content">

        @if(Session::has('mhmessage'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('mhmessage') }}</p>
        @endif

        @if(Session::has('employeeUpdate'))
            <p id="alert_message" class="alert alert-info">{{ Session::get('employeeUpdate') }}</p>
        @endif

        @if(Session::has('appointment_latter_message'))
            <p id="alert_message" class="alert alert-danger">{{ Session::get('appointment_latter_message') }}</p>
        @endif

        @if(Session::has('upPassword'))
            <p id="alert_message" class="alert alert-info">{{ Session::get('upPassword') }}</p>
        @endif

        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('delete'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('delete')}}</p>
        @endif
        @if(Session::has('edit'))
            <p id="alert_message" class="alert alert-info">{{Session::get('edit')}}</p>
        @endif
        @if(Session::has('createEmployee'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('createEmployee') }}</p>
        @endif
        @if(Session::has('fileSize'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('fileSize')}}</p>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-user"></i> Employee  <strong>Details</strong></h3>
                    </div>
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary" id="employ-details-nav-tab-show">
                            <li class="active"><a href="#tab2_1" data-toggle="tab"><i class="icon-user"></i> Profile</a></li>
                            <li><a href="#tab2_2" data-toggle="tab"><i class="fa fa-anchor"></i> Nominee</a></li>
                            <li><a href="#tab2_3" data-toggle="tab"><i class="fa fa-book"></i> Educational Info</a></li>
                            <li><a href="#tab2_4" data-toggle="tab"><i class="fa fa-ils"></i> Working Experience</a></li>
                            <li><a href="#tab2_7" data-toggle="tab"><i class="fa fa-gavel "></i> Skill Test</a></li>
                            <li><a href="#tab2_8" data-toggle="tab"><i class="fa fa-empire"></i> Training</a></li>
                            <li><a href="#tab2_5" data-toggle="tab"><i class="fa fa-codepen"></i> Others</a></li>
                            <li><a href="#tab2_6" data-toggle="tab"><i class="fa fa-money"></i> Salary</a></li>
                            <li><a href="#tab2_9" data-toggle="tab"><i class="icon-cloud-download"></i> Download</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab2_1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img class="img-responsive" height="200" width="200" src="
                                            @if($employee->empPhoto ==null)
                                        @if($employee->empGenderId==1 || $employee->empGenderId==3)
                                        {{asset("Employee_Profile_Pic/male.jpg")}}
                                        @endif
                                        @if($employee->empGenderId==2)
                                        {{asset("Employee_Profile_Pic/female.jpg")}}
                                        @endif

                                        @endif
                                        @if($employee->empPhoto!=null)
                                        {{asset("Employee_Profile_Pic/".$employee->empPhoto)}}
                                        @endif
                                        ">
                                              
                                    </div>
                                    <div class="col-md-9">
                                        <h2><strong>{{$employee->empFirstName." ".$employee->empLastName }}</strong></h2>
                                        <p><i class="fa fa-phone"></i>{{$employee->empPhone}}<span class="padLeft20"><i class="fa fa-envelope"></i></span><span>{{$employee->empEmail}}</span></p>
                                        <a href="#"><button class="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-edit"></i> Edit Info</button></a>
                                        <a href="{{route('employee.bangla_information',base64_encode($employee->id))}}" target="_BLANK"><button class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Update Bangla Info</button></a>
                                        <a href="#"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editPhoto"><i class="fa fa-image"></i> Update Picture</button></a>
                                        <a href="#"><button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editProxy"><i class="fa fa-edit"></i> Update Proxy Card</button></a>
                                        @if(checkPermission(['admin']))
                                            <a href="#"><button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editPassword"><i class="fa fa-key"></i> Update Password</button></a>
                                        @elseif(checkPermission(['hr']))
                                            <a href="#"><button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editPassword"><i class="fa fa-key"></i> Update Password</button></a>
                                        @endif
                                        <hr>
                                        <span>
                                            Status: <span class="employee-profile-text">
                                                @if($employee->empAccStatus=='0')
                                                    <span class="red-text">Inactive</span>

                                                @else
                                                    <span>Active</span>

                                                @endif
                                            </span>
                                        </span>
                                        <span class="padLeft20">
                                            Employee Id:<span class="employee-profile-text">
                                                {{ $employee->employeeId }}
                                            </span>
                                        </span>
                                        <span class="padLeft20">
                                            Department: <span class="employee-profile-text">
                                                {{ $employee->departmentName }}
                                            </span>
                                        </span>
                                        <span class="padLeft20">
                                            Designation: <span class="employee-profile-text">
                                                {{$employee->designation}}
                                            </span>
                                        </span>
                                        <hr>
                                    </div>

                                    <!--Edit Profile-->

                                @include('employee.modal.editDetails')


                                @include('employee.modal.editProxy')


                                <!--End Of Edit Profile-->



                                </div>



                                <div class="profile-body row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="panel panel-default">
                                                <div class="panel-heading"><h3><strong>Personal Information</strong></h3></div>
                                                <div class="panel-body">

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Full Name (Bangla):<span class="employee-profile-text">
                                                                    {{$employee->empBnFullName}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Card No (Bangla):<span class="employee-profile-text">
                                                                    {{$employee->employeeBnId}}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Educational Qualification (Bangla):
                                                                    {{$employee->empBnEduQuality}}
                                                                
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Father's Name (English):<span class="employee-profile-text">
                                                                    {{$employee->empFatherName}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Mother's Name (English):<span class="employee-profile-text">
                                                                    {{$employee->empMotherName }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Biometric ID (English):
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->empGlobalId}}
                                                                    {{--{{\Carbon\Carbon::parse($employee->empDOB)->format('l j F Y') }}--}}



                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Father's Name (Bangla):<span class="employee-profile-text">
                                                                    {{$employee->empBnFatherName}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Mother's Name (Bangla):<span class="employee-profile-text">
                                                                    {{$employee->empBnMotherName }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Working Type (Bangla):
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->empBnWorkingType }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Current Address(English):<br><span class="employee-profile-text">
                                                                    {{$employee->empCurrentAddress}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Permanent Address(English):<br><span class="employee-profile-text">
                                                                    {{$employee->empParAddress }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Religion:
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->empReligion }}
                                                                    {{--{{\Carbon\Carbon::parse($employee->empDOB)->format('l j F Y') }}--}}



                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>



                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Current Address(Bangla):<br><span class="employee-profile-text">
                                                                    {{$employee->empBnCurrentAddress}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Permanent Address(Bangla):<br><span class="employee-profile-text">
                                                                    {{$employee->empBnParAddress }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Previous working experience:
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->empPreExperience }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>




                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                National ID:<span class="employee-profile-text">
                                                                    {{$employee->empNid }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Nationality:<span class="employee-profile-text">
                                                                    {{$employee->nationalitiesName }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Date of Birth:
                                                                <span class="employee-profile-text">
                                                                    @if($employee->empDOB!=null)
                                                                        {{\Carbon\Carbon::parse($employee->empDOB)->format('j F Y') }}
                                                                    @endif
                                                                    {{--{{\Carbon\Carbon::parse($employee->empDOB)->format('l j F Y') }}--}}



                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>


                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Gender:<span class="employee-profile-text">
                                                                    @if($employee->empGenderId==1)
                                                                        Male
                                                                    @endif
                                                                    @if($employee->empGenderId==2)
                                                                        Female
                                                                    @endif
                                                                    @if($employee->empGenderId==3)
                                                                        Others
                                                                    @endif
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Marital Status:<span class="employee-profile-text">
                                                                    {{$employee->ms_name }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Joining Date:
                                                                <span class="employee-profile-text">
                                                                    {{\Carbon\Carbon::parse($employee->empJoiningDate)->format('j F Y') }}
                                                                    {{--{{\Carbon\Carbon::parse($employee->empDOB)->format('l j F Y') }}--}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Unit:<span class="employee-profile-text">
                                                                    {{$employee->unit_name}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Line:<span class="employee-profile-text">
                                                                    {{$employee->line_no }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Floor:<span class="employee-profile-text">
                                                                    {{$employee->floor }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Section:<span class="employee-profile-text">
                                                                    {{$employee->empSection}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Salary Type:<span class="employee-profile-text">
                                                                    {{$employee->salary_type }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Payment Mode:<span class="employee-profile-text">
                                                                    {{$employee->payment_mode }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Work Shift:<span class="employee-profile-text">
                                                                    {{$employee->shiftName}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Overtime Status:<span class="employee-profile-text">
                                                                    <?php if($employee->empOTStatus==1){ echo "Yes";} ?>
                                                                    <?php if($employee->empOTStatus==0){ echo "No";} ?>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Attendance Bonus:<span class="employee-profile-text">
                                                                    <?php if($employee->empAttBonusId==0){ echo "No";}else{
                                                                        echo $employee->bTitle;
                                                                    } ?>
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Bank Account:<span class="employee-profile-text">
                                                                    {{$employee->bank_account}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Bank Information:<span class="employee-profile-text">
                                                                    {{$employee->bank_info }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Emergency Contact:<span class="employee-profile-text">
                                                                    {{$employee->empEcontactName}}
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Emergency Contact Relation:<span class="employee-profile-text">
                                                                    {{$employee->emergency_contact_relation}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Emergency Contact Phone:<span class="employee-profile-text">
                                                                    {{$employee->emergencyPhone }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Emergency Contact Address:<br>
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->emergencyAddress }}
                                                                    {{--{{\Carbon\Carbon::parse($employee->empDOB)->format('l j F Y') }}--}}

                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <?php $empId = $employee->id; ?>
                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Reference Type:<span class="employee-profile-text">
                                                                    {{$employee->reference_type }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Reference Details:<span class="employee-profile-text"><br>
                                                                    {{$employee->reference_description }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Skill Level:<span class="employee-profile-text">
                                                                    {{$employee->skill_level}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Working Group:<span class="employee-profile-text">
                                                                    {{$employee->work_group}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Blood Group:<span class="employee-profile-text">
                                                                    {{$employee->empBloodGroup }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Card:<span class="employee-profile-text">
                                                                    {{$employee->empCardNumber }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>


                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Number of Children (সন্তান সংখ্যা): <span class="employee-profile-text">
                                                                    {{$employee->empChildrenNumber}}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Number of Boy (ছেলে সন্তানের সংখ্যা): <span class="employee-profile-text">
                                                                    {{$employee->empBoyNumber }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span>
                                                                Number of Girl (মেয়ে সন্তানের সংখ্যা): <span class="employee-profile-text">
                                                                    {{$employee->empGirlNumber }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>


                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Remaining Probation Period:<span class="employee-profile-text">
                                                                    {{--@if($employee->probation_period==1)--}}
                                                                        {{--Active--}}
                                                                    {{--@else--}}
                                                                        {{--<span class="red-text">Inactive</span>--}}

                                                                    {{--@endif--}}
                                                                @if($expectedDate<$joiningDate)
                                                                    <span class="red-text">{{$difference}}</span> Month/s</span>

                                                                @else
                                                                    <span class="green-text">Completed</span>
                                                                @endif
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Employee Status:<span class="employee-profile-text">
                                                                    @if($employee->empAccStatus==1)
                                                                        Active
                                                                    @else
                                                                        <span class="red-text">Inactive</span>

                                                                    @endif
                                                                </span>
                                                            </span>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <span>
                                                                Date of Discontinuation:<span class="employee-profile-text">
                                                                    @if($employee->date_of_discontinuation !=null)
                                                                        {{\Carbon\Carbon::parse($employee->date_of_discontinuation)->format('j F Y') }}
                                                                    @endif
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>
                                                    <div class="row padTop10">
                                                        <div class="col-md-4">
                                                            <span>
                                                                Reason of Discontinuation:
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->reason_of_discontinuation }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <span>
                                                                Discontinuation Type:
                                                                <span class="employee-profile-text">
                                                                    {{ $employee->discon_type=='1' ? 'Lefty':'' }}
                                                                    {{ $employee->discon_type=='2' ? 'Resign':'' }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="profile-body row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="panel panel-default">
                                                <div class="panel-heading"><h3><strong>Leave Details</strong></h3></div>
                                                    <div class="panel-body">
                                                        <table class="table table-info table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Leave Name</th>
                                                                <th>Total Days</th>
                                                                <th>Available Days</th>
                                                                <th>Leave Taken</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            {{--{{$leave_data->leave_type}}--}}
                                                                @foreach($leave_data as $key=>$ld)
                                                                    @if($key!=0 || $employee->empGenderId!=1)
                                                                        <?php
                                                                        if($ld->leave_type=="Earn Leave"){;
                                                                            $ld->total_days=floor($days/18)+$previous_earn_leave;
                                                                        }
                                                                        else{
                                                                            if(isset($employee->empJoiningDate)){
                                                                                if(\Carbon\Carbon::parse($employee->empJoiningDate)->format('Y')==\Carbon\Carbon::now()->format('Y'))
                                                                                {
                                                                                    $mr=13-\Carbon\Carbon::parse($employee->empJoiningDate)->format('m');
                                                                                    $ld->total_days=floor(($ld->total_days/12)*$mr);
                                                                                }
                                                                            }
                                                                        }

                                                                        ?>
                                                                        <tr>
                                                                            <td>{{$ld->leave_type}}</td>
                                                                            <td>{{$ld->total_days}}</td>
                                                                            @if(isset($ld->leave_taken))
                                                                                <?php
                                                                                    $ld->leave_available= $ld->total_days-$ld->leave_taken-$paid_leave
                                                                                ?>
                                                                            @else
                                                                                <?php
                                                                                    $ld->leave_available=$ld->total_days-$paid_leave;
                                                                                ?>
                                                                            @endif
                                                                            <td>
                                                                                {{$ld->leave_available}}

                                                                            </td>
                                                                            <td>
                                                                                @if(isset($ld->leave_taken))
                                                                                    {{$ld->leave_taken}}

                                                                                @else
                                                                                    {{$ld->total_days-$ld->leave_available}}
                                                                                @endif
                                                                            </td>

                                                                        </tr>
                                                                    @endif

                                                                @endforeach
                                                            </tbody>
                                                        </table>

                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="profile-body row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="panel panel-dark">
                                                <div class="panel-heading"><h3><strong>Attendance Details - {{$empPresent->month}} (Till Today)</strong></h3></div>
                                                <div class="panel-body">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>Month</th>
                                                            <th>Present</th>
                                                            <th>Absent</th>
                                                            <th>Leave</th>
                                                            {{--<th>5</th>--}}

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>{{$empPresent->month}}</td>
                                                            <td>{{$empPresent->present}}</td>
                                                            <td>{{$empPresent->total-$empPresent->present-$empPresent->leave}}</td>
                                                            <td>{{$empPresent->leave}}</td>
                                                            {{--<td>P</td>--}}

                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                            </div>
                            <div class="tab-pane fade" id="tab2_2">
                                <div class="panel-group">
                                    @foreach($nominees as $ec)
                                        <div class="panel panel-default">

                                            <div class="panel-heading">
                                                <h3>
                                                    <strong> {{ $ec->priority }} Nominee</strong>
                                                    <a href="{{route('nominee.edit',$ec->id)}}" class="edu-edit-button show-nominee-edit" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('nominee.delete.show',$ec->id)}}"  class="edu-dlt-button show-nominee-delete" title="Delete"><i class="fa fa-trash-o"></i></a>
                                                </h3>



                                            </div>
                                            <div class="panel-body">
                                                <p>Name: <strong>{{$ec->nominee_name}}</strong></p>
                                                <p>Phone: <strong>{{$ec->nominee_phone}}</strong></p>
                                                <p>Address: <strong>{{$ec->nominee_address}}</strong></p>
                                                <p>Other Details: <strong>{{$ec->nominee_details}}</strong></p>

                                                @if($ec->nominee_attachments)
                                                    <a target="_blank" href="{{asset('Employee_Nominee_Attachments/'.$ec->nominee_attachments)}}"><p><button type="button" class="btn btn-custom-download"><i class="fa fa-download"></i> Download Attachment</button></p></a>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach

                                    <div class="panel-group">
                                        <center><a href="#" data-toggle="modal" data-target="#newNominee" class="btn btn-blue btn-md"><i class="fa fa-plus-circle"></i> Add New Nominee</a></center>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab2_3">
                                <div class="panel-group">
                                    @foreach($edu_cers as $ec)
                                        <div class="panel panel-default">

                                            <div class="panel-heading">
                                                <h3>
                                                    <strong> {{$ec->empExamTitle}}</strong>
                                                    <a href="#" data-toggle="modal" data-target="{{'#educationEdit'.$ec->id}}" class="edu-edit-button" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="#" data-toggle="modal" data-target="{{'#educationDelete'.$ec->id}}" class="edu-dlt-button" title="Delete"><i class="fa fa-trash-o"></i></a>
                                                </h3>



                                            </div>
                                            <div class="panel-body">
                                                <p>Institution: <strong>{{$ec->empInstitution}}</strong></p>
                                                <p>Exam Result: <strong>{{$ec->empResult}}</strong></p>
                                                <p>Result Scale: <strong>{{$ec->empScale}}</strong></p>
                                                <p>Passing Year: <strong>{{$ec->empPassYear}}</strong></p>

                                                @if($ec->empCertificate)
                                                    <a target="_blank" href="{{asset('Educational_Certificates/'.$ec->empCertificate)}}"><p><button type="button" class="btn btn-custom-download"><i class="fa fa-download"></i> Download Attachment</button></p></a>
                                                @endif
                                            </div>
                                        </div>
                                        <!--Edit Education Modal-->

                                        @include('employee.modal.editEmployeeEducation')

                                        <!--End of Edit Education Modal-->

                                        <!--Delete Education Model-->

                                        @include('employee.modal.deleteEmployeeEducation')

                                        <!--End of Delete Education Model-->

                                    @endforeach
                                </div>
                                <center><a href="#" data-toggle="modal" data-target="#educationalInfo" class="btn btn-blue btn-md"><i class="fa fa-graduation-cap"></i> New Educational Information</a></center>
                            </div>
                            <div class="tab-pane fade" id="tab2_4">
                                <div class="panel-group">
                                    @foreach($workExp as $ew)
                                        <div class="panel panel-default">

                                            <div class="panel-heading">
                                                <h3>
                                                    <strong> {{$ew->empJobTile}}</strong>
                                                    <a href="{{route('workexpericence.edit',$ew->id)}}" class="edu-edit-button show-edit-we-modal" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('workexperience.delete.show',$ew->id)}}" class="edu-dlt-button show-delete-we-modal" title="Delete"><i class="fa fa-trash-o"></i></a>
                                                </h3>



                                            </div>
                                            <div class="panel-body">
                                                <p>Company Name: <strong>{{$ew->empCompanyName}}</strong></p>
                                                <p>Joining Date: <strong>{{\Carbon\Carbon::parse($ew->empJoiningDate)->format('j F Y') }}</strong></p>
                                                <p>Ending Date: <strong>{{\Carbon\Carbon::parse($ew->empLeaveDate)->format('j F Y') }}</strong></p>
                                                <p>Other Details: <strong>{{$ew->empWHDescription}}</strong></p>
                                                @if($ew->empWHattachment)
                                                <a target="_blank" href="{{asset('Employee_Working_Experience/'.$ew->empWHattachment)}}"><p><button type="button" class="btn btn-custom-download"><i class="fa fa-download"></i> Download Attachment</button></p></a>
                                                @endif
                                            </div>
                                        </div>

                                    @endforeach
                                </div>


                                <div class="panel-group">
                                    <center><a href="#" data-toggle="modal" data-target="#newWork" class="btn btn-blue btn-md"><i class="fa fa-graduation-cap"></i>Add Working Experience</a></center>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab2_7">
                                <div class="panel-group">
                                    @foreach($skillTest as $ew)
                                        <div class="panel panel-default">

                                            <div class="panel-heading">
                                                <h3>
                                                    <strong> {{$ew->skill_name}}</strong>
                                                    <a href="{{route('skill.edit',$ew->id)}}" class="edu-edit-button show-edit-we-modal" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('skill.delete.show',$ew->id)}}" class="edu-dlt-button show-delete-we-modal" title="Delete"><i class="fa fa-trash-o"></i></a>
                                                </h3>



                                            </div>
                                            <div class="panel-body">
                                                <p>Test Date: <strong>{{\Carbon\Carbon::parse($ew->test_date)->format('j F Y') }}</strong></p>
                                                <p>Performance: <strong>{{$ew->performance}}</strong></p>
                                                <p>Other Details: <strong>{{$ew->skill_Description}}</strong></p>
                                                @if($ew->attachment)
                                                <a target="_blank" href="{{asset('Employee_skill_test/'.$ew->attachment)}}"><p><button type="button" class="btn btn-custom-download"><i class="fa fa-download"></i> Download Attachment</button></p></a>
                                                @endif
                                            </div>
                                        </div>

                                    @endforeach
                                </div>


                                <div class="panel-group">
                                    <center><a href="#" data-toggle="modal" data-target="#skillTest" class="btn btn-blue btn-md"><i class="fa fa-graduation-cap"></i>Add Skill Test</a></center>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab2_5">

                            <div class="medical_history_c">
                                      <div class="panel-content">
                                        <ul class="nav nav-tabs nav-primary">
                                          <li class="active"><a href="#tab3_1" data-toggle="tab" aria-expanded="true"><i class="fa fa-hospital-o"></i> Medical History</a></li>
                                          <li class="active"><a href="#tab3_2" data-toggle="tab" aria-expanded="true"><i class="fa fa-puzzle-piece"></i> Police Verification </a></li>
                                          <li class=""><a href="#tab3_3" data-toggle="tab" aria-expanded="false"><i class="fa fa-joomla"></i> Additional Info </a></li>
                                          
                                        </ul>
                                        <div class="tab-content">

                                          <div class="tab-pane animated zoomIn active" id="tab3_1">
                                            
                                             <?php
                                                $medical_history=DB::table('tbemployee_medical_history')->where('empId','=',$employee->id)->take(1)->first();
                                                if((empty($medical_history))){
                                                ?>
                                                {!!  Form::open(array('url' => '/store_medical_history', 'method' => 'POST')) !!}
                                            <?php }else{ ?>
                                                {!! Form::open( ['method'=>'PATCH','url'=>['/update_medical_history',$employee->id]]) !!}
                                            <?php }

                                             ?>
                                            <input name="empId" value="{{$empId}}" hidden >
                                            <h3><strong>Medical History</strong></h3>
                                            <hr>
                                            <p><strong>1. Do employee have any of the following that could affect his/her performance in this job? If “Yes”, please explain- <br />
                                            (নিম্নলিখিত কোনটির দ্বারা কর্মকর্তা/ কর্মচারীর কার্যক্ষমতা প্রভাবিত হতে পারে? যদি ‘হ্যাঁ’ হয় তবে ব্যাখ্যা করুন।)</strong></p>
                                            <table width="100%" style="margin-left:25px;">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Current and/or reoccurring (chronic) medical/health conditions (বর্তমান অথবা দীর্ঘস্থায়ী চিকিৎসা/স্বাস্থ্য অবস্থা):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="18%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->curHCs)&&($medical_history->curHCs)==1){ echo "checked";} ?> name="curHCs" id="curHCsy" required > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="curHCs" id="curHCsn"  <?php if(!empty($medical_history->curHCs)&&($medical_history->curHCs)==0){ echo "checked";} if(empty($medical_history->curHCs)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="curHCt" value="<?php if(!empty($medical_history->curHCt)&&($medical_history->curHCt)!=NULL){ echo $medical_history->curHCt;} ?>"  id="curHCst" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Current medications (বর্তমান চিকিৎসা):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->curMedicationss)&&($medical_history->curMedicationss)==1){ echo "checked";} ?>  name="curMedicationss" id="curMedicationsy" required > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="curMedicationss" id="curMedicationsn" <?php if(!empty($medical_history->curMedicationss)&&($medical_history->curMedicationss)==0){ echo "checked";} if(empty($medical_history->curMedicationss)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="curMedicationst" id="curMedicationst" placeholder="Please explain " value="<?php if(!empty($medical_history->curMedicationst)&&($medical_history->curMedicationst)!=NULL){ echo $medical_history->curMedicationst;} ?>" class="form-control" >
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Any medical conditions that make you prone to infections? (কোন মেডিকেল শর্তাবলি আপনার সংক্রমনের প্রবনতা বৃদ্ধি করে?):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->mcpis)&&($medical_history->mcpis)==1){ echo "checked";} ?>  name="mcpis" id="mcpisy" required > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="mcpis" id="mcpisn" <?php if(!empty($medical_history->mcpis)&&($medical_history->mcpis)==0){ echo "checked";} if(empty($medical_history->mcpis)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="mcpit"  value="<?php if(!empty($medical_history->mcpit)&&($medical_history->mcpit)!=NULL){ echo $medical_history->mcpit;} ?>" id="mcpit" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Past surgery (পূর্বে কোন সার্জারি):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->pastSurgerys)&&($medical_history->pastSurgerys)==1){ echo "checked";} ?>  name="pastSurgerys" id="pastSurgerysy" required > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="pastSurgerys" id="pastSurgerysn" <?php if(!empty($medical_history->pastSurgerys)&&($medical_history->pastSurgerys)==0){ echo "checked";} if(empty($medical_history->pastSurgerys)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="pastSurgeryt"  value="<?php if(!empty($medical_history->pastSurgeryt)&&($medical_history->pastSurgeryt)!=NULL){ echo $medical_history->pastSurgeryt;} ?>" id="pastSurgeryt" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Past and/or present musculoskeletal injuries/problems (back, shoulder, neck, hand, wrist, hip, knee, etc) (অতীত অথবা বর্তমানে কোনরকম পেশী আঘাত/সমস্যা (কাঁধ, ঘাড়, হাত, কব্জি, হাঁটু ইত্যাদি):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->ppmips)&&($medical_history->ppmips)==1){ echo "checked";} ?>  name="ppmips" id="ppmipsy" required > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="ppmips" id="ppmipsn" <?php if(!empty($medical_history->ppmips)&&($medical_history->ppmips)==0){ echo "checked";} if(empty($medical_history->ppmips)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="ppmipt"  value="<?php if(!empty($medical_history->ppmipt)&&($medical_history->ppmipt)!=NULL){ echo $medical_history->ppmipt;} ?>" id="ppmipt" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Vision problems (দৃষ্টি সমসশা):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->visionProblems)&&($medical_history->visionProblems)==1){ echo "checked";} ?>  name="visionProblems" id="visionProblemsy" required > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="visionProblems" id="visionProblemsn" <?php if(!empty($medical_history->visionProblems)&&($medical_history->visionProblems)==0){ echo "checked";} if(empty($medical_history->visionProblems)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="visionProblemt"  value="<?php if(!empty($medical_history->visionProblemt)&&($medical_history->visionProblemt)!=NULL){ echo $medical_history->visionProblemt;} ?>" id="visionProblemt" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Hearing problems (শ্রবণ সমসশা):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->hearProblems)&&($medical_history->hearProblems)==1){ echo "checked";} ?>  name="hearProblems" id="hearProblemsy" required > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="hearProblems" id="hearProblemsn" <?php if(!empty($medical_history->hearProblems)&&($medical_history->hearProblems)==0){ echo "checked";} if(empty($medical_history->hearProblems)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="hearProblemt" value="<?php if(!empty($medical_history->hearProblemt)&&($medical_history->hearProblemt)!=NULL){ echo $medical_history->hearProblemt;} ?>" id="hearProblemt" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Skin conditions (চর্ম সমসশাঃ):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->skillConditions)&&($medical_history->skillConditions)==1){ echo "checked";} ?>  name="skillConditions" id="skillConditionsy" required > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="skillConditions" id="skillConditionsn" <?php if(!empty($medical_history->skillConditions)&&($medical_history->skillConditions)==0){ echo "checked";} if(empty($medical_history->skillConditions)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="skillConditiont" value="<?php if(!empty($medical_history->skillConditiont)&&($medical_history->skillConditiont)!=NULL){ echo $medical_history->skillConditiont;} ?>" id="skillConditiont" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                            <br />
                                            <br />
                                            <p><strong>2. Have you had exposure to any of the following hazards without use of recommended Personal Protective Equipment(PPE). If yes, please explain<br /> 
                                            (আপনি কি সুপারিশকৃত বাক্তিগত সুরক্ষামূলক সরঞ্জাম ব্যবহার না করে নিম্নলিখিত ঝুঁকি কোনটির সংস্পর্শে এসেছেন?)</strong></p>

                                            <table width="100%" style="margin-left:25px;">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Chemicals (রাসায়নিক পদার্থ):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="18%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->chemicals)&&($medical_history->chemicals)==1){ echo "checked";} ?>  name="chemicals" id="chemicalsy" required   > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="chemicals" id="chemicalsn" <?php if(!empty($medical_history->chemicals)&&($medical_history->chemicals)==0){ echo "checked";} if(empty($medical_history->chemicals)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="chemicalt" value="<?php if(!empty($medical_history->chemicalt)&&($medical_history->chemicalt)!=NULL){ echo $medical_history->chemicalt;} ?>"  id="chemicalt" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Noise (কোলাহল):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="18%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->noises)&&($medical_history->noises)==1){ echo "checked";} ?>  name="noises" id="noisesy"  required  > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="noises" id="noisesn" <?php if(!empty($medical_history->noises)&&($medical_history->noises)==0){ echo "checked";} if(empty($medical_history->noises)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="noiset" value="<?php if(!empty($medical_history->noiset)&&($medical_history->noiset)!=NULL){ echo $medical_history->noiset;} ?>"  id="noiset" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <br>
                                                            <p>Radiation (বিকিরণ/রশ্মিবিচ্ছুরন):</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="18%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->radiations)&&($medical_history->radiations)==1){ echo "checked";} ?>  name="radiations" id="radiationsy"  required  > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="radiations" id="radiationsn" <?php if(!empty($medical_history->radiations)&&($medical_history->radiations)==0){ echo "checked";} if(empty($medical_history->radiations)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="radiationt" value="<?php if(!empty($medical_history->radiationt)&&($medical_history->radiationt)!=NULL){ echo $medical_history->radiationt;} ?>"  id="radiationt" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <br />
                                            <br />
                                            <p><strong>3. Allergies and/or Sensitivities  
                                            (এলার্জি এবং/অথবা সংবেদনশীলতা)</strong></p>

                                            <table width="100%" style="margin-left:25px;">
                                                <tbody>
                                                    <tr>
                                                        <td width="18%">
                                                            <br>
                                                            <p>Latex (তরুক্ষীর):</p>
                                                        </td>
                                                        <td width="18%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->latexs)&&($medical_history->latexs)==1){ echo "checked";} ?>  name="latexs" id="latexsy"  required  > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="latexs" id="latexsn" <?php if(!empty($medical_history->latexs)&&($medical_history->latexs)==0){ echo "checked";} if(empty($medical_history->latexs)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="latext" value="<?php if(!empty($medical_history->latext)&&($medical_history->latext)!=NULL){ echo $medical_history->latext;} ?>"  id="latext" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="18%">
                                                            <br>
                                                            <p>Drugs (ঔষধ):</p>
                                                        </td>
                                                        <td width="18%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->drugss)&&($medical_history->drugss)==1){ echo "checked";} ?>  name="drugss" id="drugssy"  required  > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="drugss" id="drugssn" <?php if(!empty($medical_history->drugss)&&($medical_history->drugss)==0){ echo "checked";} if(empty($medical_history->drugss)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="drugst" value="<?php if(!empty($medical_history->drugst)&&($medical_history->drugst)!=NULL){ echo $medical_history->drugst;} ?>"  id="drugst" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="18%">
                                                            <br>
                                                            <p>Chemicals (রাসায়নিক পদার্থ):</p>
                                                        </td>
                                                        <td width="18%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->chemicalass)&&($medical_history->chemicalass)==1){ echo "checked";} ?>  name="chemicalass" id="chemicalassy"  required  > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="chemicalass" id="chemicalassn" <?php if(!empty($medical_history->chemicalass)&&($medical_history->chemicalass)==0){ echo "checked";} if(empty($medical_history->chemicalass)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="chemicalast" value="<?php if(!empty($medical_history->chemicalast)&&($medical_history->chemicalast)!=NULL){ echo $medical_history->chemicalast;} ?>"  id="chemicalast" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="18%">
                                                            <br>
                                                            <p>Insect Stings (পোকা):</p>
                                                        </td>
                                                        <td width="18%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->insects)&&($medical_history->insects)==1){ echo "checked";} ?>  name="insects" id="insectsy"  required  > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="insects" id="insectsn" <?php if(!empty($medical_history->insects)&&($medical_history->insects)==0){ echo "checked";} if(empty($medical_history->insects)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="insectt" value="<?php if(!empty($medical_history->insectt)&&($medical_history->insectt)!=NULL){ echo $medical_history->insectt;} ?>"  id="insectt" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="18%">
                                                            <br>
                                                            <p>Fragrances (সুগন্ধী):</p>
                                                        </td>
                                                        <td width="18%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->fragrancess)&&($medical_history->fragrancess)==1){ echo "checked";} ?>  name="fragrancess" id="fragrancessy"  required  > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="fragrancess" id="fragrancessn" <?php if(!empty($medical_history->fragrancess)&&($medical_history->fragrancess)==0){ echo "checked";} if(empty($medical_history->fragrancess)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="fragrancest" value="<?php if(!empty($medical_history->fragrancest)&&($medical_history->fragrancest)!=NULL){ echo $medical_history->fragrancest;} ?>"  id="fragrancest" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="18%">
                                                            <br>
                                                            <p>Others (অন্যান্য।):</p>
                                                        </td>
                                                        <td width="18%">
                                                            <input type="radio" value="1" <?php if(!empty($medical_history->others)&&($medical_history->others)==1){ echo "checked";} ?>  name="others" id="othersy"  required  > Yes (হ্যাঁ) &nbsp;&nbsp;
                                                            <input type="radio" value="0" name="others" id="othersn" <?php if(!empty($medical_history->others)&&($medical_history->others)==0){ echo "checked";} if(empty($medical_history->others)){  echo "checked";} ?> > No (না)
                                                        </td>
                                                        <td>
                                                            <input type="text" name="othert" id="othert" value="<?php if(!empty($medical_history->othert)&&($medical_history->othert)!=NULL){ echo $medical_history->othert;} ?>" placeholder="Please explain " class="form-control" >
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <hr>
                                            <button id="save_medical_history" type="submit" class="btn btn-success"><i class="fa fa-save"></i>  Save</button>
                                             <?php
                                               if(!empty($medical_history)){
                                                ?>
                                               <a href="{{route('employee.download_ems',$employee->id)}}"  class="btn btn-primary btn-md"><i class="fa fa-download"></i> Download Medical History Report</a>
                                                <?php } ?>
                                            {{ Form::close() }}


                                          </div>

                                          <div class="tab-pane animated zoomIn" id="tab3_2">
                                            <h3><strong>Police Verification</strong>."</h3>
                                            
                                            <?php
                                                $police_verification_data=DB::table('tbpolice_verification_data')->where('empId','=',$employee->id)->take(1)->get();
                                                if(($police_verification_data->isEmpty())){
                                                ?>
                                                <p class="padLeft20 padTop10"><a target="_BLANK" href="{{route('employee.generate_pva',$employee->id)}}"  class="btn btn-danger btn-md animated zoomIn"><i class="fa fa-retweet"></i> Generate Police Verification Application</a></p>
                                                <?php }else{ ?>
                                                <p class="padLeft20 padTop10"><a href="{{route('employee.download_pva',$employee->id)}}"  class="btn btn-danger btn-md  animated zoomIn"><i class="fa fa-download"></i> Download Police Verification Application</a></p>
                                                <?php } ?>
                                          </div>
                                          
                                          <div class="tab-pane animated zoomIn" id="tab3_3">
                                            <div class="row column-seperation">
                                              <div class="col-md-12">
                                                <p class="light">
                                                    <div class="panel-group">
                                                        @foreach($attachments as $att)
                                                            <div class="panel panel-default">

                                                                <div class="panel-heading">
                                                                    <h3>
                                                                        <strong> {{$att->empAttachmentTitle}}</strong>
                                                                        <a href="{{route('attachments.edit',$att->id)}}" class="show-other-edit edu-edit-button" title="Edit"><i class="fa fa-edit"></i></a>
                                                                        <a href="{{route('attachments.show', $att->id)}}" class="edu-dlt-button show-other-delete" title="Delete"><i class="fa fa-trash-o"></i></a>
                                                                    </h3>



                                                                </div>
                                                                <div class="panel-body">
                                                                    <p>Description: <strong>{{$att->empAttachmentDescription}}</strong></p>

                                                                    @if($att->empAttachment)
                                                                        <a target="_blank" href="{{asset('Employee_Attachments/'.$att->empAttachment)}}"><p><button type="button" class="btn btn-custom-download"><i class="fa fa-download"></i> Download Attachment</button></p></a>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                        @endforeach
                                                    </div>

                                                    <div class="panel-group">
                                                        <center><a href="#" data-toggle="modal" data-target="#employeeAttachment" class="btn btn-blue btn-md"><i class="fa fa-graduation-cap"></i>Add Attachments</a></center>
                                                    </div>


                                                </p>
                                              </div>
                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                    </div>


                            </div>
                            <div class="tab-pane fade" id="tab2_8">
                                <div class="panel-group">
                                    @foreach($training_history as $th)
                                        <div class="panel panel-default">

                                            <div class="panel-heading">
                                                <h3>
                                                    <strong> {{$th->training_name}}</strong>
                                                    <a href="{{route('training.edit', $th->id) }}" class="show-other-edit edu-edit-button" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('training.delete.show', $th->id )}}" class="edu-dlt-button show-other-delete" title="Delete"><i class="fa fa-trash-o"></i></a>
                                                </h3>



                                            </div>
                                            <div class="panel-body">
                                                <p>Description: <strong>{{$th->training_description}}</strong></p>
                                                <p>Institution: <strong>{{$th->training_institution}}</strong></p>
                                                <p>Starting Date: <strong>{{$th->training_start}}</strong></p>
                                                <p>Ending Date: <strong>{{$th->training_end}}</strong></p>

                                                @if($th->training_attachment)
                                                    <a target="_blank" href="{{asset('Training_Attachments/'.$th->training_attachment)}}"><p><button type="button" class="btn btn-custom-download"><i class="fa fa-download"></i> Download Attachment</button></p></a>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach

                                    @foreach($company_training_history as $cth)
                                        <div class="panel panel-default">

                                            <div class="panel-heading">
                                                <h3>
                                                    <strong> {{$cth->training_name}} (Internal)</strong>
                                                </h3>



                                            </div>
                                            <div class="panel-body">
                                                <p>Description: <strong>{{$cth->description}}</strong></p>
                                                <p>Institution: <strong class="green-text">{{$companyInformation->company_name}}</strong></p>
                                                <p>Starting Date: <strong>{{$cth->training_starting_date}}</strong></p>
                                                <p>Ending Date: <strong>{{$cth->training_ending_date}}</strong></p>

                                                @if($cth->attachment!="No Attachment")
                                                    <a target="_blank" href="{{asset('Training_Attachment/'.$cth->attachment)}}"><p><button type="button" class="btn btn-custom-download"><i class="fa fa-download"></i> Download Attachment</button></p></a>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach

                                    <div class="panel-group">
                                        <center><a href="#" data-toggle="modal" data-target="#newTrainingHistory" class="btn btn-blue btn-md"><i class="fa fa-graduation-cap"></i>Add Training</a></center>
                                    </div>

                                </div>

                            </div>

                            <div class="tab-pane fade" id="tab2_6">
                                @if($checkSal==0)
                                    <div class="row">
                                        <div class="col-md-12 portlets">
                                            <div class="panel">
                                                <div class="panel-header-danger">
                                                    <h3><strong>No previous</strong> salary record found.</h3>
                                                </div>
                                                <div class="panel-header">
                                                    <h3>Add <strong>salary</strong></h3>
                                                </div>
                                                <div class="panel-content">
                                                    {{Form::open(array('url' => 'salary_store','method' => 'post'))}}
                                                    <div class="form-group">
                                                        <input type="hidden" name="emp_select" value="{{session('emp_id')}}">
                                                        <input type="hidden" name="current_month" value="{{date('m-Y')}}">
                                                        <div class="form-group">
                                                            <label for="">Select Grade</label>
                                                            <select required name="grade_select" id="grade_select" class="form-control">
                                                                <option value="grade_null">Select Grade</option>
                                                                @foreach($payGrade as $geade)
                                                                    <option value="{{$geade->id}}">{{$geade->grade_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label for="gross"><b>Gross Salary</b></label>
                                                            <input type="number" onkeyup="recalculatesalary()" step="any" name="gross" class="form-control form-white" id="gross" placeholder="Amount" required >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="basic">Basic</label>
                                                            <input type="text" readonly name="basic" class="form-control form-white" id="basic" placeholder="Amount" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="house">House Rent</label>
                                                            <input type="text" readonly name="houserant" class="form-control form-white" id="house" placeholder="Amount" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="medical">Medical</label>
                                                            <input type="number" step="any" onkeyup="recalculatesalary1()" name="medical" class="form-control form-white" id="medical" placeholder="Amount" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="transport">Transportation</label>
                                                            <input type="number" step="any" onkeyup="recalculatesalary1()" name="transport" class="form-control form-white" id="transport" placeholder="Amount" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="food">Food</label>
                                                            <input type="number" step="any" onkeyup="recalculatesalary1()" name="foods" class="form-control form-white" id="food" placeholder="Amount" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="other">Others</label>
                                                            <input type="number" step="any" onkeyup="recalculatesalary1()" name="others" class="form-control form-white" id="other" placeholder="Amount" required>
                                                        </div>

                                                        <button id="salary_check" type="submit" class="btn btn-primary">Save</button>
                                                        {{ Form::close() }}


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {{--</div>--}}
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        setTimeout(function() {
                                            $('#alert_message').fadeOut('fast');
                                        }, 5000);
                                        $("#salary_check").click(function(){
                                            var emp = document.getElementById("emp_select");
                                            var grade = document.getElementById("grade_select");
                                            if(emp.value == "emp_null") {
                                                alert('Select Employee');
                                                return false;
                                            }
                                            if(grade.value == "grade_null"){
                                                alert('Select Grade');
                                                return false;
                                            }
                                        });
                                        $('select[name="grade_select"]').on('change', function() {
                                            var gradeid = $(this).val();
                                            var grade = document.getElementById("grade_select");
                                            if(grade.value=='grade_null'){
                                                document.getElementById('basic').value='';
                                                document.getElementById('house').value='';
                                                document.getElementById('medical').value='';
                                                document.getElementById('transport').value='';
                                                document.getElementById('food').value='';
                                                document.getElementById('other').value='';
                                            }
                                            $.ajaxSetup({
                                                headers: {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                }
                                            })
                                            $.ajax({
                                                type: 'POST',
                                                url: '../gradeajax'+'/'+gradeid,
                                                success:function(data) {
                                                    $.each(data, function(key, value) {
                                                        var basic_salary=value.basic_salary;
                                                        var house_rant=value.house_rant;
                                                        var transport=value.transport;
                                                        var medical=value.medical;
                                                        var food=value.food;
                                                        var others=value.others;
                                                        document.getElementById('basic').value=basic_salary;
                                                        document.getElementById('house').value=house_rant;
                                                        document.getElementById('medical').value=transport;
                                                        document.getElementById('transport').value=medical;
                                                        document.getElementById('food').value=food;
                                                        document.getElementById('other').value=others;
                                                        recalculategross();
                                                    });
                                                }
                                            });
                                        });
                                    });
                                </script>

                                @else
                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3>
                                                    Present Salary: <strong> @if(isset($sal->total_employee_salary)){{$sal->total_employee_salary}} @else 0 @endif Taka</strong>
                                                    <a class="btn btn-blue btn-sm sal-open edu-edit-button open_modal" title="Edit"><i class="fa fa-edit"></i> Edit</a>
                                                    <button class="no-display" id="salary_grade_modal" value=@if(isset($sal->id))"{{$sal->id}}"@else 0 @endif></button>

                                                </h3>
                                            </div>
                                            @if(isset($sal->total_employee_salary))
                                                <div class="panel-body">
                                                    <p>Salary Grade: <strong>{{ $sal->grade_name}}</strong></p>
                                                    <p>Basic Salary: <strong>{{ floor($sal->basic_salary) }} Taka</strong></p>
                                                    <p>House Rent: <strong>{{ floor($sal->house_rant) }} Taka</strong></p>
                                                    <p>Medical: <strong>{{ floor($sal->medical) }} Taka</strong></p>
                                                    <p>Transport: <strong>{{ floor($sal->transport) }} Taka</strong></p>
                                                    <p>Food: <strong>{{ floor($sal->food) }}</strong> Taka</p>
                                                    <p>Other: <strong>{{ floor($sal->other) }}</strong> Taka</p>

                                                </div>
                                            @endif
                                        </div>

                                    </div>
                                @endif
                                <?php if(!empty($sal)){ ?>
                                    <div class="modal fade" id="employeesalarymodaledit" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 style="font-weight: bold;" class="modal-title"><i class="fa fa-edit"></i> Salary Update</h4>
                                                </div>
                                                <br>
                                                <br>
                                                <b><h2 class="text-center" style="font-weight: bold;">Employee: <span id="emp_name" ></span></h2></b>
                                                <div class="modal-body">
                                                    {{Form::open(array('route' => 'salary.employee_salary_update','method' => 'post'))}}
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="pwd">Select Grade:</label>
                                                            <select id="grade_employee_select1" disabled="" name="grade_employee_select1" class="form-control">
                                                                <option>Select Grade</option>
                                                                @foreach($payGrade as $grade)
                                                                    <option <?php if(!empty($sal)){if($sal->designationGradeId==$grade->id){ echo 'selected';}} ?> value="{{$grade->id}}">{{$grade->grade_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <input type="hidden" name="salary_grade_url" id="salary_grade_url" value="{{URL::to('/employee/salary/grade/modals')}}">
                                                        <input type="hidden" name="grade_url" id="grade_url" value="{{URL::to('/employee/grade/salary/update')}}">
                                                        <input type="hidden" name="emp_grade_id" id="current_grade_id" value="">
                                                        <input type="hidden" id="emp_idsss" name="emp_ac_id" value="">
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="pwd">Gross:</label>
                                                            <input type="number" step="any" id="grossu" name="gross" onkeyup="recalculatesalary_u()" class="form-control form-white" value="{{($sal->basic_salary+$sal->house_rant+$sal->medical+$sal->transport+$sal->food+$sal->other)}}" required >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="pwd">Basic:</label>
                                                            <input type="number" readonly  onkeyup="recalculatesalary_up()"  step="any" id="basicu" name="basic" class="form-control form-white" value="{{ floor($sal->basic_salary) }}" required >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="pwd">House rant:</label>
                                                            <input type="number" readonly step="any"  id="houseu" name="house_rant" class="form-control form-white" value="{{($sal->house_rant) }}" required >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="pwd">Medical:</label>
                                                            <input type="number" readonly step="any" id="medicalu" name="medical" class="form-control form-white" value="{{ floor($sal->medical) }}" required >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="pwd">Transport:</label>
                                                            <input type="number" readonly step="any" id="transportu" name="transport" class="form-control form-white" value="{{ floor($sal->transport) }}" required >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="pwd">Food:</label>
                                                            <input type="number" readonly step="any"  id="foodsu" name="foods" class="form-control form-white" value="{{ floor($sal->food) }}" required >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="pwd">Other:</label>
                                                            <input type="number" readonly step="any" id="othersu" name="others" class="form-control form-white" value="{{ floor($sal->other) }}" required >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <hr>
                                                    <button onclick="return confirm('Are you sure?')" name="submit" type="submit" class="btn btn-success"> Update</button>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="actual_id" name="salary_hidden_id" value="">
                                                <div class="modal-footer">
                                                </div>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                            <script type="text/javascript">
                                function recalculatesalary_u(){
                                    var gross=parseFloat($("#grossu").val());
                                    $("#medicalu").val('600.00');
                                    $("#transportu").val('350.00');
                                    $("#foodsu").val('900.00');
                                    $("#othersu").val('0.00');


                                    var medical=parseFloat($("#medicalu").val());
                                    var transport=parseFloat($("#transportu").val());
                                    var food=parseFloat($("#foodsu").val());
                                    var other=parseFloat($("#othersu").val());
                                    var deduction = (medical+transport+food+other);
                                    var basic = ((gross-deduction)/1.5);
                                    $("#basicu").val(basic.toFixed());
                                    var house = ((basic/100)*50);
                                    $("#houseu").val(house.toFixed());
                                    // alert(deduction);
                                }

                            </script>

                            </div>


                            <div class="tab-pane fade" id="tab2_9">
                                <p class="padLeft20 padTop10"><a target="_blank" href="{{route('employee.id_card',$employee->id)}}"  class="btn btn-success btncus btn-md animated zoomIn"><i class="fa fa-download"></i> পরিচয়পত্র (Identity card)</a></p>
                                <p class="padLeft20 padTop10"><a href="{{route('employee.job_application_form',$employee->id)}}"  class="btn btn-success btncus btn-md animated zoomIn"><i class="fa fa-download"></i>চাকুরীর আবেদন পত্র (Job Application Letter)</a></p>
                                <p class="padLeft20 padTop10"><a href="{{route('employee.appointment_latter',$employee->id)}}"  class="btn btn-success btncus btn-md animated zoomIn"><i class="fa fa-download"></i> নিয়োগ পত্র (Appointment letter)</a></p>
                                <p class="padLeft20 padTop10"><a href="{{route('employee.employee_background_check',$employee->id)}}"  class="btn btncus btn-success btn-md animated zoomIn"><i class="fa fa-download"></i> গুরুত্বপূর্ণ তথ্যাবলী (Background Check)</a></p>
                                <p class="padLeft20 padTop10"><a href="{{route('employee.training_subject_list',$employee->id)}}"  class="btn btncus btn-success btn-md animated zoomIn"><i class="fa fa-download"></i> প্রশিক্ষনের বিষয়সমূহ (Training topics)</a></p>
                            
                                <?php if($employee->work_group=='Worker'){ ?>
                                <div class="dropdown padLeft20 padTop10  ">
                                  <button class="btn btn-success dropdown-toggle animated zoomIn " type="button" data-toggle="dropdown"><i class="fa fa-download"></i> দায়িত্ব ও কর্তব্য (Duty and Responsibility Guide)
                                  <span class="caret"></span></button>
                                  <ul class="dropdown-menu">
                                    <li><a  title="Quality Inspactor Responsibility Guide"  href="{{route('employee.quality_inspactor_responsibility_guide',$employee->id)}}">কোয়ালিটি ইন্সপেক্টর এর দায়িত্ব ও কর্তব্য </a></li>
                                    <li><a  title="Sewing Machine Operator's Duty and Responsibility"  href="{{route('employee.sewing_machine_responsibility_guide',$employee->id)}}"> সেলাই মেশিন অপারেটর এর দায়িত্ব ও কর্তব্য </a></li>
                                    <li><a  title=""  href="{{route('employee.sewing_assistant_machine_responsibility_guide',$employee->id)}}">সহকারী সেলাই মেশিন অপারেটরের দায়িত্ব ও কর্তব্য </a></li>
                                    <li><a  title=""  href="{{route('employee.finishing_assistant_responsibility_guide',$employee->id)}}">ফিনিশিং সহকারী এর দায়িত্ব ও কর্তব্য</a></li>
                                    <li><a  title="Line Ironman Responsibility Guide" href="{{route('employee.line_ironman_responsibility_guide',$employee->id)}}">লাইন আইরনম্যান দায়িত্ব ও কর্তব্য  </a></li>
                                  </ul>
                                </div>
                                <br>
                                <?php } ?>
                                
                                <p class="padLeft20 padTop10"><a href="{{route('employee.age_ability_certificate',$employee->id)}}"  class="btn btncus btn-success btn-md animated zoomIn"><i class="fa fa-download"></i> বয়স ও সক্ষমতার প্রত্যয়ন পত্র (Certification of age and competence) </a></p>
                                <p class="padLeft20 padTop10"><a title="Payment announcement and nomination forms" href="{{route('employee.nominee_form_pdf',$employee->id)}}"  class="btn btn-success btncus btn-md animated zoomIn"><i class="fa fa-download"></i> অর্থ পরিশোধের ঘোষনা ও মনোনয়নের ফরম </a></p>
                                 <?php
                                   if(!empty($medical_history)){
                                    ?>
                                   <p class="padLeft20 padTop10"><a href="{{route('employee.download_ems',$employee->id)}}"  class="btn btn-dark btncus btn-md animated zoomIn"><i class="fa fa-download"></i> Download Medical History Report</a></p>
                                    <?php } ?>
                                <?php
                                if(($police_verification_data->isEmpty())){
                                ?>
                                <p class="padLeft20 padTop10"><a target="_BLANK" href="{{route('employee.generate_pva',$employee->id)}}"  class="btn btn-primary btncus btn-md animated zoomIn"><i class="fa fa-retweet"></i> Generate Police Verification Application</a></p>
                                <?php }else{ ?>
                                <p class="padLeft20 padTop10"><a href="{{route('employee.download_pva',$employee->id)}}"  class="btn btn-primary btncus btn-md animated zoomIn"><i class="fa fa-download"></i> Download Police Verification Application</a></p>
                                <?php } ?>
                                <!-- <p class="padLeft20 padTop10"><a href="{{route('company.recruitment_policy')}}"  class="btn btn-warning btncus btn-md animated zoomIn"><i class="fa fa-download"></i> Download Recruitment Policy</a></p> -->
                                <p class="padLeft20 padTop10"><a href="{{route('employee.resignation_letter', $employee->id)}}"  class="btn btncus btn-dark btn-md animated zoomIn"><i class="fa fa-download"></i> Download Resignation Letter</a></p>
                                @if($employee->date_of_discontinuation !=null)
                                    <p class="padLeft20 padTop10"><a href="{{route('employee.final_settlement', $employee->id)}}"   class="btn btncus btn-danger btn-md animated zoomIn"><i class="fa fa-download"></i> Download Final Settlement Form</a></p>
                                @else
                                    <p class="padLeft20 padTop10"><a href="#" class="btn btn-md btncus btn-danger animated zoomIn" data-toggle="modal" data-target="#dod"><i class="fa fa-download"></i> Download Final Settlement Form</a></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Edit Password-->

    @include('employee.modal.editPassword')
    @include('employee.modal.date_of_discontinuation')
    <!--End Of Edit Password-->
    <!--New Education-->
    @include('employee.modal.newEmployeeEducation')
    <!--End Of New Education-->
    <!--Edit Profile Picture Modal-->
    @include('employee.modal.editProfilePicture')
    <!--End Of Edit Profile Picture Modal-->
    <!--New Work Experience-->
    @include('employee.modal.newWorkExp')
    <!--End Of New Work Experience-->
    <!--New Work Experience-->
    @include('employee.modal.newEmployeeAttachment')
    <!--End Of New Work Experience-->
    @include('employee.modal.editWorkingExperience')
    @include('employee.modal.deleteWorkingExperience')
    @include('employee.modal.editOther')
    {{--@include('employee.modal.date_of_discontinuation')--}}
    @include('employee.modal.deleteOtherAttachment')
    @include('employee.modal.editNominee')
    @include('employee.modal.newNominee')
    @include('employee.modal.deleteNominee')
    @include('employee.modal.skillTest')
    @include('employee.modal.newTrainingHistory')
    <script>
      
// $("input[name='others']").change(function(){
//   alert( "Handler for .click() called." );
//
// });

//        <script>
        $(document).on('click','.open_modal',function(){
            var id = $("#salary_grade_modal").val();
            var url = $('#salary_grade_url').val();
            var urlid=url+'/'+id;
            var formData = {
                id: $(this).val(),
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                type: "GET",
                url: urlid,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#employeesalarymodaledit').modal('show');
                    $.each(data, function (i, item) {
                        var empid=document.getElementById('emp_idsss').value= item.emp_id;
                        var acid=document.getElementById('actual_id').value= item.default_id;
                        var gradeid=document.getElementById('current_grade_id').value= item.grades_id;
                        var empname=document.getElementById('emp_name').innerHTML=item.empFirstName+item.empLastName;
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
$(document).ready(function(){
    $("#grade_employee_select").change(function(){
        var url = $('#grade_url').val();
        var id=$("#grade_employee_select").val();
        var urlid=url+'/'+id;
        var formData = {
            id: $('#grade_employee_select').val(),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        $.ajax({
            type: "GET",
            url: urlid,
            data: formData,
            dataType: 'json',
            success: function (data) {
                $.each(data, function (i, item) {
                    var gradeid=document.getElementById('current_grade_id').value= item.id;
                    var gradename=document.getElementById('current_grade').value= item.grade_name;
                    var basic=document.getElementById('basic').value= item.basic_salary;
                    var house=document.getElementById('house_rant').value= item.house_rant;
                    var medical=document.getElementById('medical').value= item.medical;
                    var transport=document.getElementById('transport').value= item.transport;
                    var food=document.getElementById('foods').value= item.food;
                    var others=document.getElementById('others').value= item.others;
                });
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
});
    {{--</script>--}}
        $('.show-nominee-delete').click(function (event) {
            event.preventDefault();
//            alert('fuck');
            var url=$(this).attr('href');

            $.ajax({
                url:url,
                method:'get',
                success:function(response){
                    $('#nominee-delete-form').html(response);

                }
            });
            $('#deleteNominee').modal('show');


        });


        $('.show-nominee-edit').click(function (event) {
            event.preventDefault();
            var url=$(this).attr('href');
//            alert(url);
            $.ajax({
                url:url,
                method:'get',
                dataType:'html',
                success:function (response) {
                    $('#nominee-edit-form').html(response);
                }

            });
            $('#editNomineeModal').modal('show');

        });

        $('.show-other-edit').click(function (event) {
            event.preventDefault();
            var url=$(this).attr('href');

            $.ajax({
                url:url,
                dataType:'html',
                success:function (response) {
                    $('#other-edit-form').html(response);

                }
            });

            $('#editOther').modal('show');

        });

        $('.show-other-delete').click(function (event) {
            event.preventDefault();
            var url=$(this).attr('href');
            $.ajax({
                url:url,
                dataType:'html',
                success:function (response) {
                    $('#other-attachment-delete-form').html(response);

                }

            });
            $('#deleteOther').modal('show');

        });

        $('.show-delete-we-modal').click(function (event) {
            event.preventDefault();
//            alert('something');
            var url=$(this).attr('href');
//            console.log(url);
//            var method='DELETE';
            $.ajax({
                url:url,
//                method:method,
                dataType:'html',
                success:function (response) {
                    $('#working-experience-delete-form').html(response);

                }

            });
            $('#deleteWorkingExperience').modal('show');

        });

        $('.show-edit-we-modal').click(function (event) {
            event.preventDefault();
//            alert("c");
            var url=$(this).attr('href');
//            console.log(url);
            $.ajax({
                url:url,
                dataType:'html',
                success:function (response) {
                    $('#working-experience-edit-form').html(response);

                },
                error:function (xhr) {
                    console.log(xhr);

                }
            });
            $('#editWorkingExperience').modal('show');

        });


//        $('#update-working-experience').click(function (event) {
//            event.preventDefault();
//             $.ajaxSetup({
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                }
//             });
//            var form=$('#working-experience-edit-form').find('form'),
//                method="PATCH";
//            var url=form.attr('action');
////            var formData = new FormData(form);
////            formData.append('file', $('input[type=file]')[0].files[0]);
////            console.log(url);
//            $.ajax({
//                url:url,
//                method:method,
//                async:false,
////                data:form.serialize(),
//                data:new FormData($("#editFrom")[0]),
//                processData: false,
//                contentType: false,
//                success:function (response) {
//                    console.log(response);
//
//                },
//                error:function (xhr) {
//
//                }
//
//            });
//
////            alert('hello world');
//
//
//        });

        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <script type="text/javascript">
            function recalculategross(){

                var basic=parseFloat($("#basic").val());
                var house=parseFloat($("#house").val());
                var medical=parseFloat($("#medical").val());
                var transport=parseFloat($("#transport").val());
                var food=parseFloat($("#food").val());
                var other=parseFloat($("#other").val());

                var gross = (basic+house+medical+transport+food+other);
                
                $("#gross").val(gross.toFixed(2));

            }
            recalculategross();

            function recalculatesalary(){
                var gross=parseFloat($("#gross").val());
                $("#medical").val('600.00');
                $("#transport").val('350.00');
                $("#food").val('900.00');
                $("#other").val('0.00');


                var medical=parseFloat($("#medical").val());
                var transport=parseFloat($("#transport").val());
                var food=parseFloat($("#food").val());
                var other=parseFloat($("#other").val());
                var deduction = (medical+transport+food+other);
                var basic = ((gross-deduction)/1.5);
                $("#basic").val(basic.toFixed());
                var house = ((basic/100)*50);
                $("#house").val(house.toFixed());
            }

            function recalculatesalary1(){
                var gross=parseFloat($("#gross").val());
                var medical=parseFloat($("#medical").val());
                var transport=parseFloat($("#transport").val());
                var food=parseFloat($("#food").val());
                var other=parseFloat($("#other").val());
                var deduction = (medical+transport+food+other);
                var basic = ((gross-deduction)/1.5);
                $("#basic").val(basic.toFixed());
                var house = ((basic/100)*50);
                $("#house").val(house.toFixed());
            }

    </script>
    @include('include.copyright')
@endsection





