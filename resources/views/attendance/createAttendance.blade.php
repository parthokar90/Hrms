@extends('layouts.master')
@section('title', 'Manual Attendance')
@section('content')
    <div class="page-content">
        @if(Session::has('success'))
        <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
        @elseif(Session::has('error'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
        @endif
        <div class="row">
            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
            @endif
            <div class="col-md-4">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Create Manual </strong>Attendance</h2>
                    </div>
                 
                    <div class="panel-body bg-white">
                        <div class="">
                            {!! Form::open(['method'=>'POST', 'action'=>'AttendanceController@manualAttendance']) !!}
                                <div class="required form-group">
                                    <label class="control-label">Shift</label>
                                    <select name="shift_id" id="shift_id" class="form-control" data-search="true" required>
                                        @foreach($shift as $shifts)
                                            <option value="{{$shifts->id}}">{{$shifts->shiftName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="required form-group">
                                    <label class="control-label">Employee Name Or ID</label>
                                    <input type="text" id="search_text" name="employee" autocomplete="off" class="form-control" placeholder="Search by Name or ID" required>
                                </div>

                                <div class="required form-group">
                                    <label class="control-label">Date</label>
                                    <div class="append-icon">
                                        <input type="text" name="date" autocomplete="off" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                                <div class="required form-group">
                                    <label class="control-label">In Time(HH:MM am/pm)</label>
                                    <div class="append-icon">
                                        <input type="time" name="stime" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Out Time(HH:MM am/pm)</label>
                                    <div class="append-icon">
                                        <input type="time" name="etime" class="form-control">
                                    </div>
                                </div>
                                <div class="text-center  m-t-20">
                                    <button type="submit" class="btn btn-embossed btn-primary">Create</button>
                                    <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Create Manual </strong>Attendance</h2>
                    </div>
                 
                    <div class="panel-body bg-white">
                        <div class="">
                            {!! Form::open(['method'=>'POST', 'action'=>'AttendanceController@manualAttendancedept']) !!}
                            <div class="required form-group">
                                <label class="control-label">Shift</label>
                                <select name="shift_id" id="shift_id" class="form-control" data-search="true" required>
                                    @foreach($shift as $shifts)
                                        <option value="{{$shifts->id}}">{{$shifts->shiftName}}</option>
                                    @endforeach
                                </select>
                            </div>
                                <div class="required form-group">
                                    <label class="control-label">Select Department</label>
                                    <select name="dept_id" id="dept_id" class="form-control" data-search="true">
                                        @foreach($department as $emp)
                                            <option value="{{$emp->id}}">{{$emp->departmentName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                    
                                <div class="required form-group">
                                    <label class="control-label">Date</label>
                                    <div class="append-icon">
                                        <input type="text" name="date" autocomplete="off" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                                <div class="required form-group">
                                    <label class="control-label">In Time(HH:MM am/pm)</label>
                                    <div class="append-icon">
                                        <input type="time" name="stime" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Out Time(HH:MM am/pm)</label>
                                    <div class="append-icon">
                                        <input type="time" name="etime" class="form-control">
                                    </div>
                                </div>
                                <div class="text-center  m-t-20">
                                    <button type="submit" class="btn btn-embossed btn-primary">Create</button>
                                    <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Create Manual </strong>Attendance</h2>
                    </div>
                 
                    <div class="panel-body bg-white">
                        <div class="">
                            {!! Form::open(['method'=>'POST', 'action'=>'AttendanceController@manualAttendancesection']) !!}
                            <div class="required form-group">
                                <label class="control-label">Shift</label>
                                <select name="shift_id" id="shift_id" class="form-control" data-search="true" required>
                                    @foreach($shift as $shifts)
                                        <option value="{{$shifts->id}}">{{$shifts->shiftName}}</option>
                                    @endforeach
                                </select>
                            </div>
                                <div class="required form-group">
                                    <label class="control-label">Select Section</label>
                                    <select name="section_id" class="form-control" data-search="true">
                                        @foreach($section as $emp)
                                            <option value="{{$emp->empSection}}">{{$emp->empSection}}</option>
                                        @endforeach
                                    </select>
                                </div>
                    
                                <div class="required form-group">
                                    <label class="control-label">Date</label>
                                    <div class="append-icon">
                                        <input type="text" name="date" autocomplete="off" class="date-picker form-control" placeholder="Select a date..." required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                                <div class="required form-group">
                                    <label class="control-label">In Time(HH:MM am/pm)</label>
                                    <div class="append-icon">
                                        <input type="time" name="stime" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Out Time(HH:MM am/pm)</label>
                                    <div class="append-icon">
                                        <input type="time" name="etime" class="form-control">
                                    </div>
                                </div>
                                <div class="text-center  m-t-20">
                                    <button type="submit" class="btn btn-embossed btn-primary">Create</button>
                                    <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <script>
        $(document).ready(function () {
            var url = "{{ route('autocomplete.employee') }}";
            $('#search_text').typeahead({
                minLength: 2,
                source:  function (query, result) {
                    $.ajax({
                        url: url,
                        data: 'query=' + query,
                        dataType: "json",
                        type: "GET",
                        success: function (data) {
                            result($.map(data, function (item) {
                                return item;
                            }));
                        }
                    });

                }

            });
        });




        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 10000);

    </script>
    @include('include.copyright')

@endsection
