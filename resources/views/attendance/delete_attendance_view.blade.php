@extends('layouts.master')
@section('title', 'Manual Edit Attendance')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('success'))
                <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
            @endif
            @if(Session::has('error'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
            @endif
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Delete </strong>Attendance Record</h2>
                    </div>
                    <div class="panel-body bg-white">
                        <div class="">
                           <table class="table table-bordered">
                            @foreach($att_record as $ar)
                               <tr>
                                   <td width="20%">Employee Name</td>
                                   <td>{{$ar->empFirstName." ".$ar->empLastName}}</td>
                               </tr>
                               <tr>
                                   <td>Employee ID</td>
                                   <td>{{$ar->employeeId}}</td>
                               </tr>
                               <tr>
                                   <td>Employee Card Number</td>
                                   <td>{{$ar->empCardNumber}}</td>
                               </tr>
                               <tr>
                                   <td>Date</td>
                                   <td>{{$ar->date}}</td>
                               </tr>
                               <tr>
                                   <td>In time</td>
                                   <td>{{$ar->in_time}}</td>
                               </tr>
                               <tr>
                                   <td>Out Time</td>
                                   <td>{{$ar->out_time}}</td>
                               </tr>
                               <tr>
                                   <td></td>
                                   <td>
                                        <a onclick="return confirm('Are you sure?')" href="{{route('attendance.delete_record',$ar->id)}}"   title="Delete Attendance Record" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i> Delete Selected Attendance Record</a>
                                   </td>
                               </tr>
                            @endforeach
                           </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
        
        $(function() {
            $("#date").datepicker({ dateFormat: "yy-mm-dd" });
        });
    </script>
    @include('include.copyright')

@endsection
