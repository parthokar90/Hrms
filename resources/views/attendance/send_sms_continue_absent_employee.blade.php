@extends('layouts.master')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                        <div class="col-md-6">
                            <h3 ><i class="fa fa-users "></i> <strong> Continued Absent</strong> Employee List</h3>
                        </div>
                        <div class="col-md-6" style="margin-top:8px;">
                            <a href="{{route('employee.continue_absent_list_autosms')}}" class="btn btn-danger btn-round btn-md"  style="float:right;" ><i class="fa fa-envelope"></i> Send Auto SMS</a>
                        </div>
                      
                       
                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        
                        @if(Session::has('smsnotification'))
                            <p id="alert_message" class="alert alert-success">{{ Session::get('smsnotification') }}</p>
                        @endif
                        @if(Session::has('emailnotification'))
                            <p id="alert_message" class="alert alert-success">{{ Session::get('emailnotification') }}</p>
                        @endif
                        @if(Session::has('emailnotificationfailed'))
                            <p id="alert_message" class="alert alert-danger">{{ Session::get('emailnotificationfailed') }}</p>
                        @endif
                        @if(Session::has('smsnotificationfailed'))
                            <p id="alert_message" class="alert alert-danger">{{ Session::get('smsnotificationfailed') }}</p>
                        @endif
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Employee Id</th>
                                <th>Designation</th>
                                <th>Department</th>
                                <th>Absent Days</th>
                                <th>Last Present Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ttDays as $employee)
                                <tr>
                                    <td>{{$employee->empFirstName ." ".$employee->empLastName}}</td>
                                    <td>{{$employee->employeeId}}</td>
                                    <td>{{$employee->designation}}</td>
                                    <td>{{$employee->departmentName}}</td>
                                    <td>{{$employee->day_between}}</td>
                                    <td>{{\Carbon\Carbon::parse($employee->date)->format('d-M-Y')}}</td>
                                   
                                    <td>
                                     {!! Form::open(['method'=>'POST','action'=>'EmployeeController@continueAbsentEmployeeNotification']) !!}
                                        
                                        <input name="id" value="{{$employee->id}}" hidden >
                                        <input name="days" value="{{$employee->day_between}}" hidden >
                                        <input name="lastPresent" value="{{$employee->date}}" hidden >

                                        <button type="submit" value="downloadletter" name="notificationType" class="btn btn-info btn-sm"><i class="fa fa-download"></i> &nbsp; Download Letter </button>
                                        <button type="submit" value="sentsms" name="notificationType" class="btn btn-blue btn-sm"><i class="fa fa-mobile"></i> &nbsp; Send SMS </button>
                                        <button type="submit" value="sentmail" name="notificationType" class="btn btn-success btn-sm"><i class="fa fa-envelope"></i> &nbsp; Send Email</button>
                                    {!! Form::close() !!}

                                    </td>

                                </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
    <script>
      $(document).ready(function(){
          setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
      });
    </script>
@endsection