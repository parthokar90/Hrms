@extends('layouts.master')
@section('title', 'Manage Work Shifts')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-gavel"></i> <strong>Manage Shifts</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="panel-content well">
                        <h3><i class="fa fa-plus-square"></i> <b>New Shift Information</b></h3><hr>
                            {{Form::open(array('url' => 'attendance/settings/store','method' => 'post'))}}
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="pwd">Shift Name:</label>
                                <input type="text" style="padding:15px" name="shiftName" class="form-control form-white" placeholder="Shift Name" required maxlength="20">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="pwd">Entry Time:</label>
                                <input type="time" name="entry_time" class="form-control form-white" placeholder="only number" required>
                            </div>

                            <div class="form-group">
                                <label for="pwd">Maximum Entry Time:</label>
                                <input type="time" name="max_entry_time" class="form-control form-white" placeholder="only number" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="pwd">Exit Time:</label>
                                <input type="time" name="exit_time" class="form-control form-white" placeholder="only number" required>
                            </div>
                            <div class="form-group">
                                <label for="pwd">Minimum Exit Time:</label>
                                <input type="time" name="min_exit_time" class="form-control form-white" placeholder="only number" required>
                            </div>
                        </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Create New Shift</button>
                            </div>                            
                            {{ Form::close() }}
                        </div>
                        <hr>
                        <table class="table-responsive table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Order</th>
                                <th>Shift Name</th>
                                <th>Entry Time</th>
                                <th>Max: Entry Time</th>
                                <th>Exit Time</th>
                                <th>Min Exit Time</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $order=0; @endphp
                            @foreach($data as $setup)
                                @php $order++; @endphp
                            <tr>
                                <td>{{$order}}</td>
                                <td>{{$setup->shiftName}}</td>
                                <td>{{date("g:i A",strtotime($setup->entry_time))}}</td>
                                <td>{{date("g:i A",strtotime($setup->max_entry_time))}}</td>
                                <td>{{date("g:i A",strtotime($setup->exit_time))}}</td>
                                <td>{{date("g:i A",strtotime($setup->min_exit_time))}}</td>
                                <td><a data-toggle="modal" data-target="#{{$setup->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>
                                    <a data-toggle="modal" data-target="#delete{{$setup->id}}" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a></td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade" id="{{$setup->id}}" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Update</h4>
                                        </div>
                                        <div class="modal-body">
                                            {{Form::open(array('url' => 'attendance/settings/update','method' => 'post'))}}
                                            <div class="form-group">
                                                <label for="shiftName">Shift Name</label>
                                                <input type="text" name="shiftName" class="form-control form-white" required value="{{$setup->shiftName}}" placeholder="Enter Shift Name " >
                                            </div>
                                             <div class="form-group">
                                                <label for="entryTime">Entry Time</label>
                                                <input type="time" name="entry_time" class="form-control form-white" required value="{{$setup->entry_time}}" id="entryTime" placeholder="Enter Entry Time " >
                                            </div>
                                            <div class="form-group">
                                                <label for="entryTime">Max: Entry Time</label>
                                                <input type="time" name="max_entry_time" class="form-control form-white" required value="{{$setup->max_entry_time}}" id="entryTime" placeholder="Enter Maximum Entry Time " >
                                            </div>
                                            <div class="form-group">
                                                <label for="exitTime">Exit Time</label>
                                                <input type="time" name="exit_time" class="form-control form-white" required value="{{$setup->exit_time}}" id="exitTime" placeholder="Enter Exit Time " >

                                            </div>
                                            <div class="form-group">
                                                <label for="minexitTime">Minimum Exit Time</label>
                                                <input type="time" name="min_exit_time" class="form-control form-white" required value="{{$setup->min_exit_time}}" id="minexitTime" placeholder="Enter Minimum Exit Time " >

                                            </div>
                                            <input type="hidden" name="settings_id" value="{{$setup->id}}">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="delete{{$setup->id}}" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Remove Shift Information </h4>
                                        </div>
                                        <div class="modal-body">
                                            
                                            {{Form::open(array('url' => 'attendance/settings/delete','method' => 'post'))}}
                                             <div class="form-group">
                                                <h3><b>Are you sure to remove {{$setup->shiftName}}?</b></h3>
                                            </div>
                                            <hr>
                                            <input type="hidden" name="settings_id" value="{{$setup->id}}">
                                            <button type="submit" class="btn btn-danger">Remove Shift</button>
                                            <button  class="btn btn-default" type="button" class="close" data-dismiss="modal">Close</button>
                                            
                                            {{ Form::close() }}

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection