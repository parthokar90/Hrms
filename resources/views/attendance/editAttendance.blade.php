@extends('layouts.master')
@section('title', 'Manual Edit Attendance')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('success'))
                <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
            @endif
            @if(Session::has('error'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
            @endif
            <div class="col-md-4">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Edit </strong>Attendance</h2>
                    </div>
                    <div class="panel-body bg-white">
                        <div class="">
                            {!! Form::open(['method'=>'PATCH', 'action'=>['AttendanceController@updateAttendance',0]]) !!}
                            <div class="required form-group">
                                <label class="control-label">Employee Name (Id)</label>
                                <select name="emp_id" id="emp_id" class="form-control" data-search="true">
                                    @foreach($employees as $emp)
                                        <option value="{{$emp->id}}">{{$emp->empFirstName." (".$emp->employeeId.")"}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="required form-group">
                                <label class="control-label">Date</label>
                                <div class="append-icon">
                                    <input type="text" id="date" name="date" autocomplete="off" class="form-control" placeholder="Select a date..." required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                            <div id="data-field">

                            </div>
                            <div class="text-center  m-t-20">
                                <button type="submit" class="btn btn-embossed btn-primary">Update</button>
                                <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Edit </strong>Attendance</h2>
                    </div>
                    <div class="panel-body bg-white">
                        <div class="">
                            {!! Form::open(['method'=>'get', 'action'=>['AttendanceController@updateAttendancedept',0]]) !!}
                            <div class="required form-group">
                                <label class="control-label">Select Department</label>
                                <select name="dept_id" id="dept_id" class="form-control" data-search="true">
                                    @foreach($department as $emp)
                                        <option value="{{$emp->id}}">{{$emp->departmentName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="required form-group">
                                <label class="control-label">Date</label>
                                <div class="append-icon">
                                    <input type="text" name="date" autocomplete="off" class="form-control date-picker" placeholder="Select a date..." required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                          
                            <div class="text-center  m-t-20">
                                <button type="submit" class="btn btn-embossed btn-primary">Processs</button>
                                <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Edit </strong>Attendance</h2>
                    </div>
                    <div class="panel-body bg-white">
                        <div class="">
                            {!! Form::open(['method'=>'get', 'action'=>['AttendanceController@updateAttendancesection',0]]) !!}
                            <div class="required form-group">
                                <label class="control-label">Select Section</label>
                                <select name="dept_id" id="section_id" class="form-control" data-search="true">
                                    @foreach($section as $emp)
                                        <option value="{{$emp->empSection}}">{{$emp->empSection}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="required form-group">
                                <label class="control-label">Date</label>
                                <div class="append-icon">
                                    <input type="text" name="date" autocomplete="off" class="form-control date-picker" placeholder="Select a date..." required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                          
                            <div class="text-center  m-t-20">
                                <button type="submit" class="btn btn-embossed btn-primary">Processs</button>
                                <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <script>
        $(function() {
            var selected;
            var id;
//            var in_time;

            $("#date").datepicker({ dateFormat: "yy-mm-dd" });
            $("#date").on("change",function(){
                id=$('#emp_id option:selected').val();
                selected = $(this).val();
                selected=selected+","+id;
                $.ajax({
                    url:"data/"+selected,
                    method:'GET',
                    dataType:"html",
                    success:function (response) {
//                        console.log(response);
                        $("#data-field").html(response);

                    },
                    error:function (xhr) {
                        var error="<div class='alert alert-danger'> <strong>Error!</strong> No Employee Attendance Record Found for The Given Date. </div> <b>First add attendance record. Go to <a href='{{route('attendance.manual.create')}}'> Manual Attendance </a> to add Attendance</b>";
                        $("#data-field").html(error);

                    }
                });
                $("#in_time").val(in_time);

            });
        });
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

    </script>
    @include('include.copyright')

@endsection
