@extends('layouts.master')
@section('title', 'Temporary Employee Proxy Card List')
@section('content')
    <div class="page-content">
        @if(Session::has('success'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('success') }}</p>
        @endif
        @if(Session::has('error'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
        @endif

        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><i class="fa fa-table"></i> <strong>Temporary Employee Proxy Card List </strong></h3>
                            </div>
                            <div class="col-lg-6" style="text-align: right;">
                                <a data-toggle="modal" data-target="#add_new" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>Add New</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="text-align: center;">
                            <a href="{{route('temporary_employee_proxy.merge_attendance')}}" class="btn btn-danger">Merge Attendance</a>
                        </div>
                    </div>
                    <div class="panel-content pagination2 table-responsive">

                        <table class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>Serial</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Proxy Number</th>
                                <th>Joining Date</th>
                                <th>Created By</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($employee_proxy_card_list as $epcl)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$epcl->employee_id}}</td>
                                    <td>{{$epcl->employee_name}}</td>
                                    <td>{{$epcl->proxy_number}}</td>
                                    <td>{{date('d-m-Y', strtotime($epcl->joining_date))}}</td>
                                    <td>{{$epcl->created_by}}</td>
                                    <td>
                                        <a title="destroy" onclick="return confirm('Are you sure to destroy?')" href="{{route('employee_proxy_card_list.destroy', base64_encode($epcl->id))}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Add New Modal -->
    <div class="modal fade" id="add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>Temporary Employee Proxy Card List </strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST', 'route'=>'employee_proxy_card_list.store']) !!}
                        <div class="form-group">
                          <label for="employee_id"> Employee ID</label>
                          <input type="text" name="employee_id" autocomplete="off" class="form-control" placeholder="Employee ID" >
                        </div>
                        <div class="form-group">
                          <label for="employee_name"> Employee Name</label>
                          <input type="text" name="employee_name" autocomplete="off" class="form-control" required="" placeholder="Employee Name" >
                        </div>
                        <div class="form-group">
                          <label for="proxy_number"> Proxy Card Number</label>
                          <input type="text" minlength="10" name="proxy_number" autocomplete="off" class="form-control" required="" placeholder="Proxy Card Number" >
                        </div>
                        <div class="form-group">
                          <label for="joining_date"> Joining Date</label>
                          <input type="text" name="joining_date" autocomplete="off" class="date-picker form-control" placeholder="Joining Date" >
                        </div>
                        <div class="form-group">
                          <label for="remarks"> Remarks </label>
                          <textarea name="remarks" rows="6" autocomplete="off" class="form-control" placeholder="Remarks" ></textarea>
                        </div>
                        <div class="modal-footer">
                            <hr>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="Submit" class="btn btn-primary">Add New</button>
                        </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

          $( function() {
            $( "#datepicker1" ).datepicker({

            });
          } );
    </script>


    @include('include.copyright')
@endsection