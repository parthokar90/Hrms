@extends('layouts.master')
@section('title', 'Process Attendance')
@section('content')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div id="loading-gif-end" style="display:none; padding-left:20px; color: #2a2977; padding-top: 20px; font-size: 20px;">
                        <p>Data Processing Completed</p>
                    </div>
                    <div id="loading-gif-error" style="display:none; padding-left:20px; color: #2a2977; padding-top: 20px; font-size: 20px;">
                        <p>Something Went Wrong. Please check the .xls file and try to resubmit.</p>
                    </div>
                    <div class="modal fade" id="myModal" data-keyboard="false" data-backdrop="static" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header" style="background: cornflowerblue;">
                                    <h4 class="modal-title" style="font-weight: bold">Attendance Data Processing </h4>
                                </div>
                                <div class="modal-body">
                                    <p> Please wait till it completes...(It can take up to 3 minutes)</p>
                                    <center><img  id="loading_logo"  src="{{asset('hrm_script/images/6.gif')}}" style="width:150px;padding-bottom:10px"><br></center>

                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="panel-header ">
                        <h3>Data<strong> import</strong></h3>
                    </div>
                    <div class="panel-content" id="attendance-process-form">


                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Date</th>
                                <th>File Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $csv_data)
                            <tr>
                                <td>{{$csv_data->id}}</td>
                                <td>{{$csv_data->comment}}</td>
                                <td>{{$csv_data->date}}</td>
                                <td>{{$csv_data->attachment}}</td>
                                <td>
                                    {{Form::open(array('action' => 'AttendanceController@store','method' => 'post','files' =>true))}}
                                      <input type="hidden" name="hidden_id" value="{{$csv_data->id}}">
                                      <button type="submit" name="file_name" class="btn btn-success import-btn">Import</button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

            @include('include.copyright')
    </div>

    <script>
        $(document).ready(function(){
            $("#myBtn").click(function(){
                $("#myModal").modal();
            });
        });
        $('.import-btn').click(function (event) {
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
             });
            var form=$('#attendance-process-form').find('form'),
                url=form.attr('action'),
                method='POST';
//            alert(url);



            $(document).ajaxStop(function(){
                $('#loading-gif-end').show();
                $('#loading-gif-start').hide();
                $("#myModal").modal('hide');


            });
            $(document).ajaxStart(function(){
                $('#loading-gif-end').hide();
                $('#loading-gif-start').show();
                $('#loading-gif-error').hide();
                $("#myModal").modal();
            });

            $.ajax({
                url:url,
                method:method,
                data:form.serialize(),
                success:function (response) {
                    console.log('ok');

                },
                error:function (xhr) {
                    $('#loading-gif-error').show();

                }


            });

        });

    </script>


@endsection