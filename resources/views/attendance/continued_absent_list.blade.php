@extends('layouts.master')
@section('title', 'Continue Absent Employee List')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                        <div class="col-md-4">
                            <h3 ><i class="fa fa-users "></i> <strong> Continued Absent</strong> Employee List</h3>
                        </div>
                        <div class="col-md-8" style="margin-top:8px; text-align: right;">
                            <a href="{{route('employee.continue_absent_list_autosms')}}" class="btn btn-info btn-round btn-md"  style="float:right; height:32px;" ><i class="fa fa-envelope"></i> Send Auto SMS</a>
                        
                           <!--  {!! Form::open(['method'=>'POST','action'=>'EmployeeController@inactive_continue_absent']) !!}
                                <button type="submit" onclick="return confirm('Are you sure you want to inactive all employee?');" class="btn btn-danger btn-round btn-md btn-inactive"  style="float:right; height: 32px;" ><i class="fa fa-envelope"></i> Inactive All Employee</button>
                            {!! Form::close() !!} -->

                        </div>
                       
                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        
                        @if(Session::has('smsnotification'))
                            <p id="alert_message" class="alert alert-success">{{ Session::get('smsnotification') }}</p>
                        @endif
                        @if(Session::has('emailnotification'))
                            <p id="alert_message" class="alert alert-success">{{ Session::get('emailnotification') }}</p>
                        @endif
                        @if(Session::has('emailnotificationfailed'))
                            <p id="alert_message" class="alert alert-danger">{{ Session::get('emailnotificationfailed') }}</p>
                        @endif
                        @if(Session::has('smsnotificationfailed'))
                            <p id="alert_message" class="alert alert-danger">{{ Session::get('smsnotificationfailed') }}</p>
                        @endif
                        
                        <div id="continue_absent_list"> </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
    <script>
      $(document).ready(function(){
        
        function get_acc_summary(){
            $.ajax({
                url: 'continued_absent_list_data',
                method: 'get',
                dataType: 'html',
                beforeSend:function(){  
                      $('#continue_absent_list').html('<center><img class="img img-responsive" src="{{asset("hrm_script/images/preloader/9.gif")}}" ><br />Please wait...</center><br /><br /><br /><br />');  
                 }, 
                success: function (response) {
                    $('#continue_absent_list').html(response);
                }
            });
        }
        get_acc_summary();  

        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
      });
    </script>
@endsection