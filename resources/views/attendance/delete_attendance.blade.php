@extends('layouts.master')
@section('title', 'Delete Attendance')
@section('content')
    <div class="page-content">
        <div class="row">

            <div class="col-md-4">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Delete </strong>Attendance Record</h2>
                    </div>
                    <div class="panel-body bg-white">
                        
                        @if(Session::has('success'))
                            <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
                        @endif
                        @if(Session::has('error'))
                            <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
                        @endif
                        <div class="">
                            {!! Form::open(['method'=>'get', 'route'=>['attendance.deleteAttendanceProccess1']]) !!}
                            <div class="required form-group">
                                <label class="control-label">Employee Name or ID </label>
                                <select name="emp_id[]" id="emp_id" class="form-control" data-search="true" multiple>
                                    @foreach($employees as $emp)
                                        <option value="{{$emp->id}}">{{$emp->empFirstName." (".$emp->employeeId.")"}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="required form-group">
                                <label class="control-label">Date</label>
                                <div class="append-icon">
                                    <input type="text" id="date" name="date" autocomplete="off" class="form-control" placeholder="Select a date..." required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                            <div class="text-center  m-t-20">
                                <button type="submit" class="btn btn-embossed btn-primary"><i class="fa fa-right-arrow"></i>Proceed</button>
                                <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                @if(Session::has('success_one'))
                <p id="alert_message" class="alert alert-success">{{Session::get('success_one')}}</p>
                @endif
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Delete </strong>Attendance Record</h2>
                    </div>
                    <div class="panel-body bg-white">
                        @if(Session::has('success'))
                            <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
                        @endif
                        @if(Session::has('error'))
                            <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
                        @endif
                        <div class="">
                            {!! Form::open(['method'=>'post', 'url'=>['/attendance/delete/date/wise']]) !!}
                            <div class="required form-group">
                                <label class="control-label">Date</label>
                                <div class="append-icon">
                                    <input type="text" id="date_search" name="date" autocomplete="off" class="form-control" placeholder="Select a date..." required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                            <div class="text-center  m-t-20">
                                <button onclick="return confirm('Are you sure??')" type="submit" class="btn btn-embossed btn-danger"><i class="fa fa-right-arrow"></i>Delete All</button>
                                <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Delete </strong>Attendance Record</h2>
                    </div>
                    <div class="panel-body bg-white">
                        
                        @if(Session::has('success_two'))
                            <p id="alert_message" class="alert alert-success">{{Session::get('success_two')}}</p>
                        @endif
                        @if(Session::has('error'))
                            <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
                        @endif
                        <div class="">
                            {!! Form::open(['method'=>'get', 'url'=>['/attendance/delete/date/wise/dept']]) !!}
                            <div class="required form-group">
                                <label class="control-label">Select Department </label>
                                <select name="dept_id" id="dept_id" class="form-control" data-search="true">
                                    @foreach($department as  $departments)
                                        <option value="{{$departments->id}}">{{$departments->departmentName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="required form-group">
                                <label class="control-label">Date</label>
                                <div class="append-icon">
                                    <input type="text" id="date_search_dept" name="date" autocomplete="off" class="form-control" placeholder="Select a date..." required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                            <div class="text-center  m-t-20">
                                <button  type="submit" class="btn btn-embossed btn-info"><i class="fa fa-right-arrow"></i>Process</button>
                                <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>






        </div>
    </div>
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
        
        $(function() {
            $("#date").datepicker({ dateFormat: "yy-mm-dd" });
        });

        $(function() {
            $("#date_search").datepicker({ dateFormat: "yy-mm-dd" });
        });

        $(function() {
            $("#date_search_dept").datepicker({ dateFormat: "yy-mm-dd" });
        });
    </script>
    @include('include.copyright')

@endsection
