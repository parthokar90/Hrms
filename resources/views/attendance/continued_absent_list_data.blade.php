<table class="table table-hover table-dynamic table-bordered animated fadeIn">
    <thead>
    <tr>
        <th>Name</th>
        <th>Employee Id</th>
        <th>Designation</th>
        <th>Department</th>
        <th>Absent Days</th>
        <th>Last Present Date</th>
        <th><center>Action</center></th>
    </tr>
    </thead>
    <tbody>
    @foreach($ttDays as $key=>$employee)
        {!! Form::hidden("emp_id[]",$employee->id) !!}
        <tr>
            <td>{{$employee->empFirstName ." ".$employee->empLastName}}</td>
            <td>{{$employee->employeeId}}</td>
            <td>{{$employee->designation}}</td>
            <td>{{$employee->departmentName}}</td>
            <td>{{$employee->day_between}}</td>
            <td>{{\Carbon\Carbon::parse($employee->date)->format('d-M-Y')}}</td>
           
            <td style="text-align: center" id="continue_attendence_list_action">
             {!! Form::open(['method'=>'POST','action'=>'EmployeeController@continueAbsentEmployeeNotification']) !!}
                
                <input name="id" value="{{$employee->id}}" hidden >
                <input name="days" value="{{$employee->day_between}}" hidden >
                <input name="lastPresent" value="{{$employee->date}}" hidden >

                <button type="submit" value="downloadletter" name="notificationType" class="btn btn-info btn-sm"><i class="fa fa-download"></i> &nbsp; Download Letter </button>
                <button type="submit" value="sentsms" name="notificationType" class="btn btn-blue btn-sm"><i class="fa fa-mobile"></i> &nbsp; Send SMS </button>
                <button type="submit" value="sentmail" name="notificationType" class="btn btn-success btn-sm"><i class="fa fa-envelope"></i> &nbsp; Send Email</button>
            {!! Form::close() !!}

            </td>

        </tr>
    @endforeach
  </tbody>
</table>