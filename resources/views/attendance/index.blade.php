@extends('layouts.master')
@section('title', 'Upload Attendance File')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-md-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3>Atten<strong>dance</strong></h3>
                        {{--<a href="#" class="btn btn-success btn-sm" href="">View data</a>--}}
                    </div>
                    <div class="panel-content">
                        {{Form::open(array('url' => 'csv_upload','method' => 'post','files' =>true))}}

                        <div class="form-group">
                            {!! Form::label('comment','Title: ') !!}
                            {!! Form::text('comment',null,['class'=>'form-control','required'=>'']) !!}

                        </div>
                        <label>Upload File</label>
                        <div class="form-group">
                           <input type="file" required accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="form-control" name="upload-file">
                        </div>
                        <input type="hidden" name="cur_date" value="{{date("d/m/Y")}}">
                        <button id="salary_check" type="submit" class="btn btn-primary">Upload</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
@endsection
