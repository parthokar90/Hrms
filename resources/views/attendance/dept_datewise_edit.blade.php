@extends('layouts.master')
@section('title', 'Edit Attendance')
@section('content')
    <div class="page-content">
        {!! Form::open(['method'=>'post', 'action'=>['AttendanceController@updateattdept']]) !!}
        <button type="submit" class="btn btn-embossed btn-primary">Update All</button>
        <div class="panel-content pagination2 table-responsive">
            <table class="table table-hover table-dynamic">
                <thead>
                <tr>
                    <th>Sl</th>
                    <th>Id</th>
                    <th>Employee</th>
                    <th>In Time</th>
                    <th>Out Time</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                    @php $order=0; @endphp
                    @foreach($data as $emp)
                    @php $order++; @endphp
                    <tr>
                     <td>{{$order}}</td>
                     <td>{{$emp->employeeId}}</td>
                     <td>{{$emp->empFirstName}}</td>
                    <input type="hidden" name="emp_id[]" value="{{$emp->emp_id}}">
                    <input type="hidden" name="date" value="{{$emp->date}}">
                     <td><input class="form-control" type="time" name="in_time[]" value="{{$emp->in_time}}"></td>
                     <td><input class="form-control" type="time" name="out_time[]" value="{{$emp->out_time}}"></td>
                     <td>{{date('d-F-Y',strtotime($emp->date))}}</td>
                    </tr>
                    @endforeach
                </div>
            </div>
        </div>
        </tbody>                     
    </table>
       {!! Form::close() !!}
    </div>
    
    @include('include.copyright')

@endsection
