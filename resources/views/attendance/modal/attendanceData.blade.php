{!! Form::open(['method'=>'POST','action'=>'AttendanceController@storeAttendance']) !!}
<div class="form-group">
    {!! Form::label('employeeId','Employee Id:',['class'=>'required']) !!}
    {!! Form::text('employeeId',$data->employeeId,['class'=>'form-control','disabled'=>''] ) !!}


</div>
<div class="form-group">
    {!! Form::label('name','Employee Name:',['class'=>'required']) !!}
    {!! Form::text('name',$data->empFirstName." ".$data->empLastName,['class'=>'form-control','disabled'=>''] ) !!}

</div>

<div class="form-group">
    <label class="required form-label">Date</label>
    <div class="prepend-icon">
        {!! Form::text('date',$date,['class'=>'form-control','disabled'=>''] ) !!}
        <i class="icon-calendar"></i>
    </div>
</div>
{!! Form::hidden('emp_id',$data->id) !!}
{!! Form::hidden('date', $date) !!}

<div class="form-group">
    {!! Form::label('in_time','Entry Time:',['class'=>'required']) !!}
    {!! Form::time('in_time',null,['class'=>'form-control','required'=>'']) !!}

</div>

<div class="form-group">
    {!! Form::label('out_time','Leaving Time:',['class'=>'required']) !!}
    {!! Form::time('out_time',null,['class'=>'form-control','required'=>'']) !!}

</div>


</div>
<div class="modal-footer">
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="Submit" id='update-working-experience' class="btn btn-primary">Add Attendance</button>
    </div>

{!! Form::close() !!}