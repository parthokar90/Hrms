@php
    use App\Http\Controllers\dashboardcontroller;
@endphp
@extends('layouts.master')
@section('title', 'Edit Attendance')
@section('content')
    <div class="page-content ">
        <div class="row panel"  style="border:1px solid #999">

            <div class="col-xlg-12 col-lg-12  col-sm-12">
                <div class="text-center" >
                    <h2><b>Edit Attendance Date Wise</b></h2>
                    <hr>
                </div>
            </div>



            {!! Form::open(['method'=>'POST','action'=>'AttendanceController@showEditAttendanceDate']) !!}
            <div class="col-xlg-7 col-md-10 col-md-offset-1 col-xlg-offset-2">
                @if(Session::has('success'))
                    <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
                @elseif(Session::has('error'))
                    <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
                @endif


                <div class="form-group">
                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">Start Date<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <input type="text" name="date" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select a date...">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label class="col-md-3">End Date<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <input type="text" name="endDate" class="date-picker form-control" value="{{\Carbon\Carbon::now()->format('m/d/Y')}}" required placeholder="Select a date...">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-form-gap"></div>
                        <label for="emp_id" class="col-md-3">Select Single Employee<span class="clon">:</span></label>
                        <div class="col-md-9">
                            <select name="emp_id" id="emp_id" required class="form-control" data-search="true">
                                <option value="">Select Employee</option>
                                @foreach($employees as $emp)
                                    <option value="{{$emp->id}}">{{$emp->empFirstName." (".$emp->employeeId.")"}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <hr>
                            <button type="submit" id="process-btn" class="btn btn-success"><i class="fa fa-list"></i> &nbsp;Process</button>
                            {{--<button type="submit" value="Generate PDF" name="viewType" class="btn btn-primary"><i class="fa fa-download"></i> &nbsp;Download as PDF</button>--}}
                            <hr>
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#process-btn').click(function (event) {
                var emp_id=$('#emp_id').val();
                if(emp_id.length===0){
                    alert('Select a Employee First');
                    event.preventDefault();
                }
                else {

                }

            });

        });

    </script>

    @include('include.copyright')
@endsection