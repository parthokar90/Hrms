
@extends('layouts.master')
@section('content')
    <div class="page-content">
        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif

      <div class="panel"  style="border-radius:4px">
        <div class="panel-header">
            <h2><strong>Daily Absent</strong> Report</h2>
        </div>
        <div class="panel-content">
        {{--{!! Form::open(['method'=>'GET']) !!}--}}
        {!! Form::label('date', 'Select a date: ') !!}
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="text" class="form-control" id="date" name="date"/>
                </div>
            </div>
        </div>
        <hr>
        <div id="hidd" class="row">
            {{--@if(isset($applications))--}}
                {{--@include('recruitment.recruitmentStatus')--}}

            {{--@endif--}}
        </div>
      </div>
    </div>
  </div>

    @include('attendance.modal.showAttendance')


    <script>
        $(function() {
            var dateVariable;
            $("#date").datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function(dateText) {
                    dateVariable = dateText;
//                    alert(dateVariable);
//                    alert($("#date").val())
                    $.ajax({
                        url:"status/absent/"+dateVariable,
                        method:'GET',
                        dataType:'html',
                        success: function (response) {
                            if(!response){
                                response="<h3><center><strong class='red-text'>No absent report found</strong></center></h3>"
                            }
                            $('#hidd').html(response);

//                            alert(response);

                        },
                        error:function () {
//                            alert('error')

                        }

                    });

                },
                onClose:function () {


//                    document.getElementById('hid').style.display='block';

                }

            });

        });
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

    </script>

    @include('include.copyright')
@endsection