@extends('layouts.master')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <div class="row">
                        <div class="col-md-6">
                            <h3 ><i class="fa fa-users "></i> <strong> Todays Notified </strong> Employee List</h3>
                        </div>
                        <div class="col-md-6" style="margin-top:8px;">
                            <a href="{{route('employee.continue_absent_list_autosms')}}" class="btn btn-danger btn-round btn-md"  style="float:right;" ><i class="fa fa-envelope"></i> Send Auto SMS</a>
                        </div>
                       
                        </div>

                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Employee Id</th>
                                <th>Designation</th>
                                <th>Department</th>
                                <th>Absent Days</th>
                                <th>Last Present Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ttDays as $employee)
                                <tr>
                                    <td>{{$employee->empFirstName ." ".$employee->empLastName}}</td>
                                    <td>{{$employee->employeeId}}</td>
                                    <td>{{$employee->designation}}</td>
                                    <td>{{$employee->departmentName}}</td>
                                    <td>{{$employee->day_between}}</td>
                                    <td>{{\Carbon\Carbon::parse($employee->date)->format('d-M-Y')}}</td>
                                   
                                </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('include.copyright')
    <script>
      $(document).ready(function(){
          setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
      });
    </script>
@endsection