@extends('layouts.master')
@section('title', 'Edit Attendance')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('message')}}</p>
            @endif
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong>Edit </strong>Attendance</h2>
                    </div>
                    @if(Session::has('success'))
                        <p id="alert_message" class="alert alert-success">{{Session::get('success')}}</p>
                    @elseif(Session::has('error'))
                        <p id="alert_message" class="alert alert-danger">{{Session::get('error')}}</p>
                    @endif
                    <div class="panel-body bg-white">
                        <div class="">
                            {!! Form::open(['method'=>'POST', 'action'=>'ReportController@update_present']) !!}
                            {{--<div class="required form-group">--}}
                            {{--<label class="control-label">Employee Name (Id)</label>--}}
                            {{--<select name="emp_id" id="emp_id" class="form-control" data-search="true">--}}
                            {{--@foreach($employees as $emp)--}}
                            {{--<option value="{{$emp->id}}">{{$emp->empFirstName." (".$emp->employeeId.")"}}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                            {{--</div>--}}
                            {!! Form::hidden('id',$attendance->aid) !!}
                            <div class="required form-group">
                                <label class="control-label">Employee Name Or ID</label>
                                <input type="text" disabled value="{{$attendance->empFirstName." ".$attendance->empLastName." (".$attendance->employeeId.")"}}" id="search_text" name="employee" autocomplete="off" class="form-control" placeholder="Search by Name or ID" required>
                            </div>

                            <div class="required form-group">
                                <label class="control-label">Date</label>
                                <div class="append-icon">
                                    <input type="text" disabled value="{{Carbon\Carbon::parse($attendance->date)->format('d M Y')}}" name="date" autocomplete="off" class="form-control" placeholder="Select a date..." required>
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                            <div class="required form-group">
                                <label class="control-label">In Time(HH:MM am/pm)</label>
                                <div class="append-icon">
                                    <input type="time" name="stime" value="{{$attendance->in_time}}" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Out Time(HH:MM am/pm)</label>
                                <div class="append-icon">
                                    <input type="time" name="etime" value="{{$attendance->out_time}}" class="form-control">
                                </div>
                            </div>
                            <div class="text-center  m-t-20">
                                <button type="submit" class="btn btn-embossed btn-primary">Update</button>
                                <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var url = "{{ route('autocomplete.employee') }}";
            $('#search_text').typeahead({
                minLength: 2,
                source:  function (query, result) {
                    $.ajax({
                        url: url,
                        data: 'query=' + query,
                        dataType: "json",
                        type: "GET",
                        success: function (data) {
                            result($.map(data, function (item) {
                                return item;
                            }));
                        }
                    });

                }

            });
        });




        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 10000);

    </script>
    @include('include.copyright')

@endsection
