@extends('layouts.master')
@section('title', 'Manual SMS')
@section('content')
    <div class="page-content">
        @if(Session::has('notification'))
            <p id="alert_message" class="alert alert-success">{{Session::get('notification')}}</p>
        @endif
        @if(Session::has('sms'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('meetingdeleted')}}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-envelope"></i> <strong>Manual SMS </strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                      
                        <form action="{{url('/manual_sms')}}" method="POST">
                          {{ csrf_field() }}
                          
                          <div class="required form-group">
                              <label class="control-label">Phone Number</label>
                              <div class="append-icon">
                                  <input type="text" name="phoneNumber" class="form-control" minlength="8" placeholder="Please Insert Valid Phone Number.." required >
                            </div>
                           </div>

                           <div class="required form-group">
                              <label class="control-label">Message</label>
                              <div class="append-icon">
                                <textarea rows="8" maxlength="250" id="messageArea" name="message" class="form-control" placeholder="Write Message here ... " required></textarea>
                                <br />
                                <br />
                                <div id="charNum"></div>
                              </div>
                           </div>

                           <div class=" m-t-20">
                                <button type="submit" class="btn btn-embossed btn-primary">Sent SMS</button>
                                <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Clear</button>
                           </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
      $('#messageArea').keyup(function () {
        var max = 250;
        var len = $(this).val().length;
        if (len >= max) {
          $('#charNum').text(' You have reached the limit');
          $('#charNum').css({"color": "red"});
        } else {
          var char = max - len;
          $('#charNum').text(char + ' characters left');
          $('#charNum').removeAttr("style");
        }
      });

      setTimeout(function() {
          $('#alert_message').fadeOut('fast');
      }, 5000);
    </script>
@include('include.copyright')
@endsection