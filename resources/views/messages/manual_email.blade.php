@extends('layouts.master')
@section('title', 'Manual Email')
@section('content')
    <div class="page-content">
        @if(Session::has('notification'))
            <p id="alert_message" class="alert alert-success">{{Session::get('notification')}}</p>
        @endif
        @if(Session::has('fnotification'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('fnotification')}}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-envelope"></i> <strong>Manual Email </strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                      
                        <form action="{{url('/manual_email')}}" enctype="multipart/form-data" method="POST">
                          {{ csrf_field() }}
                          
                          <div class="required form-group">
                              <label class="control-label">Email Address</label>
                              <div class="append-icon">
                                  <input type="email" name="emailAddress" class="form-control" placeholder="Please Insert Valid Email Address" required >
                            </div>
                           </div>

                          <div class="required form-group">
                              <label class="control-label">Email Subject</label>
                              <div class="append-icon">
                                  <input type="text" name="emailSubject" class="form-control" placeholder="Please Insert Email Subject " required >
                            </div>
                           </div>

                           <div class="required form-group">
                              <label class="control-label">Email Message</label>
                              <div class="append-icon">
                                <textarea rows="8" id="messageArea" name="emailMessage" class="form-control" placeholder="Write Message here ... " required ></textarea>
                              </div>
                           </div>

                           <div class=" form-group">
                              <label class="control-label">Email Attachment</label>
                              <div class="append-icon">
                                <input type="file" name="file" class="form-control" >
                              </div>
                           </div>

                           <div class=" m-t-20">
                                <button type="submit" class="btn btn-embossed btn-primary">Sent Email</button>
                                <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Clear</button>
                           </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
    <script>

      setTimeout(function() {
          $('#alert_message').fadeOut('fast');
      }, 5000);

    </script>
@include('include.copyright')
@endsection