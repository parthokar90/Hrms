@extends('layouts.master')
@section('title', 'Manual Bulk SMS')
@section('content')
{{ Html::script('hrm_script/plugins/jquery/jquery-1.11.1.min.js') }}
        <script type='text/javascript'>
            $(document).ready(function(){
                // Check or Uncheck All checkboxes
                $("#checkall").change(function(){
                   var checked = $(this).is(':checked');
                   if(checked){
                     $(".checkbox").each(function(){
                       $(this).prop("checked",true);
                     });
                   }else{
                     $(".checkbox").each(function(){
                       $(this).prop("checked",false);
                     });
                   }
               });
                
               // Changing state of CheckAll checkbox 
               $(".checkbox").click(function(){
    
                  if($(".checkbox").length == $(".checkbox:checked").length) {
               $("#checkall").prop("checked", true);
              } else {
               $("#checkall").removeAttr("checked");
              }

               });
               $(".checkbox").each(function(){
                       $(this).prop("checked",true);
                     });
            });
        </script>
    <div class="page-content">
        @if(Session::has('notification'))
            <p id="alert_message" class="alert alert-success">{{Session::get('notification')}}</p>
        @endif
        @if(Session::has('sms'))
                <p class="alert alert-danger">{{Session::get('sms')}}</p>
        @endif
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="fa fa-envelope"></i> <strong>Manual Bulk SMS </strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <div id="loading_logo"> <img src="hrm_script/images/loading2.gif" alt=""></div>
                        
                        <form id="formData" method="POST">
                            
                           <div class="required form-group">
                              <label class="control-label">Message</label>
                              <div class="append-icon">
                                <textarea rows="8" required maxlength="800" id="messageArea" name="message" class="form-control" placeholder="Write Message here ... " ></textarea>
                                <br />
                                <br />
                                <div id="charNum"></div>
                              </div>
                           </div>

                            <button type="button" name="sent_sms" id="btnSubmit" class="btn btn-primary"><i class="fa fa-envelope"></i> Sent SMS </button>
                         <hr>
                        
                         <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th> <input type="checkbox" id='checkall' checked /></th>
                                <th>Employee Id</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Department</th>
                                <th>Unit</th>
                                <th>Section</th>
                                <th>Gender</th>
                                <th>Phone</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=0; @endphp
                            @foreach($employees as $employee)
                            <input type="text" hidden id="id_name" value="{{$employee->empFirstName ." ".$employee->empLastName}}" name="sendAbleEmployeeName[]" >
                            <input type="text" hidden id="id_phone" value="{{$employee->empPhone}}" name="sendAbleEmployeePhone[]" >
                            <input type="text" hidden id="id_empId" value="{{$employee->id}}" name="empId[]" >
                            <tr>
                                <td> <input type="checkbox" class='checkbox' name="cid[]" value="{{$i++}}" /></td>
                                <td>{{$employee->employeeId}}</td>
                                <td>{{$employee->empFirstName ." ".$employee->empLastName}}</td>
                                <td>{{$employee->designation}}</td>
                                <td>{{$employee->departmentName}}</td>
                                <td>{{$employee->unitName}}</td>
                                <td>{{$employee->empSection}}</td>
                                <td>
                                    @if($employee->empGenderId==1)
                                    Male
                                    @endif
                                    @if($employee->empGenderId==2)
                                    Female
                                    @endif
                                    @if($employee->empGenderId==3)
                                    Other
                                    @endif
                                </td>
                                <td>{{$employee->empPhone}}</td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>   
                        </form>
                        <div id="msgArea"></div>
                    </div>
                    <div class="panel-footer"></div>
                </div>
            </div>
        </div>

    </div>

<script>  
 $(document).ready(function(){  

     $('#loading_logo').hide(); 

      $('#btnSubmit').click(function(){  
                var message = $('#messageArea').val();  

        if(message==''){
          alert("Please Type Message First."); 
        }else{
        $.ajax({  
             url:"sent_bulk_sms_to_employee",  
             method:"POST",  
             data:$('#formData').serialize(),  
             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             beforeSend:function(){  

                $('#loading_logo').fadeIn(); 
                $('#formData').fadeOut();
             },  
             success:function(data){  
                $('#loading_logo').fadeOut(); 
                    data.forEach(function(entry) {
                        $('#msgArea').prepend("<p class='animated zoomIn alert alert-success'> SMS has been sent to <b>"+entry['sendAbleEmployeeName']+" ("+entry['sendAbleEmployeePhone']+"</b>) </p>");
                    });
              // console.log(data);
             }
        });  
      }
      });  
  });  





  $('#messageArea').keyup(function () {
    var max = 800;
    var len = $(this).val().length;
    if (len >= max) {
      $('#charNum').text(' You have reached the limit');
      $('#charNum').css({"color": "red"});
    } else {
      var char = max - len;
      $('#charNum').text(char + ' characters left');
      $('#charNum').removeAttr("style");
    }




  });
</script>
@include('include.copyright')
@endsection