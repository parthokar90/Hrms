@php
use App\Http\Controllers\dashboardcontroller;
@endphp
<?php

        //One Year Employee
        $oneYearEmployee=DB::table('employees')
            ->leftJoin('designations', function ($query) {
                $query->on('designations.id', '=', 'employees.empDesignationId');
            })
            ->select('employees.*','employees.empFirstName','employees.id as emp_id','employees.employeeId','employees.empLastName','designations.designation',DB::raw('abs(DATEDIFF(employees.empJoiningDate,NOW())) as just'))
            ->whereRaw(DB::raw('abs(DATEDIFF(employees.empJoiningDate,NOW()))>=(employees.probation_period*30)'))
            ->whereRaw(DB::raw('abs(DATEDIFF(employees.empJoiningDate,NOW()))<(employees.probation_period*30)+30'))
            ->selectRaw('DATEDIFF(employees.empJoiningDate,NOW())')
            ->orderBy('employees.empJoiningDate','DESC')
            ->get();

        // all active employee count
        $activeemployee=DB::table('employees')
            ->where('empAccStatus','=','1')
            ->count();

        // all Inactive employee count
        $inactiveemployee=DB::table('employees')
            ->where('empAccStatus','=','0')
            ->count();   
        //     $now=date('Y-m-d');
        //   // all notices
        // $allnotices=DB::table('tbnotices')
        //     ->where('startDate', '>=', $now)
        //     ->where('endDate', '<=', $now)
        //     ->get();
        //     dd($allnotices);
        // all male employee count
        $maleemployee=DB::table('employees')
            ->where([['empAccStatus','=','1'],['empGenderId','=','1']])
            ->count();

        // all female employee count
        $femaleemployee=DB::table('employees')
            ->where([['empAccStatus','=','1'],['empGenderId','=','2']])
            ->count();

        // all other employee count
        $othersgenderemployee=DB::table('employees')
            ->where([['empAccStatus','=','1'],['empGenderId','=','3']])
            ->count();

        // all new employee list
        $NewEmployeeList=DB::table('employees')->orderBy('empJoiningDate', 'DESC')->take(12)->get();

        //all department
        $department=DB::table('departments')->count();

        //all units
        $totunits=DB::table('units')->count();

        //all designation
        $totdesignations=DB::table('designations')->count();

        //all training
        $training=DB::table('tb_employee_training')
            ->where('training_starting_date','>',now())
            ->groupBy('training_id')
            ->get()->count();


        $pending_leave_list=DB::table('tb_leave_application')->
        join('employees', 'employees.id','=','tb_leave_application.employee_id')
            ->
            join('designations', 'designations.id','=','employees.empDesignationId')
            ->
            leftjoin('tb_employee_leave',['tb_employee_leave.employee_id'=>'tb_leave_application.employee_id', 'tb_employee_leave.leave_type_id'=>'tb_leave_application.leave_type_id'])->
            join('tb_leave_type','tb_leave_type.id','=','tb_leave_application.leave_type_id')
            ->select('employees.*','designations.designation','tb_employee_leave.leave_available',
                'tb_leave_type.leave_type','tb_leave_application.*')
            ->where('tb_leave_application.status','=',0)
            ->orderBy('tb_leave_application.id','DESC')
            ->take(10)->get();
        // dd($pending_leave_list);
        
        //all pending leave
        $all_pending_leave_with_name=DB::table('tb_leave_application')->leftJoin('employees','tb_leave_application.employee_id','employees.id')->select('tb_leave_application.*','employees.empFirstName as emp_name')->where('status','=',0)->get();


        //attendance settings
        $attendance_settings=DB::table('attendance_setup')->first();
        //today attendance
        $today_attendance=DB::table('attendance')->where('date',date('Y-m-d'))->count();
        //today's late employee list
        $today_attendance1=DB::table('attendance')
            ->leftJoin('employees', function ($query) {
                $query->on('attendance.emp_id', '=', 'employees.id');
            })
            ->join('designations', function ($query) {
                $query->on('designations.id', '=', 'employees.empDesignationId');
            })
            ->select('attendance.*', 'employees.empFirstName','employees.empDepartmentId', 'employees.id', 'employees.employeeId', 'employees.empLastName', 'designations.designation')
            ->where('date',date('Y-m-d'))
            ->orderBy('attendance.in_time','DESC')
            ->get();

           $employee_overtime_statistic=DB::table('employee_overtime')
            ->select("id","month" ,DB::raw("(SUM(overtime_hour)) as overtime_hours"))
            ->orderBy('month','DESC')
            ->groupBy(DB::raw("MONTH(month)"))
            ->take(6)
            ->get();
        //continue Absent List
        //have to work
        $continued_absent_list=DB::table('attendance')->where('date',date('Y-m-d'))->get();

        //meeting count current date
        $meeting_date=DB::table('meetings')->where('date',date('Y-m-d'))->count();
        $meeting_count=DB::table('meetings')->where('date','>',date('Y-m-d'))->count();


        // Today's Expense List
        $todays_expense_list=DB::table('tbexpenselist')
        ->where('expenseDate',date('Y-m-d'))
        ->leftJoin('tbexpensecategory','tbexpensecategory.id','tbexpenselist.categoryId')
        ->select("tbexpenselist.*","tbexpensecategory.categoryName")
        ->get();


        $date=date('Y-m-d');
        $sql="SELECT * FROM tbnotices WHERE '$date'>=startDate AND '$date'<=endDate";
        $notices=DB::select($sql);

        $mettings=DB::table('meetings')->where('date','>=',$date)->get();


?>
@extends('layouts.master')
@section('title', 'Dashboard')
@section('content')

 <div class="page-content page-thin dashboard_page_cont">
   <div class="row  dashboard myAnimation">
     <div class="col-lg-12" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a href="{{url('/employee')}}">
              <div class="panel my_panel_2 dash_bg">
                <div class="panel-content widget-info total_employee dash_w" >
                  <div class="row">
                    <div class="left">
                      	<i class="fa fa-users bg-blue"></i>
                    </div>
                    <div class="right">
                      <p class="number countup">{{sprintf('%03d',$employee)}}</p>
                      <p class="f-14"><b>Total Employee</b></p>
                    </div>
                  </div>
                </div>
              </div>
             </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a href="{{url('/active_employee')}}">
              <div class="panel my_panel_3 dash_bg">
                <div class="panel-content widget-info active_employee dash_w" >
                  <div class="row">
                    <div class="left">
                      	<i class="fa fa-check-circle-o bg-green"></i>
                    </div>
                    <div class="right">
                      @php
                      $pae = ($activeemployee * 100) / $employee;
                      @endphp
                      <p class="number countup">{{sprintf('%03d',$activeemployee)}} ({{round($pae)}} %)</p>
                      <p class="f-14"><b>Active Employee</b></p>
                    </div>
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a href="{{url('/inactive_employee')}}">
              <div class="panel my_panel_1 dash_bg">
                <div class="panel-content widget-info inactive_employee dash_w" >
                  <div class="row">
                    <div class="left">
                      	<i class="fa fa-exclamation-circle bg-red"></i>
                    </div>
                    <div class="right">
                      @php
                      $piae = ($inactiveemployee * 100) / $employee;
                      @endphp
                      <p class="number countup">{{sprintf('%03d',$inactiveemployee)}} ({{round($piae)}} %)</p>
                      <p class="f-14"><b>Inactive Employee</b></p>
                    </div>
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a href="{{url('/male_employee')}}">
              <div class="panel my_panel_4 dash_bg">
                <div class="panel-content widget-info male_employee dash_w">
                  <div class="row">
                    <div class="left">
                      	<i class="fa fa-male bg-blue"></i>
                    </div>
                    <div class="right">
                      @php
                      $pme = ($maleemployee * 100) / $activeemployee;
                      @endphp
                      <p class="number countup">{{sprintf('%03d',$maleemployee)}} ({{round($pme)}} %)</p>
                      <p class="f-14"><b>Male Employee</b></p>
                    </div>
                  </div>
                </div>
              </div>
               </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInLeft">
              <a href="{{url('/female_employee')}}">
              <div class="panel my_panel_5 dash_bg">
                <div class="panel-content widget-info female_employee dash_w" >
                  <div class="row">
                    <div class="left">
                      	<i class="fa fa-female bg-red"></i>
                    </div>
                    <div class="right">
                      @php
                      $pfme = ($femaleemployee * 100) / $activeemployee;
                      @endphp
                      <p class="number countup">{{sprintf('%03d',$femaleemployee)}}  ({{round($pfme)}} %)</p>

                      <p class="f-14"><b>Female Employee</b></p>
                    </div>
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceIn">
              <a href="{{url('/settings/department')}}">
              <div class="panel my_panel_6 dash_bg">
                <div class="panel-content widget-info dash_department dash_w" >
                  <div class="row">
                    <div class="left">
                      	<i class="fa fa-support bg-yellow"></i>
                    </div>
                    <div class="right">
                      <p class="number countup">{{sprintf('%03d',$department)}}</p>
                      <p class="f-14"><b>Departments</b></p>
                    </div>
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceIn">
              <a href="{{url('/settings/designation')}}">
              <div class="panel my_panel_7 dash_bg">
                <div class="panel-content widget-info dash_designation dash_w" >
                  <div class="row">
                    <div class="left">
                      	<i class="fa fa-empire bg-green"></i>
                    </div>
                    <div class="right">
                      <p class="number countup">{{sprintf('%03d',$totdesignations)}}</p>
                      <p class="f-14"><b>Designations</b></p>
                    </div>
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInRight">
             <a href="{{url('/settings/unit')}}">
              <div class="panel my_panel_8 dash_bg">
                <div class="panel-content widget-info dash_units dash_w">
                  <div class="row">
                    <div class="left">
                      	<i class="fa fa-building  bg-dark"></i>
                    </div>
                    <div class="right">
                      <p class="number countup">{{sprintf('%03d',$totunits)}}</p>
                      <p class="f-14"><b>Units</b></p>
                    </div>
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <div class="panel my_panel_12 dash_bg">
                <div class="panel-content widget-info dash_attendance dash_w" >
                  <div class="row">
                    <div class="left">
                      <i class="fa fa-check bg-pink"></i>
                    </div>
                    <div class="right">
                      <p class="number countup f-14"> {{sprintf('%03d',$today_attendance)}} Entries</p>
                      <p class="f-12"><b>In Today's Attendance</b></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a class="" href="{{url('/leave')}}">
              <div class="panel my_panel_10 dash_bg">
                <div class="panel-content widget-info dash_pleave dash_w" >
                  <div class="row">
                    <div class="left">
                      <i class="fa fa-tasks bg-dark"></i>
                    </div>
                    <div class="right">
                      <p class="number countup">{{sprintf('%03d',$pending_leave)}}</p>
                      <p class="f-14"><b>Pending Leave</b></p>
                    </div>
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInDown">
              <a class="" href="{{url('/training_history')}}">
              <div class="panel my_panel_11 dash_bg">
                <div class="panel-content widget-info dash_training dash_w" >
                  <div class="row">
                    <div class="left">
                      <i class="fa fa-ge bg-purple"></i>
                    </div>
                    <div class="right">
                      <p class="number countup">{{sprintf('%03d',$training)}}</p>
                      <p class="f-14"><b>Upcoming Training</b></p>
                    </div>
                  </div>
                </div>
              </div>
              </a>
            </div>


            <div class="col-xlg-3 col-lg-3 col-sm-3 animated bounceInUp">
              <a class="" href="{{url('/AbsentEmployees/sendAutoSMS')}}" onclick="return confirm('Are you sure?')">
              <div class="panel my_panel_9 dash_bg">
                <div class="panel-content widget-info dash_cbl dash_w">
                  <div class="row">
                    <div class="left">
                      <i class="fa fa-envelope bg-pink"></i>
                    </div>
                    <div class="right">
                      <p class="number countup f-13">Send Auto SMS</p>
                      <p class="f-10"><b>To Continue Absent Employee</b></p>
                    </div>
                  </div>
                </div>
              </div>
              </a>
            </div>

        </div>
     </div>

     <div class="col-md-3 animated bounceInLeft">
       <div class="panel widget_panel_dash">
           <div class="panel-header my_panel_6">
               <h5><i class="fa fa-users"></i> <strong>Employee Ratio</strong></h5>
           </div>
           <div class="panel-body">
               <canvas id="malefemaleEmployee" style="max-width:100%;"></canvas>
           </div>
       </div>
     </div>

     <div class="col-md-3 animated bounceInUp">
       <div class="panel widget_panel_dash">
           <div class="panel-header my_panel_3">
               <h5><i class="fa fa-simplybuilt"></i> <strong>Todays Present-Absent Ratio</strong></h5>
           </div>
           <div class="panel-body">
               <canvas id="present-absent2" style="max-width:100%;"></canvas>
           </div>
       </div>
     </div>

     <div class="col-md-3 animated bounceInDown">
       <div class="panel widget_panel_dash">
           <div class="panel-header my_panel_4">
               <h5><i class="fa fa-gears"></i> <strong>Leave Statistic (Last 4 Months)</strong></h5>
           </div>
           <div class="panel-body">
               <canvas id="monthlyLeave" style="max-width:100%;"></canvas>
           </div>
       </div>
     </div>

     <div class="col-md-3 animated bounceInRight">
       <div class="panel calender_panel_body">
           <div class="panel-body ">
              <div class="widget widget_calendar bg-dark">
                <div class="multidatepicker"></div>
              </div>
           </div>
       </div>
     </div>

     <div class="col-md-6 animated bounceInLeft">
       <div class="panel  widget_panel_dash">
           <div class="panel-header my_panel_5">
               <h5><i class="icon-list"></i> <strong>Present Statistic (Last 30 Days)</strong></h5>
           </div>
           <div class="panel-body">
               <canvas id="attendancestatistic2" style="max-width:100%;"></canvas>
           </div>
       </div>
     </div>

     <div class="col-md-6 animated bounceInRight">
       <div class="panel widget_panel_dash">
           <div class="panel-header my_panel_7">
               <h5><i class="icon-list"></i> <strong>Absent Statistic (Last 30 Days)</strong></h5>
           </div>
           <div class="panel-body">
               <canvas id="absentstatistic" style="max-width:100%;"></canvas>
           </div>
       </div>
     </div>

	<div class="col-md-4 animated bounceInUp">
      <div class="panel widget_panel_dash" style="border-radius:4px">
        <div class="panel-header my_panel_6">
          <h5><a href="{{URL('/employee')}}" class="dash_a"><i class="fa fa-users"></i> <strong>New Employee List</strong></a></h5>
        </div>
        <div class="panel-content widget-table">
          <div class="withScroll mCustomScrollbar _mCS_18" data-height="400" style="height: 400px;">
          	<div class="mCustomScrollBox mCS-light" id="mCSB_18" style="position:relative; height:100%; overflow:hidden; max-width:100%;">
	          <div class="mCSB_container" style="position:relative; top:0;">
	            <table class="table table-striped table-hover table-bordered">
	              <tbody>
                  @if(count($NewEmployeeList)>0)
                    @foreach($NewEmployeeList as $employeeDetails)
                     <tr class="f-12">
                      <td><b class="f-12"><a target="_BLANK"  class="dash_stitle" href="{{route('employee.show',$employeeDetails->id)}}"  >{{$employeeDetails->employeeId}} ({{$employeeDetails->empFirstName}} {{$employeeDetails->empLastName}})</a> </b>
                      <br>
                        <span class="dash_stitle2">Joining Date: <b>{{\Carbon\Carbon::parse($employeeDetails->empJoiningDate)->format('d-M-Y')}}</b></span>
                      </td>
                    </tr>
                    @endforeach
                  @else
                    <tr><td></td></tr>
                    <tr class="f-12">
                        <td >No data exits.</td>
                    </tr>
                  @endif
                </tbody>
	            </table>
	          </div>

          	</div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4 animated bounceInDown">
      <div class="panel widget_panel_dash" style="border-radius:4px">
        <div class="panel-header my_panel_8">
          <h5><i class="fa fa-qrcode"></i> <strong>Incrementable Employee List</strong></h5>
        </div>
        <div class="panel-content widget-table">
          <div class="withScroll mCustomScrollbar _mCS_18" data-height="400" style="height: 400px;">
            <div class="mCustomScrollBox mCS-light" id="mCSB_18" style="position:relative; height:100%; overflow:hidden; max-width:100%;">
            <div class="mCSB_container" style="position:relative; top:0;">
              <table class="table table-striped table-hover table-bordered">
                 <tbody>
                  @if(count($oneYearEmployee)>0)
                    @foreach($oneYearEmployee as $employeeDetails)
                    <tr class="f-12">
                      <td><b class="f-12"><a target="_BLANK" class="dash_stitle" href="{{route('employee.show',$employeeDetails->id)}}"  >{{$employeeDetails->employeeId}} ({{$employeeDetails->empFirstName}} {{$employeeDetails->empLastName}})</a></b>
                      <br>
                        <span  class="dash_stitle2">Joining Date: <b>{{\Carbon\Carbon::parse($employeeDetails->empJoiningDate)->format('d-M-Y')}}</b></span>
                        <br>
                        <span class="dash_stitle3">Days: <b>{{$employeeDetails->just}}</b></span>
                      </td>
                    </tr>
                    @endforeach
                  @else
                    <tr><td></td></tr>
                    <tr class="f-12">
                        <td >No data exits.</td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4 animated bounceInLeft">
      <div class="panel widget_panel_dash" style="border-radius:4px">
        <div class="panel-header my_panel_9">
          <h5><a href="{{URL('/leave')}}" class="dash_a"><i class="fa fa-ioxhost"></i> <strong>Pending Leave Request List</strong></a></h5>
        </div>
        <div class="panel-content widget-table">
          <div class="withScroll mCustomScrollbar _mCS_18" data-height="400" style="height: 400px;">
            <div class="mCustomScrollBox mCS-light" id="mCSB_18" style="position:relative; height:100%; overflow:hidden; max-width:100%;">
            <div class="mCSB_container" style="position:relative; top:0;">
              <table class="table table-striped table-hover table-bordered">
                <tbody>
                  @if(count($pending_leave_list)>0)
                    @foreach($pending_leave_list as $pendingLeaveDetails)
                      <tr class="f-12">
                        <td ><b class="f-12"><a href="{{URL('/leave')}}"  class="dash_stitle" >{{$pendingLeaveDetails->employeeId}} ({{$pendingLeaveDetails->empFirstName}} {{$pendingLeaveDetails->empLastName}})</a></b>
                        <br>
                          <span  class="dash_stitle3">Leave type: <b> {{$pendingLeaveDetails->leave_type}}</b> </span>
                          <br>
                          <span  class="dash_stitle2">Application Date: <b>{{\Carbon\Carbon::parse($pendingLeaveDetails->created_at)->format('d-M-Y')}}</b></span>
                        </td>
                      </tr>
                    </a>
                    @endforeach
                  @else
                    <tr><td></td></tr>
                    <tr class="f-12">
                        <td >No data exits.</td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>

            </div>
          </div>
        </div>
      </div>
    </div>



    <div class="col-md-4 animated bounceInLeft">
      <div class="panel widget_panel_dash" style="border-radius:4px">
        <div class="panel-header my_panel_3">
          <h5><a href="{{URL('/report/daily_late_present')}}" class="dash_a"><i class="fa fa-history"></i> <strong>Todays Late Employee List</strong></a></h5>
        </div>
        <div class="panel-content widget-table">
          <div class="withScroll mCustomScrollbar _mCS_18" data-height="400" style="height: 400px;">
            <div class="mCustomScrollBox mCS-light" id="mCSB_18" style="position:relative; height:100%; overflow:hidden; max-width:100%;">
            <div class="mCSB_container" style="position:relative; top:0;">
              <table class="table table-striped table-hover table-bordered">

              <tbody>
                  @if(count($today_attendance1)>0)
                    @foreach($today_attendance1 as $lateatt)
                      @if($lateatt->in_time>$attendance_settings->max_entry_time)

                      <tr class="f-12">
                        <td ><b class="f-12"><a target="_BLANK" class="dash_stitle" href="{{route('employee.show',$lateatt->emp_id)}}"  >{{$lateatt->employeeId}} ({{$lateatt->empFirstName}} {{$lateatt->empLastName}})</a></b>
                        <br>
                         <span class="dash_stitle2">In Time: <b> {{$lateatt->in_time}}</b> </span>
                          <br>
                          <span class="dash_stitle3">Late Time: <b>
                              @if($lateatt->in_time>$attendance_settings->max_entry_time)
                              {{ date('G:i:s', strtotime($lateatt->in_time) - strtotime($attendance_settings->max_entry_time)) }}
                              @endif
                          </b></span>
                        </td>
                      </tr>
                      @endif
                    @endforeach
                  @else
                    <tr><td></td></tr>
                    <tr class="f-12">
                        <td >No data exits.</td>
                    </tr>
                  @endif
                </tbody>

              </table>
            </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4 animated bounceInRight">
      <div class="panel widget_panel_dash" style="border-radius:4px">
        <div class="panel-header my_panel_11">
          <h5><a href="{{URL('/expense/selectDate')}}" class="dash_a"><i class="fa fa-money"></i> <strong>Todays Expenses List</strong></a></h5>
        </div>
        <div class="panel-content widget-table">
          <div class="withScroll mCustomScrollbar _mCS_18" data-height="400" style="height: 400px;">
            <div class="mCustomScrollBox mCS-light" id="mCSB_18" style="position:relative; height:100%; overflow:hidden; max-width:100%;">
            <div class="mCSB_container" style="position:relative; top:0;">
              <table class="table table-striped table-hover table-bordered">

                <tbody>
                    @if(count($todays_expense_list)>0)
                      @foreach($todays_expense_list as $expense_list)
                        <tr class="f-12">
                          <td ><b class="f-13 dash_stitle">{{$expense_list->title}} > ({{$expense_list->categoryName}})</b>
                           <br>
                           <span  class="dash_stitle2">Amount: <b> @money($expense_list->amount)</b> </span>
                            <br>
                          </td>
                        </tr>
                      @endforeach
                    @else
                      <tr><td></td></tr>
                      <tr class="f-12">
                          <td >No data exits.</td>
                      </tr>
                    @endif
                  </tbody>
              </table>
            </div>

            </div>
          </div>
        </div>
      </div>
    </div>


       <div class="col-md-4 animated bounceInLeft">
           <div class="panel widget_panel_dash" style="border-radius:4px">
               <div class="panel-header my_panel_12">
                   <h5><a href="{{URL('/attendance/status')}}" class="dash_a"><i class="fa fa-shekel"></i> <strong>Latest Announcements</strong></a></h5>
               </div>
               <div class="panel-content widget-table">
                   <div class="withScroll mCustomScrollbar _mCS_18" data-height="400" style="height: 400px;">
                       <div class="mCustomScrollBox mCS-light" id="mCSB_18" style="position:relative; height:100%; overflow:hidden; max-width:100%;">
                           <div class="mCSB_container" style="position:relative; top:0;">
                               <table class="table table-striped table-hover table-bordered">

                                   <tbody>
                                   @if(count($notices)>0)
                                       @foreach($notices as $notice)
                                           <tr class="f-12">

                                               <td >
                                                   <b  class="f-13" ><span  class="dash_stitle3">Notice</span></b><br />
                                                   <a  class="dash_stitle"target="_NEW" href="{{route('notices.show',$notice->id)}}" >{{substr($notice->noticeDescription, 0, 177)}} .... </a>
                                                   <br>
                                                   <span  class="dash_stitle2">Notice Date: <b>{{\Carbon\Carbon::parse($notice->created_at)->format('d-M-Y')}} </b> </span>
                                                   <br>
                                               </td>


                                           </tr>
                                       @endforeach
                                   @endif
                                   @if(count($mettings)>0)
                                       @foreach($mettings as $metting)
                                           <tr class="f-12">

                                               <td >
                                                   <b  class="dash_stitle3" class="f-13" > <span  style="color:#ff8100;" >Meeting</span></b><br />
                                                   <b class="f-13"><a  class="dash_stitle1" target="_BLANK" href="{{route('meeting.show',$metting->id)}}" >{{substr($metting->msub, 0, 50)}} <br>

                                                   {{substr($metting->descrip, 0, 70)}} .... </a></b>
                                                   <br>
                                                   <span  class="dash_stitle2">Metting Date: <b>{{\Carbon\Carbon::parse($metting->created_at)->format('d-M-Y')}} </b> </span>
                                                   <br>
                                               </td>


                                           </tr>
                                       @endforeach
                                   @endif
                                   </tbody>
                               </table>
                           </div>

                       </div>
                   </div>
               </div>
           </div>
       </div>




       @php
    $datapointsOverTimeStatistic = array();
    $datasetsOverTimeStatistic = array();
    foreach($employee_overtime_statistic as $data){
      array_push($datapointsOverTimeStatistic,date("M-Y", strtotime($data->month)));
      array_push($datasetsOverTimeStatistic,$data->overtime_hours);
    }

    $MonthWiseLeaveApplication=(dashboardcontroller::getMonthWiseLeaveApplication());
    $datapointsLeaveStatistic = array();
    $datasetsLeaveStatistic = array();
    foreach($MonthWiseLeaveApplication as $data){
      array_push($datapointsLeaveStatistic,$data->monthYear);
      array_push($datasetsLeaveStatistic,$data->actual_days);
    }

    function getLastNDays($days, $format = 'd/m'){
         date_default_timezone_set('Asia/Dhaka');

          $m = date("m");
          $de= date("d");
          $y= date("Y");
          $dateArray = array();
          for($i=0; $i<=$days-1; $i++){
              $dateArray[] = date($format, mktime(0,0,0,$m,($de-$i),$y)) ;
          }
          return array_reverse($dateArray);
      }

    $days=getLastNDays(30,'Y-m-d');

    $dataPointsAttendenceStatistic = array();
    $dataSetsAttendenceStatistic = array();
    $dataSetsAbsentStatistic = array();

    foreach($days as $day){
      array_push($dataPointsAttendenceStatistic,$day);
      array_push($dataSetsAttendenceStatistic,dashboardcontroller::getAttendanceValueByDate($day));
      array_push($dataSetsAbsentStatistic,($activeemployee-dashboardcontroller::getAttendanceValueByDate($day)));
    }


      date_default_timezone_set('Asia/Dhaka');
      $datapointPresentAbsent=array();
      $present=dashboardcontroller::getAttendanceValueByDate(date("Y-m-d"));
      array_push($datapointPresentAbsent,$present);
      array_push($datapointPresentAbsent,($activeemployee-$present));


   @endphp



     <script>

         window.onload = function() {
             var ctx = document.getElementById("malefemaleEmployee").getContext('2d');
             var myChart = new Chart(ctx, {
                 type: 'doughnut',
                 data: {
                     labels: ["Male", "Female", "Others"],
                              legend: {
                                  labels: {
                                      fontColor: "white",
                                      fontSize: 18
                                  }
                            },
                     datasets: [{
                         label: '# ',
                         data: [{{$maleemployee}}, {{$femaleemployee}}, {{$othersgenderemployee}}],
                         backgroundColor: [
                         <?php if(Auth::user()->theme_style==0){  ?>
                             'rgba(79, 132, 209, 0.7)',
                             'rgba(233,30,99, 0.7)',
                             'rgba(213,0,249, 0.7)'
                        <?php }elseif(Auth::user()->theme_style==1) { ?>
                             'rgba(228,228,228, 0.7)',
                             'rgba(190,190,190, 0.7)',
                             'rgba(110,110,110, 0.7)'
                        <?php }elseif(Auth::user()->theme_style==2) { ?>
                             
                             'rgba(70, 161, 185, 0.9)',
                             'rgba(124, 187, 207, 0.9)',
                             'rgba(181, 212, 224, 0.9)'
                        <?php }elseif(Auth::user()->theme_style==3) { ?>
                             
                             'rgba(70, 161, 185, 0.9)',
                             'rgba(124, 187, 207, 0.9)',
                             'rgba(181, 212, 224, 0.9)'
                          <?php } ?>
                         ],
                         borderColor: [
                             'rgba(1,1,1,0.1)',
                             'rgba(1,1,1,0.1)',
                             'rgba(1,1,1,0.1)'

                         ],
                         borderWidth: 1
                      }]
                 },
                 options: {
                  <?php if(Auth::user()->theme_style==2) { ?>
                    legend: {
                        labels: {
                            fontColor: "white"
                        }
                    }
                  <?php } ?>
                 }
             });

             var ctx = document.getElementById("monthlyLeave").getContext('2d');
             var myChart = new Chart(ctx, {
                 type: 'doughnut',
                 data: {
                     labels: <?php echo json_encode($datapointsLeaveStatistic, JSON_NUMERIC_CHECK); ?>,
                     datasets: [{
                         data: <?php echo json_encode($datasetsLeaveStatistic, JSON_NUMERIC_CHECK); ?>,
                         backgroundColor: [
                         <?php if(Auth::user()->theme_style==0){  ?>
                             'rgb(142, 68, 173, 0.7)',
                             'rgb(150, 40, 27, 0.7)',
                             'rgb(251,192,45, 0.7)',
                             'rgb(250, 126, 34, 0.7)'
                        <?php }elseif(Auth::user()->theme_style==1) { ?>
                             'rgba(228,228,228, 0.9)',
                             'rgba(190,190,190, 0.9)',
                             'rgba(140,140,140, 0.9)',
                             'rgba(110,110,110, 0.9)'
                        <?php }elseif(Auth::user()->theme_style==2) { ?>
                             'rgba(57, 134, 155, 0.9)',
                             'rgba(70, 161, 185, 0.9)',
                             'rgba(124, 187, 207, 0.9)',
                             'rgba(181, 212, 224, 0.9)'
                        <?php }elseif(Auth::user()->theme_style==3) { ?>
                             'rgba(57, 134, 155, 0.9)',
                             'rgba(70, 161, 185, 0.9)',
                             'rgba(124, 187, 207, 0.9)',
                             'rgba(181, 212, 224, 0.9)'
                          <?php } ?>
                         ],
                         borderColor: [
                             'rgba(1,1,1,0.1)',
                             'rgba(1,1,1,0.1)',
                             'rgba(1,1,1,0.1)',
                             'rgba(1,1,1,0.1)'
                         ],
                         borderWidth: 1
                     }]
                 },
                 options: {
                  <?php if(Auth::user()->theme_style==2) { ?>
                    legend: {
                        labels: {
                            fontColor: "white"
                        }
                    }
                  <?php } ?>
                 }
             });


             var ctx = document.getElementById("present-absent2").getContext('2d');
             var myChart = new Chart(ctx, {
                 type: 'doughnut',
                 data: {
                     labels: ["Present", "Absent"],
                     datasets: [{
                         data: <?php echo json_encode($datapointPresentAbsent, JSON_NUMERIC_CHECK); ?>,
                         backgroundColor: [
                        <?php if(Auth::user()->theme_style==0){  ?>
                             'rgba(46, 204, 113, 0.7)',
                             'rgba(231, 76, 60, 0.7)'
                        <?php }elseif(Auth::user()->theme_style==1) { ?>
                             'rgba(228,228,228, 0.7)',
                             'rgba(180,180,180, 0.8)'
                        <?php }elseif(Auth::user()->theme_style==2) { ?>
                             'rgba(70, 161, 185, 0.9)',
                             'rgba(124, 187, 207, 0.9)',
                        <?php }elseif(Auth::user()->theme_style==3) { ?>
                             'rgba(70, 161, 185, 0.9)',
                             'rgba(124, 187, 207, 0.9)'
                          <?php } ?>

                         ],
                         borderColor: [
                             'rgba(1, 1, 1, .1)',
                             'rgba(1, 1, 1, .1)'
                         ],
                         borderWidth: 1
                     }]
                 },
                 options: {
                  <?php if(Auth::user()->theme_style==2) { ?>
                    legend: {
                        labels: {
                            fontColor: "white"
                        }
                    }
                  <?php } ?>
                 }
             });


             var ctx = document.getElementById("attendancestatistic2").getContext('2d');
             var myChart = new Chart(ctx, {
                 type: 'line',
                 data: {

                     labels: <?php echo json_encode($dataPointsAttendenceStatistic, JSON_NUMERIC_CHECK); ?>,
                     datasets: [{
                         label: 'Present Employee',
                         data: <?php echo json_encode($dataSetsAttendenceStatistic, JSON_NUMERIC_CHECK); ?>,
                         backgroundColor:
                         <?php if(Auth::user()->theme_style==0){  ?>
                             'rgba(0, 128, 0, 0.5)',
                        <?php }elseif(Auth::user()->theme_style==1) { ?>
                             'rgba(180,180,180, 0.9)',
                        <?php }elseif(Auth::user()->theme_style==2) { ?>
                             'rgba(12, 120, 152, 1)',
                        <?php }elseif(Auth::user()->theme_style==3) { ?>
                             'rgba(12, 120, 152, 1)',
                          <?php } ?>
                             
                         borderColor:
                             'rgba(1, 1, 1, 0.3)',
                         borderWidth: 1
                     }]
                 },
                 options: {
                  <?php if(Auth::user()->theme_style==2) { ?>
                    legend: {
                        labels: {
                            fontColor: "#dedede"
                        }
                    },
                    scales: {
                         yAxes: [{
                             ticks: {
                              fontColor: "#dedede",
                                 beginAtZero:true
                             }
                         }],
                         xAxes: [{
                            ticks: {
                                fontColor: "#dedede"
                            }
                        }]
                     }
                  <?php }else{ ?>
                     scales: {
                         yAxes: [{
                             ticks: {
                                 beginAtZero:true
                             }
                         }]
                     }
                  <?php } ?>

                 }
             });


             var ctx = document.getElementById("absentstatistic").getContext('2d');
             var myChart = new Chart(ctx, {
                 type: 'line',
                 data: {

                     labels: <?php echo json_encode($dataPointsAttendenceStatistic, JSON_NUMERIC_CHECK); ?>,
                     datasets: [{
                         label: 'Absent Employee',
                         data: <?php echo json_encode($dataSetsAbsentStatistic, JSON_NUMERIC_CHECK); ?>,
                         backgroundColor:
                         <?php if(Auth::user()->theme_style==0){  ?>
                             'rgba(229,57,47, .5)',
                        <?php }elseif(Auth::user()->theme_style==1) { ?>
                             'rgba(228,228,228, 1)',
                        <?php }elseif(Auth::user()->theme_style==2) { ?>
                             'rgba(124, 187, 207, 1)',
                        <?php }elseif(Auth::user()->theme_style==3) { ?>
                             'rgba(124, 187, 207, 1)',
                          <?php } ?>
                         borderColor:
                             'rgba(1 , 1, 1, 0.3)',
                         borderWidth: 1
                     }]
                 },
                 options: {
                  <?php if(Auth::user()->theme_style==2) { ?>
                    legend: {
                        labels: {
                            fontColor: "#dedede"
                        }
                    },
                    scales: {
                         yAxes: [{
                             ticks: {
                              fontColor: "#dedede",
                                 beginAtZero:true
                             }
                         }],
                         xAxes: [{
                            ticks: {
                                fontColor: "#dedede"
                            }
                        }]
                     }
                  <?php }else{ ?>
                     scales: {
                         yAxes: [{
                             ticks: {
                                 beginAtZero:true
                             }
                         }]
                     }
                  <?php } ?>

                 }

             });

         }

     </script>
  </div>
 </div>
@include('include.copyright')
@endsection
