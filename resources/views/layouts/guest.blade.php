<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>FEITS HRMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
	<link rel="shortcut icon" href="{{url('hrm_script/images/favicon.png')}}" type="image/png">
    <link href="{{url('hrm_script/css/style.css')}}" rel="stylesheet">
    <link href="{{url('hrm_script/css/animate.css')}}" rel="stylesheet">
    <link href="{{url('hrm_script/css/ui.css')}}" rel="stylesheet">
    <link href="{{url('hrm_script/plugins/bootstrap-loading/lada.min.css')}}" rel="stylesheet">
</head>
<body class="account separate-inputs separate-inputs boxed custom-background" data-page="login">
<!-- BEGIN LOGIN BOX -->
@yield('content')
<script src="{{url('hrm_script//plugins/jquery/jquery-1.11.1.min.js')}}"></script>
<script src="{{url('hrm_script/plugins/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{url('hrm_script/plugins/gsap/main-gsap.min.js')}}"></script>
<script src="{{url('hrm_script/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('hrm_script/plugins/backstretch/backstretch.min.js')}}"></script>
<script src="{{url('hrm_script/plugins/bootstrap-loading/lada.min.js')}}"></script>
<script src="{{url('hrm_script/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{url('hrm_script/plugins/jquery-validation/additional-methods.min.js')}}"></script>
<script src="{{url('hrm_script/js/login-v1.js')}}"></script>
</body>
</html>