@extends('layouts.guest')
@section('content')
    <div class="container" id="login-block">
        <div class="row animated jackInTheBox">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <div class="account-wall">
                    <br />
                    <h3 class="text-center"><b>HRMS LOGIN</b></h3>
                    <hr>
                    <i class="user-img icons-faces-users-03"></i>
                    <form method="POST" class="form-signin" action="{{ route('login') }}" role="form">
                        @csrf
                        <div class="append-icon">
                            <!--<input id="email" type="email" class="form-control form-white" name="email" value="admin@email.com" placeholder="Email" required autofocus >-->
                             <input id="email" type="email" autocomplete="off" class="form-control form-white username{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                            <i class="icon-user"></i>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" style="color:red;">
                                   <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <br>
                        <div class="append-icon m-b-20">
                            <input  type="password" autocomplete="off" class="form-control form-white password{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  placeholder="Password" required >
                            <i class="icon-lock"></i>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-lg btn-primary btn-block ladda-button" data-style="expand-left"> <i class="fa fa-sign-in"></i> &nbsp; Sign In</button>
                        <div class="clearfix">
                            <p class="pull-left m-t-20"><a  href="{{ route('password.request') }}">Forgot password?</a></p>
                        </div>
                        
                        <hr />
                       <center><span><a style="color:#20224c;" href="https://feits.co/" target="_BLANK">Copyright &copy; Far-East IT Solutions Limited</a></span></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection