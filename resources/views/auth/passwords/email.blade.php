@extends('layouts.guest')

@section('content')
    <div class="container" id="login-block">
        <div class="row animated jackInTheBox">
            <div class="col-sm-6 col-md-6 col-md-offset-3">
                <div class="account-wall">
                    <h3><b>HRMS Password Reset</b></h3>
                    <hr>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                             <div class="col-md-12">
                                <input id="email" autofocus type="email" class="form-control form-white {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email Address" >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br />
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-danger">
                                   <i class="fa fa-envelope"></i> &nbsp; {{ __('Send Password Reset Link') }}
                                </button>
                                <a href="{{url('/')}}"  class="btn btn-success" style="color:white !important;"> <i class="fa fa-sign-in"></i> Login</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection