@extends('layouts.app')

@section('content')
    {{--<div class="container" id="login-block">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-6 col-md-6 col-md-offset-3">--}}
                {{--<div class="account-wall">--}}
                    {{--<i class="user-img icons-faces-users-03"></i>--}}
                    {{--<form class="form-signup" action="dashboard.html" role="form">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-sm-6">--}}
                                {{--<div class="append-icon">--}}
                                    {{--<input type="text" name="firstname" id="firstname" class="form-control form-white firstname" placeholder="First Name" required autofocus>--}}
                                    {{--<i class="icon-user"></i>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-6">--}}
                                {{--<div class="append-icon">--}}
                                    {{--<input type="text" name="lastname" id="lastname" class="form-control form-white lastname" placeholder="Last Name" required>--}}
                                    {{--<i class="icon-user"></i>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="append-icon">--}}
                            {{--<input type="email" name="email" id="email" class="form-control form-white email" placeholder="Email" required>--}}
                            {{--<i class="icon-envelope"></i>--}}
                        {{--</div>--}}
                        {{--<div class="append-icon">--}}
                            {{--<input type="password" name="password" id="password" class="form-control form-white password" placeholder="Password" required>--}}
                            {{--<i class="icon-lock"></i>--}}
                        {{--</div>--}}
                        {{--<div class="append-icon m-b-20">--}}
                            {{--<input type="password" name="password" id="password2" class="form-control form-white password2" placeholder="Repeat Password" required>--}}
                            {{--<i class="icon-lock"></i>--}}
                        {{--</div>--}}
                        {{--<div class="terms option-group">--}}
                            {{--<label  for="terms" class="m-t-10">--}}
                                {{--<input type="checkbox" name="terms" id="terms" data-checkbox="icheckbox_square-blue" required/>--}}
                                {{--I agree with terms and conditions--}}
                            {{--</label>--}}
                        {{--</div>--}}
                        {{--<button type="submit" id="submit-form" class="btn btn-lg btn-dark m-t-20" data-style="expand-left">Register</button>--}}
                        {{--<div class="social-btn">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<button type="button" class="btn btn-lg btn-block btn-primary"><i class="fa fa-facebook pull-left"></i>Sign In with Facebook</button>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<button type="button" class="btn btn-lg btn-block btn-danger"><i class="fa fa-google pull-left"></i>Sign In with Google</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix">--}}
                            {{--<p class="pull-right m-t-20"><a href="user-login-v1.html">Already have an account? Sign In</a></p>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<p class="account-copyright">--}}
            {{--<span>Copyright © 2015 </span><span>THEMES LAB</span>.<span>All rights reserved.</span>--}}
        {{--</p>--}}
        {{--<div id="account-builder">--}}
            {{--<form class="form-horizontal" role="form">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<i class="fa fa-spin fa-gear"></i>--}}
                        {{--<h3><strong>Customize</strong> Login Page</h3>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="col-xs-8 control-label">Social Login</label>--}}
                            {{--<div class="col-xs-4">--}}
                                {{--<label class="switch m-r-20">--}}
                                    {{--<input id="social-cb" type="checkbox" class="switch-input" checked>--}}
                                    {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
                                    {{--<span class="switch-handle"></span>--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="col-xs-8 control-label">Boxed Form</label>--}}
                            {{--<div class="col-xs-4">--}}
                                {{--<label class="switch m-r-20">--}}
                                    {{--<input id="boxed-cb" type="checkbox" class="switch-input" checked>--}}
                                    {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
                                    {{--<span class="switch-handle"></span>--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="col-xs-8 control-label">Background Image</label>--}}
                            {{--<div class="col-xs-4">--}}
                                {{--<label class="switch m-r-20">--}}
                                    {{--<input id="image-cb" type="checkbox" class="switch-input" checked>--}}
                                    {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
                                    {{--<span class="switch-handle"></span>--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="col-xs-8 control-label">Background Slides</label>--}}
                            {{--<div class="col-xs-4">--}}
                                {{--<label class="switch m-r-20">--}}
                                    {{--<input id="slide-cb" type="checkbox" class="switch-input">--}}
                                    {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
                                    {{--<span class="switch-handle"></span>--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="col-xs-8 control-label">Terms Checkbox</label>--}}
                            {{--<div class="col-xs-4">--}}
                                {{--<label class="switch m-r-20">--}}
                                    {{--<input id="terms-cb" type="checkbox" class="switch-input" checked>--}}
                                    {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
                                    {{--<span class="switch-handle"></span>--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="col-xs-8 control-label">User Image</label>--}}
                            {{--<div class="col-xs-4">--}}
                                {{--<label class="switch m-r-20">--}}
                                    {{--<input id="user-cb" type="checkbox" class="switch-input">--}}
                                    {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
                                    {{--<span class="switch-handle"></span>--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</form>--}}
        {{--</div>--}}
    {{--</div>--}}


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
