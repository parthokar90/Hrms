
      
        
       @if(count($employeesByDesignation)!=0)
        <div class="panel-content pagination2 table-responsive">
        <h3><i class="fa fa-table"></i> <strong>Selected Employees</strong></h3>
            <table class="table table-hover table-dynamic dataTable no-footer">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee ID</th>
                    <th>Designation</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employeesByDesignation as $a)
                    <tr id="product{{$a->id}}">
                        <td>{{$a->empFirstName}} {{$a->empLastName}}</td>
                        <td>{{$a->employeeId}}</td>
                        <td>{{$a->designation}}</td>
                        <td>
                            <button value="{{$a->id}}" onclick="testfunc(this)" id="testid" type="button" class="btn btn-sm btn-danger">Remove</button>
                         </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @else
        <h4 style="margin-bottom: 3px; margin-top:0px; text-align: center;">No Data Found</h4>
       <hr>
        @endif



<script>
    function testfunc(that){
        const del= $(that).val();
        //alert(del);

        $.ajax({
            url:"training/delete/"+del, 
            method:'GET',
            dataType:'html',
            success: function (data) {
                $("#product" + del).remove();
            },
            error:function (xhr) {
                console.log('failed');    
            }
        });
    }
</script>
