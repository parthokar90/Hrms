@extends('layouts.master')
@section('title', 'Assign Training')
@section('content')
    <div class="page-content">
        <div class="row">
            @if(Session::has('success1'))
                <p id="alert_message" class="alert alert-success">{{Session::get('success1')}}</p>
            @endif
            @if(Session::has('success2'))
                <p id="alert_message" class="alert alert-success">{{Session::get('success2')}}</p>
            @endif
            <div class="col-md-12">
                <div class="panel panel-default no-bd">
                    <div class="panel-header bg-dark">
                        <h2 class="panel-title"><strong> Assign Training</strong></h2>
                    </div>
                    <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    {!! Form::open(['method'=>'POST','url' =>'training/assign', 'files'=>true]) !!}
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="required form-group">
                                                <label class="control-label">Choose training</label>
                                                <div class="option-group">
                                                    {!! Form::select('training_id',[''=>'Select training']+ $training,null,['class'=>'form-control','required'=>''])!!}
                                                </div>
                                            </div>
                                        </div>
                                    
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Assign By</label>
                                                    <div class="option-group">
                                                        <select id="language" onchange="interviewCheck(this);" name="interviewStatus" class="form-control">
                                                            <option value="">Select Method</option>
                                                            <option value="Designation">By Designation</option>
                                                            <option value="Employee">By Employee</option> </select>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                            
                                    <div class="row">   
                                        <div id="conditionDisplay1">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Choose Designations</label>
                                                    <div>
                                                        {!! Form::select('designation_id[]', $designation,null,['class' => 'form-control','data-search'=>"true" ,'onchange'=>'icheck(this)','multiple' => 'multiple'])!!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div id="conditionDisplay2">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Choose Employees</label>
                                                    <div class="option-group">
                                                        {!! Form::select('employee_id[]',$employees,null,['class'=>'form-control', 'data-search'=>'true' ,'onchange'=>'emp(this)' ,'multiple' => 'multiple'])!!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div id="conditionDisplay3">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label> Training Start Date </label>
                                                    <div class="">
                                                        <input type="text" class="date-picker form-control" required name="startDate"   placeholder="Click here for pick start date ..." />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>

                                
                                    <div class="row">
                                        <div id="table">
                                            <div class="col-sm-6 portlets">
                                                @if(isset($employeesByDesignation))
                                                    @include('training.employeeByDesignation')
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-7 center-block">
                                            <button type="submit" class="btn btn-primary center-block">Assign Training</button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}

                             </div>       
                        </div>
                    
            </div>
        </div>
    </div>

    @include('include.copyright')

    <script>
        function interviewCheck(that) {
            if(that.value=='Designation'){
//                alert('accepted');
                document.getElementById('conditionDisplay1').style.display="block";
                document.getElementById('conditionDisplay3').style.display="block";
                document.getElementById('conditionDisplay2').style.display="none";
            }
            else if(that.value=='Employee'){
//                alert('Rejected');
                document.getElementById('conditionDisplay1').style.display="none";
                document.getElementById('conditionDisplay2').style.display="block";
                document.getElementById('conditionDisplay3').style.display="block";
            }
            else{
                document.getElementById('conditionDisplay1').style.display="none";
                document.getElementById('conditionDisplay3').style.display="none";
                document.getElementById('conditionDisplay2').style.display="none";
            }

        }

        function icheck(that){ 
            const designation= $(that).val();
            //alert(designation);

            $.ajax({
                url:"training/des_table/"+designation, 
                method:'GET',
                dataType:'html',
                success: function (data) {
                    $('#table').html(data);

                },
                error:function (xhr) {
                    console.log('failed');    
                }
            });
        }

        function emp(that){ 
            const empid= $(that).val();

            $.ajax({
                url:"training/addEmployee/"+empid, 
                method:'GET',
                dataType:'html',
                success: function (data) {
                    $('#table').html(data);

                },
                error:function (xhr) {
                    console.log('failed');    
                }
            });
        }


        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

    </script>
   

@endsection