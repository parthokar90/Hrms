@extends('layouts.master')
@section('title', 'Training List')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header">
                        <h3> <strong>Training</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="form-group">
                            <a data-toggle="modal" data-target="#addTraining" class="btn btn-success btn-md" type="submit"><i class="fa fa-edit"></i>Add Training</a>
                            <div class="modal fade" id="addTraining" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h5 class="modal-title" id="exampleModalLongTitle">Add Training</h5>

                                        </div>
                                        <div class="modal-body">
                                            {{--{{Form::open(array('url' => 'addTraining','method' => 'post'))}}--}}
                                            {!! Form::open(['method'=>'POST', 'action'=>'TrainingController@storeTraining','files'=>true]) !!}
                                                <div class="form-group">
                                                    <label for="trainingName">Training Name</label>
                                                    <input type="text" name="training_name" class="form-control form-white" id="trainingName"  placeholder="Enter Training Name" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="totaldays">Duration</label>
                                                    <input type="number" name="duration" step="any" class="form-control form-white" id="totaldays"  placeholder="Enter Number of Training Days" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="description">Training Description</label>
                                                    <input type="text" name="description" step="any" class="form-control form-white" id="description"  placeholder="Enter Training Description" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="attachment">Attachment</label>
                                                    <input type="file" name="attachment" step="any" class="form-control form-white" id="attachment"  placeholder="Upload Attachment (If any)">
                                                </div>
                                                <input type="hidden" name="emp_name" value="{{auth::user()->name}}">
                                                <input type="hidden" name="emp_id" value="{{auth::user()->emp_id}}">
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save</button>
                                                </div>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-header">
                            <h3><i class="fa fa-table"></i> <strong>Training List</strong></h3>
                        </div>
                        <div class="panel-content pagination2 table-responsive">
                            <table class="table table-hover table-dynamic">
                                <thead>
                                <tr>
                                    <th>Training Name</th>
                                    <th>Training Description</th>
                                    <th>Duration</th>
                                    <th>Attachment</th>
                    
                                    <th>Update/Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($training_data as $training)
                                        <tr>
                                            <td>{{$training->training_name}}</td>
                                            <td>{{$training->description}}</td>
                                            <td>{{$training->duration}}</td>
                                            <td>{{$training->attachment}}</td>
 <td>
                                                <a data-toggle="modal" data-target="#{{$training->id}}" class="btn btn-success btn-sm" type="submit"><i class="fa fa-edit"></i></a>
                                                <a href="{{ url('/training_delete/'.$training->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="1{{$training->id}}" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Assign Training to Employees</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        {{Form::open(array('url' => 'employee_training','method' => 'post'))}}
                                                            <div class="form-group">

                                                                <div class="form-group">
                                                                    <label class="control-label">Choose by Designation</label>
                                                                    <div class="option-group">
                                                                        {!! Form::select('designation',[''=>'Select employee designaton']+ $designation,null,['class'=>'form-control']) !!}
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Choose by employee
                                                                    </label>
                                                                    <div class="option-group">
                                                                        {!! Form::select('employee',[''=>'Select employee']+ $employee,null,['class'=>'form-control']) !!}
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label> Training Start Date </label>
                                                                    <div class="">
                                                                        <input type="text" class="date-picker form-control" required name="startDate"  placeholder="Click here for pick training start date ..." />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label> Training End Date </label>
                                                                    <div class="">
                                                                        <input type="text" class="date-picker form-control" required name="endDate"  placeholder="Click here for pick training end date ..." />
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="emp_name" value="{{auth::user()->name}}">
                                                                <input type="hidden" name="emp_id" value="{{auth::user()->emp_id}}">
                                                                <input type="hidden" name="training_id" value="{{$training->id}}">
                                                                <button type="submit" class="btn btn-primary">Assign Training</button>
                                                            </div>
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade" id="{{$training->id}}" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Update Training info</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        {{Form::open(array('url' => 'training_update','method' => 'post', 'files'=>true))}}
                                                        <div class="form-group">

                                                            <div class="form-group">
                                                                <label for="trainingName">Training Name</label>
                                                                <input type="text" name="training_name" class="form-control form-white" id="trainingName" value="{{$training->training_name}}"  placeholder="Enter Teaining Name" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="totaldays">Duration</label>
                                                                <input type="number" name="duration" step="any" class="form-control form-white" id="totaldays" value="{{$training->duration}}" placeholder="Enter Number of Training Days">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="description">Training Description</label>
                                                                <input type="text" name="description" step="any" class="form-control form-white" id="description" value="{{$training->description}}" placeholder="Enter Training Description">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="attachment">Attachment</label>
                                                                <input type="file" name="attachment" step="any" class="form-control form-white" id="attachment" value="{{$training->attachment}}" placeholder="Upload Attachment (If any)">
                                                            </div>
                                                            <input type="hidden" name="emp_name" value="{{auth::user()->name}}">
                                                            <input type="hidden" name="emp_id" value="{{auth::user()->emp_id}}">
                                                            <input type="hidden" name="training_id" value="{{$training->id}}">
                                                            <button type="submit" class="btn btn-primary">Update</button>
                                                            {{ Form::close() }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @include('include.copyright')

@endsection