<div class="panel">
        <div class="panel-header panel-controls">
            <h3><i class="fa fa-table"></i> <strong>Selected Employees </strong></h3>
        </div>
        <div class="panel-content pagination2 table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee ID</th>
                    <th>Designation</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employeesByID as $a)
                    <tr>
                        <td>{{$a->empFirstName}} {{$a->empLastName}}</td>
                        <td>{{$a->employeeId}}</td>
                        <td>{{$a->designation}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>



